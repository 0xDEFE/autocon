
#include <wchar.h>
#include <stdio.h>

#include <System.SysUtils.hpp>


#include "BasicUtils.h"
#include "AutoconLogging.h"

#include <Dbghelp.h>
#pragma comment(lib, "Dbghelp.lib")

std::wstring CharToWCharStr(const char* str)
{
	if(!str)
		return L"NULL";
	int len = strlen(str);

	std::wstring wstr;

	for(int i=0;i<len;i++)
	{
		wstr+=std::btowc(str[i]);
	}
	return wstr;
}

std::string  WCharToCharStr(const wchar_t* str)
{
	if(!str)
		return "NULL";
	int len = wcslen(str);

	std::string cstr;

	for(int i=0;i<len;i++)
	{
		cstr+=std::wctob(str[i]);
	}
	return cstr;
}

void TraceCallStack()
{
	unsigned int   i;
	void         * stack[ 100 ];
	unsigned short frames;
	SYMBOL_INFO  * symbol;
	HANDLE         process;

	process = GetCurrentProcess();

	if(!SymInitialize( process, NULL, false/*true*/ ))
	{
		//TRACE("SymInitialize - FAILED!\r\n");
		return;
    }

	frames               = CaptureStackBackTrace( 0, 100, stack, NULL );
	symbol               = ( SYMBOL_INFO * )calloc( sizeof( SYMBOL_INFO ) + 256 * sizeof( char ), 1 );
	symbol->MaxNameLen   = 255;
	symbol->SizeOfStruct = sizeof( SYMBOL_INFO );

	IMAGEHLP_LINE *line = (IMAGEHLP_LINE *)malloc(sizeof(IMAGEHLP_LINE));
    line->SizeOfStruct = sizeof(IMAGEHLP_LINE);

	AnsiString report;

	report+="Stack trace:\r\n";
	for( i = 0; i < frames; i++ )
	{
		SymFromAddr( process, ( DWORD64 )( stack[ i ] ), 0, symbol );

		report+=StringFormatA( "%i: %s - 0x%0X", frames - i - 1, symbol->Name, symbol->Address );

		/*DWORD dwDisplacement;
		if(SymGetLineFromAddr(process, (DWORD)(stack[i]), &dwDisplacement, line))
		{
			report+=StringFormatA( "\t[ line=%d, file=%s ]", line->LineNumber,line->FileName);
		}
		else
		{
			DWORD errCode = GetLastError();
			int t=0;
			t++;
		}*/

		report+="\r\n";
	};

	//TRACE(report.c_str());

	free( symbol );
}

const char* GetAliveFilePath(const char* paths[], const int pathCount)
{
	for( int i = 0; i < pathCount; i++)
	{
		if(FileExists(paths[i],true))
		{
			return paths[i];
		}
	}
	return NULL;
}

const char* GetAliveFolderPath(const char* paths[], const int pathCount)
{
	for( int i = 0; i < pathCount; i++)
	{
		if(DirectoryExists(paths[i],true))
		{
			return paths[i];
		}
	}
	return NULL;
}

//---------------Path Mgr-------------------

std::map<UINT, sPathData> PathMgr::pathItems;

UINT calculateHash(const wchar_t *str)
{
	static UINT lenStr = 0;
	static UINT hash = 0;  // seed
	static UINT step = 0;  // if string is too long, don't hash all its chars
	static UINT iStr;

	//init
	lenStr = wcslen(str);
	hash = lenStr;
	step = (lenStr>>5)+1;

	//compute hash
	for (iStr=lenStr; iStr>=step; iStr-=step)
		hash = hash ^ ((hash<<5)+(hash>>2)+(wchar_t)(str[iStr-1]));

	return hash;
}

bool PathMgr::isNameCharacter(const wchar_t c)
{
	if((c >= L'a') && (c <= L'z'))
		return true;
	if((c >= L'A') && (c <= L'Z'))
		return true;
	if((c >= L'0') && (c <= L'9'))
		return true;
	if(wcschr(L"$_-#!~[]{}",c) != NULL)
		return true;
	return false;
}

bool PathMgr::isName(const wchar_t* name)
{
	UINT lenStr = wcslen(name);
	for(UINT i = 0; i < lenStr; i++)
	{
		if(!isNameCharacter(name[i]))
			return false;
	}
	return true;
}

UnicodeString PathMgr::transformPath(const wchar_t* path)
{
	//UnicodeString pathStr = path;
	if(DirectoryExists(path,true))
		return path;
	else if(DirectoryExists(UnicodeString("..\\") + path,true))
		return UnicodeString("..\\") + path;

	return "";
}

void PathMgr::PushPath(const wchar_t* name, const wchar_t* path, bool bTransform)
{
	UnicodeString nameStr = UnicodeString(name).UpperCase();

	if(!isName(nameStr.c_str()))
		return;

	UINT hash = calculateHash(nameStr.c_str());

	if( pathItems[hash].name.Length() == 0 )
		pathItems[hash].name = nameStr;
	else if(pathItems[hash].name != nameStr)
		return; //one hash, two strings

	UnicodeString pathStr = bTransform ? transformPath(path) : UnicodeString(path);
    if(!DirectoryExists( pathStr ))
    {
        assert(false);
        return;
    }
	for(unsigned int i = 0; i < pathItems[hash].paths.size(); i++)
	{
		if(pathItems[hash].paths[i] == pathStr)
			return;
	}

	pathItems[hash].paths.push_back(pathStr);
}

void PathMgr::RemovePath(const wchar_t* name)
{
	UnicodeString nameStr = UnicodeString(name).UpperCase();

	UINT hash = calculateHash(nameStr.c_str());

	std::map<UINT, sPathData>::iterator it = pathItems.find(hash);
	if(it!=pathItems.end())
	{
    	pathItems.erase(it);
    }
}

UnicodeString PathMgr::GetPathFromName(const wchar_t* name)
{
	UnicodeString nameStr = UnicodeString(name).UpperCase();

	UINT hash = calculateHash(nameStr.c_str());

	std::map<UINT, sPathData>::iterator it = pathItems.find(hash);
	if(it!=pathItems.end())
	{
		for(unsigned int i = 0; i < it->second.paths.size(); i++)
		{
			if(DirectoryExists(it->second.paths[i],true))
				return it->second.paths[i];
			if(FileExists(it->second.paths[i],true))
				return it->second.paths[i];
		}
	}
	return "";
}

UnicodeString PathMgr::GetPath(const wchar_t* path)
{
	UnicodeString pathStr = path;

	enum eSearchState
	{
		SS_OUTSIDE_BRACKET,
		SS_INSIDE_BRACKET,
		SS_END_BRACKET,
		SS_ERROR
	} sstate = SS_OUTSIDE_BRACKET;

	int bracketStart = -1;
	int bracketEnd = -1;
	UnicodeString finalStr;
	for(int i = 0; i < pathStr.Length();)
	{
		wchar_t c = pathStr.c_str()[i];

		switch( sstate )
		{
			case SS_OUTSIDE_BRACKET:
				i++;
				if(c == L'(') {bracketStart = i; sstate = SS_INSIDE_BRACKET; }
				else if(c == L')') {sstate = SS_ERROR; i--;}
				else finalStr+=c;
				break;
			case SS_INSIDE_BRACKET:
				if(c == L')') {bracketEnd = i-1; sstate = SS_END_BRACKET;}
				else if( !isNameCharacter(c) ) {sstate = SS_ERROR;}
				else i++;
				break;
			case SS_END_BRACKET:
				i++;
				if(	(bracketStart < 0) || (bracketStart >= pathStr.Length()) ||
					  (bracketEnd < 0) || (bracketEnd >= pathStr.Length()) ||
					  (bracketStart >= bracketEnd))
				{
					sstate = SS_ERROR;
				}
				else
				{
					UnicodeString pathName = pathStr.SubString(bracketStart + 1,(bracketEnd - bracketStart) + 1).UpperCase();
					UnicodeString foundPath = GetPath( GetPathFromName(pathName.c_str()).c_str() );//Recursion for nested pathes
					if(foundPath.IsEmpty())
						sstate = SS_ERROR;
					else
					{
						finalStr+=foundPath;
						bracketStart = -1;
						bracketEnd = -1;
						sstate = SS_OUTSIDE_BRACKET;
					}
                }
				break;
			case SS_ERROR:
				return "";
				break;
		}

    }
    return finalStr;
}

