object LogForm: TLogForm
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSizeToolWin
  Caption = 'Log Form'
  ClientHeight = 37
  ClientWidth = 74
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Visible = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 74
    Height = 37
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 3
    Caption = ' '
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 6
      Top = 68
      Height = 1
      Visible = False
      ExplicitLeft = 455
      ExplicitTop = 38
      ExplicitHeight = 292
    end
    object Panel6: TPanel
      Left = 3
      Top = 3
      Width = 68
      Height = 65
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel48: TPanel
        Left = 71
        Top = 0
        Width = 0
        Height = 65
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LogAScan: TSpeedButton
          Left = 455
          Top = 2
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 1
          Caption = 'AScan'
          ParentShowHint = False
          ShowHint = False
        end
        object LogBScan: TSpeedButton
          Left = 704
          Top = 2
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 4
          Caption = 'BScan'
        end
        object LogPathEncoder: TSpeedButton
          Left = 621
          Top = 2
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 3
          Caption = 'Path Encoder'
        end
        object LogIn: TSpeedButton
          Left = 191
          Top = 2
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 22
          Caption = 'In'
          ParentShowHint = False
          ShowHint = False
        end
        object LogOut: TSpeedButton
          Left = 360
          Top = 2
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 33
          Caption = 'Out'
        end
        object LogState: TSpeedButton
          Left = 13
          Top = 1
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 55
          Caption = 'On'
          ParentShowHint = False
          ShowHint = False
        end
        object LogAScanMeasure: TSpeedButton
          Left = 538
          Top = 2
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 2
          Caption = 'AScan Measure'
          ParentShowHint = False
          ShowHint = False
        end
        object LogASD: TSpeedButton
          Left = 538
          Top = 32
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 7
          Caption = 'ASD'
        end
        object LogBScan2: TSpeedButton
          Left = 456
          Top = 32
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 5
          Caption = 'BScan2'
        end
        object LogNoBody: TSpeedButton
          Left = 273
          Top = 2
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 225
          Caption = 'No Body'
          ParentShowHint = False
          ShowHint = False
        end
        object SpeedButton2: TSpeedButton
          Left = 18
          Top = 44
          Width = 107
          Height = 22
          GroupIndex = 222
          Down = True
          Caption = '1'
          Visible = False
        end
        object SpeedButton5: TSpeedButton
          Left = 131
          Top = 44
          Width = 107
          Height = 22
          GroupIndex = 222
          Caption = '2'
          Visible = False
        end
        object SpeedButton6: TSpeedButton
          Left = 244
          Top = 44
          Width = 107
          Height = 22
          GroupIndex = 222
          Caption = '3'
          Visible = False
        end
        object LogSerialNumber: TSpeedButton
          Left = 621
          Top = 32
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 8
          Caption = 'SN'
        end
        object LogFWver: TSpeedButton
          Left = 704
          Top = 32
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 9
          Caption = 'FWver'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object UMUManage: TSpeedButton
          Left = 790
          Top = 32
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 90
          Caption = 'UMU Manage'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LoggerMsgBtn: TSpeedButton
          Left = 790
          Top = 2
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 90
          Caption = 'Logger'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Button12: TButton
          Left = 96
          Top = 1
          Width = 80
          Height = 27
          Caption = 'Clear'
          TabOrder = 0
          OnClick = Button12Click
        end
        object Panel27: TPanel
          Left = 182
          Top = 2
          Width = 4
          Height = 59
          BevelOuter = bvLowered
          TabOrder = 1
        end
        object Panel36: TPanel
          Left = 5
          Top = 2
          Width = 4
          Height = 59
          BevelOuter = bvLowered
          TabOrder = 2
        end
        object Panel45: TPanel
          Left = 446
          Top = 2
          Width = 4
          Height = 59
          BevelOuter = bvLowered
          TabOrder = 3
        end
        object CheckBoxOneMessage: TCheckBox
          Left = 15
          Top = 34
          Width = 98
          Height = 17
          Caption = 'One Message'
          TabOrder = 4
        end
      end
      object Panel49: TPanel
        Left = 0
        Top = 0
        Width = 71
        Height = 65
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object CheckBox1: TCheckBox
          Left = 0
          Top = 6
          Width = 73
          Height = 17
          Caption = ' Show Log'
          TabOrder = 0
          OnClick = CheckBox1Click
        end
      end
    end
    object PersistDataListBox: TListBox
      Left = 3
      Top = 68
      Width = 3
      Height = 1
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 1
    end
    object Memo1: TMemo
      Left = 9
      Top = 68
      Width = 45
      Height = 1
      Align = alClient
      DoubleBuffered = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 12
      Font.Name = 'Verdana'
      Font.Style = []
      ParentDoubleBuffered = False
      ParentFont = False
      TabOrder = 2
      WordWrap = False
    end
    object ScrollBar1: TScrollBar
      Left = 54
      Top = 68
      Width = 17
      Height = 1
      Align = alRight
      Kind = sbVertical
      PageSize = 0
      TabOrder = 3
      OnChange = ScrollBar1Change
    end
  end
  object UpdateLogTimer: TTimer
    Interval = 200
    OnTimer = UpdateLogTimerTimer
    Left = 280
    Top = 192
  end
end
