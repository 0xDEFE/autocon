//---------------------------------------------------------------------------

#ifndef MainMenuUnitH
#define MainMenuUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>

#include "DebugStateUnit.h"
#include "AutoconMain.h"
#include "IPCameraUnit.h"
#include <Vcl.Dialogs.hpp>
#include <Vcl.Buttons.hpp>;
#include "ConfigUnit.h";
#include "ScreenMessageUnit2.h";
#include "HeaderTestUnit.h"
#include "ArchiveDefinitions.h"
#include "LogUnit.h"

//---------------------------------------------------------------------------
class TMainMenuForm : public TForm
{
__published:	// IDE-managed Components
    TPanel *LeftMainPanel;
    TGroupBox *GroupBox2;
    TPanel *Panel3;
    TPanel *Panel7;
	TButton *ExitButton;
    TTimer *Timer1;
    TMemo *Memo1;
    TPanel *Panel1;
	TTimer *OpenConnectionTimer;
	TPanel *Panel5;
	TPanel *Panel4;
	TPanel *Panel6;
	TPanel *Panel2;
	TTimer *ChangeModeTimer;
    TOpenDialog *OpenDialog;
    TButton *Button2;
    TPanel *Panel8;
    TPanel *InfoPanel;
    TButton *Button1;
    TComboBox *IPComboBox;
    TButton *ManualModeBtn;
    TButton *OpenConnectionButton;
    TGroupBox *DebugMenuPanel;
    TGridPanel *GridPanel6;
    TPanel *Panel9;
    TPanel *Panel12;
    TLabel *Label6;
    TLabel *Label5;
    TLabel *Label2;
    TLabel *m_BUM1NameLabel;
    TLabel *m_BUM2NameLabel;
    TLabel *m_BUM3NameLabel;
    TLabel *m_JointsCheckedLabel;
    TLabel *m_WorkTimeLabel;
    TLabel *m_SoftwareVersionLabel;
    TLabel *m_BUM1StatusLabel;
    TLabel *m_BUM2StatusLabel;
    TLabel *m_BUM3StatusLabel;
    TBitBtn *SearchButton;
    TBitBtn *ArchButton;
    TBitBtn *CalibButton;
    TBitBtn *AlignButton;
    TBitBtn *Button8;
    TBitBtn *Button6;
    TBitBtn *Button5;
    TBitBtn *TestButton;
    TButton *ShowHideDebugMenus;
    TButton *TuneTableBtn;
    TGridPanel *GridPanel7;
    TGridPanel *GridPanel8;
    TGridPanel *GridPanel9;
    TPanel *Panel10;
    TGridPanel *GridPanel10;
    TPanel *Panel11;
    TGroupBox *GroupBox1;
    TGroupBox *GroupBox3;
    TGroupBox *GroupBox4;
    TLabel *Label1;
    TLabel *m_TestSO3PLabel;
    TLabel *Label4;
    TLabel *m_TestSOPLabel;
    TButton *OnButton;
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall ExitButtonClick(TObject *Sender);
    void __fastcall Timer1Timer(TObject *Sender);
    void __fastcall OpenConnectionButtonClick(TObject *Sender);
    void __fastcall TestButtonClick(TObject *Sender);
    void __fastcall Button8Click(TObject *Sender);
    void __fastcall SearchButtonClick(TObject *Sender);
    void __fastcall SearchButtonClick2(TObject *Sender);
    void __fastcall AlignButtonClick(TObject *Sender);
    void __fastcall CalibButtonClick(TObject *Sender);
	void __fastcall OpenConnectionTimerTimer(TObject *Sender);
	void __fastcall ChangeModeTimerTimer(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall Button5Click(TObject *Sender);
    void __fastcall Button6Click(TObject *Sender);
	void __fastcall OnManualModeBtnClicked(TObject *Sender);
    void __fastcall ArchButtonClick(TObject *Sender);
    void __fastcall Button2Click(TObject *Sender);
    void __fastcall ShowHideDebugMenusClick(TObject *Sender);
    void __fastcall TuneTableBtnClick(TObject *Sender);
    void __fastcall OnButtonClick(TObject *Sender);
    void __fastcall Panel5Click(TObject *Sender);
private:	// User declarations
	bool ChangeModeFlag;
    bool bDebugMenusAreHidden;
    void UpdateInfo();
public:		// User declarations
    __fastcall TMainMenuForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainMenuForm *MainMenuForm;
//---------------------------------------------------------------------------

extern cAutoconMain *AutoconMain;
extern cACConfig * ACConfig;
extern TStringList* TextLog;

#endif
