﻿//---------------------------------------------------------------------------

#pragma hdrstop

#include "ArchiveDefinitions.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


UnicodeString ADBFieldTypeToName(eADBFieldType fieldType)
{
	switch(fieldType)
	{
		case ADB_FIELDTYPE_ID:					return L"№";
		case ADB_FIELDTYPE_FILENAME:			return L"Имя файла";
		case ADB_FIELDTYPE_FILETYPE_IDX:		return L"Индекс типа файла";
		case ADB_FIELDTYPE_FILETYPE_NAME:		return L"Тип протокола";
		case ADB_FIELDTYPE_DATETIME:			return L"Дата/Время";
		case ADB_FIELDTYPE_GANG_NUMBER:			return L"Номер смены";
		case ADB_FIELDTYPE_REPORT_NUMBER:		return L"Номер протокола контроля";
		case ADB_FIELDTYPE_PLET_NUMBER:			return L"Номер плети";
		case ADB_FIELDTYPE_JOINT_NUMBER:		return L"Номер стыка";
		case ADB_FIELDTYPE_TOP_STRAIGHTNESS:	return L"Прямолинейность сверху";
		case ADB_FIELDTYPE_SIDE_STRAIGHTNESS:	return L"Прямолинейность сбоку";
		case ADB_FIELDTYPE_HARDNESS:			return L"Твердость";
		case ADB_FIELDTYPE_HARDNESS_UNIT:		return L"Ед. изм. твердости";
		case ADB_FIELDTYPE_CENTR_SYS_COORD:		return L"Центральная координата";
		case ADB_FIELDTYPE_MAX_SYS_COORD:		return L"Максимальная координата";
		case ADB_FIELDTYPE_MAX_CHANNEL:			return L"Максимальный канал";
		case ADB_FIELDTYPE_PLAINT:				return L"Предприятие";
		case ADB_FIELDTYPE_PLANT_NUMBER:		return L"Заводской номер установки";
		case ADB_FIELDTYPE_VER_SOFT:			return L"Версия ПО";
		case ADB_FIELDTYPE_OPERATOR:		   	return L"Оператор ФИО";
		case ADB_FIELDTYPE_DEFECT_CODE:			return L"Код дефекта";
		case ADB_FIELDTYPE_VALIDITY_GROUP:		return L"Группа годности";
		case ADB_FIELDTYPE_CONCLUSION:			return L"Заключение";
        case ADB_FIELDTYPE_HAS_BSCAN:           return L"+BSC";
        case ADB_FIELDTYPE_HAS_HSCAN_IMAGES:    return L"+HSI";
        case ADB_FIELDTYPE_HAS_HSCAN_BSCAN:     return L"+HSB";
        case ADB_FIELDTYPE_HAS_LINEARITY:       return L"+LIN";
        case ADB_FIELDTYPE_HAS_CAMERA_IMAGES:   return L"+CAM";
		case ADB_FIELDTYPE_END:					return L"UNKNOWN";
	}
	return "UNKNOWN";
}

UnicodeString ADBFieldTypeToDBFieldName(eADBFieldType fieldType)
{
	switch(fieldType)
	{
		case ADB_FIELDTYPE_ID:					return L"ID";
		case ADB_FIELDTYPE_FILENAME:			return L"FileName";
		case ADB_FIELDTYPE_FILETYPE_IDX: 		return L"FileType_idx";
		case ADB_FIELDTYPE_FILETYPE_NAME:		return L"FileType_name";
		case ADB_FIELDTYPE_DATETIME:			return L"ReportDateTime";
		case ADB_FIELDTYPE_GANG_NUMBER:			return L"GangNumber";
		case ADB_FIELDTYPE_REPORT_NUMBER:		return L"ReportNumber";
		case ADB_FIELDTYPE_PLET_NUMBER:			return L"PletNumber";
		case ADB_FIELDTYPE_JOINT_NUMBER:		return L"JointNumber";
		case ADB_FIELDTYPE_TOP_STRAIGHTNESS:	return L"TopStraightness";
		case ADB_FIELDTYPE_SIDE_STRAIGHTNESS:	return L"SideStraightness";
		case ADB_FIELDTYPE_HARDNESS:			return L"Hardness";
		case ADB_FIELDTYPE_HARDNESS_UNIT:		return L"HardnessUnit";
		case ADB_FIELDTYPE_CENTR_SYS_COORD:		return L"CentrSysCoord";
		case ADB_FIELDTYPE_MAX_SYS_COORD:		return L"MaxSysCoord";
		case ADB_FIELDTYPE_MAX_CHANNEL:			return L"MaxChannel";
		case ADB_FIELDTYPE_PLAINT:				return L"Plaint";
		case ADB_FIELDTYPE_PLANT_NUMBER:		return L"PlantNumber";
		case ADB_FIELDTYPE_VER_SOFT:			return L"VerSoft";
		case ADB_FIELDTYPE_OPERATOR:		   	return L"Operator";
		case ADB_FIELDTYPE_DEFECT_CODE:			return L"DefectCode";
		case ADB_FIELDTYPE_VALIDITY_GROUP:		return L"ValidityGroup";
		case ADB_FIELDTYPE_CONCLUSION:			return L"Conclusion";
        case ADB_FIELDTYPE_HAS_BSCAN:           return L"HasBScan";
        case ADB_FIELDTYPE_HAS_HSCAN_IMAGES:    return L"HasHScanImages";
        case ADB_FIELDTYPE_HAS_HSCAN_BSCAN:     return L"HasHScanBScan";
        case ADB_FIELDTYPE_HAS_LINEARITY:       return L"HasLinearity";
        case ADB_FIELDTYPE_HAS_CAMERA_IMAGES:   return L"HasCameraImages";
	}
	return "UNKNOWN";
}

UnicodeString ADBFieldValTypeToDBFieldVal(eADBFieldType fieldType, UnicodeString& val)
{
	switch(fieldType)
	{
		//INTEGER:
		case ADB_FIELDTYPE_ID:
		case ADB_FIELDTYPE_FILETYPE_IDX:
		case ADB_FIELDTYPE_GANG_NUMBER:
		case ADB_FIELDTYPE_REPORT_NUMBER:
		case ADB_FIELDTYPE_PLET_NUMBER:
		case ADB_FIELDTYPE_JOINT_NUMBER:
		case ADB_FIELDTYPE_TOP_STRAIGHTNESS:
		case ADB_FIELDTYPE_SIDE_STRAIGHTNESS:
		case ADB_FIELDTYPE_HARDNESS:
		case ADB_FIELDTYPE_HARDNESS_UNIT:
		case ADB_FIELDTYPE_CENTR_SYS_COORD:
		case ADB_FIELDTYPE_MAX_SYS_COORD:
		case ADB_FIELDTYPE_MAX_CHANNEL:
		    return val;

		//TEXT:
        case ADB_FIELDTYPE_FILETYPE_NAME:
		case ADB_FIELDTYPE_FILENAME:
		case ADB_FIELDTYPE_PLAINT:
		case ADB_FIELDTYPE_PLANT_NUMBER:
		case ADB_FIELDTYPE_VER_SOFT:
		case ADB_FIELDTYPE_OPERATOR:
		case ADB_FIELDTYPE_DEFECT_CODE:
		case ADB_FIELDTYPE_VALIDITY_GROUP:
		case ADB_FIELDTYPE_CONCLUSION:
        case ADB_FIELDTYPE_HAS_BSCAN:
        case ADB_FIELDTYPE_HAS_HSCAN_IMAGES:
        case ADB_FIELDTYPE_HAS_HSCAN_BSCAN:
        case ADB_FIELDTYPE_HAS_LINEARITY:
        case ADB_FIELDTYPE_HAS_CAMERA_IMAGES:
    		return UnicodeString("\"") + val + "\"";

        //DATETIME:
		case ADB_FIELDTYPE_DATETIME:
		    return UnicodeString("#") + val + "#";
	}
	return val;
}

DWORD GetFieldAssigns(eADBFieldType fieldType)
{
	DWORD assign = 0;

	//Отображение в таблице
	//Сортировка
	//Поиск
	switch(fieldType)
	{
		case ADB_FIELDTYPE_FILENAME:
		case ADB_FIELDTYPE_DATETIME:
		case ADB_FIELDTYPE_GANG_NUMBER:
		case ADB_FIELDTYPE_REPORT_NUMBER:
		case ADB_FIELDTYPE_PLET_NUMBER:
		case ADB_FIELDTYPE_JOINT_NUMBER:
		case ADB_FIELDTYPE_OPERATOR:
		case ADB_FIELDTYPE_DEFECT_CODE:
		case ADB_FIELDTYPE_VALIDITY_GROUP:
		case ADB_FIELDTYPE_CONCLUSION:
		case ADB_FIELDTYPE_FILETYPE_NAME:

        case ADB_FIELDTYPE_HAS_BSCAN:
        case ADB_FIELDTYPE_HAS_HSCAN_IMAGES:
        case ADB_FIELDTYPE_HAS_HSCAN_BSCAN:
        case ADB_FIELDTYPE_HAS_LINEARITY:
        case ADB_FIELDTYPE_HAS_CAMERA_IMAGES:
			assign |= ADB_FIELDASSIGN_TABLE;
			assign |= ADB_FIELDASSIGN_SORT;
			assign |= ADB_FIELDASSIGN_SEARCH;

		break;

  		//case ADB_FIELDTYPE_FILETYPE_IDX:
        //break;
	}

	//В сортировке и поиске используются одинаковые поля
	assign |= ADB_FIELDASSIGN_TEST;
	assign |= ADB_FIELDASSIGN_CONTROL;

	return assign;
}

//-------------------------------------------------------------------------------

UnicodeString AFDOpTypeToDBOp(eAFDOpType afdOp)
{
	switch(afdOp)
	{
		case AFD_OPTYPE_EQUAL:			return "=";
		case AFD_OPTYPE_NOTEQUAL:		return "<>";
		case AFD_OPTYPE_LESS:			return "<";
		case AFD_OPTYPE_GREAT:			return ">";
		case AFD_OPTYPE_LESS_EQUAL:		return "<=";
		case AFD_OPTYPE_GREAT_EQUAL:	return ">=";
	}
	return "";
}

UnicodeString AFDOpTypeToName(eAFDOpType afdOp)
{
	switch(afdOp)
	{
		case AFD_OPTYPE_EQUAL:			return L"=";
		case AFD_OPTYPE_NOTEQUAL:		return L"≠";
		case AFD_OPTYPE_LESS:			return L"<";
		case AFD_OPTYPE_GREAT:			return L">";
		case AFD_OPTYPE_LESS_EQUAL:		return L"≤";
		case AFD_OPTYPE_GREAT_EQUAL:	return L"≥";
	}
	return L"";
}

UnicodeString ADBFileTypeIdxToFileTypeName(int idx)
{
    switch(idx)
    {
        case 1: return L"Протокол контроля";
        case 2: return L"Протокол тестирования";
        case 3: return L"Протокол юстировки";
        case 4: return L"Протокол ручных ПЭП";
    }
    return L"Не известно";
}

int ADBFileTypeNameToFileTypeIdx(UnicodeString name)
{
    for(int i = 1; i <= 4; i++)
    {
        if( name == ADBFileTypeIdxToFileTypeName(i))
            return i;
    }
    return 0;
}
