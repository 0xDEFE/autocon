//---------------------------------------------------------------------------

#ifndef DrawRailUnitH
#define DrawRailUnitH

#include <Vcl.Graphics.hpp>
#include <System.Types.hpp>

#include "Utils.h"

class cDrawRailUnit
{
public:
	cDrawRailUnit();
	virtual ~cDrawRailUnit();

	void loadRailImages();
	void resize(unsigned int width, unsigned int height);
	void update(bool bForceUpdate = false);
	TBitmap* getBuffer();
	TPoint getKPPoint(unsigned int kp_index);
	void calculateOptimalHardpoint(unsigned int kp_index, TPoint RailOffset, TPoint RectPoint, TPoint RectNormal, bool bNoCorners = true);
	TPoint getOptimalHardpoint(unsigned int kp_index);
	TPoint getOptimalMidpoint(unsigned int kp_index, int ptIndex);
	TPoint getBScanHardpoint(unsigned int kp_index);
	TColor getHardpointColor(unsigned int kp_index);

	void draw(TBitmap* pDrawBuffer, bool bDrawHardpoints = true, DWORD UnvisibleFlags = 0);
	void scaleToBuffer(TBitmap* pDrawBuffer, bool bDrawHardpoints = true, DWORD UnvisibleFlags = 0);


	//[RAIL_HGL_KP1 - RAIL_HGL_KP8] - Supports all colors
	//[RAIL_HGL_S1 - RAIL_HGL_S5] - Supports only clRed, clGreen, clWhite or clGrey

    //Sets color of KP (RAIL_HGL_KP#), RailSegment (RAIL_HGL_S#)
    //  or KP surface (RAIL_HGL_KP1_SURF#) by mask
	void setColor(unsigned int mask, unsigned int color);
	void setColorByIndex(unsigned int index, unsigned int color);
	unsigned int getColorByIndex( unsigned int index);
	int TestPoint(TPoint pt);
	void setOrientation( bool bLeftToRight );

public:

	/*        |KP1|
		---	|------| ---
		KP2	|  S1  | KP3
		---	|------| ---
		---	  |  |   ---
		KP4	  |S2|   KP5
		---	  |  |   ---
		  ------------
		  |S3 |S4| S5|
		  ------------
		 |KP6| KP8 |KP7|
	*/



	//Highlight masks

	const static int RAIL_HGL_KP1 = 0x01;
	const static int RAIL_HGL_KP2 = 0x02;
	const static int RAIL_HGL_KP3 = 0x04;
	const static int RAIL_HGL_KP4 = 0x08;
	const static int RAIL_HGL_KP5 = 0x10;
	const static int RAIL_HGL_KP6 = 0x20;
	const static int RAIL_HGL_KP7 = 0x40;
	const static int RAIL_HGL_KP8 = 0x80;

	const static int RAIL_HGL_S1 = 0x100;
	const static int RAIL_HGL_S2 = 0x200;
	const static int RAIL_HGL_S3 = 0x400;
	const static int RAIL_HGL_S4 = 0x800;
	const static int RAIL_HGL_S5 = 0x1000;

	const static int RAIL_HGL_KP1_SURF = 0x2000;
	const static int RAIL_HGL_KP2_SURF = 0x4000;
	const static int RAIL_HGL_KP3_SURF = 0x8000;
	const static int RAIL_HGL_KP4_SURF = 0x10000;
	const static int RAIL_HGL_KP5_SURF = 0x20000;
	const static int RAIL_HGL_KP6_SURF = 0x40000;
	const static int RAIL_HGL_KP7_SURF = 0x80000;
	const static int RAIL_HGL_KP8_SURF = 0x100000;

    #define RAIL_HGL_KP(x)      (cDrawRailUnit::RAIL_HGL_KP1<<(x-1))
    #define RAIL_HGL_S(x)       (cDrawRailUnit::RAIL_HGL_S1<<(x-1))
    #define RAIL_HGL_KP_SURF(x) (cDrawRailUnit::RAIL_HGL_KP1_SURF<<(x-1))

	static const int KP_TO_RAIL[];

    bool bRailColorFromKPColor;
protected:
	void _updateKPSizes(unsigned int width, unsigned int height);
protected:
   TBitmap* Buffer;
   bool bNeedUpdateFlag;


   bool bOrientationLeftToRight;
   bool bNeedUpdateSize;

   int fontHeight;
   TRect KPRects[8];
   UnicodeString KPText[8][3];
   TRect SRects[5];
   TPoint OptimalHardpoints[8];
   TPoint OptimalMidpoints[8][2];
   TPoint BScanHardpoints[8];
   TPoint _RailOffset;
   TPoint lastCalculatedBufferSize;

   //  RailImages [ fragment ][ color (white, red, green) ]
   TPicture* RailImages[5][3];
   bool bImagesLoaded;

   unsigned int colors[32];

   const static TColor FILL_COLOR = clWhite;
};

//---------------------------------------------------------------------------
#endif
