﻿/**
 * @file DataStorage.h
 * @author Denis Fedorenko
 */


#include <stddef.h>
//----------------------------------------------------------------
#ifndef DataStorageH
#define DataStorageH

#include <jpeg.hpp>
#include "Definitions.h"
#include "System.ZLib.hpp"
#include "vector"
#include "LanProtUMU.h"
//Added by KirillB
#include "Utils.h"
#include "CriticalSection_Win.h"


struct sFilterParams
{
    bool State;          // Состояние - ВКЛ / ВЫКЛ
    int DeltaDelay;      // максимольно допустимое изменение задержки  [мкс]
    int SameDelayCount;  // Разрешенное количество идущих подряд сигналов с одинаковой задержкой
    int MaxSysCoordStep; // Максимально допустимый разрыв в пачке по координате
    int OKLength;        // Минимально допустимый размер пачки
};

const ScanChannelCount = 90;
const OneCrdOneChannelSignalsCount = 8;
const NewItemListSize = 5000;
const MasksMaxCount = 32; // Максимальное количество масок

//! Параметры канала контроля
struct sScanChannelParams
{
	int Sens;                 //!< Условная чувствительность [дБ]
	int Gain;                 //!< Аттенюатор [дБ]
	int StGate;               //!< Начало строб АСД [мкс]
	int EdGate;               //!< Конец строба АСД [мкс]
	int PrismDelay;           //!< Время в призме [10 * мкс]
	int TVG;                  //!< ВРЧ [мкс]
};

//! Заголовок протокола контроля сварного стыка (версия 1)
struct sJointTestingReportHeader_Version_01
{
						 // ------------- Протокол


    int FileType;               //!< Тип данных: 1 - контроль; 2 - тест
	TDateTime DateTime;     	//!< Дата / Время контроля
	int	GangNumber;				//!< Номер смены
	int	ReportNumber;	 	    //!< Номер протокола контроля
	int PletNumber;        	    //!< Номер плети
	int JointNumber;    	    //!< Номер стыка
	float TopStraightness; 		//!< Прямолинейность сверху [мм]
	float SideStraightness;		//!< Прямолинейность сбоку [мм]
	int	Hardness; 				//!< Твёрдость
	int	HardnessUnit; 			//!< Ед. изм. Твёрдости: 1 – HB, 2 - HV, 3 - HRC

						 // ------------- Параметры контроля

	int CentrSysCoord; 			//!< Центр, Положение стыка
	int MaxSysCoord;   			//!< Максимальная координата
	int MaxChannel;    			//!< Количество каналов сканирования

	sScanChannelParams ChannelParams[ScanChannelCount]; //!< Параметры каналов контроля

	int END_;    		 //!< Маркер окончания данных

						 // ------------- Текстовые поля

	UnicodeString Plaint;   	  //!< Предприятие
	UnicodeString PlantNumber;    //!< Заводской номер установки
	UnicodeString VerSoft;		  //!< Версия ПО
	UnicodeString Operator; 	  //!< Оператор ФИО
	UnicodeString DefectCode;     //!< Код дефекта
	UnicodeString ValidityGroup;  //!< Гр. Годности

};

enum HardnessPoint
{
    hpCentrePoint1 = 0,
    hpCentrePoint2 = 1,
    hpCentrePoint3 = 2,
    hpLeftPoint1 = 3,
    hpLeftPoint2 = 4,
    hpLeftPoint3 = 5,
    hpRightPoint1 = 6,
    hpRightPoint2 = 7,
    hpRightPoint3 = 8
};

//! Заголовок протокола контроля сварного стыка (версия 2)
struct sJointTestingReportHeader_Version_02
{
						 // ------------- Протокол

    int FileType;               //!< Тип данных: 1 - контроль; 2 - тест
	TDateTime DateTime;     	//!< Дата / Время контроля
	int	GangNumber;				//!< Номер смены
	int	ReportNumber;	 	    //!< Номер протокола контроля
	int PletNumber;        	    //!< Номер плети
	int JointNumber;    	    //!< Номер стыка
	float TopStraightness; 		//!< Прямолинейность сверху [мм]
	float SideStraightness;		//!< Прямолинейность сбоку [мм]
	int	HardnessState;          //!< Твёрдость: 0 - не измерялась, 1 - измерялась
	int	HardnessUnit_Old; 		//!< Ед. изм. Твёрдости: 1 – HB, 2 - HV, 3 - HRC

						 // ------------- Параметры контроля

	int CentrSysCoord; 			//!< Центр, Положение стыка
	int MaxSysCoord;   			//!< Максимальная координата
	int MaxChannel;    			//!< Количество каналов сканирования

	sScanChannelParams ChannelParams[ScanChannelCount]; //!< Параметры каналов контроля

    int	Hardness[9];            //!< Твёрдость

	int END_;    		 //!< Маркер окончания данных

						 // ------------- Текстовые поля

	UnicodeString Plaint;   	  //!< Предприятие
	UnicodeString PlantNumber;    //!< Заводской номер установки
	UnicodeString VerSoft;		  //!< Версия ПО
	UnicodeString Operator; 	  //!< Оператор ФИО
	UnicodeString DefectCode;     //!< Код дефекта
	UnicodeString ValidityGroup;  //!< Гр. Годности
	UnicodeString Сonclusion;     //!< Заключение

};

//! Заголовок протокола контроля сварного стыка (версия 3)
struct sJointTestingReportHeader_Version_03
{
						 // ------------- Протокол

    int FileType;               //!< Тип данных: 1 - контроль; 2 - тест; 3 - юстировка; 4 - контроль только ручик;
	TDateTime DateTime;     	//!< Дата / Время контроля
	int	GangNumber;				//!< Номер смены
	int	ReportNumber;	 	    //!< Номер протокола контроля
	int PletNumber;        	    //!< Номер плети
	int JointNumber;    	    //!< Номер стыка
	float TopStraightness; 		//!< Прямолинейность сверху [мм]
	float SideStraightness;		//!< Прямолинейность сбоку [мм]
	int	HardnessState;          //!< Твёрдость: 0 - не измерялась, 1 - измерялась
	int	HardnessUnit_Old; 		//!< Ед. изм. Твёрдости: 1 – HB, 2 - HV, 3 - HRC

						 // ------------- Параметры контроля

	int CentrSysCoord; 			//!< Центр, Положение стыка
	int MaxSysCoord;   			//!< Максимальная координата
	int MaxChannel;    			//!< Количество каналов сканирования

	sScanChannelParams ChannelParams[ScanChannelCount]; //!< Параметры каналов контроля

    int	Hardness[9];            //!< Твёрдость
    int ScanChannelCount;       //!< Количество каналов контроля (сканирования)

	int END_;    		        //!< Маркер окончания данных

						 // ------------- Текстовые поля

	UnicodeString Plaint;   	  //!< Предприятие
	UnicodeString PlantNumber;    //!< Заводской номер установки
	UnicodeString VerSoft;		  //!< Версия ПО
	UnicodeString Operator; 	  //!< Оператор ФИО
	UnicodeString DefectCode;     //!< Код дефекта
	UnicodeString ValidityGroup;  //!< Гр. Годности
	UnicodeString Сonclusion;     //!< Заключение

};

//! Заголовок протокола контроля сварного стыка (версия 4)
struct sJointTestingReportHeader_Version_04
{
						 // ------------- Протокол

    int FileType;               //!< Тип данных: 1 - контроль; 2 - тест; 3 - юстировка; 4 - контроль только ручик;
	TDateTime DateTime;     	//!< Дата / Время контроля
	int	GangNumber;				//!< Номер смены
	int	ReportNumber;	 	    //!< Номер протокола контроля
	int PletNumber;        	    //!< Номер плети
	int JointNumber;    	    //!< Номер стыка
	float TopStraightness; 		//!< Прямолинейность сверху [мм]
	float SideStraightness;		//!< Прямолинейность сбоку [мм]
	int	HardnessState;          //!< Твёрдость: 0 - не измерялась, 1 - измерялась
	int	HardnessUnit_Old; 		//!< Ед. изм. Твёрдости: 1 – HB, 2 - HV, 3 - HRC

						 // ------------- Параметры контроля

	int CentrSysCoord; 			//!< Центр, Положение стыка
	int MaxSysCoord;   			//!< Максимальная координата
	int MaxChannel;    			//!< Количество каналов сканирования

	sScanChannelParams ChannelParams[ScanChannelCount]; //!< Параметры каналов контроля

    int	Hardness[9];            //!< Твёрдость
    int NotUse;                 //!< Было - Количество каналов контроля (сканирования) ????
    float TimeInHandScanChannel[10]; //!< Время работы в ручном канале контроля

	int END_;    		        //!< Маркер окончания данных

						 // ------------- Текстовые поля

	UnicodeString Plaint;   	  //!< Предприятие
	UnicodeString PlantNumber;    //!< Заводской номер установки
	UnicodeString VerSoft;		  //!< Версия ПО
	UnicodeString Operator; 	  //!< Оператор ФИО
	UnicodeString DefectCode;     //!< Код дефекта
	UnicodeString ValidityGroup;  //!< Гр. Годности
	UnicodeString Сonclusion;     //!< Заключение

};

//! Заголовок протокола контроля сварного стыка (версия 5)
/** Добавились параметры каналов: КП1 0 ЭХО и КП8 0 ЭХО */
struct sJointTestingReportHeader_Version_05
{
						 // ------------- Протокол

    int FileType;               //!< Тип данных: 1 - контроль; 2 - тест; 3 - юстировка; 4 - контроль только ручик;
	TDateTime DateTime;     	//!< Дата / Время контроля
	int	GangNumber;				//!< Номер смены
	int	ReportNumber;	 	    //!< Номер протокола контроля
	int PletNumber;        	    //!< Номер плети
	int JointNumber;    	    //!< Номер стыка
	float TopStraightness; 		//!< Прямолинейность сверху [мм]
	float SideStraightness;		//!< Прямолинейность сбоку [мм]
	int	HardnessState;          //!< Твёрдость: 0 - не измерялась, 1 - измерялась
	int	HardnessUnit_Old; 		//!< Ед. изм. Твёрдости: 1 – HB, 2 - HV, 3 - HRC

						 // ------------- Параметры контроля

	int CentrSysCoord; 			//!< Центр, Положение стыка
	int MaxSysCoord;   			//!< Максимальная координата
	int MaxChannel;    			//!< Количество каналов сканирования

	sScanChannelParams ChannelParams[ScanChannelCount]; //!< Параметры каналов контроля
    sScanChannelParams ChannelParams_WP1N0EMS_0x46; //!< Параметры канала контроля - КП1 0 ЭХО
    sScanChannelParams ChannelParams_WP8N0EMS_0x90; //!< Параметры канала контроля - КП8 0 ЭХО

    int	Hardness[9];            //!< Твёрдость
    int NotUse;                 //!< Было - Количество каналов контроля (сканирования) ????
    float TimeInHandScanChannel[10]; //!< Время работы в ручном канале контроля

	int END_;    		        //!< Маркер окончания данных

						 // ------------- Текстовые поля

	UnicodeString Plaint;   	  //!< Предприятие
	UnicodeString PlantNumber;    //!< Заводской номер установки
	UnicodeString VerSoft;		  //!< Версия ПО
	UnicodeString Operator; 	  //!< Оператор ФИО
	UnicodeString DefectCode;     //!< Код дефекта
	UnicodeString ValidityGroup;  //!< Гр. Годности
	UnicodeString Сonclusion;     //!< Заключение

};

//! Заголовок протокола контроля сварного стыка (версия 6)
/** Добавилось: - Дата последнего измерения твердости;
 *             - Третье изображения с телнфона линейки;
 *             - Дата последней проверки на СОП и СО-3Р
 *             - Флаг последнее измерение твердости
 */
struct sJointTestingReportHeader_Version_06
{
						 // ------------- Протокол

    int FileType;               //!< Тип данных: 1 - контроль; 2 - тест; 3 - юстировка; 4 - контроль только ручик;
	TDateTime DateTime;     	//!< Дата / Время контроля
	int	GangNumber;				//!< Номер смены
	int	ReportNumber;	 	    //!< Номер протокола контроля
	int PletNumber;        	    //!< Номер плети
	int JointNumber;    	    //!< Номер стыка
	float TopStraightness_;     //!< Прямолинейность сверху [мм] //FDV
	float WF_Straightness_;		//!< Прямолинейность сбоку (РГ) [мм] //FDV
	int	HardnessState;          //!< Твёрдость: 0 - не измерялась, 1 - измерялась сейчас 2 - измерялась ранее
	float WNF_Straightness_; 		//!< Прямолинейность сбоку (НРГ) [мм] //FDV

						 // ------------- Параметры контроля

	int CentrSysCoord; 			//!< Центр, Положение стыка
	int MaxSysCoord;   			//!< Максимальная координата
	int MaxChannel;    			//!< Количество каналов сканирования

	sScanChannelParams ChannelParams[ScanChannelCount]; //!< Параметры каналов контроля
    sScanChannelParams ChannelParams_WP1N0EMS_0x46; //!< Параметры канала контроля - КП1 0 ЭХО
    sScanChannelParams ChannelParams_WP8N0EMS_0x90; //!< Параметры канала контроля - КП8 0 ЭХО

    int	Hardness[9];            //!< Твёрдость
    int NotUse;                 //!< Было - Количество каналов контроля (сканирования) ????
    float TimeInHandScanChannel[10]; //!< Время работы в ручном канале контроля

    TDateTime HardnessLastMeasureDateTime; //!< Дата последнего измерения твердости;
    TDateTime LastSSCTestDateTime; //!< Дата последней проверки на СОП
    TDateTime LastSS3RTestDateTime; //!< Дата последней проверки на СО-3Р

	int END_;    		        //!< Маркер окончания данных

						 // ------------- Текстовые поля

	UnicodeString Plaint;   	  //!< Предприятие
	UnicodeString PlantNumber;    //!< Заводской номер установки
	UnicodeString VerSoft;		  //!< Версия ПО
	UnicodeString Operator; 	  //!< Оператор ФИО
	UnicodeString DefectCode;     //!< Код дефекта
	UnicodeString ValidityGroup;  //!< Гр. Годности
	UnicodeString Сonclusion;     //!< Заключение

};

//! Заголовок протокола тестирования работоспособности установки
struct sSystemCheckReportHeader
{
	UnicodeString Operator; //!< Оператор ФИО
	TDateTime DateTime;     //!< Дата Время контроля
	// .....
};

//! Сигналы одного канала зафиксированные на одной координате при сканировании
struct sScanSignalsOneCrdOneChannel
{
	char Count;                                              //!< Количество сигналов
	tUMU_BScanSignal Signals[OneCrdOneChannelSignalsCount];  //!< Сигналы
    unsigned short WorkCycle;   //!< Рабочий цикл, в котором были зафиксированы сигналы
    bool bIsAlarmed;            //!< Были ли сигналы АСД на данном канале
};

struct sScanSignalsOneCrdAllChannel
{
	sScanSignalsOneCrdOneChannel Channel[ScanChannelCount];
};

typedef sScanSignalsOneCrdAllChannel* PsScanSignalsOneCrdAllChannel;

typedef sScanSignalsOneCrdOneChannel* PsScanSignalsOneCrdOneChannel;

//! Сигналы одного канала зафиксированные на одной координате при ручном контроле
struct sHandSignalsOneCrd
{
	int SysCoord;           //!< Координата
	char Count;             //!< Количество сигналов
	tUMU_BScanSignal Signals[OneCrdOneChannelSignalsCount];  //!< Сигналы
};

typedef sHandSignalsOneCrd* PsHandSignalsOneCrd;

//! Заголовок данных ручного контроля
struct sHandScanParams
{
	int EnterAngle;           //!< Угол ввода
	eInspectionMethod Method; //!< Метод контроля
	int ScanSurface;          //!< Контролируемая поверхность
	int Ku;                   //!< Условная чувствительность [дБ]
	int Att;                  //!< Аттенюатор [дБ]
	int StGate;               //!< Начало строб АСД [мкс]
	int EdGate;               //!< Конец строба АСД [мкс]
	int PrismDelay;           //!< Время в призме [10 * мкс]
	int TVG;                  //!< ВРЧ [мкс]
};

//! Заголовок данных сигналов с ручника
struct sHandSignalsRecord
{
	sHandScanParams Params;
	std::vector < sHandSignalsOneCrd > BScanData;
};

//! Заголовок данных для АСД
struct sAlarmRecord
{
	sAlarmRecord()
	{
		Side = dsNone;
		Channel = 0;
        memset(State, false, sizeof( State ));
        memset(LastState, false, sizeof( LastState ));
	};

	eDeviceSide Side; //!< Сторона дефектоскопа
	CID Channel;      //!< Канал
	bool State[4];    //!< [Номер строба]
    bool LastState[4];//!< Последнее состояние
};

////////////////////////////////////////////////////////////////////////////////

struct sMaskSignalItem
{
    int SysCoord; // Координата
	tUMU_BScanSignal Signal; // Сигнал
};

struct sMaskItem
{
	int StartSysCoord; // Координата
	int LastSysCoord; // Координата
	unsigned char LastDelay; // Задержки максимумов

	sMaskSignalItem Signals_[32]; // Сигналы
	int Count; // Количество сигналов в Signals
	int SameDelay; // Разрешенное количество идущих подряд сигналов с одинаковой задержкой
	bool Busy; // Ячейка занята
	bool OK; // Идет прямое сохранение сигналов
};

////////////////////////////////////////////////////////////////////////////////

// Файл

// # .pragma pack (1)
#pragma pack(push,1)

//! Заголовок бинароного файла
struct sFileHeaderVersion_02
{
	char FileID[16];          //!< Идентификатор
	int Version;              //!< Версия формата файла

	int HeaderDataOffset;     //!< Заголовок файла
	int HeaderDataSize;
	int ScanDataOffset;       //!< Данные сканирования
    int ScanDataSize;
	int HandDataOffset[32];   //!< Данные ручного сканирования
	int HandDataSize;
	int HandDataItemsCount;

	int FotoDataOffset;       //!< Фото стыка
	int FotoDataSize;
};
//! Системный заголовок бинароного файла - версия 3
struct sFileHeaderVersion_03
{
	char FileID[16];          //!< Идентификатор
	int Version;              //!< Версия формата файла

	int HeaderDataOffset;     //!< Заголовок файла
	int HeaderDataSize;
	int ScanDataOffset;       //!< Данные сканирования
    int ScanDataSize;
	int HandDataOffset[32];   //!< Данные ручного сканирования
	int HandDataSize;
	int HandDataItemsCount;

	int FotoDataOffset;       //!< Фото стыка
	int FotoDataSize;

	int ScreenShotDataOffset;         //!< Скриншот экрана телефона линейки
	int ScreenShotDataSize;

	int AlarmRecordsDataOffset;       //!< Данные АСД для каждого канала
	int AlarmRecordsDataSize;
    int AlarmRecordsItemsCount;

	int HandScanScreenShotOffset[32]; //!< Скриншоты экрана ручного сканирования
	int HandScanScreenShotSize[32];
	int HandScanScreenShotCount;
};

//! Системный заголовок бинароного файла - версия 4
struct sFileHeaderVersion_04
{
	char FileID[16];          //!< Идентификатор
	int Version;              //!< Версия формата файла

	int HeaderDataOffset;     //!< Заголовок файла
	int HeaderDataSize;
	int ScanDataOffset;       //!< Данные сканирования
    int ScanDataSize;
	int HandDataOffset[32];   //!< Данные ручного сканирования
	int HandDataSize;
	int HandDataItemsCount;

	int FotoDataOffset;       //!< Фото стыка
	int FotoDataSize;

	int ScreenShotDataOffset;         //!< Скриншот №1 экрана телефона линейки
	int ScreenShotDataSize;

	int AlarmRecordsDataOffset;       //!< Данные АСД для каждого канала
	int AlarmRecordsDataSize;
    int AlarmRecordsItemsCount;

	int HandScanScreenShotOffset[32]; //!< Скриншоты экрана ручного сканирования
	int HandScanScreenShotSize[32];
	int HandScanScreenShotCount;

	int ScreenShot2_DataOffset;         //!< Скриншот №2 экрана телефона линейки
	int ScreenShot2_DataSize;

	int RailBottomFotoDataOffset;       //!< Фото подошвы
	int RailBottomFotoDataSize;

};

//! Системный заголовок бинароного файла - версия 4
struct sFileHeaderVersion_05
{
	char FileID[16];          //!< Идентификатор
	int Version;              //!< Версия формата файла

	int HeaderDataOffset;     //!< Заголовок файла
	int HeaderDataSize;
	int ScanDataOffset;       //!< Данные сканирования
    int ScanDataSize;
	int HandDataOffset[32];   //!< Данные ручного сканирования
	int HandDataSize;
	int HandDataItemsCount;

	int FotoDataOffset;       //!< Фото стыка
	int FotoDataSize;

	int ScreenShotDataOffset;         //!< Скриншот №1 экрана телефона линейки
	int ScreenShotDataSize;

	int AlarmRecordsDataOffset;       //!< Данные АСД для каждого канала
	int AlarmRecordsDataSize;
    int AlarmRecordsItemsCount;

	int HandScanScreenShotOffset[32]; //!< Скриншоты экрана ручного сканирования
	int HandScanScreenShotSize[32];
	int HandScanScreenShotCount;

	int ScreenShot2_DataOffset;         //!< Скриншот №2 экрана телефона линейки
	int ScreenShot2_DataSize;

	int RailBottomFotoDataOffset;       //!< Фото подошвы
	int RailBottomFotoDataSize;

	int ScreenShot3_DataOffset;         //!< Скриншот №3 экрана телефона линейки
	int ScreenShot3_DataSize;
};

#pragma pack(pop)

//# pragma pack ()

typedef struct { int Crd; int Ch; } TNewItemStruct;

//! Вспомогадельные данные для каналов контроля
struct sChannelControlData
{
    sChannelControlData()
    {
        id = 0;
        currentMaximum = -1;
        isSurfaceControlChannel = false;
        bHasAlarmDefect = false;
        bHasLossContactDefect = false;
        lossContactPos = 0;
		controlState = -1;
        collectIntervalMin = -1;
        collectIntervalMax = -1;
        BScanMult = 1;
    };

    CID id;                 //!< Идентификатор канала
    int currentMaximum;     //!< Текущий максимум в канале
    int collectIntervalMin; //!< Интервал сбора максимумов в канале (начало)
    int collectIntervalMax; //!< Интервал сбора максимумов в канале (конец)

    bool isSurfaceControlChannel; //!< Признак канала контроля контакта с поверхностью рельса
    bool bHasAlarmDefect;         //!< Признак наличия в канале дефекта (по АСД)
    bool bHasLossContactDefect;   //!< Признак наличия в канале дефекта (по вычислению разрывов в сигнале)
    int lossContactPos;           //!< Позиция разрыва
    int BScanMult;                //!< Множитель для в-развертки (для вычисления реальной задержки)

    int channelStrobes[3][2]; //!< [bscan, near(асд), far (донный?)][start, end]

	std::vector < sAlarmRecord > AlarmsInChannel; //!< Сигналы АСД в канале
	int controlState; //!< 0 - ok, 1 - warning, 2 - error, -1 - unknown
};

class cChannelsTable;
class cDeviceCalibration;
//! Данные протокола контроля сварного стыка
class cJointTestingReport
{
	private:
		int FSearchSysCoord;
		int FSearchChannel;


		std::vector < int > FCoordPtrList; //!< Данные сканирования
		std::vector < sScanSignalsOneCrdAllChannel* > FSignalList; //!< Данные сканирования


		std::vector < sAlarmRecord > FAlarmList; //!< Сохранение сигналов АСД
		bool bAlarmsUpdated; //!< устанавливается при изменении значений

		//------------Added by KirillB---------------------
		//! Максимумы для настройки
		std::vector <int> FTuneMaximumsList;

		std::vector < sChannelControlData > FChanCtrlData; //!< Данные для индикации канала при контроле и настройке
        bool bHandScanStoreEnabled;
        bool bEnableSounds;
        DWORD prevSoundTime;
		//-------------------------------------------------

		// Данные ручника
		std::vector < sHandSignalsRecord > FHandScanList;
        sHandSignalsRecord storedHandSignals;

		// Данные фильтрации сигналов В-развертки
        bool FFiltrState_; // Состояние фильтра ВКЛ / ВЫКЛ
    	sMaskItem Masks[ScanChannelCount][MasksMaxCount]; // Масив масок поиска
		int MasksCount[ScanChannelCount]; // Количество активных масок
		bool FromFile;

	public:
        sFilterParams fp;
        bool FExitFlag;
		UnicodeString ReportFileName;

		sJointTestingReportHeader_Version_06 Header; //!< Текущий заголовок данных отчета

		cJointTestingReport(int MaxSysCoord);
		~cJointTestingReport();

		TJPEGImage * Photo;               //!< Фото стыка
		TJPEGImage * PhoneScreenShot;     //!< Скриншот №1 экрана телефона линейки
		TJPEGImage * PhoneScreenShot2;    //!< Скриншот №2 экрана телефона линейки
		TJPEGImage * PhoneScreenShot3;    //!< Скриншот №3 экрана телефона линейки
		TJPEGImage * RailBottomPhoto;     //!< Фото подошвы рельса

		std::vector < TJPEGImage* > ScreenShotHandScanList; //!< Скриншоты экрана ручного сканирования

			// Сигналы сканирования
		void ClearData(void); //!< Вызывает функцию Release
        void Release();       //!< Удаляет все данные из отчета

		//-------------Added by KirillB-------------------
        void ClearScreenShotHandScanList(); //!< Удаляет все изображения с ручников

        //! Получение данных контроля для канала
        /** \param idx Индекс в FChanCtrlData \returns Выбранный канал */
        sChannelControlData* GetChannelCtrlData(unsigned int idx);
        unsigned int GetChannelCtrlDataCount(); //!< Получение кол-ва каналов в FChanCtrlData
        bool CheckForLossContactDefects(cDeviceCalibration* Calib,int workCycle); //!< Проверка на разрывы
        void ClearChannelMaximumsData();    //!< Очистка данных максимумов для всех каналов
        int GetSurfaceChannelIdByKP(unsigned int kp_index) const; //!< Получение канала контроля поверхности по индексу КП
        void CleanupChannelCtrlData();
        int GetChannelMaximum(unsigned int index);
        //! Установка интервала сбора максимумов для канала
        /** \param idx Индекс канала \param int1 Начало интервала \param int2 Конец интервала */
        void SetChannelCollectInterval(unsigned int idx, int int1, int int2);

        //! Обновление состояния канала (есть дефект/нет дефекта) для режима Поиск
        void UpdateCtrlStateForSearchMode();
        //! Обновление состояния канала (настроен/не настроен) для режима Настройка
        void UpdateCtrlStateForTuneMode(cChannelsTable* Tbl);
        //! Разрешить звуковой сигнал по приходу АСД
        void EnableAsdSounds(bool bVal);
        //! Обновить данные каналов
		void UpdateChannelIndicationData();
		//-----------------------------------------------

        //! Добавить новую запись АСД
		void AddAlarmSignals(int Channel, sAlarmRecord& item, bool bHandScan = false);
		sAlarmRecord GetAlarmSignals(int Channel);
		bool CheckAlarmUpdated(bool bResetUpdateToFalse = false); //!< Проверить, были ли изменения
		int GetAlarmSignalsCount();
        void ClearAlarmSignals();


		void PutScanSignals(int SysCoord, int Channel, PsScanSignalsOneCrdOneChannel pData);
        PsScanSignalsOneCrdOneChannel GetScanSignals(int SysCoord, int Channel);
		PsScanSignalsOneCrdOneChannel GetFirstScanSignals(int SysCoord, int Channel, bool* Result);
		PsScanSignalsOneCrdOneChannel GetNextScanSignals(int* SysCoord, int* Channel, bool* Result);

        //! Разрешить сохранение сигналов с ручников во временный массив
        void EnableHandScanStore(bool bVal);
        //! Очистить сохраненные сигналы с ручников из временного массива
        void ClearStoredHandSignals();
        //! Сохранить сигнал с ручников во временный массив
        void StoreScanSignal(sHandSignalsOneCrd& signal);
        //! Сохранить сигнал с ручников из временного массива в отчет
        int AddStoredHandSignals(sHandScanParams Params);

        //! Добавляет сигналы ручного контроля
		/** \param CrdCount количество записей sHandOneCrdSignals
            \param pData указатель на первую запись
            \return Индекс сигнала в массиве FHandScanList
          */
		int AddHandSignals(sHandScanParams Params, PsHandSignalsOneCrd pData, int CrdCount);
        //! Получение данных ручного контроля
		PsHandSignalsOneCrd GetHandSignals(int RecIndex, sHandScanParams* Params, bool* Result, int * CrdCount);
        //! Получение количества имеющихся записей:
		int  GetHandScanCount(void) const;
        //! Удаление записи
		bool DeleteHandScan(int Index);

        //! Запись отчета в файл
		bool WriteToFile(UnicodeString filename);
        //! Загрузка отчета из файла
		bool ReadFromFile(UnicodeString filename, bool OnlyHeader, sFileHeaderVersion_05* pRawHeader = NULL);

		void ClearNewItemList(int Idx);
        float GetTimeInHandScanChannel(CID cid);
        float GetTimeInHandScanChannelByAngle(int angle,eInspectionMethod meth);
        void SetTimeInHandScanChannel(CID cid, float NewValue);

        TNewItemStruct NewItemList[NewItemListSize]; //!< Массив недавно пришедших сигналов (для того, чтобы не перерисовывать все)
        int NewItemListIdx; //!< Текущий индекс последнего сигнала в массиве #NewItemList

	    sScanChannelParams GetChannelParams(CID Channel, int GateIdx); //!< Получение параметров канала контроля; GateIdx = 1, 2
	    void SetChannelParams(CID Channel, int GateIdx, sScanChannelParams Par); //!< Установка параметров канала контроля; GateIdx = 1, 2

		// Фильтрация В-развертки

        void ResetFilter_(void);
        void SetFilterState_(bool State);

        void CalcAlarmSignals(void);
};

typedef cJointTestingReport* PcJointTestingReport;

//! Хранилище протоколов контроля
class cJointTestingBase
{
		bool PutTestingReport(cJointTestingReport * NewReport);
		bool GetScanSignals(int Params1, int Params2, PcJointTestingReport * Report);
};
//---------------------------------------------------------------------------
#endif

