object ScreenMessageForm3: TScreenMessageForm3
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'ScreenMessageForm3'
  ClientHeight = 425
  ClientWidth = 702
  Color = clRed
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -28
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 34
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 696
    Height = 419
    Align = alClient
    BorderWidth = 10
    BorderStyle = bsSingle
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -28
    Font.Name = 'Myriad Pro'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object TextLabel: TLabel
      AlignWithMargins = True
      Left = 14
      Top = 81
      Width = 664
      Height = 112
      Align = alTop
      AutoSize = False
      Caption = 'Text'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -29
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
      WordWrap = True
    end
    object GridPanel1: TGridPanel
      Left = 11
      Top = 11
      Width = 670
      Height = 50
      Align = alTop
      BevelOuter = bvNone
      Color = clWhite
      ColumnCollection = <
        item
          Value = 100.000000000000000000
        end
        item
          SizeStyle = ssAbsolute
          Value = 50.000000000000000000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = CaptionLabel
          Row = 0
        end
        item
          Column = 1
          Control = TimeoutPB
          Row = 0
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -28
      Font.Name = 'Myriad Pro'
      Font.Style = []
      ParentFont = False
      RowCollection = <
        item
          Value = 100.000000000000000000
        end>
      TabOrder = 0
      object CaptionLabel: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 93
        Height = 34
        Align = alClient
        Caption = 'Caption'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object TimeoutPB: TPaintBox
        Left = 620
        Top = 0
        Width = 50
        Height = 50
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        Visible = False
        ExplicitLeft = 664
        ExplicitTop = 16
        ExplicitWidth = 105
        ExplicitHeight = 105
      end
    end
    object GridPanel2: TGridPanel
      Left = 11
      Top = 61
      Width = 670
      Height = 17
      Align = alTop
      BevelOuter = bvNone
      Color = clWhite
      ColumnCollection = <
        item
          Value = 84.999998637971740000
        end
        item
          Value = 15.000001362028250000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = captionPanel
          Row = 0
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -28
      Font.Name = 'Myriad Pro'
      Font.Style = []
      ParentFont = False
      RowCollection = <
        item
          Value = 100.000000000000000000
        end>
      TabOrder = 1
      object captionPanel: TPanel
        Left = 0
        Top = 0
        Width = 569
        Height = 17
        Align = alClient
        BevelOuter = bvNone
        Color = clHotLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
      end
    end
    object AnswerGridPanel: TGridPanel
      AlignWithMargins = True
      Left = 14
      Top = 199
      Width = 664
      Height = 151
      Align = alClient
      BevelOuter = bvNone
      ColumnCollection = <
        item
          Value = 100.000000000000000000
        end>
      ControlCollection = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -28
      Font.Name = 'Myriad Pro'
      Font.Style = []
      ParentFont = False
      RowCollection = <
        item
          Value = 100.000000000000000000
        end>
      TabOrder = 2
      Visible = False
      ExplicitTop = 167
      ExplicitHeight = 183
    end
    object ResultBtnsPanel: TPanel
      Left = 11
      Top = 353
      Width = 670
      Height = 51
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object BtnCancel: TBitBtn
        AlignWithMargins = True
        Left = 505
        Top = 3
        Width = 162
        Height = 45
        Align = alRight
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 0
        Visible = False
        OnClick = BtnCancelClick
      end
      object BtnNo: TBitBtn
        AlignWithMargins = True
        Left = 337
        Top = 3
        Width = 162
        Height = 45
        Align = alRight
        Caption = #1053#1077#1090
        TabOrder = 1
        Visible = False
        OnClick = BtnNoClick
      end
      object BtnOkYes: TBitBtn
        AlignWithMargins = True
        Left = 169
        Top = 3
        Width = 162
        Height = 45
        Align = alRight
        Caption = #1054#1050
        TabOrder = 2
        Visible = False
        OnClick = BtnOkYesClick
      end
    end
  end
  object UpdateTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = UpdateTimerTimer
    Left = 216
    Top = 16
  end
end
