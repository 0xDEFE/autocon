﻿/**
 * @file ManualTuneFrameUnit.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef ManualTuneFrameUnitH
#define ManualTuneFrameUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "BasicFrameUnit.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Samples.Spin.hpp>
#include <Vcl.ToolWin.hpp>
#include <Vcl.Touch.Keyboard.hpp>

#include "BScanFrameUnit.h"
#include <Vcl.ImgList.hpp>

//---------------------------------------------------------------------------
#include <set>

/** \struct sMCSelectedChannelData2
    Содержит данные о выбранных для настройки каналах
 */
struct sMCSelectedChannelData2
{
    sMCSelectedChannelData2()
    {
        bValid = false;
    };

    bool hasGroup(int groupIdx)
    {
        return chGroups.find(groupIdx) != chGroups.end();
    };

    std::set<int> chGroups;
    bool bSelected;
    int scanIdx;
    bool bValid;
};

//! Отображает форму ручной настройки
class TManualTuneFrame : public TBasicFrame
{
__published:	// IDE-managed Components
    TToolBar *m_MainToolBar;
    TToolButton *ShowSelectChannelPanelBtn;
    TToolButton *ToolButton6;
    TToolButton *ShowMovePanelBtn;
    TToolButton *ToolButton9;
    TToolButton *CloseBtn;
    TPageControl *PageControl1;
    TTabSheet *SelectChannelsPage;
    TLabel *Label1;
    TLabel *ActiveChannelsLabel;
    TGridPanel *GridPanel1;
    TSpeedButton *ScanGroupBtn3;
    TSpeedButton *ScanGroupBtn5;
    TSpeedButton *ScanGroupBtn6;
    TSpeedButton *ScanGroupBtn8;
    TSpeedButton *ScanGroupBtn10;
    TSpeedButton *ScanGroupBtn12;
    TSpeedButton *ScanGroupBtn14;
    TGridPanel *GridPanel2;
    TBitBtn *ChanLeftBtn;
    TBitBtn *ChanToggleBtn;
    TBitBtn *ChanRightBtn;
    TSpeedButton *ChanBtn1;
    TSpeedButton *ChanBtn2;
    TSpeedButton *ChanBtn3;
    TSpeedButton *ChanBtn4;
    TSpeedButton *ChanBtn5;
    TSpeedButton *ChanBtn6;
    TSpeedButton *ChanBtn7;
    TSpeedButton *ChanBtn8;
    TSpeedButton *ChanBtn9;
    TSpeedButton *ChanBtn10;
    TSpeedButton *ChanBtn11;
    TSpeedButton *ChanBtn12;
    TSpeedButton *ChanBtn13;
    TSpeedButton *ChanBtn14;
    TSpeedButton *ChanBtn15;
    TSpeedButton *ChanBtn16;
    TSpeedButton *ChanBtn17;
    TSpeedButton *ChanBtn18;
    TSpeedButton *ChanBtn19;
    TSpeedButton *ChanBtn20;
    TSpeedButton *ChanBtn21;
    TTabSheet *MovePage;
    TGroupBox *GroupBox1;
    TLabel *Label2;
    TLabel *Label3;
    TGridPanel *GridPanel6;
    TSpinEdit *BtmCaretSpin;
    TGridPanel *GridPanel7;
    TSpinEdit *TopCaretSpin;
    TPanel *Panel1;
    TBitBtn *m_MoveCaretsBtn;
    TBitBtn *m_CalibrateOnMoveBtn;
    TBitBtn *m_MoveToHomeBtn;
    TTouchKeyboard *TouchKeyboard1;
    TPanel *m_FramePanel;
    TTimer *TestTimer;
    TGridPanel *GridPanel3;
    TSpeedButton *m_UpdateCaretsPosBtn;
    TGridPanel *GridPanel4;
    TStaticText *m_TopCaretLabel;
    TStaticText *m_BotCaretLabel;
    TGridPanel *GridPanel5;
    TTrackBar *m_TopCaretTrack;
    TTrackBar *m_BotCaretTrack;
    TPanel *Panel2;
    TTabSheet *OtherPage;
    TBitBtn *ClearBScanBtn;
    TBitBtn *ResetPathEncoderBtn;
    TToolButton *ClearBtn;
    TToolButton *ToolButton2;
    TToolButton *ToolButton3;
    TPanel *LeftPanel;
    TBitBtn *HidePanelBtn;
    TImageList *ImageList1;
    TGridPanel *GridPanel8;
    TSpeedButton *off1_Btn;
    TSpeedButton *off2_Btn;
    TSpeedButton *off3_Btn;
    TSpeedButton *off4_Btn;
    TSpeedButton *off5_Btn;
    TGridPanel *GridPanel9;
    TSpeedButton *BtmCaretMoveLeftBtn;
    TSpeedButton *TopCaretMoveLeftBtn;
    TSpeedButton *TopCaretMoveRightBtn;
    TSpeedButton *BtmCaretMoveRightBtn;
    TLabel *Label4;
    TPanel *KeyboardPanel;
    void __fastcall ShowSelectChannelPanelBtnClick(TObject *Sender);
    void __fastcall ShowMovePanelBtnClick(TObject *Sender);
    void __fastcall CloseBtnClick(TObject *Sender);
    void __fastcall ScanGroupBtnClick(TObject *Sender);
    void __fastcall ChanLeftBtnClick(TObject *Sender);
    void __fastcall ChanToggleBtnClick(TObject *Sender);
    void __fastcall ChanRightBtnClick(TObject *Sender);
    void __fastcall ChanBtnClick(TObject *Sender);
    void __fastcall CaretSpinChange(TObject *Sender);
    void __fastcall BtmCaretSpinClick(TObject *Sender);
    void __fastcall TopCaretSpinClick(TObject *Sender);
    void __fastcall m_MoveCaretsBtnClick(TObject *Sender);
    void __fastcall m_CalibrateOnMoveBtnClick(TObject *Sender);
    void __fastcall m_MoveToHomeBtnClick(TObject *Sender);
    void __fastcall m_UpdateCaretsPosBtnClick(TObject *Sender);
    void __fastcall ClearBScanBtnClick(TObject *Sender);
    void __fastcall ResetPathEncoderBtnClick(TObject *Sender);
    void __fastcall ClearBtnClick(TObject *Sender);
    void __fastcall HidePanelBtnClick(TObject *Sender);
    void __fastcall onSetOffsetValueBtn(TObject *Sender);
private:	// User declarations
    void OnMFrameAttach(TBasicFrame* pMain);
    void OnMFrameDetach(TBasicFrame* pMain);
    void OnMFrameCreate(TBasicFrame* pMain);
    void OnMFrameDestroy(TBasicFrame* pMain);
    void __fastcall OnBScanChannelBtnStateChanged(TObject *Sender);
    //void OnAcMotion(eMainUnitSignalType sigtype, int WorkCycle);

    void UpdateCaretPos();

    void SetChGroup(int groupIdx, bool bSelAll = false);
    void SetChannel(int chId, bool bVal);
    void UpdateChannelButtons(bool bUpdateGroupBtns = true, bool bUpdateChBtns = true);
    void UpdateBScanFrameChannels();
    void UpdateChannelClearBtn();
    void SetCurrentPanel(int idx);

    int firstChannelIdx;
    bool bToggleBtnState;

    TBScanFrame* BScanFrame;

    std::vector<TSpeedButton*> groupBtnsArr;
    std::vector<TSpeedButton*> channelBtnsArr;

    std::vector<sMCSelectedChannelData2> selectedChannels;
    std::vector<int> curGroupSelChIndices;
    int curGroupIdx;

public:		// User declarations
    __fastcall TManualTuneFrame(TComponent* Owner);
    UnicodeString GetFrameName() {return L"TManualTuneFrame";};
};
//---------------------------------------------------------------------------
//extern PACKAGE TManualTuneFrame *ManualTuneFrame;
//---------------------------------------------------------------------------
#endif
