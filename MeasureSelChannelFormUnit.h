//---------------------------------------------------------------------------

#ifndef MeasureSelChannelFormUnitH
#define MeasureSelChannelFormUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TMeasureSelChannelForm : public TForm
{
__published:	// IDE-managed Components
    TGridPanel *GridPanel1;
    TPanel *SelPanel1;
    TCheckBox *SelCheck1;
    TPanel *SelPanel2;
    TCheckBox *SelCheck2;
    TPanel *SelPanel3;
    TCheckBox *SelCheck3;
    TPanel *SelPanel4;
    TCheckBox *SelCheck4;
    TPanel *SelPanel5;
    TCheckBox *SelCheck5;
    TPanel *SelPanel6;
    TCheckBox *SelCheck6;
    TPanel *SelPB1;
    TPanel *SelPB2;
    TPanel *SelPB3;
    TPanel *SelPB4;
    TPanel *SelPB5;
    TPanel *SelPB6;
    TBitBtn *BitBtn1;
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
private:	// User declarations
    struct sMeasChannelData
    {
        int idx;
        int id;
        TPanel* pPanel;
        TCheckBox* pCheck;
        TPanel* pPB;
    };

    sMeasChannelData chData[6];
public:		// User declarations
    __fastcall TMeasureSelChannelForm(TComponent* Owner);

    void clearChannels();
    void setChannel(unsigned int idx, int id, const UnicodeString& name, TColor color);
    bool channelState(int id);
};
//---------------------------------------------------------------------------
extern PACKAGE TMeasureSelChannelForm *MeasureSelChannelForm;
//---------------------------------------------------------------------------
#endif
