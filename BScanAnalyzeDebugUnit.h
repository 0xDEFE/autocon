//---------------------------------------------------------------------------

#ifndef BScanAnalyzeDebugUnitH
#define BScanAnalyzeDebugUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "DataStorage.h"
#include "AutoconMain.h"
#include <VCLTee.Chart.hpp>
#include <VCLTee.Series.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <Vcl.ComCtrls.hpp>
#include <VCLTee.TeeShape.hpp>
#include "cspin.h"
#include <Vcl.Samples.Spin.hpp>

//---------------------------------------------------------------------------
class TBScanAnalyzeDebugForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TComboBox *ChList;
	TButton *Button1;
	TChart *Chart1;
	TPointSeries *BScanSeries;
	TTrackBar *ThTrackBar;
	TLabel *ThLabel;
	TLabel *RatioLabel;
	TChart *Chart2;
	TFastLineSeries *Series1;
	TChartShape *Series2;
	TCSpinEdit *AngleEdit;
	TCSpinEdit *SumLenEdit;
	TLabel *Label1;
	TLabel *Label2;
    TChartShape *Series3;
    TLabel *Label3;
    TCSpinEdit *ResEdit;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall ThTrackBarChange(TObject *Sender);
	void __fastcall ChListChange(TObject *Sender);
	void __fastcall Chart1Zoom(TObject *Sender);
    void __fastcall Chart1Click(TObject *Sender);
    void __fastcall Chart1MouseMove(TObject *Sender, TShiftState Shift, int X, int Y);


private:	// User declarations
	int Th;
    int ChartX;
    int ChartY;

    int ChartPosX;
    int ChartPosY;

	void DrawSignals(int SysCoord, CID Channel, PsScanSignalsOneCrdOneChannel Sgl);
	int ResGis[2000];

public:		// User declarations

   cJointTestingReport *Rep;
	__fastcall TBScanAnalyzeDebugForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TBScanAnalyzeDebugForm *BScanAnalyzeDebugForm;
//---------------------------------------------------------------------------
extern cAutoconMain *AutoconMain;
#endif
