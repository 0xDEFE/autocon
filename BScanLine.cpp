//---------------------------------------------------------------------------

#pragma hdrstop

#include "BScanLine.h"
#include <algorithm>

//---------------------------------------------------------------------------

TBScanLine::TBScanLine(void)
{
    Buffer = new TBitmap();
    PointWidth = 3;

    MinDelay = 0;
    MaxDelay = 1;

    MinCrd = 0;
    MaxCrd = 1;
}

TBScanLine::~TBScanLine(void)
{
    delete Buffer;
}

void TBScanLine::Clear(void)
{
    Buffer->Canvas->Brush->Color = clWhite;
    Buffer->Canvas->FillRect(Rect(0, 0, Buffer->Width, Buffer->Height));
}

void TBScanLine::SetSize(TSize Sz)
{
    Buffer->FreeImage();
    Buffer->SetSize(Sz.cx + Sz.cx / 4, Sz.cy); // ������� ������ + ����� 25%
    ViewWidth = Sz.cx;
}

void TBScanLine::Draw(int X, int Y, TCanvas *Canvas)
{
    Canvas->CopyRect(Rect(X, Y, ViewWidth, Buffer->Height), Buffer->Canvas, Rect(0, 0, ViewWidth, Buffer->Height));
//    Canvas->Draw(X, Y, Buffer);
//    Canvas->MoveTo(ViewWidth, 0);
//    Canvas->LineTo(ViewWidth, 1000);
}

int TBScanLine::CrdToPixel(int Crd)
{
    return ViewWidth * (float(Crd - MinCrd) / float(MaxCrd - MinCrd));
}

int TBScanLine::DelayToPixel(float Delay)
{
    return Buffer->Height - Buffer->Height * (float(Delay - MinDelay) / float(MaxDelay - MinDelay));
}

void TBScanLine::SetViewDelayZone(int MinDelay_, int MaxDelay_)
{
    MinDelay = MinDelay_;
    MaxDelay = std::max(MinDelay+1, MaxDelay_);
}

int i = 0;

void TBScanLine::SetViewCrdZone(int MinCrd_, int MaxCrd_)
{
    Buffer->Canvas->CopyRect(Rect(0, 0, ViewWidth + PointWidth, Buffer->Height),
                             Buffer->Canvas,
                             Rect(CrdToPixel(MaxCrd_) - ViewWidth, 0, CrdToPixel(MaxCrd_) + PointWidth, Buffer->Height));
    Buffer->Canvas->Brush->Color = clWhite;
    Buffer->Canvas->FillRect(Rect(ViewWidth + PointWidth, 0, CrdToPixel(MaxCrd_) + PointWidth, Buffer->Height));
    MinCrd = MinCrd_;
    MaxCrd = MaxCrd_;
}

int HLSMAX = 240;
int RGBMAX = 255;
int UNDEFINED;
int R, G, B; //  �����
float Magic1, Magic2;

float HueToRGB(float n1, float n2, float hue)
{
  if (hue < 0) hue = hue + HLSMAX;
  if (hue > HLSMAX) hue = hue - HLSMAX;
  if (hue < (HLSMAX/6))
  {
     return ( n1 + (((n2-n1)*hue+(HLSMAX/12))/(HLSMAX/6)) );
  }
  else if (hue < (HLSMAX/2)) return n2;
       else if (hue < ((HLSMAX*2)/3)) return (n1 + (((n2-n1)*(((HLSMAX*2)/3)-hue)+(HLSMAX/12))/(HLSMAX/6)));
            else return n1;
}

TColor HLStoRGB(int H, int L, int S)
{
    UNDEFINED = (HLSMAX * 2) / 3;

    if (S == 0)
    {
       B = (L*RGBMAX)/HLSMAX ;
       R = B;
       G = B;
    }
    else
    {
        if (L <= (HLSMAX/2)) Magic2 = (L*(HLSMAX + S) + (HLSMAX/2))/HLSMAX;
                             else Magic2 = L + S - ((L*S) + (HLSMAX/2))/HLSMAX;
        Magic1 = 2*L-Magic2;
        R = (HueToRGB(Magic1,Magic2,H+(HLSMAX/3))*RGBMAX + (HLSMAX/2))/HLSMAX ;
        G = (HueToRGB(Magic1,Magic2,H)*RGBMAX + (HLSMAX/2)) / HLSMAX ;
        B = (HueToRGB(Magic1,Magic2,H-(HLSMAX/3))*RGBMAX + (HLSMAX/2))/HLSMAX ;
    }
    if (R<0) R=0; if (R>RGBMAX) R = RGBMAX;
    if (G<0) G=0; if (G>RGBMAX) G = RGBMAX;
    if (B<0) B=0; if (B>RGBMAX) B = RGBMAX;
    return (TColor)(R + (G << 8) + (B << 16));
}

void TBScanLine::SetPoint(int Crd, float minDelay, float maxDelay, int Ampl) // ����� �� �-���������
{
    int X = CrdToPixel(Crd);
//    Buffer->Canvas->Brush->Color = clRed;
    Buffer->Canvas->Brush->Color = HLStoRGB(165 - 165 * Ampl / 255, 116, 218);
    int Y1 = DelayToPixel(minDelay);
    int Y2 = DelayToPixel(maxDelay);
    Buffer->Canvas->FillRect(Rect(X, Y2, X + PointWidth, Y1));
}

//---------------------------------------------------------------------------

