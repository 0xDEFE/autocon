//---------------------------------------------------------------------------

// #pragma hdrstop

#include "BScanDraw.h"


// #pragma package(smart_init)

/*
       cChannelsTable* Tbe;
       cDeviceConfig* Cfg;
       cJointTestingReport* Rep;
       std::vector < cBScanLines* > BScanLines;
*/

cBScanDraw::cBScanDraw(cChannelsTable* Table, cDeviceConfig* Config, cJointTestingReport* Report,cDeviceCalibration* Calibration)
{
   Tbl = Table;
   Cfg = Config;
   Rep = Report;
   Calib = Calibration;
   bUseReportForGateData = false;
   //Modifyed by KirillB (bug)
   Th = 0;
   //GroupShift �� ���������������!
   memset(GroupShift,0,sizeof(GroupShift));
   CursorSysCrd = 0;
   CursorBScanLinesIdx = 0;
   CursorBScanLinesSubIdx = 0;
   bCursorEnabled = false;

   bEnableCompareDataDraw = false;
   pCompareData = NULL;
}

cBScanDraw::~cBScanDraw(void)
{
    //
}

void cBScanDraw::ClearBScanLines()
{
	BScanLinesList.clear();
}

void cBScanDraw::AddBScanLine(cBScanLines* BScanLines)
{
   BScanLinesList.push_back(BScanLines);
}

void cBScanDraw::Prepare(void)
{
	for (int idx = 0; idx < MaxLinkData; idx++) LinkData[idx].Used = false;

	//int t=BScanLinesList.size();
	//Added by KirillB
	memset(LinkData,0,MaxLinkData*sizeof(LinkDataItem));

	int Shift_CID;
	for (unsigned int BScanLines_idx = 0; BScanLines_idx < BScanLinesList.size(); BScanLines_idx++)
	{
        for (int x = 0; x < 6; x++)
            for (int y = 0; y < 6; y++)
				if (BScanLinesList[BScanLines_idx]->BtnVisible[x][y])
                {
                    Shift_CID = BScanLinesList[BScanLines_idx]->BtnId[x][y] - AutoconStartCID;
					LinkData[Shift_CID].Used = true;
                    LinkData[Shift_CID].Enabled = BScanLinesList[BScanLines_idx]->BtnBtnState[x][y];
                    LinkData[Shift_CID].Rt = BScanLinesList[BScanLines_idx]->LineRect[y];
					LinkData[Shift_CID].Buffer = BScanLinesList[BScanLines_idx]->Buffer;
					LinkData[Shift_CID].Color =  BScanLinesList[BScanLines_idx]->BtnChColor[x][y];
					LinkData[Shift_CID].drawType = BScanLinesList[BScanLines_idx]->LineDrawType[y];
					Tbl->ItemByCID(BScanLinesList[BScanLines_idx]->BtnId[x][y], &LinkData[Shift_CID].ChDesc);
                }
	}
}

bool cBScanDraw::GetSysCoord(int X, int Y, int* SysCoord, Shift_CID_List* Shift_CID, int* Shift_CID_Cnt, BScan_Color_List *BScanColor, int* pDelay)
{
    int SysCrd;
    int BScanLine;
	int Shift_CID_;

	for (unsigned int BScanLines_idx = 0; BScanLines_idx < BScanLinesList.size(); BScanLines_idx++)
        if (BScanLinesList[BScanLines_idx]->Used)
        {
            if (BScanLinesList[BScanLines_idx]->GetSysCoord(X, Y, Rep->Header.MaxSysCoord, &SysCrd, &BScanLine))
            {
                *Shift_CID_Cnt = 0;
                for (int x = 0; x < BScanLinesList[BScanLines_idx]->BtnColumnCount * 2; x++)
                {
                    Shift_CID_ = BScanLinesList[BScanLines_idx]->BtnId[x][BScanLine]; // �������� ���� �� �������� Shift_CID
                    if (Shift_CID_ != 0)
                    {
                        Shift_CID_ = Shift_CID_ - AutoconStartCID;
                        if (LinkData[Shift_CID_].Enabled)
                        {
                           int idx = *Shift_CID_Cnt;
                           (*Shift_CID)[(*Shift_CID_Cnt)] = Shift_CID_;
                           (*BScanColor)[(*Shift_CID_Cnt)] = BScanLinesList[BScanLines_idx]->BtnChColor[x][BScanLine];
                           (*Shift_CID_Cnt)++;
                        }
                    }
                }
                *SysCoord = SysCrd;
                if(pDelay)
                {
                    *pDelay = ScreenToDelay(Y,BScanLines_idx,BScanLine);
                    int rev = DelayToScreen(*pDelay,BScanLines_idx,BScanLine);
                    //assert(Y == rev);
                }
                return true;
            }
        }
    return false;
}

void cBScanDraw::SetCursorXY(bool bEnable, int X, int Y, TColor col)
{
    int SysCrd;
    int BScanLine;

    for (unsigned int BScanLines_idx = 0; BScanLines_idx < BScanLinesList.size(); BScanLines_idx++)
        if (BScanLinesList[BScanLines_idx]->Used)
        {
            if (BScanLinesList[BScanLines_idx]->GetSysCoord(X, Y, Rep->Header.MaxSysCoord, &SysCrd, &BScanLine))
            {
                SetCursorSC( bEnable,SysCrd, BScanLines_idx, BScanLine, col);
                return;
            }
        }
}

void cBScanDraw::SetCursorSC(bool bEnable, int SysCoord, int BScanLines_idx,int BScanLines_sub_idx, TColor col)
{
    CursorSysCrd = SysCoord;
    if(BScanLines_idx != -1)
        CursorBScanLinesIdx = BScanLines_idx;
    if(BScanLines_sub_idx != -1)
        CursorBScanLinesSubIdx = BScanLines_sub_idx;
    bCursorEnabled = bEnable;

    Draw(false);
    return;
};

int HLSMAX = 240;
int RGBMAX = 255;
int UNDEFINED;
int R, G, B; //  �����
float Magic1, Magic2;

float HueToRGB(float n1, float n2, float hue)
{
  if (hue < 0) hue = hue + HLSMAX;
  if (hue > HLSMAX) hue = hue - HLSMAX;
  if (hue < (HLSMAX/6))
  {
     return ( n1 + (((n2-n1)*hue+(HLSMAX/12))/(HLSMAX/6)) );
  }
  else if (hue < (HLSMAX/2)) return n2;
       else if (hue < ((HLSMAX*2)/3)) return (n1 + (((n2-n1)*(((HLSMAX*2)/3)-hue)+(HLSMAX/12))/(HLSMAX/6)));
            else return n1;
}

TColor HLStoRGB(int H, int L, int S)
{
    UNDEFINED = (HLSMAX * 2) / 3;

    if (S == 0)
    {
       B = (L*RGBMAX)/HLSMAX ;
       R = B;
       G = B;
    }
    else
    {
        if (L <= (HLSMAX/2)) Magic2 = (L*(HLSMAX + S) + (HLSMAX/2))/HLSMAX;
                             else Magic2 = L + S - ((L*S) + (HLSMAX/2))/HLSMAX;
        Magic1 = 2*L-Magic2;
        R = (HueToRGB(Magic1,Magic2,H+(HLSMAX/3))*RGBMAX + (HLSMAX/2))/HLSMAX ;
        G = (HueToRGB(Magic1,Magic2,H)*RGBMAX + (HLSMAX/2)) / HLSMAX ;
        B = (HueToRGB(Magic1,Magic2,H-(HLSMAX/3))*RGBMAX + (HLSMAX/2))/HLSMAX ;
    }
    if (R<0) R=0; if (R>RGBMAX) R = RGBMAX;
    if (G<0) G=0; if (G>RGBMAX) G = RGBMAX;
    if (B<0) B=0; if (B>RGBMAX) B = RGBMAX;
    return TColor(R + (G << 8) + (B << 16));
}

//---------------------------------------------------------------------------

bool cBScanDraw::bDisableAScanStrobeCheck = false;
bool cBScanDraw::bDisableBScanStrobeCheck = false;
void cBScanDraw::DrawSignals(int SysCoord, CID Channel, PsScanSignalsOneCrdOneChannel Sgl)
{
    if (!Sgl) return;
    if((Channel < 0) || (Channel > 200))
        return;

	int DelayHeight = LinkData[Channel].ChDesc.cdBScanGate.gEnd - LinkData[Channel].ChDesc.cdBScanGate.gStart;
	int Height = LinkData[Channel].Rt.Height();
    int aidx = 11;
    bool Flg = false;

    //Update channels gates
    int stGate = 0;
    int edGate = 0;

    //Updating channel gates - if channel draws as LINEDRAW_SMALL_LINE
    if(LinkData[Channel].drawType == LINEDRAW_SMALL_LINE)
    {
        for(int i = 0; i < 9; i++)
        {
            if(channels_gates_temp[i][0] == LinkData[Channel].ChDesc.id)
            {
                stGate = channels_gates_temp[i][1];
                edGate = channels_gates_temp[i][2];
            }
        }
    }

    //��� �� �� �������� �� ������� ������� ���������
    HRGN ClipRgn;
    if( LinkData[Channel].Used )
    {
        ClipRgn = ::CreateRectRgn(LinkData[Channel].Rt.left,LinkData[Channel].Rt.top,
                                    LinkData[Channel].Rt.right,LinkData[Channel].Rt.bottom);
        ::SelectClipRgn(LinkData[Channel].Buffer->Canvas->Handle,ClipRgn);
    }

	if ((LinkData[Channel].Used) && (LinkData[Channel].Enabled))
        for (int idx = 0; idx < Sgl->Count; idx++)
        {
            float RealDelay = float(Sgl->Signals[idx].Delay) / float(LinkData[Channel].ChDesc.cdBScanDelayMultiply);
            if (((RealDelay >= LinkData[Channel].ChDesc.cdBScanGate.gStart) &&
                        (RealDelay <= LinkData[Channel].ChDesc.cdBScanGate.gEnd)) || bDisableAScanStrobeCheck)
            {
                //�������� - ���� ������ ������������� ��� LINEDRAW_SMALL_LINE
                    //�� �� �������� ���� �� �� � ������
                if((LinkData[Channel].drawType == LINEDRAW_SMALL_LINE) && (!bDisableAScanStrobeCheck))
                {
                    if((RealDelay < stGate) ||    //���� ������ �� � ������
                        (RealDelay > edGate))
                        continue;
                }

                //�������� - ������ ���� ������
				Flg = false;
                for (int aidx = 0; aidx < 24; aidx++)
                    if (Sgl->Signals[idx].Ampl[aidx] >= Th)
                    {
                        Flg = true;
                        break;
                    }

                //���� ���� ������ - ������
                if (Flg)
                {
                    x = LinkData[Channel].Rt.Left + (LinkData[Channel].Rt.Right - LinkData[Channel].Rt.Left) * SysCoord / Rep->Header.MaxSysCoord;
                    y = LinkData[Channel].Rt.Bottom - float(Height) * float(RealDelay - LinkData[Channel].ChDesc.cdBScanGate.gStart) / float(DelayHeight);

                    if(Sgl->bIsAlarmed)
                    {
                        ////LinkData[Channel].Buffer->Canvas->Brush->Color = LinkData[Channel].Color;
                        //LinkData[Channel].Buffer->Canvas->Brush->Color = clBlack;
                        //LinkData[Channel].Buffer->Canvas->FillRect(Rect(x, LinkData[Channel].Rt.Top + 1, x+1, LinkData[Channel].Rt.Top + 10/*LinkData[Channel].Rt.Bottom - 1*/));
                    }

                    const int pointHalfHeight = std::max(1.0, ceil(128.f / float(DelayHeight)));

					LinkData[Channel].Buffer->Canvas->Brush->Color = LinkData[Channel].Color;

					if(LinkData[Channel].drawType == LINEDRAW_STD)
					{
						LinkData[Channel].Buffer->Canvas->FillRect(Rect(x-1, y-pointHalfHeight, x+1, y+pointHalfHeight));
					}
					else if( LinkData[Channel].drawType == LINEDRAW_SMALL_LINE )
					{
						int offset = 3;
						LinkData[Channel].Buffer->Canvas->FillRect(Rect(x - 1, LinkData[Channel].Rt.Top + offset, x + 1, LinkData[Channel].Rt.Bottom - offset));
                    }
                }
            }
        }

    if( LinkData[Channel].Used )
    {
        ::SelectClipRgn(LinkData[Channel].Buffer->Canvas->Handle,NULL);
        DeleteObject(ClipRgn);
    }
}

int cBScanDraw::SysCoordToScreen(int SysCoord,CID Channel, TRect rect)
{
    return rect.Left + (rect.Right - rect.Left) * SysCoord / Rep->Header.MaxSysCoord;
};

int cBScanDraw::DelayToScreen(int Delay,CID Channel, TRect rect)
{
    int DelayHeight = LinkData[Channel].ChDesc.cdBScanGate.gEnd - LinkData[Channel].ChDesc.cdBScanGate.gStart;
	int Height = rect.Height();

    return rect.Bottom - float(Height) * float(Delay - LinkData[Channel].ChDesc.cdBScanGate.gStart) / float(DelayHeight);
};

int cBScanDraw::DelayToScreen(int Delay, unsigned int BScanLines_idx, int LineIdx)
{
    CID Channel = -1;
    for (int BtnIdx = 0; BtnIdx < BScanLinesList[BScanLines_idx]->BtnColumnCount * 2; BtnIdx++)
    {
        if (!BScanLinesList[BScanLines_idx]->BtnVisible[BtnIdx][LineIdx])
            continue;
        Channel = BScanLinesList[BScanLines_idx]->BtnId[BtnIdx][LineIdx];
        break;
    }

    return DelayToScreen(Delay, Channel  - AutoconStartCID, BScanLinesList[BScanLines_idx]->LineRect[LineIdx]) + BScanLinesList[BScanLines_idx]->BoundsRect.Top;
};

int cBScanDraw::ScreenToDelay(int Y, unsigned int BScanLines_idx, int LineIdx)
{
    CID Channel = -1;

    for (int BtnIdx = 0; BtnIdx < 6; BtnIdx++)
    {
        if (!BScanLinesList[BScanLines_idx]->BtnVisible[BtnIdx][LineIdx])
            continue;
        Channel = BScanLinesList[BScanLines_idx]->BtnId[BtnIdx][LineIdx];
        break;
    }

    TRect rect = BScanLinesList[BScanLines_idx]->LineRect[LineIdx];
    int DelayHeight = LinkData[Channel - AutoconStartCID].ChDesc.cdBScanGate.gEnd - LinkData[Channel - AutoconStartCID].ChDesc.cdBScanGate.gStart;
    int strobeStart = LinkData[Channel - AutoconStartCID].ChDesc.cdBScanGate.gStart;
    int Height = rect.Height();

    return ((float(rect.Bottom - (Y - BScanLinesList[BScanLines_idx]->BoundsRect.Top)) * float(DelayHeight)) / float(Height)) + strobeStart;
};

void cBScanDraw::Draw(bool NewItemFlag)
{
	PsScanSignalsOneCrdOneChannel sgl;
    int SysCoord = 0;
    CID ChannelId = 0;
    bool Result;

    //Update channels gates (����� �� �������� GetStGate � getFirstSChannelbyID �� ������ ���������� - ������ ��� ���)
    sScanChannelDescription scDesc;
    for (int i = 0; i < MaxLinkData; i++)
		if (LinkData[i].drawType == LINEDRAW_SMALL_LINE)
        {
            if(Cfg->getFirstSChannelbyID(LinkData[i].ChDesc.id, &scDesc) != -1)
            {
                channels_gates_temp[scDesc.BScanGroup][0] = LinkData[i].ChDesc.id;

                if(bUseReportForGateData)
                {
                    sScanChannelParams params;
                    params = Rep->GetChannelParams(LinkData[i].ChDesc.id, 1);

                    channels_gates_temp[scDesc.BScanGroup][1] = params.StGate;
                    channels_gates_temp[scDesc.BScanGroup][2] = params.EdGate;
                }
                else
                {
                    channels_gates_temp[scDesc.BScanGroup][1] =
                                Calib->GetStGate(dsNone, LinkData[i].ChDesc.id, 0);
                    channels_gates_temp[scDesc.BScanGroup][2] =
                                Calib->GetEdGate(dsNone, LinkData[i].ChDesc.id, 0);
                }
            }
        }

    if (!NewItemFlag)
    {
        drawGrid();
        if(pCompareData && bEnableCompareDataDraw) //fill measure regions
        {
            drawMeasureGrid();
        }

		sgl = Rep->GetFirstScanSignals(SysCoord, ChannelId, &Result);
        if (Result) DrawSignals(SysCoord, ChannelId, sgl);
        while (true)
        {
            sgl = Rep->GetNextScanSignals(&SysCoord, &ChannelId, &Result);
            if (!Result) break;
            DrawSignals(SysCoord, ChannelId, sgl);
        }
        drawGridLegend();
    }
    else
    {
        int SaveIdx = Rep->NewItemListIdx;
        for (int i = 0; i < SaveIdx; i++)
        {
            sgl = Rep->GetScanSignals(Rep->NewItemList[i].Crd, Rep->NewItemList[i].Ch);
            DrawSignals(Rep->NewItemList[i].Crd, Rep->NewItemList[i].Ch, sgl);
        }
        Rep->ClearNewItemList(SaveIdx);
        drawGridLegend();
    }

    if(bCursorEnabled)
    {
        drawCursor();
    }

    if(pCompareData && bEnableCompareDataDraw)
    {
        drawMeasureData();
    }

    //Draw joint line
	for (int i = 0; i < MaxLinkData; i++)
        if (LinkData[i].Buffer)
        {
            LinkData[i].Buffer->Canvas->MoveTo((LinkData[i].Rt.left + LinkData[i].Rt.right) / 2, LinkData[i].Rt.Top);
            LinkData[i].Buffer->Canvas->LineTo((LinkData[i].Rt.left + LinkData[i].Rt.right) / 2, LinkData[i].Rt.Bottom);
			LinkData[i].Buffer->Canvas->Brush->Style = bsClear;

			LinkData[i].Buffer->Canvas->Rectangle(LinkData[i].Rt);
        }
}

void cBScanDraw::drawGrid(unsigned int BScanLines_idx, int LineIdx, TColor col1, TColor col2)
{
    TRect lineRect = BScanLinesList[BScanLines_idx]->LineRect[LineIdx];
    BScanLinesList[BScanLines_idx]->Buffer->Canvas->Brush->Color = clWhite;
    BScanLinesList[BScanLines_idx]->Buffer->Canvas->FillRect(lineRect);

    if(!lineRect.Width())
        return;

    float realToBScanCoeff =  float(lineRect.Width()) / float(Rep->Header.MaxSysCoord);
    int x_inc = 50;
    int startOffset = (Rep->Header.MaxSysCoord / 2) % x_inc;
    int x_start = (startOffset - x_inc);

    //TRACE("[%d] x_inc=%d, startOffset=%d, x_start=%d, rt_width=%d, maxcrd=%d",i, x_inc,startOffset,x_start,lineRect.Width(),Rep->Header.MaxSysCoord);

    bool bColorType = false;
    for(int k = x_start; k < Rep->Header.MaxSysCoord; k+= x_inc)
    {
        TRect rect(lineRect.left + k*realToBScanCoeff, lineRect.top, lineRect.left + (k + x_inc)*realToBScanCoeff, lineRect.bottom);
        rect.left = std::max( lineRect.left, rect.left);
        rect.right = std::min( lineRect.right, rect.right);
        if(bColorType)
            BScanLinesList[BScanLines_idx]->Buffer->Canvas->Brush->Color = col1;
        else
            BScanLinesList[BScanLines_idx]->Buffer->Canvas->Brush->Color = col2;
        BScanLinesList[BScanLines_idx]->Buffer->Canvas->FillRect(rect);
        bColorType = !bColorType;
    }
};

void cBScanDraw::drawGridLegend(unsigned int BScanLines_idx, int LineIdx)
{
    TRect lineRect = BScanLinesList[BScanLines_idx]->LineRect[LineIdx];
    TRect legendRect(lineRect.left + 5, lineRect.top + 2, lineRect.right, lineRect.top + 12);

    if(!lineRect.Width())
        return;

    TTextFormat tf;
    tf.Clear();
    tf << tfLeft << tfVerticalCenter << tfSingleLine << tfNoClip;

    float realToBScanCoeff =  float(lineRect.Width()) / float(Rep->Header.MaxSysCoord);
    float x_inc = 50;
    float startOffset = (Rep->Header.MaxSysCoord / 2) % (int)x_inc;
    float x_inc_conv = (startOffset + x_inc) * realToBScanCoeff;
    legendRect.left = lineRect.left + x_inc_conv;

    TCanvas* pCanvas = BScanLinesList[BScanLines_idx]->Buffer->Canvas;

    pCanvas->Brush->Color = clBlack;
    pCanvas->Brush->Style = bsClear;
    pCanvas->Pen->Color = clBlack;
    pCanvas->Font->Size = -10;
    pCanvas->Font->Name = "Verdana";
    pCanvas->Font->Style = TFontStyles();
    pCanvas->Font->Color = RGB(100,100,100);

    pCanvas->MoveTo(legendRect.Left, legendRect.Top + legendRect.Height() /2);
    pCanvas->LineTo(legendRect.Left + x_inc_conv, legendRect.Top + legendRect.Height() /2);

    pCanvas->MoveTo(legendRect.Left, legendRect.Top + legendRect.Height() /2 - 2.5);
    pCanvas->LineTo(legendRect.Left, legendRect.Top + legendRect.Height() /2 + 3);
    pCanvas->MoveTo(legendRect.Left + x_inc_conv, legendRect.Top + legendRect.Height() /2 - 2.5);
    pCanvas->LineTo(legendRect.Left + x_inc_conv, legendRect.Top + legendRect.Height() /2 + 3);

    TRect textRect = legendRect;
    textRect.left = legendRect.Left + x_inc_conv + 5;

    UnicodeString str = StringFormatU(L"%d ��",(int)x_inc);
    pCanvas->TextRect( textRect, str, tf);
};

void cBScanDraw::drawGridLegend()
{
    for(unsigned int i = 0; i < BScanLinesList.size(); i++)
    {
        if(!BScanLinesList[i])
            continue;

        for(int j = 0; j < BScanLinesList[i]->BScanLineCount; j++)
        {
            if(BScanLinesList[i]->LineDrawType[j] != LINEDRAW_STD)
                continue;
            drawGridLegend(i,j);
        }
    }
}

void cBScanDraw::drawGrid()
{
    for(unsigned int i = 0; i < BScanLinesList.size(); i++)
    {
        if(!BScanLinesList[i])
            continue;
        for(int j = 0; j < BScanLinesList[i]->BScanLineCount; j++)
        {
            drawGrid(i,j,TColor(RGB(240,240,240)), clWhite);
        }
    }
}

void cBScanDraw::drawMeasureGrid()
{
    for(unsigned int i = 0; i < BScanLinesList.size(); i++)
    {
        if(!BScanLinesList[i])
            continue;
        for(int j = 0; j < BScanLinesList[i]->BScanLineCount; j++)
        {
            HRGN rgn = createMeasureRgn(i,j);
            TBitmap* buffer = BScanLinesList[i]->Buffer;

            ::SelectClipRgn(buffer->Canvas->Handle,rgn);
            drawGrid(i,j,TColor(RGB(253,194,170)), TColor(RGB(254,220,205)));
            ::SelectClipRgn(buffer->Canvas->Handle,NULL);
            ::DeleteObject(rgn);
        }
    }
};

void cBScanDraw::drawCursor()
{
    TRect rect = BScanLinesList[CursorBScanLinesIdx]->LineRect[CursorBScanLinesSubIdx];
    int top = BScanLinesList[CursorBScanLinesIdx]->LineRect[CursorBScanLinesSubIdx].top;
    int bottom = BScanLinesList[CursorBScanLinesIdx]->LineRect[CursorBScanLinesSubIdx].bottom;
    TBitmap* buffer = BScanLinesList[CursorBScanLinesIdx]->Buffer;

    TPoint triangle[4];
    const int triRadius = 4;

    int sysPosX = rect.Left + (rect.Right - rect.Left) * CursorSysCrd / Rep->Header.MaxSysCoord;

    buffer->Canvas->MoveTo(sysPosX, top);
    buffer->Canvas->LineTo(sysPosX, bottom);

    buffer->Canvas->Brush->Color = clBlack;
    buffer->Canvas->Brush->Style = bsSolid;

    triangle[0].x = sysPosX - triRadius;
    triangle[0].y = top;
    triangle[1].x = sysPosX + triRadius;
    triangle[1].y = top;
    triangle[2].x = sysPosX;
    triangle[2].y = top + triRadius*2;
    triangle[3].x = sysPosX - triRadius;
    triangle[3].y = top;
    buffer->Canvas->Polygon(triangle,3);

    triangle[0].x = sysPosX - triRadius;
    triangle[0].y = bottom;
    triangle[1].x = sysPosX + triRadius;
    triangle[1].y = bottom;
    triangle[2].x = sysPosX;
    triangle[2].y = bottom - triRadius*2;
    triangle[3].x = sysPosX - triRadius;
    triangle[3].y = bottom;
    buffer->Canvas->Polygon(triangle,3);
}

HRGN cBScanDraw::createMeasureRgn(unsigned int BScanLines_idx, int LineIdx)
{
    HRGN CombineRgn = ::CreateRectRgn(0,0,0,0);

    TRect rect = BScanLinesList[BScanLines_idx]->LineRect[LineIdx];
    TBitmap* buffer = BScanLinesList[BScanLines_idx]->Buffer;

    CID id = -1;
    for (int BtnIdx = 0; BtnIdx < BScanLinesList[BScanLines_idx]->BtnColumnCount * 2; BtnIdx++)
    {
        if (!BScanLinesList[BScanLines_idx]->BtnVisible[BtnIdx][LineIdx])
            continue;

        id = BScanLinesList[BScanLines_idx]->BtnId[BtnIdx][LineIdx];

        for(unsigned int i = 0; i < pCompareData->regions.size(); i++)
        {
            if(!pCompareData->regions[i].hasChannel(id))
                continue;

            const PolyReg& poly = pCompareData->regions[i].poly;
            if(!poly.isValid())
                continue;

            POINT* pPoints = new POINT[poly.points.size()];
            for(unsigned int j = 0; j < poly.points.size(); j++)
            {
                PolyPt pt = SysToScreenCrd(poly.points[j], id - AutoconStartCID, rect);
                pPoints[j].x = pt.x;
                pPoints[j].y = pt.y;
            }
            HRGN PolyRgn = ::CreatePolygonRgn(pPoints, poly.points.size(), WINDING);
            ::CombineRgn( CombineRgn, CombineRgn, PolyRgn, RGN_OR);
            DeleteObject(PolyRgn);
        }
    }
    return CombineRgn;
}

void cBScanDraw::drawMeasureData()
{
    TTextFormat tf;
    tf.Clear();
    tf << tfCenter << tfVerticalCenter << tfSingleLine << tfNoClip;

    for (unsigned int BScanLines_idx = 0; BScanLines_idx < BScanLinesList.size(); BScanLines_idx++)
        if (BScanLinesList[BScanLines_idx]->Used)
        {
            for (int LineIdx = 0; LineIdx < BScanLinesList[BScanLines_idx]->BScanLineCount; LineIdx++)
            {
                TRect rect = BScanLinesList[BScanLines_idx]->LineRect[LineIdx];
                TBitmap* buffer = BScanLinesList[BScanLines_idx]->Buffer;
                buffer->Canvas->Font->Color = clBlack;

                HRGN ClipRgn = ::CreateRectRgn(rect.left,rect.top,
                                        rect.right,rect.bottom);
                ::SelectClipRgn(buffer->Canvas->Handle,ClipRgn);

                CID id = -1;
                for (int BtnIdx = 0; BtnIdx < BScanLinesList[BScanLines_idx]->BtnColumnCount * 2; BtnIdx++)
                {
                    if (!BScanLinesList[BScanLines_idx]->BtnVisible[BtnIdx][LineIdx])
                        continue;

                    id = BScanLinesList[BScanLines_idx]->BtnId[BtnIdx][LineIdx];

                    for(unsigned int i = 0; i < pCompareData->regions.size(); i++)
                    {
                        if(!pCompareData->regions[i].hasChannel(id))
                            continue;

                        const PolyReg& poly = pCompareData->regions[i].poly;
                        bool bPolyIsValid = poly.isValid();

                        double totErr = 0;
                        bool bNoError = true;
                        for(unsigned int j = 0; j < pCompareData->regions[i].measureData.size(); j++)
                        {
                            if(pCompareData->regions[i].measureData[j].errorValue < 0)
                                continue;
                            totErr += pCompareData->regions[i].measureData[j].errorValue;
                            bNoError = false;
                        }

                        const int polyPtRad = 3;
                        for(unsigned int j = 0; j < poly.points.size(); j++)
                        {
                            unsigned int nextPtIdx = j + 1;
                            if(nextPtIdx >= poly.points.size())
                                nextPtIdx -= poly.points.size();

                            PolyPt ptStart = SysToScreenCrd(poly.points[j], id - AutoconStartCID, rect);
                            PolyPt ptEnd = SysToScreenCrd(poly.points[nextPtIdx], id - AutoconStartCID, rect);

                            if(bNoError || (totErr < 0.2) || true) //+hack
                            {
                                buffer->Canvas->Pen->Width = 1;
                                buffer->Canvas->Pen->Style = psSolid;
                                buffer->Canvas->Pen->Color = bPolyIsValid ? clBlack : clRed;
                            }
                            else
                            {
                                buffer->Canvas->Pen->Width = 3;
                                buffer->Canvas->Pen->Style = psSolid;
                                buffer->Canvas->Pen->Color = clBlack;
                            }

                            buffer->Canvas->MoveTo(ptStart.x,ptStart.y);
                            buffer->Canvas->LineTo(ptEnd.x,ptEnd.y);


                            if(!bPolyIsValid || (j == 0))
                            {
                                buffer->Canvas->Brush->Color = clRed;
                                buffer->Canvas->Ellipse(ptStart.x - polyPtRad, ptStart.y - polyPtRad,
                                                            ptStart.x + polyPtRad, ptStart.y + polyPtRad);
                            }
                        }

                        buffer->Canvas->Pen->Color = clBlack;
                        buffer->Canvas->Pen->Width = 1;
                        buffer->Canvas->Pen->Style = psSolid;

                        PolyPt center = SysToScreenCrd(poly.center(), id - AutoconStartCID, rect);
                        TRect textRect(center.x - 10, center.y - 10, center.x + 10, center.y + 10);

                     /* //FDV
                        if(!bNoError)
                        {
                            UnicodeString str = StringFormatU(L"%.1f%%", totErr * 100);
                            buffer->Canvas->Brush->Style = bsClear;
                            buffer->Canvas->TextRect(textRect, str, tf);
                        }
                    */
                    }

                }

                ::SelectClipRgn(buffer->Canvas->Handle,NULL);
                DeleteObject(ClipRgn);
            }

        }
}

void cBScanDraw::SetMeasureData(BScanMeasure* pSnapshot)
{
    pCompareData = pSnapshot;
};

void cBScanDraw::EnableMeasureDataDraw(bool bVal)
{
    bEnableCompareDataDraw = bVal;
}
