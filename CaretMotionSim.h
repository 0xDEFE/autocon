//---------------------------------------------------------------------------

#ifndef CaretMotionSimH
#define CaretMotionSimH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Buttons.hpp>
//---------------------------------------------------------------------------

#include <vector>
#include "Utils.h"

#include "controllers/cavtk2.h"
#include "CriticalSection_Win.h"

enum eCMS_Mode
{
    CMS_MODE_SEARCH_TEST,
    CMS_MODE_FREE_MOTION,
};

struct sCaretData
{
    bool bActive;
    float pos;
    int dir;
    float pe;
    float target;
};

struct sCaretMotion
{
    sCaretMotion()
    {
        topOffset = btmOffset = 0;
        bIsAbsOffsetTop = bIsAbsOffsetBtm = false;
    };

    sCaretMotion( int topOffset, int btmOffset, bool bIsAbsOffsetTop = true,bool bIsAbsOffsetBtm=true)
    {
        this->topOffset = topOffset;
        this->btmOffset = btmOffset;
        this->bIsAbsOffsetTop = bIsAbsOffsetTop;
        this->bIsAbsOffsetBtm = bIsAbsOffsetBtm;
    };

    int topOffset;
    int btmOffset;
    bool bIsAbsOffsetTop;
    bool bIsAbsOffsetBtm;
};

void CALLBACK CaretMotionSimOnTimer(HWND v1, UINT v2, UINT_PTR v3, DWORD v4);

class cCaretMotionSim : public Singleton<cCaretMotionSim>
{
public:
    cCaretMotionSim();
    ~cCaretMotionSim()
    {
        //KillTimer(NULL, timerId);
        //delete pMotionTimer;
    };

    void initSearchMotionTable();

    void moveCaretsAbs(int topOffset, int btmOffset);
    void moveCaretsRel(int topOffset, int btmOffset);
    void setCallback(tCallBack proc);

    int getCariagesStatus(tCARIAGESTAT *pDest);
    int motionStart(tCallBack proc);
    int motionStart2(int bottomTrolleyShift, int upperTrolleyShift, tCallBack proc);
    int trolleysReturn(tCallBack proc);
    int motionBreak(tCallBack proc);

    void SetPathEncoderData(char PathEncoderIdx, int NewValue);
    int  GetPathEncoderData(char PathEncoderIdx);
    int  GetPathEncoderDir(char PathEncoderIdx);
    void ResetPathEncoder();

    bool waitStop();

    void __fastcall OnMovementTimer(TObject *Sender);
protected:

    bool processCarets();
    void onStartMovement();
    void onEndMovement();
private:
    eCMS_Mode mode;
    sCaretData carets[2];
    TTimer* pMotionTimer;
    HANDLE hCaretMoveEndEvent;
    tCallBack endMovementCallback;
    std::vector<sCaretMotion> motionsTable;
    int iCurrMotion;

    UINT_PTR timerId;


    cCriticalSectionWin mainLock;
};



#endif

