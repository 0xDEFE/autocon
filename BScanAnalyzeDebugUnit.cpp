//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BScanAnalyzeDebugUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cspin"
#pragma resource "*.dfm"
TBScanAnalyzeDebugForm *BScanAnalyzeDebugForm;
//---------------------------------------------------------------------------
__fastcall TBScanAnalyzeDebugForm::TBScanAnalyzeDebugForm(TComponent* Owner)
	: TForm(Owner)
{
}

const wchar_t *GetWC(const char *c)
{
	const size_t cSize = strlen(c)+1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs (wc, c, cSize);

	return wc;
}

//---------------------------------------------------------------------------
void __fastcall TBScanAnalyzeDebugForm::FormShow(TObject *Sender)
{
	sChannelDescription ChanDesc;
	UnicodeString tmp;
	tChannelName *name;

	ChList->Items->Clear();
	for (int id = 0x3F; id < 0x96; id++)
	{
		AutoconMain->Table->ItemByCID(id, &ChanDesc);
		name = AutoconMain->Table->GetChangeTitle(id);
		ChList->Items->AddObject(WideString(*name) + " " + IntToStr(id - AutoconStartCID), (TObject*)(id));

//		    Memo1->Lines->Add(Format("���������� - %s", ARRAYOFCONST((Text))));

	}
	ChList->ItemIndex = 0;
	ChList->ItemIndex = 93 - 63 + 5;
}

//---------------------------------------------------------------------------

//float Angle = - 45;
bool Break_ = false;

void TBScanAnalyzeDebugForm::DrawSignals(int SysCoord, CID Channel, PsScanSignalsOneCrdOneChannel Sgl)
{
	bool Flg;
	float x;
	float L;
    int tmp;

	if (!Sgl) return;

	if (Channel + AutoconStartCID  != (int)ChList->Items->Objects[ChList->ItemIndex]) return;
	for (int idx = 0; idx < Sgl->Count; idx++)
	{
		//�������� - ������ ���� ������
		Flg = false;
		for (int aidx = 0; aidx < 24; aidx++)
			if (Sgl->Signals[idx].Ampl[aidx] >= Th)
			{
				Flg = true;
				break;
			}

		//���� ���� ������ - ������
		if (Flg) BScanSeries->AddXY(SysCoord, Sgl->Signals[idx].Delay);

//		L = Sgl->Signals[idx].Delay / cos((float)AngleEdit->Value / (float)180 * 3.14);
//		x = sqrt( pow(L, 2) -  pow(Sgl->Signals[idx].Delay, 2));
//		x = Sgl->Signals[idx].Delay / (tan((float)90 - (float)AngleEdit->Value / (float)180 * 3.14));

        if (TryStrToInt(AngleEdit->Text, tmp))
  	        x = Sgl->Signals[idx].Delay / (tan( (float)AngleEdit->Value / (float)180 * 3.14)); else x = 0;

//		x = sqrt( pow(Sgl->Signals[idx].Delay * cos(Angle / 180 * 3.14), 2) -  pow(Sgl->Signals[idx].Delay, 2));

        if (Flg)
            if (TryStrToInt(SumLenEdit->Text, tmp))
                for (int ridx = std::max((float)0, x - SumLenEdit->Value); ridx < std::min((float)2000, x + SumLenEdit->Value); ridx++)
                    for (int aidx = 0; aidx < 24; aidx++)
                        ResGis[SysCoord + ridx] += Sgl->Signals[idx].Ampl[aidx];

		Break_ = true;

	}
}

void __fastcall TBScanAnalyzeDebugForm::Button1Click(TObject *Sender)
{

	sChannelDescription ChanDesc;
    AutoconMain->Table->ItemByCID((int)ChList->Items->Objects[ChList->ItemIndex], &ChanDesc);

    if ((ChanDesc.cdEnterAngle == 65) && (ChanDesc.cdDir == cdZoomIn)) AngleEdit->Value = 31;
    if ((ChanDesc.cdEnterAngle == 65) && (ChanDesc.cdDir == cdZoomOut)) AngleEdit->Value = 149;
    if ((ChanDesc.cdEnterAngle == 45) && (ChanDesc.cdDir == cdZoomIn)) AngleEdit->Value = 37;
    if ((ChanDesc.cdEnterAngle == 45) && (ChanDesc.cdDir == cdZoomOut)) AngleEdit->Value = 143;
    if ((ChanDesc.cdEnterAngle == 58) && (ChanDesc.cdDir == cdZoomIn)) AngleEdit->Value = 20;
    if ((ChanDesc.cdEnterAngle == 58) && (ChanDesc.cdDir == cdZoomOut)) AngleEdit->Value = 160;

//-----------------------------------

	BScanSeries->Clear();
	Series1->Clear();

	memset(&ResGis[0], 0, sizeof(ResGis));

	PsScanSignalsOneCrdOneChannel sgl;
	int SysCoord = 0;
	CID ChannelId = 0;
	bool Result;
	Break_ = false;

 /*
	sgl = Rep->GetFirstScanSignals(SysCoord, ChannelId, &Result);
	if (Result)
	{
		if (sgl->Count > 0) sgl->Signals[0].Delay = 250;
		DrawSignals(SysCoord, ChannelId, sgl);
	}
	if (!Break_)
		while (true)
		{
			sgl = Rep->GetNextScanSignals(&SysCoord, &ChannelId, &Result);
			if (sgl->Count > 0) sgl->Signals[0].Delay = 250;
			if (!Result) break;
			DrawSignals(SysCoord, ChannelId, sgl);
			if (Break_) break;
		}
 */

	sgl = Rep->GetFirstScanSignals(SysCoord, ChannelId, &Result);
	if (Result) DrawSignals(SysCoord, ChannelId, sgl);
	while (true)
	{
		sgl = Rep->GetNextScanSignals(&SysCoord, &ChannelId, &Result);
		if (!Result) break;
		DrawSignals(SysCoord, ChannelId, sgl);
	}

    int MaxGis = 0;
	for (int idx = 0; idx < 2000; idx++)
    {
		Series1->AddXY(idx, ResGis[idx]);
        MaxGis = std::max(ResGis[idx], MaxGis);
    }

	Series2->X0 = ChartPosX;
	Series2->Y0 = ChartPosY;
	Series2->X1 = ChartPosX + 1000 * cos((float)AngleEdit->Value / (float)180 * 3.14);
	Series2->Y1 = ChartPosY - 1000 * sin((float)AngleEdit->Value / (float)180 * 3.14);

	Series3->X0 = ChartPosX + ChartPosY / (tan( (float)AngleEdit->Value / (float)180 * 3.14));
	Series3->Y0 = 0;
	Series3->X1 = Series3->X0;
	Series3->Y1 = MaxGis;

    Chart2->LeftAxis->SetMinMax(0, MaxGis);
    ResEdit->Value = ResGis[(int)Series3->X0];
}
//---------------------------------------------------------------------------


void __fastcall TBScanAnalyzeDebugForm::ThTrackBarChange(TObject *Sender)
{
	UnicodeString TextList[17] = {"����", "-12", "-10", "-8", "-6", "-4", "-2", "0", "2", "4", "6", "8", "10", "12", "14", "16", "18"};
	int ValList[17] = {0, 8, 10, 12, 16, 20, 25, 32, 40, 50, 64, 80, 101, 128, 160, 202, 254};
	ThLabel->Caption = "����� �����������: " + TextList[ThTrackBar->Position];
	if (ThTrackBar->Position != 0) ThLabel->Caption = ThLabel->Caption + " ��";

	Th = ValList[ThTrackBar->Position];
	Button1Click(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TBScanAnalyzeDebugForm::ChListChange(TObject *Sender)
{
	Button1Click(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TBScanAnalyzeDebugForm::Chart1Zoom(TObject *Sender)
{
	float DXDY = (Chart1->BottomAxis->Maximum - Chart1->LeftAxis->Minimum) / (Chart1->LeftAxis->Maximum - Chart1->BottomAxis->Minimum);
	RatioLabel->Caption = Format("DX / DY = %3.1f; Atan = %3.1f", ARRAYOFCONST((DXDY, atan(DXDY) * 180 / 3.14)));
}
//---------------------------------------------------------------------------



//	int DelayHeight = LinkData[Channel].ChDesc.cdBScanGate.gEnd - LinkData[Channel].ChDesc.cdBScanGate.gStart;
//	int Height = LinkData[Channel].Rt.Height();
//    int aidx = 11;
//	bool Flg = false;
	//Update channels gates
//    int stGate = 0;
//    int edGate = 0;

	//Updating channel gates - if channel draws as LINEDRAW_SMALL_LINE
/*	if(LinkData[Channel].drawType == LINEDRAW_SMALL_LINE)
	{
		for(int i = 0; i < 9; i++)
		{
			if(channels_gates_temp[i][0] == LinkData[Channel].ChDesc.id)
			{
				stGate = channels_gates_temp[i][1];
				edGate = channels_gates_temp[i][2];
			}
		}
	}
			*/
	//��� �� �� �������� �� ������� ������� ���������
/*	HRGN ClipRgn;
	if( LinkData[Channel].Used )
	{
		ClipRgn = ::CreateRectRgn(LinkData[Channel].Rt.left,LinkData[Channel].Rt.top,
									LinkData[Channel].Rt.right,LinkData[Channel].Rt.bottom);
		::SelectClipRgn(LinkData[Channel].Buffer->Canvas->Handle,ClipRgn);
	}

	if ((LinkData[Channel].Used) && (LinkData[Channel].Enabled))
*/
			//					x = SysCoord; // / Rep->Header.MaxSysCoord;

//			float RealDelay = float(Sgl->Signals[idx].Delay) / float(LinkData[Channel].ChDesc.cdBScanDelayMultiply);
/*			if (((RealDelay >= LinkData[Channel].ChDesc.cdBScanGate.gStart) &&
						(RealDelay <= LinkData[Channel].ChDesc.cdBScanGate.gEnd)) || bDisableAScanStrobeCheck)
			{
  */			/*
				//�������� - ���� ������ ������������� ��� LINEDRAW_SMALL_LINE
					//�� �� �������� ���� �� �� � ������
				if((LinkData[Channel].drawType == LINEDRAW_SMALL_LINE) && (!bDisableAScanStrobeCheck))
				{
					if((RealDelay < stGate) ||    //���� ������ �� � ������
						(RealDelay > edGate))
						continue;
				}

				//�������� - ������ ���� ������
				Flg = false;
				for (int aidx = 0; aidx < 24; aidx++)
					if (Sgl->Signals[idx].Ampl[aidx] >= Th)
					{
						Flg = true;
						break;
					}

				//���� ���� ������ - ������
				if (Flg)
				{      */
//					x = SysCoord; // / Rep->Header.MaxSysCoord;
//                    y = LinkData[Channel].Rt.Bottom - float(Height) * float(RealDelay - LinkData[Channel].ChDesc.cdBScanGate.gStart) / float(DelayHeight);

//                    if(Sgl->bIsAlarmed)
//                    {
					   ////LinkData[Channel].Buffer->Canvas->Brush->Color = LinkData[Channel].Color;
						//LinkData[Channel].Buffer->Canvas->Brush->Color = clBlack;
						//LinkData[Channel].Buffer->Canvas->FillRect(Rect(x, LinkData[Channel].Rt.Top + 1, x+1, LinkData[Channel].Rt.Top + 10)); // LinkData[Channel].Rt.Bottom - 1
//                    }
//					const int pointHalfHeight = std::max(1.0, ceil(128.f / float(DelayHeight)));
//					LinkData[Channel].Buffer->Canvas->Brush->Color = LinkData[Channel].Color;
/*
					if(LinkData[Channel].drawType == LINEDRAW_STD)
					{
						LinkData[Channel].Buffer->Canvas->FillRect(Rect(x-1, y-pointHalfHeight, x+1, y+pointHalfHeight));
					}
					else if( LinkData[Channel].drawType == LINEDRAW_SMALL_LINE )
					{
						int offset = 3;
						LinkData[Channel].Buffer->Canvas->FillRect(Rect(x - 1, LinkData[Channel].Rt.Top + offset, x + 1, LinkData[Channel].Rt.Bottom - offset));
					} */
//				}
		//    }
//		}
	  /*
	if( LinkData[Channel].Used )
	{
		::SelectClipRgn(LinkData[Channel].Buffer->Canvas->Handle,NULL);
		DeleteObject(ClipRgn);
	}   */
	 /*
int cBScanDraw::SysCoordToScreen(int SysCoord,CID Channel, TRect rect)
{
    return rect.Left + (rect.Right - rect.Left) * SysCoord / Rep->Header.MaxSysCoord;
};

int cBScanDraw::DelayToScreen(int Delay,CID Channel, TRect rect)
{
    int DelayHeight = LinkData[Channel].ChDesc.cdBScanGate.gEnd - LinkData[Channel].ChDesc.cdBScanGate.gStart;
	int Height = rect.Height();

    return rect.Bottom - float(Height) * float(Delay - LinkData[Channel].ChDesc.cdBScanGate.gStart) / float(DelayHeight);
};

int cBScanDraw::DelayToScreen(int Delay, unsigned int BScanLines_idx, int LineIdx)
{
    CID Channel = -1;
    for (int BtnIdx = 0; BtnIdx < BScanLinesList[BScanLines_idx]->BtnColumnCount * 2; BtnIdx++)
    {
        if (!BScanLinesList[BScanLines_idx]->BtnVisible[BtnIdx][LineIdx])
			continue;
        Channel = BScanLinesList[BScanLines_idx]->BtnId[BtnIdx][LineIdx];
        break;
	}

    return DelayToScreen(Delay, Channel  - AutoconStartCID, BScanLinesList[BScanLines_idx]->LineRect[LineIdx]) + BScanLinesList[BScanLines_idx]->BoundsRect.Top;
};

int cBScanDraw::ScreenToDelay(int Y, unsigned int BScanLines_idx, int LineIdx)
{
    CID Channel = -1;

    for (int BtnIdx = 0; BtnIdx < 6; BtnIdx++)
    {
		if (!BScanLinesList[BScanLines_idx]->BtnVisible[BtnIdx][LineIdx])
            continue;
        Channel = BScanLinesList[BScanLines_idx]->BtnId[BtnIdx][LineIdx];
        break;
    }

    TRect rect = BScanLinesList[BScanLines_idx]->LineRect[LineIdx];
    int DelayHeight = LinkData[Channel - AutoconStartCID].ChDesc.cdBScanGate.gEnd - LinkData[Channel - AutoconStartCID].ChDesc.cdBScanGate.gStart;
    int strobeStart = LinkData[Channel - AutoconStartCID].ChDesc.cdBScanGate.gStart;
    int Height = rect.Height();

    return ((float(rect.Bottom - (Y - BScanLinesList[BScanLines_idx]->BoundsRect.Top)) * float(DelayHeight)) / float(Height)) + strobeStart;
};

*/


void __fastcall TBScanAnalyzeDebugForm::Chart1Click(TObject *Sender)
{
    ChartPosX = Chart1->BottomAxis->CalcPosPoint(ChartX);
    ChartPosY = Chart1->LeftAxis->CalcPosPoint(ChartY);
	Button1Click(Sender);
}

//---------------------------------------------------------------------------

void __fastcall TBScanAnalyzeDebugForm::Chart1MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y)
{
    ChartX = X;
    ChartY = Y;
}
//---------------------------------------------------------------------------

