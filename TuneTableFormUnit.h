//---------------------------------------------------------------------------

#ifndef TuneTableFormUnitH
#define TuneTableFormUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Samples.Spin.hpp>
#include <Vcl.Buttons.hpp>
#include <VCLTee.TeCanvas.hpp>
#include <Vcl.ToolWin.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------

#include <vector>

enum eTuneTableEditingState
{
    TT_STATE_VIEW,
    TT_STATE_EDIT,
    TT_STATE_ADD
};

struct sTuneTableItem
{
    sTuneTableItem()
    {
        number = 0;
        nGroup = 0;
        model = "Unknown";
        scanStart[0] = scanStart[1] = scanDelta[0] = scanDelta[1] = 0;
        strobeStart = strobeEnd = 0;
        caretEnabled[0] = caretEnabled[1] = false;
    };

    sTuneTableItem(const sTuneTableItem& other)
    {
        (*this) = other;
    };

    sTuneTableItem& operator=(const sTuneTableItem& other)
    {
        this->number = other.number;
        this->nGroup = other.nGroup;
        this->channelIndices = other.channelIndices;
        this->model = other.model;
        this->info = other.info;
        this->scanStart[0] = other.scanStart[0];
        this->scanStart[1] = other.scanStart[1];
        this->scanDelta[0] = other.scanDelta[0];
        this->scanDelta[1] = other.scanDelta[1];
        this->caretEnabled[0] = other.caretEnabled[0];
        this->caretEnabled[1] = other.caretEnabled[1];
        this->strobeStart = other.strobeStart;
        this->strobeEnd = other.strobeEnd;
        return *this;
    }

    unsigned int number;
    unsigned int nGroup;
    std::vector<unsigned int> channelIndices;
    UnicodeString model;
    UnicodeString info;
    bool caretEnabled[2];
    int scanStart[2];
    int scanDelta[2];
    unsigned int strobeStart;
    unsigned int strobeEnd;
};

enum eTuneItemStatus
{
    TIS_READY,
    TIS_WRONG_GROUP,
    TIS_NO_CHANNELS,
    TIS_CHANNELS_FROM_DIFFERENT_GROUPS,
    TIS_NO_MODEL,
    TIS_NO_CARET_MOVEMENT,
    TIS_WRONG_SCAN_START,
    TIS_WRONG_SCAN_DELTA,
    TIS_WRONG_START_STROBE,
    TIS_WRONG_END_STROBE
};

bool isItemStatusCritical( eTuneItemStatus stat )
{
    if( (stat == TIS_READY) || (stat == TIS_NO_MODEL) )
        return false;
    return true;
}
UnicodeString ItemStatusToString( eTuneItemStatus stat )
{
    switch(stat)
    {
        case TIS_READY:                 return "Ok";
        case TIS_WRONG_GROUP:           return "�������� ������ �������";
        case TIS_NO_CHANNELS:           return "�� ������� ������������ ������";
        case TIS_CHANNELS_FROM_DIFFERENT_GROUPS:    return "������������ ������ �� ������������� ��������� ������";
        case TIS_NO_MODEL:              return "�� ������� ������ �������";
        case TIS_NO_CARET_MOVEMENT:     return "����������� �������� �������";
        case TIS_WRONG_SCAN_START:      return "����������� ������� ��������� ���������� ��������";
        case TIS_WRONG_SCAN_DELTA:      return "����������� ������� ������";
        case TIS_WRONG_START_STROBE:    return "�������� �������� ������ ������";
        case TIS_WRONG_END_STROBE:      return "�������� �������� ����� ������";
    }
    return "����������� ������ (��?)";
}

class TTuneTableForm : public TForm
{
__published:	// IDE-managed Components
    TStringGrid *m_GridTable;
    TPanel *m_EditPanel;
    TLabel *Label1;
    TSpinEdit *m_PosTopSpin;
    TTrackBar *m_PosTopTrack;
    TLabel *Label2;
    TSpinEdit *m_PosBtmSpin;
    TTrackBar *m_PosBtmTrack;
    TLabel *Label3;
    TSpinEdit *m_DeltaTopSpin;
    TTrackBar *m_DeltaTopTrack;
    TSpinEdit *m_DeltaBtmSpin;
    TTrackBar *m_DeltaBtmTrack;
    TLabel *Label4;
    TLabel *Label5;
    TLabel *Label6;
    TSpinEdit *m_StrobeStartSpin;
    TSpinEdit *m_StrobeEndSpin;
    TTrackBar *m_StrobeEndTrack;
    TTrackBar *m_StrobeStartTrack;
    TComboFlat *m_GroupCombo;
    TLabel *Label7;
    TEdit *m_ChannelsEdit;
    TLabel *Label8;
    TEdit *m_ModelEdit;
    TGridPanel *GridPanel1;
    TGridPanel *GridPanel2;
    TGridPanel *GridPanel3;
    TGridPanel *GridPanel4;
    TGridPanel *GridPanel5;
    TGridPanel *GridPanel6;
    TGridPanel *GridPanel7;
    TSpeedButton *m_SelChannelsBtn;
    TSpeedButton *m_TableMovLeftHiBtn;
    TSpeedButton *m_AddBtn;
    TSpeedButton *m_TableMovRightHiBtn;
    TSpeedButton *m_TableMovRightLowBtn;
    TSpeedButton *m_TableMovLeftLowBtn;
    TToolBar *m_MainToolBar;
    TToolButton *m_LoadBtn;
    TToolButton *ToolButton2;
    TToolButton *m_SaveBtn;
    TToolButton *ToolButton7;
    TToolButton *m_ClearBtn;
    TToolButton *ToolButton8;
    TToolButton *m_BuildBtn;
    TToolButton *ToolButton9;
    TGridPanel *GridPanel8;
    TGridPanel *GridPanel9;
    TSpeedButton *m_RemoveBtn;
    TSpeedButton *m_ApplyBtn;
    TSpeedButton *m_CancelBtn;
    TToolButton *m_CloseBtn;
    TImageList *ImageList1;
    TOpenDialog *OpenDialog1;
    TSaveDialog *SaveDialog1;
    TPanel *Panel1;
    TMemo *m_InfoMemo;
    TToolButton *m_OptimizeBtn;
    TToolButton *ToolButton3;
    TGroupBox *m_TopCaretGroupBox;
    TGroupBox *m_BotCaretGroupBox;
    TGridPanel *GridPanel10;
    TSpeedButton *m_EnableTopCaret;
    TSpeedButton *m_EnableBotCaret;
    TGroupBox *GroupBox1;
    TFlowPanel *FlowPanel1;
    TGroupBox *GroupBox2;
    TMemo *m_ChInfoMemo;
    void __fastcall m_GridTableSelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
    void __fastcall m_GridTableDrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State);
    void __fastcall OnTableMove(TObject *Sender);
    void __fastcall m_GridTableMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
    void __fastcall OnEditPanelSpinChanged(TObject *Sender);
    void __fastcall m_AddBtnClick(TObject *Sender);
    void __fastcall m_CancelBtnClick(TObject *Sender);
    void __fastcall m_ApplyBtnClick(TObject *Sender);
    void __fastcall m_RemoveBtnClick(TObject *Sender);
    void __fastcall m_CloseBtnClick(TObject *Sender);
    void __fastcall m_ClearBtnClick(TObject *Sender);
    void __fastcall m_SaveBtnClick(TObject *Sender);
    void __fastcall m_LoadBtnClick(TObject *Sender);
    void __fastcall m_SelChannelsBtnClick(TObject *Sender);
    void __fastcall m_BuildBtnClick(TObject *Sender);
    void __fastcall m_OptimizeBtnClick(TObject *Sender);
    void __fastcall m_EnableTopCaretClick(TObject *Sender);
    void __fastcall m_EnableBotCaretClick(TObject *Sender);

private:	// User declarations
    bool bEditMode;
    eTuneTableEditingState editingState;
    int tmpSelCol;
    bool bTmpColIsSelected;

    UnicodeString currentFileName;

    std::vector<sTuneTableItem> tableItems;

    void UpdateTable();
    void UpdateInfo();
    int TableColToItem(int colIndex) {return colIndex - 1;};
    int TableItemToCol(int itemIndex) {return itemIndex + 1;};
    void SetTableItem(int colIndex, sTuneTableItem& item);
    void SetEditPanelItem(sTuneTableItem& item);
    sTuneTableItem GetEditPanelItem();
    void UpdateEditPanel(bool bFromTableToPanel);
    void RemoveTableItem(unsigned int colIndex);
    sTuneTableItem* GetTableItem(unsigned int colIndex);
    eTuneItemStatus CheckItem(sTuneTableItem& item);






    void OptimizeRoutes();

    void setEditingState(eTuneTableEditingState newState);
    //void updateEditPanel(bool bFromTableToPanel);


    UnicodeString ConvertTextToTable(UnicodeString text);
    //int addTableItem( sTuneTableItem& item );
    //sTuneTableItem* getTableItem(unsigned int colIndex);
    //void updateTableItem(unsigned int colIndex, bool bFromTableToStruct );
    //void removeTableItem(unsigned int colIndex);



    void adjustTableColumnsWidth();

public:		// User declarations
    __fastcall TTuneTableForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TTuneTableForm *TuneTableForm;
//---------------------------------------------------------------------------
#endif
