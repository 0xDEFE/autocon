﻿/**
 * @file BScanPainter.h
 * @author Kirill Belyaevskiy
 */

#ifndef BScanPainterH
#define BScanPainterH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>


#include <Vcl.AppEvnts.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Samples.Spin.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.Series.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <VCLTee.TeeShape.hpp>
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.Graphics.hpp>
#include <Bde.DBTables.hpp>
#include <Data.DB.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Tabs.hpp>
#include <VCL.Forms.hpp>
#include <VclTee.TeeGDIPlus.hpp>


#include "BScanLines.h"
#include "BScanDraw.h"
#include "DrawRailUnit.h"

/** \enum eBScanVisualMode
 *  Режим отображения В-развертки
 */
enum eBScanVisualMode
{
	BS_VISUAL_UNKNOWN = 0, //!< Не известен
	BS_VISUAL_SEARCH,      //!< Поиск
	BS_VISUAL_TUNE         //!< Настройка
};

/** \enum eChannelHideMode
 *  Режим сокрытия каналов
 */
enum eChannelHideMode
{
    CFM_HIDE_NONE,              //!< Подсвечивать все
    CFM_HIDE_INACTIVE,          //!< Скрыть не входящие в текущую группу сканирования
    CFM_HIDE_WITH_NO_DEFECTS,   //!< Скрыть каналы без дефектов
    CFM_HIDE_ALL,               //!< Скрыть все
    CFM_HIDE_SELECTED,          //!< Скрыть выбранные при помощи флага cBScanLines::BtnChannelIsActive
};

/** \enum eBScanLayout
 *  Режим размещения каналов
 */
enum eBScanLayout
{
    BSL_ALL_IN_ONE,             //!< Размещать по стандартной схеме
    BSL_ONLY_CONTROL_CHANNELS   //!< Размещать один над другим
};

/** \enum eBScanLayout
 *  Режим отображения каналов
 */
enum eBScanVisibleChannels
{
    BSVC_ALL_CHANNELS,          //!< Показывать все
    BSVC_ONLY_CONTROL_CHANNELS  //!< Показывать только каналы контроля
};

//! Класс предназначен для рисования в-развертки в различных режимах
class cBScanPainter
{
public:
	cBScanPainter()
	{
		prevUpdateWorkCycle = -1;
		currVisualMode = BS_VISUAL_UNKNOWN;
		bLeftToRightBScanLayout = true;
		bLeftToRightRailLayout = true;
		pScanDraw = NULL;
		bInitialized = false;
        Rep = NULL;
		memset( scanLines, 0, sizeof(cBScanLines*)*9);
	}

	virtual ~cBScanPainter();

    //! Инициализация класса
    /** \param Rep Указатель на отчет, который надо визуализировать
        \param layoutType Тип лейаута
        \param visibleChannelsType Видимые каналы
     */
	void init(cJointTestingReport* Rep, eBScanLayout layoutType = BSL_ALL_IN_ONE, eBScanVisibleChannels visibleChannelsType = BSVC_ALL_CHANNELS);
    bool isInitialized() {return bInitialized;};

    //! Установка лейаута
    void setBScanLayout(eBScanLayout type);
    //! Применение лейаута
	void applyBScanLayout();

	void setVisualMode(eBScanVisualMode mode) {currVisualMode = mode;};

    //! Очистка в-развертки
	void clearBScan();

    //! Обновление состояния подсветки. Перерисовка в-развертки, рельса.
    /** \param bNewItemsFlag Перерисовать только новые сигналы */
	void updateBScan(bool bNewItemsFlag);
    //! Перерисовка в-развертки (всех страниц).
    /** \param bNewItemsFlag Перерисовать только новые сигналы */
	void updateBScanPages(bool bNewItemsFlag);
    //! Перерисовка в-развертки (определенной страницы).
    /** \param idx Номер страницы
        \param bNewItemsFlag Перерисовать только новые сигналы
        \param bUpdateSearchHighlight Обновить подсветку для режима поиск
        \param bUpdateTuneHighlight Обновить подсветку для режима настройка
     */
	void updateBScanPage(int idx, bool bNewItemsFlag, bool bUpdateSearchHighlight = false, bool bUpdateTuneHighlight = false);
    //! Перерисовка рельса
	void updateRail();

    //Highlight update
    void updateHightlight(bool bForceUpdate = false);
    void updateBScanHideChannelsMode();
	void updateTuneHighlight(int idx);
	void updateSearchHighlight(int idx);

    //! Отрисовка всего в буфер
	void drawBScan(TCanvas* pCanvas);
    //! Отрисовка страницы в буфер
	void drawBScanPage(unsigned int nPage, TCanvas* pCanvas);
    //! Отрисовка рельса в буфер
    /** \param UnvisibleFlags Не используется
        \param bDrawHardpoints Отрисовывать точки соединения
    */
	void drawRail(UINT UnvisibleFlags,bool bDrawHardpoints, TCanvas* pCanvas);

    //! Установка прямоугольника, в котором будет рисоваться в-развертка
	void setBScanRect(TRect rect);
    //! Установка ориентации в-развертки (справа-налево или слева направо)
	void setBScanOrientation(bool bLeftToRight);
    //! Установка прямоугольника, в котором будет рисоваться рельс
	void setRailRect(TRect rect);
    TRect getRailRect() {return RailRect;}
    //! Установка ориентации рельса (справа-налево или слева направо)
	void setRailOrientation(bool bLeftToRight);
    //! Установка прямоугольника, в котором будет рисоваться страница в-развертки
	TRect setBScanPageRect(TRect rect, unsigned int nPage);
    //! Обработка нажатия мыши
	int click(unsigned int nPage, int x, int y);

    //! Установка состояния подсветки канала
    void setChannelHide(unsigned int chId, bool bHide);
    //! Установка состояния подсветки всех каналов
    void setAllChannelsHide(bool bVal);

    //! Установка типа подсветки каналов
    void setHideChannelsMode(eChannelHideMode mode);

	bool getBScanOrientation() {return bLeftToRightBScanLayout;};
	bool getRailOrientation() {return bLeftToRightRailLayout;};
protected:
	void _initBScanPages_AllChannels();
    void _initBScanPages_OnlyControl();
	void _applyBScanLayout_AllIn();
    void _applyBScanLayout_OnlyControl();



private:
    eBScanLayout layoutType;
    eBScanVisibleChannels visibleChannelsType;
	int prevUpdateWorkCycle;
	eBScanVisualMode currVisualMode;
	bool bLeftToRightBScanLayout;
	bool bLeftToRightRailLayout;
	TRect RailRect;
	TRect BScanRect;
	bool bInitialized;
    int prevScanChannelGroup;
    //bool bEnableActiveChannelsHighlight;
    eChannelHideMode chanHideMode;

    cJointTestingReport* Rep;

    std::vector<bool> chHideState;

    bool bNeedBufferRedraw[9];
public:
	cDrawRailUnit railDraw;     //!< Класс отрисовки рельса
	cBScanLines* scanLines[9];  //!< Классы отрисовки страниц в-развертки по колесам (1-8)
	cBScanDraw* pScanDraw;      //!< Класс отрисовки сигналов в-развертки
};
#endif
