//---------------------------------------------------------------------------

#ifndef BScanMeasureViewResUnitH
#define BScanMeasureViewResUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.CheckLst.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TBScanMeasureViewRes : public TForm
{
__published:	// IDE-managed Components
	TCheckListBox *List;
	TPanel *Panel1;
	TLabel *Label1;
	TButton *Button1;
	TButton *Button2;
private:	// User declarations
public:		// User declarations
	__fastcall TBScanMeasureViewRes(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TBScanMeasureViewRes *BScanMeasureViewRes;
//---------------------------------------------------------------------------
#endif
