//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "LanProtUMU.h"

#include "LogUnit.h"
//---------------------------------------------------------------------------
//#pragma package(smart_init)
#pragma resource "*.dfm"
TLogForm *LogForm;
//---------------------------------------------------------------------------
__fastcall TLogForm::TLogForm(TComponent* Owner)
	: TForm(Owner)
{
	logger.setChannelMask(Logger::CHANNEL_ID_ALL);
}
//---------------------------------------------------------------------------
void __fastcall TLogForm::Button12Click(TObject *Sender)
{
    Memo1->Lines->Clear();
    LogLines->Clear();
}

//---------------------------------------------------------------------------

void __fastcall TLogForm::CheckBox1Click(TObject *Sender)
{
	if (CheckBox1->Checked)
	{
	Tag  = 1;
		Width = SaveWidth;
		Height = SaveHeight;
			Tag = 0;
	}
	else
	{
		SaveWidth = Width;
		SaveHeight = Height;
		Tag  = 1;
		Width = 98;
		Height = 70;
		Tag = 0;
	};
/*
    MainForm->Timer2->Tag = 1;
    MainForm->Timer2Timer(NULL);
    MainForm->Timer2->Tag = 0;
*/
}
//---------------------------------------------------------------------------
void __fastcall TLogForm::FormCreate(TObject *Sender)
{
    Tag  = 1;
    SaveWidth = 950;
    SaveHeight = 190;
    Left = Screen->Width - Width - 25;
    Top = 05;
    LogLines = new TStringList();
    LANProtDebug::onAddLog = &AddLog;
}
void __fastcall TLogForm::FormDestroy(TObject *Sender)
{
    LANProtDebug::onAddLog = NULL;
    delete LogLines;
}
//---------------------------------------------------------------------------
void __fastcall TLogForm::FormResize(TObject *Sender)
{
    if (Tag == 1) { return;

    }
    SaveWidth = Width;
    SaveHeight = Height;

    ScrollBar1Change(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TLogForm::UpdateLogTimerTimer(TObject *Sender)
{
    if(!LoggerMsgBtn->Down)
        return;

	Logger::sLoggerMessage msg;
	while(logger.pop(msg))
	{
		std::wstring str = msg.toString().c_str();
		//str.TrimRight();
		if(str[str.size()-1]==L'\n')
			str[str.size()-1]=L' ';
//		Memo1->Lines->Add(str.c_str());
        LogLines->Add(str.c_str());

	}

	if(logger.isNewPersistendData())
	{
		Logger::sLoggerMessage* pMessages;
		UINT MsgSize = 0;

		if(logger.readPersistentData(&pMessages,&MsgSize,true))
		{
			while(PersistDataListBox->GetCount() != (int)MsgSize)
			{
				if( PersistDataListBox->GetCount() <= (int)MsgSize)
					PersistDataListBox->AddItem("",NULL);
			}

			for(unsigned int i=0;i<MsgSize;i++)
			{
				while(PersistDataListBox->GetCount() <= (int)i)
					PersistDataListBox->AddItem("",NULL);

				PersistDataListBox->Items->Strings[i] = pMessages[i].data.c_str();
				//PersistDataListBox->AddItem(pMessages[i].data.c_str(),NULL);
			}


			PersistDataListBox->Update();
            delete [] pMessages;
        }
	}

    updateUMULog();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------


void __fastcall TLogForm::ScrollBar1Change(TObject *Sender)
{
    HGDIOBJ OldFont;
    HDC Hand;
    TTextMetric TM;
    TRect Rect;
    int tempint;
    Hand = GetDC(Memo1->Handle);
    OldFont = SelectObject(Hand, Memo1->Font->Handle);
    GetTextMetrics(Hand, &TM);
    Memo1->Perform(EM_GETRECT, 0, (NativeInt)(&Rect));
    tempint = (Rect.Bottom - Rect.Top) / (TM.tmHeight + TM.tmExternalLeading);
    SelectObject(Hand, OldFont);
    ReleaseDC(Memo1->Handle, Hand);

    // --------------------

    if (ScrollBar1->Tag == 1) return;
    Memo1->Clear();
    Memo1->Lines->BeginUpdate();
     for (int i = 0; i < tempint; i++)
       if (LogLines->Count > i + ScrollBar1->Position)
         Memo1->Lines->Add(IntToStr(i + ScrollBar1->Position) + ". " + LogLines->Strings[i + ScrollBar1->Position]);
     Memo1->Lines->EndUpdate();

}
//---------------------------------------------------------------------------

void __fastcall TLogForm::AddText(UnicodeString Str)
{
	if (LogState->Down)
        LogLines->Add(Str);
}

UnicodeString GetUMUMessageName(unsigned char id) {
    switch(id) {
    case dAScanData: {
            return "�-���������";
        }
    case dAScanMeas: {
            return "������ �������� �-���������";
		}
    case dBScanData: {
            return "�-���������";
        }
    case dBScan2Data: {
            return "�-��������� (��� 2)";
        }
    case dMScanData: {
            return "�-���������";
        }
    case dAlarmData: {
            return "���";
        }
    case dPathStep: {
            return "������ ����";
        }
    case dPathEncoderSetup: {
            return "��������� �������� ������� ����";
        }
    case dVoltage: {
            return "U �����";
        }
    case dWorkTime: {
            return "����� ������ ���";
        }
    case dEnvelopeMeas: {
            return "�������� ��������� �������";
		}
    case dTestAScan: {
            return "������� �-���������";
        }
    case uStrokeCount   : { return "���������� ���������� ������"; }
    case uParameter     : { return "�������� �������� ��������� � ������� ������"; }
    case uGainSeg       : { return "�������� ��������� ������� ������ ���"; }
    case uGate          : { return "�������� ��������� ������"; }
    case uPrismDelay    : { return "�������� �������� � ������"; }
    case uEnableAScan   : { return "��������� �������� �-���������"; }
    case uPathSimulator : {	return "�������� ������� ����"; }
    case uEnable        : {	return "���������� ������� ���"; }
    case cDevice_       : { return "��������� � ������� ���������� � ����"; }
    case cFWver         : { return "������ � ���� �������� �����"; }
    case cSerialNumber  : {	return "�������� ����� ����������"; }
    default: {
            return "��������� ID";
        }
    }
}

int cnt = 0;
UnicodeString tmp;
UnicodeString tmp1;
void __fastcall TLogForm::AddLog(int UMUIdx, unsigned char * msg, int way)
{
    if (!LogState) return;
	if (!LogState->Down) return;

    if (way == 3)
    {
        LogLines->Add("������ ������ ������");
        return;
    };

	if (CheckBoxOneMessage->Checked) LogLines->Clear();

    if (!msg) return;

    // -----------------------------------------

	if (((way == 1) && (!LogIn->Down)) ||
		((way == 2) && (!LogOut->Down)) ||
		(((*msg) == 0x3D) && (!LogPathEncoder->Down)) ||
		(((*msg) == 0x82) && (!LogAScanMeasure->Down)) ||
		(((*msg) == 0x7F) && (!LogAScan->Down)) ||
		(((*msg) == 0x70) && (!LogBScan->Down)) ||
		(((*msg) == 0x72) && (!LogBScan2->Down)) ||
//        (((*msg) == 0x71) && (!LogMScan->Down)) ||
		(((*msg) == 0x83) && (!LogASD->Down)) ||
		(((*msg) == 0xDF) && (!LogFWver->Down)) ||
		(((*msg) == 0xDB) && (!LogSerialNumber->Down)) ||
		(((*msg) == uEnable) && (!UMUManage->Down))) return;

    switch(way)
    {

        case 1: {
                    tmp1 = "UMU " + IntToStr(UMUIdx) + " Recived";
                    break;
                };
		case 2: {
                    tmp1 = "UMU " + IntToStr(UMUIdx) + " Sended";
                    break;
                };
        case 3: {
					tmp = "ERROR";
					LogLines->Add(tmp);
                    return;
                };
    }


	LogLines->Add(" [" + GetUMUMessageName((msg[0])) + "]");
    tmp = "";
    for (int i = 0; i < 6; i++)
    {
		tmp = tmp + Format("%2x ", ARRAYOFCONST((msg[i])));
	}
	LogLines->Add(tmp1 + " HEAD:" + tmp);

	if  (!LogNoBody->Down)
	{
		tmp = "";
		for (int i = 0; i < (msg[2] | (msg[3] << 8)); i++)
		{
            tmp = tmp + Format("%2x ", ARRAYOFCONST((msg[6 + i])));
		}

		LogLines->Add(tmp1 + " BODY:" + tmp);

        if ((*msg) == 0x83)
        {
            tmp = "�����������: ";
		    LogLines->Add(tmp);
            int idx = 0;
            for (int i = 0; i < ((tLAN_Message*)msg)->Size / 2; i++)
            {
                tmp = "����: " + IntToStr(i);
		        LogLines->Add(tmp);
                tmp = "�������: 0; �����: 0;";
                tmp = tmp + Format("����� 0,1,2,3 - %d,%d,%d,%d ",
                                    ARRAYOFCONST(((int)((((tLAN_Message*)msg)->Data[idx] & 0x01) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x02) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x04) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x08) != 0))));
		        LogLines->Add(tmp);
                tmp = "�������: 0; �����: 1;";
                tmp = tmp + Format("����� 0,1,2,3 - %d,%d,%d,%d ",
                                    ARRAYOFCONST(((int)((((tLAN_Message*)msg)->Data[idx] & 0x10) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x20) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x40) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x80) != 0))));
                idx++;

		        LogLines->Add(tmp);
                tmp = "�������: 1; �����: 0;";
                tmp = tmp + Format("����� 0,1,2,3 - %d,%d,%d,%d ",
                                    ARRAYOFCONST(((int)((((tLAN_Message*)msg)->Data[idx] & 0x01) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x02) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x04) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x08) != 0))));
		        LogLines->Add(tmp);
                tmp = "�������: 1; �����: 1;";
                tmp = tmp + Format("����� 0,1,2,3 - %d,%d,%d,%d ",
                                    ARRAYOFCONST(((int)((((tLAN_Message*)msg)->Data[idx] & 0x10) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x20) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x40) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x80) != 0))));
                idx++;
		        LogLines->Add(tmp);
            }
        }

    }
}

void TLogForm::updateUMULog()
{
    if (!LogState) return;

    if (LastLogLinesCount == LogLines->Count) return;

    LastLogLinesCount = LogLines->Count;

    // -------------------- ����������� ���������� ����� � TMemo

    HGDIOBJ OldFont;
    HDC Hand;
    TTextMetric TM;
    TRect Rect;
    int tempint;
    Hand = GetDC(Memo1->Handle);
    OldFont = SelectObject(Hand, Memo1->Font->Handle);
    GetTextMetrics(Hand, &TM);
    Memo1->Perform(EM_GETRECT, 0, (NativeInt)(&Rect));
    tempint = (Rect.Bottom - Rect.Top) / (TM.tmHeight + TM.tmExternalLeading);
    SelectObject(Hand, OldFont);
    ReleaseDC(Memo1->Handle, Hand);

    // --------------------

    int TH = tempint;

    ScrollBar1->Tag = 1;
    ScrollBar1->Position = std::max(0, LogLines->Count - TH);
    ScrollBar1->Max = std::max(0, LogLines->Count - TH);
    ScrollBar1->Tag = 0;
    Memo1->Lines->BeginUpdate();
    Memo1->Clear();


//    int St = -1;
//    int Ed = -1;

    for (int i = 0; i < TH; i++)
      if (LogLines->Count > i + ScrollBar1->Position)
      {
  //      if (St == - 1) { St = i + ScrollBar1->Position; }
        Memo1->Lines->Add(IntToStr(i + ScrollBar1->Position) + ". " + LogLines->Strings[i + ScrollBar1->Position]);
//        Ed  = i + ScrollBar1->Position;
      }
    Memo1->Lines->EndUpdate();
}
//---------------------------------------------------------------------------

