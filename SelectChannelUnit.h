//----------------------------------------------------------------------------
#ifndef SelectChannelUnitH
#define SelectChannelUnitH
//----------------------------------------------------------------------------
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <StdCtrls.hpp>
#include <Controls.hpp>
#include <Forms.hpp>
#include <Graphics.hpp>
#include <Classes.hpp>
#include <SysUtils.hpp>
#include <Windows.hpp>
#include <System.hpp>
#include <Vcl.ComCtrls.hpp>

#include "AutoconMain.h"
#include "Utils.h"
#include <Vcl.CheckLst.hpp>
extern cAutoconMain *AutoconMain;

#include <set>

enum eSelectChannelMode
{
    SELM_NONE = -1,
	SELM_BY_CHANNEL = 0,
	SELM_BY_SCAN_GROUP,
	SELM_BY_KP,

};

enum eSortChannelMode
{
	SORTM_NONE = 0,
	SORTM_BY_CHANNEL,
	SORTM_BY_GROUP,
	SORTM_BY_KP,
	SORTM_BY_INDEX,
	SORTM_BY_NAME
};

//Adding new group types - here
//Setup group - in UpdateChannels()
enum eSortChannelGroupType
{
	GRT_NONE = 0,
	GRT_SCAN_GROUP,
	GRT_KP_GROUP,
    GRT_LAST
};

struct sSelChannelDesc
{
	int itemId;
	int channelId;
	//int channelIndex;
	//int scanGroupId;
	//int kpGroupId;

	UnicodeString name;
	UnicodeString title;
	int kpNumber;
	int scanGrNumber;

	int colomnIndex;
};

struct sSelGroupDesc
{
	sSelGroupDesc()
	{
		this->item_id=0;
		this->name="";
		this->type=GRT_SCAN_GROUP;
	};
	sSelGroupDesc(int item_id, const wchar_t* name, eSortChannelGroupType type)
	{
		this->item_id=item_id;
		this->name=name;
		this->type=type;
	};

	int item_id;
	UnicodeString name;
	eSortChannelGroupType type;

	std::set<UINT> groupChannels;
};

struct sSelListItemElemDesc
{
	sSelListItemElemDesc( bool bIsGroup,int channelId,int groupId)
	{
		this->bIsGroup = bIsGroup;
		this->channelId = channelId;
		this->groupId = groupId;
	}
	bool bIsGroup;
	int channelId;
	int groupId;
};

UnicodeString CIDArrayToStr(std::vector<UINT>& arr);
bool CIDStrToArray( UnicodeString str, std::vector<UINT>& arr);
//----------------------------------------------------------------------------
class TSelectChannelDial : public TForm
{
__published:
	TButton *OKBtn;
	TButton *CancelBtn;
	TButton *m_SingleMoveRightBtn;
	TButton *m_MultMoveRightBtn;
	TButton *m_SingleMoveLeftBtn;
	TButton *m_MultMoveLeftBtn;
	TComboBox *m_SortTypeComboSrc;
	TStaticText *StaticText3;
	TSplitter *Splitter1;
	TGridPanel *GridPanel1;
	TComboBox *m_GroupTypeComboSrc;
	TStaticText *StaticText6;
	TListBox *m_ChannelsListDst;
	TListBox *m_ChannelsListSrc;
	TGridPanel *GridPanel2;
	TComboBox *m_SortTypeComboDst;
	TStaticText *StaticText5;
	TStaticText *StaticText7;
	TComboBox *m_GroupTypeComboDst;
	TMemo *m_ChannelInfoMemo;
    TButton *m_PrintBtn;
    TGridPanel *GridPanel3;
    TPanel *m_ChannelsPanelSrc;
    TPanel *m_SelectBtnsPanel;
    TPanel *m_ChannelsPanelDst;
    TLabel *Label1;
    TLabel *Label2;
    TGridPanel *GridPanel4;
    TLabel *Label3;
	void __fastcall m_SelectDataPanelResize(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall m_GroupTypeComboSrcChange(TObject *Sender);
	void __fastcall m_GroupTypeComboDstChange(TObject *Sender);
	void __fastcall m_ChannelsListSrcMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
	void __fastcall m_ChannelsListDstMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
	void __fastcall m_SingleMoveRightBtnClick(TObject *Sender);
	void __fastcall m_SingleMoveLeftBtnClick(TObject *Sender);
	void __fastcall m_MultMoveRightBtnClick(TObject *Sender);
	void __fastcall m_MultMoveLeftBtnClick(TObject *Sender);
    void __fastcall m_PrintBtnClick(TObject *Sender);
private:
	void UpdateSizes();
	void ResetChannels();
	void ResetGroups();

	//void UpdateChannelPanels(bool bReset = false);
	void SetSelectionListGroup(eSortChannelGroupType newGroupMode, int listIndex);
	void SetSelectionListSort(eSortChannelMode newSortMode, int listIndex);
	void UpdateSelectionList(int listIndex);
	TCustomListBox* getSelectionList(int index);
	void linkGroupAndChannel(int grNumber, UINT chNumber,eSortChannelGroupType grType);
	UnicodeString GetGroupName(eSortChannelGroupType groupMode, int groupId);

    bool bSingleSelect;
	eSelectChannelMode selectMode;
	eSortChannelMode sortMode[2];
	eSortChannelGroupType groupMode[2];
	std::vector<sSelChannelDesc> channels;

    eSelectChannelMode filterType;
    int filterValue;

	//-------------------------------------------------
	std::map<int, 	std::set<UINT> > groupToChannel[GRT_LAST];
	std::map<UINT, 	std::set<int> > channelToGroup[GRT_LAST];
	//-------------------------------------------------

    //-----------Interface commands------------------
	int GetFirstSelection(int listIndex);
	void MoveSelected(int listIndex, TStrings *Items);
	void SetItem(int listIndex, int Index);
	void SetButtons();
	void OnListItemSelect(int listIndex, int index);
	//------------------------------------------------
	void PushSelectedToChannelsBuffer();

	std::vector<UINT> inOutChannelsBuffer;
public:
	void Init(bool bSingleSelect = false, eSelectChannelMode selectMode = SELM_BY_CHANNEL,eSortChannelMode sortMode = SORTM_BY_CHANNEL,eSortChannelGroupType groupMode = GRT_KP_GROUP);
    void Filter(eSelectChannelMode type, int value);
	//setting selected channels as string (ex: "0x29, 0x66, 0x78" or "23, 43, 11")
	bool SetSelectedAsString(UnicodeString str);
	//same as SetSelectedAsString, but id with array
	bool SetSelectedAsArray(std::vector<UINT>& data);
	bool SetSelectedAsGroupIdx(int nGroup);
	UnicodeString GetSelectedAsString();
	std::vector<UINT> GetSelectedAsArray();
	int GetSelectedAsGroupIdx();

public:
	virtual __fastcall TSelectChannelDial(TComponent* AOwner);
};
//----------------------------------------------------------------------------
extern PACKAGE TSelectChannelDial *SelectChannelDial;
//----------------------------------------------------------------------------
#endif    
