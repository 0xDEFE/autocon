//---------------------------------------------------------------------------

#ifndef HeaderTestUnitH
#define HeaderTestUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class THeaderTestForm : public TForm
{
__published:	// IDE-managed Components
    TPanel *Panel3;
    TPanel *Panel4;
    TMemo *Memo;
    TPanel *Panel1;
    TPanel *Panel2;
    TButton *Button2;
    TButton *Button1;
    TImage *Image1;
    TImage *Image2;
    TImage *Image3;
    TImage *Image4;
private:	// User declarations
public:		// User declarations
    __fastcall THeaderTestForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE THeaderTestForm *HeaderTestForm;
//---------------------------------------------------------------------------
#endif
