﻿/**
 * @file ScanScriptParser.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef ScanScriptParserH
#define ScanScriptParserH
//---------------------------------------------------------------------------

#include <stack>
#include <string>
#include <list>
#include <vector>
#include <map>
#include <sstream>

#define SSP_BYTECODE_NOOP           0x00 //!< Байткод "нет операции"
#define SSP_BYTECODE_STACK_PUSH		0x01 //!< Байткод "положить в стек"
#define SSP_BYTECODE_STACK_POP      0x02 //!< Байткод "убрать из стека"
#define SSP_BYTECODE_CALL           0x03 //!< Байткод "вызов"
#define SSP_BYTECODE_ENTRY_POINT    0x04 //!< Байткод "точка входа"
#define SSP_BYTECODE_DEBUG_BREAK    0x05 //!< Байткод "брейкпоинт"

typedef unsigned char BYTE;
typedef unsigned int UINT;
typedef unsigned short USHORT;

/** \struct sScanScriptError
    \brief Описывает ошибку
 */
struct sScanScriptError
{
	int line;           //!< Линия, на которой возникла ошибка
	int code;           //!< Код ошибки
	std::string desc;   //!< Пояснение ошибки

	std::string toString(); //!< Преобразование в строку
};

/** \struct sScanScriptErrorBuffer
    \brief Буфер ошибок
 */
struct sScanScriptErrorBuffer
{
	void add(int line, int code, const char* str)
	{
		sScanScriptError err;
		err.line = line;
		err.code = code;
		err.desc = str;
		errors.push_back(err);
	};

	std::string toString();

	std::vector<sScanScriptError> errors;
};

/** \struct sScanScriptCmdDecl
    \brief Описание скриптовой команды
 */
struct sScanScriptCmdDecl
{
	const char* name;  //!< Текстовое имя
	USHORT code;       //!< Код команды
	USHORT argc;       //!< Кол-во аргументов
};

/** \struct sScanScriptCodeDecl
    \brief Описание набора скриптовых команд
 */
struct sScanScriptCodeDecl
{
	std::vector<sScanScriptCmdDecl> commandsDecl;

	//Declare new function with own id and argument count
	void DeclareCmd(const char* name,UINT id,UINT argc);
	void ClearCmd() {commandsDecl.clear();};
	UINT GetCmdDeclCount() {return commandsDecl.size();};
	sScanScriptCmdDecl& GetCmdDecl(UINT index) {return commandsDecl[index];};
    sScanScriptCmdDecl* GetCmdDeclByCmdCode( USHORT code )
    {
        for(unsigned int i = 0; i < commandsDecl.size(); i++)
        {
            if(commandsDecl[i].code == code)
                return &(commandsDecl[i]);
        };
        return NULL;
    };
};

/** \struct sScanScriptEntryPoint
    \brief Описание точки входа в скрипт
 */
struct sScanScriptEntryPoint
{
	sScanScriptEntryPoint()
	{
		codeOffset = 0;
	};
	sScanScriptEntryPoint(const sScanScriptEntryPoint& other)
	{
		name=other.name;
		arguments=other.arguments;
		codeOffset=other.codeOffset;
	};

	std::string name;
	std::vector<int> arguments;
	UINT codeOffset;
};

/** \enum eLexType
    \brief Тип лексемы
 */
enum eLexType
{
	LEXEM_UNKNOWN = -1,  //!< Не известно
	LEXEM_NUMBER = 0,    //!< Номер
	LEXEM_NAME,          //!< Имя
	LEXEM_COLON,         //!< Двоеточие
	LEXEM_ENDL,          //!< Конец строки
	LEXEM_COMMA,         //!< Запятая
	LEXEM_COMMENT,       //!< Комментарий
};

/** \struct sScanScriptLexeme
    \brief Описание лексемы
 */
struct sScanScriptLexeme
{
	eLexType type;   //!< Тип лексемы
	UINT start;      //!< Начало лексемы в файле
	UINT size;       //!< Размер лексемы в файле
};

//! Вывод сообщения
void ScanScriptMessageOut(std::ostream* pOutputStream, sScanScriptErrorBuffer* pErrBuf, const char* string, int code = -1, int line = -1);

class cScanScriptCmdBuffer;
//! Парсер скриптового файла
class cScanScriptParser
{
public:
	cScanScriptParser()
	{
    	pCodeDecl = NULL;
	};
	virtual ~cScanScriptParser() {};

    //! Загружает и парсит файл в буфер команд
	int ParseFile(const char* path, cScanScriptCmdBuffer* pCmdBuffer, std::ostream* pOutputStream = NULL, sScanScriptErrorBuffer* pErrBuff = NULL);
    //! Парсит массив текста в буфер команд
	int Parse(const char* code, cScanScriptCmdBuffer* pCmdBuffer, std::ostream* pOutputStream = NULL, sScanScriptErrorBuffer* pErrBuff = NULL);

	sScanScriptCodeDecl* GetCodeDecl() {return pCodeDecl;};
	void SetCodeDecl(sScanScriptCodeDecl* decl) {pCodeDecl = decl;};
private:
    /**
	    \brief Parse line of code and return result
	    \param line string to parse
	    \param count line size
	    \param code array to store bytecode
	    \param pErrBuff pointer to err buffer
     */
	int _parseLine(UINT lineCnt, const char* start, UINT count, cScanScriptCmdBuffer* pCmdBuffer,
								 std::ostream* pOutputStream, sScanScriptErrorBuffer* pErrBuff);
protected:
	sScanScriptCodeDecl* pCodeDecl;
};

//! Загружает скрипт из файла в виде буфера команд #cScanScriptCmdBuffer
bool LoadScanScriptFromFile(const char* path, sScanScriptCodeDecl* pCodeDecl, cScanScriptCmdBuffer* pCmdBuffer, std::ostream* pOutputStream = NULL, sScanScriptErrorBuffer* pErrBuff = NULL);
//! Загружает скрипт из массива в виде буфера команд #cScanScriptCmdBuffer
bool LoadScanScriptFromMemory(const char* code, sScanScriptCodeDecl* pCodeDecl, cScanScriptCmdBuffer* pCmdBuffer, std::ostream* pOutputStream = NULL, sScanScriptErrorBuffer* pErrBuff = NULL);

//! Выполняет обратную трансляцию скрипта в текст
bool ReverseTranslateScanScriptCode(cScanScriptCmdBuffer* pCmdBuf, sScanScriptCodeDecl* pCodeDecl, char** ppCodeBuff, unsigned int* pCodeBufSize);

/** \enum eSSCmd
    \brief Тип команды
 */
enum eSSCmd
{
	SSC_MOVCARET = 0,       //!< Двигать каретки (movcarets, 2 arg). ex: movcarets [caret one offset], [caret two offset];
	SSC_MOVTOBASE,          //!< Двигать каретки к начальному положению (movtobase, 0 arg). ex: movtobase;
	SSC_SETSG,              //!< Установить группу сканирования (setsg, 2 arg). ex: setsg [scan_group_id];
	SSC_SETSGSENS,          //!< Установить чувствительность группы каналов (setsgsens, 2 arg).
	SSC_SETSGGAIN,          //!< Установить усиление группы каналов (setsggain, 2 arg).
	SSC_SETSGPRISM,         //!< Установить время в призме группы каналов (setsgprism, 2 arg).
	SSC_SETCH,              //!< Установить канал сканирования (setch, 1 arg). ex: setch [scan_channel_id];
	SSC_SETCHSENS,          //!< Установить чувствительность для канала (setchsens, 2 arg).
	SSC_SETCHGAIN,          //!< Установить усиление для канала (setchsens, 2 arg).
	SSC_SETCHPRISM,         //!< Установить время в призме для канала (setchsens, 2 arg).
	SSC_PRINTSG,            //!< Вывести в консоль параметры группы сканирования (printsg, 1 arg).
	SSC_PRINTSGMAX,         //!< Вывести в консоль максимумы в каналах группы сканирования (printchmax, 1 arg).
	SSC_PRINTCH,            //!< Вывести в консоль параметры канала (printch, 1 arg).
	SSC_PRINTCHMAX,         //!< Вывести в консоль максимум канала (printchmax, 1 arg).
	SSC_PRINTCARETS,        //!< Вывести в консоль положения кареток (printcarets, 1 arg).
	SSC_TUNE,               //!< Выполнить настройку всех каналов по собранным максимумам (tune, 0 arg).
	SSC_TUNESG,             //!< Выполнить настройку групп каналов по собранным максимумам (tunesg, not limited arg). ex: tunesg 5,6,3,1
	SSC_TUNECH,             //!< Выполнить настройку каналов по собранным максимумам (tunech, not limited arg). ex: tunech 0x3F,0x5D
	SSC_TUNECLEAR,          //!< Очистить собранные максимумы (tuneclear, 0 arg).
	SSC_TUNESETWCCNT,       //!< Установить кол-во рабочих циклов (tunesetwccnt, 1 arg).
	SSC_PATHRESET,          //!< Сбросить датчик пути (pathreset, 0 arg).
	SSC_SCAN,               //!< Разрешить / Запретить получение данных с каналов (scan, 1 arg).
	SSC_ENDPRG,             //!< Завершить исполнение программы (end, 0 arg).
	SSC_SLEEP,              //!< Пауза в милисекундах (sleep, 1 arg).
	SSC_SETWCCTL,           //!< Разрешить / Запретить ручную установку рабочего цикла (setwcctl, 1 arg).
	SSC_SETWC,              //!< Установить рабочий цикл (setwc, 1 arg).
    SSC_INTERVAL,           //!< Установить интервал сбора максимумов для каналов (interval, 3 arg).
	SSC_LAST                //!< Кол-во команд
};

//! Буфер команд скрипта (содержит байткоды)
class cScanScriptCmdBuffer
{
public:
	cScanScriptCmdBuffer() {};
	~cScanScriptCmdBuffer() {};

	void pushCommand(int cmd,int argc,...);   //!< Добавить новую команду
	void pushEntryPoint(const char* name,int argc,...); //!< Добавить новую точку входа

	void pushCall(int cmd);     //!< Добавить вызов функции
	void pushEP(USHORT value);  //!< Добавить точку входа
	void pushStack(int value);  //!< Положить переменную в стек
    void pushBreak(unsigned short line); //!< Добавить точку останова на линию

	void clear(); //!< Очистить буфер команд.

public:
	std::vector<BYTE> bytecode;
	std::list<sScanScriptEntryPoint> entryPoints;
    std::vector<int> lineToCodeInfo;
};

/** \enum eScanScriptRetCode
    \brief Код возврата при исполнении скрипта
 */
enum eScanScriptRetCode
{
    SSRC_END_PROGRAMM = 0, //!< Программа завершена
    SSRC_RUNNING = 1,      //!< Программа продолжит выполнение
    SSRC_DEBUG_BREAK,      //!< Точка останова
    SSRC_RUNTIME_ERROR,    //!< Ошибка времени исполнения
    SSRC_SYSCODE_ENDP      //!< Что то связанное с брейкпоинтами
};

//! Обеспечивает выполнение скриптовой программы
class cScanScriptProgramm
{
public:
	cScanScriptProgramm()
	{
		initCodeDecl();
		IP = 0;
        breakNextLine = -1;
	};
	virtual ~cScanScriptProgramm() {};

    static sScanScriptCodeDecl GetStdCodeDecl(); //!< Генерация стандартных деклараций команд

	bool createFromFile(const char* path); //!< Загрузка скрипта из файла
	bool createFromMemory(const char* code);    //!< Загрузка скрипта из памяти
	bool createFromCode(cScanScriptCmdBuffer& cmdBuffer); //!< Загрузка буфера с байткодами
	void release();  //!< Очистка программы

	void cleanup();  //!< Очистка стека и буфера ошибок
	eScanScriptRetCode executeAll(); //!< Выполнить программу
	eScanScriptRetCode executeNext();//!< Выполнить следующую команду
    eScanScriptRetCode executeLine();//!< Выполнить следующую строку кода

	bool isValid(); //!< Проверка валидности
	UINT getInstructionPointer(); //!< Получить индекс исполняемой инструкции
	UINT getCodeSize();           //!< Получить размер буфера инструкций

    int IPToLine(unsigned int ip_);  //!< Преобразовать индекс инструкции к строке кода
    int LineToIP(unsigned int line); //!< Преобразовать строку кода к индексу инструкции
    int getCurrentLine();            //!< Получить текущую исполняемую строку кода
    int getTotalLines();             //!< Получить кол-во строк кода
    void insertBreakpoint(unsigned int line); //!< Добавить точку останова
    void removeBreakpoint(unsigned int line); //!< Удалить точку останова

	bool setEntryPoint(bool bSetNext,UINT argc,const char* name, int index); //!< Установить точку входа

	sScanScriptErrorBuffer& getLastError(); //!< Получить последнюю ошибку
	cScanScriptCmdBuffer& getCode();        //!< Получить исполняемый байткод
	std::ostringstream& getOutputStream();  //!< Получить поток вывода
protected:
	virtual void initCodeDecl();  //Overwrite for custom commands
	virtual eScanScriptRetCode executeCmd(USHORT cmdCode); //Overwrite for custom execution
    eScanScriptRetCode handleCmd(USHORT cmdCode, UINT argc);
	bool pushStack(void* pData, UINT size);
	bool popStack(void* pData, UINT size);
protected:
	UINT IP;
	std::stack<BYTE> stack;
	sScanScriptErrorBuffer errBuf;
	std::ostringstream outputStream;
	sScanScriptCodeDecl codeDecl;
	cScanScriptCmdBuffer cmdBuffer;
    std::vector<bool> debugBreaks;
    int breakNextLine;
};

///UTILS
void TuneChannels();
void TuneChannelGroup(int chGr);
bool TuneChannel(int chId);
bool TuneChannel(int chId, int chMax);

#endif
