//---------------------------------------------------------------------------

#ifndef FiltrParamUnitH
#define FiltrParamUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.jpeg.hpp>
#include <Vcl.Samples.Spin.hpp>
//---------------------------------------------------------------------------
class TFiltrParamForm : public TForm
{
__published:	// IDE-managed Components
    TPanel *Panel1;
    TImage *Image1;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label4;
    TButton *Button1;
    TButton *Button2;
    TSpinEdit *SameCount;
    TSpinEdit *DelayDelta;
    TSpinEdit *MaxSpace;
    TSpinEdit *MinLenEdit;
	TLabel *Label3;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TFiltrParamForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFiltrParamForm *FiltrParamForm;
//---------------------------------------------------------------------------
#endif
