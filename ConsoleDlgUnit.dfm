object ConsoleDlg: TConsoleDlg
  Left = 227
  Top = 108
  BorderStyle = bsSizeToolWin
  Caption = 'Console'
  ClientHeight = 348
  ClientWidth = 612
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 606
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object m_ConsoleEdit: TEdit
      Left = 0
      Top = 0
      Width = 493
      Height = 25
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 21
    end
    object m_ConsoleExecBtn: TButton
      Left = 493
      Top = 0
      Width = 83
      Height = 25
      Align = alRight
      Caption = 'Execute'
      Default = True
      TabOrder = 1
      OnClick = m_ConsoleExecBtnClick
    end
    object m_ExecFromFileBtn: TButton
      Left = 576
      Top = 0
      Width = 30
      Height = 25
      Align = alRight
      Caption = '...'
      Default = True
      TabOrder = 2
      OnClick = m_ExecFromFileBtnClick
    end
  end
  object m_ConsoleMemo: TMemo
    AlignWithMargins = True
    Left = 3
    Top = 34
    Width = 606
    Height = 311
    Align = alClient
    Lines.Strings = (
      '# Type "help" for command list')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
end
