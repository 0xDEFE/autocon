﻿/**
 * @file AScanFrameUnit.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef AScanFrameUnitH
#define AScanFrameUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.Series.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <VCLTee.TeeShape.hpp>

#include "MainUnit.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.Samples.Spin.hpp>
#include <VCLTee.TeCanvas.hpp>
#include "BasicFrameUnit.h"
#include "BScanLine.h"
#include <Vcl.AppEvnts.hpp>
#include "ConfigUnit.h";

#define MF_ASCAN_FLAG_PHOTO_BTN     0x01 //!< Кнопка снимка А-развертки
#define MF_ASCAN_FLAG_BSCAN_BTN     0x02 //!< Кнопка записи В-развертки
#define MF_ASCAN_FLAG_TUNE_BTN      0x04 //!< Кнопка настройки чувствительности
#define MF_ASCAN_FLAG_KU_PANEL      0x08 //!< Показать Ку
#define MF_ASCAN_FLAG_ATT_PANEL     0x10 //!< Показать АТТ
#define MF_ASCAN_FLAG_SURFACE_COMBO 0x20 //!< Показать выбор поверхности
#define MF_ASCAN_FLAG_BSCAN_PANEL   0x40 //!< Показать В-развертку
#define MF_ASCAN_FLAG_HSCAN_BTNS_VISIBLE 0x80  //!< Показать ручники
#define MF_ASCAN_FLAG_SINGLEUSE     0x100      //!< Для выхода из режима по закрытии окна (не используется когда, например, переход произошел из режима Поиск)
#define MF_ASCAN_FLAG_RESTORE_CAL_TUNE_GATES     0x200 //!< Восстановление режима стробов при выходе (hack)

//! Флаги для режима Поиск
#define MF_ASCAN_SEARCH_FLAGS (/*MF_ASCAN_FLAG_PHOTO_BTN | MF_ASCAN_FLAG_BSCAN_PANEL |  MF_ASCAN_FLAG_BSCAN_BTN |*/ MF_ASCAN_FLAG_KU_PANEL | MF_ASCAN_FLAG_ATT_PANEL )
//! Флаги для режима Ручной
#define MF_ASCAN_HAND_FLAGS (MF_ASCAN_FLAG_PHOTO_BTN | MF_ASCAN_FLAG_BSCAN_BTN | MF_ASCAN_FLAG_KU_PANEL | MF_ASCAN_FLAG_SURFACE_COMBO | MF_ASCAN_FLAG_BSCAN_PANEL | MF_ASCAN_FLAG_HSCAN_BTNS_VISIBLE | MF_ASCAN_FLAG_SINGLEUSE)
//! Флаги для режима Настройка Ручных ПЭП
#define MF_ASCAN_HAND_TUNE_FLAGS (MF_ASCAN_FLAG_TUNE_BTN | MF_ASCAN_FLAG_KU_PANEL | MF_ASCAN_FLAG_ATT_PANEL | MF_ASCAN_FLAG_HSCAN_BTNS_VISIBLE  | MF_ASCAN_FLAG_SINGLEUSE | MF_ASCAN_FLAG_RESTORE_CAL_TUNE_GATES)
//! Флаги для режима Настройка
#define MF_ASCAN_CALIB_FLAGS (MF_ASCAN_FLAG_TUNE_BTN | MF_ASCAN_FLAG_BSCAN_PANEL |  MF_ASCAN_FLAG_KU_PANEL | MF_ASCAN_FLAG_ATT_PANEL )

//---------------------------------------------------------------------------
//! Отображает А-развертку
class TAScanFrame : public TBasicFrame
{
__published:	// IDE-managed Components
    TGridPanel *ChInfoGridPanel;
    TPanel *HPanel;
    TGridPanel *GridPanel2;
    TGridPanel *GridPanel3;
    TPanel *LPanel;
	TPanel *RPanel;
    TPanel *ChannelTitlePanel;
    TPanel *InfoPanel;
    TPanel *ChartPanel;
    TPanel *RightPanel;
    TPanel *NumPadPanel;
    TSpeedButton *SpeedButton10;
    TSpeedButton *SpeedButton11;
    TSpeedButton *SpeedButton20;
    TSpeedButton *SpeedButton21;
    TSpeedButton *SpeedButton26;
    TSpeedButton *SpeedButton27;
    TSpeedButton *SpeedButton28;
    TSpeedButton *SpeedButton30;
    TSpeedButton *SpeedButton31;
    TSpeedButton *SpeedButton32;
    TSpeedButton *SpeedButton33;
    TSpeedButton *SpeedButton34;
    TSpeedButton *SpeedButton35;
    TSpeedButton *SpeedButton36;
    TSpeedButton *SpeedButton29;
    TScrollBox *ScrollBox1;
    TPanel *Panel7;
    TPanel *PrismDelayPanel;
    TSpinButton *SpinButton05;
    TPanel *Panel8;
    TPanel *Panel9;
    TPanel *TVGPanel;
    TSpinButton *SpinButton04;
    TPanel *Panel10;
    TPanel *AScanKuPanel;
    TPanel *SensPanel;
    TSpinButton *SpinButton03;
    TPanel *Panel12;
    TPanel *Panel13;
    TPanel *EdGatePanel;
    TSpinButton *SpinButton02;
    TPanel *Panel15;
    TPanel *Panel16;
    TPanel *StGatePanel;
    TSpinButton *SpinButton01;
    TPanel *Panel18;
    TPanel *LabelPanel;
    TPanel *GeneratorPanel;
    TSpinButton *SpinButton06;
    TPanel *Panel21;
    TPanel *BScanRecPanel;
    TPanel *ReceiverPanel;
    TPanel *Panel14;
    TSpinButton *SpinButton3;
    TPanel *EndPanel;
    TPanel *Panel24;
    TSpeedButton *SpeedButton1;
    TPanel *RullerModePanel;
    TPanel *Panel26;
    TPanel *TuningPanel;
    TSpeedButton *CallibrationButton;
    TPanel *ValuePanel00;
    TPanel *Panel29;
    TPanel *Panel17;
    TButton *Button15;
    TPanel *Panel19;
    TSpeedButton *sbSideLine;
    TPanel *ValuePanel08;
    TPanel *Panel20;
    TPanel *Panel25;
    TPanel *BScanGateLevelPanel;
    TPanel *Panel28;
    TSpinButton *SpinButton5;
    TPanel *Panel32;
    TSpeedButton *SpeedButton8;
    TPanel *NoiseRedPanel;
    TPanel *Panel34;
    TPanel *Panel35;
    TSpeedButton *SpeedButton9;
    TPanel *AnglePanel;
    TPanel *Panel37;
    TPanel *Panel38;
    TPanel *DurationPanel;
    TPanel *Panel40;
    TSpinButton *SpinButton9;
    TPanel *Panel41;
    TPanel *DelayScalePanel;
    TPanel *Panel43;
    TSpinButton *SpinButton10;
    TPanel *Panel44;
    TPanel *ZondAmplPanel;
    TPanel *Panel46;
    TSpinButton *SpinButton11;
    TPanel *Panel33;
    TPanel *BScanMaxDelayPanel;
    TSpinButton *SpinButton1;
    TPanel *Panel39;
    TPanel *Panel42;
    TPanel *BScanMinDelayPanel;
    TSpinButton *SpinButton2;
    TPanel *Panel47;
    TPanel *Panel22;
    TPanel *EvaluationGateLevelPanel;
    TPanel *Panel30;
    TSpinButton *SpinButton4;
    TPanel *AScanAttPanel;
    TPanel *AttPanel;
    TSpinButton *SpinButton6;
    TPanel *Panel31;
    TPanel *Panel3;
    TPanel *BScanGatePanel;
    TPanel *Panel5;
    TSpinButton *SpinButton7;
    TPanel *Panel4;
    TSpeedButton *BScanViewBtn;
    TPanel *BScanViewPanel;
    TPanel *Panel52;
    TPanel *Panel55;
    TPanel *Stroke;
    TPanel *Panel57;
    TSpinButton *SpinButton8;
    TPanel *Panel56;
    TPanel *StrokeCount;
    TPanel *Panel59;
    TSpinButton *SpinButton12;
    TPanel *Panel2;
    TSpeedButton *SpeedButton2;
    TPanel *GateIdxPanel;
    TPanel *GateIdxTextPanel;
    TSpinButton *SpinButton13;
    TPanel *Panel49;
    TPanel *AScanPhotoPanel;
    TSpeedButton *SpeedButton5;
    TPanel *AScanPhotoLabelPanel;
    TPanel *WriteBScanPanel;
    TSpeedButton *WriteBScanBtn;
    TPanel *WriteBScanLabel;
    TPanel *AScanDebugPanel;
    TPanel *DebugPanel;
    TPanel *Panel36;
    TPanel *BScanDataPanel;
    TPanel *ControlPanel;
    TButton *Button34;
    TCheckBox *CheckBox1;
    TCheckBox *CheckBox6;
    TPanel *BScanPanel;
    TPaintBox *BScanPB;
    TShape *Shape2;
    TShape *Shape3;
    TPanel *HandChannelPanel;
    TLabel *SurfaceLabel;
    TComboFlat *cbSurface;
    TPanel *BScanDebugPanel;
    TPanel *AmplDebugPanel;
    TChart *BScanChart;
    TLineSeries *BScanSeries;
    TPanel *Panel54;
    TLabel *Label1;
    TComboBox *SelEchoIdx;
    TChart *BScanChart2;
    TLineSeries *BSSeries1;
    TLineSeries *BSSeries2;
    TLineSeries *BSSeries3;
    TLineSeries *BSSeries4;
    TLineSeries *BSSeries5;
    TLineSeries *BSSeries6;
    TLineSeries *BSSeries7;
    TLineSeries *BSSeries8;
    TGridPanel *GridPanel1;
    TGridPanel *GridPanel4;
    TTimer *BScanHandTimer;
    TTimer *BScanDrawTimer;
    TApplicationEvents *ApplicationEvents1;
    TTimer *Timer1;
    TTimer *HandScanTimeTimer;
    TChart *AScanChart;
    TAreaSeries *AScanSeries_;
    TChartShape *MaxPosSeries;
    TAreaSeries *TVGSeries_;
    TChartShape *PeakSeries;
    TPointSeries *BScanDebugSeries;
    TChartShape *BScanGateMainSeries;
    TChartShape *BScanGateLeftSeries;
    TChartShape *BScanGateRightSeries;
    TChartShape *GateSeriesMain;
    TChartShape *GateSeriesLeft;
    TChartShape *GateSeriesRight;
    TChartShape *Series1;
    TChartShape *Series2;
    TLineSeries *AScanSeries;
    TLineSeries *TVGSeries;
    TPanel *Panel1;
    TBitBtn *m_MarkEnableBtn;
    TGridPanel *m_MarkOffsetGrid;
    TPanel *m_MarkOffsetPanel;
    TTimer *MarkMovementTimer;
    TSpeedButton *m_MarkOffsetLeftBtn;
    TSpeedButton *m_MarkOffsetRightBtn;
    TChartShape *MarkSeries;
    TPanel *m_MarkWidthPanel;
    TLabel *DebugPanelLabel;
	TPanel *NPanel;
    TBitBtn *rightPanelDownBtn;
    TBitBtn *rightPanelUpBtn;
    TSpeedButton *Button_70echo;
    TSpeedButton *Button_65echo;
    TSpeedButton *Button_58echo;
    TSpeedButton *Button_50echo;
    TSpeedButton *Button_45echo;
    TSpeedButton *Button_0echo;
    TSpeedButton *Button_0ztm;
    void __fastcall AScanChartClick(TObject *Sender);
    void __fastcall SpeedButton2Click(TObject *Sender);
    void __fastcall sbSideLineClick(TObject *Sender);
    void __fastcall OnSpinDownBtnClick(TObject *Sender);
    void __fastcall OnSpinUpBtnClick(TObject *Sender);
    void __fastcall NumPadClick(TObject *Sender);
    void __fastcall BScanHandTimerTimer(TObject *Sender);
    void __fastcall EditModePanelClick(TObject *Sender);
    void __fastcall BScanDrawTimerTimer(TObject *Sender);
    void __fastcall ApplicationEvents1Message(tagMSG &Msg, bool &Handled);
    void __fastcall Timer1Timer(TObject *Sender);
    void __fastcall HandScanTimeTimerTimer(TObject *Sender);
    void __fastcall SetHandChannelBtn(TObject *Sender);
    void __fastcall BScanPanelResize(TObject *Sender);
    void __fastcall m_MarkEnableBtnClick(TObject *Sender);
    void __fastcall MarkMovementTimerTimer(TObject *Sender);
    void __fastcall OnMarkBtnMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
    void __fastcall OnMarkBtnMouseUp(TObject *Sender);
    void __fastcall OnMarkBtnMouseUp2(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
    void __fastcall DebugPanelDblClick(TObject *Sender);
    void __fastcall rightPanelUpBtnClick(TObject *Sender);
    void __fastcall rightPanelDownBtnClick(TObject *Sender);

private:	// User declarations
    void OnMFrameAttach(TBasicFrame* pMain);
    void OnMFrameDetach(TBasicFrame* pMain);
    void OnMFrameCreate(TBasicFrame* pMain);
    void OnMFrameDestroy(TBasicFrame* pMain);
    void OnUMUSignal(eMainUnitSignalType sigtype, sPtrListItem& sig);
    void OnFormShortCut(TWMKey &Msg, bool &Handled);

    void BeginHandBScanWrite();
    void EndHandBScanWrite();

    TBScanLine *bsl;

    UnicodeString EnterModeUndo;
	bool EnterModeState;
    TPanel* EditPanel;

    DWORD StartHandBScanTime;

    int LastSelectChannels;
    DWORD LastSelectChannelsTickCount;

    bool bMarkBtnState;
    bool MarkBtnDownState[2];
    void updateMarkState();

    bool bFirstTime;
    bool EditMode;
    bool BScanView;
    bool NoiseRed;
    bool DebugOut;
    int SaveParamH;
    int SaveParamKd;
    double SaveMax1;
	double SaveMax2;
    int RullerMode; // 0 - мкс; 1 - мм; 2 - отсчеты
    int OldRullerMode; // 0 - мкс; 1 - мм; 2 - отсчеты
public:		// User declarations
    __fastcall TAScanFrame(TComponent* Owner);
    void SetFrameFlags(DWORD flags);
    unsigned char Filter(unsigned char Value);
    float PixelToChartValue(int Pixel);
    float ChartValueToDelay(float Src);
    float DelayToChartValue(float Src);
    void ValueToControl(void);
    UnicodeString GetFrameName() {return L"TAScanFrame";};
};
//---------------------------------------------------------------------------
//extern PACKAGE TAScanFrame *AScanFrame;
//---------------------------------------------------------------------------

extern cACConfig * ACConfig;

#endif
