//---------------------------------------------------------------------------

#pragma hdrstop

#include "TestFiltrUnit.h"
#include "Autocon.inc"
//---------------------------------------------------------------------------
#pragma package(smart_init)


void Filter(cJointTestingReport *SrcRep, sFilterParams fp)
{
    cJointTestingReport *DestRep;
    DestRep = new cJointTestingReport(ScanLen);
    DestRep->SetFilterState_(True);
    DestRep->fp = fp;

	PsScanSignalsOneCrdOneChannel sgl;
	int SysCoord = 0;
	CID ChannelId = 0;
	bool Result;
    /*
	sgl = SrcRep->GetFirstScanSignals(SysCoord, ChannelId, &Result);
	if (Result) DestRep->PutScanSignals(SysCoord, ChannelId, sgl);
	while (true)
	{
		sgl = SrcRep->GetNextScanSignals(&SysCoord, &ChannelId, &Result);
		if (!Result) break;
		DestRep->PutScanSignals(SysCoord, ChannelId, sgl);
	}
      */

	for (int i = SrcRep->Header.MaxSysCoord; i > 0; i--)
		for (int ch = 0; ch < 86; ch++)
//        for (int ch = 0; ch < 1; ch++)
        {
	  //		int ch = 12;

            sgl = SrcRep->GetFirstScanSignals(i, ch, &Result);
	        if (Result) DestRep->PutScanSignals(i, ch, sgl);
        }

/*
	sgl = SrcRep->GetFirstScanSignals(SysCoord, ChannelId, &Result);
	if (Result) if (ChannelId < 40) DestRep->PutScanSignals(SysCoord, ChannelId, sgl);
	while (true)
	{
		sgl = SrcRep->GetNextScanSignals(&SysCoord, &ChannelId, &Result);
		if (!Result) break;
		if (ChannelId < 40) DestRep->PutScanSignals(SysCoord, ChannelId, sgl);
	}

 	SysCoord = 0;
	ChannelId = 0;

	sgl = SrcRep->GetFirstScanSignals(SysCoord, ChannelId, &Result);
	if (Result) if (ChannelId >= 40) DestRep->PutScanSignals(SysCoord, ChannelId, sgl);
	while (true)
	{
		sgl = SrcRep->GetNextScanSignals(&SysCoord, &ChannelId, &Result);
		if (!Result) break;
		if (ChannelId >= 40) DestRep->PutScanSignals(SysCoord, ChannelId, sgl);
	}
*/
	DestRep->Header = SrcRep->Header;

	DestRep->WriteToFile(SrcRep->ReportFileName + "_F_" + IntToStr((int)GetTickCount()) + ".acd");
	delete DestRep;

}
