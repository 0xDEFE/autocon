﻿/**
 * @file BScanDraw.h
 * @author Denis Fedorenko
 */

//---------------------------------------------------------------------------

#ifndef BScanDrawH
#define BScanDrawH

#include "DataStorage.h"
#include "DeviceConfig.h"
#include "DeviceConfig_Autocon.h"
#include "DeviceCalibration.h"
#include "BScanLines.h"
//Added by KirillB
#include "Utils.h"
#include "BScanMeasureUnit.h"

const
  MaxLinkData = 100; //!< Максимальное количество записей в массиве cBScanDraw::LinkData

typedef
  CID Shift_CID_List[6];

typedef
  TColor BScan_Color_List[6];

/** \struct LinkDataItem
    \brief Обеспечивает связывание между каналом и его отображением
 */
struct LinkDataItem
{
	TRect Rt;
	bool Enabled;
	sChannelDescription ChDesc;
	TBitmap* Buffer;
	TColor Color;
	eBScanLineDrawType drawType;
	bool Used;
};

class cBScanDraw
{
	private:

       int x;
       int y;
       cChannelsTable* Tbl;
       cDeviceConfig* Cfg;
       cJointTestingReport* Rep;
       cDeviceCalibration* Calib;
       std::vector < cBScanLines* > BScanLinesList;
	   void DrawSignals(int SysCoord, CID Channel, PsScanSignalsOneCrdOneChannel Sgl);

	   LinkDataItem LinkData[MaxLinkData];

       int channels_gates_temp[9][3];//!< [kp number][chId, stGate, edGate]

       int CursorSysCrd;
       int CursorBScanLinesIdx;
       int CursorBScanLinesSubIdx;
       bool bCursorEnabled;
       bool bUseReportForGateData;
       bool bEnableCompareDataDraw;
       BScanMeasure* pCompareData;

        template<typename F>
        inline Vec2T<F> SysToScreenCrd(const Vec2T<F>& val, CID channel, TRect rect)
        {
            return Vec2T<F>(SysCoordToScreen(val.x,channel,rect),
                                DelayToScreen(val.y, channel,rect));
        };

        void drawGrid();
        void drawMeasureGrid();
        void drawGrid(unsigned int BScanLines_idx, int LineIdx, TColor col1, TColor col2);
        void drawGridLegend(unsigned int BScanLines_idx, int LineIdx);
        void drawGridLegend();
        void drawCursor();
        void drawMeasureData();
        HRGN createMeasureRgn(unsigned int BScanLines_idx, int LineIdx);

	public:

        static bool bDisableAScanStrobeCheck; // FIXME - remove after not needed
        static bool bDisableBScanStrobeCheck; // FIXME - remove after not needed
        int Th;
        int GroupShift[15];

        cBScanDraw(cChannelsTable* Table, cDeviceConfig* Config, cJointTestingReport* Report,cDeviceCalibration* Calibration);
        ~cBScanDraw(void);

        void enableReportStrobeData(bool bUseReportForGateData) { this->bUseReportForGateData = bUseReportForGateData;};

        //Sets cursor with xy screen coordinates (for example, with mouse click)
        void SetCursorXY(bool bEnable, int X = 0, int Y = 0, TColor col = clBlack);
        //Sets cursor with system b-scan coordinates
        void SetCursorSC(bool bEnable, int SysCoord = 0, int BScanLines_idx = -1,int BScanLines_sub_idx = -1, TColor col = clBlack);

        int SysCoordToScreen(int SysCoord,CID Channel, TRect rect);
        int DelayToScreen(int Delay,CID Channel, TRect rect);
        int DelayToScreen(int Delay, unsigned int BScanLines_idx, int LineIdx);
        int ScreenToDelay(int Y, unsigned int BScanLines_idx, int LineIdx);

		void ClearBScanLines();
        void AddBScanLine(cBScanLines* BScanLines);
        void Prepare(void);
        void Draw(bool NewItemFlag);
        bool GetSysCoord(int X, int Y, int* SysCoord, Shift_CID_List* Shift_CID, int* Shift_CID_Cnt, BScan_Color_List* BScanColor, int* pDelay = NULL);

        void SetMeasureData(BScanMeasure* pSnapshot);
        void EnableMeasureDataDraw(bool bVal);
};

#endif
