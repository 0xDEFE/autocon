//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop


#include "MainUnit.h"
#include "ScreenMessageUnit2.h"
#include "FotoUnit.h"
#include "DebugStateUnit.h"
#include "LogUnit.h"

#include "ScreenMessageUnit3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainUnit *MainUnit;
//---------------------------------------------------------------------------
__fastcall TMainUnit::TMainUnit(TComponent* Owner)
    : TForm(Owner)
{
    //currFrame = NULL;
    ChangeModeFlag = false;
    motionState = MS_MOTION_END;
    bMessageShowState = false;
    pTopFrame = NULL;
    bRailBlockCriticalMode = false;
    _setRailBlockState(false);
#ifdef DEBUG
    ShowDebugWindowsBtn->Visible = true;
#endif
}
//---------------------------------------------------------------------------
void __fastcall TMainUnit::SignalTimerTimer(TObject *Sender)
{
    SignalTimer->Enabled = false;
    cavtkQueueLock.Enter();
    procCavtkMessages();
    cavtkQueueLock.Release();
    SignalTimer->Enabled = true;


    UpdateSignals();
}

void TMainUnit::procCavtkMessages()
{
    while(!cavtk_messages.empty())
    {
        std::pair<cavtk*, tCavtkMsg> msg = cavtk_messages.front();

        const char* msg_type = "unknown";
        switch(msg.second.type)
        {
            case MessageNone:       msg_type = "MessageNone"; break;
            case ErrorMesage:       msg_type = "ErrorMesage"; break;
            case ErrorAnswer:       msg_type = "ErrorAnswer"; break;
            case UserStopMessage:   msg_type = "UserStopMessage"; break;
        }

        const char* msg_code = "unknown";
        if(msg.second.type == ErrorMesage)
        {
            msg_code = get_errstr( msg.second.code );
        }

        std::string str = StringFormatStdA("CAVTK Message: type = %s (%d), code = %s (%d), flag = %d",msg_type, msg.second.type,msg_code, msg.second.code,msg.second.flag);

        std::vector<sSMF3_Answer> answers;
        DWORD timeout = 5000;
        DWORD flags = SMF3_FLAG_MB_OK | SMF3_FLAG_ICONINFO | SMF3_FLAG_OK_ON_CLICK;
        if((msg.second.type == ErrorMesage) && (msg.second.flag == 1))
        {
            str += "�������� �������:";
            timeout = 0xFFFFFFFF;
            flags |= SMF3_FLAG_SELECT_MODE;

            sSMF3_Answer ans;
            ans.retCode = -1;
            ans.text = "�������� ����� ������";
            answers.push_back(ans);

            ans.retCode = -2;
            ans.text = "�������� ����� ������";
            answers.push_back(ans);
        }

        int retAns = 0;
        TScreenMessageForm3::MessageBoxA(NULL, "������!", str.c_str(),
             flags,
            clRed, timeout, &answers, &retAns);

        cavtk_messages.pop();
    }
}
//---------------------------------------------------------------------------
void TMainUnit::UpdateSignals()
{
    AUTO_LOCK_WCS(*(AutoconMain->CS));

    bool bHasAScanData = false;
    bool bHasTVGData = false;
    bool bHasAScanMeasData = false;
    ringbuf<sPtrListItem>& Ptr_List = AutoconMain->devt->Ptr_List;

    if(!pTopFrame || !pTopFrame->Enabled)
        return;

    pTopFrame->OnUMUArray( Ptr_List );

    //Signals from last to first
    for(int PtrListIdx = Ptr_List.size() - 1; PtrListIdx >= 0; PtrListIdx--)
    {
        sPtrListItem& Ptr_Item = Ptr_List[PtrListIdx];

        switch(Ptr_Item.Type)
        {
            case edAScanMeas:
            {
                if(bHasAScanMeasData)
                    break;
                if ((Ptr_Item.Ptr1.pAScanMeasure->Side != AutoconMain->DEV->GetSide()) ||
                    (Ptr_Item.Ptr1.pAScanMeasure->Channel != AutoconMain->DEV->GetChannel()))
                    break;
                bHasAScanMeasData = true;
                pTopFrame->OnUMUSignal(MU_SIGNAL_UMU_ASCAN_MEAS, Ptr_Item);
                break;
            }
            case edAScanData:
            {
                if(bHasAScanData)
                    break;
                if ((Ptr_Item.Ptr1.pAScanHead->Side != AutoconMain->DEV->GetSide()) ||
                    (Ptr_Item.Ptr1.pAScanHead->Channel != AutoconMain->DEV->GetChannel()))
                    break;
                bHasAScanData = true;
                pTopFrame->OnUMUSignal(MU_SIGNAL_UMU_ASCAN_DATA, Ptr_Item);
                break;
            }
            case edTVGData:
            {
                if(bHasTVGData)
                    break;
                if ((Ptr_Item.Ptr1.pAScanHead->Side != AutoconMain->DEV->GetSide()) ||
                    (Ptr_Item.Ptr1.pAScanHead->Channel != AutoconMain->DEV->GetChannel()))
                    break;
                bHasTVGData = true;
                pTopFrame->OnUMUSignal(MU_SIGNAL_UMU_TVG_DATA, Ptr_Item);
                break;
            }
            case edAlarmData:
            {
                pTopFrame->OnUMUSignal(MU_SIGNAL_UMU_ALARM_DATA, Ptr_Item);
                break;
            }
        }
    }

    //Signals from first to last
    for(unsigned int PtrListIdx = 0; PtrListIdx < Ptr_List.size(); PtrListIdx++)
    {
        sPtrListItem& Ptr_Item = Ptr_List[PtrListIdx];

        switch(Ptr_Item.Type)
        {
            case edBScan2Data:
            {
                pTopFrame->OnUMUSignal(MU_SIGNAL_UMU_BSCAN2_DATA, Ptr_Item);
                break;
            }
            case edMScan2Data:
            {
                pTopFrame->OnUMUSignal(MU_SIGNAL_UMU_MSCAN2_DATA, Ptr_Item);
                break;
            }
        };
    }

    while(!Ptr_List.empty())
    {
        Ptr_List.front().free();
        Ptr_List.pop_front();
    }
}

void TMainUnit::Initialize()
{
    AutoconMain->ac->SetMessagesProc(GetMessages);
    AutoconMain->ac->SetSearchMotion(SearchMotion_); // ����� "�����"
    AutoconMain->ac->SetTestMotion(TestMotion_);   // ����� "����"
	//AutoconMain->ac->SetAdjustMotion(AdjustMotion_); // ����� "���������"
	AutoconMain->ac->SetTuneMotion(TuneMotion_);   // ����� "���������"
	AutoconMain->ac->SetManualMotion(ManualMotion_);   // ����� "������"
    AutoconMain->ac->SetCavtkRequestProc(OnCavtkRequest_);


}

//---------------------------------------------------------------------------

void __fastcall TMainUnit::FormCreate(TObject *Sender)
{
    FormatSettings.DecimalSeparator = '.';  // fixme ?

    assert(!pTopFrame);
    TBasicFrame* pFrame = new TBasicFrame(this);
    pFrame->Initialize(NULL, this, 0, true);
    pFrame->Align = alClient;
    pFrame->Parent = this;
    this->InsertComponent(pFrame);

    pFrame->bPopOnEmpty = true;

    pTopFrame = pFrame;
}
//---------------------------------------------------------------------------

void __fastcall TMainUnit::FormDestroy(TObject *Sender)
{
    TBasicFrame* pFrame = pTopFrame;
    pTopFrame = NULL;

    pFrame->Visible = false;
    pFrame->Parent = NULL;
    this->RemoveComponent(pFrame);
    deleteLater(pFrame);
    updateRemove();
}
//---------------------------------------------------------------------------

void __fastcall TMainUnit::FormShow(TObject *Sender)
{
    assert(pTopFrame);
    SignalTimer->Enabled = true;
    pTopFrame->Visible = true;
}
//---------------------------------------------------------------------------
void __fastcall TMainUnit::FormHide(TObject *Sender)
{
    assert(pTopFrame);
    /*AutoconMain->ac->SetSearchMotion(NULL); // ����� "�����"
    AutoconMain->ac->SetTestMotion(NULL);   // ����� "����"
	AutoconMain->ac->SetAdjustMotion(NULL); // ����� "���������"
	AutoconMain->ac->SetTuneMotion(NULL);   // ����� "���������"
	AutoconMain->ac->SetManualMotion(NULL);   // ����� "������"       */
    SignalTimer->Enabled = false;
    pTopFrame->Visible = false;
}
//---------------------------------------------------------------------------
void __fastcall TMainUnit::FormShortCut(TWMKey &Msg, bool &Handled)
{
    if(pTopFrame)
        pTopFrame->OnFormShortCut(Msg, Handled);
}

void TMainUnit::ProcMotion(eMainUnitSignalType type, int WorkCycle)
{
    eMotionState prevState = motionState;


    TACWorkMode WorkMode = AutoconMain->ac->GetWorkMode();
    bool bEndCycle = (WorkCycle == -1) || (((WorkMode == wmTest) || (WorkMode == wmSearch)) && (WorkCycle == 16));
    switch(motionState)
    {
    case MS_MOTION_BEGIN:
        if(bEndCycle)
            motionState = MS_MOTION_END;
        else
           motionState = MS_MOTION_IN_PROCESS;
        break;
    case MS_MOTION_IN_PROCESS:
        if(bEndCycle)
            motionState = MS_MOTION_END;
        break;
    case MS_MOTION_END:
        if(!bEndCycle)
            motionState = MS_MOTION_BEGIN;
        break;
    }

    if(pTopFrame)
        pTopFrame->OnAcMotion(type,WorkCycle,motionState,prevState!=motionState);
}

void TMainUnit::GetMessages(TMessagesType Type, UnicodeString Text, DWord Time) // �������� ��������� �� ������
{
    if(bMessageShowState)
    {
        ScreenMessageForm2->Add(Type, Text, Time,NULL);//No disable parent (?)
    }
    else ScreenMessageForm2->Add(Type, Text, Time,this);
}
bool TMainUnit::OnCavtkRequest_(cavtk *ctrl, tCavtkMsg& msg)
{
    cavtkQueueLock.Enter();
    cavtk_messages.push(std::make_pair(ctrl, msg));
    cavtkQueueLock.Release();

    while(!cavtk_messages.empty())
    {
        Sleep(100);
    }


//    const char* msg_type = "unknown";
//    switch(msg.type)
//    {
//        case MessageNone:       msg_type = "MessageNone"; break;
//        case ErrorMesage:       msg_type = "ErrorMesage"; break;
//        case ErrorAnswer:       msg_type = "ErrorAnswer"; break;
//        case UserStopMessage:   msg_type = "UserStopMessage"; break;
//    }
//
//    const char* msg_code = "unknown";
//    if(msg.type == ErrorMesage)
//    {
//        msg_code = get_errstr( msg.code );
//    }
//
//    std::string str = StringFormatStdA("CAVTK Message: type = %s (%d), code = %s (%d), flag = %d",msg_type, msg.type,msg_code, msg.code,msg.flag);
//    //MessageBoxA(NULL, str.c_str(), str.c_str(), MB_OK);
//
//    TScreenMessageForm3::MessageBox(NULL, "������!", str.c_str(),
//        SMF3_FLAG_MB_OK | SMF3_FLAG_ICONINFO | SMF3_FLAG_OK_ON_CLICK,
//        clRed, 5000, NULL);

    return true;
}
void TMainUnit::SearchMotion_(int WorkCycle) // ����� "�����"
{
    ProcMotion(MU_SIGNAL_AC_SEARCH_OR_TEST, WorkCycle);
}
void TMainUnit::TestMotion_(int WorkCycle)   // ����� "����"
{
    ProcMotion(MU_SIGNAL_AC_SEARCH_OR_TEST, WorkCycle);
}
void TMainUnit::AdjustMotion_(int WorkCycle) // ����� "���������"
{
    ProcMotion(MU_SIGNAL_AC_ADJUST, WorkCycle);
}
void TMainUnit::TuneMotion_(int WorkCycle) // ����� "���������"
{
    ProcMotion(MU_SIGNAL_AC_TUNE, WorkCycle);
}
void TMainUnit::ManualMotion_(int WorkCycle) // ����� "������"
{
    ProcMotion(MU_SIGNAL_AC_MANUAL, WorkCycle);
}

void TMainUnit::BackButton()
{
    if ((AutoconMain->Rep) && (!AutoconMain->SkipRep))
    {
        TRACE("if ((AutoconMain->Rep) && (!AutoconMain->SkipRep))");
        if (AutoconMain->GetFlags() &  AM_FLAG_TAKE_RAIL_PHOTO) // �������� - ������ ����
        {
            TRACE("FotoForm->ShowModal()");
            FotoForm->ShowModal();
            TRACE("AutoconMain->Rep->Photo = new TJPEGImage;");
            AutoconMain->Rep->Photo = new TJPEGImage;
            AutoconMain->Rep->Photo->Assign(FotoForm->Image1->Picture);
        }
    }

    if (AutoconMain->GetFlags() & AM_FLAG_SHOW_TEST_RAIL_MSG) // ���� - ����� �������� �����
    {
        this->GetMessages(TMessagesType::mtInfo, "��������� �����, �������� �������� �����.", GetTickCount());
    }

    if (AutoconMain->GetFlags() & AM_FLAG_TUNE_STATE_ON_END)
    {
        AutoconMain->ac->TuneSetState(tmWorkBreak);
    }
}

void __fastcall TMainUnit::ChangeModeTimerTimer(TObject *Sender)
{
    static int ptcnt = 0;

    if (ChangeModeFlag && (AutoconMain) && (AutoconMain->ac))
	{
        bMessageShowState = true;
        WaitPanel->Visible = true;
        if(pTopFrame)
            pTopFrame->Visible = false;
        ptcnt++;
        if(ptcnt > 5)
            ptcnt = 0;
        UnicodeString str = "�������� ���������� ���������� ���������";
        for(int i = 0; i < ptcnt; i++)
            str += L'.';
        for(int i = ptcnt; i < 5; i++)
            str += L' ';
        WaitLabel->Caption = str;

        //msgCs.Enter();
        //if(currMessage.Length())
        //{
        //    MessageLabel->Caption = currMessage;
        //    currMessage = "";
        //}
        //msgCs.Release();

		if (AutoconMain->ac->SetMode(acReady) && (AutoconMain->ac->GetMode() == acReady))
		{
            bMessageShowState = false;
            WaitPanel->Visible = false;
			this->Visible = false;
			ChangeModeFlag = false;
            ChangeModeTimer->Enabled = false;
		}
	}
}
//---------------------------------------------------------------------------
void TMainUnit::DoMainUnitDestroy()
{
    //this->Visible = false;
    ChangeModeFlag = true;
    ChangeModeTimer->Enabled = true;
}

void TMainUnit::deleteLater(TBasicFrame* pFrame)
{
    TRACE("Call delete later: %s",AnsiString(pFrame->GetFrameName()).c_str());
    pFrame->DoMFrameDestroy(NULL);
    deleteLaterFrames.push_back(pFrame);
    DeleteLaterTimer->Enabled = true;
}
void TMainUnit::updateRemove()
{
    std::list<TBasicFrame*>::iterator it = deleteLaterFrames.begin();
    while(it != deleteLaterFrames.end())
    {
        TRACE("Delete frame: %s",AnsiString((*it)->GetFrameName()).c_str());
        delete (*it);
        it = deleteLaterFrames.erase(it);
    }
}
void __fastcall TMainUnit::DeleteLaterTimerTimer(TObject *Sender)
{
    updateRemove();
    DeleteLaterTimer->Enabled = false;
}
//---------------------------------------------------------------------------

void TMainUnit::_setRailBlockState(bool bVal)
{
    TBitmap* pBitmap = new TBitmap();
    if(bVal)
    {
        RailBlockStateImages->GetBitmap(2, pBitmap);
        RailBlockStateLabel->Caption = "���������� ������: ������������";
    }
    else
    {
        if(!bRailBlockCriticalMode)
            RailBlockStateImages->GetBitmap(1, pBitmap);
        else
            RailBlockStateImages->GetBitmap(0, pBitmap);
        RailBlockStateLabel->Caption = "���������� ������: �����������";
    }

    bRailBlockState = bVal;
    RailBlockStateImage->Picture->Assign(pBitmap);
    delete pBitmap;
}

void TMainUnit::updateRailBlockState()
{
    _setRailBlockState(false);
}
void __fastcall TMainUnit::UpdateRailBlockStateTimerTimer(TObject *Sender)
{
    updateRailBlockState();
    if(bRailBlockState)
    {
        RailBlockStateImage->Visible = true;
        RailBlockStateLabel->Font->Color = clBlack;
    }
    else
    {
        bool bVal = RailBlockStateImage->Visible;
        RailBlockStateImage->Visible = !bVal;
        if(!bVal)
            RailBlockStateLabel->Font->Color = clRed;
        else
            RailBlockStateLabel->Font->Color = clBlack;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainUnit::ShowDebugWindowsBtnClick(TObject *Sender)
{
    if(!ShowDebugWindowsBtn->Down)
    {
        LogForm->Visible = true;
        DebugStateForm->Visible = true;
    }
    else
    {
        LogForm->Visible = false;
        DebugStateForm->Visible = false;
    }
}
//---------------------------------------------------------------------------



