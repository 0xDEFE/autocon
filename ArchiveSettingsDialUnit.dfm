object ArchiveSettingsDial: TArchiveSettingsDial
  Left = 227
  Top = 108
  BorderStyle = bsNone
  Caption = 'Dialog'
  ClientHeight = 667
  ClientWidth = 973
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 973
    Height = 667
    Align = alClient
    BevelKind = bkFlat
    BevelOuter = bvNone
    TabOrder = 0
    object m_TableHeaderLabel: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 60
      Width = 963
      Height = 34
      Align = alTop
      BiDiMode = bdLeftToRight
      Caption = #1055#1086#1083#1103' '#1090#1072#1073#1083#1080#1094#1099' ('#1074#1099#1073#1088#1072#1085#1086' 0 '#1080#1079' 10):'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -28
      Font.Name = 'Myriad Pro'
      Font.Style = []
      ParentBiDiMode = False
      ParentFont = False
      ExplicitWidth = 394
    end
    object m_SelectTableBtnsPanel: TFlowPanel
      AlignWithMargins = True
      Left = 3
      Top = 100
      Width = 963
      Height = 501
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Myriad Pro'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Panel1: TPanel
      Left = 0
      Top = 604
      Width = 969
      Height = 59
      Align = alBottom
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object m_ClearBtn: TSpeedButton
        Left = 3
        Top = 3
        Width = 150
        Height = 50
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1074#1089#1077
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = m_ClearBtnClick
      end
      object m_FillBtn: TSpeedButton
        Left = 159
        Top = 3
        Width = 155
        Height = 50
        Caption = #1042#1099#1073#1088#1072#1090#1100' '#1074#1089#1077
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = m_FillBtnClick
      end
      object m_SetDefaultBtn: TSpeedButton
        Left = 320
        Top = 3
        Width = 161
        Height = 50
        Caption = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = m_SetDefaultBtnClick
      end
      object CancelBtn: TButton
        Left = 829
        Top = 1
        Width = 125
        Height = 50
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1080#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ModalResult = 2
        ParentFont = False
        TabOrder = 0
      end
      object OKBtn: TButton
        Left = 698
        Top = 1
        Width = 125
        Height = 50
        Caption = #1055#1088#1080#1085#1103#1090#1100
        Default = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ModalResult = 1
        ParentFont = False
        TabOrder = 1
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 969
      Height = 57
      Align = alTop
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object Label2: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 963
        Height = 27
        Align = alTop
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1090#1072#1073#1083#1080#1094#1099
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 226
      end
      object Panel3: TPanel
        Left = 4
        Top = 37
        Width = 373
        Height = 12
        BevelOuter = bvNone
        Color = clHotLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
      end
    end
  end
end
