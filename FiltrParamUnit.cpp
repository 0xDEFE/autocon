//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ConfigUnit.h"
extern cACConfig * ACConfig;

#include "FiltrParamUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFiltrParamForm *FiltrParamForm;
//---------------------------------------------------------------------------
__fastcall TFiltrParamForm::TFiltrParamForm(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------


void __fastcall TFiltrParamForm::FormShow(TObject *Sender)
{
  MinLenEdit->Value = ACConfig->MinLenEdit_;
  MaxSpace->Value = ACConfig->MaxSpace_;
  DelayDelta->Value = ACConfig->DelayDelta_;
  SameCount->Value = ACConfig->SameCount_;
}
//---------------------------------------------------------------------------

void __fastcall TFiltrParamForm::Button1Click(TObject *Sender)
{
  ACConfig->MinLenEdit_  = MinLenEdit->Value;
  ACConfig->MaxSpace_    = MaxSpace->Value   ;
  ACConfig->DelayDelta_  = DelayDelta->Value ;
  ACConfig->SameCount_   = SameCount->Value  ;
  ACConfig->UseFiltr_   = true;

}
//---------------------------------------------------------------------------

