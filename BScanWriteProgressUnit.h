//---------------------------------------------------------------------------

#ifndef BScanWriteProgressUnitH
#define BScanWriteProgressUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TBScanWriteProgressForm : public TForm
{
__published:	// IDE-managed Components
    TPanel *Panel1;
    TLabel *Label1;
    TPanel *Panel2;
    TLabel *Label2;
    TBitBtn *CloseBtn;
    TProgressBar *ProgressBar1;
    void __fastcall CloseBtnClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
private:	// User declarations
    bool bCloseFlag;
public:		// User declarations
    __fastcall TBScanWriteProgressForm(TComponent* Owner);


    void SetPercent(int val);
    bool IsCloseBtnClicked();
};
//---------------------------------------------------------------------------
extern PACKAGE TBScanWriteProgressForm *BScanWriteProgressForm;
//---------------------------------------------------------------------------
#endif
