//---------------------------------------------------------------------------

#ifndef LogUnitH
#define LogUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>

//Added by KirillB
#include "Utils.h"
//---------------------------------------------------------------------------
class TLogForm : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel2;
	TPanel *Panel6;
	TPanel *Panel48;
	TSpeedButton *LogAScan;
	TSpeedButton *LogBScan;
	TSpeedButton *LogPathEncoder;
	TSpeedButton *LogIn;
	TSpeedButton *LogOut;
	TSpeedButton *LogState;
	TSpeedButton *LogAScanMeasure;
	TSpeedButton *LogASD;
	TSpeedButton *LogBScan2;
	TSpeedButton *LogNoBody;
	TSpeedButton *SpeedButton2;
	TSpeedButton *SpeedButton5;
	TSpeedButton *SpeedButton6;
	TButton *Button12;
	TPanel *Panel27;
	TPanel *Panel36;
	TPanel *Panel45;
	TCheckBox *CheckBoxOneMessage;
	TPanel *Panel49;
	TCheckBox *CheckBox1;
	TTimer *UpdateLogTimer;
	TListBox *PersistDataListBox;
	TSplitter *Splitter1;
    TSpeedButton *LogSerialNumber;
    TSpeedButton *LogFWver;
    TMemo *Memo1;
    TScrollBar *ScrollBar1;
    TSpeedButton *UMUManage;
    TSpeedButton *LoggerMsgBtn;
	void __fastcall Button12Click(TObject *Sender);
	void __fastcall CheckBox1Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall UpdateLogTimerTimer(TObject *Sender);
    void __fastcall ScrollBar1Change(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
private:	// User declarations
	Logger::CLoggerListener logger;

public:		// User declarations

    TStringList* LogLines;
    void __fastcall AddLog(int UMUIdx, unsigned char * msg, int way);
    void updateUMULog();
    int LastLogLinesCount;

    int SaveWidth;
	int SaveHeight;
	__fastcall TLogForm(TComponent* Owner);

    void __fastcall TLogForm::AddText(UnicodeString Str);
};
//---------------------------------------------------------------------------
extern PACKAGE TLogForm *LogForm;
//---------------------------------------------------------------------------
#endif
