//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FotoUnit.h"

#include "Autocon.inc"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFotoForm *FotoForm;
//---------------------------------------------------------------------------
__fastcall TFotoForm::TFotoForm(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFotoForm::Button1Click(TObject *Sender)
{
     int addr;
     int size;
     UnicodeString ip;

     //TJPEGImage * Jpg = new TJPEGImage; <-- not used
     TMemoryStream * Str = new TMemoryStream;

     if (Edit1->ItemIndex == 0) ip = "192.168.100.10"; else ip = "192.168.100.11";

     if (GetPhoto(ip, &addr, &size))
     {
       Label1->Caption = L"������: OK ";
       Str->SetSize(size);
       memcpy(Str->Memory, (void*)addr, size);
       TJPEGImage *ggg=new TJPEGImage;
       ggg->LoadFromStream(Str);
       Image1->Picture->Assign(ggg);
     }  else Label1->Caption = L"������: ������ ";
}
//---------------------------------------------------------------------------
void __fastcall TFotoForm::BackButtonClick(TObject *Sender)
{
    this->Visible = false;
}
//---------------------------------------------------------------------------
void __fastcall TFotoForm::Edit1Change(TObject *Sender)
{
    Button1Click(NULL);
    ACConfig->FotoSide = (bool)Edit1->ItemIndex;
}
//---------------------------------------------------------------------------
void __fastcall TFotoForm::FormShow(TObject *Sender)
{
    Edit1->ItemIndex = (int)ACConfig->FotoSide;
    #ifndef NO_CAMERA_PHOTO
    Button1Click(NULL);
    #endif
}
//---------------------------------------------------------------------------


