﻿/**
 * @file BasicFrameUnit.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef BasicFrameUnitH
#define BasicFrameUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>

/** \enum eMainUnitSignalType
    \brief Содержит типы сигналов, пересылаемых из #MainUnit.
 */
enum eMainUnitSignalType
{
    MU_SIGNAL_FIRST = 0,
    MU_SIGNAL_UMU_ASCAN_MEAS    = 0x01,
    MU_SIGNAL_UMU_ASCAN_DATA    = 0x02,
    MU_SIGNAL_UMU_TVG_DATA      = 0x04,
    MU_SIGNAL_UMU_ALARM_DATA    = 0x08,
    MU_SIGNAL_UMU_BSCAN2_DATA   = 0x10,
    MU_SIGNAL_UMU_MSCAN2_DATA   = 0x20,
    MU_SIGNAL_UMU_ARRAY         = 0x40,
    MU_SIGNAL_AC_SEARCH_OR_TEST = 0x80,
    MU_SIGNAL_AC_ADJUST         = 0x100,
    MU_SIGNAL_AC_TUNE           = 0x200,
    MU_SIGNAL_AC_MANUAL         = 0x400,
    MU_SIGNAL_FORM_SHORTCUT     = 0x800
};

/** \enum eMotionState
    \brief Состояние кареток
 */
enum eMotionState
{
    MS_MOTION_BEGIN,        //!< Движение началось
    MS_MOTION_IN_PROCESS,   //!< Движение идет
    MS_MOTION_END           //!< Движение закончилось
};

#include "AutoconMain.h"
//---------------------------------------------------------------------------


class TMainUnit;
/** \class TBasicFrame
    \brief Базовый класс для стекового оконного интерфейса.
 */
class TBasicFrame : public TFrame
{
__published:	// IDE-managed Components
public:		// User declarations
    __fastcall TBasicFrame(TComponent* Owner);
    virtual UnicodeString GetFrameName() {return L"Unknown";};
    virtual void SetFrameFlags(DWORD flags) {};
    virtual DWORD GetFrameFlags() {return frameFlags;};
    virtual DWORD GetAquiredSignals() {return aquiredSignalsFlags;};
    TWinControl* GetConnectedOwner() {return pConnectedOwner;};

    bool SetAcState(TACState state, DWORD timeout = 0xFFFFFFFF);

    /** Функция инициализации
        \param pParent Родительский фрейм
        \param pMainUnitForm Родительская форма
        \param frameFlags Флаги фрейма (индивидуальны для каждого класса, наследующегося от TBasicFrame)
        \param bModal При старте фрейма сообщения остальным фреймам идти не будут
        \param pConnectedOwner Владелец child фреймов (по умолчанию - this);
     */
    void Initialize(TBasicFrame* pParent, TMainUnit* pMainUnitForm, DWORD frameFlags, bool bModal,TWinControl* pConnectedOwner = NULL)
    {
        this->pParentFrame = pParent;
        this->pMainUnitForm = pMainUnitForm;
        this->frameFlags = frameFlags;
        this->bModal = bModal;
        this->pConnectedOwner = pConnectedOwner;
    };

    virtual void OnMFrameAttach(TBasicFrame* pMain) {}; //!< Вызывается когда текущая форма становится активной
    virtual void OnMFrameDetach(TBasicFrame* pMain) {}; //!< Вызывается когда текущая форма становится неактивной
    virtual void OnMFrameCreate(TBasicFrame* pMain) {}; //!< Вызывается когда текущая форма становится создается
    virtual void OnMFrameDestroy(TBasicFrame* pMain){}; //!< Вызывается когда текущая форма становится удаляется

    virtual void OnUMUSignal(eMainUnitSignalType sigtype, sPtrListItem& sig);  //!< Вызывается по сигналу от БУМа
    virtual void OnUMUArray(ringbuf<sPtrListItem>& sigList);                   //!< Вызывается по массиву сигналов от БУМа
    virtual void OnAcMotion(eMainUnitSignalType sigtype, int WorkCycle, eMotionState state, bool bStateChanged); //!< Вызывается по движению кареток
    virtual void OnFormShortCut(TWMKey &Msg, bool &Handled); //!< Вызывается сочетанию клавиш (?)

    bool isModal() {return bModal;};

protected:
    TBasicFrame* pParentFrame;
    TMainUnit* pMainUnitForm;
    DWORD frameFlags;
    DWORD aquiredSignalsFlags;
    bool bModal;
    TWinControl* pConnectedOwner;

    void DoMFrameAttach(TBasicFrame* pMain);
    void DoMFrameDetach(TBasicFrame* pMain);
    void DoMFrameCreate(TBasicFrame* pMain);
    void DoMFrameDestroy(TBasicFrame* pMain);
    friend class TMainUnit;

//Controller part
protected:
    void attachFrame(TBasicFrame* pFrame);
    void detachFrame();
    void attachFrame(TBasicFrame* pFrame, TWinControl* pOwner);
    void detachFrame(TBasicFrame* pFrame, TWinControl* pOwner);
    void __pushFrame(TBasicFrame* pFrame, DWORD frameFlags, bool bModal = true);
    void __connectFrame(TBasicFrame* pFrame, DWORD frameFlags,TWinControl* pOwner);
protected:
    TBasicFrame* currFrame;
    TWinControl* defaultOwner;
    std::deque<TWinControl*> ownersStack;
    std::deque<TBasicFrame*> frameStack;
    std::list<TBasicFrame*> connectedFrames;
public:
    bool bPopOnEmpty;

    /** \brief Создает экземпляр шаблонного типа T и помещает его на верхушку стека (с отображением поверх текущей формы)
        \param T Шаблонный тип формы (должен быть наследован от TBasicFrame)
        \param frameFlags Флаги формы
        \returns Указатель на созданный экземпляр формы
     */
    template<typename T>
    T* pushFrame(DWORD frameFlags, bool bModal = true)
    {
        T* pFrame = new T(NULL);
        __pushFrame(pFrame,frameFlags,bModal);
        return pFrame;
    }
    //! Убирает ферхнюю форму из стека
    void popFrame();

    /** \brief Не используется
        \deprecated
     */
    template<typename T>
    T* connectFrame(DWORD frameFlags,TWinControl* pOwner)
    {
        T* pFrame = new T(NULL);
        __connectFrame(pFrame,frameFlags,pOwner);
        return pFrame;
    }
    /** \brief Не используется
        \deprecated
     */
    void disconnectFrame(TBasicFrame* pFrame);
};
//---------------------------------------------------------------------------
//extern PACKAGE TBasicFrame *MainUnitFrame;
//---------------------------------------------------------------------------
#endif
