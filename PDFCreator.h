﻿/**
 * @file PDFCreator.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef PDFCreatorH
#define PDFCreatorH
//---------------------------------------------------------------------------
#include <hpdf.h>
#include "ParseSVG.h"
#include <map>
#include <vector>
#include <string>
#include <functional>
#include <vcl.h>
#include <setjmp.h>

typedef Line_attribute sPDFLine; //!< Описывает линию в PDF документе.

/** \struct sPDFText
    \brief Описывает текстовый элемент в PDF документе.
 */
struct sPDFText
{
	sPDFText()
	{
		font = NULL;
        transform[0] = transform[3] = 1;
	};

	sPDFText(const Text_attribute& other)
	{
		memcpy(this,&other,sizeof(other.id));
		this->x = other.x;
		this->y = other.y;
		this->text = other.text;
		this->font_size = other.font_size;
		memcpy(transform,other.transform,sizeof(transform));
		font = NULL;
	};

	char id[32]; //!< Текстовый идентификатор
	float x;     //!< Позиция по горизонтали
	float y;     //!< Позиция по вертикали
	AnsiString text;  //!< Отображаемый текст
	int font_size;    //!< Размер шрифта
	float transform[6]; //!< Матрица трансформации (поворот + смещение + масштаб)
	//end block
	HPDF_Font font;   //!< Тип шрифта
};

/** \struct sPDFRect
    \brief Описывает прямоугольник в PDF документе.
    Может содержать текст и изображение.
 */
struct sPDFRect
{
	sPDFRect()
	{
		image = NULL;
		bRotate=false;
        bDrawRect=false;
        transform[0] = transform[3] = 1;
	};

	sPDFRect(const Rect_attribute& other)
	{
		memcpy(this,&other,sizeof(other));
		image = NULL;
		bRotate=false;
        bDrawRect=false;
	};

	char id[32]; //!< Текстовый идентификатор
	float x;     //!< Позиция по горизонтали
	float y;     //!< Позиция по вертикали
	float width;   //!< Ширина
	float height;  //!< Высота
	float transform[6]; //!< Матрица трансформации (поворот + смещение + масштаб)
	//end block
	TBitmap* image; //!< Изображение в прямоугольнике (может быть NULL)
	bool bRotate;   //!< Повернуть для горизонтальной ориентации страницы
    bool bDrawRect; //!< Рисовать рамку прямоугольника

    AnsiString text; //!< Текст в прямоугольнике
    int font_size;   //!< Размер шрифта
    HPDF_Font font;  //!< Тип шрифта
};

class PDFCreator;
/** \class PDFPage
    \brief Описывает страницу PDF документа.
    Может содержать элементы #sPDFRect и #sPDFText.
 */
class PDFPage
{
public:
	PDFPage()
	{
		width = height = 0;
	};
	~PDFPage() {};

	bool LoadLayout(const char* path); //!< Load page layout from svg file

	sPDFRect* GetRectangle(const char* name);  //!< Получить прямоугольник по текстовому идентификатору
	sPDFText* GetText(const char* name);  //!< Получить текстовый элемент по текстовому идентификатору
	sPDFLine* GetLine(UINT index);   //!< Получить линию по текстовому идентификатору

    /** Добавляет новый прямоугольник на страницу
        \param rect структура, описывающая прямоугольник
        \renurn Идентификатор прямоугольника (хэш от имени)
     */
	UINT AddRectangle(sPDFRect& rect);
    /** Добавляет новый текстовый элемент на страницу
        \param rect структура, описывающая прямоугольник
        \renurn Идентификатор (хэш от имени)
     */
	UINT AddText(sPDFText& rect);
    /** Добавляет новую линию на страницу
        \param rect структура, описывающая прямоугольник
        \renurn Идентификатор номер по порядку
     */
	UINT AddLine(sPDFLine& rect);

	float GetWidth() {return width;};
	float GetHeight() {return height;};

protected:
	friend class PDFCreator;
	std::map<UINT, sPDFRect> rectangle_map;
	std::map<UINT, sPDFText> text_map;
	std::vector<sPDFLine> line_vector;

	float width;
	float height;
};

/** \class PDFCreator
    \brief Описывает PDF документ.
    Может содержать элементы #PDFPage.
 */
class PDFCreator
{
public:
	PDFCreator();
	~PDFCreator();

	int LoadPage( const char* path); //!< Загружает страницу из svg файла
	void ClearPages();               //!< Удаляет все страницы

	PDFPage* GetPage( UINT index );  //!< Возвращает страницу с заданным номером
	UINT GetPageCount();             //!< Возвращает кол-во страниц

    /** Сохраняет набор страниц как PDF документ.
        \param path Путь к файлу
        \param fontFolder Путь к папке со шрифтами
        \renurn true если сохранение удалось, в обратном случае - false.
     */
	bool SaveAsPDF(const char* path, const char* fontFolder = NULL);

protected:
	bool _drawRects(HPDF_Page page, PDFPage& layout);
	bool _drawText(HPDF_Page page, PDFPage& layout);
	bool _drawLines(HPDF_Page page, PDFPage& layout);
protected:
	std::vector< PDFPage > pages;
	HPDF_Font defaultFont;
	HPDF_Doc  pdf;
	float ppi_scale;
};

#endif
