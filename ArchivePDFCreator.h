//---------------------------------------------------------------------------

#include <Vcl.Graphics.hpp>
#include <System.Types.hpp>

#ifndef ArchivePDFCreatorH
#define ArchivePDFCreatorH

#include "PDFCreator.h"
#include "DrawRailUnit.h"
#include "BScanPainter.h"
//---------------------------------------------------------------------------

class cArchivePDFCreator
{
public:
	cArchivePDFCreator();
	~cArchivePDFCreator();

    bool save(const char* outPath, unsigned int ThIndex, bool bTitlePage = true, bool bBScanShort = true, bool bBScanDetail = true, bool bHScanPages = true, bool bChannelsTable = false, bool bLinearityPage = false);

protected:
	bool init();
	bool initPathes(); //init alive file pathes
	void release();

	void addTitlePage(const char* titlePagePath);
	void addBScanPage(const char* bscanPagePath, unsigned int ThIndex, int pageIndex = 0, int firstScanIndex = -1, int secondScanIndex = -1);
	void addBScanPageShort(const char* bscanPagePath,unsigned int ThIndex);
	void addHandScanPage(const char* hscanPagePath, bool bTitle,int imgFirstCnt,
				TJPEGImage* firstScanImg = NULL, TJPEGImage* secondScanImg = NULL,
				TJPEGImage* thirdScanImg = NULL, TJPEGImage* fourthScanImg = NULL);
    void addHandBScanPage(const char* hscanPagePath, bool bTitle,int scanStartIndex, int itemCount);
    bool addChannelsTablePage(const char* tablePagePath, bool bTitle, int startChannelNumber, int startChannelId, int* pEndChannelNumberOut, int* pEndChannelIdOut);
    void addLinearityPage(const char* linearityPagePath, TJPEGImage* firstImg = NULL, TJPEGImage* secondImg = NULL,TJPEGImage* thirdImg = NULL);
protected:
	bool bInitialized;

	AnsiString titlePagePath;
	AnsiString bscanPagePath;
    AnsiString hscanPagePath;
    AnsiString hscanBScanPagePath;
	AnsiString bscanPageShortPath;
    AnsiString channelsTablePath;
	AnsiString endingPagePath;
    AnsiString linearityPagePath;
	AnsiString fontsPath;

    int titleNumber;
    int subTitleNumber;


	UINT RailImageUnvisibleFlags[8];
	//Temporary bitmaps
	TBitmap* RailImageBitmaps[8];
	TBitmap* ChannelsImageBitmaps[8];
	TBitmap* pCameraImageBitmap;
    std::vector<TBitmap*> temporaryBitmaps;

	cBScanPainter painter;
	PDFCreator pdfDoc;
};

#endif
