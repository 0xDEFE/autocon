﻿/**
 * @file ScreenMessageUnit3.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef ScreenMessageUnit3H
#define ScreenMessageUnit3H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Buttons.hpp>
//---------------------------------------------------------------------------

#include <vector>

/** \enum eSMF3_Flags
    \brief Флаги настроек и элементов окна вывода сообщения
 */
enum eSMF3_Flags
{
    SMF3_FLAG_MB_OK         = 0x01, //!< Кнопка ОК
    SMF3_FLAG_MB_CANCEL     = 0x02, //!< Кнопка Отмена
    SMF3_FLAG_MB_YESNO      = 0x04, //!< Кнопки Да\Нет
    SMF3_FLAG_ICONERROR     = 0x08, //!< Иконка ошибки
    SMF3_FLAG_ICONINFO      = 0x10, //!< Иконка информации
    SMF3_FLAG_ICONWARNING   = 0x20, //!< Иконка предупреждения
    SMF3_FLAG_OK_ON_CLICK   = 0x80, //!< Ок по нажатию на окно
    SMF3_FLAG_SELECT_MODE   = 0x100,//!< Режим выбора
    SMF3_FLAG_NONMODAL      = 0x200,//!< Не модальное окно
};

/** \enum eSMF3_Result
    \brief Флаги возвращаемого значения окна вывода сообщения
 */
enum eSMF3_Result
{
    SMF3_RESULT_UNKNOWN = 0,  //!< Не известно
    SMF3_RESULT_OK,           //!< ОК
    SMF3_RESULT_YES,          //!< Да
    SMF3_RESULT_NO,           //!< Нет
    SMF3_RESULT_CANCEL,       //!< Отмена
};

/** \struct sSMF3_Answer
    \brief Описывает варианты выбора
 */
struct sSMF3_Answer
{
    sSMF3_Answer()
    {
        retCode = -1;
        text = "unknown";
        pGlyph = NULL;
    };
    int retCode;        //!< Код возврата при выборе
    UnicodeString text; //!< Текст элемента
    TBitmap* pGlyph;    //!< Иконка элемента
};

/** \struct sSMF3_Data
    \brief Описывает данные сообщения
 */
struct sSMF3_Data
{
    sSMF3_Data()
    {
        flags =  SMF3_FLAG_MB_OK | SMF3_FLAG_ICONINFO | SMF3_FLAG_OK_ON_CLICK;
        color = clHotLight;
        timeout = 5000;
        caption = "Unknown";
        text = "Unknown";
    };

    eSMF3_Flags flags;  //!< Флаги сообщения
    TColor color;       //!< Цвет
    DWORD timeout;      //!< Время показа
    UnicodeString caption; //!< Заголовок
    UnicodeString text;    //!< Текст
    std::vector<sSMF3_Answer> answers; //!< Варианты ответа (список, из которого пользователь выбирает)
};

/** \class TScreenMessageForm3
    \brief Расширенный вариант формы вывода сообщения для пользовотеля (по сравнению с TScreenMessageForm2). Содержит методы для предоставления пользователю выбора из нескольких вариантов.
    \note Не дописан до конца
 */
class TScreenMessageForm3 : public TForm
{
__published:	// IDE-managed Components
    TPanel *Panel1;
    TGridPanel *GridPanel1;
    TLabel *CaptionLabel;
    TPaintBox *TimeoutPB;
    TGridPanel *GridPanel2;
    TPanel *captionPanel;
    TLabel *TextLabel;
    TGridPanel *AnswerGridPanel;
    TPanel *ResultBtnsPanel;
    TBitBtn *BtnCancel;
    TBitBtn *BtnNo;
    TBitBtn *BtnOkYes;
    TTimer *UpdateTimer;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall BtnOkYesClick(TObject *Sender);
    void __fastcall BtnNoClick(TObject *Sender);
    void __fastcall BtnCancelClick(TObject *Sender);
    void __fastcall UpdateTimerTimer(TObject *Sender);
    void __fastcall OnAnswerClick(TObject *Sender);
private:	// User declarations
    sSMF3_Data data;
    int selectedCode;
    std::vector<TBitBtn*> ansButtons;
    DWORD StartTime;
public:		// User declarations
    __fastcall TScreenMessageForm3(sSMF3_Data& data, TComponent* Owner);
    int execute();  //!< Показать форму вывода сообщений
    int getSelectedCode(); //!< Получить код выбранного пользователем варианта

    /** Функция вывода формы сообщения (ASCII)
        \param Owner Владелец формы
        \param Caption Заголовок сообщения
        \param Text Текст сообщения
        \param flags Флаги сообщения
        \param color Цвет окантовки формы
        \param timeout Время вывода сообщения
        \param pAnswers Указатель на массив с вариантами ответа
        \param pRetAnswer Указатель на переменную, в которую будет скопирован код ответа
        \return ModalResult
     */
    static int MessageBoxA(TComponent* Owner, const char* Caption, const char* Text,
                            unsigned int flags = SMF3_FLAG_MB_OK | SMF3_FLAG_ICONINFO | SMF3_FLAG_OK_ON_CLICK,
                            TColor color = clHotLight, DWORD timeout = 0xFFFFFFFF,
                            std::vector<sSMF3_Answer>* pAnswers = NULL, int* pRetAnswer = NULL);
    /** Функция вывода формы сообщения (WCHAR)
        \param Owner Владелец формы
        \param Caption Заголовок сообщения
        \param Text Текст сообщения
        \param flags Флаги сообщения
        \param color Цвет окантовки формы
        \param timeout Время вывода сообщения
        \param pAnswers Указатель на массив с вариантами ответа
        \param pRetAnswer Указатель на переменную, в которую будет скопирован код ответа
        \return ModalResult
     */
    static int MessageBoxW(TComponent* Owner, const wchar_t* Caption, const wchar_t* Text,
                            unsigned int flags = SMF3_FLAG_MB_OK | SMF3_FLAG_ICONINFO | SMF3_FLAG_OK_ON_CLICK,
                            TColor color = clHotLight, DWORD timeout = 0xFFFFFFFF,
                            std::vector<sSMF3_Answer>* pAnswers = NULL, int* pRetAnswer = NULL);
    static void ErrorMessageA( const char* Caption, const char* Text, DWORD timeout = 0xFFFFFFFF );
    static void InfoMessageA( const char* Caption, const char* Text, DWORD timeout = 0xFFFFFFFF);
    static void ErrorMessageW( const wchar_t* Caption, const wchar_t* Text, DWORD timeout = 0xFFFFFFFF );
    static void InfoMessageW( const wchar_t* Caption, const wchar_t* Text, DWORD timeout = 0xFFFFFFFF);
};
//---------------------------------------------------------------------------
//extern PACKAGE TScreenMessageForm3 *ScreenMessageForm3;
//---------------------------------------------------------------------------
#endif
