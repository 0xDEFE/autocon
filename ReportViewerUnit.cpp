//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AutoconMain.h"
#include "BScanPainter.h"
#include "AScanViewUnit.h"
#include "ArchiveDefinitions.h"
#include "ArchiveUnit.h"
#include "ReportViewerUnit.h"
#include "CalibrationFormUnit.h"
//#include "BScanMeasureFormUnit.h"
#include "ScreenMessageUnit3.h"
#include "MeasureSelChannelFormUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TReportViewerForm *ReportViewerForm;
//---------------------------------------------------------------------------
__fastcall TReportViewerForm::TReportViewerForm(TComponent* Owner)
    : TForm(Owner)
{
    ViewFlags = 0;

    bsl.SetViewDelayZone(0, 40);
    bsl.SetViewCrdZone(0, BScanHandPBox->Width);
    bsl.SetSize(TSize(BScanHandPBox->Width, BScanHandPBox->Height));
    BScanHandScale = 1;

    BScanPage->TabVisible = false;
    BScanHandPage->TabVisible = false;
    AScanImagesPage->TabVisible = false;
    LargePhotoPage->TabVisible = false;
    EmptyTab->TabVisible = false;
    ChannelParamsPage->TabVisible = false;

    painter.init(AutoconMain->Rep);
    painter.setHideChannelsMode(CFM_HIDE_NONE);

    pArchive = NULL;

    editedRegionIdx = -1;
}
//---------------------------------------------------------------------------

DWORD GetReportContentFlags(const cJointTestingReport* rep)
{
    DWORD flags = 0;
    if((rep->Header.FileType == 1) || (rep->Header.FileType == 2))
        flags |= REPVIEW_FLAG_BSCAN;
    if(rep->GetHandScanCount() != 0)
        flags |= REPVIEW_FLAG_BSCAN_HAND;
    if(!rep->ScreenShotHandScanList.empty())
        flags |= REPVIEW_FLAG_ASCAN;
    if(rep->PhoneScreenShot || rep->PhoneScreenShot2 || rep->PhoneScreenShot3)
        flags |= REPVIEW_FLAG_PHONE;
    if(rep->Photo)
        flags |= REPVIEW_FLAG_CAMERA;
    flags |= REPVIEW_FLAG_CHANNELS;
    return flags;
}

void __fastcall TReportViewerForm::FormShow(TObject *Sender)
{
	painter.pScanDraw->Th = 0;

	MeasureBtn->Visible = (AutoconMain->Rep->Header.FileType == 2); // FDV

    setReportFlags( GetReportContentFlags(AutoconMain->Rep) );
}
//---------------------------------------------------------------------------
void TReportViewerForm::setReportFlags(DWORD flags)
{
    BScanBtn->Visible = flags & REPVIEW_FLAG_BSCAN;
    BScanHandBtn->Visible = (flags & REPVIEW_FLAG_BSCAN_HAND);
    AScanImagesBtn->Visible = (flags & REPVIEW_FLAG_ASCAN);
    LinearityBtn->Visible = flags & REPVIEW_FLAG_PHONE;
    ShowCameraPhotoBtn->Visible = flags & REPVIEW_FLAG_CAMERA;
    ChannelsTableBtn->Visible = flags & REPVIEW_FLAG_CHANNELS;

    if(flags & REPVIEW_FLAG_BSCAN)
        PageControl1->ActivePageIndex = 0;
    else if(flags & REPVIEW_FLAG_ASCAN)
        PageControl1->ActivePageIndex = 1;
    else if(flags & REPVIEW_FLAG_BSCAN_HAND)
        PageControl1->ActivePageIndex = 3;
    else if(flags & REPVIEW_FLAG_PHONE)
        PageControl1->ActivePageIndex = 2;
    else if(flags & REPVIEW_FLAG_CAMERA)
        PageControl1->ActivePageIndex = 2;
    else if(flags & REPVIEW_FLAG_CHANNELS)
        PageControl1->ActivePageIndex = 5;
    else PageControl1->ActivePageIndex = 4;

    ViewFlags = flags;
    UpdateReport();
}

void TReportViewerForm::UpdateReport()
{
    if(ViewFlags & REPVIEW_FLAG_BSCAN)
    {
        UpdateBScan(false);
        DrawBScan();
    }
    if(ViewFlags & REPVIEW_FLAG_BSCAN_HAND)
    {
        BScanHandCombo->Clear();
        for(int i = 0; i < AutoconMain->Rep->GetHandScanCount(); i++)
        {
            BScanHandCombo->Add(StringFormatU(L"������ �%d",i+1));
        }
        if(BScanHandCombo->Items->Count)
            BScanHandCombo->ItemIndex = 0;

        UpdateBScanHand();
    }
    if(ViewFlags & REPVIEW_FLAG_ASCAN)
        UpdateAScan();

    if(PageControl1->ActivePageIndex == 2)
    {
        if(ViewFlags & REPVIEW_FLAG_PHONE)
            UpdatePhonePhoto();
        else if(ViewFlags & REPVIEW_FLAG_CAMERA)
            UpdateCameraPhoto();
    }

    if(ViewFlags & REPVIEW_FLAG_CHANNELS)
        UpdateChannelsTable();

    m_ReportInfoLabel->Caption =
        StringFormatU(L"�������� �%d (%s)",AutoconMain->Rep->Header.ReportNumber,
                                            DateTimeToStr(AutoconMain->Rep->Header.DateTime));
}

void TReportViewerForm::UpdateChannelsTable()
{
    std::vector<sCalibData> channelsData;

    sChannelDescription chanDesc;
    sScanChannelDescription scanChanDesc;
    for (unsigned int i = 0; i < AutoconMain->Table->Count(); i++)
    {
        CID chId;
        if(!AutoconMain->Table->CIDByIndex(&chId,i))
            continue;
        if(!AutoconMain->Table->ItemByCID(chId, &chanDesc))
            continue;
        bool bIsScanChannel = AutoconMain->Config->ScanChannelExists(chId);
        bool bIsHandChannel = AutoconMain->Config->HandChannelExists(chId);
        //if(!(bIsScanChannel || bIsHandChannel))
        if(!bIsScanChannel)
            continue;

        sCalibData channel;
        channel.ID = chId;
        channel.itemNumber = channelsData.size() + 1;
        channel.TVG = AutoconMain->Rep->GetChannelParams(chId,1).TVG;
        channel.PrismDelay = float(AutoconMain->Rep->GetChannelParams(chId,1).PrismDelay) / 10.f;
        channel.GateCount = chanDesc.cdGateCount;

        for(int j = 0; j < channel.GateCount; j++)
        {
            channel.StGate[j] = AutoconMain->Rep->GetChannelParams(chId,j+1).StGate;
            channel.EdGate[j] = AutoconMain->Rep->GetChannelParams(chId,j+1).EdGate;
        }

        for(int j = 0; j < channel.GateCount; j++)
        {
            channel.Sens[j] = AutoconMain->Rep->GetChannelParams(chId,j+1).Sens;
            channel.Gain[j] = AutoconMain->Rep->GetChannelParams(chId,j+1).Gain;
        }

        channel.ChannelName = chanDesc.Title;
        if(bIsHandChannel)
            channel.ChannelName = L"���. " + channel.ChannelName;

        channel.KpNumber = -1;
        if(AutoconMain->Config->getFirstSChannelbyID(chId, &scanChanDesc) != -1)
        {
            channel.KpNumber = scanChanDesc.BScanGroup;
        }

        channelsData.push_back(channel);
    }

    CalibTable->ColCount = 9;
    CalibTable->RowCount = channelsData.size() + 1;

    CalibTable->Cells[0][0] = "�";
    CalibTable->Cells[1][0] = "���";
    CalibTable->Cells[2][0] = "��";
    CalibTable->Cells[3][0] = "2��";
    CalibTable->Cells[4][0] = "���";
    CalibTable->Cells[5][0] = "��";
    CalibTable->Cells[6][0] = "���";
    CalibTable->Cells[7][0] = "���. ������";
    CalibTable->Cells[8][0] = "���. ������";

    for(unsigned int i = 0; i < channelsData.size(); i++)
    {
        CalibTable->Cells[0][i+1] = IntToStr(channelsData[i].itemNumber);
        CalibTable->Cells[1][i+1] = channelsData[i].ChannelName;
        CalibTable->Cells[2][i+1] = (channelsData[i].KpNumber > 0) ? IntToStr(channelsData[i].KpNumber) : UnicodeString("--");
        CalibTable->Cells[3][i+1] = FloatToStr(channelsData[i].PrismDelay);
        CalibTable->Cells[4][i+1] = IntToStr(channelsData[i].TVG);

        CalibTable->Cells[5][i+1] = IntToStr(channelsData[i].Sens[0]);
        CalibTable->Cells[6][i+1] = IntToStr(channelsData[i].Gain[0]);
        CalibTable->Cells[7][i+1] = IntToStr(channelsData[i].StGate[0]);
        CalibTable->Cells[8][i+1] = IntToStr(channelsData[i].StGate[0]);
        if(channelsData[i].GateCount > 1)
        {
            CalibTable->Cells[5][i+1] = CalibTable->Cells[5][i+1] + ", " + IntToStr(channelsData[i].Sens[1]);
            CalibTable->Cells[6][i+1] = CalibTable->Cells[6][i+1] + ", " + IntToStr(channelsData[i].Gain[1]);
            CalibTable->Cells[7][i+1] = CalibTable->Cells[7][i+1] + ", " + IntToStr(channelsData[i].StGate[1]);
            CalibTable->Cells[8][i+1] = CalibTable->Cells[8][i+1] + ", " + IntToStr(channelsData[i].StGate[1]);
        }
    }

    AdjustChannelsTableColumnsWidth();
}

void TReportViewerForm::AdjustChannelsTableColumnsWidth()
{
    const int DEFBORDER_HOR = 12;
    const int DEFBORDER_VER = 6;

    TFont* oldFont = Canvas->Font;

	int* colWidthMax = new int[ CalibTable->ColCount ];
    int* rowHeightMax = new int[ CalibTable->RowCount ];
	memset(colWidthMax,0,sizeof(int)*CalibTable->ColCount);
	memset(rowHeightMax,0,sizeof(int)*CalibTable->RowCount);

	//Getting column title width
	Canvas->Font = CalibTable->Font;
    TStringList* pStrList = new TStringList();
	for( int i = 0; i < CalibTable->ColCount; i++)
	{
        for( int j = 0; j < CalibTable->RowCount; j++)
        {
            pStrList->Text = CalibTable->Cells[i][j];

            int maxWidth = 0;
            int maxHeight = 0;
            for(int k = 0; k < pStrList->Count; k++)
            {
                maxWidth = std::max( maxWidth,Canvas->TextWidth( pStrList->Strings[k] ));
                maxHeight += Canvas->TextHeight( pStrList->Strings[k] );
            }

            colWidthMax[i] = std::max( colWidthMax[i], maxWidth + DEFBORDER_HOR);
            rowHeightMax[j] = std::max( rowHeightMax[j], maxHeight + DEFBORDER_VER);
        }
	}

    for( int i = 0; i < CalibTable->ColCount; i++)
        CalibTable->ColWidths[i] = colWidthMax[i];

    for( int i = 1; i < CalibTable->RowCount; i++)
        CalibTable->RowHeights[i] = rowHeightMax[i];

    Canvas->Font = oldFont;
    delete pStrList;
    delete [] colWidthMax;
}

void TReportViewerForm::UpdateBScan(bool NewItemFlag)
{
    if((BScanImage->Picture->Bitmap->Width != BScanImage->Width) ||
        (BScanImage->Picture->Bitmap->Height != BScanImage->Height))
    {
        BScanImage->Picture->Bitmap->SetSize(BScanImage->Width,BScanImage->Height);

        BScanImage->Canvas->Brush->Color = clWhite;
        BScanImage->Canvas->FillRect(TRect(0,0,BScanImage->Width,BScanImage->Height));

        painter.setBScanRect(TRect(0,0,BScanImage->Width,BScanImage->Height));
        painter.applyBScanLayout();
        painter.pScanDraw->enableReportStrobeData(true);
    }

    int ValList[17] = {0, 8, 10, 12, 16, 20, 25, 32, 40, 50, 64, 80, 101, 128, 160, 202, 254};
    painter.pScanDraw->Th = ValList[ThTrackBar->Position];

    painter.updateHightlight(true); //FDV - ��� ����������� ������������ ��� �� ������� ������ ��� ��������� ���������� � ������� (����� ���� ��������������)
	painter.updateBScan(NewItemFlag);
    //painter.updateBScan(true);
}

void TReportViewerForm::DrawBScan()
{
    painter.drawBScan(BScanImage->Canvas);
}

void DrawHandBScanLine(int curHandScanIndex,PsHandSignalsOneCrd coords_arr, int coordCount, TCanvas *Canvas, int Width, int Height)
{
    int BScanCrd = 0;

    TBScanLine bsl;
    bsl.SetViewDelayZone(0, 140);
    bsl.SetViewCrdZone(0, Width);
    bsl.SetSize(TSize(Width, Height));
    //bsl.SetViewCrdZone(0, BScanHandPBox->Width);
    bsl.SetSize(TSize(Width, Height));

    const int ThValList[16] = { 8, 10, 12, 16, 20, 25, 32, 40, 50, 63, 80, 101, 127, 160, 201, 254};

    int UpperBound = 0;
    int LowerBound = 0xFFFFFFFF;

    for (int ItemIdx = 0; ItemIdx < coordCount; ItemIdx++)
        for (int SignalIdx = 0; SignalIdx < coords_arr[ItemIdx].Count; SignalIdx++)
        {
            // ����� ������ �� ������������ ���������
            int Ampl;
            int MaxDelay;
            int MaxAmpl = - 1;
            for (int AmplIdx = 6; AmplIdx < 10; AmplIdx++)
            {
                #ifdef BScanSize_12Byte
                if (AmplIdx&1) Ampl = ThValList[(coords_arr[ItemIdx].Signals[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                               Ampl = ThValList[(coords_arr[ItemIdx].Signals[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                #endif
                #ifndef BScanSize_12Byte
                Ampl = coords_arr[ItemIdx].Signals[SignalIdx].Ampl[AmplIdx];
                #endif
                if (MaxAmpl < Ampl) {
                    MaxAmpl = Ampl;
                    MaxDelay = AmplIdx;
                }
                //MaxAmpl = std::max(MaxAmpl, Ampl);
            }

            // ����� ����� �������
            if (MaxDelay != - 1)
            {

                int StDelay = - 1;
                int EndDelay = - 1;
                for (int AmplIdx = MaxDelay; AmplIdx > 0; AmplIdx--)
                {
                    #ifdef BScanSize_12Byte
                    if (AmplIdx&1) Ampl = ThValList[(coords_arr[ItemIdx].Signals[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                   Ampl = ThValList[(coords_arr[ItemIdx].Signals[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                    #endif
                    #ifndef BScanSize_12Byte
                    Ampl = coords_arr[ItemIdx].Signals[SignalIdx].Ampl[AmplIdx];
                    #endif
                    if (Ampl < 32) break;
                    StDelay = AmplIdx;
                }

                // ����� ������ �������
                for (int AmplIdx = MaxDelay; AmplIdx < 24; AmplIdx++)
                {
                    #ifdef BScanSize_12Byte
                    if (AmplIdx&1) Ampl = ThValList[(coords_arr[ItemIdx].Signals[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                   Ampl = ThValList[(coords_arr[ItemIdx].Signals[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                    #endif
                    #ifndef BScanSize_12Byte
                    Ampl = coords_arr[ItemIdx].Signals[SignalIdx].Ampl[AmplIdx];
                    #endif
                    if (Ampl < 32) break;
                    EndDelay = AmplIdx;
                }

                /*if (SelEchoIdx->ItemIndex == SignalIdx)
                {
                    SaveMaxAmpl = MaxAmpl;
                    SaveMaxDelay = MaxDelay;
                }*/

                if ((StDelay != - 1) && (EndDelay != - 1))
                {
                    int Dly = coords_arr[ItemIdx].Signals[SignalIdx].Delay;
                    bsl.SetPoint(BScanCrd++, Dly - (MaxDelay - StDelay) * 0.333, Dly + (EndDelay - MaxDelay) * 0.333, MaxAmpl);

                    UpperBound = std::max( UpperBound, int(Dly + (EndDelay - MaxDelay) * 0.333));
                    LowerBound = std::min( LowerBound, int(Dly - (MaxDelay - StDelay) * 0.333));
                }
            }
        }

    int PageSize = float(Width);//*BScanHandScale;

    //bsl.SetViewDelayZone(LowerBound, UpperBound);
    if(AutoconMain && AutoconMain->DEV)
        bsl.SetViewDelayZone(0, AutoconMain->DEV->GetAScanLen());
    else
        bsl.SetViewDelayZone(LowerBound, UpperBound);

    bsl.Draw(0, 0, Canvas);

    Canvas->Brush->Style = bsSolid;
    Canvas->Brush->Color = clBlack;
    Canvas->Pen->Style = psSolid;
    Canvas->Pen->Width = 1;
    Canvas->Pen->Color = clBlack;
    Canvas->FrameRect(TRect(0,0,Width,Height));
}

void TReportViewerForm::UpdateBScanHand()
{
    if(BScanHandCombo->ItemIndex == -1)
        return;
    int curHandScanIndex = BScanHandCombo->ItemIndex;

    sHandScanParams params;
    bool bRes = false;
    int coordCount;
    bsl.Clear();
    PsHandSignalsOneCrd coords_arr = AutoconMain->Rep->GetHandSignals( curHandScanIndex, &params, &bRes, &coordCount);

    if(!bRes || !coords_arr)
    {
        Vcl::Graphics::TTextFormat tf;
        tf.Clear();
        tf << tfCenter << tfVerticalCenter << tfSingleLine << tfNoClip;

        TRect rect(0,0,BScanHandPBox->Width, BScanHandPBox->Height);
        BScanHandPBox->Canvas->Brush->Color = clGray;
        BScanHandPBox->Canvas->FillRect(rect);
        BScanHandPBox->Canvas->Font->Size = -28;
        UnicodeString str("��� ��������");
        BScanHandPBox->Canvas->TextRect(rect,str,tf);
    }
    else DrawHandBScanLine(curHandScanIndex,coords_arr,coordCount, BScanHandPBox->Canvas, BScanHandPBox->Width, BScanHandPBox->Height);

    if(!bRes)
        return;

    //Setting labels
    m_EnterAngleLabel->Caption = IntToStr(params.EnterAngle) + L"�";
    switch( params.Method )
    {
        case imNotSet:          m_ControlMethodLabel->Caption = L"�� �����"; break;
	    case imEcho:            m_ControlMethodLabel->Caption = L"��� �����"; break;
	    case imMirrorShadow:    m_ControlMethodLabel->Caption = L"��������� ������� �����"; break;
	    case imMirror:          m_ControlMethodLabel->Caption = L"���������� �����"; break;
	    case imShadow:          m_ControlMethodLabel->Caption = L"������� �����"; break;
    }
    m_ControlSurfaceLabel->Caption = IntToStr(params.ScanSurface);
    m_KuLabel->Caption = IntToStr(params.Ku) + L" ��";
    m_AttLabel->Caption = IntToStr(params.Att) + L" ��";
    m_StGateLabel->Caption = IntToStr(params.StGate) + L" ���";
    m_EdGateLabel->Caption = IntToStr(params.EdGate) + L" ���";
    m_TVGLabel->Caption = IntToStr(params.TVG) + L" ���";
    m_PrismDelayLabel->Caption = FloatToStr(float(params.PrismDelay)/10.f) + L" ���";

    float timeInChannel = AutoconMain->Rep->GetTimeInHandScanChannelByAngle(params.EnterAngle, params.Method);
    m_ControlTimeLabel->Caption = FloatToStr(timeInChannel) + L" ���";



}

void TReportViewerForm::UpdateAScan()
{
    AScanListBox->Clear();
    if(AutoconMain->Rep->ScreenShotHandScanList.empty())
        return;

    for(unsigned int i = 0; i < AutoconMain->Rep->ScreenShotHandScanList.size(); i++)
    {
        AScanListBox->AddItem(StringFormatU(L"����������� %d",i+1),AutoconMain->Rep->ScreenShotHandScanList[i]);
    }

    AScanListBox->Selected[0] = true;
    AScanImage->Picture->Bitmap->Assign((TJPEGImage*)AScanListBox->Items->Objects[0]);
}

void TReportViewerForm::UpdatePhonePhoto(int index)
{
    switch(index)
    {
    case 0:
        if(AutoconMain->Rep->PhoneScreenShot)
            PhonePhotoImage->Picture->Bitmap->Assign(AutoconMain->Rep->PhoneScreenShot);
        else PhonePhotoImage->Picture->Assign(NoPhotoImage->Picture);
        break;
    case 1:
        if(AutoconMain->Rep->PhoneScreenShot2)
            PhonePhotoImage->Picture->Bitmap->Assign(AutoconMain->Rep->PhoneScreenShot2);
        else PhonePhotoImage->Picture->Assign(NoPhotoImage->Picture);
        break;
    case 2:
        if(AutoconMain->Rep->PhoneScreenShot3)
            PhonePhotoImage->Picture->Bitmap->Assign(AutoconMain->Rep->PhoneScreenShot3);
        else PhonePhotoImage->Picture->Assign(NoPhotoImage->Picture);
        break;
    }
}

void TReportViewerForm::UpdateCameraPhoto()
{
    if(AutoconMain->Rep->Photo)
        PhonePhotoImage->Picture->Bitmap->Assign(AutoconMain->Rep->Photo);
}

void __fastcall TReportViewerForm::CloseBtnClick(TObject *Sender)
{
	if(AScanViewForm->Visible)
		AScanViewForm->Visible = false;

	painter.pScanDraw->EnableMeasureDataDraw(false); //FDV
	painter.pScanDraw->SetMeasureData(NULL); //FDV
	this->Visible = false;
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::PageControl1Resize(TObject *Sender)
{
    UpdateReport();
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::AScanListBoxClick(TObject *Sender)
{
    if(AScanListBox->ItemIndex == -1)
        return;

    AScanImage->Picture->Bitmap->Assign((TJPEGImage*)AScanListBox->Items->Objects[AScanListBox->ItemIndex]);
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::BScanHandPBoxPaint(TObject *Sender)
{
    UpdateBScanHand();
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::AScanImageUpBtnClick(TObject *Sender)
{
    if(AScanListBox->Count == 0)
        return;
    if(AScanListBox->ItemIndex > 0)
        AScanListBox->ItemIndex--;
    else
        AScanListBox->ItemIndex = AScanListBox->Count - 1;

    AScanImage->Picture->Bitmap->Assign((TJPEGImage*)AScanListBox->Items->Objects[AScanListBox->ItemIndex]);
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::AScanImageDownBtnClick(TObject *Sender)
{
    if(AScanListBox->Count == 0)
        return;

    if(AScanListBox->ItemIndex < (AScanListBox->Count-1))
        AScanListBox->ItemIndex++;
    else
        AScanListBox->ItemIndex = 0;

    AScanImage->Picture->Bitmap->Assign((TJPEGImage*)AScanListBox->Items->Objects[AScanListBox->ItemIndex]);
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::BScanHandComboSelect(TObject *Sender)
{
    UpdateBScanHand();
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::BScanBtnClick(TObject *Sender)
{
    PageControl1->ActivePageIndex = 0;
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::AScanImagesBtnClick(TObject *Sender)
{
    PageControl1->ActivePageIndex = 1;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TReportViewerForm::LinearityBtnClick(TObject *Sender)
{
    if(ViewFlags & REPVIEW_FLAG_PHONE)
        UpdatePhonePhoto();
    PhotoLinearityTopBtn->Visible = true;
    PhotoLinearityTopBtn->Down = true;
    PhotoLinearityLeftBtn->Visible = true;
    PhotoLinearityRightBtn->Visible = true;
    PhotoCameraBtn->Visible = false;
    LargePhotoPageBtnPanel->Visible = true;
    PageControl1->ActivePageIndex = 2;
}
void __fastcall TReportViewerForm::ShowCameraPhotoBtnClick(TObject *Sender)
{
    if(ViewFlags & REPVIEW_FLAG_CAMERA)
        UpdateCameraPhoto();
    PhotoLinearityTopBtn->Visible = false;
    PhotoLinearityLeftBtn->Visible = false;
    PhotoLinearityRightBtn->Visible = false;
    PhotoCameraBtn->Visible = true;
    LargePhotoPageBtnPanel->Visible = false;
    PageControl1->ActivePageIndex = 2;
}

//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::BScanImageMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
    int id = - 1;
    int sel_kp = -1;
    if (PageControl1->ActivePageIndex == 0)
    {
        for (int idx = 1; idx < 9; idx++)
		{
			id = painter.click(idx, X, Y);
			if (id != -1)
			{
                sel_kp = idx;
				break;
			}
		}
    }

    if(measureState != MEAS_STATE_ADD_REGION)
    {
        if (id == - 1) ShowAScanOnBScan(X, Y, -1);
        else if(id > 0) ShowAScanOnBScan(X, Y, id);
        else if(id < 0)
        {
            painter.pScanDraw->Prepare();
            for (int idx = 1; idx < 9; idx++)
            {
                painter.scanLines[idx]->Refresh();
                painter.scanLines[idx]->PaintToBuffer();
            }
            UpdateBScan(false);
            DrawBScan();
        }
        else if(id != -1) DrawBScan();
    }
    else
    {
        if(measureState == MEAS_STATE_ADD_REGION)
        {
            if(editedRegionIdx == -1)
            {
                editedRegionIdx = BScanMeasureForm->getMeasure().regions.size();
                BScanRegion reg;
                BScanMeasureForm->getMeasure().regions.push_back(reg);
            }

            int delay = 0;
            if((id == -1) &&
                (painter.pScanDraw->GetSysCoord(X, Y, &AScanSysCoord, &Shift_CID, &Shift_CID_Cnt, &BSColor, &delay)) &&
                ((int)BScanMeasureForm->getMeasure().regions.size() > editedRegionIdx) &&
                (editedRegionIdx >= 0))
            {
                PolyReg& poly = BScanMeasureForm->getMeasure().regions[editedRegionIdx].poly;
                if(BScanMeasureForm->getMeasure().regions[editedRegionIdx].measureData.empty())
                {
                    BScanMeasureData meas;
                    for(int i = 0; i < Shift_CID_Cnt; i++)
                    {
                        meas.channels.insert(Shift_CID[i] + AutoconStartCID);
                    }
                    BScanMeasureForm->getMeasure().regions[editedRegionIdx].measureData.push_back(meas);
                }

                measureKP = sel_kp;
                measureXY.set(AScanSysCoord,delay);
                if(poly.isValid())
                    if(measureXY.getDistance(poly.points[0]) < 10)
                    {
                        TMeasureSelChannelForm* pDial = new TMeasureSelChannelForm(NULL);
                        pDial->clearChannels();

                        int chIdx = 0;
                        BScanRegion& region = BScanMeasureForm->getMeasure().regions[editedRegionIdx];
                        if(!region.measureData.empty())
                        {
                            std::set<CID>::const_iterator it = region.measureData[0].channels.begin();
                            while(it != region.measureData[0].channels.end())
                            {
                                int shiftChIdx = -1;
                                for(int k = 0; k < Shift_CID_Cnt; k++)
                                {
                                    if(Shift_CID[k] == *it - AutoconStartCID)
                                    {
                                        shiftChIdx = k;
                                        break;
                                    }
                                }
                                sChannelDescription desc;
                                if(AutoconMain->Table->ItemByCID(*it, &desc))
                                {
                                    if((shiftChIdx != -1) && (chIdx < 6))
                                        pDial->setChannel(chIdx++, *it, desc.Title, BSColor[shiftChIdx]);
                                }
                                it++;
                            }
                        }

                        pDial->ShowModal();

                        if(!region.measureData.empty())
                        {
                            std::set<CID>::iterator it = region.measureData[0].channels.begin();
                            while(it != region.measureData[0].channels.end())
                            {
                                if(!pDial->channelState(*it))
                                    it = region.measureData[0].channels.erase(it);
                                else
                                    it++;
                            }
                        }

                        delete pDial;


                        setMeasureState(MEAS_STATE_MEASURE);
                        return;
                    }
                poly.points.push_back(measureXY);
                UpdateBScan(false);
                DrawBScan();
                //setMeasureState(MEAS_STATE_PROCESS_MEASURE);
            }
        }
    }
}

void TReportViewerForm::ShowAScanOnBScan(int X, int Y, int showInfoId)
{
    if (X > BScanImage->Width / 2) AScanViewForm->Left = 0; else AScanViewForm->Left = BScanImage->Width - AScanViewForm->Width;
    if (Y > BScanImage->Height / 2) AScanViewForm->Top = 0; else AScanViewForm->Top = BScanImage->Height - AScanViewForm->Height;
    AScanViewForm->OnLeftBtnClicked = OnAScanFrameLeftBtnClick;
    AScanViewForm->OnRightBtnClicked = OnAScanFrameRightBtnClick;
    AScanViewForm->OnCloseBtnClicked = OnAScanFrameCloseBtnClick;
    AScanViewForm->Visible = True;


    int MouseX = X;
    int MouseY = Y;

    if(showInfoId != -1)
    {
        //UnicodeString chName, int Ku, int Att, int StGate, int EdGate, int TVG, int PrismDelay

        sChannelDescription chDesc;
        if(AutoconMain->Table->ItemByCID(showInfoId, &chDesc))
        {
            UnicodeString chName = chDesc.Title;
            showInfoId -= 0x3F;
            AScanViewForm->setInfoMode(true);

            sScanChannelParams params_gate1, params_gate2;
            int gateCnt = AutoconMain->Table->GetGateCount(showInfoId + AutoconStartCID);
            params_gate1 = AutoconMain->Rep->GetChannelParams(showInfoId + AutoconStartCID, 1);
            if(gateCnt == 2)
                params_gate2 = AutoconMain->Rep->GetChannelParams(showInfoId + AutoconStartCID, 2);
            AScanViewForm->setInfo(chName, &params_gate1, (gateCnt == 2) ? &params_gate2 : NULL);
        }
    }
    else
    {
        AScanViewForm->setInfoMode(false);
        if (painter.pScanDraw->GetSysCoord(MouseX, MouseY, &AScanSysCoord, &Shift_CID, &Shift_CID_Cnt, &BSColor))
        {
            painter.pScanDraw->SetCursorXY(AScanViewForm->Visible,MouseX,MouseY,clRed);
            ReDrawAScanOnBScan();
        }
        else DrawBScan();
    }
}

void TReportViewerForm::ReDrawAScanOnBScan()
{
    const unsigned char TableDB[256]=
    {
     0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x11,0x11,0x11,0x22,0x22,0x22,
     0x33,0x33,0x33,0x33,0x44,0x44,0x44,0x44,0x44,0x44,0x55,0x55,0x55,0x55,0x55,0x55,
     0x66,0x66,0x66,0x66,0x66,0x66,0x66,0x66,0x66,0x77,0x77,0x77,0x77,0x77,0x77,0x77,
     0x77,0x77,0x77,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,
     0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,
     0x99,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,
     0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,
     0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,
     0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,
     0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,
     0xCC,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,
     0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,
     0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,
     0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,
     0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,
     0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xFF
    };

    const int AmplId_to_DB[16] = { -12, -10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10, 12, 14, 16, 18 };

    bool res;

    // ����� �-��������� ��� ���������� ���������� �����

    AScanViewForm->AScanSeries1->Visible = false;
    AScanViewForm->AScanSeries2->Visible = false;
    AScanViewForm->AScanSeries3->Visible = false;
    AScanViewForm->AScanSeries4->Visible = false;
    AScanViewForm->AScanSeries5->Visible = false;
    AScanViewForm->AScanSeries6->Visible = false;


    AScanViewForm->Panel5->Caption = " ����������: " + IntToStr(AScanSysCoord - ScanLen/2) + " ��"; //  + " Cnt:" + IntToStr(Shift_CID_Cnt);

    sChannelDescription tmp;
    unsigned char buff[768];
    int Index;
    unsigned int aidx2;
    PsScanSignalsOneCrdOneChannel sgl;
    TLineSeries *LineSer;
    int LinkArr[24] = {0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7};

    AScanViewForm->removeGateButtons();
    for (int chidx = 0; chidx < Shift_CID_Cnt; chidx++)
    {
        //set gates
        sChannelDescription chDesc;
        CID absId = Shift_CID[chidx] + AutoconStartCID;
//        ShowMessage(IntToStr(absId));
//        assert(AutoconMain->Table->ItemByCID(absId,&chDesc));
        if (AutoconMain->Table->ItemByCID(absId,&chDesc))


        {

            sScanChannelParams params_gate1, params_gate2;
            int gateCnt = AutoconMain->Table->GetGateCount(absId);
            params_gate1 = AutoconMain->Rep->GetChannelParams(absId, 1);
            if(gateCnt == 2)
                params_gate2 = AutoconMain->Rep->GetChannelParams(absId, 2);
            int stGate[2];
            int edGate[2];
            stGate[0] = params_gate1.StGate;
            edGate[0] = params_gate1.EdGate;
            if(gateCnt == 2)
            {
                stGate[1] = params_gate2.StGate;
                edGate[1] = params_gate2.EdGate;
            };

    //        UnicodeString chName = chDesc.Title;
            AScanViewForm->setGateBtn(chidx, chDesc.Title, BSColor[chidx], gateCnt, stGate[0], edGate[0], stGate[1], edGate[1]);

            switch (chidx)
            {
                case 0: { LineSer = AScanViewForm->AScanSeries1; break; }
                case 1: { LineSer = AScanViewForm->AScanSeries2; break; }
                case 2: { LineSer = AScanViewForm->AScanSeries3; break; }
                case 3: { LineSer = AScanViewForm->AScanSeries4; break; }
                case 4: { LineSer = AScanViewForm->AScanSeries5; break; }
                case 5: { LineSer = AScanViewForm->AScanSeries6; break; }
            }
            LineSer->Clear();
            LineSer->Color = BSColor[chidx];
            sgl = AutoconMain->Rep->GetFirstScanSignals(AScanSysCoord, Shift_CID[chidx], &res);
            if (res)
            {
                memset(&(buff[0]), 0, 768);
                for (int idx = 0; idx < sgl->Count; idx++)
                {
                    for (int aidx = 0; aidx < 24; aidx++)
                    {
                        Index = (sgl->Signals[idx].Delay * 3) / chDesc.cdBScanDelayMultiply + (aidx - 11);
                        if (AScanViewForm->DelayCountCheckBox->Checked) aidx2 = LinkArr[aidx]; else aidx2 = aidx;
                        if ((Index >= 0) && (Index < 768)) buff[Index] = sgl->Signals[idx].Ampl[aidx2];
                    }
                }

                AutoconMain->Table->ItemByCID(Shift_CID[chidx] + AutoconStartCID, &tmp);
                AScanViewForm->AscanChart->BottomAxis->SetMinMax(0, tmp.cdScanDuration);

                LineSer->Visible = true;
                for (int idx = 0; idx < 768; idx++)
                    if (buff[idx] != 0)
                    {
                        if (AScanViewForm->ValueCountCheckBox->Checked)
                            LineSer->AddXY((float)idx / (float)3, AmplId_to_DB[TableDB[buff[idx]] >> 4]); else
                            LineSer->AddXY((float)idx / (float)3, (float)20 * log10((float)buff[idx] / (float)32));
                    } else LineSer->AddXY((float)idx / (float)3, -20);
            }
        }
    }

    AScanViewForm->updateButtons();

    painter.pScanDraw->SetCursorSC(AScanViewForm->Visible,AScanSysCoord,-1,-1,clRed);

    DrawBScan();
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::OnAScanFrameLeftBtnClick(TObject *Sender)
{
    AScanSysCoord--;
    ReDrawAScanOnBScan();
    //UpdateBScan();
}
void __fastcall TReportViewerForm::OnAScanFrameRightBtnClick(TObject *Sender)
{
    AScanSysCoord++;
    ReDrawAScanOnBScan();
    //UpdateBScan();
}
void __fastcall TReportViewerForm::OnAScanFrameCloseBtnClick(TObject *Sender)
{
    ReDrawAScanOnBScan();
}
void __fastcall TReportViewerForm::BScanHandBtnClick(TObject *Sender)
{
    PageControl1->ActivePageIndex = 3;
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::ThTrackBarChange(TObject *Sender)
{
    UnicodeString TextList[17] = {"����", "-12", "-10", "-8", "-6", "-4", "-2", "0", "2", "4", "6", "8", "10", "12", "14", "16", "18"};

    ThLabel->Caption = "����� �����������: " + TextList[ThTrackBar->Position];
    if (ThTrackBar->Position != 0) ThLabel->Caption = ThLabel->Caption + " ��";

    UpdateBScan();
    DrawBScan();
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    setMeasureState(MEAS_STATE_NONE);
    AScanListBox->Clear();
}
//---------------------------------------------------------------------------

void TReportViewerForm::SetArchiveForm(TArchiveForm* pArch,UnicodeString protocolPath)
{
    pArchive = pArch;
    this->protocolPath = protocolPath;
}

void __fastcall TReportViewerForm::PrintBtnClick(TObject *Sender)
{
    if(!pArchive)
        return;

    pArchive->SaveProtocolAsReport(protocolPath,ThTrackBar->Position);
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::PhotoLinearityTopBtnClick(TObject *Sender)
{
    UpdatePhonePhoto(0);
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::PhotoLinearityRightBtnClick(TObject *Sender)
{
    UpdatePhonePhoto(2);
}

void __fastcall TReportViewerForm::PhotoLinearityLeftBtnClick(TObject *Sender)
{
    UpdatePhonePhoto(1);
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::PhotoCameraBtnClick(TObject *Sender)
{
    UpdateCameraPhoto();
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::ThTrackBarLeftBtnClick(TObject *Sender)
{
    ThTrackBar->Position--;
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::ThTrackBarRightBtnClick(TObject *Sender)
{
    ThTrackBar->Position++;
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::PageUpBtnClick(TObject *Sender)
{
    if(CalibTable->TopRow > 1)
    {
        CalibTable->TopRow = std::max(1, (int)(CalibTable->TopRow - CalibTable->VisibleRowCount));
    }
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::PageDownBtnClick(TObject *Sender)
{
    if(CalibTable->TopRow < ( CalibTable->RowCount - CalibTable->VisibleRowCount))
    {
        CalibTable->TopRow = std::min((int)( CalibTable->RowCount - CalibTable->VisibleRowCount),
                                        (int)(CalibTable->TopRow + CalibTable->VisibleRowCount));
    }
}
//---------------------------------------------------------------------------



void __fastcall TReportViewerForm::ChannelsTableBtnClick(TObject *Sender)
{
    PageControl1->ActivePageIndex = 5;
}
//---------------------------------------------------------------------------

void TReportViewerForm::OnBScanMeasureFormMessage(eBScanMeasureMsgType Type, void* pValue)
{
    switch(Type)
    {
		case BSMM_UPDATE:
			painter.pScanDraw->SetMeasureData(&BScanMeasureForm->getMeasure());
			painter.pScanDraw->EnableMeasureDataDraw(true);
            UpdateBScan();
            DrawBScan();
            break;
        case BSMM_BTN_CLOSE:
            setMeasureState(MEAS_STATE_NONE);
            break;
        case BSMM_ADD_REGION:
            setMeasureState(MEAS_STATE_ADD_REGION);
            break;
    }
}

void TReportViewerForm::setMeasureState( eMeasureState state)
{
    switch(measureState)
    {
        case MEAS_STATE_NONE:
            if(state == MEAS_STATE_MEASURE)
            {
                painter.pScanDraw->SetMeasureData(NULL);
                BScanMeasureForm->initialize(AutoconMain->Rep, measureXY, std::vector<CID>(), OnBScanMeasureFormMessage);
                BScanMeasureForm->Top = painter.getRailRect().top;
                BScanMeasureForm->Left = painter.getRailRect().CenterPoint().x - BScanMeasureForm->Width / 2;
                BScanMeasureForm->Visible = true;
                measureState = state;
            }
            break;
        case MEAS_STATE_MEASURE:
            if(state == MEAS_STATE_ADD_REGION)
            {
                BScanMeasureForm->Visible = false;
                editedRegionIdx = -1;
                measureState = state;
            }
            if(state == MEAS_STATE_NONE)
            {
                BScanMeasureForm->Visible = false;
                measureState = state;
            }
            break;
        case MEAS_STATE_ADD_REGION:
            if(state == MEAS_STATE_MEASURE)
            {
                BScanMeasureForm->Visible = true;
                measureState = state;
            }
            if(state == MEAS_STATE_NONE)
            {
                BScanMeasureForm->Visible = false;
                measureState = state;
            }
            break;


//        case MEAS_STATE_NONE:
//            if(state == MEAS_STATE_WAIT_SELECTLINE)
//            {
//                painter.pScanDraw->SetMeasureData(NULL);
//                TScreenMessageForm3::InfoMessageA("����������", "�������� ����� b-��������� ��� ���������.", 2000);
//                measureState = state;
//            }
//            break;
//        case MEAS_STATE_WAIT_SELECTLINE:
//            if(state == MEAS_STATE_PROCESS_MEASURE)
//            {
//                std::vector<CID> measChannels;
//                for(int i = 0; i < Shift_CID_Cnt; i++)
//                {
//                    measChannels.push_back(Shift_CID[i] + AutoconStartCID);
//                }
//                BScanMeasureForm->initialize(AutoconMain->Rep, measureXY,measChannels, OnBScanMeasureFormMessage);
//
//
//                BScanMeasureForm->Top = painter.getRailRect().top;
//                BScanMeasureForm->Left = painter.getRailRect().CenterPoint().x - BScanMeasureForm->Width / 2;
//                BScanMeasureForm->Visible = true;
//                measureState = state;
//            }
//            if(state == MEAS_STATE_NONE)
//            {
//                measureState = state;
//            }
//            break;
//        case MEAS_STATE_PROCESS_MEASURE:
//            if(state == MEAS_STATE_NONE)
//            {
//                BScanMeasureForm->Visible = false;
//                measureState = state;
//            }
//            break;
    }
}

void __fastcall TReportViewerForm::MeasureBtnClick(TObject *Sender)
{
    setMeasureState(MEAS_STATE_NONE);
    setMeasureState(MEAS_STATE_MEASURE);
}
//---------------------------------------------------------------------------


void __fastcall TReportViewerForm::BScanPageResize(TObject *Sender)
{
	MeasureBtn->Top = BScanPage->Top + 10;
	MeasureBtn->Left = BScanPage->Left + BScanPage->Width - (MeasureBtn->Width + 10);

	DebugButton->Left = BScanPage->Left + BScanPage->Width - (MeasureBtn->Width + 10);
	FilterButton->Left = BScanPage->Left + BScanPage->Width - (MeasureBtn->Width + 10) + DebugButton->Width + 25;
    Button1->Left = BScanPage->Left + BScanPage->Width - (MeasureBtn->Width + 10);
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::BScanPageHide(TObject *Sender)
{
    setMeasureState(MEAS_STATE_NONE);
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::FormHide(TObject *Sender)
{
    setMeasureState(MEAS_STATE_NONE);
}
//---------------------------------------------------------------------------


void __fastcall TReportViewerForm::DebugButtonClick(TObject *Sender)
{
	BScanAnalyzeDebugForm->Rep = AutoconMain->Rep;
	BScanAnalyzeDebugForm->Visible = true;
	BScanAnalyzeDebugForm->BringToFront();
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::FilterButtonClick(TObject *Sender)
{
    sFilterParams fp;

    if (FiltrParamForm->ShowModal() == mrOk)
    {
        fp.OKLength = FiltrParamForm->MinLenEdit->Value;
        fp.MaxSysCoordStep = FiltrParamForm->MaxSpace->Value;
        fp.DeltaDelay  = FiltrParamForm->DelayDelta->Value;
        fp.SameDelayCount = FiltrParamForm->SameCount->Value;
        Filter(AutoconMain->Rep, fp);
    }
}
//---------------------------------------------------------------------------

void __fastcall TReportViewerForm::Button1Click(TObject *Sender)
{
    AutoconMain->Rep->CalcAlarmSignals();
    UpdateBScan(false);
    DrawBScan();
}
//---------------------------------------------------------------------------

