//---------------------------------------------------------------------------

//#pragma hdrstop

#include "PDFCreator.h"
#include "Utils.h"
//---------------------------------------------------------------------------
//#pragma package(smart_init)

UINT calculateHash(const char *str)
{
	static UINT lenStr = 0;
	static UINT hash = 0;  // seed
	static UINT step = 0;  // if string is too long, don't hash all its chars
	static UINT iStr;

	//init
	lenStr = strlen(str);
	hash = lenStr;
	step = (lenStr>>5)+1;

	//compute hash
	for (iStr=lenStr; iStr>=step; iStr-=step)
		hash = hash ^ ((hash<<5)+(hash>>2)+(unsigned char)(str[iStr-1]));

	return hash;
}

//-----------------------PDF PAGE--------------------------------------------

bool PDFPage::LoadLayout(const char* path)
{
	ParseSVG psvg;
	psvg.SetFilename(path);

	if(psvg.start_parse())
		return false;

	psvg.get_page_attribute(&width, &height);

	Rect_attribute rp;
	while (!psvg.get_rect_attribute(&rp) )
	{
		sPDFRect pdf_rect(rp);
		AddRectangle(pdf_rect);
	}

	Text_attribute tp;
	while (!psvg.get_text_attribute(&tp) )
	{
		sPDFText pdf_text(tp);
		AddText(pdf_text);
	}

	Line_attribute lp;
	while (!psvg.get_line_attribute(&lp) )
	{
		sPDFLine pdf_line(lp);
		AddLine(pdf_line);
	}
	psvg.end_parse();
	return true;
}

sPDFRect* PDFPage::GetRectangle(const char* name)
{
	UINT hash = calculateHash(name);
	std::map<UINT, sPDFRect>::iterator it = rectangle_map.find(hash);

	if(it != rectangle_map.end())
		return &(it->second);
	return NULL;
}
sPDFText* PDFPage::GetText(const char* name)
{
	UINT hash = calculateHash(name);
	std::map<UINT, sPDFText>::iterator  it = text_map.find(hash);

	if(it != text_map.end())
		return &(it->second);
	return NULL;
}
sPDFLine* PDFPage::GetLine(UINT index)
{
	if(index >= line_vector.size())
		return NULL;
	return &line_vector[index];
}

UINT PDFPage::AddRectangle(sPDFRect& rect)
{
	UINT hash = calculateHash(rect.id);
	rectangle_map[hash] = rect;
	return hash;
}
UINT PDFPage::AddText(sPDFText& rect)
{
	UINT hash = calculateHash(rect.id);
	text_map[hash] = rect;
	return hash;
}
UINT PDFPage::AddLine(sPDFLine& rect)
{
	line_vector.push_back(rect);
	return line_vector.size() - 1;
}

//-------------------END PDF PAGE---------------------------------------------

//--------------------PDF CREATOR---------------------------------------------

//Some shit for exception handling in HPDF
jmp_buf env;


#ifdef HPDF_DLL
void  __stdcall
#else
void
#endif
error_handler (HPDF_STATUS   error_no,
			   HPDF_STATUS   detail_no,
			   void         *user_data)
{
	char str[1024];
	sprintf(str,"ERROR: error_no=%04X, detail_no=%u\n\0",(HPDF_UINT)error_no,
				(HPDF_UINT)detail_no);
	OutputDebugStringA(str);


	//printf ("ERROR: error_no=%04X, detail_no=%u\n", (HPDF_UINT)error_no,
	//			(HPDF_UINT)detail_no);
	longjmp(env, 1);
}

PDFCreator::PDFCreator()
{
	ppi_scale = 1;
}

PDFCreator::~PDFCreator()
{
}

int PDFCreator::LoadPage( const char* path )
{
	int pageIndex = pages.size();
	pages.push_back(PDFPage());

	if(GetPage( pageIndex )->LoadLayout(path))
		return pageIndex;
	pages.pop_back();
	return -1;
}

void PDFCreator::ClearPages()
{
	pages.clear();
}

PDFPage* PDFCreator::GetPage( UINT index )
{
	if(index>=pages.size())
		return NULL;
    return &pages[index];
}
UINT PDFCreator::GetPageCount()
{
	return pages.size();
}

bool PDFCreator::SaveAsPDF(const char* path, const char* fontFolder)
{
	pdf = HPDF_New (error_handler, NULL);
	if (!pdf) {
		printf ("error: cannot create PdfDoc object\n");
		return false;
	}
	if (setjmp(env)) {
		HPDF_Free (pdf);
		return false;
	}

	HPDF_SetCompressionMode (pdf, HPDF_COMP_ALL);
	//HPDF_SetCurrentEncoder(pdf, "UTF-8");
	//HPDF_UseUTF8Encoding(pdf);

	// ������������� ������
	AnsiString fontName = fontFolder ? fontFolder : "";
	char font_name[64], bold_font_name[64];
	strcpy(font_name, HPDF_LoadTTFontFromFile(pdf,(fontName+"\\ARIAL.ttf").c_str(), 1));
	strcpy(bold_font_name, HPDF_LoadTTFontFromFile(pdf, (fontName+"\\Times_New_Roman_Bold.ttf").c_str(), 1));

	defaultFont = HPDF_GetFont(pdf, font_name, "CP1251");
	//bold_font = HPDF_GetFont(pdf, bold_font_name, "CP1251");


	for (unsigned int i = 0; i < pages.size(); i++)
	{
		HPDF_Page page = HPDF_AddPage (pdf);

		double scaleX = double(HPDF_Page_GetWidth (page)) / double(pages[i].GetWidth());
		double scaleY = double(HPDF_Page_GetWidth (page)) / double(pages[i].GetWidth());
		HPDF_Page_Concat (page, scaleX, 0, 0, scaleY, 0, 0);
		//HPDF_Page_SetWidth(page, pages[i].GetWidth());
		//HPDF_Page_SetHeight(page, pages[i].GetHeight());

		//pages[i].width = HPDF_Page_GetWidth (page);
		//pages[i].height = HPDF_Page_GetHeight (page);



		_drawRects(page,pages[i]);
		_drawText(page,pages[i]);
		_drawLines(page,pages[i]);
	}

	bool bRet =
		HPDF_SaveToFile (pdf, path) == HPDF_OK;

	HPDF_Free (pdf);
	return bRet;
}

#define COLORVALUE(x,ind) (((BYTE*)(&x))[ind])

TRect AdjustRectSize(TRect rectFrom, TRect rectTo)
{
	if(rectFrom.Width()==0)
		rectFrom.SetWidth(rectTo.Width());
	if(rectFrom.Height()==0)
		rectFrom.SetHeight(rectTo.Height());

	float aspect =  float(rectFrom.Width())/float(rectFrom.Height());

	if(rectFrom.Width() > rectTo.Width())
	{
		rectFrom.SetWidth(rectTo.Width());
		rectFrom.SetHeight( float(rectTo.Width()) / aspect );
	}

	if(rectFrom.Height() > rectTo.Height())
	{
		rectFrom.SetHeight(rectTo.Height());
		rectFrom.SetWidth( float(rectTo.Height()) * aspect );
	}

	return rectFrom;
}

TRect ImageRectToFrameRect(TRect imgRect, TRect frameRect)
{
	imgRect = AdjustRectSize( imgRect, frameRect);

	/*if((imgRect.Width() > frameRect.Width()) || (imgRect.Width() == 0))
		imgRect.SetWidth(frameRect.Width());
	if((imgRect.Height() > frameRect.Height()) || (imgRect.Height() == 0))
		imgRect.SetHeight(frameRect.Height());*/

	float bufHeight = imgRect.Height();
	float bufWidth = imgRect.Width();
	float bufAspect = bufWidth/bufHeight;

	TRect bufRect(frameRect);
	bufRect = TRect(frameRect.Width()/2 - bufWidth/2,
					frameRect.Height()/2 - bufHeight/2,
					frameRect.Width()/2 + bufWidth/2,
					frameRect.Height()/2 + bufHeight/2);

	float chartHeight = frameRect.Height();
	float chartWidth = frameRect.Width();
	float chartAspect = chartWidth/chartHeight;



	//calculating draw rect with correct aspect
	if (bufAspect > chartAspect)
	{
		int nNewHeight = (int)(chartWidth/bufWidth*bufHeight);
		int nCenteringFactor = (chartHeight - nNewHeight) / 2;
		bufRect = TRect( 0,
					  nCenteringFactor,
					  (int)chartWidth,
					  nNewHeight + nCenteringFactor);

	}
	else if (bufAspect < chartAspect)
	{
		int nNewWidth =  (int)(chartHeight/bufHeight*bufWidth);
		int nCenteringFactor = (chartWidth - nNewWidth) / 2;
		bufRect = TRect( nCenteringFactor,
					  0,
					  nNewWidth + nCenteringFactor,
					  (int)(chartHeight));
	}

	//bufRect.SetWidth(std::min(bufRect.Width(), frameRect.Width()));
	//bufRect.SetHeight(std::min(bufRect.Height(), frameRect.Height()));

	bufRect.top+=frameRect.top;
	bufRect.bottom+=frameRect.top;
	bufRect.left+=frameRect.left;
	bufRect.right+=frameRect.left;
	return bufRect;
}

void Convert_BCC_Bitmap_To_RAW_Bitmap(TBitmap* pSrcBmp,int srcBytesCount, BYTE* pDstBuf)
{
    int width = pSrcBmp->Width;
    int height = pSrcBmp->Height;

    BYTE* pDstPixel = pDstBuf;
    BYTE* pSrcPixel = 0;
    for(int y = 0; y < height; y++)
    {
        pSrcPixel = (BYTE*)pSrcBmp->ScanLine[y];
        for(int x = 0; x < width; x++)
        {
            pDstPixel[2] = pSrcPixel[0];
            pDstPixel[1] = pSrcPixel[1];
            pDstPixel[0] = pSrcPixel[2];

            pDstPixel+=3;
            pSrcPixel+=srcBytesCount;
        }
    }
}

void Convert_BCC_Bitmap_To_RAW_Bitmap2(TBitmap* pSrcBmp,BYTE* pDstBuf, bool bTranspose)
{
    int ind = 0;
    int src_ind;
    int dst_ind;

    int width = pSrcBmp->Width;
    int height = pSrcBmp->Height;

    int yc=0;
    int xm=0;

    for(int y = 0; y < height; y++)
    {
        BYTE* ScanLine = (BYTE*)pSrcBmp->ScanLine[y];

        yc = bTranspose ? (height - y - 1) : (y * width);
        xm = bTranspose ? (height) : (1);

        for(int x = 0; x < width; x++)
        {
                src_ind = x * 4;
                dst_ind = (x * xm + yc) * 3;

                pDstBuf[ dst_ind ] = ScanLine[src_ind + 2];
                pDstBuf[ dst_ind + 1 ] = ScanLine[src_ind + 1];
                pDstBuf[ dst_ind + 2 ] = ScanLine[src_ind];
        }
    }
}

bool PDFCreator::_drawRects(HPDF_Page page, PDFPage& layout)
{
	std::map<UINT, sPDFRect>::iterator it = layout.rectangle_map.begin();
	while(it !=  layout.rectangle_map.end())
	{
		sPDFRect& rect = it->second;

		HPDF_Page_GSave (page);

		//apply transform
        if(rect.bRotate)
        {
            HPDF_Page_Concat (page, rect.transform[0], rect.transform[1]*-1, rect.transform[2]*-1,
                                            rect.transform[3], rect.transform[4] - rect.height,
                                            layout.GetHeight() - rect.transform[5]);
        }
        else
        {
            HPDF_Page_Concat (page, rect.transform[0], rect.transform[1]*-1, rect.transform[2]*-1,
		    								rect.transform[3], rect.transform[4],
		    								layout.GetHeight() - rect.transform[5]  - rect.height);
        }

		HPDF_Page_SetLineWidth (page, 1);

		if(rect.image && (std::max(rect.image->Width , rect.image->Height) > 0))
		{
			LOGINFO("RectImage: %d, %d",rect.image->Width , rect.image->Height);
			int size = std::max(rect.image->Width , rect.image->Height);
			BYTE* pBuf = new BYTE[ rect.image->Width * rect.image->Height * 3 ];

            if(rect.image->PixelFormat == pf24bit)
                Convert_BCC_Bitmap_To_RAW_Bitmap(rect.image,3,pBuf);
            else if(rect.image->PixelFormat == pf32bit)
                Convert_BCC_Bitmap_To_RAW_Bitmap(rect.image,4,pBuf);
            else
            {
                rect.image->PixelFormat = pf24bit;
                Convert_BCC_Bitmap_To_RAW_Bitmap(rect.image,3,pBuf);
            }

			int newWidth = rect.image->Width;
			int newHeight = rect.image->Height;

			HPDF_Image hpdfImage =
				HPDF_LoadRawImageFromMem(pdf,pBuf,
						 newWidth,newHeight,HPDF_CS_DEVICE_RGB,8);

			TRect imgRect = ImageRectToFrameRect(TRect(0,0,newWidth,newHeight),
                				TRect((int)rect.x,
                                        (int)rect.y,
                                            (int)rect.x + (int)rect.width,
                                            (int)rect.y + (int)rect.height));
			HPDF_Page_DrawImage (page, hpdfImage,
                                                rect.width / 2 - imgRect.Width() / 2, rect.height / 2 - imgRect.Height() / 2,
                                                imgRect.Width(), imgRect.Height());

			delete [] pBuf;
		}

		if(rect.bDrawRect)
		{
			HPDF_Page_Rectangle (page, 0, 0, rect.width, rect.height);
			HPDF_Page_Stroke (page);

            if(!rect.text.IsEmpty())
            {
                HPDF_Page_BeginText (page);

                HPDF_Page_SetFontAndSize (page, rect.font ? rect.font : defaultFont, rect.font_size);
                //HPDF_Page_TextOut (page, 5, 5, rect.text.c_str());

                UINT len = rect.text.Length();
                HPDF_Page_TextRect(page, 0,rect.height,rect.width, 0, rect.text.c_str(), HPDF_TALIGN_CENTER, &len);

                HPDF_Page_EndText (page);
            }
		}

		HPDF_Page_GRestore (page);

		it++;
	}
	return true;
}
bool PDFCreator::_drawText(HPDF_Page page, PDFPage& layout)
{
	std::map<UINT, sPDFText>::iterator it = layout.text_map.begin();
	while(it !=  layout.text_map.end())
	{
		sPDFText& text = it->second;

		HPDF_Page_GSave (page);

		//apply transform
		HPDF_Page_Concat (page, text.transform[0], text.transform[1]*-1, text.transform[2]*-1,
										text.transform[3], text.transform[4],
										layout.GetHeight() - text.transform[5]);

		HPDF_Page_BeginText (page);

		HPDF_Page_SetFontAndSize (page, text.font ? text.font : defaultFont, text.font_size);

		HPDF_Page_TextOut (page, 0, 0, text.text.c_str());

		HPDF_Page_EndText (page);
		HPDF_Page_GRestore (page);

		it++;
	}

	return true;
}
bool PDFCreator::_drawLines(HPDF_Page page, PDFPage& layout)
{
	for (unsigned int i = 0; i < layout.line_vector.size(); i++)
	{
		sPDFLine& line = layout.line_vector[i];

		HPDF_Page_SetLineWidth (page, line.width);

		HPDF_Page_MoveTo (page, line.x1, layout.GetHeight() - line.y1);
		HPDF_Page_LineTo (page, line.x2, layout.GetHeight() - line.y2);

		HPDF_Page_Stroke (page);
	}
	return true;
}

//----------------END PDF CREATOR---------------------------------------------
