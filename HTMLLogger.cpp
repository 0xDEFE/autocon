//---------------------------------------------------------------------------

#pragma hdrstop

//#include <codecvt>
#include <locale>
#include <wchar.h>

#include "Utils.h"
#include "HTMLLogger.h"
//---------------------------------------------------------------------------
CHTMLLogger::CHTMLLogger(void) : Logger::CLoggerListener()
{
	bLogInit=false;

}


CHTMLLogger::~CHTMLLogger(void)
{
}

void CHTMLLogger::Init(const char* FilePath, const char* LogName)
{
	Release();


	if(FilePath==NULL) LogFile.open("./LOG.html",std::ios::out | std::ios::trunc);
	else LogFile.open(FilePath,std::ios::out | std::ios::trunc);

	bLogInit=true;
	if(LogName==NULL) StartBanner("Global Log");
	else StartBanner(LogName);
};

void CHTMLLogger::Release()
{
	if(bLogInit)
	{
		EndBanner();
		LogFile.close();
		bLogInit=false;
	};
};

void CHTMLLogger::onNewMessage(Logger::sLoggerMessage& Msg)
{
	std::string colorFmt=StringFormatTpl<std::string,char>("#%.2X%.2X%.2X",COLORVALUE(Msg.color,0),
															COLORVALUE(Msg.color,1),
															COLORVALUE(Msg.color,2));

    AnsiString str = Msg.data.c_str();
//	char* str = new char[Msg.data.size()];
//	for(unsigned int i=0;i<Msg.data.size();i++)
//	{
//		str[i] = std::wctob(Msg.data[i]);
//	}

	Write(colorFmt.c_str(),str.c_str());
	//delete [] str;
}

void  CHTMLLogger::Write(const char* color, const char* string)
{
	if(!bLogInit) return;
	if (!string) return;

    WriteTime();
	LogFile<<"<Font Size = 2 Color = "<<color
			<<" Face = Verdana>"<<string<<"</font><BR>";
	LogFile.flush();
}

void CHTMLLogger::StartBanner(const char *logname)
{
	if(!bLogInit) return;
	LogFile<<"<html>";
	LogFile<<"<title>Log</title>";
	LogFile<<"<meta http-equiv=\"Content-Type\"content=\"text/html; charset=windows-1251\">";
	LogFile<<"<body bgcolor=\"#000000\" text=\"#aaaaaa\">";
	LogFile<<"<pre>";
	LogFile<<"<h1>Work Log - "<<logname<<"</h1><BR>";
	LogFile<<"<h3><NoBR>----------------------------- Begin ------------------------------</NoBR></h3>";
	LogFile.flush();
}

void CHTMLLogger::EndBanner()
{
	if(!bLogInit) return;
	LogFile<<"<h3><NoBR>---------------------------- Results -----------------------------</NoBR></h3>";
	LogFile<<"<B>"<<Logger::GetLogger().getInfoCount()<<" message(s), "<<Logger::GetLogger().getErrorCount()<<" error(s), "<<Logger::GetLogger().getWarningCount()<<" warning(s), "<<Logger::GetLogger().getUnknownCount()<<" unknown(s)";
	LogFile<<"</B></pre></body></html>";
}

void CHTMLLogger::WriteTime()
{
	if(!bLogInit) return;
	SYSTEMTIME time;
	GetLocalTime(&time);
	LogFile<<"<Font Size = 1 Color = #ABABAB Face = Verdana><B>";
	LogFile<<time.wHour<<L":"<<time.wMinute<<":"<<time.wSecond<<"."<<time.wMilliseconds<<" - ";
	LogFile<<"</B></font>";
	LogFile.flush();
};