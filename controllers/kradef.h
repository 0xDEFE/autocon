#ifndef _kradefh
#define _kradefh

#pragma pack(push,1)


#define a_key 5     // ����� ������ �� ������
#define a_dsensor 19 // ����� ���������� �������� (� �.�. ������)
#define a_asensor 4 // ����� �������� �����������
#define a_outputs  10 // ����� ���������� �������

#define cAlarmMsgTimeOutMin 5000 

typedef enum _messageId
{
    dsensorstateid = 1,   //   �� <- kra
    asensorstateid = 2,    // �� <- kra
    opinid =  3,  //    �� -> kra     // ��������� �������� ��������
    sensorqry = 4, //    �� -> kra     // ������ ��������� �������
    setsensitive =  5,  //    �� -> kra     // ���������� "����������������" ��� ����������� �������
    resetMotor =  6,  //    �� -> kra     
    errorMessageId = 7,    // �� <- kra
    deviceNumberQryId = 8,    // �� -> kra // ������ ��������� ������ ����������
    deviceNumberMessageId =  9,  // �� <- kra
    versionQryId = 10,    // �� -> kra // ������ ������ �� �����������
    versionMessageId  = 11,  // �� <- kra
    dataBlockMessageId =  12, // �� -> kra - ������ �� �������� ��������� dataBlock ������ 1
    errorreportId = 13,    // �� <- kra
    dataBlockMessageNewId =  14, // �� -> kra - ������ �� �������� ��������� dataBlock ��������� ������ 
    opinidforced =  15,  //    �� -> kra     // �������������� ��������� �������� �������� - �� ��� ����, ��. SetOutputPin()

    alarmMsgId  = 255  //    �� -> kra     
} tMessageId;
//
#define outputstateid  opinid   //   �� <- kra
//
//

typedef enum _outputpinerror
{
    e_clusped = 1,             //  ������� ������� - ��������� ��������� ������ ����������
    e_railOrClusped =2,   //  ����� � ������ ��� ������� ������� - �������� ��������� ������ ����������
    e_noRailAndTestRailNotUp = 3, //  ��� ������ � ������ � ����� �� ������ ������� ������� - �������� ������� ����������
    e_cluspedOrTestRailUp = 4 // ������� ������� ��� �������� ����� ������- ���������� ���������� ����������
} tOutputpinError;

//--------------------------------------------------------------
// dsensorstateid     //   �� <- kra
// +1 - ������������� �������
// +2 - ������� ��������� ������� - ����������/ �������  (on_state/off_state)
#define  szDsensorstate 3 // ����� ����� 

//--------------------------------------------------------------
// asensorstateid      // �� <- kra
// +1 - ������������� �������
// +2..+3 - �������� �������
#define  szAsensorstate 4 // ����� ����� 

//--------------------------------------------------------------
// opinid              //    �� -> kra     // ��������� �������� ��������
//  opinidforced   //    �� -> kra     // �������������� ��������� �������� ��������

// +1 - ������������� �������
// +2 - ��������� ��������� ������� - ����������/ �������  (on_state/off_state)
#define szOpin 3 // ����� ����� 



//--------------------------------------------------------------
// sensorqry        //    �� -> kra     // ������ ��������� �������
// +1 - 0\1 - ����������\���������� ������ 
// +2 - ������������� �������
#define szSensorqry 3 // ����� ����� 

//--------------------------------------------------------------
// setsensitive    //    �� -> kra     // ���������� "����������������" ��� ����������� �������
// +1- ������������� ����������� �������
// +2- �������� dt
#define szSetsensitive 3 // ����� ����� 

//--------------------------------------------------------------
// ����� ��������� ����������� ���������� �����������
// ���� � ������� ������� ������ ������ �� ����������, �� ������� ������������
// resetMotor     //    �� -> kra     
// +1..+2 - ������������ ������� ������ � ��
#define szResetMotor 3 // ����� ����� 
//
//
//--------------------------------------------------------------
// errorMessageId      // �� <- kra
// +1 - �������� ������������� ������ 
// +2 - �������������� ������������� ������ 
#define  szErrorMessage 3 // ����� ����� 

//--------------------------------------------------------------
// deviceNumberQryId    // �� -> kra // ������ ��������� ������ ����������
#define szDeviceNumberQry 1 // ����� ����� 

//--------------------------------------------------------------
// deviceNumberMessageId   // �� <- kra
// +1 -  �������� ����� ���������� (2 �����)
#define szDeviceNumberMsg 3 // ����� ����� 

//--------------------------------------------------------------
// versionQryId      // �� -> kra // ������ ������ �� �����������
#define szVersionQry 1 // ����� ����� 

//--------------------------------------------------------------
// versionMessageId     // �� <- kra
// +1 -  ������ �� ����������� (4 �����)
#define szVersionMsg 5 // ����� �����

//--------------------------------------------------------------
// dataBlockMessageId    // �� -> kra - ������ �� �������� ��������� dataBlock ������ 1
//   �� <- kra - ������ - ��������� dataBlock
#define szdataBlockQry 1     // ����� ����� �������
// 

//--------------------------------------------------------------
// outputstateid   //   �� <- kra
// +1 - ������������� ������
// +2 - ������� ��������� ������ - ����������/ �������  (on_state/off_state)
#define  szOutputstate 3 // ����� ����� 
//
//--------------------------------------------------------------
// errorreportId    // �� <- kra

//--------------------------------------------------------------
#define  alarmMsgId   255  //    �� -> kra     
// +1- ������� (�������������� ����� ������� ���������� ���������)
#define szAlarmMsg 2    // ����� ����� 

//--------------------------------------------------------------
// dataBlockMessageNewId    // �� -> kra - ������ �� �������� ��������� dataBlock ��������� ������ 
//   �� <- kra - ������ - ��������� dataBlock � ��������������� dataBlockMessageId
#define szdataBlockQry 1     // ����� ����� �������
// 



#define on_state 0x55
#define off_state 0xAA


#define rldavl_id       6
//
#define dtindc1_id    7
//
#define dt_davl_id    8
//
#define dtindc2_id   9
//
#define dtkv1_id       10

//
#define dtkv2_id       11
//
#define dtkvg1_id     12
#define dtkvg2_id     13
#define dtkvg3_id     14
#define dtkvg4_id     15

#define dtsec1_id     16
#define dtsec2_id     17
#define dtsec3_id     18
#define dtsec4_id     19

// ����� - ����� ���� ������ ������ ���� ������ ��  ���� 
#define k_zahvat_id    1
#define k_lev_id          2
#define k_prav_id       3
#define k_pusk_id      4
#define k_rels_id        5

// ���������� ������� �����������
#define dt_sm1_id 1
#define dt_sm2_id 2
#define dt_sm3_id 3
#define dt_sm4_id 4
//


#define pnraspred1_id 1
//
#define pnraspred2_id 2 
//
#define pnraspred3_id 3
//
#define pointer_id        4

#define klv_id                5  // ������ ������ ����

#define kl_blokir_id       6   // ���������� ���������


// ������ ���������� ������
#define kl_sec1_id       7
#define kl_sec2_id       8
#define kl_sec3_id       9 
#define kl_sec4_id       10


//���� ������ ��� errorMessageId
#define ERROR_KEYS_PRESSED_TOO_RAPIDLY 1
//
#define DATAV1SIGN 1
typedef struct _DATAV1
{
    unsigned char  datapackversion;   // ��� DATAV1  �������� - DATAV1SIGN
	unsigned char  output[a_outputs]; // �������� - on_state / off_state
	unsigned char  dsensor[a_dsensor];// �������� - on_state / off_state
    unsigned short asensor[a_asensor];// ��������� ������������ �������� ���������� ��������
} DATAV1;
//
//
#define DATAV2SIGN 2
typedef struct _DATAV2
{
    unsigned char  datapackversion;   // ��� DATAV2  �������� - DATAV2SIGN
    unsigned char  output[a_outputs]; // �������� - on_state / off_state
    unsigned char  dsensor[a_dsensor];// �������� - on_state / off_state
    unsigned short asensor[a_asensor];// ��������� ������������ �������� ���������� ��������
    unsigned short tcpConnectionCounterValue;
} DATAV2;
//



#define ERRORREPORT1_V1 1
typedef struct _errorreport1datav1
{
  unsigned char  datapackversion; // ���   �������� - ERRORREPORT1_V1
  unsigned char  outputpinid;         //  ������������� ����������� ������ (1..
  unsigned char  outputpinvalue;   // �������� - on_state / off_state
  DWORD  cause;                          // ��� ������ �� CERROR
} tErrorReport1DataV1;
//
#pragma pack(pop)


#endif













