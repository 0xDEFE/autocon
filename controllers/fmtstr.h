﻿// КОДИРОВКА UTF-8 (без BOM)---------------------------------------------------------------------------
#ifndef _fmtstrh
#define _fmtstrh

#include "stdarg.h"


#ifdef __cplusplus
extern "C"
{
#endif


void fmtstrcnvcpy(char* buf,unsigned int bufsz, const char *fmt,va_list ap);

#ifdef __cplusplus
}
#endif
//
#endif

