﻿
// КОДИРОВКА UTF-8 (без BOM)---------------------------------------------------------------------------
//
// упраувление подсветки для камеры подошвы идет через выход с идентификатором pnraspred1_id
// см. lightState()

#include "cavtk2.h"

#include "kradef.h"
#include "math.h"
#include "stdarg.h"
#include "fmtstr.h"

#define TBD

#include "errstr.h"

char *get_errstr(CERROR i) {
	switch(i) {
	case e_initsensorqry:
		return (char*)stre_initsensorqry;

	case e_kv1off:
		return (char*)stre_kv1off;

	case e_kv2off:
		return (char*)stre_kv2off;

	case e_kvg1off:
		return (char*)stre_kvg1off;

	case e_kvg2off:
		return (char*)stre_kvg2off;

	case e_kvg3off:
		return (char*)stre_kvg3off;

	case e_kvg4off:
		return (char*)stre_kvg4off;

		//
	case e_kvg1on:
		return (char*)stre_kvg1on;

	case e_kvg2on:
		return (char*)stre_kvg2on;

	case e_kvg3on:
		return (char*)stre_kvg3on;

	case e_kvg4on:
		return (char*)stre_kvg4on;

		//
	case e_dip12:
		return (char*)stre_dip12;

	case e_dip34:
		return (char*)stre_dip34;

	case e_farfromstartpos:
		return (char*)stre_farfromstartpos;

		//
	case e_id1off:
		return (char*)stre_id1off;

	case e_id2off:
		return (char*)stre_id2off;

	case e_id1on:
		return (char*)stre_id1on;

	case e_id2on:
		return (char*)stre_id2on;

		//
	case e_ddoff:
		return (char*)stre_ddoff;

	case e_rdoff:
		return (char*)stre_rdoff;

	case e_caretReturn:
		return (char*)stre_caretReturn;
	case e_drvnotready:
		return (char*)stre_drvnotready;
	case e_cmdCancell:
		return (char*)stre_cmdCancell;
	case e_drvaborted:
		return (char*)stre_drvaborted;
	case e_keyPressedTooRapidly:
		return (char*)stre_keyPressedTooRapidly;
	case e_keyDisabled:
		return (char*)stre_keyDisabled;
	case e_tcprevive:
		return (char*)stre_tcprevive;
	case e_inituncluspedkv:
		 return (char*)stre_inituncluspedkv;
	case e_inituncluspeda:
		 return (char*)stre_inituncluspeda;
	case e_kv1on:
		return (char*)stre_kv1on;
	case e_kv2on:
		return (char*)stre_kv2on;
	case e_drvpwron:
		return (char*)stre_drvpwron;
	case e_drvpwroff:
		return (char*)stre_drvpwroff;
//
	 case e_wrongmode:  return (char*)stre_wrongmode;
	 case e_wrongmodeandstate: return (char*)stre_wrongmodeandstate;
	 case e_wrongstate: return (char*)stre_wrongstate; // остановлено из-за  e_wrongstate
	 case e_wrongact: return (char*)stre_wrongact;
	 case e_softwarever: return (char*)stre_softwarever;
	 case e_softwareres: return (char*)stre_softwareres;

	 case e_caretunknown: return  (char*)stre_caretunknown;
	 case e_railunknown:  return  (char*)stre_railunknown;
	 case e_usrstopped:  return  (char*)stre_usrstopped;

	default:	return NULL;
	}
}
// --------------------------------------------------------------------
char *getKRAErrorStr(tOutputpinError i) 
{
	switch(i) 
                    {
	case e_clusped: return (char*)stre_clusped;
	case e_railOrClusped: return (char*)stre_railOrClusped;
	case e_noRailAndTestRailNotUp: return (char*)stre_noRailAndTestRailNotUp;
	case e_cluspedOrTestRailUp: return (char*)stre_cluspedOrTestRailUp;
	default: return NULL;
                    }
}
// --------------------------------------------------------------------
const char onStateStr[] = {"<вкл>"};
const char offStateStr[] = {"<откл>"};
const char unknownStr[] = {"<?>"};


char *getOutputpinStateStr(UCHAR i) 
{
    switch(i)
    {
        case on_state: return (char*)onStateStr; 
        case off_state: return (char*)offStateStr; 
        default: return (char*)unknownStr;
	}
}
// --------------------------------------------------------------------
const char pnraspred1Str[] = {"<Подсветка>"};
const char pnraspred2Str[] = {"<Сведение кареток>"};
const char pnraspred3Str[] = {"<Подъем рельса>"};
const char pointerStr[] = {"<Указатель>"};
const char klvStr[] = {"<Клапан воды>"};
const char klblokirStr[] = {"<Блокировка таскателя>"};

char *getOutputpinNameStr(UCHAR i) 
{
    switch(i)
    {
        case pnraspred1_id: return (char*)pnraspred1Str; 
        case pnraspred2_id: return (char*)pnraspred2Str; 
		case pnraspred3_id: return (char*)pnraspred3Str;
        case pointer_id: return (char*) pointerStr;
        case klv_id: return (char*) klvStr;
        case kl_blokir_id: return (char*) klblokirStr;
        default: return (char*)unknownStr;
    }
}
// --------------------------------------------------------------------
//
DWORD WINAPI secondThread(LPVOID self)

{
	cavtk *pdev;
	pdev = (cavtk*)self;
	//
 //	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_ABOVE_NORMAL);
	while (pdev->getstate())
                     {
		pdev->Tick();
		if (pdev->isTransferToMotorDisabled() == false) {
			pdev->motorDev->tick();
		}
		SleepEx(1, false);
	}
	pdev->secondThread_Flg = false;

	return 0;
}

// --------------------------------------------------------------------
//
DWORD WINAPI AThread(LPVOID self) {
	cavtk *pdev;
	unsigned long tmr; // отсчет интервалов выдачи сообщений об ошибках по датчику и реле давления, по неготовности приводов
	BOOL atfirst = true;
	pdev = (cavtk*)self;
	//
	pdev->settimer(&tmr);
 //	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_ABOVE_NORMAL);
	timeBeginPeriod(2);
	while (pdev->getstate())
	{
		if (atfirst) { // чтобы исключить задержку при первом обнаружении ошибок по датчику и реле давления, по неготовности приводов
			if (pdev->getmode() > 0)
				atfirst = false;
			pdev->engine(NULL);
		}
		else
			pdev->engine(&tmr);
		SleepEx(1, false);
	}
	pdev->AThread_Flg = false;

	return 0;
}
//
// -------------------------------------------------------------------
cavtk::cavtk(int UMUIdx_, cDataTransfer* hrd_, const char *motorControlIP_,
	BOOL fMotorDisabled_)
{
	unsigned int ii;
	dev = NULL;

	DT = hrd_;
	UnitIdx = UMUIdx_;
	fMotorDisabled = fMotorDisabled_;
	fMD = true;
	fTransferToMotorDis = true;
	fTransferToKRAEn    = true;

	//
	Recive = false;
	bWork = false;
	//
	puskKeyPressedProc = NULL;
	errorLog_proc = NULL;
	debugLog_proc = NULL;
	motionEndEventProc = NULL;
	//
	STATUS s = {mstart,s0i};
#ifdef CRASH_LOG
//	pStatus = new cSTATUS(s,LOG,crash);
//	pStatus = new cSTATUS(s,NULL,NULL);
	pStatus = new cSTATUS(s);
#else
//	pStatus = new cSTATUS(s,LOG,NULL);
	pStatus = new cSTATUS(s);
#endif
					 setkeysmissed();

	for (ii = 0; ii < a_dsensor; ii++)
		dsensorstate[ii] = false;
	for (ii = 0; ii < a_asensor; ii++)
		asensorvalue[ii] = 0;

	keypressedId = longPressedKeyId = 0;
	longPressedTmr = 0; // станет не 0, после нажатия на кн. ВЛЕВО или ВПРАВО

	InitializeCriticalSection(&LogCr);


	kv1Kv2FailIgnore = false;
	initalRailPositionIgnore = false;
	motorDev = NULL;
	if (fMotorDisabled == false)
		strcpy(motorControlIP, motorControlIP_);
	else
		motorControlIP[0] = 0;
	settimer(&motorDevTimer);
	fMotionEnd = false;
	motorRestoreCounter = kraRestoreCounter = сMotorRestoreCounter;
	InitializeCriticalSection(&motorObjCr);
	fMotorPowered = false;
	fMotorRestarted = false;
//	defaultVelocity = 0;

	infoQryEvent  = CreateEvent (NULL,MANUAL_RESET_EVENT, FALSE, NULL); // ручн. сброс,NonSignaled
	InitializeCriticalSection(&infoQryCr);
	infoDataBuffer = NULL;
//
    fAlarmState = AlarmSend;
	alarmTimer.setTimer();
	alarmCounter = alarmCounterMem = 0;
//
	for (ii = 0; ii < a_outputs; ii++)
	    (actualOutputs[ii]) = (targetOutputs[ii]).state =  (targetOutputs[ii]).forced = FALSE;
	InitializeCriticalSection(&tickCr);
	InitializeCriticalSection(&setModeCr);
	hSetModeEvent = CreateEvent (NULL,MANUAL_RESET_EVENT, FALSE, NULL);
#ifdef OWN_LOG_FILE_NAME
	if (strlen(OWN_LOG_FILE_NAME) != 0)
	{
		ownLog = new LogFileO;
		if (ownLog)
		{
		   if (ownLog->init(OWN_LOG_FILE_NAME) < 0)
		   {
			   delete ownLog;
			   ownLog = NULL;
		   }
		}
	}
		else ownLog = NULL;
#else
    ownLog = NULL;
#endif
//
#ifdef CRASH_LOG
	InitializeCriticalSection(&crashLogCr);
#endif
	alarmSendPeriod  = (cAlarmMsgTimeOutMin/3);
	alarmReceiveTimeout = ((cAlarmMsgTimeOutMin*3)/2);

	kraSoftwareVer.isDataValid = false;
	fKRAtcpConnectionCounterValid = false;
#ifdef LOG_SHOW_INTERVAL
	 logTickMem.setTimer();
#endif
     messageClear();
}
// -------------------------------------------------------------------
cavtk::~cavtk(void) {
    LOG("cavtk::~cavtk enter");
	if (DT) {
		dev->EndWork();
		bWork = false;
		while

	((secondThread_Flg == true) ||
		(AThread_Flg == true))

		Sleep(10); // until  AThread has terminated
		//
		delete dev;
	}
                if (ownLog) delete ownLog;
	DeleteCriticalSection(&LogCr);
	DeleteCriticalSection(&motorObjCr);

	CloseHandle(infoQryEvent);
	DeleteCriticalSection(&infoQryCr);
	DeleteCriticalSection(&tickCr);
	DeleteCriticalSection(&setModeCr);
	CloseHandle(hSetModeEvent);
	delete pStatus;
#ifdef CRASH_LOG
	DeleteCriticalSection(&crashLogCr);
#endif


}
// -------------------------------------------------------------------
// возвращает 0 при удачном завершении
// ошибки:
// -1 -  не удалось установить TCP-соединение с bosh-контроллером
// -2 -  класс управления контроллером не создан либо не удалось инициализировать
// -3 - не иссякла необходимая задержка перед инициализацией bosh-контроллера
// после включения питания
// -4 - фукнция инициализации-1 bosh-контроллера завершилась с ошибкой
//
int cavtk::init(INPARAMDEF *inparamptr) {
int result;
STATUS s = {minit0,sinit0};
//
	Recive = true;
	puskKeyPressedProc = inparamptr->scanstateproc;
	errorLog_proc = inparamptr->errorLogProc;
	debugLog_proc = inparamptr->debugLogProc;

	dev = new KAVT(UnitIdx, DT);
	dev->SetProcedures(asensorevent, dsensorevent,infoEvent,alarmEvent,dataBlockEvent,outputsEvent,errorReportEvent);
	//dev->SetProcedures(NULL, dsensorevent);
	dev->setErrorMessageProcedure(avtErrorEvent);
	dev->StartWork();
	//
	bWork = true;
	DWORD dwThreadId = 1;
	pStatus->lock();
	pStatus->setMode(minit,true);
	pStatus->setState(s0i,true);
	pStatus->pushStatus(s,true);
	pStatus->unlock();

	CreateThread(NULL, 0, secondThread, this, 0, &dwThreadId);
	CreateThread(NULL, 0, AThread, this, 0, &dwThreadId);
	Sleep(10);
	dwThreadId = 1;

	secondThread_Flg = true;
	AThread_Flg = true;

	result = 0;
	if (fMotorDisabled == false) {
		result = 0;
		if (GetTickCount_() < motorInitializaitionDelayTime)
			result = -3;
		else {
			result = DT->AddConnection("", motorControlIP, 5002, 5002, true,
				true);
			if (result >= 0) {
				motorIdx = result;
				result = motorInit(motorIdx, DT);
				if (result < 0) {
					LOG(
						"cavtk::init:  фукнция инициализации bosh-контроллера завершилась с ошибкой %d", result);
					result = -2;
				}
				else {
					fTransferToMotorDis = false;
					result = motorInit1();
					if (result < 0) {
						LOG(
							"cavtk::init:  фукнция инициализации-1 bosh-контроллера завершилась с ошибкой %d", result);
						fTransferToMotorDis = true;
						result = -4;
					}
					else {
						if (debugLog_proc)
							motorDev->setLogProcedure(motorLog);
						fMD = false;
					}
				}
			}
			else {
				LOG(
					"cavtk::init: установление соединения с bosh-контроллером, ошибка %d", result);
				result = -1;
			}
		}
	}
	if (result < 0)
	{
		bWork = false;
		while

	((secondThread_Flg == true) ||
		(AThread_Flg == true)) Sleep(50);
	}
	return result;
}

// --------------------------------------------------------------
// проверяет включенное состояние датчиков квг2,4
// возвращает true, если они оба включены
// если нет, при установленном флаге ferrprn - сообщает об ошибке
//
BOOL cavtk::testkvg24on(BOOL ferrprn) {
	if ((dsensorstate[dtkvg2_id - 1] == true) &&
		(dsensorstate[dtkvg4_id - 1] == true)) {
		return true;
	}
	else {
		if (ferrprn) {
			if (dsensorstate[dtkvg2_id - 1] == false)
				set_error(e_kvg2off);
			if (dsensorstate[dtkvg4_id - 1] == false)
				set_error(e_kvg4off);
		}
		return false;
	}
};
// --------------------------------------------------------------
// проверяет разжатое состояние клещей по аналоговым датчикам
// возвращает true, если они разжаты
BOOL cavtk::testUncluspedA(void)
{
	return ((asensorvalue[0]  > uncluspMinValue) &&
				(asensorvalue[1] > uncluspMinValue ) &&
				(asensorvalue[2] > uncluspMinValue) &&
				(asensorvalue[3] > uncluspMinValue));
}
// --------------------------------------------------------------
// проверяет разжатое состояние клещей по дискретным датчикам
// возвращает true, если они разжаты
BOOL cavtk::testUncluspedD(void)
{
	 return ((dsensorstate[dtkv1_id - 1] == false) && (dsensorstate[dtkv2_id - 1] == false));
}
// --------------------------------------------------------------

void cavtk::engine(unsigned long *ptmr)
{
CMODE unitmode;
CSTATE unitstate;

	pStatus->lock();
	unitmode = pStatus->getMode(true);
	unitstate = pStatus->getState(true);

	switch(unitmode)
	{
	  case mstoppedwm:
	  case mstoppedwms:
	  case mstoppedwa:
	  case musrstopped:
		pStatus->unlock();
		return;
	}
//
	messageSeize(FALSE);  //
	if (getMessageType()==UserStopMessage)
	{
		messageClear();
		messageSeize(TRUE);  // удалить сообщение
		set_error(e_usrstopped);
		go_mode(musrstopped);
		crash("%s = %d",get_errstr(e_usrstopped),unitmode);
		pStatus->unlock();
		return;
	}
	  else messageClear();

//
	if ((unitstate != unsteady1) && (unitstate != unsteady2))
	{
		messageSeize(TRUE);
		switch(unitmode)
		{
		 case minit:
		 case mpoisk:
		 case mrail:
		 case mtune:
		 case mhand:
		   engineMainProcedure(ptmr);
		   testErrorReports();
//
		   unitstate = pStatus->getState(true);
//
		   pStatus->unlock();
		   if ((fMD == false) &&
			  (!(unitstate == motorRestore1) || (unitstate == motorRestore2) || (unitstate == motorRestore3) || (unitstate == motorRestore4) ))
				  motorDev->motionEngine();
		   engineAlarmProcedure(fReinitTCPConnections,false);
		   break;
//
		   case minit0:
				engineInit0();
				pStatus->unlock();
				break;


		   default:
			   set_error(e_wrongmode);
			   go_mode(mstoppedwm);
			   crash("%s = %d",get_errstr(e_wrongmode),unitmode);
			   pStatus->unlock();
		}
	}
		else
		{
			engineUnsteadyStates(unitmode,unitstate);
			pStatus->unlock();
		}
}
// --------------------------------------------------------------
void cavtk::wrongModeAndStateStop(char* s,CMODE unitmode,CSTATE unitstate,BOOL locked)
{
	if (!locked) pStatus->lock();
	set_error(e_wrongmodeandstate);
	go_mode(mstoppedwms);
	crash("%s %s, режим = %d, состояние = %d",s,get_errstr(e_wrongmodeandstate),unitmode,unitstate);
	if (!locked) pStatus->unlock();
}
// --------------------------------------------------------------
void cavtk::wrongStateStop(char* s,CSTATE unitstate,BOOL locked)
{
	if (!locked) pStatus->lock();
	set_error(e_wrongstate);
	go_mode(mstoppedws);
	crash("%s %s, состояние = %d",s,get_errstr(e_wrongstate),unitstate);
	if (!locked) pStatus->unlock();
}
// --------------------------------------------------------------
// д.б. уже выпонено pStatus->lock
void cavtk::mainEngineSrailuAct(CSTATE state)
{
#ifndef IGNORE_KVG
			if ((dsensorstate[dtkvg2_id - 1] == false) &&
				(dsensorstate[dtkvg4_id - 1] == false) &&
				(dsensorstate[dtkvg1_id - 1] == true) &&
				(dsensorstate[dtkvg3_id - 1] == true))
			{
			   if (state == srailu)	set_state(srail, true);
				   else set_state(sraile, true);
			}
			  else if (get_duration() >= railupmaxtime)
				   {
					if (dsensorstate[dtkvg1_id - 1] == false)
					{
						set_error(e_kvg1off);

					}
					if (dsensorstate[dtkvg3_id - 1] == false)
					{
						set_error(e_kvg3off);

					}
					if (dsensorstate[dtkvg2_id - 1] == true)
					{
						set_error(e_kvg2on);

					}
					if (dsensorstate[dtkvg4_id - 1] == true)
					 {
						set_error(e_kvg4on);
					 }
					if (state == srailu) set_state(sraild, true);
						else   set_state(sraild2, true);
					}
#else
	if (state == srailu) set_state(srail, true);
		else set_state(sraile, true);
#endif
}
// --------------------------------------------------------------
// д.б. уже выпонено pStatus->lock
void cavtk::mainEngineSraildAct(CSTATE state)
{
#ifndef IGNORE_KVG
			if ((dsensorstate[dtkvg2_id - 1] == true) &&
				(dsensorstate[dtkvg4_id - 1] == true) &&
				(dsensorstate[dtkvg1_id - 1] == false) &&
				(dsensorstate[dtkvg3_id - 1] == false) )
			{
				if (state == sraild) set_state(s0, true);
					else set_state(s0E, true);

			}
			else if (get_duration() >= raildownmaxtime)
				 {
					if (dsensorstate[dtkvg1_id - 1] == true)
						set_error(e_kvg1on);
					if (dsensorstate[dtkvg3_id - 1] == true)
						set_error(e_kvg3on);
					if (dsensorstate[dtkvg2_id - 1] == false)
						set_error(e_kvg2off);
					if (dsensorstate[dtkvg4_id - 1] == false)
						set_error(e_kvg4off);
					if (state == sraild) set_state(srailde, true);
						else set_state(srailde2, true);
				}

#else
	if (state == sraild) set_state(s0, true);
		else set_state(s0E, true);
#endif
}
// --------------------------------------------------------------
// д.б. уже выпонено pStatus->lock
void cavtk::mainEngineSseize0Act(void)    
{
			if ((dsensorstate[dtkv1_id - 1] == true) &&
				(dsensorstate[dtkv2_id - 1] == true))
                                                              {
				set_state(sseize, true);
			}
				else if (get_duration() >= seizemaxtime) {
				if (dsensorstate[dtkv1_id - 1] == false)
					set_error(e_kv1off);
				if (dsensorstate[dtkv2_id - 1] == false)
					set_error(e_kv2off);
				if (kv1Kv2FailIgnore == false)
					set_state(spwroff, true);
				else
					set_state(sseize, true);
			}

}
// --------------------------------------------------------------
// д.б. уже выпонено pStatus->lock
void cavtk::mainEngineSleave0Act(CMODE unitmode)    
{ // при разведении полагаем, что значение asensorvalue[] увеличивается
 if ((testUncluspedD())
#   ifndef IGNORE_ANALOG_SENSOR
    && (testUncluspedA())
#  endif
    ) {

#ifdef TUNE_WITHOUT_TESTRAIL
	if (unitmode == mrail)
#else
	if ((unitmode == mrail) || (unitmode == mtune))
#endif
		set_state(srail, true);
			else set_state(s0, true);
	}
		else if (get_duration() >= seizemaxtime)
		{
		// при разведении не достигли крайнего положения
		   set_error(e_farfromstartpos);
		   set_state(sleavee, true);
	 }
}
// --------------------------------------------------------------
// ожидание окончания поданной команды break на моторы
// д.б. уже выпонено pStatus->lock
void cavtk::mainEngineBrkstate2Act(CMODE unitmode)
{
STATUS s;
	if ((fMD == false) && (fMotionEnd == false)) return;
	if ((fMD == false) &&  (fMotionEnd) && (motionEndError.errorNumber != ERROR_NO))
	{
				set_error(e_cmdCancell);
				set_state(sseize, true);
	}
		else
		{
#ifdef SCAN_MOTION_DEBUG
					 {
						STATUS s;
						s = pStatus->getStoredStatus(true);
						 if   ((unitmode == mtune) || (s.state == sposttune)) set_state(sseize, true);
							 else set_state(scr, true);
					 }
#else
					  if (unitmode == mtune) set_state(sseize, true);
		  else set_state(scr, true);
#endif
		}
}
// --------------------------------------------------------------
// д.б. уже выпонено pStatus->lock
// только в режимах mrail, mpoisk,mhand (из-за сведения кареток, в котором нужно возвращать каретки в ИП)
void cavtk::mainEngineScrAct(CMODE unitmode)
{
STATUS s;
	if ((fMD == false) && (fMotionEnd == false)) return;
	if ((fMD == false) &&  (fMotionEnd) && (motionEndError.errorNumber != ERROR_NO)) set_error(e_caretReturn);
	s = pStatus->getStoredStatus(true);
	if ((unitmode == mrail) &&  (s.state == aj1) || (s.state == aj2))    set_state(sseize, true);
		else if (s.state == sseize) set_state(spwroff, true);
				 else
				 {
					  if (((unitmode == mpoisk) && ((s.state == s0) || (s.state == s0E)) ) ||
                                                                                      ((unitmode == mhand) &&  ((s.state == s0) || (s.state == s0E)) ) ||
						  ((unitmode == mrail) && ((s.state == srail)||(s.state == sraile)) ) ||
#ifdef TUNE_WITHOUT_TESTRAIL
						  ((unitmode == mtune) && ((s.state == s0)||(s.state == s0E))) ||
#else
						  ((unitmode == mtune) && ((s.state == srail)||(s.state == sraile)) ) ||
#endif
						  (s.state == sleavee))	 set_state(sseize0, true);
						     else if ((s.state == spostscan1) && ((unitmode == mpoisk)||(unitmode == mrail))) set_state(sseize,true);
								   else  wrongModeAndStateStop("mainEngineSrcAct: ",unitmode,s.state,true);
				 }
}
// --------------------------------------------------------------
// д.б. уже выпонено pStatus->lock
// только в режимах mrail
void cavtk::mainEngineAjAct(CSTATE unitstate)    
{
    if ((unitstate != aj1e1) && (unitstate != aj2e1))
	{
         wrongStateStop("mainEngineAjAct: параметр ",unitstate,true);
         return; 
    }
//
	if ((fMD == false) && (fMotionEnd == false)) return;
	if ((fMD == false) && (fMotionEnd) && (motionEndError.errorNumber != ERROR_NO)) motorErrorLog(motionEndError);
	if (unitstate == aj1e1)	set_state(aj2, true);
		else  set_state(aj3, true);
}
// --------------------------------------------------------------
// д.б. уже выпонено pStatus->lock
// только в режимах mrail,mpoisk.mtune,mhand (из-за сведения кареток, в котором нужно возвращать каретки в ИП)
void cavtk::mainEnginePwrAct(CMODE unitmode, CSTATE unitstate)
{
	if ((unitstate != spwron) && (unitstate != spwroff))
	{
		 wrongModeAndStateStop("mainEnginePwrAct: ",unitmode,unitstate,true);
		 return;
	}

	if ((fMD == false) && (fMotionEnd == false)) return;
	if ((fMD == false) && (fMotionEnd) && (motionEndError.errorNumber != ERROR_NO))
	{
		   motorErrorLog(motionEndError);
		  if (unitstate == spwron) set_error(e_drvpwron);
			  else  set_error(e_drvpwroff);
	}
	if (unitstate == spwron)
	{
		if (motionEndError.errorNumber == ERROR_NO)  set_state(scr,true);
			else
			{
				STATUS s = pStatus->getStoredStatus(true);
				set_state(s.state,true);
			}
	}
		else set_state(sleave0,true);
}
//--------------------------------------------------------------
// 1 - сообщение типа ErrorAnswer
// 0 - сообщение другого типа
// -1 - поле flag имеет недопустимое значение
int cavtk::errorAnswerTest(void)
{
	if (getMessageType() != ErrorAnswer) return 0;
	if ((imessage.flag == RejectCode) || (imessage.flag == AcceptCode)) return 1;
	crash("errorAnswerTest: field flag has wrong value - %d",imessage.flag);
	return -1;
}
//--------------------------------------------------------------
void cavtk::mainEngineUserAct(CMODE unitmode, CSTATE unitstate)
{
int res;
	if ((fAnswerPollingIntervalON == FALSE)
		|| (fAnswerPollingIntervalON == TRUE) && (answerPollingTmr.getDuration() > AnswerPollingInterval) )
	{
		fAnswerPollingIntervalON = TRUE;
		answerPollingTmr.setTimer();
		switch(unitstate)
		{
			case sleavee:
			   setError(e_caretunknown,TRUE);
			   break;
			case srailde:
			case srailde2:
			   setError(e_railunknown,TRUE);
			   break;
			default:
				crash("mainEngineUserAct: calling with wrong parameters - unitmode = %d, unitstate = %d",unitmode,unitstate);
		}
		return;
	}
	res = errorAnswerTest();
	if (res != 0)
	{
	  BOOL res = FALSE;
	  switch (unitstate)
	  {
		 case sleavee:
			res = (imessage.code == e_caretunknown);
			 break;
		 case srailde:
		 case srailde2:
			res = (imessage.code == e_railunknown);
			 break;
		 default:
				crash("mainEngineUserAct: calling with wrong parameters - unitmode = %d, unitstate = %d",unitmode,unitstate);
	  }
	   if ((res == 1) && res)
	   {
		   if (imessage.flag == AcceptCode)
		   {
			   switch (unitstate)
			   {
				   case sleavee:
					   set_state(spwron,true);
					   break;
				   case srailde:
					   set_state(srailu,true);
					   break;
				   case srailde2:
					   set_state(srailu2,true);
					   break;
			   }
		   }
				else
				{
#ifdef TUNE_WITHOUT_TESTRAIL
					if ((unitmode == mrail) && (unitstate == sleavee))
#else
					if (((unitmode == mrail) || (unitmode == mtune))&& (unitstate == sleavee))
#endif
					{
						set_state(sraile,true);
					}
					  else set_state(s0E,true);
				}
	   }
	   messageClear();
	}
}
// --------------------------------------------------------------
void cavtk::engineMainProcedureStop(CMODE mode, CSTATE state)
{
	wrongModeAndStateStop("engineMainProcedureStop:",mode,state,true);
}
// --------------------------------------------------------------
// д.б. уже выпонено pStatus->lock
void cavtk::engineMainProcedure(unsigned long *ptmr)
{
unsigned int ii;
int res;
CSTATE unitstate;
CMODE unitmode;

	unitstate = pStatus->getState(true);
					 unitmode = pStatus->getMode(true);

	if ((unitmode == minit) || (unitmode == mpoisk) || (unitmode == mrail) || (unitmode == mtune) || (unitmode == mhand))
	{ //
		ii = 0;
#ifndef IGNORE_DD
			if (unitmode != minit)
			{
			   if (dsensorstate[dt_davl_id - 1] == true) { // низкое давление - true
			   if ((!ptmr) || (get_duration(ptmr) > e_sendperiod)) {
				   set_error(e_ddoff);
					ii = 1;
			   }
			}
			}
			   else  ii = 1;
#endif
		if (fMotorDisabled == false) {
			switch(unitstate) {
			case motorRestore1:
				lockMotorObj();
				unLockMotorObj(); // чтобы быть уверенным, что motorDev->tick()
				// уже не выполняется
				delete motorDev;
				motorDev = NULL;
				DT->closeConnections(motorIdx);
				dev->setMotorReset(motorResetSignalTime);
				fMotorRestarted = true;
				settimer(&motorDevTimer); // будем считать продолжительность сигнала сброса
				set_state(motorRestore2, true);
				break;
			case motorRestore2:
				if (get_duration(&motorDevTimer) > motorResetSignalTime) {
					int res;
					res = DT->restoreConnection(motorIdx);
					if (res == 0) {
						res = motorInit(motorIdx, DT);
						if (res == 0) {
							set_state(motorRestore3, true);
							fTransferToMotorDis = false;
						}
						else {
							LOG(
								"engine: фукнция инициализации bosh-контроллера завершилась с ошибкой %d", res);
																																				 goToMotorReviving(true);
						}
					}
					else {
						LOG(
							"engine: восстановление соединения с bosh-контроллером, ошибка %d", res);
																																				 goToMotorReviving(true);
					}
				}
				break;
			case motorRestore3: {
					if (debugLog_proc)
						motorDev->setLogProcedure(motorLog);
					motorDev->parametersLog();
					settimer(&motorDevTimer);
					// settimer(&motorDevTimer1);

					res = motorInit1();
					if (res == 0) {
						if (isMotorPowered() == true) {
							switchMotorPwr(true);
						}
						set_state(motorRestore4, true);
					}
					else {
						LOG(
							"engine: фукнция инициализации-1 bosh-контроллера завершилась с ошибкой %d", res);
																															 goToMotorReviving(true);

					}

					break;
				}
				/*
				case restore5: {
				if (get_duration(&motorDevTimer) <= 20000) {
				if (get_duration(&motorDevTimer1) <= 1000) {
				settimer(&motorDevTimer1);
				motorDev->parametersLog();
				}
				}
				else
				set_state(motorRestore4, true);
				break;
				}
				 */
			default:
				if ((get_duration(&motorDevTimer) > e_sendperiod) ||
					(unitstate == motorRestore4)) {
					if (unitstate != motorRestore4)
						settimer(&motorDevTimer);
					res = motorDev->driverIsReady();
					if (res == 1) {
						if (unitstate == motorRestore4) {
							motorRestoreCounter = сMotorRestoreCounter;
							fMD = false;
							// motorDev->parametersLog();
							settimer(&motorDevTimer);
							motionEndEventProc = NULL;
							leaveMotorReviving();
						}
					}
					else if (res == 0) {
						set_error(e_drvnotready);
					}
					else if (res < 0) {
						LOG("engine: driver status getting error: %d", res);
																															 goToMotorReviving(true);
					}
				}
			}
		}
		if (ii)
			settimer(ptmr);
	}
//
//
	switch (unitstate)
	{
						 case srailu:
						 case srailu2:
						 {
#ifdef TUNE_WITHOUT_TESTRAIL
							if (unitmode == mrail)
#else
							if ((unitmode == mrail) || (unitmode == mtune))
#endif
							{
								mainEngineSrailuAct(unitstate);
							}
								else engineMainProcedureStop(unitmode,unitstate);
							 break;
						 }
//
						 case sraild:
						 case sraild2:
						 {
#ifdef TUNE_WITHOUT_TESTRAIL
							if (unitmode == mrail)
#else
							if ((unitmode == mrail) || (unitmode == mtune))
#endif
							{
								mainEngineSraildAct(unitstate);
							}
								else engineMainProcedureStop(unitmode,unitstate);
							 break;
						 }
//
						 case sseize0:
						 {
							if ((unitmode == mrail) || (unitmode == mpoisk) || (unitmode == mtune) ||  (unitmode == mhand))
							{
								mainEngineSseize0Act();
							}
								else engineMainProcedureStop(unitmode,unitstate);
							 break;
						 }
//
						 case sleave0:
						 {
							if ((unitmode == mrail) || (unitmode == mpoisk) || (unitmode == mtune) || (unitmode == mhand) )
							{
								mainEngineSleave0Act(unitmode);
							}
								else engineMainProcedureStop(unitmode,unitstate);
							 break;
						 }
//
						 case brkstate2:
						 {
							if ((unitmode == mrail) || (unitmode == mpoisk) || (unitmode == mtune))   
							{
								mainEngineBrkstate2Act(unitmode);
							}
								else engineMainProcedureStop(unitmode,unitstate);
							 break;
						 }
//
						 case scr:
						 {
							if ((unitmode == mrail) || (unitmode == mpoisk) || (unitmode == mtune) || (unitmode == mhand))
							{
								mainEngineScrAct(unitmode);
							}
								else engineMainProcedureStop(unitmode,unitstate);    // в mtune при возврате кареток состояние stuneWaitEndMotion
							 break;
						 }
//
						 case aj1e1:
						 case aj2e1:
						 {
							if (unitmode == mrail )
							{
								mainEngineAjAct(unitstate);
							}
								else engineMainProcedureStop(unitmode,unitstate);
							 break;
						 }
//
						 case spwron:
						 case spwroff:
						 {
							if ((unitmode == mrail) || (unitmode == mpoisk) || (unitmode == mtune) || (unitmode == mhand))
							{
								mainEnginePwrAct(unitmode,unitstate);
							}
								else engineMainProcedureStop(unitmode,unitstate);
							 break;
						 }
						 case spredscan:
							  if ((unitmode == mrail) || (unitmode == mpoisk) ) setstate(sscan,true);
								else engineMainProcedureStop(unitmode,unitstate);
							 break;
						 case spostscan1:
							if ((unitmode == mrail) || (unitmode == mpoisk) )
							{
#ifdef SCAN_MOTION_DEBUG
								set_state(sseize, true);
#else
								set_state(brkstate2, true);
#endif
							}
								else engineMainProcedureStop(unitmode,unitstate);
							 break;
//
						 case spostscan2:
							if ((unitmode == mrail) || (unitmode == mpoisk) )
							{
								set_state(spwroff, true);
							}
								else engineMainProcedureStop(unitmode,unitstate);
							 break;
//
						 case spredtune:
							 if (unitmode == mtune)
							 {
								 set_state(stune, true);
							 }
								else engineMainProcedureStop(unitmode,unitstate);
							 break;
//
						 case sposttune1:
							 if (unitmode == mtune)
							 {
								 set_state(brkstate2, true);
							 }
								else engineMainProcedureStop(unitmode,unitstate);
							 break;
//
						 case sposttune2:
							 if (unitmode == mtune)
							 {
								 set_state(sseize, true);
							 }
								else engineMainProcedureStop(unitmode,unitstate);
							 break;
//

						  case s0BLOFF:
						  case s0FBLOFF:
							  set_state(s0i, true);
							  break;

						  case s0BLON:
							  set_state(s0, true);
							  break;

						  case sleavee:
						  case srailde:
						  case srailde2:
							 mainEngineUserAct(unitmode,unitstate);
							 break;
	}
	 engineKeyProcedure();
}
// --------------------------------------------------------------
void cavtk::engineAlarmProcedure(BOOL fReinitConn, BOOL locked)
{
#ifndef DISABLE_ALARM_MESSAGE
	if (fAlarmState == AlarmSend)
	{
		if (alarmTimer.getDuration() > alarmSendPeriod)
		{
			alarmCounterMem++;
			dev->sendAlarmMessage(alarmCounterMem);
			fAlarmState = AlarmReceive;
			alarmTimer.setTimer();
		}
	}
		else
		{
			if (alarmTimer.getDuration() > alarmReceiveTimeout)
			{
			   fAlarmState = AlarmSend;
			   if (fReinitConn)
			   {
				 goToKRAReviving(locked);
			   }
				 else
				{
				 alarmTimer.setTimer();
				}
			}
				else if (alarmCounter == alarmCounterMem)
					 {
						 fAlarmState = AlarmSend;
						 alarmTimer.setTimer();
					 }
		}
#endif
}
// --------------------------------------------------------------

// д.б. уже выпонено pStatus->lock
void cavtk::longPressedKeyReleaseAction(void)
{
	   if ((fMD == false) && ((longPressedKeyId == k_lev_id) ||
	  (longPressedKeyId == k_prav_id)) &&(fMotorRestarted == false))
	   {
		   ERRORMSG e = {	ERROR_NO, 0, 0, 0};
		   motorDev->writeParameter1(STOP, &e);
		   motorErrorLog(e);
		   conditionalGoToMotorReviving(&e, true);
	  }
  longPressedKeyId = 0;
}
// --------------------------------------------------------------
void cavtk::shortPressedKeyReleaseAction(CSTATE stateWhenPressed)
{
ERRORMSG e = {ERROR_NO, 0, 0, 0};
	 if (keypressedIdMem == k_lev_id)
     {
         if (stateWhenPressed == aj2)
		 {
             if (fMD == false)
			 {
                 motorDev->writeParameter1(LEFTMINBOT, &e);
				 motorErrorLog(e);
             }
         }
             else {
	    if (fMD == false)
                         {
							 motorDev->writeParameter1(LEFTMINUP, &e);
                             motorErrorLog(e);
						  }
                     }
     }
         else if (stateWhenPressed == aj2)
                 {
                     if (fMD == false)
                     {
                         motorDev->writeParameter1(RIGHTMINBOT, &e);
	    motorErrorLog(e);
					 }
				 }
	else {
				if (fMD == false)
								 {
		motorDev->writeParameter1(RIGHTMINUP, &e);
		motorErrorLog(e);
								 }
			}
	conditionalGoToMotorReviving(&e, true);
}
// --------------------------------------------------------------
// д.б. уже выпонено pStatus->lock
void cavtk::engineKeyProcedure(void)
{
CSTATE unitstate = pStatus->getState(true);
CMODE unitmode = pStatus->getMode(true);
UCHAR num;
BOOL state;

    
   if ((qKeyEvent.empty() == true) && (keypressedId == 0)) return;
   if (qKeyEvent.empty() == false)
   {
        num = qKeyEvent.front();
		state = ((num & KEYPRESSEDEVENTMASK)==KEYPRESSEDEVENTMASK);
        num &= ~KEYPRESSEDEVENTMASK;
       if (keypressedId == 0) 
       {
		 if (state)
         {
			 if (keysStatus[num - 1] == KEN)
			 { // нажата единственная кнопка фиксируем ее - если нажатие разрешено
	keypressedId = num;
	LOG("Enabled key %s pressed", getKeyName(num));
             }
                 else if (keysStatus[num - 1] & KDIS)
                         { // нажатие не фиксируем
	        LOG("Key %s pressed, but disabled", getKeyName(num));
                             set_error(e_keyDisabled); 
                         }
          }
       }
           else if ((keypressedId == num) && (state == false)) 
                     {  // отпущена кнопка, нажатие которой зафиксировано
	    keypressedId = 0;
	    LOG("Tracked key %s released", getKeyName(num));
	}
       qKeyEvent.pop();
    }
//
	if (keypressedId == 0)
	{
		if (keypressedIdMem != 0)
		{
		   // отрабатываем отпускание кнопки
				if (longPressedKeyId != 0)  longPressedKeyReleaseAction();
				else
				 // отпускание после короткого нажатия
		   if ((keypressedIdMem == k_lev_id) || (keypressedIdMem == k_prav_id))  shortPressedKeyReleaseAction(unitstate);
			 keypressedIdMem = 0;
			 fMotorRestarted = false;

		}
	}
	else {
		if (keypressedIdMem == 0)
										 {
			keypressedIdMem = keypressedId;
			settimer(&longPressedTmr);
			switch(keypressedIdMem)
			{
			case k_zahvat_id:
				if (unitstate == sseize)	set_state(scr, true);
				else {
#ifndef IGNORE_DD
					if (dsensorstate[dt_davl_id - 1] == true)
					// низкое давление - true
						set_error(e_ddoff);
					else
#endif
						set_state(spwron, true);
				}
				break;
			case k_pusk_id:
				if (unitstate == sseize)
				{
					if (unitmode == mtune)
					{
						set_state(spredtune, true);
					}
						else
						{
							int res = 1;
#ifndef SCAN_MOTION_DEBUG
							if (fMD == false) res = motorDev->isUpperTrolleyInCenter();
#endif
							if (res == 1)
							{
								set_state(spredscan, true);
							}
							   else set_error(e_outofcenter); // в т.ч. если ошибка
						}
				}
				else if (unitstate == aj2)
					 {
						 set_state(aj2e1, true);
					 }
					 else  if (unitmode == mtune)
						   {
							 if (unitstate == stune) set_state(sposttune2, true);
								 else  set_state(sposttune1, true); // хотя здесь кн. запрещена
						   }
							  else set_state(spostscan1, true);
				break;
			case k_rels_id:
			{
				switch(unitstate)
				{
				   case s0:
				   {
					set_state(srailu, true);
					break;
				   }
//
				   case s0E:
				   {
				   STATUS s;
					   s = pStatus->getStoredStatus(true);
					   if (s.state != sraild2) set_state(srailu, true);
							else set_state(srailu2, true);

					   break;
				   }
//
				   case srailu:
				   case srail:
				   {
					  set_state(sraild, true);
					  break;
				   }
//
				   case srailu2:
				   case sraile:
				   {
					  set_state(sraild2, true);
					  break;
				   }
				}
				break;
			 }
			}
		}
		else
		{
			if (keypressedIdMem == keypressedId) {
				// отслеживание длинного нажатия кнопок
				if ((longPressedKeyId == 0) && (get_duration(&longPressedTmr)
						> a_keyLongPressedTime)) {
					ERRORMSG e = {
						ERROR_NO, 0, 0, 0
					};
					longPressedKeyId = keypressedIdMem;
					switch(longPressedKeyId) {
					case k_lev_id:
						if (fMD == false) {
							if ((unitstate == aj1) || (unitstate == sseize)) {
								motorDev->writeParameter1(LEFTMAXUP, &e);
							}
							else
								motorDev->writeParameter1(LEFTMAXBOT, &e);
						}
						break;
					case k_prav_id:
						if (fMD == false) {
							if ((unitstate == aj1) || (unitstate == sseize)) {
								motorDev->writeParameter1(RIGHTMAXUP, &e);
							}
							else
								motorDev->writeParameter1(RIGHTMAXBOT, &e);
						}
						break;
					}
					conditionalGoToMotorReviving(&e, true);
				}
			}
			else { // пропустили отпускание ранее нажатой кнопки
				LOG("Engine: key releasing missed");
			}
		}

	}
}

// --------------------------------------------------------------
#define qryattempts 3    // число попыток опроса состояния датчиков и лог.выходов
// выполняем в режиме minit0
// д.б. уже выпонено pStatus->lock
void cavtk::engineInit0(void)
{
CMODE mode;
CSTATE state;
int res;
//
		state = pStatus->getState(true);
		switch(state)
		{
			case kraRestore1:
				EnterCriticalSection(&tickCr);
				fTransferToKRAEn = false;
				LeaveCriticalSection(&tickCr);
				setkeysmissed();
				DT->closeConnections(UnitIdx);
//    разборки с переменными клавиатуры после разрыва связи по TCP
				if (keypressedIdMem != 0)
				{
					 if (longPressedKeyId != 0)
					 {
						 longPressedKeyReleaseAction();
					 }
					keypressedIdMem = 0;
					fMotorRestarted = false;
				}
				keypressedId = 0;
//
//
				Sleep(500);
				res = DT->restoreConnection(UnitIdx);
				if (res == 0)
				{
					fTransferToKRAEn = true;
					pStatus->setState(kraRestore2,true);
				}
					else if (--kraRestoreCounter == 0)
							{
								 set_error(e_tcprevive);
								 pStatus->setState(siniterror,true); // восстановление связи невозможно
							}
							   else Sleep(50);
				break;
//
			 case sinit0:
			 {
				 UCHAR buf[4] ;

				 if (getVersion(buf) == 4)
				 {
					 kraSoftwareVer.major = buf[2];
					 kraSoftwareVer.minor = buf[1];
					 kraSoftwareVer.build = buf[0];
					 kraSoftwareVer.isDataValid = true;
					 LOG("engineInit0: версия ПО контроллера - %d.%d.%d",kraSoftwareVer.major,kraSoftwareVer.minor,kraSoftwareVer.build);

					 if (1)
					 {
					  if (isKRAVersionValid(((KRASOFTWAREVERSION*)&KRAMinimalVer))) pStatus->setState(sinit01, true);
					   else
					   {
						   LOG("engineInit0: требуется версия ПО контроллера не ниже  %d.%d.%d",KRAMinimalVer.major,KRAMinimalVer.minor,KRAMinimalVer.build);
						   pStatus->setState(siniterror, true);
					   }
					 }
						 else pStatus->setState(sinit01, true);
				 }
					 else
					 {
						 set_error(e_softwarever);
						 pStatus->setState(siniterror,true);
					 }
				 break;
			 }
			 case sinit01:
			 case  kraRestore2:
			 {
	unsigned int ii, jj, kk;
	kk = qryattempts;
	while(--kk)
	{
		if (isKRAVersionValid(((KRASOFTWAREVERSION*)&minimalVerDemand1)))
		{
		 if (dev->sendQryDataBlockMessage(true,10000) == 0) break;
		}
			else
			  if (dev->sendQryDataBlockMessage(10000) == 0) break;
	}
	if (kk)
	{
	// состояние всех датчиков получено
						 if (state == sinit01)
						 {
							 pStatus->setState(sinit1, true);

						 }
                            else  
                            {
                                 if (isKRAVersionValid((KRASOFTWAREVERSION*)&minimalVerDemand1))
                                 {
                                       if ((fKRAtcpConnectionCounterValid == false) || (KRAtcpConnectionCounter == 1))
									   { // контроллер был выключен и только что включен
										  set_error(e_softwareres);
										  pStatus->setState(siniterror,true);
                                          break;
                                       } 
                                 } 
                                  kraRestoreCounter = сMotorRestoreCounter;
	              walkTowardFormerState();
							}
       	}
	    else
	    {
                             if (state == sinit01)
                             {
                                 set_error(e_initsensorqry);
                                 pStatus->setState(siniterror,true);
                             }
                                 else
	            {    //  идем на пересброс снова
	                LOG("engineInit0: reinit TCP again");
  	                pStatus->setState(kraRestore1,true);
                                 }
                }
				break;

            }
            case sinit1:
            {
                if (testUncluspedD())
                {
#ifndef IGNORE_ANALOG_SENSOR
                    if (testUncluspedA() == false)
					{
                        set_error(e_inituncluspeda);
                        pStatus->setState(siniterror,true);
                    }
                        else
#endif
						{ // проверить опущенное состояние рельса
#ifndef IGNORE_KVG
                            if (testkvg24on(true) == false) pStatus->setState(siniterror,true);
                                else 
#endif
								walkTowardFormerState(); // i.e. (minit:s0)
                        }
                }
                    else
                    {
                      set_error(e_inituncluspedkv);
					  pStatus->setState(siniterror,true);
                    }
                break;
            }
		   case siniterror:
// чтобы контроллер сам не разрывал связь из-за неполучения alarmMsg
			   engineAlarmProcedure(!fReinitTCPConnections,true);
			   break;
		}
}
// --------------------------------------------------------------
// отработка действий в unsteady1,unsteady2
// д.б. уже выпонено pStatus->lock
void cavtk::engineUnsteadyStates(CMODE mode,CSTATE state)
{
	switch(state)
	{
		case unsteady1:
		{
			 unsteadyWaitTimer.setTimer();
			 setOutputPinsToTarget();
			 if (state == unsteady1) pStatus->setState(unsteady2,true);
			 break;
        }
		 case unsteady2:
		 {
			unsigned int ii;
			for (ii=0; ii<a_outputs; ii++)
				if(actualOutputs[ii] != (targetOutputs[ii]).state) break;
					else (targetOutputs[ii]).forced = FALSE;
			if (ii < a_outputs)
			{
				 if (unsteadyWaitTimer.getDuration() > unsteadyWaitTimeout)
				 {
					//  время ожидания вышло
					  goToKRAReviving(true);
				 }
					 else
					 {
						 if (testErrorReports())
						 {
						 STATUS s;
							 pStatus->popStatus(true);
							 s = pStatus->getStoredStatus(true);
							 pStatus->setMode(s.mode,true);
							 pStatus->setState(s.state,true);
							 defineKeysStatus();
							 alarmTimer.setTimer();
							 crash("возвращено состояние %s:%s",getModeName(s.mode),getStateName(s.state));
							 SetEvent(hSetModeEvent);
						 }
					 }
			 }
				 else
				 {
				  STATUS s;
				   pStatus->popStatus(true);
				   defineKeysStatus();
					 switch (mode)
					 {
					  case minitreq:
							 pStatus->setMode(minit,true);
							 SetEvent(hSetModeEvent);
							 break;
					  case mrailreq:
							 pStatus->setMode(mrail,true);
							 SetEvent(hSetModeEvent);
							 break;
					  case mpoiskreq:
							 pStatus->setMode(mpoisk,true);
							 SetEvent(hSetModeEvent);
							 break;
					  case mtunereq:
							 pStatus->setMode(mtune,true);
							 SetEvent(hSetModeEvent);
							 break;
					  case mhandreq:
							 pStatus->setMode(mhand,true);
							 SetEvent(hSetModeEvent);
 						                 break;

					 }
				   alarmTimer.setTimer();
				   s.state = pStatus->getState(true);
				   s.mode = pStatus->getMode(true);
				   LOG("восстановлено состояние %s:%s",getModeName(s.mode),getStateName(s.state));
				 }
			 break;
	   }
	   default: crash("engineUnsteadyStates: error - bad input parameter %s",getStateName(state));
	}
}
// --------------------------------------------------------------
int cavtk::getmode(BOOL locked)
{
CSTATE unitstate;
CMODE unitmode;
int res;

	if (locked == false) pStatus->lock();
	unitstate = pStatus->getState(true);
	unitmode = pStatus->getMode(true);
	res = (int)unitmode;
	if ((unitmode == mstart) || (unitmode == minit0))
		res = -1;
	else {
		if (unitmode == mrail) {
			if ((unitstate == aj1) || (unitstate == aj1e1)) res = (int)madj1;
			else if ((unitstate == aj2) || (unitstate == aj2e1)) res = (int)madj2;
			else if (unitstate == aj3)	res = (int)madj3;
			else if ((unitstate == brkstate2) || (unitstate == scr))
				res = (int)mcancel;
		}
		else if (((unitmode == mpoisk) || (unitmode == mhand)) &&
				((unitstate == brkstate2) || (unitstate == scr)))
				 res = (int)mcancel;
	}
	if (locked == false) pStatus->unlock();
	return res;
}
// --------------------------------------------------------------
int cavtk::getmode(void)
{
int res;
	pStatus->lock();
	res = getmode(true);
	pStatus->unlock();
	return res;
}

// --------------------------------------------------------------
// возвращает 0, если переход в друой режим осуществлен
// -1 ошибка - переход недопустим
// -2 - не выполнены условия для перехода
// -3 - переход был отклонен из-за невыполнения команды контроллером
// -4 - не дождались завершения перехода для перехода
// -5 - ошибка события на ожидании
int cavtk::setmode(CMODE newmode)
{
int res = 0;
CSTATE unitstate;
CMODE unitmode;

	if ((newmode != minit)
		&& (newmode != mpoisk)
		&& (newmode != mtune)
		&&  (newmode != mrail)
		&&  (newmode != mhand))
		{
		 crash("Вызов setmode() c недопустимым параметром = %d",newmode);
		 return-1;
		}
//
	EnterCriticalSection(&setModeCr);
	pStatus->lock();
	unitstate = pStatus->getState(true);
	unitmode = pStatus->getMode(true);
//

	if ( (unitmode != newmode) &&
		 (unitmode != minit) &&
		 (newmode != minit))
	{
			pStatus->unlock();
			LeaveCriticalSection(&setModeCr);
			return -1;
	}
//
	if ((unitmode == minit) && (unitstate != s0i))
	{
			pStatus->unlock();
			LeaveCriticalSection(&setModeCr);
			return -1;
	}
//
	if (((unitmode == mpoisk) ||
		(unitmode == mrail) ||
		(unitmode == mtune)  ||
		(unitmode == mhand)) &&
		(unitstate != s0) &&
		(unitstate != s0E))
	{
			pStatus->unlock();
			LeaveCriticalSection(&setModeCr);
			return -1;
	}

	if (unitmode == newmode)
	{
		if (!((unitmode == mrail) && ((unitstate == aj1) || (unitstate == aj2))))
		{
			pStatus->unlock();
			LeaveCriticalSection(&setModeCr);
			return res;
		}
	}
//
	if ((newmode == madj1) || (newmode == madj2))
	{
		if (unitmode != mrail) res = -1;
		else
		{
		if (newmode == madj1)
		{
			switch(unitstate) {
			case sseize:
				set_state(aj1, true);
				break;
			case aj1:	break;
			default:
				res = -1;
			}
		}
		else if (newmode == madj2)
			 {
				 switch(unitstate)
				 {
					 case aj1:	 set_state(aj1e1, true);
						 break;
					 case aj1e1:
					 case aj2:
						 break;
					 default:
					 res = -1;
				 }
			  }
		 }
	}
	else if ((newmode == mrail) && (unitmode == mrail) &&
		((unitstate == aj1) || (unitstate == aj2) ) )
		 {
			set_state(scr, true);
		 }
			 else
			 {
				 ResetEvent(hSetModeEvent);
				 res = set_mode(newmode, true);
				 if (res == -1) res = -2;
		         if (res == 0)
				 {
					 pStatus->unlock();
					 DWORD waitRes = WaitForSingleObject(hSetModeEvent,setModeTimeout) ;
					 switch(waitRes)
					 {
						 case WAIT_OBJECT_0:
						 {
							 pStatus->lock();
							 unitmode = pStatus->getMode(true);
							 if (unitmode != newmode)
							 {
								 crash("Переход в режим %d не выполнен т.к. команда установки логических выходов была отклонена контроллером",newmode);
								 res = -3;
							 }
							 break;
						 }
						 case WAIT_TIMEOUT:
						 {
							 crash("Переход в режим %d не выполнен т.к. истек тайм-аут ожидания события",newmode);
							 res = -4;
							 pStatus->lock();
							 break;
						 }
						 default:
							 crash("Переход в режим %d не выполнен т.к. возникла ошибка ожидания события %d",newmode,waitRes);
							 res = -5;
							 pStatus->lock();
					}
				 }
			   }
   pStatus->unlock();
   LeaveCriticalSection(&setModeCr);
   return res;
}
// --------------------------------------------------------------
// д.б. pStatus->lock()

void cavtk::go_mode(CMODE newmode)
{
STATUS s;

	 if ((newmode == minit) || (newmode == mrail) || (newmode == mpoisk) || (newmode == mtune) || (newmode == mhand))
	 {
		 switch(newmode)
		 {
			case minit:
			{
CSTATE unitstate;
				s.mode = minitreq;
				unitstate = pStatus->getState(true);
				if (unitstate == s0E) s.state = s0FBLOFF; // принудительное отключение блокировки
					else s.state = s0BLOFF;
				break;
			}
			case mrail:
			{
				s.mode = mrailreq;
				s.state = s0BLON;   // включение блокировки
				break;
			}
			case mpoisk:
			{
				s.mode = mpoiskreq;
				s.state = s0BLON;
				break;
			}
			case mtune:
			{
			   s.mode = mtunereq;
			   s.state = s0BLON;
			   break;
			}
			case mhand:
			{
			   s.mode = mhandreq;
			   s.state = s0BLON;
			   break;
			}

		 }
		 setStatus(&s,true);
		 LOG("go_mode: Режим -%s:%s-  установлен",getModeName(s.mode),getStateName(s.state));
	 }
		 else
		 {
			while(0);
			pStatus->setMode(newmode,true);
			LOG("go_mode: Режим -%s-  установлен",getModeName(newmode));
		 }
	defineKeysStatus();
}

// --------------------------------------------------------------
// устанавливает режимы minit,mrail mpoisk,mtune,mhand
// locked - устанавливаем в true при вызове из engine
// возвращает 0, если переход в друой режим осуществлен
// -1 - не выполнены условия для перехода
int cavtk::set_mode(CMODE newmode, BOOL locked) {
	//
unsigned int ii;
CSTATE unitstate;
CMODE unitmode;
	//
	if (!locked) pStatus->lock();
	unitstate = pStatus->getState(true);
	unitmode = pStatus->getMode(true);
	//
	ii = 0;
	switch(newmode)
	{
	case mpoisk:
	case mhand:
#ifdef TUNE_WITHOUT_TESTRAIL
	case mtune:
#endif
					 {
#ifndef IGNORE_RAILSENSOR
			if (dsensorstate[dtindc1_id - 1] == false) {
				ii = 1;
				set_error(e_id1off);
			}
			if (dsensorstate[dtindc2_id - 1] == false) {
				ii = 1;
				set_error(e_id2off);
			}
#endif
#ifndef IGNORE_KVG
			if (dsensorstate[dtkvg2_id - 1] == false) {
				ii = 1;
				set_error(e_kvg2off);
			}
			//
			if (dsensorstate[dtkvg4_id - 1] == false) {
				ii = 1;
				set_error(e_kvg4off);
			}
#endif
			//
			break;
		}
		//
	case mrail:
#ifndef TUNE_WITHOUT_TESTRAIL
	case mtune:
#endif
#ifndef IGNORE_RAILSENSOR
		if (dsensorstate[dtindc1_id - 1] == true) {
			ii = 1;
			set_error(e_id1on);
		}
		if (dsensorstate[dtindc2_id - 1] == true) {
			ii = 1;
			set_error(e_id2on);
		}
#endif
		//
		break;
	}
	if (ii == 0) go_mode(newmode);
	if (!locked) pStatus->unlock();
	if (ii)	return -1;
	return 0;
}

// --------------------------------------------------------------
void __fastcall cavtk::dsensorevent(unsigned char num, BOOL state)
{
	if (dsensorstate[num - 1] != state)
	{
		dsensorstate[num - 1] = state;
		if ((num >= k_zahvat_id) && (num <= k_rels_id))
		 {
			 if (state) qKeyEvent.push(num | KEYPRESSEDEVENTMASK);
				 else qKeyEvent.push(num);
		 }
	}
}
// --------------------------------------------------------------
void __fastcall cavtk::asensorevent(unsigned char num, unsigned short value) {
	asensorvalue[num - 1] = value;
}

// --------------------------------------------------------------
void __fastcall cavtk::avtErrorEvent(unsigned char majorcode, unsigned char minorcode)
{
	set_error(e_keyPressedTooRapidly);
}
// --------------------------------------------------------------
void __fastcall cavtk::outputsEvent(unsigned char num, BOOL state)
{
	actualOutputs[num - 1] = state;
}
// --------------------------------------------------------------
// предварительно д.б. вызвана pStatus->lock()
void cavtk::go_status(STATUS newS)
{
STATUS s;
		   pStatus->setStatus(newS.mode,newS.state,true);
		   if ((newS.state & SHORTPATH) == 0)
		   {
			   s.mode = pStatus->getMode(true);
			   s.state = unsteady1;
			   pStatus->pushStatus(s,true);
			   goStatus(s);
		   }
			  else goStatus(newS);

}
// --------------------------------------------------------------
// предварительно д.б. вызвана pStatus->lock()
void cavtk::go_status(STATUS newS, VPROCB proc,BOOL parameter)
{
	go_status(newS);
	if (proc) proc(parameter);
}
// --------------------------------------------------------------
// предварительно д.б. вызвана pStatus->lock()
void cavtk::goStatus(STATUS s)
{
	defineKeysStatus();
	LOG("goStatus: cостояние -%s:%s- установлено",getModeName(s.mode),getStateName(s.state));
}
// --------------------------------------------------------------
// предварительно д.б. вызвана pStatus->lock()
void cavtk::goState(CSTATE newstate)
{
	defineKeysStatus();
	LOG("Состояние -%s- установлено", getStateName(newstate));
}
// --------------------------------------------------------------
// предварительно д.б. вызвана pStatus->lock()
void cavtk::walkTowardFormerState(void)
{
STATUS s;
    s = pStatus->popStatus(true);
    pStatus->pushStatus(s,true);
	pStatus->setState(unsteady1,true);
}
// --------------------------------------------------------------
void _fastcall cavtk::motionEndProc(tMotionResult result) {
	motionEndError.errorNumber = result.error.errorNumber;
	motionEndError.code1 = result.error.code1;
	motionEndError.code2 = result.error.code2;
	motionEndError.code3 = result.error.code3;
	fMotionEnd = true;
}
// --------------------------------------------------------------
void cavtk::clearMotionEndError(void)
{
    motionEndError.errorNumber = ERROR_NO;
    motionEndError.code1 = motionEndError.code2 = motionEndError.code3 = 0;

}
// --------------------------------------------------------------
void cavtk::motorErrorLog(ERRORMSG error) {
	if (error.errorNumber != ERROR_NO)
		LOG(
		"motorErrorLog: error = %d, code1 = %d, code2 = %d, code3 = %d ",
		error.errorNumber, error.code1, error.code2, error.code3);

}

// --------------------------------------------------------------
void __fastcall cavtk::motorPWROnEndProc(tMotionResult result) {
	if (result.error.errorNumber == ERROR_NO) {
		fMotorPowered = true;
	}
					motionEndProc(result);
}
// --------------------------------------------------------------
void __fastcall cavtk::motorPWROffEndProc(tMotionResult result) {
	if (result.error.errorNumber == ERROR_NO) {
		fMotorPowered = false;
	}
					motionEndProc(result);
}
// --------------------------------------------------------------
/*
первый вариант
void cavtk::setStatus(STATUS *statusPtr,BOOL locked)
{
	if (!locked) pStatus->lock();
	pStatus->setMode(statusPtr->mode,true);
	set_state(statusPtr->state,true);
	if (!locked) pStatus->unlock();
}
*/
void cavtk::setStatus(STATUS *newStatus, BOOL locked)
{
CSTATE unitstate;
CSTATE newstate;
CMODE unitmode;

	if (!locked) pStatus->lock();
	unitstate = pStatus->getState(true);
	unitmode = pStatus->getMode(true);
	newstate = newStatus->state;
	if (newstate == unitstate)
	{
		if (!locked) pStatus->unlock();
		return;
	}
	//
	switch(newstate)
	{
	case srailu: // поднятие тестового рельса, каретки разведены
	case srailu2: // поднятие тестового рельса, каретки разведены
		setPnRaspred3(true);
		settimer();
		go_status(*newStatus);
		break;
		//
	case s0: // тестовый рельс опущен, каретки разведены
	case s0E: //или каретки разведены неизвестно как или кое-как тестовый рельс опущен
		setWaterGate(false);
		go_status(*newStatus);
		break;
	case srail: // тестовый рельс поднят, каретки разведены
	case sraile:
		setWaterGate(false);
		go_status(*newStatus);
		break;
		//
	case sraild: // опускание тестового рельса, каретки разведены
		setPnRaspred3(false);
		settimer();
		go_status(*newStatus);
		break;
		//
	case sraild2:
		resetPnRaspred3Forced();
		settimer();
		go_status(*newStatus);
		break;
//
	case sseize0: // процесс сведения кареток
		setPnRaspred2(true);
		settimer();
		go_status(*newStatus);
		break;
		//
	case sleave0: // процесс разведения кареток
		setPointer(false);
		setPnRaspred2(false);
		settimer();
		go_status(*newStatus);
		break;
		//
	case sseize: // каретки сведены
		if (unitmode != mtune) setPointer(true);
		setWaterGate(false); // если stune->sseize и не только
		go_status(*newStatus);
		break;
		//
	case spredscan:
	case aj1: // юстировка 1
		setPointer(false);
//
	case spredtune:
		setWaterGate(true);
		go_status(*newStatus);
		break;
//
	case aj3: // юстировка 3
		go_status(*newStatus,puskKeyPressedProc,true);
		break;
//
	case stune:
		if (unitstate == spredtune)  go_status(*newStatus,puskKeyPressedProc,true);
			else  go_status(*newStatus);
		break;
//
	case sscan: // процесс сканирования рельса
		if (unitstate == spredscan) go_status(*newStatus,puskKeyPressedProc,true);
			else  go_status(*newStatus);
		break;
//
	 case spostscan1:
	 case spostscan2:
	 case sposttune1:
	 case sposttune2:
		 if (puskKeyPressedProc) go_status(*newStatus,puskKeyPressedProc,false);
			 else  go_status(*newStatus);
		 break;
	 case s0BLOFF:
		setBlokir(FALSE);
		go_status(*newStatus);
		break;
//
	 case s0FBLOFF:
		resetBlokirForced();
		go_status(*newStatus);
		break;
//
	 case s0BLON:
		setBlokir(TRUE);
		go_status(*newStatus);
		break;

		case sleavee:
		case srailde:
		case srailde2:
			fAnswerPollingIntervalON = FALSE;
			go_status(*newStatus);
			 break;

		case s0i:
		case brkstate2:
		case aj1e1:
		case aj2e1:
		case aj2: // юстировка 2
		case motorRestore2:
		case motorRestore3:
		case motorRestore4:
		case scr:
		case stuneWaitEndMotion:
		case spwron:
		case spwroff:
			go_status(*newStatus);
			break;
	default:
	 wrongStateStop("setStatus(): неизвестный параметр-",newstate,locked);
	}
	if (!locked) pStatus->unlock();
}

// --------------------------------------------------------------
// locked устанавливаем при вызове из engine
// производит переход в состояние newstate, если не не было
// ошибок по двигателям
// в случае ошибок переходим в restartMotor1
void cavtk::set_state(CSTATE newstate, BOOL locked)
{
bool ferror = false;

	if (fMD == false)
					{
	 switch(newstate) {
		case spwroff:
			if (isMotorPowered() == true)
											 {
			   fMotionEnd = false;
			   switchMotorPwr(false);
			}
			   else
			   {
				  clearMotionEndError();
				  fMotionEnd = true;
			   }
		break;
//
		case spwron:
			fMotionEnd = false;
			switchMotorPwr(true);
			break;
//
										 case aj2e1:
		case scr:      { // перемещение верхней и нижней кареток в исходное положение
				fMotionEnd = false;
				motorDev->command(caretReturn, &motionEndProc);
				break;
			}
			//
		case brkstate2: {
				fMotionEnd = false;
				motorDev->command(cycleBreak, &motionEndProc);
				break;
			}
			//
		case aj1e1: {
				fMotionEnd = false;
				motorDev->command(adjust2, &motionEndProc);
				break;
			}
		case stuneWaitEndMotion: break;

		default:
			if ((newstate & NONSTOREDSTATE) != 0)
			{
			   wrongStateStop("set_state(): неизвестный параметр-",newstate,locked);
			   ferror = true;
			}
		}
		if (ferror == false) setstate(newstate, locked);
	}
		else
		{
			switch (newstate)
			{
								   case spwroff:
								   case spwron:
								   case scr:
								   case aj1e1:
								   case aj2e1:
								   case brkstate2:
								   case stuneWaitEndMotion:
					clearMotionEndError();
					fMotionEnd = true;
			}
			setstate(newstate, locked);
		 }

}

// --------------------------------------------------------------
// locked устанавливаем при вызове из engine
// производит переход в друое состояние
void cavtk::setstate(CSTATE newstate, BOOL locked)
{
STATUS s;
	if (!locked) pStatus->lock();
	s.mode = pStatus->getMode(true);
	s.state = newstate;
	setStatus(&s,true);
	if (!locked) pStatus->unlock();
}

/*
старый вариант
void cavtk::setstate(CSTATE newstate, BOOL locked)
{
CSTATE unitstate;
CMODE unitmode;

	if (!locked) pStatus->lock();
	unitstate = pStatus->getState(true);
	unitmode = pStatus->getMode(true);
	if (newstate == unitstate)
	{
		if (!locked) pStatus->unlock();
		return;
	}
	//
	switch(newstate)
	{
	case srailu: // поднятие тестового рельса, каретки разведены
	case srailu2: // поднятие тестового рельса, каретки разведены
		setPnRaspred3(true);
		settimer();
		go_state(newstate);
		break;
		//
	case s0: // тестовый рельс опущен, каретки разведены
	case s0E: //или каретки разведены неизвестно как или кое-как тестовый рельс опущен
		setWaterGate(false);
		go_state(newstate);
		break;
	case srail: // тестовый рельс поднят, каретки разведены
	case sraile:
		setWaterGate(false);
		go_state(newstate);
		break;
		//
	case sraild: // опускание тестового рельса, каретки разведены
		setPnRaspred3(false);
		settimer();
		go_state(newstate);
		break;
		//
	case sraild2:
		resetPnRaspred3Forced();
		settimer();
		go_state(newstate);
		break;
//
	case sseize0: // процесс сведения кареток
		setPnRaspred2(true);
		settimer();
		go_state(newstate);
		break;
		//
	case sleave0: // процесс разведения кареток
		setPointer(false);
		setPnRaspred2(false);
		settimer();
		go_state(newstate);
		break;
		//
	case sseize: // каретки сведены
		if (unitmode != mtune) setPointer(true);
		setWaterGate(false); // если stune->sseize и не только
		go_state(newstate);
		break;
		//
	case spredscan:
	case aj1: // юстировка 1
		setPointer(false);
//
	case spredtune:
		setWaterGate(true);
		go_state(newstate);
		break;
//
	case aj3: // юстировка 3
		go_state(newstate,puskKeyPressedProc,true);
		break;
//
	case stune:
		if (unitstate == spredtune)  go_state(newstate,puskKeyPressedProc,true);
			else  go_state(newstate);
		break;
//
	case sscan: // процесс сканирования рельса
		if (unitstate == spredscan) go_state(newstate,puskKeyPressedProc,true);
			else  go_state(newstate);
		break;
//
	 case spostscan1:
	 case spostscan2:
	 case sposttune1:
	 case sposttune2:
		 if (puskKeyPressedProc) go_state(newstate,puskKeyPressedProc,false);
			 else  go_state(newstate);
		 break;
	 case s0BLOFF:
		setBlokir(FALSE);
		go_state(newstate);
		break;
//
	 case s0FBLOFF:
		resetBlokirForced();
		go_state(newstate);
		break;
//
	 case s0BLON:
		setBlokir(TRUE);
		go_state(newstate);
		break;

		case sleavee:
		case srailde:
		case srailde2:
			fAnswerPollingIntervalON = FALSE;
			go_state(newstate);
			 break;

		case s0i:
		case brkstate2:
		case aj1e1:
		case aj2e1:
		case aj2: // юстировка 2
		case motorRestore2:
		case motorRestore3:
		case motorRestore4:
		case scr:
		case stuneWaitEndMotion:
		case spwron:
		case spwroff:
			go_state(newstate);
			break;
	default:
	 wrongStateStop("setstate(): неизвестный параметр-",newstate,locked);
	}
	if (!locked) pStatus->unlock();
}
*/



// --------------------------------------------------------------
// запрещает все кнопки пульта
void cavtk::setkeysdisabled(void) {
	for (unsigned int ii = 0; ii < a_key; ii++)
		keysStatus[ii] = KDIS;
}
// --------------------------------------------------------------
// запрещает все кнопки пульта
void cavtk::setkeysmissed(void) {
	for (unsigned int ii = 0; ii < a_key; ii++)
		keysStatus[ii] = KMIS;
}

// --------------------------------------------------------------
// предварительно д.б. вызвана pStatus->lock()
// выставляет разрешения кнопок в соответствии состоянием и режимом
//
void cavtk::defineKeysStatus(void)
{
	unsigned int kk;
	CSTATE unitstate;
	CMODE unitmode;

					 unitstate = pStatus->getState(true);
					 unitmode = pStatus->getMode(true);
	if ((unitmode == mpoisk) ||  (unitmode == mrail) ||  (unitmode == mtune) ||  (unitmode == mhand))
	    {
                        switch (unitmode)
                        {
                             case mpoisk:  kk = 0; break;
                             case mrail:     kk = 1; break;
                             case mtune:   kk = 2; break;
                             default: kk = 3;
                        }
		for (unsigned int ii = 0; ii < a_key; ii++)
		keysStatus[ii] = cKeysStatus[unitstate & ~(NONSTOREDSTATE | SHORTPATH)][kk][ii];
                     }
                         else if (unitmode == minit) setkeysdisabled();
                                    else  setkeysmissed();
}

// --------------------------------------------------------------
int cavtk::getdsensorarray(BOOL *array, unsigned int size) {
	unsigned int i;
	if (size < a_dsensor)
		return-1;
	else {
		for (i = 0; i < a_dsensor; i++) {
			*array++ = dsensorstate[i];
		}
	}
	return a_dsensor;
}

// --------------------------------------------------------------
int cavtk::getasensorarray(unsigned int *array, unsigned int size) {
	unsigned int i;
	if (size < a_asensor)
		return-1;
	else {
		for (i = 0; i < a_asensor; i++) {
			*array++ = asensorvalue[i];
		}
	}
	return a_asensor;
}

// --------------------------------------------------------------
unsigned long cavtk::get_duration(void) {
	register long ii;
	unsigned long mscurrent;
	mscurrent = GetTickCount_();
	ii = mscurrent - tmr;
	return(unsigned long)((ii >= 0) ? ii : (0 - ii));
}

// --------------------------------------------------------------
unsigned long cavtk::get_duration(unsigned long *pt) {
	register long ii;
	unsigned long mscurrent;
	mscurrent = GetTickCount_();
	ii = mscurrent - *pt;
	return(unsigned long)((ii >= 0) ? ii : (0 - ii));
}

//
//
// --------------------------------------------------------------
void cavtk::set_error(CERROR errcode)
{
   setError(errcode,FALSE);
}
// --------------------------------------------------------------
void cavtk::setError(CERROR errcode,BOOL fToAnswer)
{
char *ps;
tCavtkMsg msg;

	LOG("Код ошибки %d сформирован", (unsigned int)errcode);
	ps = get_errstr(errcode);
	if (ps) errorLOG(ps);

	msg.type = ErrorMesage;
	msg.code = static_cast<unsigned int>(errcode);
	if (fToAnswer) msg.flag = AnswerBinary;
		else msg.flag = AnswerNone;
	outcomeMsgBuffer.push(msg);
}
// --------------------------------------------------------------
/*
void cavtk::set_error(CERROR errcode, int value)
{
	char *ps;

	LOG("Код ошибки %d сформирован", (unsigned int)errcode);
	if (errorNumSet_proc) errorNumSet_proc(errcode);
			ps = get_errstr(errcode);
	if (ps) errorLOG(ps);
}
*/
// --------------------------------------------------------------
//перед началом вызова ее pStatus->lock уже вызван
void __fastcall cavtk::cavtkCallBack(tMotionResult Result)
{
CSTATE unitstate;
#ifdef SCAN_MOTION_DEBUG
CMODE unitmode;
#endif

	 if (motionEndEventProc)
		motionEndEventProc(Result);
	  motionEndEventProc = NULL;

	  unitstate = pStatus->getState(true);
#ifdef SCAN_MOTION_DEBUG
	  unitmode = pStatus->getMode(true);
#endif
	  if (Result.error.errorNumber == ERROR_MOTION_NOT_COMPLETED)
	  {
	      if (pStatus->getMode(true) != mtune)   set_state(spostscan1, true);
                               else set_state(stune, true);
	  }
		  else  if (Result.parameter == cLastMotion)
		           switch (unitstate)
				   {
#ifndef SCAN_MOTION_DEBUG
					   case sscan:	set_state(spostscan2, true); break;
#endif
					   case aj3:    set_state(spostscan1, true); break;
#ifndef SCAN_MOTION_DEBUG
						case stuneWaitEndMotion:  set_state(stune, true); break;
#else
						case stuneWaitEndMotion:
						{
							if (unitmode == mtune)  set_state(stune, true);
								else set_state(sscan, true);
							break;
						}
#endif
				   }
	conditionalGoToMotorReviving(&Result.error, true);
}
// --------------------------------------------------------------
// возвращает: -1, если параметр proc не задан, иначе - 0
// функция proc может быть вызвана до того, как завершится эта
int cavtk::motionStart(tCallBack proc) {
CSTATE unitstate;
MOTIONRESULT r = {
		0, ERROR_NO, 0, 0, 0
	};
	if (proc == NULL)
		return-1;
	//
	pStatus->lock();
	unitstate = pStatus->getState(true);
	if ((unitstate == aj3)
#ifndef SCAN_MOTION_DEBUG
                         || (unitstate == sscan)
#endif
						 ) {
		if (fMD == false) {
			if (motionEndEventProc == NULL) {

				motionEndEventProc = proc;
				if (unitstate == sscan)
					motorDev->command(scancycle, &cavtkCallBack);
				else
					motorDev->command(adjust3, &cavtkCallBack);
			}
			else {
				r.error.errorNumber = ERROR_ACTION_NOT_COMPLETED;
				r.parameter = getmode(true);
				proc(r);

			}
		}
#ifdef SIMULATE_MOTION_END
        else proc(r);
#endif
	}
	else {
		r.error.errorNumber = ERROR_ACTION_DENIED;
		r.parameter = getmode(true);
		proc(r);
	}
	pStatus->unlock();
	return 0;
}

// -----------------------------------------------------------------------------------------
// вызывает смещение каретки на smesh мм относительно текущего положения в режиме настройка
// в состоянии stune
// возвращает: -1, если параметр proc не задан, иначе - 0
// fBottomTrolley = false - верхняя каретка
// smesh < 0 - движение влево
// возможно функция proc вызовется до того, как завершится эта
int cavtk::motionStart(BOOL fBottomTrolley, int smesh, tCallBack proc)
{
	if (fBottomTrolley) return motionStart2(smesh,0,proc);
        else return motionStart2(0,smesh,proc);
}
// -----------------------------------------------------------------------------------------
// вызывает смещение кареток относительно текущего положения в режиме настройка
// в состоянии stune
// возвращает: -1, если параметр proc не задан, иначе - 0
// upperTrolleyShift - смещение верхней каретки, bottomTrolleyShift - нижней
// ...TrolleyShift< 0 - движение влево
// возможно функция proc вызовется до того, как завершится эта
int cavtk::motionStart2(int bottomTrolleyShift, int upperTrolleyShift, tCallBack proc)
{
MOTIONRESULT r = {0, ERROR_NO, 0, 0, 0};
CSTATE unitstate;
CMODE unitmode;
	if (proc == NULL) return-1;
	pStatus->lock();
	unitstate = pStatus->getState(true);
	unitmode = pStatus->getMode(true);
	if ((unitmode == mtune) && (unitstate == stune)
#ifdef SCAN_MOTION_DEBUG
                         || (unitstate == sscan)
#endif
                        )
	{
		if (fMD == false)
		{
			if (motionEndEventProc == NULL)
			{
				set_state(stuneWaitEndMotion,true);
				motionEndEventProc = proc;
				motorDev->command2(bottomTrolleyShift,upperTrolleyShift,&cavtkCallBack);
			}
				else
				{
					r.error.errorNumber = ERROR_ACTION_NOT_COMPLETED;
					r.parameter = getmode(true);
					proc(r);
			   }
		}
#ifdef SIMULATE_MOTION_END
        else proc(r);
#endif
	}
		else
		{
			r.error.errorNumber = ERROR_ACTION_DENIED;
			r.parameter = getmode(true);
			proc(r);
		}
	pStatus->unlock();
	return 0;
}
// --------------------------------------------------------------
// прерывает последовательность движений в состояниях сканирование или
// Юстировка 3 и в  режиме Настройка:
//
// возвращает: -1, если параметр proc не задан, при успешном завершении - 0
// или ERROR_ACTION_DENIED
// может вызываться, после сведения кареток
// возможно функция proc вызовется до того, как завершится эта
int cavtk::motionBreak(tCallBack proc) {
CSTATE unitstate;
CMODE unitmode;


	MOTIONRESULT r = {
		0, ERROR_NO, 0, 0, 0
	};
	if (proc == NULL)
		return-1;
	//
	pStatus->lock();
	unitstate = pStatus->getState(true);
	unitmode = pStatus->getMode(true);
	if ((unitstate == sscan) || (unitstate == aj3) || (unitstate == stune) || (unitstate == stuneWaitEndMotion))   
                     {
                        if (unitmode == mtune) set_state(sposttune1, true);
                            else set_state(spostscan1, true);
	}                else 
                                     {
		r.error.errorNumber = ERROR_ACTION_DENIED;
		r.parameter = getmode(true);
		proc(r);
				   }
	pStatus->unlock();
	return 0;
}
// --------------------------------------------------------------
// вызывает возврат кареток в парковочное положение в режиме настройка
// в состоянии stune или в состоянии sscan, если SCAN_MOTION_DEBUG
// возвращает: -1, если параметр proc не задан, иначе - 0
// возможно функция proc вызовется до того, как завершится эта
int cavtk::trolleysReturn(tCallBack proc)
{
MOTIONRESULT r = {0, ERROR_NO, 0, 0, 0};
CSTATE unitstate;
CMODE unitmode;
	if (proc == NULL) return-1;
	pStatus->lock();
	unitstate = pStatus->getState(true);
	unitmode = pStatus->getMode(true);
	if ((unitmode == mtune) && (unitstate == stune)
#ifdef SCAN_MOTION_DEBUG
                    || (unitstate == sscan)
#endif
                        )
	{
		if (fMD == false)
		{
			if (motionEndEventProc == NULL)
			{
				set_state(stuneWaitEndMotion,true);
				motionEndEventProc = proc;
				motorDev->command(caretReturn,&cavtkCallBack);
			}
				else
				{
					r.error.errorNumber = ERROR_ACTION_NOT_COMPLETED;
					r.parameter = getmode(true);
					proc(r);
			   }
		}
#ifdef SIMULATE_MOTION_END
        else proc(r);
#endif
	}
		else
		{
			r.error.errorNumber = ERROR_ACTION_DENIED;
			r.parameter = getmode(true);
			proc(r);
		}
	pStatus->unlock();
	return 0;
}
// --------------------------------------------------------------
BOOL cavtk::isTransferToMotorDisabled() {
	return fTransferToMotorDis;
}

// --------------------------------------------------------------
//
// -1 - объект уже существует и инициализирован
// -2 - не удалось создать объект управления двигателями
// -3 - функция инициализации выполнилась с ошибкой
// -4 - функция инициализации-1 выполнилась с ошибкой
int cavtk::motorInit(UCHAR idx, cDataTransfer *DT) {
	int result = 0;
	//
	if (motorDev == NULL) {
		motorDev = new motorctrl(4, 3,closeWaterGate);
		if (motorDev) {
			if (motorDev->init(idx, DT) == 0) {
				result = 0;
				// result = motorDev->init1();
				// if (result < 0)
				// result = -4;
			}
			else
				result = -3;
		}
		else
			result = -2;
	}
	else
		result = -1;
	return result;
}

// --------------------------------------------------------------
//
// -1 - не создан объект управления двигателями
// -2 - функция инициализации-1 выполнилась с ошибкой
int cavtk::motorInit1(void) {
	int result = 0;

	if (motorDev) {
		result = motorDev->init1();
		if (result < 0) result = -2;
 //			else defaultVelocity = motorDev->getDefaultVelocity();
	}
	else
		result = -1;
	return result;
}

// --------------------------------------------------------------
//
void cavtk::defineKv1Kv2FailIgnore(BOOL state) {
	kv1Kv2FailIgnore = state;
}
// --------------------------------------------------------------
void cavtk::defineInitalRailPositionIgnore(BOOL state) {
	initalRailPositionIgnore = state;
}

// --------------------------------------------------------------
void cavtk::goToMotorReviving(BOOL locked)
{
CSTATE unitstate;

    if (!locked) pStatus->lock();
    fMD = true;
    fTransferToMotorDis = true;
	unitstate = pStatus->getState(true);
    if (motorRestoreCounter != 0)
	{
        motorRestoreCounter--;
		if ((unitstate == motorRestore1)  ||
			   (unitstate == motorRestore2) ||
			   (unitstate == motorRestore3) ||
			   (unitstate == motorRestore4) ) pStatus->setState(motorRestore1,true);
                   else pStatus->pushStatus(motorRestore1,true);
               goState(motorRestore1);
    }
        else
        {
            set_error(e_drvaborted);
            fMotorDisabled = true;
			if ((unitstate == motorRestore1)  ||
			   (unitstate == motorRestore2) ||
			   (unitstate == motorRestore3) ||
			   (unitstate == motorRestore4) ) leaveMotorReviving();
        }
    if (!locked) pStatus->unlock();
}
// --------------------------------------------------------------
void cavtk::conditionalGoToMotorReviving(ERRORMSG *pe, BOOL locked) {
	if ((pe) && (pe->errorNumber != ERROR_NO) &&
		(pe->errorNumber != ERROR_ACTION_DENIED) && (pe->errorNumber != ERROR_WRONG_TROLLEY_POS)&& (pe->errorNumber != ERROR_CMD_CANCELLED)) {
                                          goToMotorReviving(locked);
	}
}
// --------------------------------------------------------------
// предварительно д.б. вызвана pStatus->lock()
void cavtk::leaveMotorReviving(void)
{
STATUS s;
	s = pStatus->popStatus(true);
    goState(s.state);
}
// --------------------------------------------------------------
void cavtk::goToKRAReviving(BOOL locked)
{
const STATUS s = {minit0,kraRestore1};
    if (!locked) pStatus->lock();
    pStatus->pushStatus(s,true);
	setkeysmissed();
	LOG("%s:%s was set", getModeName(s.mode),getStateName(s.state));
    if (!locked) pStatus->unlock();
}
// --------------------------------------------------------------
void cavtk::switchMotorPwr(BOOL fPwrOn)
{
    if (fPwrOn == true) motorDev->command(drvPwrOn, &motorPWROnEndProc);
	else motorDev->command(drvPwrOff, &motorPWROffEndProc);
}
// --------------------------------------------------------------
BOOL cavtk::isMotorPowered(void) {
	return fMotorPowered;
}
// --------------------------------------------------------------
void __fastcall cavtk::infoEvent(unsigned char *buf, unsigned int dataLength)
{
    if (infoDataBuffer)
    {
        memcpy(infoDataBuffer,buf,std::min(infoDataBufferLength,dataLength));
        SetEvent(infoQryEvent);
    }
}
// --------------------------------------------------------------
// возвращает серийный номер контроллера - положительное число (младшие 2 байта)
// в течение infoQryTimeout мС
// в случае ошибки возвращает отрицательное значение
// -1 - предыдущий запрос не завершен
// -2 -  не дождались ответа на запрос
// -3 -  ошибка функции ожидания события
//
int cavtk::getDeviceNumber(void)
{
USHORT buf;
int res;

   EnterCriticalSection(&infoQryCr);
   if (infoDataBuffer != 0)
   {
      LeaveCriticalSection(&infoQryCr);
       return (-1);
	}
   infoDataBuffer = (UCHAR*)&buf;
   infoDataBufferLength = sizeof(buf);
   ResetEvent(infoQryEvent);
   LeaveCriticalSection(&infoQryCr);
   dev->QryInfo(DEVICENUMBER_ID);
   switch (WaitForSingleObject(infoQryEvent,infoQryTimeout))
   {
      case WAIT_OBJECT_0:
      {
          res = buf & 0xFFFF;
          break;
      }
      case WAIT_TIMEOUT:
      {
          res =  -2;
          break;
      }
      default:
      {
        res = -3;
      }
   }
   EnterCriticalSection(&infoQryCr);
   infoDataBuffer = NULL;
   LeaveCriticalSection(&infoQryCr);
   return res;
}
//-------------------------------------------------------------------
// возвращает версию ПО адаптера в виде X.Y.Z :
// в течение infoQryTimeout мС
// в случае успеха возвращает 4 и в буфер пользователя записывает 4 байт,
// где Z -> bufptr[0], Y -> bufptr[1], X -> bufptr[2]
// в случае ошибки возвращает отрицательное значение
// -1 - предыдущий запрос не завершен
// -2 -  не дождались ответа на запрос
// -3 -  ошибка функции ожидания события
int cavtk::getVersion(UCHAR *buf)
{
int res;

   EnterCriticalSection(&infoQryCr);
	if (infoDataBuffer != 0)
	{
	   LeaveCriticalSection(&infoQryCr);
	   return (-1);
	}
   infoDataBuffer = buf;
   infoDataBufferLength = 4;
   ResetEvent(infoQryEvent);
   LeaveCriticalSection(&infoQryCr);
   dev->QryInfo(VERSION_ID);
   switch (WaitForSingleObject(infoQryEvent,infoQryTimeout))
   {
      case WAIT_OBJECT_0:
      {
          res = 4;
          break;
      }
      case WAIT_TIMEOUT:
      {
          res =  -2;
          break;
      }
      default:
      {
        res = -3;
      }
   }
   EnterCriticalSection(&infoQryCr);
   infoDataBuffer = NULL;
   LeaveCriticalSection(&infoQryCr);
   return res;
}
//-------------------------------------------------------------------
void __fastcall cavtk::alarmEvent(unsigned char number)
{
    alarmCounter = number;
}
//-------------------------------------------------------------------
void cavtk::setWaterGate(BOOL state)
{
	(targetOutputs[klv_id-1]).state = state;
}
//-------------------------------------------------------------------
void cavtk::setLight(BOOL state)
{
	(targetOutputs[pnraspred1_id-1]).state = state;
	dev->SetOutput(pnraspred1_id, state,FALSE);  // т.к. вызывается пользователем и нет перевода в unsteady1
}
//-------------------------------------------------------------------
void cavtk::setPnRaspred2(BOOL state)
{
	(targetOutputs[pnraspred2_id-1]).state = state;
}
//-------------------------------------------------------------------
void cavtk::setPnRaspred3(BOOL state)
{
	setPnRaspred3(state,FALSE);
}
//-------------------------------------------------------------------
void cavtk::resetPnRaspred3Forced(void)
{
	setPnRaspred3(FALSE,TRUE);
}
//-------------------------------------------------------------------
void cavtk::setPnRaspred3(BOOL state,BOOL fForced)
{
	(targetOutputs[pnraspred3_id-1]).forced = fForced;
	(targetOutputs[pnraspred3_id-1]).state = state;
}
//-------------------------------------------------------------------
void cavtk::setPointer(BOOL state)
{
	(targetOutputs[pointer_id-1]).state = state;
}
//-------------------------------------------------------------------
void cavtk::setBlokir(BOOL state)
{
	setBlokir(state,FALSE);
}
//-------------------------------------------------------------------
void cavtk::setBlokir(BOOL state,BOOL fForced)
{
	 (targetOutputs[kl_blokir_id-1]).forced = fForced;
	 (targetOutputs[kl_blokir_id-1]).state = state;
}
//-------------------------------------------------------------------
void cavtk::resetBlokirForced(void)
{
	setBlokir(FALSE,TRUE);
}
//-------------------------------------------------------------------
void cavtk::closeWaterGate() // вызывается перед выполнением 15 и 24 движений
{
        (targetOutputs[klv_id-1]).state = false;
		dev->SetOutput(klv_id, FALSE,FALSE);
}
//-------------------------------------------------------------------
void __fastcall cavtk::dataBlockEvent(unsigned char *pDataBuf)
{
DATAV1 *pdata;
DATAV2 *pdata2;
unsigned char ii;
unsigned short value;
unsigned char *pb;
	pdata = (DATAV1*)pDataBuf;
	if ((pdata->datapackversion == DATAV1SIGN) || (pdata->datapackversion == DATAV2SIGN))
	{
		 pb = pdata->dsensor;
		 for (ii=0; ii<a_dsensor; ii++)
		 {
			 register unsigned char b = *pb;
			 if ((b == off_state) || (b == on_state))
			 {
				 if (b == off_state)   dsensorevent(ii+1,FALSE);
					 else dsensorevent(ii+1,TRUE);
			 }
			 pb++;
		 }
		 pb = (UCHAR*)pdata->asensor;
		 for (ii=0; ii<a_asensor; ii++)
		 {
			 value = *pb;
			 pb++;
			 value |= (*pb) << 8;
			 pb++;
			 asensorevent(ii+1,value);
		 }
		 pb = (UCHAR*)pdata->output;
		 for (ii=0; ii<a_outputs; ii++)
		 {
		     register unsigned char b = *pb;
		     if ((b == off_state) || (b == on_state))
		     {
		         if (b == off_state)   outputsEvent(ii+1,FALSE);
		             else outputsEvent(ii+1,TRUE);
		    }
                                              pb++;
                                           }
                         fKRAtcpConnectionCounterValid = false; 
	}
	if (pdata->datapackversion == DATAV2SIGN) 
                     {
	    pdata2 = (DATAV2*)pDataBuf;
                         KRAtcpConnectionCounter = pdata2->tcpConnectionCounterValue;   
                         fKRAtcpConnectionCounterValid = true; 
                     } 
}
//-------------------------------------------------------------------
void cavtk::setOutputPinsToTarget(void)
{
register unsigned int ii;
	 for (ii=0; ii<a_outputs; ii++)
	 {
		 if(actualOutputs[ii] != (targetOutputs[ii]).state) dev->SetOutput(ii+1, (targetOutputs[ii]).state,(targetOutputs[ii]).forced);
	 }
 }
//-------------------------------------------------------------------
void cavtk::Tick(void)
{
    EnterCriticalSection(&tickCr);
    if (fTransferToKRAEn == true)   dev->Tick();
    LeaveCriticalSection(&tickCr);
}
//-------------------------------------------------------------------
void __fastcall cavtk::errorReportEvent(unsigned char *pDataBuf)
{
        errorReportBuffer.push(*((tErrorReport1DataV1*)pDataBuf));
}
//-------------------------------------------------------------------
// разбирает сообщения из очереди
// возвращает элементы массива targetOutputs к состоянию до подачи команды
// возвращает true, если в очереди были сообщения errorreportId
BOOL cavtk::testErrorReports(void)
{
BOOL fErrorReport = false;
tErrorReport1DataV1 tmp;
	while (errorReportBuffer.empty() == false)
	{
		memcpy(&tmp,(unsigned char *)&errorReportBuffer.front(),sizeof(tErrorReport1DataV1));
        errorReportBuffer.pop();
		if (tmp.datapackversion == ERRORREPORT1_V1)
        {
            fErrorReport = true;
            switch(tmp.outputpinvalue)
            {
                case on_state: 
				{
					(targetOutputs[tmp.outputpinid-1]).state = TRUE;
					break;
				}
				case off_state:
				{
					(targetOutputs[tmp.outputpinid-1]).state = FALSE;
					break;
				}
			}
			(targetOutputs[tmp.outputpinid-1]).forced = FALSE;
			crash("Дискретный выход %s оставлен в состоянии %s. %s",getOutputpinNameStr(tmp.outputpinid),getOutputpinStateStr(tmp.outputpinvalue),getKRAErrorStr(tmp.cause));
        }
			else crash("Принято сообщение errorreportId с неизвестным полем datapackversion = %d",tmp.datapackversion);
	}
	return fErrorReport;
}
//-------------------------------------------------------------------
//   записывает состояние кареток двигателей в структуру, задаваемую pDest (motorctrl.h)
//   0 нормальное завершение
//  -1 ошибка чтения регистров контроллера двигателей
//  -2 внутренняя ошибка
//  -3 объект управления двигателями не существует
int cavtk::getCariagesStatus(tCARIAGESTAT *pDest)
{
	if (motorDev) return motorDev->getCariagesStatus(pDest);
		else return -3;
}
//-------------------------------------------------------------------
//
// если к.л. из полей  minor или build в *pMinimalDemand равно 0, то значение поля несущественно
// для сравнения
BOOL cavtk::isKRAVersionValid(KRASOFTWAREVERSION *pMinimalDemand)
{

	 if ((kraSoftwareVer.isDataValid == true) &&
		 ( kraSoftwareVer.major & 0x7F >= pMinimalDemand->major) &&
		 ((pMinimalDemand->minor == 0) || ( kraSoftwareVer.minor >= pMinimalDemand->minor)) &&
		 ( (pMinimalDemand->minor == 0) || (pMinimalDemand->build == 0) || ( kraSoftwareVer.build >= pMinimalDemand->build))) return true;

	return false;
}
//-------------------------------------------------------------------
//возвращает true, если сообщение получено и записано в
//*pmsg
bool cavtk::getMessage(tCavtkMsg *pMsg)
{

  if (!outcomeMsgBuffer.empty())
  {
	memcpy((unsigned char*)pMsg,(unsigned char*)&outcomeMsgBuffer.front(),sizeof(tCavtkMsg));
	outcomeMsgBuffer.pop();
	return true;
  }
  return false;
}
//
//-------------------------------------
// заполняем
// pMsg->type = ErrorAnswer;
// pMsg->code - код ошибки из сообщения, на которое отвечаем
// pMsg->flag -  RejectCode или AcceptCode
void cavtk::sendMessage(tCavtkMsg *pMsg)
{
	incomeMsgBuffer.push(*pMsg);
}
//-------------------------------------

void cavtk::messageSeizeInit(void)
{
  while (!incomeMsgBuffer.empty())
  {
	incomeMsgBuffer.pop();
  }
}
//-------------------------------------
void cavtk::messageClear(void)
{
	imessage.type = MessageNone;
}
//-------------------------------------
CAVTKMSGTYPE cavtk::getMessageType(void)
{
	return imessage.type;
}
//-------------------------------------
void cavtk::messageSeize(BOOL toDelete)
{
  if (getMessageType() == MessageNone)
  {
	  if (!incomeMsgBuffer.empty())
	  {
		  memcpy((unsigned char*)&imessage,(unsigned char*)(&incomeMsgBuffer.front()),sizeof(tCavtkMsg));
		  if (toDelete) incomeMsgBuffer.pop();
	  }
  }
	 else
	 {
		crash("messageSeize: income message (type = %d, code = %d, flag = %d) did not be used",imessage.type,imessage.code,imessage.flag);
		messageClear();

	 }
}
//-------------------------------------
BOOL cavtk::getBlokirState(void)
{
CMODE mode;
	mode = pStatus->getMode(FALSE);
	return (!(mode == minit));
}
//-------------------------------------
#define szlogstr   256
void cavtk::errorLOG(char *fmt, ...) {
	va_list ap;
	char *buf, *bufptr;

	if (errorLog_proc) {
		buf = (char*)malloc(szlogstr);
		if (buf) {
			memset(buf, 0, szlogstr);
			if (0)
			{
				sprintf(buf, "%d: ", GetTickCount_());
				bufptr = &buf[strlen(buf)];
			}
			  else bufptr = buf;

			va_start(ap, fmt);
			fmtstrcnvcpy(bufptr, szlogstr - strlen(buf) - 3, fmt, ap);
			va_end(ap);
			strcat(buf, "\n");
			EnterCriticalSection(&LogCr);
			errorLog_proc(buf);
			LeaveCriticalSection(&LogCr);
			free(buf);
		}
	}

}
//-------------------------------------------------------------------
void cavtk::LOG(char *fmt, ...) {
	va_list ap;
	char *buf, *bufptr;

	if ((debugLog_proc) || (ownLog)) {
		buf = (char*)malloc(szlogstr);
		if (buf) {
			memset(buf, 0, szlogstr);
#ifdef LOG_SHOW_INTERVAL
			{
			sprintf(buf, "%d: ", logTickMem.getDuration());
			bufptr = &buf[strlen(buf)];
            logTickMem.setTimer();
			}
#else
            bufptr = buf;
#endif

			va_start(ap, fmt);
			fmtstrcnvcpy(bufptr, szlogstr - strlen(buf) - 3, fmt, ap);
			va_end(ap);
			strcat(buf, "\n");
			EnterCriticalSection(&LogCr);
			if (debugLog_proc) debugLog_proc(buf);
															  if (ownLog) ownLog->addText(buf);
			LeaveCriticalSection(&LogCr);
			free(buf);
		}
	}

}
// --------------------------------------------------------------

#define CRASH_LOG_FILE_BASE "crash"
#ifdef CRASH_LOG
void cavtk::crash(char *fmt, ...)
{
LogFileO *crashLog;
va_list ap;
char *buf, *bufptr;
int i;

	EnterCriticalSection(&crashLogCr);
	crashLog = new LogFileO;
	if (crashLog)
	{
		if (crashLog->init(CRASH_LOG_FILE_BASE) == 0)
		{
		   buf = (char*)malloc(szlogstr);
		   if (buf)
		   {
			   memset(buf, 0, szlogstr);
			if (0)
			{
			sprintf(buf, "%d: ", GetTickCount_());
			bufptr = &buf[strlen(buf)];
			}
			  else bufptr = buf;

			   va_start(ap, fmt);
			   fmtstrcnvcpy(bufptr, szlogstr - strlen(buf) - 3, fmt, ap);
			   va_end(ap);
			   crashLog->addText(buf);
			   LOG("Сформирован файл %s",crashLog->getFileName()); // почему-то зависает на TFORM->Add()
			   free(buf);
		   }
		  }
	   delete crashLog;
	  }
   LeaveCriticalSection(&crashLogCr);
}
#else
#define crash() while(0)
#endif
// --------------------------------------------------------------
void cavtk::motorLog(char * buf)
{
    if (debugLog_proc) debugLog_proc(buf);
    if (ownLog) ownLog->addText(buf);
}
// --------------------------------------------------------------
const char kZAHVATtext[] = {"Захват"};
const char kVLEVOtext[] = {"Влево"};
const char kVPRAVOtext[] = {"Вправо"};
const char kPUSKtext[] = {"Пуск"};
const char kRELStext[] = {"Рельс"};
char errorKeyText[64];

//
char* cavtk::getKeyName(unsigned char keyNum) {
	switch(keyNum) {
	case k_zahvat_id:
		return (char*)kZAHVATtext;
	case k_lev_id:
		return (char*)kVLEVOtext;
	case k_prav_id:
		return (char*)kVPRAVOtext;
	case k_pusk_id:
		return (char*)kPUSKtext;
	case k_rels_id:
		return (char*)kRELStext;
	default:
		sprintf(errorKeyText, "Неизвестный код кнопки - %d",keyNum);
		return errorKeyText;

	}
}
// --------------------------------------------------------------
const char MSTARTTEXT[] = {"Start"};
const char MINIT0TEXT[] = {"Init0"};
const char MINITTEXT[] = {"Исходный"};
const char MPOISKTEXT[] = {"Поиск"};
const char MRAILTEXT[] = {"Тест"};
const char MTUNETEXT[] = {"Настройка"};
const char MINITREQTEXT[] = {"Ожидание перехода в Исходный"};
const char MPOISKREQTEXT[] = {"Ожидание перехода в Поиск"};
const char MRAILREQTEXT[] = {"Ожидание перехода в Тест"};
const char MTUNEREQTEXT[] = {"Ожидание перехода в Настройка"};
const char MWRONGMODETEXT[] = {"Недопустимый код режима"};
const char MWRONGSTATETEXT[] = {"Недопустимый код состояния"};
const char MWRONGMODEANDSTATETEXT[] = {"Недопустимое сочетания режима и состояния"};
const char MUSRSTOPPEDTEXT[] = {"Остановлено пользователем"};
const char MHANDTEXT[] = {"Ручной"};
const char MHANDREQTEXT[] = {"Ожидание перехода в Ручной"};



char errorModeText[64];

//
char* cavtk::getModeName(enum CMODE  num)
{
char errorText[64];
	switch(num)
	{
	case mstart:
		return (char*)MSTARTTEXT;
	case minit0:
		return (char*)MINIT0TEXT;
	case minit:
		return (char*)MINITTEXT;
	case mpoisk:
		return (char*)MPOISKTEXT;
	case mrail:
		return (char*)MRAILTEXT;
	case mtune:
		return (char*)MTUNETEXT;
	case minitreq:
		return (char*)MINITREQTEXT;
	case mpoiskreq:
		return (char*)MPOISKREQTEXT;
	case mrailreq:
		return (char*)MRAILREQTEXT;
	case mtunereq:
		return (char*)MTUNEREQTEXT;
	case mstoppedwm:
		return (char*)MWRONGMODETEXT;
	case mstoppedwms:
		return (char*)MWRONGMODEANDSTATETEXT;
	case musrstopped:
		return (char*)MUSRSTOPPEDTEXT;
	case mstoppedws:
		return (char*)MWRONGSTATETEXT;
	case mhand:
		return (char*)MHANDTEXT;
	case mhandreq:
		return (char*)MHANDREQTEXT;

	default:
		sprintf(errorModeText, "код режима = (%d)",num);
		return errorModeText;
   }
}
// --------------------------------------------------------------

const char s0TEXT[] = {"S0"};
const char srailuTEXT[] = {"Поднятие рельса"};
const char srailu2TEXT[] = {"Поднятие рельса-2"};
const char srailTEXT[] = {"Рельс поднят"};
const char sraildTEXT[] = {"Опускание рельса"};
const char sraild2TEXT[] = {"Опускание рельса-2"};
const char sseize0TEXT[] = {"Сведение кареток"};
const char sseizeTEXT[] = {"Каретки сведены"};
const char sleave0TEXT[] = {"Разведение кареток"};
const char sscanTEXT[] = {"Сканирование"};
const char aj1TEXT[] = {"Юстировка1"};
const char aj2TEXT[] = {"Юстировка2"};
const char aj3TEXT[] = {"Юстировка3"};
const char aj1e1TEXT[] = {"Окончание Юстировка1"};
const char aj2e1TEXT[] = {"Окончание Юстировка2"};
const char scrTEXT[] = {"Возврат кареток"};
const char brkstate2TEXT[] = {"Останов движения"};
const char motorRestore1TEXT[] = {"Переинициализация моторов 1"};
const char motorRestore2TEXT[] = {"Переинициализация моторов 2"};
const char motorRestore3TEXT[] = {"Переинициализация моторов 3"};
const char motorRestore4TEXT[] = {"Переинициализация моторов 4"};
const char stuneTEXT[] = {"Настройка"};
const char stuneWaitEndMotionTEXT[] = {"stuneWaitEndMotion"};
const char unsteady1TEXT[] = {"unsteady1"};
const char unsteady2TEXT[] = {"unsteady2"};
const char kraRestore1TEXT[] = {"kraRestore1"};
const char kraRestore2TEXT[] = {"kraRestore1"};
const char sinit0TEXT[] = {"sinit0"};
const char sinit1TEXT[] = {"sinit1"};;
const char siniterrorTEXT[] = {"Ошибка инициализации в imit0"};

const char spwronTEXT[] = {"Включение питания моторов"};
const char spwroffTEXT[] = {"Отключение питания моторов"};

const char spredtuneTEXT[] = {"spredtune"};
const char sposttune1TEXT[] = {"sposttune1"};
const char sposttune2TEXT[] = {"sposttune2"};
const char spredscanTEXT[] = {"spredscan"};
const char spostscan1TEXT[] = {"spostscan1"};
const char spostscan2TEXT[] = {"spostscan2"};
const char s0iTEXT[] = {"S0I"};
const char s0eTEXT[] = {"S0E"};
const char s0BLOFFTEXT[] = {"S0BLOFF"};
const char s0FBLOFFTEXT[] = {"S0FBLOFF"};
const char sleaveeTEXT[] = {"Положение кареток неизвестно"};
const char sraildeTEXT[] = {"Положение рельса неизвестно"};
const char srailde2TEXT[] = {"Положение рельса неизвестно-2"};
const char sraileTEXT[] = {"sraile"};
const char s0BLONTEXT[] = {"S0BLON"};

char errorStateText[64];

char* cavtk::getStateName(enum CSTATE  num)
{
char errorText[64];
	switch(num)
	{
	case s0:      return (char*)s0TEXT;
	case srailu: return (char*)srailuTEXT;
	case srailu2: return (char*)srailu2TEXT;
	case srail:   return (char*)srailTEXT;
	case sraild: return (char*)sraildTEXT;
	case sraild2: return (char*)sraild2TEXT;
	case sseize0: return (char*)sseize0TEXT;
	case sseize: return (char*)sseizeTEXT;
	case sleave0: return (char*)sleave0TEXT;
	case sscan: return (char*)sscanTEXT;
	case aj1: return (char*)aj1TEXT;
	case aj2:  return (char*)aj2TEXT;
	case aj3:  return (char*)aj3TEXT;
	case aj1e1: return (char*)aj1e1TEXT;
	case aj2e1: return (char*)aj2e1TEXT;
	case scr:  return (char*)scrTEXT;
	case brkstate2:  return (char*)brkstate2TEXT;
	case motorRestore1: return (char*)motorRestore1TEXT;
	case motorRestore2: return (char*)motorRestore2TEXT;
	case motorRestore3: return (char*)motorRestore3TEXT;
	case motorRestore4: return (char*)motorRestore4TEXT;
	case stune:    return (char*)stuneTEXT;
	case stuneWaitEndMotion: return (char*)stuneWaitEndMotionTEXT;
	case unsteady1:       return (char*)unsteady1TEXT;
	case unsteady2:       return (char*)unsteady2TEXT;
	case kraRestore1:    return (char*)kraRestore1TEXT;
	case kraRestore2:    return (char*)kraRestore2TEXT;
	case sinit0:               return (char*)sinit0TEXT;
	case sinit1:               return (char*)sinit1TEXT;
	case siniterror:         return (char*)siniterrorTEXT;
	case spwron:         return (char*)spwronTEXT;
	case spwroff:         return (char*)spwroffTEXT;
	case spredtune: return (char*)spredtuneTEXT;
	case sposttune1: return (char*)sposttune1TEXT;
	case sposttune2: return (char*)sposttune2TEXT;
	case spredscan: return (char*)spredscanTEXT;
	case spostscan1: return (char*)spostscan1TEXT;
	case spostscan2: return (char*)spostscan2TEXT;
//
	case s0i: return (char*)s0iTEXT;
	case s0E: return (char*)s0eTEXT;
	case s0BLOFF: return (char*)s0BLOFFTEXT;
	case s0FBLOFF: return (char*)s0FBLOFFTEXT;
	case sleavee: return (char*)sleaveeTEXT;
	case srailde: return (char*)sraildeTEXT;
	case srailde2: return (char*)srailde2TEXT;
	case sraile:   return (char*)sraileTEXT;
	case s0BLON: return (char*)s0BLONTEXT;


	default:
        sprintf(errorStateText, "код состояния = (%d)",num);
		return errorKeyText;
   }
}
//----------------------------------------------------------------------------------------------------------------------------------
void cSTATUS::getStatus(STATUS *pStatus)
{
    pStatus->mode = (status[index]).current.mode;
    pStatus->state = (status[index]).current.state;
}
//----------------------------------------------------------------------------------------------------------------------------------
void cSTATUS::setStatus(STATUS *pStatus)
{
    (status[index]).current.mode = pStatus->mode;
    (status[index]).current.state = pStatus->state;
}
//----------------------------------------------------------------------------------------------------------------------------------
void cSTATUS::storeStatus(void)
{
    (status[index]).stored.mode = (status[index]).current.mode;
    (status[index]).stored.state = (status[index]).current.state;
}
//----------------------------------------------------------------------------------------------------------------------------------
void cSTATUS::init(STATUS s)
{
	setStatus(&s);
    if (s.state & NONSTOREDSTATE)
    {
        (status[index]).stored.mode = unknownmode;
        (status[index]).stored.state = unknownstate;
    }
	   else storeStatus();
}
//----------------------------------------------------------------------------------------------------------------------------------
void cSTATUS::toSetStatus(STATUS *pStatus)
{
    if(!((status[index]).current.state & NONSTOREDSTATE))  storeStatus();
    setStatus(pStatus);
}
//----------------------------------------------------------------------------------------------------------------------------------
//cSTATUS::cSTATUS(STATUS s,LOGPROC _logproc,LOGPROC crashproc)
cSTATUS::cSTATUS(STATUS s)
{
	InitializeCriticalSection(&statusLockCr);
	lockCount = 0;
	index = 0;

	status.resize(index+1);
	predStatus.mode = unknownmode;
	predStatus.state = unknownstate;
                     init(s);
//    logProc = _logproc;
//	crashProc = crashproc;
	logProc = NULL;
	crashProc = NULL;
}
//----------------------------------------------------------------------------------------------------------------------------------
cSTATUS::~cSTATUS()
{
	DeleteCriticalSection(&statusLockCr);
	status.clear();
}
//----------------------------------------------------------------------------------------------------------------------------------
void cSTATUS::lock(void)
{
int i;
		EnterCriticalSection(&statusLockCr);
		if (lockCount != 0)
		{
		  i++;
		}
		lockCount++;
}
//----------------------------------------------------------------------------------------------------------------------------------
void cSTATUS::unlock(void)
{
int i;
		if (lockCount != 1)
		{
		  i++;
		}
		lockCount--;
		LeaveCriticalSection(&statusLockCr);
}
//----------------------------------------------------------------------------------------------------------------------------------
void cSTATUS::pushStatus(STATUS s,BOOL locked)
{
	if (locked == false) lock();
	predStatus.mode = (status[index]).current.mode;
	predStatus.state = status[index].current.state;
	index++;
	status.resize(index+1);
                     init(s);
	if (locked == false) unlock();
}
//----------------------------------------------------------------------------------------------------------------------------------
void cSTATUS::pushStatus(CSTATE state,BOOL locked)
{
STATUS s;

	if (locked == false) lock();

	s.mode = (status[index]).current.mode;
	s.state = state;
	pushStatus(s,true);

//     if (logProc) logProc("pushStatus: current = %s:%s ",getModeName(s.mode),getStateName(s.state));
//
	if (locked == false) unlock();
}
//----------------------------------------------------------------------------------------------------------------------------------
STATUS cSTATUS::popStatus(BOOL locked)
{
STATUS res;
	if (locked == false) lock();
	predStatus.mode = (status[index]).current.mode;
	predStatus.state = (status[index]).current.state;
	if (status.size() > 1)
	{
	   status.resize(index);
		index--;
	}
	 else
	 { // сообщение ошибка выгребать некуда
		if (crashProc) crashProc("popStatus: перед вызовом размер вектора уже был равен 1");
	 }
	getStatus(&res);

//	if (logProc) logProc("popStatus: current = %s:%s ",getModeName(res.mode),getStateName(res.state));

	if (locked == false) unlock();
	return res;
}
//----------------------------------------------------------------------------------------------------------------------------------
CSTATE cSTATUS::getState(BOOL locked)
{
STATUS res;
	if (locked == false) lock();
	getStatus(&res);
	if (locked == false) unlock();
	return res.state;
}
//----------------------------------------------------------------------------------------------------------------------------------
CMODE cSTATUS::getMode(BOOL locked)
{
STATUS res;
    if (locked == false) lock();
    getStatus(&res);
    if (locked == false) unlock();
    return res.mode;
}

//----------------------------------------------------------------------------------------------------------------------------------
void cSTATUS::setState(CSTATE state,BOOL locked)
{
STATUS s;
	if (locked == false) lock();
	predStatus.state = (status[index]).current.state;
	predStatus.mode = (status[index]).current.mode;
	getStatus(&s);
	s.state = state;
	toSetStatus(&s);
	if (locked == false) unlock();
}
//----------------------------------------------------------------------------------------------------------------------------------
// не изменяет поля "stored"
void cSTATUS::setMode(CMODE mode,BOOL locked)
{
STATUS s;
	if (locked == false) lock();
	predStatus.state = (status[index]).current.state;
	predStatus.mode = (status[index]).current.mode;
	getStatus(&s);
	s.mode = mode;
	setStatus(&s);
	if (locked == false) unlock();
}
//----------------------------------------------------------------------------------------------------------------------------------
void cSTATUS::setStatus(CMODE mode,CSTATE state,BOOL locked)
{
STATUS s;
	if (locked == false) lock();
	predStatus.state = (status[index]).current.state;
	predStatus.mode = (status[index]).current.mode;
	s.mode = mode;
	s.state = state;
	toSetStatus(&s);
	if (locked == false) unlock();
}
//----------------------------------------------------------------------------------------------------------------------------------
// предпочтительнее вызывать, когда доступ заблокирован извне
STATUS cSTATUS::getPredStatus(void)
{
	return predStatus;
}
//----------------------------------------------------------------------------------------------------------------------------------
STATUS cSTATUS::getStoredStatus(BOOL locked)
{
STATUS res;
    if (locked == false) lock();
    res.state = (status[index]).stored.state;
    res.mode = (status[index]).stored.mode;
    if (locked == false) unlock();
    return res;    
}


