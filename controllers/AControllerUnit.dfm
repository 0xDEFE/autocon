object AControllerForm: TAControllerForm
  Left = 0
  Top = 0
  Caption = 'AControllerForm'
  ClientHeight = 520
  ClientWidth = 609
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 105
    Width = 609
    Height = 415
    Align = alClient
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 609
    Height = 105
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 112
      Top = 13
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object Label2: TLabel
      Left = 112
      Top = 44
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object Button1: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Create'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 16
      Top = 39
      Width = 75
      Height = 25
      Caption = 'Init'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 16
      Top = 70
      Width = 75
      Height = 25
      Caption = 'Destroy'
      TabOrder = 2
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 375
      Top = 8
      Width = 75
      Height = 25
      Caption = #1048#1089#1093#1086#1076#1085#1099#1081
      TabOrder = 3
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 375
      Top = 39
      Width = 75
      Height = 25
      Caption = #1055#1086#1080#1089#1082
      TabOrder = 4
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 375
      Top = 70
      Width = 75
      Height = 25
      Caption = #1058#1077#1089#1090
      TabOrder = 5
      OnClick = Button6Click
    end
    object Button7: TButton
      Left = 464
      Top = 8
      Width = 75
      Height = 25
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      TabOrder = 6
      OnClick = Button7Click
    end
    object Button8: TButton
      Left = 464
      Top = 39
      Width = 75
      Height = 25
      Caption = #1070#1089#1090#1080#1088#1086#1074#1082#1072' 1'
      TabOrder = 7
      OnClick = Button8Click
    end
    object Button9: TButton
      Left = 464
      Top = 69
      Width = 75
      Height = 26
      Caption = #1070#1089#1090#1080#1088#1086#1074#1082#1072' 2'
      TabOrder = 8
      OnClick = Button9Click
    end
  end
  object Timer1: TTimer
    Interval = 100
    OnTimer = Timer1Timer
    Left = 224
    Top = 24
  end
end
