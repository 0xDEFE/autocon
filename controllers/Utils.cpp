
//#include "stdafx.h"
#include "Utils.h"


//Added by KirillB
sSimulationData SimData;
sSimulationData* GetSimData()
{
	return &SimData;
}
// ---  ������ ----------------------------------------------------------------------

unsigned long GetTickCount_(void) // ����� � ������ ������ ������� � ��
{
  #if defined(WIN_BUILDER)
	return GetTickCount();
  #endif

  #if defined(WIN_VS)
	return GetTickCount();
  #endif

  #if defined(LINUX_)
	struct timeval tv;
	gettimeofday(&tv,NULL);
	return (tv.tv_sec*1000+tv.tv_usec/1000);
  #endif
}

// ---  Log ���� --------------------------------------------------------------------

extern LogFileObj * Log = NULL;          // ��������� �� ������ LogFile

LogFileObj::LogFileObj(void * Owner_)
{
	Owner = Owner_;
	LogFile = fopen("debug.log", "w");
}

LogFileObj::~LogFileObj(void)
{
	fclose(LogFile);
}

void LogFileObj::AddText(char * str)
{
	fputs(str, LogFile);
	fputs("\n", LogFile);
}

void LogFileObj::AddIntParam(char * str, int Par)
{
//	fputs(str, LogFile);
	sprintf(logstr, str, Par);
	fputs(logstr, LogFile);
	fputs("\n", LogFile);
}

void LogFileObj::AddTwoIntParam(char * str, int Par1, int Par2)
{
	//fputs(str, LogFile);
	sprintf(logstr, str, Par1, Par2);
	fputs(logstr, LogFile);
	fputs("\n", LogFile);
}

void LogFileObj::AddThreeIntParam(char * str, int Par1, int Par2, int Par3)
{
	sprintf(logstr, str, Par1, Par2, Par3);
	fputs(logstr, LogFile);
	fputs("\n", LogFile);
}

void LogFileObj::AddFourIntParam(char * str, int Par1, int Par2, int Par3, int Par4)
{
	sprintf(logstr, str, Par1, Par2, Par3, Par4);
	fputs(logstr, LogFile);
	fputs("\n", LogFile);
}

void LogFileObj::Add5IntParam(char * str, int Par1, int Par2, int Par3, int Par4, int Par5)
{
	sprintf(logstr, str, Par1, Par2, Par3, Par4, Par5);
	fputs(logstr, LogFile);
	fputs("\n", LogFile);
}

void LogFileObj::AddCANBlock(char * str, int Par, CANBlock blk)
{
	sprintf(logstr, str, Par);
	fputs(logstr, LogFile);

	switch (blk[1] & 0x0F)
	{
		case 0: { sprintf(logstr, "%2x %2x", blk[0], blk[1]); break; }
		case 1: { sprintf(logstr, "%2x %2x %2x", blk[0], blk[1], blk[2]); break; }
		case 2: { sprintf(logstr, "%2x %2x %2x %2x", blk[0], blk[1], blk[2], blk[3]); break; }
		case 3: { sprintf(logstr, "%2x %2x %2x %2x %2x", blk[0], blk[1], blk[2], blk[3], blk[4]); break; }
		case 4: { sprintf(logstr, "%2x %2x %2x %2x %2x %2x", blk[0], blk[1], blk[2], blk[3], blk[4], blk[5]); break; }
		case 5: { sprintf(logstr, "%2x %2x %2x %2x %2x %2x %2x", blk[0], blk[1], blk[2], blk[3], blk[4], blk[5], blk[6]); break; }
		case 6: { sprintf(logstr, "%2x %2x %2x %2x %2x %2x %2x %2x", blk[0], blk[1], blk[2], blk[3], blk[4], blk[5], blk[6], blk[7]); break; }
		case 7: { sprintf(logstr, "%2x %2x %2x %2x %2x %2x %2x %2x %2x", blk[0], blk[1], blk[2], blk[3], blk[4], blk[5], blk[6], blk[7], blk[8]); break; }
		case 8: { sprintf(logstr, "%2x %2x %2x %2x %2x %2x %2x %2x %2x %2x", blk[0], blk[1], blk[2], blk[3], blk[4], blk[5], blk[6], blk[7], blk[8], blk[9]); break; }
	}
	fputs(logstr, LogFile);
	fputs("\n", LogFile);
}

