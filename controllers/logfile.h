﻿// КОДИРОВКА UTF-8 (без BOM)---------------------------------------------------------------------------


#ifndef _logfileh
#define _logfileh

#include "windows.h"
#include <time.h>
#include <stdio.h>


class LogFileO
{
private:
    static const unsigned int fileNameMaxLng = 512;
	static const unsigned int  fileExtLng = 5;

	char fileName[fileNameMaxLng+fileExtLng];
	unsigned int accessCounter;


public:
	LogFileO(void);
	~LogFileO(void);
	init(char * fileNameBase);
	void addText(char *);
	char *getFileName(void);


};


#endif


