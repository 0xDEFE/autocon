﻿// КОДИРОВКА UTF-8 (без BOM)---------------------------------------------------------------------------


#ifndef _errstgrh
#define _errstgrh


const char stre_initsensorqry[] = {"Начальное состояние датчиков определить не удалось - остаемся в init0"};
const char stre_kv1off[] = {"При сведении кареток kv1 остался незамкнутым"};
const char stre_kv2off[] = {"При сведении кареток kv2 остался незамкнутым"};
const char stre_kvg1off[] = {"КВГ1 остался незамкнутым"};
const char stre_kvg2off[] = {"КВГ2 остался незамкнутым"};
const char stre_kvg3off[] = {"КВГ3 остался незамкнутым"};
const char stre_kvg4off[] = {"КВГ4 остался незамкнутым"};
//

const char stre_kvg1on[] = {"КВГ1 остался замкнутым"};
const char stre_kvg2on[] = {"КВГ2 остался замкнутым"};
const char stre_kvg3on[] = {"КВГ3 остался замкнутым"};
const char stre_kvg4on[] = {"КВГ4 остался замкнутым"};
//

const char stre_dip12[] = {"Перекос по датчикам перемещения 1,2"};
const char stre_dip34[] = {"Перекос по датчикам перемещения 3,4"};
const char stre_farfromstartpos[] = {"При разведении каретки не достигли крайнего положения"};
//
const char stre_id1off[] = {"Индуктивный датчик 1 остался незамкнутым - нет рельса в потоке"};
const char stre_id2off[] = {"Индуктивный датчик 2 остался незамкнутым - нет рельса в потоке"};
const char stre_id1on[] = {"Индуктивный датчик 1 замкнут - рельс в потоке. Контроль запрещен."};
const char stre_id2on[] = {"Индуктивный датчик 2 замкнут - рельс в потоке. Контроль запрещен."};
//
const char stre_ddoff[] = {"Датчик давления разомкнут"};
const char stre_rdoff[] = {"Реле давления разомкнуто"};

const char stre_outofcenter[] = {"Неправильно определен центр стыка, либо стык находится за пределами допустимого положения"};

const char stre_caretReturn[] = {"Невозможно вернуть каретки в исх. положение"};
	
const char stre_drvnotready[] = {"Привод не готов"};	

const char stre_cmdCancell[] = {"Ошибка при попытке отмены команды движения"};

const char stre_drvaborted[] = {"Работа с приводом аварийно завершена"};
const char stre_drvpwron[] = {"Ошибка при подаче рабочего напряжения на привод"};
const char stre_drvpwroff [] = {"Ошибка снятия рабочего напряжения с привода"};

const char stre_keyPressedTooRapidly[] = {"Кнопки пульта нажимаются слишком быстро"};
const char stre_keyDisabled[] = {"Действие недоступно"};
const char stre_tcprevive[] ={"Остановлено из-за невозможности восстановить TCP соединение с контроллером"};
const char stre_inituncluspedkv[] = {"КВ1,КВ2 не разомкнуты - остаемся в init0"};
const char stre_inituncluspeda[] = {"Клещи недостаточно разведены  - остаемся в init0"};
const char stre_kv1on[] = {"Kv1 остался замкнутым"};
const char stre_kv2on[] = {"Kv2 остался замкнутым"};

// 
const char stre_caretunknown[] = {"Положение кареток неизвестно"}; // отклоняя код ошибки, пользователь подтверждает разведенное положение кареток
const char stre_railunknown[] = {"Положение рельса неизвестно"};  // отклоняя код ошибки, пользователь подтверждает опущенное положение рельса




const char stre_wrongmode[] = {"Остановлено из-за  недопустимого значения режима"};
const char stre_wrongmodeandstate[] = {"Остановлено из-за  недопустимого сочетания режима и состояния"};
const char stre_wrongstate[] = {"Остановлено из-за  недопустимого значения состояния"};
const char stre_wrongact[] = {"Остановлено из-за  подачи недопустимой команды для контроллера"};
//
const char stre_softwarever[] ={"Остановлено из-за невозможности определить версию ПО контроллера"};
const char stre_softwareres[] ={"Остановлено. Контроллер был выключен и включен снова"};
//
const char stre_usrstopped[] ={"Остановлено пользователем"};


// ошибки из tOutputpinError kradef.h
const char stre_clusped[] = {"Каретки сведены - опускание тестового рельса невозможно"};
const char stre_railOrClusped[] = {"Рельс в потоке или каретки сведены - поднятие тестового рельса невозможно"};
const char stre_noRailAndTestRailNotUp[] = {"нет рельса в потоке и рельс не поднят каретки сведены - сведение кареток невозможно"};
const char stre_cluspedOrTestRailUp[] = {"каретки сведены или тестовый рельс поднят- отключение блокировки таскателя невозможно"};


#endif


