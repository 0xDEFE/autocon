#ifndef _landefsh
#define _landefsh

typedef void __fastcall (__closure *DataReceived)(int UMUIdx, void*, int);

const int LANBufferSize = 1024;
const int LanDataMaxSize = 1019;

typedef struct
{
	unsigned char Id;
	unsigned char Source;
	unsigned short Size;
	unsigned char Data[LanDataMaxSize];
} tLAN_Message;

typedef struct
{
	unsigned short Size;
	tLAN_Message Msg;
} tLAN_MessageObj;

#endif

