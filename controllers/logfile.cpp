﻿// КОДИРОВКА UTF-8 (без BOM)---------------------------------------------------------------------------

#include <algorithm>
#include <fstream>
#include "logfile.h"
#include "stdlib.h"
#include "assert.h"



static const char CRLF[] = {13,10,0};
static const char fileExt[] = {".log"};

LogFileO::LogFileO(void)
{
    fileName[0] = 0;
    accessCounter = 0;
}

//-----------------------------------------------------------------------------------------------------------
LogFileO::~LogFileO(void)
{
}
//-----------------------------------------------------------------------------------------------------------
// -1 - не задано имя файла
int LogFileO::init(char * fileNameBase)
{
int res = -1;
int len;
SYSTEMTIME st;
char buf[fileNameMaxLng/2];
char fullName[fileNameMaxLng+fileExtLng];
std::FILE* LogFile;
BOOL ftooLongName = false;

					 accessCounter = 0;
	if ((fileNameBase == NULL) || (strlen(fileNameBase) == 0)) return res;
	len = std::min(strlen(fileNameBase)+1,fileNameMaxLng/2+1);
	strncpy(fileName,fileNameBase,len);
	fileName[len] = 0;
//
	GetSystemTime(&st);       // gets current time
	if (sprintf(buf,"_%2d%2d%2d__%2d%2d%2d_%3d",st.wDay,st.wMonth,st.wYear - st.wYear/1000*1000,st.wHour,st.wMinute,st.wSecond,st.wMilliseconds))
	{
		strcat(fileName,buf);
		 do
		 {
			strcpy(fullName,fileName);
			strcat(fullName,fileExt);
			LogFile = fopen(fullName,const_cast<char *>("rb"));
			if (LogFile == NULL) break;
			fclose(LogFile);
			if (strlen(fileName) < fileNameMaxLng-3) strcat(fileName,"_");
				else
				{
				   ftooLongName = true;
				   break;
				}
		 }  while((LogFile != NULL) && (!ftooLongName));
		 if (LogFile == NULL)
		 {
		  strcat(fileName,fileExt);
		  res = 0;
		 }
	}
	  else fileName[0] = 0;
	return res;
}
//-----------------------------------------------------------------------------------------------------------

void LogFileO::addText(char *s)
{
int i;
std::FILE* LogFile;

	if (strlen(fileName) != 0)
	{
		if (accessCounter == 0) LogFile = fopen(fileName,const_cast<char *>("wb"));
		 else LogFile = fopen(fileName,const_cast<char *>("ab"));
		accessCounter++;
		assert(LogFile != NULL);
		if (LogFile)
		{
			fwrite(s, strlen(s),1,LogFile);
			fwrite(CRLF, strlen(CRLF),1,LogFile);
			fclose(LogFile);
		}
	}
}
//-----------------------------------------------------------------------------------------------------------
char *LogFileO::getFileName(void)
{
	return fileName;
}
