﻿



#include "mytmr.h"
//
#define TRUE 1
#define FALSE 0
//

void TMR::setTimer(void)
{
	setTimer(&t_start);
}
//-------------------------------------------------------------------
void TMR::setTimer(unsigned long *ptmr)
{
   if (ptmr) *ptmr = GetTickCount_();
}
//-------------------------------------------------------------------
// возвращает длительность интервала в миллисекундах прошедшего с
// момента mstart
unsigned long TMR::getDuration(void)
{
register int ii;
unsigned long mscurrent;
	  mscurrent = GetTickCount_();
	  ii = mscurrent - t_start;
	  return (ii >=0) ? ii:(0-ii);
}

unsigned long TMR::getDuration(unsigned long *pt)
{
	register long ii;
	unsigned long mscurrent;
	mscurrent = GetTickCount_();
	ii = mscurrent - *pt;
	return(unsigned long)((ii >= 0) ? ii : (0 - ii));
}

