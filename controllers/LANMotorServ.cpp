
#include "LANMotorServ.h"
#include <math.h>
#include <windows.h>


//#ifdef LINUX_
//#include <unistd.h>
#include <string.h>
//#endif


namespace LANProtDebugMS 
{

	Add_Log onAddLog;
	Add_Log2 onAddLog2 = NULL;
	Add_Log3 onAddLog3 = NULL;

#ifdef DbgLog
	BOOL useLog = false;
#endif


#ifdef Dbg

                     unsigned int rcvbytescnt;
#ifdef LINUX_  

#else
	CRITICAL_SECTION cr;
#endif
//


void addbytcnt(unsigned int val) 
{
			EnterCriticalSection(&cr);
			rcvbytescnt += val;
			LeaveCriticalSection(&cr);

}
//
unsigned int rdbytcnt(void)
{
unsigned int res;
			EnterCriticalSection(&cr);
			res = rcvbytescnt;
			rcvbytescnt = 0;
            LeaveCriticalSection(&cr);
            return res;	
         
}
//
void initbytcnt(void) 
{
	  InitializeCriticalSection(&cr);
	  rcvbytescnt = 0;
}
//
void deinitbytcnt(void)
{
	  DeleteCriticalSection(&cr);
}
#endif

//
}

using namespace LANProtDebugMS;

//---------------------------------------------------------------------------
LANMOTORSERVICE::LANMOTORSERVICE(int UMUIdx_, cDataTransfer* hrd_)
{

	UMUIdx = UMUIdx_;
	hrd = hrd_;
	RState = rcvOff;
	OutStIdx = 0;
	OutEdIdx = 0;


	OutBlock = new tLAN_DRVMessageObj;
	memset(OutBlock,0,sizeof(tLAN_DRVMessageObj));

	SendTickT = GetTickCount_();

	OnDataReceived= NULL;

	#ifdef UMULog                           //
	if (!Log) Log = new LogFileObj(this);
	Log->AddIntParam("Create Object UMU:%d", UMUIdx);
	#endif

#ifdef Dbg
                    initbytcnt(); 
#endif

}
//---------------------------------------------------------------------------
LANMOTORSERVICE::~LANMOTORSERVICE(void)
{
	delete OutBlock;




//	#ifdef BUMLog                           // 
	#ifdef UMULog                           // 
	Log->AddIntParam("Delete Object UMU:%d", UMUIdx);
//	if ((!Log) && (Log->Owner == this)) delete Log;
	if ((Log!=NULL) && (Log->Owner == this)) delete Log;
	#endif

#ifdef Dbg
                    deinitbytcnt(); 
#endif

}
//---------------------------------------------------------------------------
void LANMOTORSERVICE::StartWork(void)
{
	RState = rcvOn;
}
//---------------------------------------------------------------------------
void LANMOTORSERVICE::EndWork(void)
{
	RState = rcvOff;
}
//---------------------------------------------------------------------------
void LANMOTORSERVICE::AddToOutBuff(void) //
{
	tLAN_DRVMessageObj tmp;
                     memcpy(&tmp,OutBlock,sizeof(tmp));
	OutBuffer.push(tmp);
	memset(OutBlock, 0, sizeof(tLAN_DRVMessageObj));
}
//---------------------------------------------------------------------------
void LANMOTORSERVICE::Unload(void)
{

	int f = OutBuffer.size();
	while (!OutBuffer.empty())
	{
		if (hrd->WriteBuffer(UMUIdx, (unsigned char *)&OutBuffer.front().Msg, OutBuffer.front().Size))
		{
			#ifdef DbgLog
				if (useLog)
				  if (onAddLog)	onAddLog((unsigned char *)&OutBuffer.front().Msg,OutBuffer.front().Size,0);
			#endif

		}
		else
		{
			unsigned char i;
			i++;
			#ifdef UMUOutDataLog
			Log->AddText(" - WriteToLAN - Error");
			#endif
		}
		OutBuffer.pop();
	}
//    OutBuffer.clear();

}
//---------------------------------------------------------------------------
void LANMOTORSERVICE::TickSend(void)    
{
	Unload();
}
//---------------------------------------------------------------------------
void LANMOTORSERVICE::TickReceive(void) //
{
	unsigned short res;
	switch (RState)
	{
		case rcvOn:
		{
				res = hrd->ReadBuffer(UMUIdx,(unsigned char *)&LAN_message,1024);
				if (res)
				{

					if (OnDataReceived) OnDataReceived(UMUIdx,(unsigned char*)&LAN_message,res);


					#ifdef DbgLog
						if (useLog)
 							onAddLog((unsigned char *)&LAN_message,1,1);
					#endif
//
					#ifdef Dbg
					addbytcnt(1);
					#endif


				}
			break;

		}
		case rcvOff: break;
	}

}
//---------------------------------------------------------------------------
void LANMOTORSERVICE::Tick(void)
{
	if ((GetTickCount_() - SendTickT) >= 50)
	{
		this->TickSend();
		SendTickT = GetTickCount_();
	}

	try
	{
	this->TickReceive();
	}
	catch(std::exception)
	{;}
}
//---------------------------------------------------------------------------
void LANMOTORSERVICE::SetOutputData(unsigned char *buf,unsigned int bufsize)
{
unsigned int i;

	OutBlock->Size   = bufsize;
	for (i = 0; i < bufsize; i++)  OutBlock->Msg.Data[i] =  buf[i];
	AddToOutBuff();

}

//---------------------------------------------------------------------------
void LANMOTORSERVICE::setRcvProc( DataReceived proc)
{
   OnDataReceived = proc;
}

