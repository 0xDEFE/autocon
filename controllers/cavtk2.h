﻿// КОДИРОВКА UTF-8 (без BOM)---------------------------------------------------------------------------

#ifndef cavtk2h
#define cavtk2h

#include "cavtk.ini"
#include "LanProtAVTK.h"
#include "motorctrl2.h"
#include "logfile.h"

//
#define AUTO_RESET_EVENT   FALSE
#define MANUAL_RESET_EVENT TRUE
//
//typedef UINT32 DWORD;
typedef DWORD ab;

enum CMODE
{
	   mstart = 0,     // состояние после создания экземпляра
	   minit0 = 1,
	   minit  = 2,      // исходный режим
	   mpoisk = 3,    // режим ПОИСК УК
	   mrail = 4,        // режим ТЕСТ УК
	   madj1 = 5,      // соответствует режиму "Юстировка 1", но unitmode = rail;  значение возвращается в getmode, когда unitstate = aj1
	   madj2 = 6,      // соответствует режиму "Юстировка 2", но unitmode = rail;  значение возвращается в getmode, когда unitstate = aj2
	   madj3 = 7,      // соответствует режиму "Юстировка 3" , но unitmode = rail;  значение возвращается в getmode, когда unitstate = aj3
	   mcalibrate = 8, // соответствует режиму "Калибровка" , но unitmode = rail;  значение возвращается в getmode, когда unitstate = scalibrate
	   mtune = 9,    //  режим "Настройка"
	   mcancel = 10,  //  значение возвращается в getmode(), идет возврат кареток
//
	   minitreq = 11,  // идет переход в  ->minit по запросу от ::setmode()
	   mpoiskreq = 12, // идет переход в mpoisk по запросу от ::setmode()
	   mrailreq = 13, // то же, но в minit->mrail
	   mtunereq = 14, // то же, но в minit->mtune
       mhand = 15,   // режим РУЧНОЙ контроль при рельсе в потоке
	   mhandreq = 16,  // идет переход в  ->mhand по запросу от ::setmode()


//
	   unknownmode = -1,
	   mstoppedwm = -2, // остановлено из-за  e_wrongmode
	   mstoppedws = -3, // остановлено из-за  e_wrongstate
	   mstoppedwms = -4, // остановлено из-за  недопустимого сочетания mode & state -  e_wrongstatemode
	   mstoppedwa = -5,   // остановлено, т.к. ПО выдает команду недопустимую с точки зрения контроллера
	   musrstopped = -6  // остановлено пользователем

 };

#define SHORTPATH            0x40     // если флаг сброшен,  то переход в состояние идет через состояния unsteady1,2
#define NONSTOREDSTATE 0x80
enum CSTATE
{
	   s0          = 0 | SHORTPATH, // тестовый рельс опущен, каретки разведены,блокировка ВКЛЮЧЕНА
	   srailu     = 1,        // поднятие тестового рельса, каретки разведены
	   srail       = 2,        // тестовый рельс поднят, каретки разведены
	   sraild     = 3,        // опускание тестового рельса, каретки разведены
	   sseize0  = 4,        // процесс сведения кареток
	   sseize   = 5,        // каретки сведены
	   sleave0 = 6,        // процесс разведения кареток
	   sscan    = 7 | SHORTPATH,        // процесс сканирования рельса  в mpoisk или тестирования в mrail
	   aj1         =  8,        // юстировка 1
	   aj2         =  9,       // юстировка 2
	   aj3         =   10 | SHORTPATH,       // юстировка 3
	   aj1e1       =   11 | NONSTOREDSTATE | SHORTPATH,
	   aj2e1       =   12 | NONSTOREDSTATE | SHORTPATH, //  записывается команда перемещение нижней   и верхней   кареток в парковочное положение, далее переход в aj3
	   scr           =  13 | NONSTOREDSTATE | SHORTPATH,  //  записывается команда перемещение нижней   и верхней   кареток в парковочное положение
	   brkstate2    =  14 | NONSTOREDSTATE | SHORTPATH,  //  запись команды breakcmd для двигателей, далее переход в scr
	   motorRestore1 = 15 | NONSTOREDSTATE,
	   motorRestore2 = 16 | NONSTOREDSTATE  | SHORTPATH,
	   motorRestore3 = 17 | NONSTOREDSTATE  | SHORTPATH,
	   motorRestore4 = 18 | NONSTOREDSTATE  | SHORTPATH,
	   spwron      = 19 | NONSTOREDSTATE  | SHORTPATH, //  включение питания двигателей, далее переход в scr
	   spwroff      = 20 | NONSTOREDSTATE  | SHORTPATH, //  отключение питания двигателей, далее переход в s0 или srail

	   stune = 21 | SHORTPATH, // настройка - ожидаем команды на перемещение кареток, возможен переход в stuneWaitEndMotion, sseize
	   stuneWaitEndMotion = 22 | NONSTOREDSTATE,
	   unsteady1  = 23,
	   unsteady2 = 24,     // ожидание сопадения массивов actualOutputs[]  и targetOutputs[], по истечении тайм-аута -> (minit0,kraRestore1)

	   spredtune  = 25,       // только в режиме mtune
	   sposttune1 = 26 | SHORTPATH,        // только в режиме mtune
	   spredscan  = 27,      // только в режимах mrail,mpoisk
	   spostscan1  = 28 | SHORTPATH,  // только в режимах mrail,mpoisk - переход по отмене
	   spostscan2  = 29 | SHORTPATH,  // только в режимах mrail,mpoisk - переход по окончанию последнего движения
	   sposttune2 = 30 | SHORTPATH,        // только в режиме mtune
//
	   s0i        = 31 | SHORTPATH,   // соответствует режиму minit
	   s0E        = 32,
	   s0BLOFF    = 33,           // обычное отключение блокировки
	   s0FBLOFF    = 34,          // принудительное отключение блокировки
	   sleavee     = 35 | SHORTPATH, // разведение кареток закончилось с ошибкой - ожидаем ответа оператора
	   sraile     = 36, //
	   srailde     = 37 | SHORTPATH, // опускание рельса закончилось с ошибкой - ожидаем ответа оператора
	   srailde2    = 38 | SHORTPATH, // опускание рельса закончилось с ошибкой - ожидаем ответа оператора
	   sraild2     = 39,  // принудительное опускание тестового рельса, когда положение кареток осталось определенным,
						  // но они признаны оператором разведенными
	   srailu2     = 40,
	   s0BLON      = 41,  // обычное включение блокировки

//

// состояния режима minit0
	   kraRestore1 = 42,               // состояние в minit0 - разрыв связи с KRA
	   kraRestore2 = 43,                // состояние в minit0
	   sinit0            = 44,                // состояние в minit0 - состояние при переходе в этот режим после старта ПО
	   sinit01           = 45,               // состояние в minit0 - состояние при переходе в этот режим после старта ПО
	   sinit1            = 46,                // состояние в minit0 - анализ состояния датчиков и выходов контроллера после старта ПО
	   siniterror      = 47,
//
	   unknownstate = -1
};
//
//
#define a_keyLongPressedTime 500    // время до фиксации длительного нажатия кнопки
//

// ошибки при работе
enum CERROR
{
	e_initsensorqry=1,      // начальное состояние датчиков определить не удалось - остаемся в init0
	e_kv1off=2,                // при сведении кареток kv1 остался незамкнутым
	e_kv2off=3,                 // при сведении кареток kv2 остался незамкнутым
	e_kvg1off=4,                // kvg1 остался незамкнутым
	e_kvg2off=5,                // kvg2 остался незамкнутым
	e_kvg3off=6,                // kvg3 остался незамкнутым
	e_kvg4off=7,                // kvg4 остался незамкнутым
//
   e_kvg1on=8,                // kvg1 остался замкнутым
   e_kvg2on=9,                // kvg2 остался замкнутым
   e_kvg3on=10,                // kvg3 остался замкнутым
   e_kvg4on=11,                // kvg4 остался замкнутым
//
   e_dip12=12,                   // перекос по датчикам перемещения 1,2  - при этом каретки автоматически разведутся
   e_dip34=13,                   // перекос по датчикам перемещения 3,4  - при этом каретки автоматически разведутся
   e_farfromstartpos=14,    // при разведении каретки не достигли крайнего положения
//
   e_id1off=15, 	// индуктивный датчик 1 остался незамкнутым - нет рельса в потоке
   e_id2off=16,	// индуктивный датчик 2 остался незамкнутым - нет рельса
   e_id1on=17,	// индуктивный датчик 1 замкнут - рельс в потоке
   e_id2on=18,	// индуктивный датчик 2 замкнут - рельс в потоке
//
	e_ddoff=19,                  // датчик давления замкнут - низкое давление
	e_rdoff=20,                 // реле давления разомкнуто - ?

	e_outofcenter=21,       // при попытке ввода ПОИСК определено, что либо неправильно определен центр стыка, либо стык
//--						// находится за пределами допустимого положения
// ниже моторы
	e_caretReturn = 22,      // ошибка при возврате кареток в исходное положение
	e_drvnotready = 23,      // привод не готов
	e_cmdCancell  = 24,      //  ошибка при попытке отмена команды
    e_drvaborted   = 25,       //  работа с контроллером двигателей была аварийно завершена  
    e_drvpwron   = 26,         //   ошибка при подаче рабочего напряжения на привод
    e_drvpwroff   = 27,         //   ошибка снятия рабочего напряжения с привода


	e_keyPressedTooRapidly   = 28,       //  кнопки пульта нажимаются в слишком быстром темпе
	e_keyDisabled   = 29,       //  действие по нажатию кнопки запрещено
	e_tcprevive        = 30,       //
//-----
	e_inituncluspedkv  = 31,// КВ1,КВ2 не разомкнуты - остаемся в init0
	e_inituncluspeda  = 32,    // Клещи недостаточно разведены  - остаемся в init0
	e_kv1on=33,                //  kv1 остался замкнутым
	e_kv2on=34,                 // kv2 остался замкнутым

//-----
	e_softwarever = 35,
	e_softwareres = 36,
//-----
	e_caretunknown = 37,
	e_railunknown  = 38,

	e_wrongmode = -1,    //
	e_wrongmodeandstate = -2,    //
	e_wrongstate        = -3,    //
	e_wrongact        = -4,    //
	e_usrstopped      = -5    //
};

char *get_errstr(CERROR i);
//-----------------------------------------------------------------
#define e_sendperiod 20000    // период посылки сообщений об ошибках датчика и реле давления, мС
//
//
//


#define KEN 0            // кнопка обрабатывается
#define KDIS 0x80     // нажатие запрещено - формируется код ошибки KDIS > KMIS
#define KMIS 0x40     // нажатие не фиксируется

const unsigned int cKeysStatus[42][4][a_key] =                           // для режимов поиск, тест, настройка,ручной
{  // ЗАХВАТ   ВЛЕВО    ВПРАВО    ПУСК    РЕЛЬС
	  { // 0
	  { KEN,  KDIS,   KDIS,    KDIS,  KDIS},   // mpoisk-s0
	  { KDIS, KDIS,   KDIS,    KDIS,  KEN} , // rail-s0
#ifdef TUNE_WITHOUT_TESTRAIL
	  { KEN, KDIS,   KDIS,    KDIS,  KDIS}, // tune-s0
#else
	  { KDIS, KDIS,   KDIS,    KDIS,  KEN}, // tune-s0
#endif
	  { KEN,  KDIS,   KDIS,    KDIS,  KDIS}},   // mhand-s0

//
	  { // 1
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},  // mpoisk-srailu - комбинация невозможна
	  { KDIS,  KDIS,   KDIS,    KDIS,  KEN}, // mrail-srailu
#ifdef TUNE_WITHOUT_TESTRAIL
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}, // tune-srailu  - комбинация невозможна
#else
	  { KDIS,  KDIS,   KDIS,    KDIS,  KEN}, // tune-srailu
#endif
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},  // mhand-srailu - комбинация невозможна

//
	  { // 2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},  // mpoisk-srail - комбинация невозможна
	  { KEN,   KDIS,   KDIS,    KDIS,  KEN}, // mrail-srail
#ifdef TUNE_WITHOUT_TESTRAIL
	  { KMIS,   KMIS,   KMIS,    KMIS,  KMIS}, // tune-srail  - комбинация невозможна
#else
	  { KEN,   KDIS,   KDIS,    KDIS,  KEN}, // tune-srail
#endif
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},  // mhand-srail - комбинация невозможна
//
	  { // 3
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},  // mpoisk-sraild - комбинация невозможна
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS}, // mrail-sraild
#ifdef TUNE_WITHOUT_TESTRAIL
	  { KMIS,   KMIS,   KMIS,    KMIS,  KMIS}, // tune-sraild  - комбинация невозможна
#else
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS}, // tune-sraild
#endif
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},  // mhand-sraild - комбинация невозможна

//
	  { // 4
	  { KDIS,   KDIS,   KDIS,    KDIS,  KDIS},  // mpoisk-sseize0
	  { KDIS,   KDIS,   KDIS,    KDIS,  KDIS}, // mrail-sseize0
	  { KDIS,   KDIS,   KDIS,    KDIS,  KDIS}, // tune-sseize0
	  { KDIS,   KDIS,   KDIS,    KDIS,  KDIS}},  // mhand-sseize0

//
	  { // 5
	  { KEN,   KEN,    KEN,     KEN,   KDIS},  // mpoisk-sseize
	  { KEN,   KEN,    KEN,     KEN,   KDIS}, // mrail-sseize
	  { KEN,   KDIS,   KDIS,    KEN,   KDIS}, // tune-sseize
	  { KEN,   KDIS,   KDIS,    KDIS,   KDIS}},  // mhand-sseize
//
	  { // 6
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS},   // mpoisk-sleave0
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS}, // rail-sleave0
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS},   // tune-sleave0
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS}},   // mhand-sleave0

//
	  { // 7
	  { KDIS,  KDIS,   KDIS,    KEN,   KDIS},   // mpoisk-sscan
	  { KDIS,  KDIS,   KDIS,    KEN,   KDIS}, // rail-sscan
	  { KMIS,  KMIS,   KMIS,    KMIS,   KMIS}, // tune-sscan - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,   KMIS}},   // mhand-sscan - комбинация невозможна

//
	  { // 8
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-aj1 - комбинация невозможна
	  { KDIS,   KEN,    KEN,     KDIS,  KDIS},  // rail-aj1
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-aj1 - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-aj1 - комбинация невозможна

//
	  { // 9
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-aj2 - комбинация невозможна
	  { KDIS,   KEN,    KEN,     KEN,  KDIS},  // rail-aj2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-aj2 - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-aj2 - комбинация невозможна

//
	  { // 10
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-aj3 - комбинация невозможна
	  { KDIS,  KDIS,   KDIS,    KEN,  KDIS},  // rail-aj3
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-aj3 - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-aj3 - комбинация невозможна

//
	  { // 11
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-j1e1 - комбинация невозможна
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS},  // rail-j1e1
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-j1e1 - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-j1e1 - комбинация невозможна

//
	  { // 12
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-j2e1 - комбинация невозможна
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS},  // rail-j2e1
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-j2e1 - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-j2e1 - комбинация невозможна


	  { // 13
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS},   // mpoisk-scr
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS},  // rail-scr
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS},   // tune-scr
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS}},   // mhand-scr


	  { // 14
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-brkstate2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},  // rail-brkstate2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-brkstate2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-brkstate2 -  - комбинация невозможна
//
	  { // 15
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-motorRestore1
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},  // rail-motorRestore1
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},  // tune-motorRestore1
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-motorRestore1

//
	  { // 16
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},    // mpoisk-motorRestore2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},    // rail-motorRestore2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},  // tune-motorRestore2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},    //mhand-motorRestore2

//
	  { // 17
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},    // mpoisk-motorRestore3
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},    // rail-motorRestore3
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},  // tune-motorRestore3
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},    // mhand-motorRestore3

//
	  { // 18
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-motorRestore4
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-motorRestore4
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-motorRestore4
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-motorRestore4

//
	  { // 19
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-spwron
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-spwron
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune--spwron
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-spwron

//
	  { // 20
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-spwroff
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-spwroff
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune--spwroff
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-spwroff


	  { // 21
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk- stune - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-stune - комбинация невозможна
	  { KDIS,  KDIS,   KDIS,     KEN,  KDIS},   // tune-stune
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand- stune - комбинация невозможна

//
	  { // 22
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk- stuneWaitEndMotion - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-stuneWaitEndMotion - комбинация невозможна
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS},   // tune-stuneWaitEndMotion
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand- stuneWaitEndMotion - комбинация невозможна

//
	  { // 23
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk- unsteady1
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-unsteady1
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-unsteady1
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand- unsteady1

//
	  { // 24
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk- unsteady2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-unsteady2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-unsteady2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand- unsteady2

//
	  { // 25
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-spredtune - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-spredtune - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-spredtune
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-spredtune - комбинация невозможна

//
	  { // 26
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-sposttune1  - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-sposttune1  - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-sposttune1
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-sposttune1  - комбинация невозможна

//
	  { // 27
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-spredscan
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mrail-spredscan
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mtune-spredscan - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-spredscan - комбинация невозможна

//
	  { // 28
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-spostscan1
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-spostscan1
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-spostscan1 - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-spostscan1 - комбинация невозможна


	  { // 29
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-spostscan2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-spostscan2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-spostscan2 - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-spostscan2 - комбинация невозможна

//
	  { // 30
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-sposttune2  - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-sposttune2  - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-sposttune2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-sposttune2

//
	  { // 31
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-s0i
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-s0i
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-s0i
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-s0i
//
	   { // 32
	   { KEN,  KDIS,   KDIS,    KDIS,  KDIS},   // mpoisk-s0E
	   { KDIS, KDIS,   KDIS,    KDIS,  KEN} , // rail-s0E
#ifdef TUNE_WITHOUT_TESTRAIL
	   { KEN, KDIS,   KDIS,    KDIS,  KDIS}, // tune-s0E
#else
	   { KDIS, KDIS,   KDIS,    KDIS,  KEN}, // tune-s0E
#endif
	   { KEN,  KDIS,   KDIS,    KDIS,  KDIS}},   // mhand-s0E

//
	  { // 33
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-sBLOFF
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-sBLOFF
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-sBLOFF
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-sBLOFF

//
	  { // 34
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-sBLFOFF
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-sBLFOFF
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-BLFOFF
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-sBLFOFF

//    35
	  {
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-sleavee
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-sleavee
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-sleavee
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-sleavee
// 36
	  {
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},  // mpoisk-sraile - комбинация невозможна
	  { KEN,   KDIS,   KDIS,    KDIS,  KEN}, // mrail-sraile
#ifdef TUNE_WITHOUT_TESTRAIL
	  { KMIS,   KMIS,   KMIS,    KMIS,  KMIS}, // tune-sraile  - комбинация невозможна
#else
	  { KEN,   KDIS,   KDIS,    KDIS,  KEN}, // tune-sraile
#endif
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},  // mhand-sraile - комбинация невозможна

//
	  { // 37
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-srailde - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-srailde
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-srailde
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-srailde - комбинация невозможна

//
	  { // 38
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-srailde2 - комбинация невозможна
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-srailde2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-srailde2
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-srailde2 - комбинация невозможна


//
	  { // 39
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},  // mpoisk-sraild2 - комбинация невозможна
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS}, // mrail-sraild2
#ifdef TUNE_WITHOUT_TESTRAIL
	  { KMIS,   KMIS,   KMIS,    KMIS,  KMIS}, // tune-sraild2  - комбинация невозможна
#else
	  { KDIS,  KDIS,   KDIS,    KDIS,  KDIS}, // tune-sraild2
#endif
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},  // mhand-sraild2 - комбинация невозможна

//
	  { // 40
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},  // mpoisk-srailu2 - комбинация невозможна
	  { KDIS,  KDIS,   KDIS,    KDIS,  KEN}, // mrail-srailu2
#ifdef TUNE_WITHOUT_TESTRAIL
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}, // tune-srailu2  - комбинация невозможна
#else
	  { KDIS,  KDIS,   KDIS,    KDIS,  KEN}, // tune-srailu2
#endif
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},  // mhand-srailu2 - комбинация невозможна


{ // 41
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // mpoisk-s0BLON
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // rail-s0BLON
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS},   // tune-s0BLON
	  { KMIS,  KMIS,   KMIS,    KMIS,  KMIS}},   // mhand-s0BLON

};
//

typedef void  (__closure *VPROCB)(BOOL);
typedef void  (__closure *VPROCUINT)(unsigned int);

#define railupmaxtime          10000                 // максимальное время поднятия рельса
#define raildownmaxtime      15000                 // максимальное время опускания рельса

#define seizemaxtime      15000                        // максимальное время захвата/отпускания рельса
#define scanoffwaitmaxtime 5000                   //

#define DIPmistake           10
#define DIPdiffmax           100                         // максимально допустимый перекос без учета разницы в исходном (разжатом) положении
// abs(DIP1s0 - DIP2s0) - разница в разжатом положении

typedef void  (__closure *LOGPROC)(char *);

//
typedef struct _INPARAMDEF
{
LOGPROC        errorLogProc; // функция пользователя, производящая запись сообщения об ошибке
VPROCUINT     errorNumSetProc; // функция пользователя, получающая код сообщения об ошибке (CERROR)
LOGPROC        debugLogProc; // функция пользователя, производящая запись отладочного сообщения
VPROCB scanstateproc; // функция пользователя,  инициирующая\останавливающая режим сканирования или Юстировки3
//  Параметр функции - НАЧАТЬ/ОСТАНОВИТЬ (1/0) сканирование, будет вызываться по нажанию на пульте кн. "пуск"
} INPARAMDEF;
//
//
//
#define AVTRERRORBASE (ERROR_WRONG_TROLLEY_POS-1)
enum AVTKERROR
{
   ERROR_ACTION_NOT_COMPLETED = AVTRERRORBASE
};
//
enum ALARMSTATE
{
        AlarmSend = 0,
        AlarmReceive
};
//
typedef struct _KRASOFTWAREVERSION
{
    UCHAR major;
    UCHAR minor;
    UCHAR build;
    BOOL    isDataValid;
} KRASOFTWAREVERSION;
//
const KRASOFTWAREVERSION KRAMinimalVer = {3,3,1}; // минимальная версия ПО контроллера
// рассчитанная на работу с этой версией класса
const KRASOFTWAREVERSION minimalVerDemand1 = {3,2,1}; // минимальная версия ПО контроллера, обрабатывающая
// запросы dataBlockMessageNewId
//

// формат байта, записываемого в qKeyEvent
#define KEYPRESSEDEVENTMASK 0x80  //   1/0 - нажата/отжата
// остальное - код кнопки
//
enum CAVTKMSGTYPE
{
   MessageNone = 0, // обозначение пустого элемента
   ErrorMesage = 1,
   ErrorAnswer = 2,
   UserStopMessage = -1
};
enum ANSWERTYPE
{
	AnswerNone = 0,      // ответ на errorMessage не требуется
	AnswerBinary = 1 //
};

enum BINARYANSWER
{
	RejectCode = 0,
	AcceptCode = 1
};

typedef struct _cavtkMsg
{
	CAVTKMSGTYPE type;
	unsigned int code;
	unsigned int flag;
} tCavtkMsg;

typedef struct _outputState
{
BOOL state;
BOOL forced;
} tOutputState;
#define AnswerPollingInterval 30000 // интервал ожидания реакции пользователя на сообщение об ошибке

typedef struct _STATUS
{
	CMODE mode;
	CSTATE state;
} STATUS;
//
class cSTATUS;
//----------------------------------------------------------------------------------------------------------------------------------------------------------
class cavtk
{
private:

			 BOOL bWork;
			 cDataTransfer *DT;
			 int UnitIdx;
			 int motorIdx;

			 BOOL Recive;
//
			   cSTATUS *pStatus;
			   unsigned long tmr;

			   static const unsigned int uncluspMinValue = 700;  // минимальное значение с датчика, при котором полагаем
// каретки разжатыми
//
			  BOOL dsensorstate[a_dsensor];
			  unsigned int asensorvalue[a_asensor];
			  BOOL actualOutputs[a_outputs];    //
			  tOutputState targetOutputs[a_outputs];
			  USHORT KRAtcpConnectionCounter;  //
			  BOOL fKRAtcpConnectionCounterValid; // сбрасывается при получении dataBlock первоначального формата

			  BOOL keysStatus[a_key];  // 0 - нажатие кнопки обрабатывается,

// код кнопки, нажатие которой зафиксировано 1...
			  unsigned char keypressedId;
			  unsigned char keypressedIdMem;   // используется в engine
			  std::queue <unsigned char> qKeyEvent;


			  BOOL kv1Kv2FailIgnore; // если установлен, переходим в состояние sseize независимо от
// состояния кв1;кв2
			  BOOL initalRailPositionIgnore;  // если установлен, подъем рельса в режиме ТЕСТ
// возможен при любом положении датчиков КВГ2,4
			  char motorControlIP[16];
			  BOOL fMotionEnd;
			  ERRORMSG motionEndError;
			  unsigned long motorDevTimer;
			  unsigned int motorRestoreCounter;
			  unsigned int kraRestoreCounter;
                                                                KRASOFTWAREVERSION kraSoftwareVer; 

			  CRITICAL_SECTION motorObjCr;

			  unsigned long motorDevTimer1;


			  unsigned long longPressedTmr;  // отслеживает длительное нажатие кнопки
			  unsigned char longPressedKeyId;  // код кнопки, длительное нажатие которой зафиксировали
			  BOOL fMotorDisabled;          // если установлен,  двигатели не используем
			  BOOL fMD;                           // если установлен, с двигателями не работаем
			  BOOL fTransferToMotorDis;
			  BOOL fMotorPowered;
			  BOOL fMotorRestarted;       // ставим true после подачи сигнала сброса на bosh
//
										 // ставим false после фиксации отпускания кнопки
			 static const DWORD infoQryTimeout = 5000;
			 CRITICAL_SECTION infoQryCr;
			 HANDLE infoQryEvent;
			 UCHAR *infoDataBuffer;
			 unsigned int infoDataBufferLength;
			 ALARMSTATE fAlarmState;
			 TMR alarmTimer;
			 unsigned char alarmCounter;
			 unsigned char alarmCounterMem;
															   unsigned int alarmSendPeriod;
															   unsigned int alarmReceiveTimeout;
															   static const BOOL fReinitTCPConnections = true;
//
			 BOOL fTransferToKRAEn;
			 CRITICAL_SECTION tickCr;
//
			TMR unsteadyWaitTimer;
//
			CRITICAL_SECTION setModeCr;
			HANDLE  hSetModeEvent;
			static const DWORD setModeTimeout = 5000;
//
			 LogFileO *ownLog;
#ifdef LOG_SHOW_INTERVAL
			 TMR logTickMem;
#endif
#ifdef CRASH_LOG
			 CRITICAL_SECTION crashLogCr;
#endif
			std::queue <tErrorReport1DataV1> errorReportBuffer;

			 static const unsigned long unsteadyWaitTimeout = 4000;

			  tCavtkMsg imessage;
			  std::queue <tCavtkMsg> outcomeMsgBuffer;
			  std::queue <tCavtkMsg> incomeMsgBuffer;
			  TMR answerPollingTmr;
			  BOOL fAnswerPollingIntervalON; // устанавливаем после отсылки кода, требующего
// ответа


// запрещает все кнопки пульта
	 void setkeysdisabled(void);
// все кнопки пульта делает неактивными
	 void setkeysmissed(void);

// выставляет разрешения кнопок в соответствии состоянием и режимом
	void defineKeysStatus(void);
//
	 VPROCB puskKeyPressedProc;
	 LOGPROC   errorLog_proc;
	 LOGPROC   debugLog_proc;
	 CRITICAL_SECTION LogCr;
	 void settimer(void) { tmr = GetTickCount_(); }
	 unsigned long get_duration(void);
	 void __fastcall dsensorevent(unsigned char num, BOOL state);
	 void __fastcall asensorevent(unsigned char num, unsigned short value);
	 void __fastcall avtErrorEvent(unsigned char majorcode, unsigned char minorcode);
	 void __fastcall outputsEvent(unsigned char num, BOOL state);
	 void __fastcall errorReportEvent(unsigned char *pDataBuf);

//
	 void go_mode(CMODE newmode);
	 void set_state(CSTATE newstate,BOOL locked);
	 void setstate(CSTATE newstate,BOOL locked);
	 void go_status(STATUS newS);
	 void go_status(STATUS newS, VPROCB proc,BOOL parameter);
	 void setStatus(STATUS *pStatus,BOOL locked);
	 tCallBack motionEndEventProc;
//
	 void motorErrorLog(ERRORMSG error);
	 void __fastcall cavtkCallBack(tMotionResult Result);
	 void __fastcall motionEndProc(tMotionResult result);
	 void __fastcall motorPWROnEndProc(tMotionResult result);
	 void __fastcall motorPWROffEndProc(tMotionResult result);
	 int  motorInit(UCHAR idx, cDataTransfer *DT);
	 int motorInit1(void);
	 void conditionalGoToMotorReviving(ERRORMSG *pe,BOOL locked);
	 void goToMotorReviving(BOOL locked);
	 void switchMotorPwr(BOOL fPwrOn);
	 BOOL isMotorPowered(void);
	 void clearMotionEndError(void);
  
	 char* getKeyName(unsigned char keyNum);
	 void __fastcall infoEvent(unsigned char *buf, unsigned int dataLength);
	 char* getModeName(enum CMODE  num);
	 char* getStateName(enum CSTATE  num);

	 void __fastcall alarmEvent(unsigned char number);
                      void setWaterGate(BOOL state);
	 void __fastcall dataBlockEvent(unsigned char *databuf);
//
	 void engineKeyProcedure(void);
	 void engineAlarmProcedure(BOOL fReinitCon, BOOL locked);
	 void engineMainProcedure(unsigned long *ptmr);
     void engineInit0(void);
	 void setOutputPinsToTarget(void);
	 void engineUnsteadyStates(CMODE unitmode,CSTATE unitstate); // отработка действий в unsteady1,unsteady2
	 void goToKRAReviving(BOOL locked);
	 void leaveMotorReviving();
	 void longPressedKeyReleaseAction(void);
     void shortPressedKeyReleaseAction(CSTATE stateWhenPressed);
	 void goStatus(STATUS newS);
	 void goState(CSTATE newstate);
     int getmode(BOOL locked);
	 void walkTowardFormerState(void);
//
                      BOOL testUncluspedA(void);
                      BOOL testUncluspedD(void);
//
	 void setPnRaspred1(BOOL state);
	 void setPnRaspred2(BOOL state);
	 void setPnRaspred3(BOOL state);
	 void setPointer(BOOL state);
	 void setBlokir(BOOL state);
//
	 void setPnRaspred3(BOOL state,BOOL fForced);
	 void setBlokir(BOOL state,BOOL fForced);
	 void resetPnRaspred3Forced(void);
	 void resetBlokirForced(void);
	 void setError(CERROR errcode,BOOL fToAnswer);

	 void mainEngineSrailuAct(CSTATE state);
	 void mainEngineSraildAct(CSTATE state);
	 void mainEngineSseize0Act(void);
	 void mainEngineSleave0Act(CMODE unitmode);
	 void mainEngineBrkstate2Act(CMODE unitmode);
	 void mainEngineScrAct(CMODE unitmode);
	 void mainEngineAjAct(CSTATE unitstate);
     void mainEnginePwrAct(CMODE unitmode, CSTATE unitstate);
	 void wrongModeAndStateStop(char* s,CMODE unitmode,CSTATE unitstate,BOOL locked);
	 void wrongStateStop(char* s,CSTATE unitstate,BOOL locked);
	 void engineMainProcedureStop(CMODE mode, CSTATE state);
     BOOL testErrorReports(void);
//           
	 BOOL isKRAVersionValid(KRASOFTWAREVERSION *pMinimalDemand);
	 void messageClear(void);
	 void messageSeize(BOOL toDeleteMessage);
     void messageSeizeInit(void);
	 CAVTKMSGTYPE getMessageType(void);
	 int errorAnswerTest(void);
     void mainEngineUserAct(CMODE unitmode, CSTATE unitstate);

public:
//----------------------------------------------------------------------------------------------------------------
	bool secondThread_Flg;
	bool AThread_Flg;

	int datalinenumber;   // номер LAN канала передачи данный


			 KAVT *dev;
			 motorctrl *motorDev;
//
			  BOOL getstate(void){return bWork;}
			  void set_error(CERROR errcode);
			  BOOL testkvg24on(BOOL ferrprn);
			  void  engine(unsigned long *tmr);
			  int set_mode(CMODE newmode,BOOL locked);
			  bool getMessage(tCavtkMsg *pMsg);
			  void sendMessage(tCavtkMsg *pMsg);
//
			   void settimer(unsigned long *pt) { if (pt) *pt = GetTickCount_(); }
			   unsigned long get_duration(unsigned long *pt);
			   BOOL isTransferToMotorDisabled();
			   void errorLOG(char *,...);
			   void LOG(char *,...);
#ifdef CRASH_LOG
			   void crash(char *,...);
#endif
			   void lockMotorObj(void) {EnterCriticalSection(&motorObjCr);}
			   void unLockMotorObj(void) {LeaveCriticalSection(&motorObjCr);}
			   void Tick(void);
			   void closeWaterGate(); // вызывается в motorctrl перед выполнением 15 и 24 движения
			   void motorLog(char * s);
//----------------------------------------------------------------------------------------------------------------

 // если последний параметр = true - приводы не использовать
			 cavtk(int, cDataTransfer* , const char*,BOOL);
			 ~cavtk(void);
//
//
//
// функции вызываемые пользователем класса
//
// необходимо вызвать после создания объекта класса для перехода из начального состояния в minit
// возвращает 0 при удачном завершении
// ошибки:
// -1 -  не удалось установить TCP-соединение с bosh-контроллером
// -2 -  класс управления контроллером не создан
// -3 -  метод init() завершился с ошибкой
// -4 - функция init1() класса управления двигателями завершилась с ошибкой
// -5 - не иссякла необходимая задержка перед инициализацией bosh-контроллера
// после включения питания
		  int init(INPARAMDEF * inparamptr);
//-------------------------------------------------------------------
// установка режима автокона minit, mpoisk, mrail (см. CMODE)
// возвращает 0 при успешном завершении
//                      -1 - задан недопустимый режим
//                      -2 - не выполнены условия для перехода, имел(и) место вызов(ы) errorproc() 
// -3 - переход был отклонен из-за невыполнения команды контроллером
// -4 - не дождались завершения перехода для перехода
// -5 - ошибка события на ожидании

// возможны переходы minit  <-> mpoisk,  minit  <-> mrail, minit<->mtune
                       int setmode(CMODE mode);
//-------------------------------------------------------------------
//
// возвращает: положительное число  - режим автокона minit,mpoisk,mrail,
//                        -1 - init() не была выполнена или еще не завершился опрос датчиков или
//                               значения всех датчиков не удалось получить см. код e_initsensorqry
	  int getmode(void);
//-------------------------------------------------------------------

//
// выполняет движение
// возвращает: -1, если параметр proc не задан, иначе - 0
// может вызываться, после сведения кареток
// proc - функция, вызываемая по завршению движения
// возможно функция proc вызовется до того, как завершится эта в случае ошибки


						  int motionStart(tCallBack proc);
//-------------------------------------------------------------------
// вызывает смещение каретки на smesh мм относительно текущего положения в режиме настройка
// возвращает: -1, если параметр proc не задан, иначе - 0
// fBottomTrolley = false - верхняя каретка
// smesh < 0 - движение влево
// возможно функция proc вызовется до того, как завершится эта в случае ошибки



						  int motionStart(BOOL fBottomTrolley,int smesh,tCallBack proc);
//-------------------------------------------------------------------
// вызывает смещение кареток относительно текущего положения в режиме настройка
// в состоянии stune
// возвращает: -1, если параметр proc не задан, иначе - 0
// upperTrolleyShift - смещение верхней каретки, bottomTrolleyShift - нижней
// ...TrolleyShift< 0 - движение влево
// возможно функция proc вызовется до того, как завершится эта
                                                                                                                               int motionStart2(int bottomTrolleyShift, int upperTrolleyShift, tCallBack proc);
//-------------------------------------------------------------------
// прерывает последовательность движений в режимах сканирование или
// Юстировка 3
// в режиме Настройка:
// - в состоянии stune вызывает переход в sseize
// - в состоянии stuneWaitMotion вызывает переход в brkstate2
// возвращает: -1, если параметр proc не задан, при успешном завершении - 0
// или ERROR_ACTION_DENIED
// может вызываться, после сведения кареток
// proc - функция, вызываемая по завршению движения
// но функция proc будет вызвана до того, как завершится эта в случае ошибки
// proc должна быть отличной от той, что была указана для motionStart(), т.к.
// при отмене незавершенного движения будут вызваны
// proc-MotionBreak c результатом ERROR_NO и proc-MotionStart с ошибкой ERROR_CANCELLED

						  int motionBreak(tCallBack proc);
//-------------------------------------------------------------------
// функции, используемые для отображения текущего состояния датчиков
//
// получить текущее состояние дискретных датчиков, (в т.ч кнопок пульта)
// size - размер массива в элементах
// возвращает положительное значение - число датчиков -  a_dsensor
// -1 - размер массива недостаточен
// элемент 0 соответствует кнопке "захват" пульта (идентификатор см. kradef.h)
	int getdsensorarray(BOOL *array, unsigned int size);
//-------------------------------------------------------------------
//
// получить текущее состояние  датчиков перемещения
// size - размер массива в элементах
// возвращает положительное значение - число датчиков: a_asensor
// -1 - размер массива недостаточен
	int getasensorarray(unsigned int *array, unsigned int size); // получить текущее состояние  датчиков перемещения
//-------------------------------------------------------------------
//
// получить число дискретных датчиков, (в т.ч кнопок пульта)
	int getdsensorq(void) {return a_dsensor;}
//-------------------------------------------------------------------
//
// получить число  датчиков перемещения
                     int getasensorq(void) {return a_asensor;}
//
//-------------------------------------------------------------------
	void defineKv1Kv2FailIgnore(BOOL state);// {kv1Kv2FailIgnore = state;}
	void defineInitalRailPositionIgnore(BOOL state);
//-------------------------------------------------------------------
// возвращает серийный номер контроллера - положительное число (младшие 2 байта)
// в течение infoQryTimeout мС
// в случае ошибки возвращает отрицательное значение
	int getDeviceNumber(void);
//-------------------------------------------------------------------
// возвращает версию ПО адаптера в виде X.Y.Z  в течение infoQryTimeout мС
// в случае успеха возвращает 4 и в буфер пользователя записывает 4 байта,
// где Z -> bufptr[0], Y -> bufptr[1], X -> bufptr[2]
// в случае ошибки возвращает отрицательное значение
	int getVersion(UCHAR *bufptr);
//-----------------------------------------------------------------------------------------------------------------
//   записывает состояние кареток двигателей в структуру, задаваемую pDest (motorctrl.h)
//   0 нормальное завершение 
//  -1 ошибка чтения регистров контроллера двигателей 
//  -2 внутренняя ошибка 
//  -3 объект управления двигателями не существует 
                     int getCariagesStatus(tCARIAGESTAT *pDest); 
//-----------------------------------------------------------------------------------------------------------------
// вызывает возврат кареток в парковочное положение в режиме настройка
// в состоянии stune
// возвращает: -1, если параметр proc не задан, иначе - 0
// возможно функция proc вызовется до того, как завершится эта
                     int trolleysReturn(tCallBack proc);
//-----------------------------------------------------------------------------------------------------------------
// переводит состояние подсветки камеры подошвы в state: true/false (вкл./откл)
	 void setLight(BOOL state);
//-----------------------------------------------------------------------------------------------------------------
// возвращает состояние блокировки рельса TRUE/FALSE - вкл/откл
	 BOOL getBlokirState(void);
};
//
//-------------------------------------------------------------------------------------------

#define motorInitializaitionDelayTime 45000     // ms - задержка до инициализации bosh-контроллера после включения питания
#define сMotorRestoreCounter  3                  // количество попыток сброса ПО bosh-контроллера для восстановления после ошибки
#define motorResetSignalTime  500                  // длительность сигнала сброса bosh-контроллера

//
//----------------------------------------------------------------------------------------------------------------------------------------------------------

/*
typedef struct _STATUS
{
	CMODE mode;
	CSTATE state;
} STATUS;
*/
//
typedef struct _tSTATUS
{
	STATUS current;
	STATUS stored;
} tSTATUS;
//----------------------------------------------------------------------------------------------------------------------------------------------------------

class cSTATUS
{
private:
	std::vector<tSTATUS> status;   // вектор содержит минимум 1 элемент

	STATUS   predStatus;
	LOGPROC   logProc;
	LOGPROC   crashProc;


	unsigned int index;
	CRITICAL_SECTION statusLockCr;
	int lockCount;
//

	void getStatus(STATUS *pStatus);
	void setStatus(STATUS *pStatus);
	void storeStatus(void);
	void init(STATUS s);
	void toSetStatus(STATUS *pStatus);
    
public:
//	cSTATUS(STATUS status,LOGPROC _logproc,LOGPROC crashproc);
   cSTATUS(STATUS status);
    ~cSTATUS();
    void lock(void);
    void unlock(void);

    void pushStatus(STATUS status,BOOL locked); // делает текущим status
    void pushStatus(CSTATE state,BOOL locked); // делает текущим state
// 
    STATUS popStatus(BOOL locked);
//
    CSTATE getState(BOOL locked);
	void setState(CSTATE state,BOOL locked);
    CMODE getMode(BOOL locked);
	void setMode(CMODE mode,BOOL locked);  // не изменяет поля "stored"
    void setStatus(CMODE mode,CSTATE state,BOOL locked);
    STATUS getPredStatus(void);
    STATUS getStoredStatus(BOOL locked);
};
#endif
