
#include "LANProtAVTK.h"
#include <math.h>
#include <windows.h>

#include <string.h>



namespace LANProtDebugAVTK
{

	Add_Log onAddLog;
	Add_Log2 onAddLog2 = NULL;
	Add_Log3 onAddLog3 = NULL;

#ifdef DbgLog
	BOOL useLog = false;
#endif


#ifdef Dbg

					 unsigned int rcvbytescnt;
#ifdef LINUX_

#else
	CRITICAL_SECTION cr;
#endif
//


void addbytcnt(unsigned int val)
{
			EnterCriticalSection(&cr);
			rcvbytescnt += val;
			LeaveCriticalSection(&cr);

}
//

unsigned int rdbytcnt(void)
{
unsigned int res;
			EnterCriticalSection(&cr);
			res = rcvbytescnt;
			rcvbytescnt = 0;
			LeaveCriticalSection(&cr);
			return res;

}
//
void initbytcnt(void)
{
	  InitializeCriticalSection(&cr);
	  rcvbytescnt = 0;
}
//
void deinitbytcnt(void)
{
	  DeleteCriticalSection(&cr);
}
#endif

//
}

using namespace LANProtDebugAVTK;

//---------------------------------------------------------------------------
KAVT::KAVT(int UMUIdx_, cDataTransfer* hrd_)
{

	UMUIdx = UMUIdx_;
	hrd = hrd_;
	RState = rsOff;
	OutStIdx = 0;
	OutEdIdx = 0;


	OutBlock = new tKRA_Message;
                memset(OutBlock,0,sizeof(tKRA_Message));


	SendTickT = GetTickCount_();

	QrySensorEvent = CreateEvent(NULL,MANUAL_RESET_EVENT,FALSE,NULL); // ����.�����, NonSignaled
	InitializeCriticalSection(&QrySensorCr);
	qrystate = QryOff;
//
	DSensorProc = NULL;
	ASensorProc = NULL;
	infoMessageProc = NULL;
	errorMessageProc = NULL;
	alarmProc = NULL;
                    dataBlockProc = NULL;
                     OutputProc = NULL;
                     errorReportProc = NULL;


	InitializeCriticalSection(&outBlockCr);
	QryDataBlockEvent = CreateEvent(NULL,MANUAL_RESET_EVENT,FALSE,NULL); // ����.�����, NonSignaled


	#ifdef UMULog                           //
	if (!Log) Log = new LogFileObj(this);
	Log->AddIntParam("Create Object UMU:%d", UMUIdx);
	#endif

#ifdef Dbg
                    initbytcnt(); 
#endif

}
//---------------------------------------------------------------------------
KAVT::~KAVT(void)
{
	delete OutBlock;

	if (QrySensorEvent) CloseHandle(QrySensorEvent);
	DeleteCriticalSection(&QrySensorCr);
	DeleteCriticalSection(&outBlockCr);
	if (QryDataBlockEvent) CloseHandle(QryDataBlockEvent);




//	#ifdef BUMLog                           // 
	#ifdef UMULog                           // 
	Log->AddIntParam("Delete Object UMU:%d", UMUIdx);
//	if ((!Log) && (Log->Owner == this)) delete Log;
	if ((Log!=NULL) && (Log->Owner == this)) delete Log;
	#endif

#ifdef Dbg
                    deinitbytcnt(); 
#endif

}
//---------------------------------------------------------------------------
void KAVT::StartWork(void)
{
	RState = rsHead;

	#ifdef UMUEventLog                           // 
	Log->AddIntParam("UMU:%d StartWork", UMUIdx);
	#endif

}
//---------------------------------------------------------------------------
void KAVT::EndWork(void)
{
	#ifdef UMUEventLog                           //
	Log->AddIntParam("UMU:%d Power Switch Off", UMUIdx);
	Log->AddIntParam("UMU:%d EndWork", UMUIdx);
	#endif
}
//---------------------------------------------------------------------------
void KAVT::AddToOutBuff(void) //      -   OutBlock
{
	tKRA_Message tmp;
    OutBlock->HeaderSign = cHeaderSign;
	memcpy(&tmp,OutBlock,sizeof(tmp));
	OutBuffer.push(tmp);
                memset(OutBlock,0,sizeof(tKRA_Message));
}
//---------------------------------------------------------------------------
void KAVT::Unload(void)
{

//	int f = OutBuffer.size();
	while (!OutBuffer.empty())
	{
		if (hrd->WriteBuffer(UMUIdx, (unsigned char *)&OutBuffer.front(),OutBuffer.front().Size+cHeaderLng))
		{
			#ifdef DbgLog
				if (useLog)
					onAddLog((unsigned char *)&OutBuffer.front(),OutBuffer.front().Size+cHeaderLng,0);
			#endif
		}
		else
		{

			#ifdef UMUOutDataLog
			Log->AddText(" - WriteToLAN - Error");
			#endif
		}
		OutBuffer.pop();

	}
//    OutBuffer.clear();

}
//---------------------------------------------------------------------------
void KAVT::TickSend(void)
{
	Unload();
}
//---------------------------------------------------------------------------
void KAVT::TickReceive(void) //
{
unsigned short res;
bool fRepeat;
   do
   {
	fRepeat = false;
	switch (RState)
	{
		case rsStart:
	 	    res = hrd->ReadBuffer(UMUIdx,&LAN_message.HeaderSign,1);
                                              if (res)
                                              {
                                                   if (LAN_message.HeaderSign == cHeaderSign)                        
                                                   {
		             RState = rsHeaderSign;
		             fRepeat = true;
                                                   }
                                              } 
                                              break;
		case rsHeaderSign:
		{
				res = hrd->ReadBuffer(UMUIdx,(unsigned char *)&LAN_message.Size,1);
				if (res)
				{
					if ((LAN_message.Size != 0)  &&  (LAN_message.Size <= cDataLengthMax) )
					{

					 RState = rsDataLng;
					 alreadyReceived = 0;
																										 ReadBytesCount = LAN_message.Size;
					 toBeReceived = ReadBytesCount;

					#ifdef DbgLog
						if (useLog)
							onAddLog((unsigned char *)&LAN_message,1,1);
					#endif
//
					#ifdef Dbg
					addbytcnt(1);
					#endif
						fRepeat = true;
					}
						else if  (LAN_message.Size != cHeaderSign)
							 {
								 RState = rsStart;
								 #ifdef DbgLog
									 if (useLog)
									 onAddLog((unsigned char *)&LAN_message,1,0xFF);
								 #endif
							 }
				}
			break;
		}
		case rsDataLng:
		{
		  res = hrd->ReadBuffer(UMUIdx,(unsigned char *)&LAN_message.Data[alreadyReceived],toBeReceived);
		  if (res)
		  {
			alreadyReceived += res;
			if (alreadyReceived == ReadBytesCount)
			{

				EnterCriticalSection(&QrySensorCr);
																				   UnPack();
				LeaveCriticalSection(&QrySensorCr);
//
				RState = rsStart;
				if (onAddLog) onAddLog((unsigned char*)&LAN_message,ReadBytesCount+1,1);

				#ifdef DbgLog
					if (useLog)
						onAddLog(LAN_message.Data,ReadBytesCount,2);
				#endif
				#ifdef Dbg
					addbytcnt(res);
				#endif

			}
				else toBeReceived -= res;
			fRepeat = true;
		   }
			break;
		}
		case rsSwitchedOff: break;
	}
   }
   while (fRepeat);

}
//---------------------------------------------------------------------------
void KAVT::UnPack()
{
    switch(LAN_message.Data[DataIDOffs]) 
    {
        case dsensorstateid:
        {
            if  (LAN_message.Size == szDsensorstate)
            {
                if (DSensorProc)
                {
	if (LAN_message.Data[DataBodyOffs+1] == off_state) DSensorProc(LAN_message.Data[DataBodyOffs],false);
	    else /*if (LAN_message.Data[1] == on_state)*/
	        DSensorProc(LAN_message.Data[DataBodyOffs],true);
                }
                if ((qrystate == QryDSensor) && (qryidx == LAN_message.Data[DataBodyOffs])) SetEvent(QrySensorEvent);
            }
            break;
        } 
//
        case asensorstateid:
        {
            if (LAN_message.Size == szAsensorstate)
            {
                if (ASensorProc)  ASensorProc(LAN_message.Data[DataBodyOffs], LAN_message.Data[DataBodyOffs+1] | (LAN_message.Data[DataBodyOffs+2] << 8));
                if ((qrystate == QryASensor) && (qryidx == LAN_message.Data[DataBodyOffs])) SetEvent(QrySensorEvent);
            }
            break;
        }  
//
       case deviceNumberMessageId:
       {
           if((LAN_message.Size == szDeviceNumberMsg) && (infoMessageProc)) 
               infoMessageProc(&LAN_message.Data[DataBodyOffs],DEVICENUMBER_MESSAGE_LENGTH);
           break;
       }
//
       case versionMessageId:
       {
           if((LAN_message.Size == szVersionMsg) && (infoMessageProc)) 
               infoMessageProc(&LAN_message.Data[DataBodyOffs],VERSION_MESSAGE_LENGTH);
           break;
       }
//
       case alarmMsgId:
       {
           if ((LAN_message.Size == szAlarmMsg) && (alarmProc)) alarmProc(LAN_message.Data[DataBodyOffs]);
           break;
       }
//
       case dataBlockMessageId:
       {
           if((LAN_message.Size == sizeof(DATAV1)+1)||(LAN_message.Size == sizeof(DATAV2)+1))
           {      
               if (dataBlockProc) dataBlockProc(&LAN_message.Data[DataBodyOffs]);
              SetEvent(QryDataBlockEvent);
           }
           break;
       }
//
       case outputstateid:
       {
         if  (LAN_message.Size == szOutputstate)
         {
             if (OutputProc) 
             {
                 if (LAN_message.Data[DataBodyOffs+1] == off_state) OutputProc(LAN_message.Data[DataBodyOffs],false);
                      else OutputProc(LAN_message.Data[DataBodyOffs],true);
             }
         }
         break;
       }
//
       case errorMessageId:
       {
           if ((LAN_message.Size == szErrorMessage) && (errorMessageProc)) 
               errorMessageProc(LAN_message.Data[DataBodyOffs],LAN_message.Data[DataBodyOffs+1]);
           break;
       }
//
       case errorreportId:
       {
           if (LAN_message.Size == sizeof(tErrorReport1DataV1)+1)
              if (errorReportProc) errorReportProc(&LAN_message.Data[DataBodyOffs]);
           break;
       }
//

    } // switch
}
//---------------------------------------------------------------------------
void KAVT::Tick(void)
{
	if ((GetTickCount_() - SendTickT) >= 50)
	{
		this->TickSend();
		SendTickT = GetTickCount_();
	}

	this->TickReceive();
}
//---------------------------------------------------------------------------
// SetOutput() QrySensor()  - ������ ���������� �� ������ �����
//
// ��������� �������� ��������
// num - ������������� �������
// on_state ��������� ��������� ������� - ����������/ �������  (true/false)
void KAVT::SetOutput(unsigned char num,BOOL onstate,BOOL fForced)
{
unsigned char pinstate;

	 if (onstate) pinstate = on_state;
		  else pinstate = off_state;

	EnterCriticalSection(&outBlockCr);
	OutBlock->Size   = szOpin;
	if (fForced && ((num == pnraspred3_id) || (num == kl_blokir_id)) && (onstate == FALSE))
	{
		OutBlock->Data[DataIDOffs] = opinidforced;
	}
		else OutBlock->Data[DataIDOffs]   = opinid;
	OutBlock->Data[DataBodyOffs] =  num;
	OutBlock->Data[DataBodyOffs+1] =  pinstate;
	AddToOutBuff();
  LeaveCriticalSection(&outBlockCr);
	
}

//---------------------------------------------------------------------------
// ������ ���������:
// - ����������� ��� ����������� ������� 
// a_sign - ��� ������� true/false - ����������/����������
// num - ������������� �������
// ������� �������� ��������� � ��������� ������� � ���������������(�������) num �
// ������� qrywaittime (� ��)
// ����������:  0, ���� � ������� ��������� ������ ���������  � ��� ����������
//                        -1, �� ��������� ���������  � ������� ��������� 
//                        -2, ������ �������� �� ��������
//
int KAVT::QrySensor(BOOL a_sign, unsigned char num,unsigned int qrywaittime) 
{
int res = 0;
//
  EnterCriticalSection(&QrySensorCr);
  EnterCriticalSection(&outBlockCr);
	OutBlock->Size   = szSensorqry;
	OutBlock->Data[DataIDOffs] = sensorqry;

	if (a_sign)     OutBlock->Data[DataBodyOffs] =  1;
		 else OutBlock->Data[DataBodyOffs] =  0;
//
	OutBlock->Data[DataBodyOffs+1] =  num;
	AddToOutBuff();
	LeaveCriticalSection(&outBlockCr);

//
                   ResetEvent(QrySensorEvent);
                   if (qrywaittime)
                   {   
	if (a_sign) 
                     { 
	     qrystate = QryASensor;
                     }
	     else 
 	     { 
                   	     	qrystate = QryDSensor;

	     } 
                   qryidx = num;
                  } 
                     else qrystate = QryOff;
  LeaveCriticalSection(&QrySensorCr);
//
//
  if (qrywaittime)
  {
     switch(WaitForSingleObject(QrySensorEvent,qrywaittime))
     {
      case WAIT_OBJECT_0:
                     break;
      case WAIT_TIMEOUT:
	res = -1;
	break;
      default: res = -2;

     }
     EnterCriticalSection(&QrySensorCr);
     qrystate = QryOff;
     LeaveCriticalSection(&QrySensorCr);
   }

  return res;
}
//---------------------------------------------------------------------------
// ������������� ������� ��������� ������� ������� ��������� � ��������� ����������� �
// ����������� �������� � �� 
// ���� ��������� �� ����� - �� ���������������� �������� ������ NULL
void KAVT::SetProcedures(ASENSOR_PROC asproc,                                             DSENSOR_PROC dsproc,
						 INFO_PROC infoproc,
						 ALARM_PROC alarmproc,
						 VPROCPV datablockproc,
						 DSENSOR_PROC outputproc,
						 VPROCPV errorreportproc)
{
     EnterCriticalSection(&QrySensorCr);
     ASensorProc = asproc;
     DSensorProc = dsproc;
     infoMessageProc = infoproc;
     alarmProc = alarmproc;
     dataBlockProc = datablockproc;
     OutputProc = outputproc;
     errorReportProc = errorreportproc;
     LeaveCriticalSection(&QrySensorCr);
}
//---------------------------------------------------------------------------
// ������������� ������� ��������� ������� ������� ��������� �� ������
// ���� ��������� �� ����� - �� ���������������� �������� ������ NULL
void KAVT::setErrorMessageProcedure(ERRORMESSAGE_PROC  emsgproc)
{
     errorMessageProc = emsgproc;
 }
//---------------------------------------------------------------------------
//
// ������������� ������, ������������ ��������� ����������� ���������� 
// �� resetDuration ��
void KAVT::setMotorReset(unsigned short resetDuration) 
{
  EnterCriticalSection(&outBlockCr);
	OutBlock->Size   = szResetMotor;
	OutBlock->Data[DataIDOffs] = resetMotor;
	OutBlock->Data[DataBodyOffs] =  resetDuration & 0xFF;
	OutBlock->Data[DataBodyOffs+1] =  resetDuration >> 8;
	AddToOutBuff();
    LeaveCriticalSection(&outBlockCr);
}
//---------------------------------------------------------------------------
// ���������� ������ �� ���������:
// - ��������� ������ ����������, ����� dataID = DEVICENUMBER_ID
// - ������ �� �����������, ����� dataID = VERSION_ID
//
void KAVT::QryInfo(unsigned int dataID)
{
USHORT size = 0;
UCHAR ID;

  if (dataID == DEVICENUMBER_ID) 
  {
	  size = szDeviceNumberQry;
	  ID = deviceNumberQryId;
  }
	  else   if (dataID == VERSION_ID)
				{
					size = szVersionQry;
					ID = versionQryId;
				}
  EnterCriticalSection(&outBlockCr);
	OutBlock->Size   = size;
	OutBlock->Data[DataIDOffs] = ID;
	if (size) AddToOutBuff();
  LeaveCriticalSection(&outBlockCr);
} 
//------------------------------------------------------------------------------------------------------------------
void KAVT::sendAlarmMessage(unsigned char number) 
{
  EnterCriticalSection(&outBlockCr);
	OutBlock->Size   = szAlarmMsg;
	OutBlock->Data[DataIDOffs] =   alarmMsgId;
	OutBlock->Data[DataIDOffs+1] = number;
	AddToOutBuff();
    LeaveCriticalSection(&outBlockCr);
}
//------------------------------------------------------------------------------------------------------------------
// ������ dataBlock �������������� ������
// ������� �������� ��������� ������� qrywaittime (� ��)
// ����������:  0, ���� � ������� ��������� ������ ���������  � ��� ����������
//                        -1, �� ��������� ���������  � ������� ��������� 
//                        -2, ������ �������� �� ��������
//
int KAVT::sendQryDataBlockMessage(unsigned int qrywaittime) 
{
    return sendQryDataBlockMessage(false,qrywaittime);
}
//------------------------------------------------------------------------------------------------------------------
// ������ dataBlock 
// ������� �������� ��������� ������� qrywaittime (� ��)
// ���� fNewFormat = false, ���������� ���� �������������� ������, ����� - ����� ���������
// ����������:  0, ���� � ������� ��������� ������ ���������  � ��� ����������
//                        -1, �� ��������� ���������  � ������� ��������� 
//                        -2, ������ �������� �� ��������
//
int KAVT::sendQryDataBlockMessage(bool fNewFormat, unsigned int qrywaittime) 
{
unsigned int res = 0;
//
  EnterCriticalSection(&outBlockCr);
	OutBlock->Size   = szdataBlockQry;
                     if (fNewFormat == false)	OutBlock->Data[DataIDOffs] =   dataBlockMessageId ;
                           else OutBlock->Data[DataIDOffs] =   dataBlockMessageNewId ;
	AddToOutBuff();
    LeaveCriticalSection(&outBlockCr);
//
  ResetEvent(QryDataBlockEvent);
  if (qrywaittime)
  {
     switch(WaitForSingleObject(QryDataBlockEvent,qrywaittime))
     {
      case WAIT_OBJECT_0:
                     break;
      case WAIT_TIMEOUT:
	res = -1;
	break;
      default: res = -2;

     }
  }
      return res;
}

