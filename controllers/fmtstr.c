﻿// КОДИРОВКА UTF-8 (без BOM)---------------------------------------------------------------------------


#include "fmtstr.h"



static void outputFnk(char ch, char **buf, unsigned int *sizebuf) {
	if (*sizebuf) {
		(*sizebuf)--;
		**buf = ch;
		(*buf)++;
	};

}
// --------------------------------------------------------------------

// [in] outputFnk - Output function where characters to be printed are
// sent
// Params (callback):
// ch - Character to be printed
// [in] number    - Number to be printed
// [in] base      - Base when printing number (2-16)
//

static void printNum(void(*outputFnk)(char ch, char** b, unsigned int *szbuf),
	unsigned int number, unsigned int base, char **outbuf, unsigned int *sizebuf) {
	char *p;
	char buf[33];
	char hexChars[] = "0123456789abcdef";

	p = buf;

	do {
		*p++ = hexChars[number % base];
	}
	while (number /= base);

	do {
		outputFnk(*--p, outbuf, sizebuf);
	}
	while (p > buf);
}

// --------------------------------------------------------------------
void fmtstrcnvcpy(char* buf,unsigned int bufsz, const char *fmt,va_list ap) 
{
	char *p;
	char ch;
	unsigned long ul;
	unsigned char lflag;
	unsigned int *sizebuf = &bufsz;
//
	for (; ; ) {
		while ((ch = *fmt++) != '%') {
			outputFnk(ch, &buf, sizebuf);
			if (ch == '\0')	return;


		}lflag = 0;

	reswitch:

		switch(ch = *fmt++) {
		case '\0':
			return;

		case 'l':
			lflag = 1;
			goto reswitch;

		case 'c':
			ch = va_arg(ap, unsigned int);
			outputFnk(ch & 0x7f, &buf, sizebuf);
			break;

		case 's':
			p = va_arg(ap, char*);
			while ((ch = *p++) != 0)
				outputFnk(ch, &buf, sizebuf);
			break;

		case 'd':
			ul = lflag ? va_arg(ap, long) : va_arg(ap, unsigned int);
			if ((long)ul < 0) {
				outputFnk('-', &buf, sizebuf);
				ul = -(long)ul;
			}
			printNum(outputFnk, ul, 10, &buf, sizebuf);
			break;

		case 'o':
			ul = va_arg(ap, unsigned int);
			printNum(outputFnk, ul, 8, &buf, sizebuf);
			break;

		case 'u':
			ul = va_arg(ap, unsigned int);
			printNum(outputFnk, ul, 10, &buf, sizebuf);
			break;

		case 'p':
			outputFnk('0', &buf, sizebuf);
			outputFnk('x', &buf, sizebuf);
			lflag = 1;
			// fall through

		case 'x':
			ul = va_arg(ap, unsigned int);
			printNum(outputFnk, ul, 16, &buf, sizebuf);
			break;

		default:
			// outputFnk('%');
			if (lflag)
				outputFnk('l', &buf, sizebuf);
			outputFnk(ch, &buf, sizebuf);
		}
	}
}

//
