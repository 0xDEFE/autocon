﻿// КОДИРОВКА UTF-8 (без BOM)---------------------------------------------------------------------------

#include "motorctrl2.h"

const unsigned int getCmdByStateTable[] =
{
 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
10,11,12,13,14,15,16,17,18,19,
20,21,22,23,22,23,24, 0, 0, 0,
30,31,0  
};


motorctrl::motorctrl(unsigned char master_Addr, unsigned char slave_Addr, VPROCV closewaterproc) {
	outcomeblk = (unsigned char*)malloc(blksize);
	incomeblk = (unsigned char*)malloc(blksize);
	hAnswEvent = CreateEvent(NULL, MANUAL_RESET_EVENT, true, NULL);
	// ручной сброс,Signaled
	resultError = 0;
	OnDataReceived = NULL;
	masterAddr = master_Addr;
	slaveAddr = slave_Addr;


	InitializeCriticalSection(&motioncr);
	log_proc = NULL;
	InitializeCriticalSection(&LogCr);
                     closeWaterProc = closewaterproc;
                     variableInit();
}
// ----------------------------------------------------------------------

motorctrl::~motorctrl() {
	if (incomeblk)
		free(incomeblk);
	if (outcomeblk)
		free(outcomeblk);
	CloseHandle(hAnswEvent);
	DeleteCriticalSection(&motioncr);
	DeleteCriticalSection(&LogCr);
}
// ----------------------------------------------------------------------
void motorctrl::variableInit()
{
	motionState = inital;
                     nextState = inital;
                     fMotionCompleted = true;
					 v1271Parameter = INIT;
					 cycleType = none;
   //					 defaultVelocity = currentVelocity = 2000;

}
// ----------------------------------------------------------------------
// если setflag установлен сосчитанную контрольную сумму
// записываем в поле CS заголовка
//
unsigned char motorctrl::calcrc(tBlock *pblock, BOOL setflag) {
	unsigned char *pb;
	unsigned char counter, sum;

	counter = pblock->telegramHdr.DatL+sizeof(tTelegramHdr); // - 7;
	sum = 0;
	pb = (unsigned char*)pblock;
	if (setflag)
		pblock->telegramHdr.CS = 0;
	while (counter--) {
		sum += *pb;
		pb++;
	}
	if (setflag) {
		sum = ~sum;
		pblock->telegramHdr.CS = sum + 1;
	}
	return sum;
}
// ----------------------------------------------------------------------

// command-telegram
//
void motorctrl::send(unsigned char *databuf, unsigned char datalng,
	unsigned char ServiceNum) {
	unsigned char *pdst, i;
	tBlock *pb = (tBlock*)outcomeblk;

	pdst = pb->data;
	for (i = 0; i < datalng; i++)
		pdst[i] = databuf[i];

	pb->telegramHdr.StZ = 2;
	pb->telegramHdr.DatL = pb->telegramHdr.DatLW = datalng; // + 7;
	pb->telegramHdr.Ctrl = 0;
	pb->telegramHdr.Service = ServiceNum;
	pb->telegramHdr.AdrS = masterAddr;
	pb->telegramHdr.AdrE = slaveAddr;
	//
	/* pb->telegramHdr.AdrES1 = 7;
	pb->telegramHdr.AdrES2 = 0;
	pb->telegramHdr.AdrES3 = 0;
	pb->telegramHdr.AdrES4 = 0;
	pb->telegramHdr.AdrES5 = 0;
	pb->telegramHdr.AdrES6 = 0;
	pb->telegramHdr.AdrES7 = 0;
	 */
	// pb->telegramHdr.PaketN = 0;

	//

	calcrc(pb, true);

	ethservice->SetOutputData(outcomeblk, pb->telegramHdr.DatL+sizeof
		(tTelegramHdr) /* -7 */ );
}

// ----------------------------------------------------------------------

int motorctrl::init(int index,cDataTransfer *pDataTransfer)
{
	int result;
	ethservice = new LANMOTORSERVICE(index,pDataTransfer);
	if (ethservice)
	{
		ethservice->setRcvProc(&unPack);
		ethservice->StartWork();
		return 0;
	}
	else return -1;

}
// ----------------------------------------------------------------------
int motorctrl::init1()
{
int result;
unsigned short value;// = 0;  // размерность short !!!
//		result = cmd_writeparam(1,1350,value);
//		if (result >= 0)
//		{
			value = 1;
			result = cmd_writeparam(1,1350,value);
			//if (result >= 0)
                        //                                      {
                      
                                           	    if  (result >= 0) result = 0;
                      //                                        }
//		}
		return result;
}

// ----------------------------------------------------------------------
void motorctrl::tick()
{
	ethservice->Tick();
}

// ----------------------------------------------------------------------
void motorctrl::setLogProcedure(LOGPROC p)
{
	log_proc = p;
}
// ----------------------------------------------------------------------
#define szlogstr   256

void motorctrl::LOG(char *fmt, ...) {
	va_list ap;
	char *buf, *bufptr;

	if (log_proc) {
		buf = (char*)malloc(szlogstr);
		if (buf) {
			memset(buf, 0, szlogstr);
			sprintf(buf, "%d: ", GetTickCount_());
			bufptr = &buf[strlen(buf)];
			va_start(ap, fmt);
			fmtstrcnvcpy(bufptr, szlogstr - strlen(buf) - 3, fmt, ap);
			va_end(ap);
			//strcat("buf", "\n");
			EnterCriticalSection(&LogCr);
			log_proc(buf);
			LeaveCriticalSection(&LogCr);
			free(buf);

		}
	}
}
// ----------------------------------------------------------------------
char *motorctrl::getErrorCodeString(enum CYCLEERROR c)
{
		  switch(c)
		  {
			 case   ERROR_ACTION_DENIED: return "ERROR_ACTION_DENIED";
			 case ERROR_MOTION_NOT_COMPLETED:  return "ERROR_MOTION_NOT_COMPLETED";
			 case ERROR_SET_CMD:                                return "ERROR_SET_CMD";
			 case ERROR_GET_CMD_STATUS:                return "ERROR_GET_CMD_STATUS";
			 case ERROR_SET_HANDCMD:                      return "ERROR_SET_HANDCMD";
			 case ERROR_HANDMOTION_NOT_COMPLETED: return  "ERROR_HANDMOTION_NOT_COMPLETED";
			 case ERROR_GET_HANDCMD_STATUS:      return  "ERROR_GET_HANDCMD_STATUS";
			 case ERROR_WRONG_TROLLEY_POS:        return "ERROR_WRONG_TROLLEY_POS";
			 default: return "UNKNOWN ERROR CODE";
		  }
}
// ----------------------------------------------------------------------

void motorctrl::errorLog(ERRORMSG e)
{
	LOG("%s, code1 = %d, code2 = %d, code3 = %d",getErrorCodeString(e.errorNumber),e.code1,e.code2,e.code3);
}
// ----------------------------------------------------------------------
void __fastcall motorctrl::unPack(int umuIdx,void* data, int size) {
	tBlock *pb = (tBlock*)data;
	unsigned char dataSize;
	unsigned char i;
	unsigned char *p;

	// if(OnDataReceived) OnDataReceived(UMUIdx, data,size);
	if (size >= sizeof(tTelegramHdr)) {
		if (calcrc(pb, false) == 0) {
			if (size >= pb->telegramHdr.DatL+sizeof(tTelegramHdr)) {
				if (pb->data[0] == 0) {
					dataSize = pb->telegramHdr.DatL - 3; // dataHeader
					if (dataSize <= resultBufferSz) {
						if (resultBuffer) {
							resultBufferSz = dataSize;
							p = (unsigned char*)resultBuffer;
							for (i = 0; i < dataSize; i++)
								* p++ = pb->data[i + 3];
							// if(OnDataReceived) OnDataReceived(UMUIdx, data,size);
						}
						else
							resultBufferSz = 0;
					}
					else
					{
					 LOG("unPack: принят заголовок: StZ = 0x%x, CS = 0x%x, DatL = 0x%x, DatLW = 0x%x, - запрашивали 0x%x,\
 Ctrl = 0x%x, Service = 0x%x, AdrS = 0x%x,AdrE = 0x%x",\
					 pb->telegramHdr.StZ,\
					 pb->telegramHdr.CS,\
					 pb->telegramHdr.DatL,\
					 pb->telegramHdr.DatLW,\
					 resultBufferSz,\
					 pb->telegramHdr.Ctrl,\
					 pb->telegramHdr.Service,\
					 pb->telegramHdr.AdrS,\
					 pb->telegramHdr.AdrE);
					 if (pb->telegramHdr.DatL < 10)
					 {
					   LOG("0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x, 0x%x",\
					   pb->data[3],\
					   pb->data[4],\
					   pb->data[5],\
					   pb->data[6],\
					   pb->data[7],\
					   pb->data[8],\
					   pb->data[9],\
					   pb->data[10],\
					   pb->data[11],\
					   pb->data[12]);
					 }
					 resultError = ERROR_OUTBUFFER_TOO_SMALL;
				  }
				}
				else {
					resultError = 0xFFFF0000 | pb->data[0];
					// error in status byte
					if (pb->data[0] == 1) {
						if (resultBuffer) {
							p = (unsigned char*)resultBuffer;
							for (i = 0; i < pb->telegramHdr.DatL - 3; i++)
								*p++ = pb->data[i + 3];
						}
					}
				}
			}
			else
				resultError = ERROR_TOO_SMALL_DATA2;
		}
		else
			resultError = ERROR_BAD_CRC;

	}
	else
		resultError = ERROR_TOO_SMALL_DATA1;

   SetEvent(hAnswEvent);
}

// ----------------------------------------------------------------------
// возвращает:
// - положительное знение (f_write==false) - значение параметра
// - отрицательное - код ошибки
//
// paramtype = 0/1 S/P
int motorctrl::cmd_parameter(unsigned char paramtype,
	unsigned short paramnumber, BOOL f_write, unsigned char *dstBuf, unsigned int dstBufSize,
	unsigned int value, unsigned char valuesz) {
	unsigned char b[sizeof(tMasterDataHdr) + 4];
	unsigned char const2;

	if (paramtype) { // P
		const2 = 0x80;
	}
	else {
		const2 = 0;
	}

	if ((WaitForSingleObject(hAnswEvent, 5000) == WAIT_TIMEOUT))
		return ERROR_SERVICE_BUSY;
	ResetEvent(hAnswEvent);

	resultError = ERROR_NO;
	resultBuffer = (void*)dstBuf;
	//
	b[0] = 0x3C;
	b[1] = slaveAddr;
	b[2] = 0; // param type
	b[3] = paramnumber & 0xFF;
	b[4] = (paramnumber >> 8) & 0x7F | const2;

	if (f_write == false) {
		resultBufferSz = dstBufSize;
		send(b, sizeof(tMasterDataHdr), ReadService);
	}
	else {
		b[5] = value & 0xFF;
		b[6] = (value >> 8) & 0xFF;
		if (valuesz == 4) {
			b[7] = (value >> 16) & 0xFF; ;
			b[8] = (value >> 24) & 0xFF;
			send(b, sizeof(tMasterDataHdr) + 4, WriteService);
		}
		else
			send(b, sizeof(tMasterDataHdr) + 2, WriteService);
	}
	//
	switch(WaitForSingleObject(hAnswEvent, 5000)) {
	case WAIT_OBJECT_0: {
			if (resultError == 0)
				return(int)resultBufferSz;
			else
				return resultError;
		}
	case WAIT_TIMEOUT: {
			SetEvent(hAnswEvent);
			return ERROR_NO_RESULT; // не дождались ответа на запрос
		}
	default: {
			SetEvent(hAnswEvent);
			return ERROR_EVENT_WAIT_ERROR;
		}
	}
}

// ----------------------------------------------------------------------
// возвращает:
// - положительное знение - значение параметра
// - отрицательное - код ошибки
//
// paramtype = 0/1 S/P
int motorctrl::cmd_readparam(unsigned char paramtype,
	unsigned short paramnumber, unsigned char *dstBuf, unsigned int dstBufSize) {
	return cmd_parameter(paramtype, paramnumber, false, dstBuf, dstBufSize, 0,
		0);
}
// ----------------------------------------------------------------------
// возвращает:
// - положительное знение - количество скопированных байт
// - отрицательное - код ошибки
//
int motorctrl::cmd_readP0param(unsigned short paramnumber,
	unsigned char *dstBuf, unsigned int dstBufSize) {
	unsigned char b[sizeof(tMasterDataHdr)];

	if ((WaitForSingleObject(hAnswEvent, 0) == WAIT_TIMEOUT))
		return ERROR_SERVICE_BUSY;
	ResetEvent(hAnswEvent);

	resultBuffer = (void*)dstBuf;
	resultBufferSz = dstBufSize;
	resultError = ERROR_NO;

	//
	b[0] = 0x3C;
	b[1] = slaveAddr;
	b[2] = 0; // param type
	b[3] = paramnumber & 0xFF;
	b[4] = (paramnumber >> 8) | 0x1000;
	send(b, sizeof(tMasterDataHdr), ReadService);
	//
	switch(WaitForSingleObject(hAnswEvent, 5000)) {
	case WAIT_OBJECT_0: {
			if (resultError == 0)
				return(int)resultBufferSz;
			else
				return resultError;
		}
	case WAIT_TIMEOUT: {
			SetEvent(hAnswEvent);
			return ERROR_NO_RESULT; // не дождались ответа на запрос
		}
	default: {
			SetEvent(hAnswEvent);
			return ERROR_EVENT_WAIT_ERROR;
		}
	}
}

// -----------------------------------------------------------------------------
// paramtype = 0/1 S/P
int motorctrl::cmd_writeparam(unsigned char paramtype,
	unsigned short paramnumber, unsigned short value) {
	return cmd_parameter(paramtype, paramnumber, true, NULL, 0, (unsigned int)
		value, 2);
}

// paramtype = 0/1 S/P
int motorctrl::cmd_writeparam(unsigned char paramtype,
	unsigned short paramnumber, unsigned int value) {
	return cmd_parameter(paramtype, paramnumber, true, NULL, 0, value, 4);
}
int motorctrl::cmd_writeparam(unsigned char paramtype,
	unsigned short paramnumber, unsigned int short value, unsigned char *errorcodebuf,
	unsigned char errorcodebuf_sz) {
	return cmd_parameter(paramtype, paramnumber, true, errorcodebuf,
		errorcodebuf_sz, value, 2);
}

int motorctrl::cmd_writeparam(unsigned char paramtype,
	unsigned short paramnumber, unsigned int value, unsigned char *errorcodebuf,
	unsigned char errorcodebuf_sz) {
	return cmd_parameter(paramtype, paramnumber, true, errorcodebuf,
		errorcodebuf_sz, value, 4);
}
// ----------------------------------------------------------------------
int motorctrl::getVersion(char *pdststring) {
	char b[255], *pstr;
	int res;

	res = cmd_readparam(0, 30, b, 254);
	if (res < 0)
		return res;
	strcpy(pdststring, &b[4]);
	pdststring[res - 4] = 0; //
	return 0;

}

// ----------------------------------------------------------------------
int motorctrl::getControllerType(char *pdststring) {
	char b[255], *pstr;
	int res;

	res = cmd_readparam(0, 140, b, 254);
	if (res < 0)
		return res;
	strcpy(pdststring, &b[4]);
	pdststring[res - 4] = 0; //
	return 0;
}

// ----------------------------------------------------------------------
// 1 - Ready
// 0 - Not ready
// отрицательное значение - код ошибки от cmd_readparam (первый или второй вызов)
int motorctrl::isReady(void) 
{
int res;
    if (1)
    {
        USHORT a,b;
		res = cmd_readparam(1,115,(UCHAR*)&a, 2);
		if (res < 0) return res;
		res = cmd_readparam(1,1661,(UCHAR*)&b, 2);
        if (res < 0) return res;
        if (((a & 0x8000) == 0x8000) && ((b & 0x8000) == 0x8000)) return 1;
        return 0; 
    }
        else
        {  
           char b[255];
	res = cmd_readparam(0, 95, b, 254);
	if (res < 0)	return res;
	if (strncmp(&b[4], "A0012", 5) == 0)	return 1;
	if (strncmp(&b[4], "A0013", 5) == 0)	return 1;
	return 0;
        }
}
// ----------------------------------------------------------------------
// 1 - Ready
// 0 - Not ready
// отрицательное значение - код ошибки от cmd_readparam
int motorctrl::driverIsReady(void) 
{
int res;
    EnterCriticalSection(&motioncr);
    res = isReady();
    LeaveCriticalSection(&motioncr);
    return res;
}

// ----------------------------------------------------------------------
// 0..4 - Language number
// отрицательное значение - код ошибки от cmd_readparam
int motorctrl::getLanguage(void) {
	unsigned short b;
	int res;

	res = cmd_readparam(0, 265, (unsigned char*) & b, 2);
	if (res < 0)
		return res;
	return(int)b;
}

// ----------------------------------------------------------------------
// languageNumber - 0..4
// отрицательное значение - код ошибки от cmd_readparam
int motorctrl::setLanguage(unsigned char languageNumber) {
	unsigned short b;
	int res;

	res = cmd_writeparam(0, 265, (unsigned short)languageNumber);
	if (res < 0)
		return res;
	return 0;
}

// отрицательное значение - код ошибки от cmd_readparam
// ----------------------------------------------------------------------
// 0 - нормальное завершение
// отрицательное значение - код ошибки от cmd_readparam
int motorctrl::getPositioningCtrlWord(unsigned int *pvalue) {
	int res;

	res = cmd_readparam(1, 1450, (unsigned char*)pvalue, 4);
	if (res < 0)
		return res;
	return 0;
}
// ------------------------------------------------------------
// отрицательное значение - код ошибки от cmd_readparam
int motorctrl::setPositioningCtrlWord(unsigned int value) {
	int res;

	res = cmd_writeparam(1, 1450, value);
	if (res < 0)
		return res;
	return 0;
}

// ------------------------------------------------------------
// отрицательное значение - код ошибки от cmd_readparam
int motorctrl::getVelocity(unsigned int *pvalue) {
	int res;

	res = cmd_readparam(1, 1451, (unsigned char*)pvalue, 4);
	if (res < 0)
		return res;
	return 0;
}

// -------------------------------------------------------------
// отрицательное значение - код ошибки от cmd_readparam
int motorctrl::setVelocity(unsigned int value) {
	int res;
	unsigned char error[255];

	res = cmd_writeparam(1, 1451, value, error, 254);
	if (res < 0)
		return res;
	return 0;
}
/*
// отрицательное значение - код ошибки от cmd_readparam
int motorctrl::set_Velocity(unsigned int value)
{
	currentVelocity = value;
	setVelocity(value);
}
*/
//---------------------------------------------------------
// отрицательное значение - код ошибки от cmd_readparam
int motorctrl::getStartAcceleration(unsigned int *pvalue) {
	int res;

	res = cmd_readparam(1, 1452, (unsigned char*)pvalue, 4);
	if (res < 0)
		return res;
	return 0;
}
//--------------------------------------------------------------
int motorctrl::setStartAcceleration(unsigned int value) {
	int res;
	unsigned char error[255];
 //
	res = cmd_writeparam(1, 1452, value, error, 254);
	if (res < 0)
		return res;
	return 0;
}
//---------------------------------------------------------
// отрицательное значение - код ошибки от cmd_readparam
int motorctrl::getStopAcceleration(unsigned int *pvalue) {
	int res;

	res = cmd_readparam(1, 1453, (unsigned char*)pvalue, 4);
	if (res < 0)
		return res;
	return 0;
}
//--------------------------------------------------------------
int motorctrl::setStopAcceleration(unsigned int value) {
	int res;
	unsigned char error[255];
 //
	res = cmd_writeparam(1, 1453, value, error, 254);
	if (res < 0)
		return res;
	return 0;
}


// -------------------------------------------------------------
// возвращает 1  - команда завершена, либо значение handrun
//                     0 - нет завершения, в *pcode - прочитанное
//                     <0 - код ошибки чтения
//
// значение параметра
//
int motorctrl::cmdIsDone(unsigned int *pcode)
{
	unsigned int b;
	int res;
	res = cmd_readparam(1, 1270, (unsigned char*) &b, 4);
	if (res == 4)
	{
		if ((b == 0) || (b == handrun) ) return 1;
		else
		{
			if (pcode )*pcode = (unsigned int) b;
			return 0;
	   }
	}
	else
		return res;
}
// -------------------------------------------------------------
// возвращает 1  - команда завершена
//                       0 - нет завершения, в *pcode - прочитанное
//                     <0 - код ошибки чтения
//
int motorctrl::handCmdIsDone(unsigned int *pcode)
{
	unsigned int b;
	int res;
	res = cmd_readparam(1, 1271, (unsigned char*) &b, 4);
	if (res == 4) {
		if (b == 0) return 1;
		else
		{
		   if (pcode )*pcode = (unsigned int) b;
		   return 0;
		}
	}
	else
		return res;
}
//-------------------------------------------------------------
//
// возвращает 0, если ОК; отрицательное значение - код ошибки
int motorctrl::getIPAddress(unsigned int *pIpAddress) {
	char b[255];
	int res;
	res = cmd_readparam(1, 1531, (unsigned char*) &b, 254);
	if (res == 8)
	{
		  *pIpAddress = (b[4] << 24 ) | (b[5] << 16) & 0xFF0000  | (b[6] << 8) | b[7] ;
	}
	if (res < 0) 	return res;
	if ((res == 0) || (res > 8)) *pIpAddress = 0;
	return 0;
}
// -------------------------------------------------------------
// возвращает 1, если каретка находится в допустимом положении для совершения 
// движения, заданного кодом v
// 0 - если положение не является допустимым,  в *ppos записывается координата в мм
//  отрицательное значение - код ошибки
int motorctrl::isTrolleyValidPos(PARAMETER1VALUE v, unsigned int *ppos)
{
int res = 1;
int pos;
 
    switch (v)   
    {
        case LEFTMINUP:
        case RIGHTMINUP:
        case LEFTMAXUP: 
        case RIGHTMAXUP:
                                            res =  getUpperTrolleyPosition(&pos);
		   break;
        case  LEFTMINBOT:
		case RIGHTMINBOT:
        case LEFTMAXBOT: 
        case RIGHTMAXBOT:
                                            res =  getBottomTrolleyPosition(&pos);
    }  
    if (res >= 0)
    {
        switch (v)   
        {
            case LEFTMINUP:
            case LEFTMINBOT:
											 if (pos >= maxLeftTrolleyPosition) res = 1;
												 else
												 {
												   if (ppos)
												   {
													 *ppos = pos;
												   }
												   res = 0;
												 }
											 break;
			case RIGHTMINUP:
												 if (pos < maxRightUpTrolleyPosition) res = 1;
													 else
													 {
													  if (ppos)
													  {
														 *ppos = pos;
													  }
													  res = 0;
													 }
												 break;

			case RIGHTMINBOT:
												 if (pos < maxRightBotTrolleyPosition) res = 1;
													 else
													 {
													  if (ppos)
													  {
														 *ppos = pos;
													  }
													  res = 0;
													 }
												 break;
			case LEFTMAXUP:
			case LEFTMAXBOT:
											 if (pos > maxLeftTrolleyPositionWithReserve) res = 1;
												 else
												 {
												   if (ppos)
												   {
														 *ppos = pos;
												   }
												   res = 0;
												 }
		   break;
			case RIGHTMAXUP:
											 if (pos < maxRightUpTrolleyPositionWithReserve) res = 1;
												 else
												 {
													  if (ppos)
													  {
														 *ppos = pos;
													  }
													 res = 0;
												 }
											break;

			case RIGHTMAXBOT:
												 if (pos < maxRightBotTrolleyPositionWithReserve) res = 1;
												 else
												 {
													  if (ppos)
													  {
														 *ppos = pos;
													  }
													 res = 0;
												 }
											break;
	   }
	}
	return res;
}

//-------------------------------------------------------------
// возвращает 1, если каретка находится в допустимом положении
// 0 - если положение не является допустимым,  в *ppos записывается координата в мм
//  отрицательное значение - код ошибки
int motorctrl::isTrolleyValidPos(bool fBottomTrolley, unsigned int *ppos)
{
int res;
int pos;
	if (fBottomTrolley) res =  getBottomTrolleyPosition(&pos);
		else res =  getUpperTrolleyPosition(&pos);
	if (res >= 0)
	{
		res = 0;
		if (fBottomTrolley)
		{
			if ((pos > (maxLeftTrolleyPosition-10))  && (pos < maxRightBotTrolleyPosition) ) res = 1;
			  else   if (ppos) *ppos = pos;
		}
			else
			{
				if ((pos > (maxLeftTrolleyPosition-10))  && (pos < maxRightUpTrolleyPosition) ) res = 1;
					else   if (ppos) *ppos = pos;
            }
	}
	return res;
}
//
//-------------------------------------------------------------
int motorctrl::setParameter1(PARAMETER1VALUE v, ERRORMSG *perrorMsg)
{
int res;
unsigned char error[255];
unsigned int b;

                    LOG("motorctrl: Set P1 = %d",v); 

	if (perrorMsg)
	{
		perrorMsg->errorNumber = ERROR_NO;
		perrorMsg->code1 = 	perrorMsg->code2 = perrorMsg->code3 = 0;
	}
	 if (motionState <= handrun)
	 {
                          b = 0;
                          res = isTrolleyValidPos(v,&b);
                          if (res == 1)
                          {
		res = 1;
		if (v != STOP) res = handCmdIsDone(&b);
		if (res > 0)
		{
											   res = ERROR_NO;
											   if (motionState == inital)
											   {
													res = setCmd(handrun);  // вкл. 1-й такт - ручное управление
													if (res < ERROR_NO)
													{
													   if (perrorMsg)
													   {
														perrorMsg->errorNumber = ERROR_SET_CMD;
														perrorMsg->code1 = (int)handrun;
														perrorMsg->code2 = res;
														errorLog(*perrorMsg);
													   }
													   res =  ERROR_SET_CMD;
													 }
														 else motionState = handrun;
												}
												 if ((res == ERROR_NO) && ((v1271Parameter != STOP) ||  (v != STOP)))
												 {
                                                                                                                                                                                                                                                                                b = (unsigned int) v;
													  res = cmd_writeparam(1, 1271, b,error,254);
													  if (res < ERROR_NO)
													  {
															   if (perrorMsg)
															   {
																 perrorMsg->errorNumber = ERROR_SET_HANDCMD;
																 perrorMsg->code1 = (int)handrun;
																 perrorMsg->code2 = res;
																 errorLog(*perrorMsg);
															   }
															   res =  ERROR_SET_HANDCMD;
													  }
                                                                                                                                                                                                                                                                                     else v1271Parameter = v;				
                                                                                                                                                                                                                                                           }                                                                    
		}
		   else if (res == 0)
				{
					if (perrorMsg)
					{
						perrorMsg->errorNumber = ERROR_HANDMOTION_NOT_COMPLETED;
						perrorMsg->code1 = b;
						errorLog(*perrorMsg);

					}
					res = ERROR_HANDMOTION_NOT_COMPLETED;
				}
					else
					{
						 if (perrorMsg)
						 {
							 perrorMsg->errorNumber = ERROR_GET_HANDCMD_STATUS;
							 perrorMsg->code1 = res;
							 errorLog(*perrorMsg);
						 }
						 res = ERROR_GET_HANDCMD_STATUS;
					}
                           }
                               else 
                               { // trolley position is not valid
		 if (perrorMsg)
		 {
		     perrorMsg->errorNumber = ERROR_WRONG_TROLLEY_POS;
		     perrorMsg->code1 = motionState;
		     perrorMsg->code2 = (int) v;
 	                          perrorMsg->code3 = (int) b;
                                               errorLog(*perrorMsg);
		 }
		res = ERROR_WRONG_TROLLEY_POS;
                               }  
	  }
		   else
		   {
			 if (perrorMsg)
			 {
				perrorMsg->errorNumber = ERROR_ACTION_DENIED;
				perrorMsg->code1 = motionState;
                                                                                   errorLog(*perrorMsg);
			 }
			res = ERROR_ACTION_DENIED;
		   }
   return res;
}
//---------------------------------------------------------------------
int motorctrl::writeParameter1(PARAMETER1VALUE v, ERRORMSG *perrorMsg)
{
   EnterCriticalSection(&motioncr);
   setParameter1(v,perrorMsg);
   LeaveCriticalSection(&motioncr);
}
//---------------------------------------------------------------------
int motorctrl::setCmd(unsigned int b) {
int res = 0;
unsigned char error[255];
   LOG("motorctrl: Set P0 = %d",b);
    res = cmd_writeparam(1, 1270, b,error,254);
    return res;
 }
//---------------------------------------------------------------------
// 
int motorctrl::setShift(BOOL fBottomTrolley,int b) 
{
int res = 0;
unsigned char error[255];
    if (fBottomTrolley)
     { 
		LOG("motorctrl: Set Shift (1273) = %d",b);
		res = cmd_writeparam(1, 1273,(unsigned int)b,error,254);
	 }
		 else
		 {
			LOG("motorctrl: Set Shift (1272) = %d",b);
			res = cmd_writeparam(1, 1272,(unsigned int)b,error,254);
         }
    return res;
 }
//---------------------------------------------------------------------
// отрицательное значение - код ошибки от cmd_readparam
// если нет ошибок, возвращаем 0 и в *pShift - ранее записанное смещение
int motorctrl::getShift(BOOL fBottomTrolley,int *pShift) 
{
int res = 0;
unsigned int b;

    if (fBottomTrolley)
     { 
     res = cmd_readparam(1,1273,(unsigned char*)&b, 4);
     }
         else
         {
             res = cmd_readparam(1,1272,(unsigned char*)&b, 4);
         }
    if (res > 0) 
    {
       res = 0;
       if (pShift)  *pShift = b;
    }
    return res;
 }


//---------------------------------------------------------------------
// отрицательное значение - код ошибки от cmd_readparam
// если нет ошибок, возвращаем 0 и в *pPosition - координата верхней каретки
int motorctrl::getUpperTrolleyPosition(int *pPosition)
{
	unsigned int b;
	int res;

	res = cmd_readparam(1,1670,(unsigned char*)&b, 4);
	if (res < 0)	return res;
                     if (pPosition)  *pPosition = ((int)b) /10000;
                     return 0;
}
//---------------------------------------------------------------------
// отрицательное значение - код ошибки от cmd_readparam
// если нет ошибок, возвращаем 0 и в *pPosition - координата нижней каретки
int motorctrl::getBottomTrolleyPosition(int *pPosition)
{
	unsigned int b;
	int res;

	res = cmd_readparam(1,1671,(unsigned char*)&b, 4);
	if (res < 0)	return res;
                     if (pPosition) *pPosition = ((int)b) /10000;
					 return 0;
}
//---------------------------------------------------------------------
// 1 - верхняя каретка находится в допустимом положении
// для запуска сканирования, иначе - 0
// отрицательное значение - код ошибки от cmd_readparam
int motorctrl::isUpperTrolleyInCenter(void)
{
int coord,res;
							 res = getUpperTrolleyPosition(&coord);
							 if (res >= 0)
							 {
								  if ((coord >= maxPositionTrolleyToLeftFromCenter) &&
											 (coord <= maxPositionTrolleyToRightFromCenter)) return 1;
										else return 0;
							 }
							 return res;
}
//---------------------------------------------------------------------

// вход в критическую секцию должен быть уже выполнен
//
void motorctrl::setInitalState(void)    // private
{
	 motionState = inital;
	 nextState = inital;
	 cycleType = none;
}
//---------------------------------------------------------------------
void motorctrl::whenMotionsCompleted(MOTIONRESULT *presult)    // private
{
	if (resultProcedure)  resultProcedure(*presult);
	resultProcedure = NULL;
	setInitalState();
	fMotionCompleted = true ;
}
//-------------------------------------------------------------------------
// при ошибочном завершении функции resultProc будет вызвана
// до ее завершения
void motorctrl::command(CYCLETYPE typ, tCallBack resultProc)
{
int res = ERROR_NO;
MOTIONRESULT r = {cNone,ERROR_NO,0,0,0};
unsigned int b;
//
 r.error.errorNumber = ERROR_NO;
 r.error.code1 =  r.error.code2 = r.error.code3 = 0;
 EnterCriticalSection(&motioncr);
 if (typ != none)
 {
	 if (cycleType == none)
	 {
		 if ((typ == scancycle) || (typ == adjust2) || (typ == adjust3) || (typ == drvPwrOff) || (typ == drvPwrOn) || (typ == caretReturn) )
		 {
			 if (typ == scancycle)
			 {
				 res = setParameter1(PUSK,&r.error);
				 if (res < ERROR_NO)
				 {
					 r.error.errorNumber = res;
					 r.error.code1 = (int)move2;
					 errorLog(r.error);
					 setInitalState();
					 if (resultProc)  resultProc(r);
				 }
					 else
					 {
						 targetState = move15;
						 nextState = move2;
						 cycleType = typ;
					 }
			 }
				 else
						 if (typ == adjust2)
						 {
							 targetState = move21;
							 nextState = move21;
							  cycleType = typ;
						  }
						 else if (typ == adjust3)
						 {
							 targetState = move26;
							 nextState = move21;
							  cycleType = typ;
						  }
							  else if (typ == caretReturn)
								   {
									   targetState = move15;
									   nextState = move15;
									   cycleType = typ;
								   }
							  else
							  {
//                                 команды включения и отключения питания
								  res = cmdIsDone(&b);
								  if (res == 1)
								  {
									  if (typ == drvPwrOn) res = setCmd(pwrOnCmd);
										  else res = setCmd(pwrOffCmd);
									  if (res != ERROR_NO)
									  {
										  r.error.errorNumber = res;
										  errorLog(r.error);
									  }
									  if (resultProc)  resultProc(r);
								  }
									 else
									 {
										 if (res == 0)
										 {
											 r.error.errorNumber = ERROR_MOTION_NOT_COMPLETED;
											 r.error.code1 = (int)motionState;
											 r.error.code2 = b;
											 errorLog(r.error);
											 if (resultProc)  resultProc(r);
										 }
											 else
											 {
												 r.error.errorNumber = ERROR_GET_CMD_STATUS;
												 r.error.code1 = (int)motionState;
												 r.error.code2 = res;
												 errorLog(r.error);
												 if (resultProc)  resultProc(r);
											  }
									 }
							  }
			 if (cycleType != none)      resultProcedure = resultProc;
		 }
			else
			   if (resultProc)  resultProc(r); // если typ!=cmdBereak, то вообще-то это ошибка - typ - содержит ошибочное значение, cycleBreak в т.ч.
	 }
	  else
	  {
		if (typ == cycleType)
		{
		   res = cmdIsDone(&b);
		   if (res == 1)
		   {
			  if (nextState < targetState)  nextState = nextState + 1;
			  resultProcedure = resultProc;
		   }
			 else if (res == 0)
					 {
		 r.error.errorNumber = ERROR_MOTION_NOT_COMPLETED;
		 r.error.code1 = (int)motionState;
						  r.error.code2 = b;
						  errorLog(r.error);
						  if (resultProc)  resultProc(r);
						  setInitalState();
					 }
						   else
						   {
							   r.error.errorNumber = ERROR_GET_CMD_STATUS;
							   r.error.code1 = (int)motionState;
							   r.error.code2 = res;
							   errorLog(r.error);
							   if (resultProc)  resultProc(r);
							   setInitalState();
						  }
		}
		   else if (typ == cycleBreak)
				{
				  res = cmdIsDone(&b);
				  if (res == 0)
				  {
					 res = setCmd(breakcmd);
					 if (res < 0)
					 {
						 r.error.errorNumber = ERROR_SET_CMD;
						 r.error.code1 = motionState;
						 r.error.code2 = (int)breakcmd;
						 r.error.code3 = res;
						 errorLog(r.error);
						 if (resultProc)  resultProc(r);
					 }
								else
								{
									r.error.code1 = motionState;
									if (resultProcedure)
									{
										r.error.errorNumber = ERROR_CMD_CANCELLED;
										resultProcedure(r);
										resultProcedure = NULL;
									}
									r.error.errorNumber = ERROR_NO; // здесь выполняли команду отменить - что и сделано
								}
				   }
					   else if (res == 1)
							{
								r.error.errorNumber = ERROR_NO; // здесь выполняли команду отменить - что и сделано
								r.error.code1 = motionState;
							}
							   else
							   {  // перед посылкой cmdBreak проверяли завершено или нет
								  // текущее движение
								  r.error.errorNumber = ERROR_GET_CMD_STATUS;
								  r.error.code1 = (int)motionState;
								  r.error.code2 = res;
								  errorLog(r.error);
							   }
					if (resultProc)  resultProc(r);
					setInitalState();
					fMotionCompleted = true;
				  }
					  else if (resultProc)  resultProc(r);    // вообще-то это ошибка - typ - содержит ошибочное значение
	  }
 }
  LeaveCriticalSection(&motioncr);
}
//
//-------------------------------------------------------------------------
/*
// при ошибочном завершении функции resultProc будет вызвана
// до ее завершения
void motorctrl::command(BOOL fBottomTrolley,int smesh, tCallBack resultProc)
{
int res = ERROR_NO;
MOTIONRESULT r = {cNone,ERROR_NO,0,0,0};
unsigned int b;

 r.error.errorNumber = ERROR_NO;
 r.error.code1 =  r.error.code2 = r.error.code3 = 0;
 EnterCriticalSection(&motioncr);
//
  if (cycleType == none)
  {
	  res = cmdIsDone(&b);
	  if (res == 1)
	  {
		  if (fBottomTrolley == false)   res = setShift(false,smesh);
			  else res = setShift(false,0);
		  if (res < ERROR_NO)
		  {
				 r.error.errorNumber = ERROR_SET_SHIFT_1272;
				 r.error.code1 = res;
				 errorLog(r.error);
				 whenMotionsCompleted(&r);
		  }
			  else
			  {
				   if (fBottomTrolley == false)   res = setShift(true,0);
					   else res = setShift(true,smesh);
				   if (res < ERROR_NO)
				   {
					   r.error.errorNumber = ERROR_SET_SHIFT_1273;
					   r.error.code1 = res;
					   errorLog(r.error);
						whenMotionsCompleted(&r);
				   }
					   else
					   {
						   res = setCmd(move30);
						   if (res == ERROR_NO)
						   {
							   resultProcedure = resultProc;
							   cycleType = tune;
							   cycleTimer.setTimer();
						   }
							   else
							   {
								   r.error.errorNumber = res;
								   errorLog(r.error);
								   whenMotionsCompleted(&r);
							   }
					   }
			  }
	  }
		  else if (res == 0)
				  {
					  r.error.errorNumber = ERROR_MOTION_NOT_COMPLETED;
					  r.error.code1 = (int)motionState;
					  r.error.code2 = b;
					  errorLog(r.error);
					  whenMotionsCompleted(&r);
				  }
					  else
					  {
						  r.error.errorNumber = ERROR_GET_CMD_STATUS;
						  r.error.code1 = (int)motionState;
						  r.error.code2 = res;
						  errorLog(r.error);
						  whenMotionsCompleted(&r);
					  }
  }
	  else
	  {
		  r.error.errorNumber = ERROR_MOTION_NOT_COMPLETED;
		  r.error.code1 = (int)motionState;
		  r.error.code2 = b;
		  errorLog(r.error);
		  whenMotionsCompleted(&r);
	 }

  LeaveCriticalSection(&motioncr);
}
*/
// -----------------------------------------------------------------------------------------
// при ошибочном завершении функции resultProc будет вызвана
// до ее завершения
void motorctrl::command(BOOL fBottomTrolley, int smesh, tCallBack resultProc)
{
	if (fBottomTrolley) command2(smesh,0,resultProc);
		else command2(0,smesh,resultProc);
}
//-------------------------------------------------------------------------
// при ошибочном завершении функции resultProc будет вызвана
// до ее завершения
void motorctrl::command2(int bottomTrolleyShift, int upperTrolleyShift, tCallBack resultProc)
{
int res = ERROR_NO;
MOTIONRESULT r = {cNone,ERROR_NO,0,0,0};
unsigned int b;

 r.error.errorNumber = ERROR_NO;
 r.error.code1 =  r.error.code2 = r.error.code3 = 0;
 EnterCriticalSection(&motioncr);
//
  if (cycleType == none)
  {
	  res = cmdIsDone(&b);
	  if (res == 1)
	  {
		  res = setShift(false,upperTrolleyShift);
		  if (res < ERROR_NO)
		  {
				 r.error.errorNumber = ERROR_SET_SHIFT_1272;
				 r.error.code1 = res;
				 errorLog(r.error);
				 whenMotionsCompleted(&r);
		  }
			  else
			  {
				   res = setShift(true,bottomTrolleyShift);
				   if (res < ERROR_NO)
				   {
					   r.error.errorNumber = ERROR_SET_SHIFT_1273;
					   r.error.code1 = res;
					   errorLog(r.error);
						whenMotionsCompleted(&r);
				   }
					   else
					   {
						   res = setCmd(move30);
						   if (res == ERROR_NO)
						   {
							   resultProcedure = resultProc;
							   cycleType = tune;
							   cycleTimer.setTimer();
						   }
							   else
							   {
								   r.error.errorNumber = res;
								   errorLog(r.error);
								   whenMotionsCompleted(&r);
							   }
					   }
			  }
	  }
		  else if (res == 0)
				  {
					  r.error.errorNumber = ERROR_MOTION_NOT_COMPLETED;
					  r.error.code1 = (int)motionState;
					  r.error.code2 = b;
					  errorLog(r.error);
					  whenMotionsCompleted(&r);
				  }
					  else
					  {
						  r.error.errorNumber = ERROR_GET_CMD_STATUS;
						  r.error.code1 = (int)motionState;
						  r.error.code2 = res;
						  errorLog(r.error);
						  whenMotionsCompleted(&r);
					  }
  }
	  else
	  {
		  r.error.errorNumber = ERROR_MOTION_NOT_COMPLETED;
		  r.error.code1 = (int)motionState;
		  r.error.code2 = b;
		  errorLog(r.error);
		  whenMotionsCompleted(&r);
	 }

  LeaveCriticalSection(&motioncr);
}
//-------------------------------------------------------------------------
// автоматическое движение каретки должно завершиться в течение времени maxMotionRequiredTime
// иначе записываем команду breakcmd
void motorctrl::motionEngine(void)
{
int res;
MOTIONRESULT r = {0,ERROR_NO,0,0,0};
unsigned int b;
//
	 if ((cycleType != none) && (cycleType != tune))
	 {
		 if (fMotionCompleted == true)
		 {
		   EnterCriticalSection(&motioncr);
			if ((nextState > handrun) && (motionState < nextState) )
			{
					 motionState = getCmdByStateTable[nextState];
					 if (closeWaterProc)
					 {
					  if (((cycleType == scancycle) || (cycleType == adjust3)) && (motionState == targetState)) closeWaterProc();
                     }
					 res = setCmd(motionState);
					 if (res == 0)
					 {
						 cycleTimer.setTimer();
						 fMotionCompleted = false ;
					 }
					 else
					 { // setCmd() competion with an error
						 r.error.errorNumber = ERROR_SET_CMD;
						 r.error.code1 = r.error.code2 = (int)motionState;
						 r.error.code3 = res;
						 errorLog(r.error);
						 r.parameter = cToBeContinued;
						 if (resultProcedure)  resultProcedure(r);
						 resultProcedure = NULL;
					}
		   }
		  LeaveCriticalSection(&motioncr);
		}
		 else
		 {
				EnterCriticalSection(&motioncr);
				 res = cmdIsDone(&b);
				 if (res == 1)
				 {
					 fMotionCompleted = true ;
					 if (nextState < targetState)
					 {
						 r.parameter = cToBeContinued;
						 if (resultProcedure)  resultProcedure(r);
						 resultProcedure = NULL;
					 }
						 else
						 {
							   r.parameter = cLastMotion;
							   if (resultProcedure)  resultProcedure(r);
							   setInitalState();
							   resultProcedure = NULL;
						 }
				 }
					 else  if (res == 0)
						   {
								   if (cycleTimer.getDuration() > maxMotionRequiredTime)
								   { // current motion occurs to be too long
																										  setCmd(breakcmd);
									   r.error.errorNumber = ERROR_MOTION_NOT_COMPLETED;
									   r.error.code1 = (int)motionState;
									   r.error.code2 = b;
									   errorLog(r.error);
									   if (resultProcedure)  resultProcedure(r);
										   setInitalState();
										   fMotionCompleted = true ;
										   resultProcedure = NULL;
								   }
						   }
							   else
							   { // cmdIsDone() competion with an error
									  r.error.errorNumber = ERROR_GET_CMD_STATUS;
									  r.error.code1 = (int)motionState;
									  r.error.code2 = res;
									  errorLog(r.error);
									  if (resultProcedure)  resultProcedure(r);
									  setInitalState();
									  fMotionCompleted = true ;
									  resultProcedure = NULL;
							   }
				LeaveCriticalSection(&motioncr);
		}
	 }
		else if (cycleType == none)
		 { // контроль положения каретки во время длительного ручного перемещения

				 switch(v1271Parameter)
				 {
					 case LEFTMAXUP:
					 case RIGHTMAXUP:
					  if (isTrolleyValidPos(v1271Parameter,&b) == 0)
					  {
						 writeParameter1(STOP,NULL);
//						 setVelocity(defaultVelocity);
					  }
//						  else defineMotorVelocity();
					  break;

					 case LEFTMAXBOT:
					 case RIGHTMAXBOT:
					  if (isTrolleyValidPos(v1271Parameter,&b) == 0)
					  {
						 writeParameter1(STOP,NULL);
//						 setVelocity(defaultVelocity);
					  }
					  break;
				 }

		 }
			else
			{ // контроль за окончанием смещения кареток в режиме настройка
				 EnterCriticalSection(&motioncr);
				 res = cmdIsDone(&b);
				 if (res == 1)
				 {
					 r.parameter = cLastMotion;
					 whenMotionsCompleted(&r);
				 }
					 else if (res == 0)
						  {
							  if (cycleTimer.getDuration() > maxMotionRequiredTime)
							  { // current motion occurs to be too long
								  setCmd(breakcmd);
								  r.error.errorNumber = ERROR_MOTION_NOT_COMPLETED;
								  r.error.code1 = (int)motionState;
								  r.error.code2 = b;
								  errorLog(r.error);
								  whenMotionsCompleted(&r);
							  }
								  else
									  if ((isTrolleyValidPos(false, &b) == 0) || (isTrolleyValidPos(true, &b) == 0))
									  {
										  setCmd(breakcmd);
										  r.parameter = cLastMotion;
										  whenMotionsCompleted(&r);
									  }
						  }
							  else
							  { // cmdIsDone() competion with an error
								  r.error.errorNumber = ERROR_GET_CMD_STATUS;
								  r.error.code1 = (int)motionState;
								  r.error.code2 = res;
								  errorLog(r.error);
								  r.parameter = cLastMotion;
								  whenMotionsCompleted(&r);
							  }
				 LeaveCriticalSection(&motioncr);
											   }
}
//-----------------------------------------------------------------------------------------------------------------------------------
//   записывает состояние кареток двигателей в структуру, задаваемую pDest
//   0 нормальное завершение 
//  -1 ошибка чтения регистров контроллера двигателей 
//  -2 внутренняя ошибка - непредвиденное значение cyleType 
int  motorctrl::getCariagesStatus(tCARIAGESTAT *pDest) 
{
int res = 0;
int uPos;
int bPos;
int status = cariageRest;

    if (pDest)
    {
       EnterCriticalSection(&motioncr);
       switch (cycleType) 
       { 
           case none:
           {

               switch(v1271Parameter)
			   {
                   case LEFTMAXUP:
                   case RIGHTMAXUP:
                   {
                      status = upperCariageMv;
                       break;
                   }
                   case LEFTMAXBOT:
                   case RIGHTMAXBOT:
                   {
                      status = bottomCariageMv;
                      break;
                   }
			 }
           }
           case tune:
           {
               int s;  
                    if (getShift(false,&s) == 0) 
                    {
                        if (s != 0) status = upperCariageMv;
                            else
                            {
                                if (getShift(true,&s) == 0) 
                               {
                                   if (s != 0) status = bottomCariageMv;
                               }
                                   else  res = -1;
                            }
                    }  
                        else  res = -1;   
                    break;
           }
           case	scancycle: 
           case 	adjust2:
           case	adjust3:
           case  caretReturn:
           {
                   if (motionState-1 >= 0) status = cariageSign[motionState-1];
                     

                    break;
           } 
           default: res = -2;
           
       } 
       if (res == 0)  res =  getUpperTrolleyPosition(&uPos);
       if (res == 0)  res =  getBottomTrolleyPosition(&bPos);
       LeaveCriticalSection(&motioncr);
       if (res == 0)
       {
           pDest->status = status;
           pDest->upperCoord = uPos;
           pDest->bottomCoord = bPos;
       }
    } 
    return res;
}

//-----------------------------------------------------------------------------------------------------------------------------------
/*
unsigned int motorctrl::getDefaultVelocity(void)
{
   return defaultVelocity;
}
*/
//-----------------------------------------------------------------------------------------------------------------------------------
/*
// задает скорость движения в зависимости от положения верхней каретки
//  верхняя каретка находится в допустимом положении  для запуска сканирования
//  задается скорость в 2 раза меньше, чем defaultVelocity, иначе - в 2 раза больше
void motorctrl::defineMotorVelocity(void)
{
unsigned int v;
int res;
		 res = isUpperTrolleyInCenter();
		 if (res >=0)
		 {
			  if (res == 1) v = defaultVelocity >> 1;
				  else v = defaultVelocity << 1;
			  if (currentVelocity != v)
			  {
				  currentVelocity = v;
				  setVelocity(v);
				  res = getVelocity(&v);
				  if (res >= 0)	  LOG("MotorVelocity = %d",v);
					  else LOG("Motor velocity defining was completed with an error = %d",res);
			  }
		 }
}
*/
 //--------------------------------------------------------------------------
void motorctrl::parametersLog(void)
{
int res;
unsigned int b;
unsigned short a;

	 res = cmd_readparam(1, 1270, (unsigned char*)&b,4);
	 if (res > 0)
	 {
		LOG("P0(P1270) = %d",b);
	 }
	 res = cmd_readparam(1, 1271, (unsigned char*)&b,4);
	 if (res > 0)
	 {
		LOG("P1(P1271) = %d",b);
	 }

	 res = cmd_readparam(1, 1351, (unsigned char*)&a,2);
	 if (res > 0)
	 {
		LOG("P1(P1351) = %d",a);
	 }
}
//---------------------------------------------------------
int motorctrl::readP1351(void)
{
unsigned short a;
int res;
	 res = cmd_readparam(1, 1351, (unsigned char*)&a,2);
	 if (res >= 0)
	 {
		return a&0xffff ;
	 }
	 return res;
}
