
#ifndef UTILS_H
#define UTILS_H

 #define WIN_BUILDER // Windows C++ Builder
//  #define WIN_VS       // Windows Microsoft Visual Studio C++
// #define LINUX_      // Linux

#define BIN8(x) BIN___(0##x)
#define BIN___(x)                                    \
	(                                                \
	((x / 01ul) % 010)*(2>>1) +                      \
	((x / 010ul) % 010)*(2<<0) +                     \
	((x / 0100ul) % 010)*(2<<1) +                    \
	((x / 01000ul) % 010)*(2<<2) +                   \
	((x / 010000ul) % 010)*(2<<3) +                  \
	((x / 0100000ul) % 010)*(2<<4) +                 \
	((x / 01000000ul) % 010)*(2<<5) +                \
	((x / 010000000ul) % 010)*(2<<6)                 \
	)

#if defined(WIN_BUILDER)
  #define snprintf snprintf
  #define vsnprintf vsnprintf
  #define strcasecmp stricmp
  #define strncasecmp strnicmp

  #include "windows.h"
  #include <time.h>
//  #include <ctime>

#endif

#if defined(WIN_VS)
  #define snprintf _snprintf
  #define vsnprintf _vsnprintf
  #define strcasecmp _stricmp
  #define strncasecmp _strnicmp

  #include <ctime>
//  #include <time.h>
  #include "windows.h"
  #include "strptime.h"

#endif

#include "stdio.h"

extern char logstr[512];
typedef unsigned char CANBlock[10];

unsigned long GetTickCount_(void); // ����� � ������ ������ ������� � ��

class LogFileObj
{

private:

	FILE* LogFile;                    // ��� ����
	char logstr[512];

public:

	void * Owner;
	LogFileObj(void * Owner_);
	~LogFileObj(void);
	void AddText(char *);
	void AddIntParam(char * str, int Par);
	void AddTwoIntParam(char * str, int Par1, int Par2);
	void AddThreeIntParam(char * str, int Par1, int Par2, int Par3);
	void AddFourIntParam(char * str, int Par1, int Par2, int Par3, int Par4);
	void Add5IntParam(char * str, int Par1, int Par2, int Par3, int Par4, int Par5);
	void AddCANBlock(char * str, int Par, CANBlock blk);
};

extern LogFileObj * Log;          // ��������� �� ������ LogFile

//Added by KirillB  (temp)
struct sSimulationData
{
	sSimulationData()
	{
		memset(currentDir,0,2*sizeof(int));
		memset(currentPos,0,2*sizeof(int));
	}

	void setCaretDirections(int dir1, int dir2)
	{
		currentDir[0] = dir1;
		currentDir[1] = dir2;
    }

	int currentDir[2];
	int currentPos[2];
};

sSimulationData* GetSimData();

#endif /* UTILS_H */

