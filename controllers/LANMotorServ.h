// для тестирования управления двигателями

#ifndef LANMotorServ
#define LANMotorServ


#include <stdio.h>
#include <stdlib.h>
#include <queue>
#include <vector>
#include "DataTr.h"
#include "Utils.h"
#include "LanProtUMU.h"


#ifdef LINUX_
#include <iostream>
typedef int eAlarmAlgorithm;
#else
#include <process.h>
#include <iostream.h>
#endif

#define DbgLog
#define Dbg

typedef void __fastcall (__closure *DataReceived)(int UMUIdx, void*, int);


namespace LANProtDebugMS
{
#ifdef LINUX_
	typedef void ( *Add_Log)(unsigned char * msg,int length,int way);
	typedef void ( *Add_Log2)(unsigned int * a, unsigned int b);
	typedef void ( *Add_Log3)(unsigned int);
#else
	typedef void __fastcall (__closure *Add_Log)(unsigned char * msg,int length,int way);
	typedef void __fastcall (__closure *Add_Log2)(unsigned int * a, unsigned int b);
	typedef void __fastcall (__closure *Add_Log3)(unsigned int);
#endif

extern	Add_Log2 onAddLog2;
extern	Add_Log onAddLog;
extern	Add_Log3 onAddLog3;

extern	BOOL useLog;


#ifdef Dbg
void addbytcnt(unsigned int val); 
unsigned int rdbytcnt(void);
void initbytcnt(void); 
void deinitbytcnt(void);
#endif

}


//---------------------------------------------------------------------------
enum ReceiveState
{
		rcvOff = 0,
		rcvOn  = 1
};

//---------------------------------------------------------------------------
typedef struct
{
	unsigned char Data[LanDataMaxSize];
} tLAN_DRVMessage;
//

#pragma pack(push,1)
typedef struct
{
	unsigned short Size;
	tLAN_DRVMessage Msg;
} tLAN_DRVMessageObj;
#pragma pack(pop)

#define AUTO_RESET_EVENT FALSE
#define MANUAL_RESET_EVENT TRUE

//
class LANMOTORSERVICE
{
private:
		cDataTransfer* hrd;                		
		ReceiveState RState;

		tLAN_DRVMessageObj* OutBlock;          	  	

		std::queue <tLAN_DRVMessageObj> OutBuffer;
//
		int OutStIdx;                     		
		int OutEdIdx;                     	


		tLAN_DRVMessage LAN_message;
		unsigned short ReadBytesCount;

//
		int SendTickT;
		DataReceived OnDataReceived;       		
//
		void Unload(void);                		
		void AddToOutBuff();        	
//
public:
		int UMUIdx;                                        

		LANMOTORSERVICE(int UMUIdx_, cDataTransfer* hrd_);
		~LANMOTORSERVICE(void);

		void StartWork(void);                            
		void EndWork(void);                              

		void Tick(void);                   

		void TickSend(void);
		void TickReceive(void);
		void SetOutputData(unsigned char *buf,unsigned int bufsize);
                                          void setRcvProc(DataReceived proc);

};

#ifdef UMUDebug      
#define UMULog 
#endif
#ifdef UMUEventLog   
#define UMULog 
#endif
#ifdef UMUInDataLog  
#define UMULog 
#endif
#ifdef UMUOutDataLog 
#define UMULog
#endif


#endif


