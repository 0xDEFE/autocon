//---------------------------------------------------------------------------


// # pragma hdrstop

#include "AController.h"


__fastcall cAController::cAController(void)
{
	EndWorkFlag = false;
	State = acNotSet;
	WorkCycle = - 1;
	WorkMode = wmNone;
	Processing = true;

	MProc = NULL;
	SearchProc = NULL; // ����� "�����"
	TestProc = NULL;   // ����� "����"
	AdjustProc = NULL; // ����� "���������"
	TuneProc = NULL;   // ����� "���������"
    CavtkRequestProc = NULL;

	bManualMode = false;

	hCaretMoveEndEvent = CreateEvent(   NULL,               // default security attributes
										FALSE,               // manual-reset event
										FALSE,              // initial state is nonsignaled
										TEXT("CATVKCarets")    // object name
										);

    hTuneSetStateEvent = CreateEvent(   NULL,               // default security attributes
										FALSE,               // manual-reset event
										FALSE,              // initial state is nonsignaled
										TEXT("hTuneSetStateEvent")    // object name
										);

	PathMgr::PushPath(L"$scripts", L"resources\\scripts");
    bCatchEndOfAdj2Movement = false;
}

__fastcall cAController::~cAController(void)
{
	EndWorkFlag = true;
	while (!this->Finished) Sleep(50);  // ���� ���������� ������

	if (ctrl)
	{
		int tmp = ctrl->datalinenumber;
		delete ctrl;
		DataTr->closeConnections(tmp);
		ctrl = NULL;
	}
}

void __fastcall cAController::Execute(void)
{
	 while (!EndWorkFlag)
	 {
	 	Sleep(1);
		//Added by KirillB
		//��������� ������ ��������� � ���� � ��� �� WorkCycle � ������ ������
		//bool bEnableCurrrentWorkCycle = bManualWorkCycleCtrl ? (WorkCycle!=PrevWorkCycle) : true;
		//PrevWorkCycle = WorkCycle;

        #ifndef SIMULATION_CONTROLER_AND_MOTORS
		if (ctrl)
		#endif
		{
            // Get all messages from cavtk
            #ifndef SIMULATION_CONTROLER_AND_MOTORS
            tCavtkMsg msg;
            while(ctrl->getMessage(&msg))
            {
                LOGERROR("CAVTK Message: type = %d, code = %d, flag = %d",msg.type,msg.code,msg.flag);
                if(CavtkRequestProc)
                {
                    CavtkRequestProc(ctrl, msg);
                }
            }
            #endif


			switch (State)
			{
				 case acWait:
				 {
					 switch (WaitMode)
					 {
						case wmInit:
						{
							//LOGPERS_NF("ACWaitMode","AC WaitMode = wmInit");

                            #ifndef SIMULATION_CONTROLER_AND_MOTORS
							if (ctrl->getmode() >= 0)
	                        #endif
							{
							   State = acReady;
							}
							// else
							//{ State = acError; }
							break;
						}
						case wmReadyEnter:
						{
							//LOGPERS_NF("ACWaitMode","AC WaitMode = wmReadyEnter");

                            #ifndef SIMULATION_CONTROLER_AND_MOTORS
							if (ctrl->setmode(minit) == 0)
	                        #endif
							{
								 State = acReady;
							}
						    break;
						}
						case wmAdjustEnter:
						{
							//LOGPERS_NF("ACWaitMode","AC WaitMode = wmAdjustEnter");

                            #ifndef SIMULATION_CONTROLER_AND_MOTORS
							if (ctrl->getmode() == mrail)
	                        #endif
							{
							   #ifndef SIMULATION_CONTROLER_AND_MOTORS
							   if (ctrl->setmode(madj1) == 0)
  	                           #endif
							   {
								 State = acAdjusting1;
                                 if (AdjustProc) AdjustProc(-5); //Top caret movement
                                 if (MProc) MProc(mtInfo, "��������� ����� ����������� ������� �������.", GetTickCount());

                                 //--------Added by KB--------------
                                 WorkCycle = - 1;
								 WorkMode = wmNone;
								 Processing = false;
                                 //------------------------------
							   }
							}
							break;
						}
						case wmTuneEnter:
						{
                            TRACE("wmTuneEnter");
							break;
						}
					 }
					 break;
				 }
                 case acReady:
				 {
                    if(WorkMode != wmNone)
                    {
                        WorkMode = wmCancel;
                    }
                    break;
                 }
			}

			// -------------------------------------------

			if (WorkMode == wmCancel)
			{
				WorkMode = wmNone;
				Sleep(100);
				WorkCycle = - 1;
                Processing = true;
                #ifndef SIMULATION_CONTROLER_AND_MOTORS
				ctrl->motionBreak(MotionBreakEndProcedure);
                #else
                cCaretMotionSim::Instance().motionBreak(MotionBreakEndProcedure);
                #endif
                TRACE("WorkMode == wmCancel:");
                switch (State)
                {
                    case acSearch:
                    {
                        if(SearchProc)
                            SearchProc(- 1);
                        break;
                    }
                    case acHandScan:
                    {
                        break;
                    }
                    case acTest:
					{
                        if(TestProc)
                            TestProc(- 1);
                        break;
                    }
                    case acTuning:
                    {
                        TRACE("\tWorkMode == wmCancel (tuning)");
                        if(TuneProc)
                            TuneProc(- 1);
                        break;
                    }
                    case acAdjusting1:
                    {
                        if(AdjustProc)
                            AdjustProc(- 1);
                        break;
                    }
                    case acAdjusting2:
                    {
                        if(AdjustProc)
                            AdjustProc(- 1);
                        break;
                    }
                }

                #ifdef SIMULATION_CONTROLER_AND_MOTORS
                Processing = false;
                #endif
			}

			 // �������� ������ ���������
			if (!Processing)
				if (WorkMode == wmTuning)
				{
                    //TRACE("WorkMode = wmTuning");
                    if((tuneNewState != tmNone) && (tuneNewState != tuneCurrState))
                    {
                        if(TuneMachineData[tuneCurrState][tuneNewState] != 2)
                        {
                            TRACE("������ � TuneMachine: old=%d, new=%d", tuneCurrState, tuneNewState);
                            tuneCurrState = tmError;
                            MProc(mtError, "������ � TuneMachine", GetTickCount());
                        }
                        else
                        {
                            tuneCurrState = tuneNewState;
                            //MProc(mtError, "������!", GetTickCount());
                        }

                        SetEvent( hTuneSetStateEvent );
                        tuneNewState = tmNone;
                    }

                    switch(tuneCurrState)
                    {
                        case tmInit:
                        {
                            //TRACE("wmTuning state tmInit");
                            break;
                        }
                        case tmAutInit:
                        {
                            //TRACE("wmTuning state tmAutInit");
                            tuneCurrState = tmAutWork;
                            break;
                        }
                        case tmManInit:
                        {
                            //TRACE("wmTuning state tmManInit");
                            tuneCurrState = tmManWork;
                            break;
                        }
                        case tmWorkBreak:
                        {
                            //TRACE("wmTuning state tmWorkBreak");
                            tuneCurrState = tmWorkEnd;
                            break;
                        }
                        case tmWorkEnd:
                        {
                            //TRACE("wmTuning state tmWorkEnd");
                            //ctrl->motionBreak(MotionOKEndProcedure);
                            WorkMode = wmCancel;
                            break;
                        }
                        case tmError:
                        {
                            //TRACE("wmTuning state tmError");
                            tuneCurrState = tmWorkBreak;
                            break;
                        }
                        case tmAutWork:
                        {
                            //TRACE("wmTuning state tmAutWork");
                            //check tune script
                            if( !TuneScriptPrg.isValid() )  //If not valid - try to load from file
                            {
                                AnsiString str = PathMgr::GetPath(L"($scripts)\\tune.sc");

                                if(str.Length() == 0)
                                {
                                    MessageBoxA(NULL,"Script file not found!","File not found!",MB_OK | MB_ICONWARNING);
                                    //ctrl->motionBreak(MotionOKEndProcedure);
                                    WorkCycle = -1;
                                    tuneCurrState = tmError;
                                    break;
                                }

                                if( !TuneScriptPrg.createFromFile(str.c_str()) )
                                {
                                    MessageBoxA(NULL,TuneScriptPrg.getLastError().toString().c_str(),"Script compilation errors:",MB_OK | MB_ICONWARNING);
                                    //ctrl->motionBreak(MotionOKEndProcedure);
                                    WorkCycle = -1;
                                    tuneCurrState = tmError;
                                    break;
                                }
                            }
                            else if( WorkCycle == -1)
                            {
                                TuneScriptPrg.cleanup();
                                WorkCycle = 0;
                            }

                            eScanScriptRetCode ret = TuneScriptPrg.executeNext();
                            if(ret == SSRC_RUNTIME_ERROR)
                            {
                                if( TuneScriptPrg.getLastError().errors.size() ) //Show errors
                                {
                                    UnicodeString str = TuneScriptPrg.getLastError().toString().c_str();
                                    //LOGERROR(L"Script error: %s",str.c_str());
                                    Logger::Error(Logger::CHANNEL_ID_MAIN,L"Script error: %s",str.c_str());
                                    TuneScriptPrg.getLastError().errors.clear();
                                }
                                tuneCurrState = tmError;
                                break;
                            }
                            else if(ret == SSRC_END_PROGRAMM) //If programm ends
                            {

                                WorkCycle = - 1;
                                TuneScriptPrg.cleanup();
                                if(TuneProc)
                                    TuneProc( -1 );

                                Sleep(100);
                                tuneCurrState = tmInit;
                            }
                            else
                            {
                                if(TuneProc)
                                    TuneProc( TuneScriptPrg.getCurrentLine()*100 / TuneScriptPrg.getTotalLines() );
                                WorkCycle++;
                            }
                            break;
                        }
                        case tmManWork:
                        {
                            //TRACE("wmTuning state tmManWork");
                            ManualModeLock.Enter();
                            if( ManualScriptPrg.isValid() )
                            {
                                eScanScriptRetCode ret = ManualScriptPrg.executeNext();
                                if(ret == SSRC_RUNNING)
                                {
                                    WorkCycle++;
                                    if(ManualProc)
                                        ManualProc(WorkCycle);
                                }
                                else if(ret == SSRC_END_PROGRAMM)
                                {
                                    if(ManualProc)
                                        ManualProc(-1);
                                    ManualScriptPrg.release();
                                    //tuneCurrState = tmInit;
                                }
                                else if(ret == SSRC_RUNTIME_ERROR)
                                {
                                    ManualScriptPrg.release();
                                    tuneCurrState = tmError;
                                }
                            }
                            ManualModeLock.Release();

                            Sleep(1);
                            break;
                        }
                    }
				}

			 // �������� ������ ���������
			if (!Processing)
				if (WorkMode == wmAdjusting)
				{
					///LOGPERS_NF("ACWorkMode","AC WorkMode = wmAdjusting");

					WorkCycle++;
					Sleep(100);
					if (WorkCycle == 7)
					{
                        if (AdjustProc) AdjustProc(WorkCycle);
						WorkCycle = - 1;
                        WorkMode = wmNone;
						Sleep(100);
					}
					else
					{
                        if (AdjustProc) AdjustProc(WorkCycle);
						Sleep(100);
						//Modifyed by KirillB
						Processing = true;
						#ifndef SIMULATION_CONTROLER_AND_MOTORS
						ctrl->motionStart(&this->MotionOKEndProcedure);
						#else
                        cCaretMotionSim::Instance().motionStart(&this->MotionOKEndProcedure);
						#endif
					}
				}

			 // �������� ������ ���� ��� Search
			if (!Processing)
				if ((WorkMode == wmTest) || (WorkMode == wmSearch))
				{
					if (WorkCycle == - 1)
					{
						WorkCycle = 2;
                        #ifdef SIMULATION_CONTROLER_AND_MOTORS
                        cCaretMotionSim::Instance().moveCaretsAbs(ScanLen/2, 50);
                        cCaretMotionSim::Instance().waitStop();
                        #endif
						if (State == acSearch) { if (SearchProc) SearchProc(WorkCycle); } else { if (TestProc) TestProc(WorkCycle); };
						Sleep(100);
						//Modifyed by KirillB
						Processing = true;
						#ifndef SIMULATION_CONTROLER_AND_MOTORS
						ctrl->motionStart(&this->MotionOKEndProcedure);
						#else
                        cCaretMotionSim::Instance().initSearchMotionTable();
                        cCaretMotionSim::Instance().motionStart(&this->MotionOKEndProcedure);
						#endif
					}
					else
					{
						//if(!bManualWorkCycleCtrl)
							WorkCycle++;
						Sleep(100);
						if (WorkCycle == 16)
						{
							if (State == acSearch) { if (SearchProc) SearchProc(WorkCycle); } else { if (TestProc) TestProc(WorkCycle); };
							WorkCycle = - 1;
							WorkMode = wmNone;
							Sleep(100);
						}
						else
						{
							if (State == acSearch) { if (SearchProc) SearchProc(WorkCycle); } else { if (TestProc) TestProc(WorkCycle); };
							Sleep(100);
							//Modifyed by KirillB
							Processing = true;
							#ifndef SIMULATION_CONTROLER_AND_MOTORS
							ctrl->motionStart(&MotionOKEndProcedure);
							#else
                            cCaretMotionSim::Instance().motionStart(&MotionOKEndProcedure);
							#endif
						}
					};
				}
		}

      #ifdef SIMULATION_CONTROLER_AND_MOTORS
		//Sleep(20000);
      #endif
        Sleep(1);

	 }
	 Sleep(100);
}

// ---------------------------------------------------------------------------

void __fastcall cAController::MotionOKEndProcedure(MOTIONRESULT r)
{
#ifdef AC_DEBUG_PRINT
	//prints error
	if(r.error.errorNumber != 0)
	{
		UnicodeString str;
		switch(r.error.errorNumber)
		{
			case ERROR_ACTION_DENIED: 
				str = "ERROR_ACTION_DENIED"; break;
			case ERROR_MOTION_NOT_COMPLETED: 
				str = "ERROR_MOTION_NOT_COMPLETED"; break; 
			case ERROR_SET_CMD: 
				str = "ERROR_SET_CMD"; break;
			case ERROR_GET_CMD_STATUS: 
				str = "ERROR_GET_CMD_STATUS"; break;
			case ERROR_SET_HANDCMD: 
				str = "ERROR_SET_HANDCMD"; break;
			case ERROR_HANDMOTION_NOT_COMPLETED: 
				str = "ERROR_HANDMOTION_NOT_COMPLETED"; break;
			case ERROR_GET_HANDCMD_STATUS: 
				str = "ERROR_GET_HANDCMD_STATUS"; break;
			case ERROR_CMD_CANCELLED: 
				str = "ERROR_CMD_CANCELLED"; break;
			case ERROR_WRONG_TROLLEY_POS: 
				str = "ERROR_WRONG_TROLLEY_POS"; break;
			case ERROR_SET_SHIFT_1272: 
				str = "ERROR_SET_SHIFT_1272"; break;
			case ERROR_SET_SHIFT_1273: 
				str = "ERROR_SET_SHIFT_1273"; break;
			default:
				 str = "UNKNOWN"; break;
		}
		
		LOGERROR("Error No%d ( %s ): %d",r.error.errorNumber,str.c_str(),r.parameter);
	}
#endif

	Processing = false;
	SetEvent(hCaretMoveEndEvent);

    TRACE("MotionOKEndProcedure");

    if(bCatchEndOfAdj2Movement)
    {
        bCatchEndOfAdj2Movement = false;
        if (AdjustProc) AdjustProc(-3);
    }
}

void __fastcall cAController::MotionBreakEndProcedure(MOTIONRESULT r)
{
	WorkCycle = - 1;
	SetEvent(hCaretMoveEndEvent);
    TRACE("MotionBreakEndProcedure");
}

// ---------------------------------------------------------------------------

void cAController::prn(char* s)
{
    //
}

// ---------------------------------------------------------------------------

void cAController::debugPrn(char* s)
{
#ifdef AC_DEBUG_PRINT
	UnicodeString str = s;
	LOGINFO("%s",str.c_str());
    TRACE(AnsiString(str).c_str());
#endif
}

// ---------------------------------------------------------------------------
void cAController::setErrorCode(unsigned int error)
{
    if (MProc)
        switch (error)
        {
            case e_initsensorqry       : { MProc(mtError, "�� ������� ���������� ��������� ��������� ��������", GetTickCount()); break; } // �������� � init0
            case e_kv1off              : { MProc(mtError, "��� �������� ������� �������� ����������� (��1) ������� ����������� - ������� ����� ������������� ���������", GetTickCount()); break; }
            case e_kv2off              : { MProc(mtError, "��� �������� ������� �������� ����������� (��2) ������� ����������� - ������� ����� ������������� ���������", GetTickCount()); break; }
            case e_kvg1off             : { MProc(mtError, "������ ����� ������ (���1) ������� �����������", GetTickCount()); break; }
            case e_kvg2off             : { MProc(mtError, "������ ����� ������ (���2) ������� �����������", GetTickCount()); break; }
            case e_kvg3off             : { MProc(mtError, "������ ����� ������ (���3) ������� �����������", GetTickCount()); break; }
			case e_kvg4off             : { MProc(mtError, "������ ����� ������ (���4) ������� �����������", GetTickCount()); break; }
            case e_kvg1on              : { MProc(mtError, "������ ����� ������ (���1) ������� ���������", GetTickCount()); break; }
            case e_kvg2on              : { MProc(mtError, "������ ����� ������ (���2) ������� ���������", GetTickCount()); break; }
            case e_kvg3on              : { MProc(mtError, "������ ����� ������ (���3) ������� ���������", GetTickCount()); break; }
            case e_kvg4on              : { MProc(mtError, "������ ����� ������ (���4) ������� ���������", GetTickCount()); break; }
            case e_dip12               : { MProc(mtError, "������� �� �������� ����������� ������� (���1 / ���2) - ������� ����� ������������� ���������", GetTickCount()); break; }
            case e_dip34               : { MProc(mtError, "������� �� �������� ����������� ������� (���3 / ���4) - ������� ����� ������������� ���������", GetTickCount()); break; }
            case e_farfromstartpos     : { MProc(mtError, "��� ���������� ������� �� �������� �������� ���������", GetTickCount()); break; }
            case e_id1off	           : { MProc(mtError, "��� ������ � ������ - ����������� ������ (��1) ������� �����������", GetTickCount()); break; }
            case e_id2off	           : { MProc(mtError, "��� ������ � ������ - ����������� ������ (��2) ������� �����������", GetTickCount()); break; }
            case e_id1on               : { MProc(mtError, "����� � ������ - ����������� ������ (��1) �������", GetTickCount()); break; }
            case e_id2on	           : { MProc(mtError, "����� � ������ - ����������� ������ (��2) �������", GetTickCount()); break; }
       //     case e_ddoff               : { MProc(mtError, "������ �������� - ������ �������� (��) �������", GetTickCount()); break; }
       //     case e_rdoff               : { MProc(mtError, "���� �������� ���������� - ?", GetTickCount()); break; }
            case e_outofcenter         : { MProc(mtError, "����������� ��������� ����� �����, ���� ���� ��������� �� ��������� ����������� ���������", GetTickCount()); break; }  // ��� ������� ����� ����� ����������, ��� ����
            case e_caretReturn         : { MProc(mtError, "������ ������� ��� �������� ������� � �������� ���������", GetTickCount()); break; }
            case e_drvnotready         : { MProc(mtError, "������ - ������ �� �����", GetTickCount()); break; }
            case e_cmdCancell          : { MProc(mtError, "������ ������� ��� ������� ������ �������", GetTickCount()); break; }
            case e_drvaborted          : { MProc(mtError, "������ ������� - ������ � ������������ ���������� ���� �������� ���������", GetTickCount()); break; }
            case e_keyPressedTooRapidly: { MProc(mtError, "������ ������ ���������� � ������� ������", GetTickCount()); break; }
            case e_keyDisabled         : { MProc(mtError, "�������� (�� ������� ������ ������) ���������", GetTickCount()); break; }
//
            case e_tcprevive       : { MProc(mtError, "���������� ������������ TCP ���������� � ������������", GetTickCount()); break; }
            case e_inituncluspedkv  : { MProc(mtError, "��1,��2 �� ���������� - �������� � ��������� init0", GetTickCount()); break; }
            case  e_inituncluspeda  : { MProc(mtError, "����� ������������ ��������� - �������� � ��������� init0", GetTickCount()); break; }
            case e_wrongmode  : { MProc(mtError, "����������� ��-��  ������������� �������� ������", GetTickCount()); break; }
            case e_wrongmodeandstate : { MProc(mtError, "����������� ��-��  ������������� ��������� ������ � ���������", GetTickCount()); break; }
            case e_wrongstate : { MProc(mtError, "����������� ��-��  ������������� �������� ���������", GetTickCount()); break; }
            case e_wrongact:      { MProc(mtError, "����������� ��-��  ������ ������������ ������� ��� �����������", GetTickCount()); break; }
 
            case e_softwarever:      { MProc(mtError, "����������� ��-�� ������������� ���������� ������ �� �����������", GetTickCount()); break; }
            case e_softwareres:      { MProc(mtError, "�����������. ���������� ��� �������� � ������� �����", GetTickCount()); break; }
        }
//	Messages->Add(Text);
}

// ---------------------------------------------------------------------------

void cAController::eventSignProc(BOOL start)
{

	if (start)
	{
		switch (State)
		{
	 //		case acReady     : { ; break; }
			case acSearch:
			{

				// acSearch, 0

				WorkCycle = - 1;
				WorkMode = wmSearch;
				Processing = false;
				break;
			}
            case acHandScan:
			{
				WorkCycle = - 1;
				WorkMode = wmHandScan;
				Processing = false;
				break;
			}
			case acTest:
			{
				// acTest, 0

				WorkCycle = - 1;
				WorkMode = wmTest;
				Processing = false;
				break;
			}
			case acTuning:
			{
				// acTuning, 0
				//WorkCycle = 0;

				//Added by KirillB
                TRACE("eventSignProc: acTuning");
				WorkCycle = - 1;
				WorkMode = wmTuning;
				Processing = false;
				break;
			}
			case acAdjusting1:
			{
				break;
			}
			case acAdjusting2:
			{

				// acAdjusting2, 0

				WorkMode = wmAdjusting;
				WorkCycle = -1;
				Processing = false;
				break;
			}
		}
	}
	else
	{
        TRACE("eventSignProc: WorkMode = wmCancel");
		WorkMode = wmCancel;
	}
}

// ---------------------------------------------------------------------------

bool cAController::Init(cDataTransfer * DataTr_)
{
	DataTr = DataTr_;

	ctrl = NULL;
    #ifndef SIMULATION_CONTROLER_AND_MOTORS
	#ifndef NO_CONTROLER_AND_MOTORS
	int res = - 1;
	if (DataTr) res = DataTr->AddConnection("", "192.168.100.104", PORT_in_cont, PORT_out_cont, true, true);
	if (res >= 0)
	{
		#ifndef NO_MOTORS
		ctrl = new cavtk(res, DataTr, "192.168.100.3", false);
		#endif
		#ifdef NO_MOTORS
		ctrl = new cavtk(res, DataTr, "192.168.100.3", true);
		#endif

		if (ctrl)
		{
			ctrl->datalinenumber = res;
			INPARAMDEF param =
				{
					&this->prn,
					&this->setErrorCode,
					&this->debugPrn,
					&this->eventSignProc
				};
			if (ctrl->init(&param) == 0)
			{
				WaitMode = wmInit;
				State = acWait;
			} else State = acError;
		} else State = acError;
	} else State = acError;

    if (ctrl)
    {
	  //  ctrl->defineKv1Kv2FailIgnore(true);
    }

	return (State != acError);
	#endif
   	#endif


    #ifdef SIMULATION_CONTROLER_AND_MOTORS
    State = acReady;
	return true;
	#endif
}

TACState cAController::GetMode(void)
{
	return State;
}

bool cAController::SetMode(TACState NewState)
{
	UnicodeString ENTER = (char)0x0D;
	ENTER = ENTER + (char)0x0A;
	bool Res = false;
    //Sleep(2000);
    #ifndef SIMULATION_CONTROLER_AND_MOTORS
    if (ctrl)
	#endif
        switch (NewState)
        {
            case acReady:
            {
                if ((State == acAdjusting1) || (State == acAdjusting2))
                {
                    TRACE("cAController::SetMode [acReady](State == acAdjusting1) || (State == acAdjusting2)");
                    #ifndef SIMULATION_CONTROLER_AND_MOTORS
                    if (ctrl->setmode(mrail) == 0)
	                #endif
                    {
//                        Messages->Add("��������� �����: " + ENTER + ENTER + "1. ��������� �������." + ENTER + "2. �������� �������� �����.");
                        if (MProc) MProc(mtInfo, "��������� �����: " + ENTER + ENTER + "1. ��������� �������." + ENTER + "2. �������� �������� �����.", GetTickCount());

                        WaitMode = wmReadyEnter;
                        State = acWait;
                        Res = true;
                    }
                }
                else
				{
                    TRACE("cAController::SetMode [acReady]");
				    Sleep(200);
					#ifndef SIMULATION_CONTROLER_AND_MOTORS
                    if (ctrl->setmode(minit) == 0)
					#endif
                    {
                        State = NewState;
                        Res = true;
                    }
                }
                break;
            }
            case acSearch:
            {
                if (State == acReady)
					#ifndef SIMULATION_CONTROLER_AND_MOTORS
                    if (ctrl->setmode(mpoisk) == 0)
					#endif
                    {

						State = NewState;
						if (MProc) MProc(mtInfo, "��������� �����: " + ENTER + ENTER + "1. ������� �������." + ENTER + "2. ������� ����� �����." + ENTER + "3. ��������� ���� ������������." + ENTER + ENTER, GetTickCount());

						#ifdef SIMULATION_CONTROLER_AND_MOTORS
						eventSignProc(true);
						#endif
                        Res = true;
                    }
                break;
            }
            case acHandScan:
            {
                if (State == acReady)
					#ifndef SIMULATION_CONTROLER_AND_MOTORS
                    if (ctrl->setmode(mhand) == 0)
					#endif
                    {

						State = NewState;
						if (MProc) MProc(mtInfo, "��������� �����: " + ENTER + ENTER + "1. ������� �������." + ENTER + ENTER, GetTickCount());

						#ifdef SIMULATION_CONTROLER_AND_MOTORS
						eventSignProc(true);
						#endif
                        Res = true;
                    }
                break;
            }
            case acTest:
            {
                if (State == acReady)
					#ifndef SIMULATION_CONTROLER_AND_MOTORS
                    if (ctrl->setmode(mrail) == 0)
					#endif
					{
						State = NewState;
						//Messages->Add("��������� �����: " + ENTER + ENTER + "1. ��������� �������� �����." + ENTER + "2. ������� �������." + ENTER + "3. ������� ����� �����.");
						if (MProc) MProc(mtInfo, "��������� �����: " + ENTER + ENTER + "1. ��������� �������� �����." + ENTER + "2. ������� �������." + ENTER + "3. ������� ����� �����." + ENTER + "4. ��������� ���� ������������." + ENTER + ENTER, GetTickCount());

						#ifdef SIMULATION_CONTROLER_AND_MOTORS
						eventSignProc(true);
						#endif
						Res = true;
                    }
                break;
            }
            case acTuning:
            {
                if (State == acReady)
				{
					//New version
                    TRACE("cAController::SetMode [acTuning]");
					#ifndef SIMULATION_CONTROLER_AND_MOTORS
					if (ctrl->setmode(mtune) == 0)
					#endif
					{
                        TRACE("cAController::SetMode [acTuning] ctrl->setmode(mtune) == 0");
                    	State = NewState;
						//Messages->Add("��������� �����: " + ENTER + ENTER + "1. ��������� �������� �����." + ENTER + "2. ������� �������." + ENTER + "3. ������� ����� �����.");
						if (MProc) MProc(mtInfo, "��������� �����: " + ENTER + ENTER + "1. ��������� �������� �����." + ENTER + "2. ������� �������.", GetTickCount());

                        tuneCurrState = tmInit;

						#ifdef SIMULATION_CONTROLER_AND_MOTORS
						eventSignProc(true);//test KirillB
						#endif
						Res = true;
                    }
                }
                break;
            }
            case acAdjusting1:
            {
                if (State == acReady)
                {
                    #ifndef SIMULATION_CONTROLER_AND_MOTORS
                    if (ctrl->setmode(mrail) == 0)
	                #endif
                    {
                        //Messages->Add("��������� �����: " + ENTER + ENTER + "1. ��������� �������� �����." + ENTER + "2. ������� �������.");
                        if (MProc) MProc(mtInfo, "��������� �����: " + ENTER + ENTER + "1. ��������� �������� �����." + ENTER + "2. ������� �������.", GetTickCount());
                        WaitMode = wmAdjustEnter;
                        State = acWait;
                        Res = true;
                    }
                }
                break;
            }
            case acAdjusting2:
            {
                if (State == acAdjusting1)
                {

					#ifndef SIMULATION_CONTROLER_AND_MOTORS
                    if (ctrl->setmode(madj2) == 0)
					#endif
                    {
                        State = acAdjusting2;
                        if (AdjustProc) AdjustProc(-4); //Bottom caret movement

                        bCatchEndOfAdj2Movement = true;
                        #ifdef SIMULATION_CONTROLER_AND_MOTORS
                        Sleep(1000);
						MotionOKEndProcedure(MOTIONRESULT());
                        #endif
                        //Messages->Add("��������� ����� ����������� ������ �������. ������ ""����"" - ������ ������������");
                        if (MProc) MProc(mtInfo, "��������� �����, ����������� ������ �������. ������ ""����"" - ������ ������������", GetTickCount());
                        Res = true;


                    }
                }
                break;
            }
		}

	return Res;
}

int cAController::GetWorkCycle(void)
{
    return WorkCycle;
}

void cAController::SetMessagesProc(TSendMessages Proc)
{
    MProc = Proc;
}

void cAController::SetSearchMotion(TChangeWorkCycle Proc) // ����� "�����"
{
  SearchProc = Proc; // ����� "�����"
}

void cAController::SetTestMotion(TChangeWorkCycle Proc)   // ����� "����"
{
  TestProc = Proc;   // ����� "����"
}

void cAController::SetAdjustMotion(TChangeWorkCycle Proc) // ����� "���������"
{
  AdjustProc = Proc; // ����� "���������"
}

void cAController::SetTuneMotion(TChangeWorkCycle Proc)   // ����� "���������"
{
  TuneProc = Proc;   // ����� "���������"
}

void cAController::SetManualMotion(TChangeWorkCycle Proc)   // ����� "������"
{
	ManualProc = Proc;
}

void cAController::SetCavtkRequestProc(TSentCavtkRequest Proc)
{
    CavtkRequestProc = Proc;
}


//Add by KirillB
bool cAController::CaretManualMove(int topOffset,int bottomOffset)
{
	int ret = 0;

    bool bCaretActive[2];
	int iCaretPos[2];
    if(!GetCaretsStatus( &bCaretActive[0], &iCaretPos[0], &bCaretActive[1], &iCaretPos[1]))
        return false;

    int iNextCaretPos[2];
    iNextCaretPos[0] = topOffset + iCaretPos[0];
    iNextCaretPos[1] = bottomOffset + iCaretPos[1];
    if((iCaretPos[0] >= 0) && (iCaretPos[0] < ScanLen) &&
        ((iNextCaretPos[0] < 0) || (iNextCaretPos[0] >= ScanLen)))
    {
        LOGERROR("Failed to move - top caret is out of bound: src_pos=%d, dst_pos=%d", iCaretPos[0], iNextCaretPos[0]);
        return false;
    }
    if((iCaretPos[1] >= 0) && (iCaretPos[1] < ScanLen) &&
        ((iNextCaretPos[1] < 0) || (iNextCaretPos[1] >= ScanLen)))
    {
        LOGERROR("Failed to move - btm caret is out of bound: src_pos=%d, dst_pos=%d", iCaretPos[1], iNextCaretPos[1]);
        return false;
    }

    Processing = true;

	#ifndef SIMULATION_CONTROLER_AND_MOTORS
	ret = ctrl->motionStart2(bottomOffset, topOffset, &MotionOKEndProcedure); // Top
	#else
    cCaretMotionSim::Instance().motionStart2(bottomOffset, topOffset, &MotionOKEndProcedure);
	#endif

	if(!WaitCaretStop())
		return false;

	return ret!=-1;
}

bool cAController::WaitCaretStop()
{
	const int WaitCaretTimeout = 20000;
	DWORD dwWaitResult;
	dwWaitResult = WaitForSingleObject(hCaretMoveEndEvent,WaitCaretTimeout);
	switch(dwWaitResult)
	{
		case WAIT_OBJECT_0: return TRUE;
				   default: return FALSE;
	}
	return false;
}

bool cAController::MoveCaretsToStart()
{
	Processing = true;
	int ret = 0;
	#ifndef SIMULATION_CONTROLER_AND_MOTORS
	ret = ctrl->trolleysReturn(&MotionOKEndProcedure); // Top
	#else
    cCaretMotionSim::Instance().trolleysReturn(&MotionOKEndProcedure);
	#endif

	if(!WaitCaretStop())
		return false;

	return ret!=-1;
}

bool cAController::GetCaretStatus(bool caretPos, bool* bActive, int* pCoord)
{
	if(!caretPos)//top
	{
        return GetCaretsStatus(bActive, pCoord, NULL, NULL);
	}
	else    	//bottom
	{
        return GetCaretsStatus(NULL, NULL, bActive, pCoord);
    }
    return false;
}

 bool cAController::GetCaretsStatus(bool* bTopActive, int* pTopCoord, bool* bBtmActive, int* pBtmCoord)
 {
    tCARIAGESTAT stat;
	#ifndef SIMULATION_CONTROLER_AND_MOTORS
	int ret = ctrl->getCariagesStatus(&stat);
	if(ret < 0)
		return false;
	#else
    cCaretMotionSim::Instance().getCariagesStatus(&stat);
    Sleep(1000);
	#endif

    if(bTopActive && pTopCoord)
    {
        (*bTopActive) = (stat.status == 1) || (stat.status == 3);
		(*pTopCoord) = stat.upperCoord;
    }

    if(bBtmActive && pBtmCoord)
    {
        (*bBtmActive) = (stat.status == 2) || (stat.status == 3);
		(*pBtmCoord) = stat.bottomCoord;
    }
    return true;
 }

 int cAController::GetCaretCoord(unsigned int idx)
 {
    bool bActive;
    int Coord;
    if(GetCaretStatus(idx, &bActive, &Coord))
        return Coord;
    return 0;
 }

cScanScriptProgramm* cAController::LockManualMode()
{
	ManualModeLock.Enter();
	return &ManualScriptPrg;
}

void cAController::UnlockManualMode()
{
	ManualModeLock.Release();
}

bool cAController::WaitTuneEvent()
{
    const int WaitCaretTimeout = 2000;
	DWORD dwWaitResult;
	dwWaitResult = WaitForSingleObject(hTuneSetStateEvent,WaitCaretTimeout);
	switch(dwWaitResult)
	{
		case WAIT_OBJECT_0: return TRUE;
				   default: return FALSE;
	}
	return false;
}

bool cAController::TuneSetState( eTuneState state )
{
    TRACE("cAController::TuneSetState");
    tuneNewState = state;
    bool bRet = WaitTuneEvent();
    return bRet && (tuneNewState != tmError);
}
