﻿// КОДИРОВКА UTF-8 (без BOM)---------------------------------------------------------------------------
// scan в CYCLETYPE переименован в scancycle
//
#ifndef _motorctrlh
#define _motorctrlh

#include "LanMotorServ.h"
#include "mytmr.h"
#include "fmtstr.h"

typedef void  (__closure *VPROCV)(void);

typedef struct CARIAGESTAT 
{
    unsigned int status;                 // состояние кареток cCariageRest   или комбинация upperCariageMv bottomCariageMv
    unsigned int upperCoord;        //  текущая координата  верхней каретки (если каретка в движении, то значение является оценочным)
    unsigned int bottomCoord;
} tCARIAGESTAT;

#pragma pack(push,1)

#define blksize 255

typedef struct t_TelegramHdr
{
	 unsigned char StZ;
	 unsigned char CS;
	 unsigned char DatL;
	 unsigned char DatLW;
	 unsigned char Ctrl;
	 unsigned char Service;
	 unsigned char AdrS;     //  адрес передатчика
	 unsigned char AdrE;     //  адрес приемника
		/* unsigned char AdrES1;
		 unsigned char AdrES2;
		 unsigned char AdrES3;
		 unsigned char AdrES4;
		 unsigned char AdrES5;
		 unsigned char AdrES6;
         unsigned char AdrES7;*/
  //       unsigned char PaketN;

} tTelegramHdr;

typedef struct t_MasterDataHdr
{
	 unsigned char control;
	 unsigned char address;
	 unsigned char paramtype;
	 unsigned short paramnumber;
} tMasterDataHdr;

typedef struct t_SlaveDataHdr
{
	 unsigned char status;
	 unsigned char control;
	 unsigned char address;
} tSlaveDataHdr;


typedef struct t_Block
{
  tTelegramHdr telegramHdr;
  unsigned char data[];
} tBlock;

#pragma pack(pop)

#define AUTO_RESET_EVENT   FALSE
#define MANUAL_RESET_EVENT TRUE

#define ReadService 0x80
#define WriteService 0x8F


#define ERROR_NO 0
#define ERRORBASE ERROR_NO
//
#define ERROR_BAD_CRC  (ERRORBASE-1)
#define ERROR_TOO_SMALL_DATA1  (ERRORBASE-2)  // данных меньше, чем TelegramHeader
#define ERROR_TOO_SMALL_DATA2  (ERRORBASE-3)  // данных меньше, чем указано в TelegramHeader
#define ERROR_NO_RESULT        (ERRORBASE-4)
#define ERROR_OUTBUFFER_TOO_SMALL (ERRORBASE-5)
#define ERROR_EVENT_WAIT_ERROR (ERRORBASE-6)
#define ERROR_SERVICE_BUSY (ERRORBASE-7)
//#define ERROR_TIMEOUT (ERRORBASE-8)

enum PARAMETER1VALUE
{
                     INIT = 0,
	STOP = 19, // останов длинного движения
	LEFTMINUP = 2,    // "короткое" движение влево верхней каретки - смещение на 1 мм, не требует подачи команды останова
	RIGHTMINUP = 1,    // то же вправо
	LEFTMAXUP = 12,   // "длинное" движение влево верхней каретки - требует подачи команды останова
	RIGHTMAXUP = 11,   // то же вправо
                     PUSK = 20,
	LEFTMINBOT = 4,    // "короткое" движение влево нижней каретки - смещение на 1 мм, не требует подачи команды останова
	RIGHTMINBOT = 3,    // то же вправо
	LEFTMAXBOT = 14,   // "длинное" движение влево нижней каретки - требует подачи команды останова
	RIGHTMAXBOT = 13,   // то же вправо
};

#define breakcmd 99 // после ввода этой команды выполнение текущего движения
// прекращается
#define pwrOnCmd 97  // команда включения питания двигателей
#define pwrOffCmd 98  // команда отключения питания двигателей

//
enum CYCLETYPE  // это также входной параметр для command,кроме none
{
    none = 0, 
	scancycle = 1,                   // отработка очередного движения в ПОИСК
	adjust2 = 2,                // верхнюю каретку в положение для Юстировка 2
	adjust3 = 3,               // отработка очередного движения в Юстировка3
	cycleBreak =  4,        // останов текущего движения
	drvPwrOff  = 5,          // отключить питание двигателей - подача pwrOffCmd
    drvPwrOn  = 6,           // включить питание двигателей - подача pwrOnCmd
    caretReturn = 7,         // возвратить каретки в исходное положение
    tune = 8 // отработка  движения в НАСТРОЙКА

};

enum MOTIONSTATE // parameter to be written into p-0-1270
{
   inital = 0,    // we are here when starting, cycle ending or cycle breaking and cycle an error
   handrun = 1,
   move2     = 2, // inital->move2; cycle1->move2 
   move3    = 3,
   move4    = 4,
   move5    = 5,
   move6    = 6,
   move7    = 7,
   move8    = 8,
   move9    = 9,
   move10  = 10,
   move11  = 11,
   move12  = 12,
   move13  = 13,
   move14  = 14,
   move15  = 15,
   move21  = 21,      // верхняя каретка - вправо до конца 
   move22  = 22,      // нижняя каретка - вправо до конца 
   move23  = 23,      // нижняя каретка - влево на парковку 
   move24  = 24,      // нижняя каретка - вправо до конца 
   move25  = 25,      // нижняя каретка - влево на парковку 
   move26  = 26,      // верхняя каретка - влево на парковку 
  
   move30 = 30,      // перемещение на заданную величину относительно текущего положения
   move31 = 31       // движения режима калибровка
};

#define cariageRest     0              //   обе каретки не двигаются
#define upperCariageMv 1              //    движение верхней каретки
#define bottomCariageMv 2

const unsigned int cariageSign[24] = 
{ cariageRest,upperCariageMv, upperCariageMv, bottomCariageMv,  // 1...4
  bottomCariageMv,bottomCariageMv,upperCariageMv | bottomCariageMv,upperCariageMv | bottomCariageMv,
  bottomCariageMv,upperCariageMv | bottomCariageMv,upperCariageMv | bottomCariageMv,upperCariageMv | bottomCariageMv,
  bottomCariageMv,upperCariageMv | bottomCariageMv,upperCariageMv | bottomCariageMv,cariageRest,
  cariageRest,cariageRest,cariageRest,cariageRest,             //  // 17...20
  upperCariageMv,bottomCariageMv,bottomCariageMv,upperCariageMv             //  21...24
};

#define CYCLEERRORBASE (-16)


enum CYCLEERROR
{
	ERROR_ACTION_DENIED = CYCLEERRORBASE,
	ERROR_MOTION_NOT_COMPLETED = CYCLEERRORBASE-1,
	ERROR_SET_CMD = CYCLEERRORBASE-2,
	ERROR_GET_CMD_STATUS = CYCLEERRORBASE-3,
	ERROR_SET_HANDCMD = CYCLEERRORBASE-4,
	ERROR_HANDMOTION_NOT_COMPLETED = CYCLEERRORBASE-5,
	ERROR_GET_HANDCMD_STATUS = CYCLEERRORBASE-6,
	ERROR_CMD_CANCELLED = CYCLEERRORBASE-7,
	ERROR_WRONG_TROLLEY_POS = CYCLEERRORBASE-8,
	ERROR_SET_SHIFT_1272 = CYCLEERRORBASE-9,
	ERROR_SET_SHIFT_1273 = CYCLEERRORBASE-10,
};


 typedef struct tERRORMSG
{
	 CYCLEERROR errorNumber;
	 int  code1;
	 int  code2;
	 int  code3;
} ERRORMSG;
//
enum  CMOTIONRESULT
{
  cNone = 0,
  cToBeContinued = 1,
  cLastMotion = 2
};
//
typedef struct tMotionResult
{
   ERRORMSG error;
   int    parameter;     
} MOTIONRESULT;
// parameter = cLastMotion, если был выполнялся последнее движение из  цикла, иначе cToBeContinued

//
//    errorNumber                               code1                               code2                                                       code3
//   ERROR_MOTION_NOT_COMPLETED              MOTIONSTATE      прочитанное значение из P0        0
//   ERROR_ SET_CMD                          MOTIONSTATE**                   Cmd**                   код ошибки setCmd()                         
//   ERROR_ GET_STATUS                       MOTIONSTATE                   код ошибки cmdIsDone()                0

//   ERROR_ SET_HANDCMD                          MOTIONSTATE        Cmd                                код ошибки setCmd()

//   ERROR_HANDMOTION_NOT_COMPLETED   MOTIONSTATE       прочитанное значение из P1        ранее записанное туда значение*
//   ERROR__GET_HANDCMD_STATUS              MOTIONSTATE       код ошибки п.п чтения из P1        0
//   ERROR_HAND_RUNNING_DENIED        MOTIONSTATE             0          0
//   ERROR_WRONG_TROLLEY_POS           MOTIONSTATE       выполняемая команда (см.PARAMETER1VALUE)       текущая позиция каретки
// значение 0 говорит о том, что поле неиспользуется
//* число  -1 означает неопределенное значение
//** для ERROR_ SET_CMD code1 = code2, если Code2 != 99 

       
//
#define driveHardwareInitializationTime 40000      // required time before ethernet connecting
#define maxMotionRequiredTime 35000


#define maxPositionTrolleyToLeftFromCenter 470
#define maxPositionTrolleyToRightFromCenter 770

#define maxLeftTrolleyPosition 52 
#define maxLeftTrolleyPositionWithReserve  (maxLeftTrolleyPosition + 20)

#define maxRightUpTrolleyPosition 1305
#define maxRightUpTrolleyPositionWithReserve  (maxRightUpTrolleyPosition - 20)

#define maxRightBotTrolleyPosition 1195
#define maxRightBotTrolleyPositionWithReserve  (maxRightBotTrolleyPosition - 20)


typedef void  __fastcall (__closure *tCallBack)(tMotionResult Result);
typedef void  (__closure *LOGPROC)(char *);

class motorctrl
{
private:

	HANDLE hAnswEvent;
	void *resultBuffer;
	unsigned int resultBufferSz;
	int resultError;


    unsigned char *outcomeblk;
    unsigned char *incomeblk;

	unsigned char masterAddr;
	unsigned char slaveAddr;
	LANMOTORSERVICE *ethservice;

	DataReceived OnDataReceived;
	unsigned int motionState;
	TMR cycleTimer;
	CRITICAL_SECTION motioncr;

                     PARAMETER1VALUE v1271Parameter;

                     CYCLETYPE cycleType;
                     MOTIONSTATE nextState;
	MOTIONSTATE targetState;
                     BOOL                   fMotionCompleted;
                     tCallBack   resultProcedure; // функция, вызываемая после окончания движения. после вызова функции
// указатель обнуляется


                     VPROCV closeWaterProc;
//
	LOGPROC   log_proc;
	CRITICAL_SECTION LogCr;
//	int defaultVelocity;
//	int currentVelocity;

	void variableInit();

	void __fastcall unPack(int umuIdx,void* data, int size);
	unsigned char calcrc( tBlock* pblock, BOOL setflag);
	void send(unsigned char *databuf,unsigned char datalng,unsigned char ServiceNum);


	void setInitalState(void);
	void whenMotionsCompleted(MOTIONRESULT *presult);

	char *getErrorCodeString(enum CYCLEERROR c);
	void errorLog(ERRORMSG e);

   int setParameter1(PARAMETER1VALUE v,ERRORMSG *perrorMsg);

   int getUpperTrolleyPosition(int *pPosition);
   int getBottomTrolleyPosition(int *pPosition);
   int isReady(void);
   int setShift(BOOL fBottomTrolley,int b); 
   int getShift(BOOL fBottomTrolley,int *pShift); 

public:

   motorctrl(unsigned char master_Addr, unsigned char slave_Addr,VPROCV closewaterproc);
   ~motorctrl();

	int init(int index,cDataTransfer *pDataTransfer);
	int init1();
	void tick();

   void setLogProcedure(LOGPROC p);
   void LOG(char *fmt, ...);
//
   void setRcvProc(DataReceived proc) { OnDataReceived = proc;}

//
   init();
   int cmd_parameter(unsigned char paramtype,unsigned short paramnumber,BOOL f_write,unsigned char *dstBuf, unsigned int dstBufSize,unsigned int value,unsigned char valuesz);
   int cmd_readparam(unsigned char paramtype,unsigned short paramnumber,unsigned char *dstBuf, unsigned int dstBufSize);
   int cmd_writeparam(unsigned char paramtype,unsigned short paramnumber,unsigned short value);
   int cmd_writeparam(unsigned char paramtype,unsigned short paramnumber,unsigned int value);
   int cmd_writeparam(unsigned char paramtype,unsigned short paramnumber,unsigned short value,unsigned char *errorbuf,unsigned char errorbufsz);
   int cmd_writeparam(unsigned char paramtype,unsigned short paramnumber,unsigned int value,unsigned char *errorbuf,unsigned char errorbufsz);


   int cmd_readP0param(unsigned short paramnumber,unsigned char *dstBuf, unsigned int dstBufSize);
   void cmd_writeP0param(unsigned short paramnumber,unsigned short value);

   int getVersion(char *dststr);//UnicodeString *pdststring);
   int getControllerType(char *pdststring);
   int driverIsReady(void);
   int getLanguage(void);
   int setLanguage(unsigned char languageNumber);
   int getIPAddress(unsigned int *pIpAddress);


   int getPositioningCtrlWord(unsigned int *pvalue);
   int setPositioningCtrlWord(unsigned int value);
   int getVelocity(unsigned int *pvalue);
   int setVelocity(unsigned int value);
   int set_Velocity(unsigned int value);
   int getStartAcceleration(unsigned int *pvalue);
   int setStartAcceleration(unsigned int value);
   int getStopAcceleration(unsigned int *pvalue);
   int setStopAcceleration(unsigned int value);

   int cmdIsDone(unsigned int *pcode);
   int handCmdIsDone(unsigned int *pcode);
   int writeParameter1(PARAMETER1VALUE v,ERRORMSG *perrorMsg);
   int setCmd(unsigned int b);


   int isUpperTrolleyInCenter();
   int isTrolleyValidPos(PARAMETER1VALUE v, unsigned int *ppos);
   int isTrolleyValidPos(bool fBottomTrolley, unsigned int *ppos);

//
   void command(CYCLETYPE typ, tCallBack resultProc);
   void command(BOOL fBottomTrolley,int smesh, tCallBack resultProc);
   void command2(int bottomTrolleyShift,int upperTrolleyShift, tCallBack resultProc);


   void motionEngine(void);
//   unsigned int getDefaultVelocity(void);
//   void defineMotorVelocity(void);
   void parametersLog(void);
   int readP1351(void);
//-----------------------------------------------------------------------------------------------------------------
//   записывает состояние кареток двигателей в структуру, задаваемую pDest
//   0 нормальное завершение 
//  -1 ошибка чтения регистров контроллера двигателей 
//  -2 внутренняя ошибка - непредвиденное значение cyleType 
   int getCariagesStatus(tCARIAGESTAT *pDest); 
};


#endif



