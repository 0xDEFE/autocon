//---------------------------------------------------------------------------

#ifndef AControllerUnitH
#define AControllerUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "AController.h"
#include <Vcl.ExtCtrls.hpp>;

//---------------------------------------------------------------------------
class TAControllerForm : public TForm
{
__published:	// IDE-managed Components
	TMemo *Memo1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TTimer *Timer1;
	TLabel *Label1;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TLabel *Label2;
	TPanel *Panel1;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
private:	// User declarations
public:		// User declarations

	cAController * ac;
	cDataTransfer * DT;

	__fastcall TAControllerForm(TComponent* Owner);
    void GetMessages(TMessagesType Type, UnicodeString Text, DWord Time); // �������� ��������� �� ������
    void SearchMotion_(int WorkCycle); // ����� "�����"
    void TestMotion_(int WorkCycle);   // ����� "����"
    void AdjustMotion_(int WorkCycle); // ����� "���������"
    void TuneMotion_(int WorkCycle); // ����� "���������"

};

//---------------------------------------------------------------------------
extern PACKAGE TAControllerForm *AControllerForm;

#endif

