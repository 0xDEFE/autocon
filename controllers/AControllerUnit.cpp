//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AControllerUnit.h"
//---------------------------------------------------------------------------
//#pragma package(smart_init)
#pragma resource "*.dfm"
TAControllerForm *AControllerForm;

//---------------------------------------------------------------------------

__fastcall TAControllerForm::TAControllerForm(TComponent* Owner)
    : TForm(Owner)
{
}

//---------------------------------------------------------------------------
void __fastcall TAControllerForm::Button1Click(TObject *Sender)
{
	ac = new cAController;
	ac->SetMessagesProc(GetMessages);

	ac->SetSearchMotion(SearchMotion_); // ����� "�����"
	ac->SetTestMotion  (TestMotion_);   // ����� "����"
	ac->SetAdjustMotion(AdjustMotion_); // ����� "���������"
    ac->SetTuneMotion  (TuneMotion_);   // ����� "���������"
}
//---------------------------------------------------------------------------

void __fastcall TAControllerForm::Button2Click(TObject *Sender)
{
	DT = new cDataTransfer();
	ac->Init(DT);
}
//---------------------------------------------------------------------------

void __fastcall TAControllerForm::Button3Click(TObject *Sender)
{
	delete ac;
	ac = NULL;
	delete DT;
	DT = NULL;
	Close();
}
//---------------------------------------------------------------------------

void __fastcall TAControllerForm::Timer1Timer(TObject *Sender)
{
	if (ac)
	{

		switch (ac->GetMode())
		{
			   case acNotSet   : {Label1->Caption = "State: NotSet   "; break; }
			   case acError    : {Label1->Caption = "State: Error    "; break; }
			   case acWait     : {Label1->Caption = "State: Wait     "; break; }
			   case acReady    : {Label1->Caption = "State: Ready    "; break; }
			   case acSearch   : {Label1->Caption = "State: Search   "; break; }
			   case acTest     : {Label1->Caption = "State: Test     "; break; }
			   case acTuning   : {Label1->Caption = "State: Tuning   "; break; }
			   case acAdjusting1 : {Label1->Caption = "State: Adjusting1"; break; }
			   case acAdjusting2 : {Label1->Caption = "State: Adjusting2"; break; }
		}


//		Label2->Caption = "WorkCycle: " + IntToStr(ac->GetWorkCycle())  + " Processing: " + IntToStr((int)ac->Processing);
//		Memo1->Lines->Assign(ac->Messages);
	}
}
//---------------------------------------------------------------------------

void __fastcall TAControllerForm::Button6Click(TObject *Sender)
{
	if (ac) ac->SetMode(acTest);
}
//---------------------------------------------------------------------------

void __fastcall TAControllerForm::Button4Click(TObject *Sender)
{
	if (ac) ac->SetMode(acReady);
}
//---------------------------------------------------------------------------

void __fastcall TAControllerForm::Button7Click(TObject *Sender)
{
	if (ac) ac->SetMode(acTuning);
}
//---------------------------------------------------------------------------

void __fastcall TAControllerForm::Button8Click(TObject *Sender)
{
	if (ac) ac->SetMode(acAdjusting1);
}
//---------------------------------------------------------------------------

void __fastcall TAControllerForm::Button9Click(TObject *Sender)
{
	if (ac) ac->SetMode(acAdjusting2);
}
//---------------------------------------------------------------------------

void __fastcall TAControllerForm::Button5Click(TObject *Sender)
{
	if (ac) ac->SetMode(acSearch);
}
//---------------------------------------------------------------------------

void TAControllerForm::GetMessages(TMessagesType Type, UnicodeString Text, DWord Time) // �������� ��������� �� ������
{
    if (Type == TMessagesType::mtError) Memo1->Lines->Add("������: " + Text);
                         Memo1->Lines->Add("���������: " + Text);

}

void TAControllerForm::SearchMotion_(int WorkCycle) // ����� "�����"
{
		Label2->Caption = "WorkCycle: " + IntToStr(ac->GetWorkCycle())  + " Processing: " + IntToStr((int)ac->Processing);
}

void TAControllerForm::TestMotion_(int WorkCycle)   // ����� "����"
{
		Label2->Caption = "WorkCycle: " + IntToStr(ac->GetWorkCycle())  + " Processing: " + IntToStr((int)ac->Processing);
}

void TAControllerForm::AdjustMotion_(int WorkCycle) // ����� "���������"
{
		Label2->Caption = "WorkCycle: " + IntToStr(ac->GetWorkCycle())  + " Processing: " + IntToStr((int)ac->Processing);
}

void TAControllerForm::TuneMotion_(int WorkCycle) // ����� "���������"
{
		Label2->Caption = "WorkCycle: " + IntToStr(ac->GetWorkCycle())  + " Processing: " + IntToStr((int)ac->Processing);
}

