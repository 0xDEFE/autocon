//---------------------------------------------------------------------------

#ifndef AController_

#define AController_

#include <sstream>
#include "Windows.h"
#include "Classes.hpp"
#include "ThreadClassList.h"

#include "Autocon.inc"
#include "DataTr.h"
#include "cavtk2.h"

//Added by KirillB
#include "../Utils.h"
#include "ScanScriptParser.h"
#include "CriticalSection_Win.h"

#include "CaretMotionSim.h"

#define PORT_out_cont 44000
#define PORT_in_cont 44001

enum TMessagesType // ��� ���������
{
    mtError = 1,
    mtInfo = 2
};

typedef void (__closure *TSendMessages)(TMessagesType Type, UnicodeString Text, DWord Time); // �������� ��������� �� ������
typedef bool (__closure *TSentCavtkRequest)(cavtk *ctrl, tCavtkMsg& msg); // �������� ��������� �� ������
typedef void (__closure *TChangeWorkCycle)(int WorkCycle); // ����� �������� �����



enum TWaitMode // ������ �������� ������ �� ����������� ��������
{
   wmInit = 1,         // �������� ������ ��������, ��� �������������
   wmAdjustEnter = 2,  // �������� ����� � ����� ��������� (mrail -> madj1)
   wmTuneEnter = 3,     // �������� ����� � ����� ��������� (mrail -> mtune)
   wmReadyEnter = 4
};

enum TACState   // ������ ������ ��������� �������
{
   acNotSet = - 2,
   acError = - 1,
   acWait = 0,

   acReady = 1,  // ����� �������� - ����� ������� ����� (init)
   acSearch = 2, // ����� ����� - ����� ������ ���� � ������
   acTest = 3,   //
   acTuning = 4,
   acAdjusting1 = 5,
   acAdjusting2 = 6,
   acHandScan = 7
};

enum TACWorkMode // ������� ���������
{
   wmNone = - 1,
   wmCancel = 0,
   wmSearch = 1,
   wmTest = 2,
   wmTuning = 3,
   wmAdjusting = 4,
   wmHandScan = 5
};

enum eTuneState
{
    tmNone      = -1,
    tmInit      = 0,
    tmAutInit   = 1,
    tmManInit   = 2,
    tmAutWork   = 3,
    tmManWork   = 4,
    tmWorkBreak = 5,
    tmWorkEnd   = 6,
    tmError     = 7
};

const unsigned char TuneMachineData[8][8] = //1 - internal transition, 2 - external transition
{
    {0, 2, 2, 0, 0 , 2, 0, 1}, //tmInit
    {0, 0, 0, 1, 0 , 2, 0, 1}, //tmAutInit
    {0, 0, 0, 0, 1 , 2, 0, 1}, //tmManInit
    {2, 0, 0, 0, 0 , 2, 0, 1}, //tmAutWork
    {2, 0, 0, 0, 0 , 2, 0, 1}, //tmManWork
    {0, 0, 0, 0, 0 , 0, 1, 1}, //tmWorkBreak
    {2, 0, 0, 0, 0 , 0, 0, 1}, //tmWorkEnd
    {0, 0, 0, 0, 0 , 2, 0, 0}, //tmError
};


class cAController : public TThread
{

 private:

  void __fastcall Execute(void);
  cDataTransfer * DataTr;
  bool CtrlWork;
  TACState State;
  TACWorkMode WorkMode;
  TWaitMode WaitMode;
  bool EndWorkFlag;
  int WorkCycle;
  //--------Added by KirillB-------------
  //bool bManualWorkCycleCtrl;
  //int PrevWorkCycle;
  //int TuneWorkCycleCount;
  cCriticalSectionWin ManualModeLock;
  bool bManualMode;
  cScanScriptProgramm ManualScriptPrg;
  cScanScriptProgramm TuneScriptPrg;

  eTuneState tuneCurrState;
  eTuneState tuneNewState;
  cCriticalSectionWin TuneModeLock;
  HANDLE hTuneSetStateEvent;
  bool WaitTuneEvent();
  //-------------------------------------

  TChangeWorkCycle SearchProc; // ����� "�����"
  TChangeWorkCycle TestProc;   // ����� "����"
  TChangeWorkCycle AdjustProc; // ����� "���������"
  TChangeWorkCycle TuneProc;   // ����� "���������"
  TChangeWorkCycle ManualProc;   // ����� "������"
  TSentCavtkRequest CavtkRequestProc; //��������� ��������� cavtk

  HANDLE hCaretMoveEndEvent;

  TSendMessages MProc;


  void __fastcall MotionOKEndProcedure(MOTIONRESULT r);
  void __fastcall MotionBreakEndProcedure(MOTIONRESULT r);

  void prn(char* s);
  void debugPrn(char* s);
  void setErrorCode(unsigned int error);
  void eventSignProc(BOOL start);

public:

  bool Processing;
  cavtk *ctrl;

  __fastcall cAController(void);
  __fastcall ~cAController(void);

  bool Init(cDataTransfer * DataTr_);
  TACState GetMode(void);
  bool SetMode(TACState NewState);
  int GetWorkCycle(void);
  void SetMessagesProc(TSendMessages Proc);
  TACWorkMode GetWorkMode() {return WorkMode;};

  //-------------Add by KirillB----------------------
  bool bCatchEndOfAdj2Movement;
  //void EnableManualWorkCycleCtrl(bool bVal);
  //void SetWorkCycle(int value);
  //void SetTuneWorCycleCount(int value);
  bool CaretManualMove(int topOffset,int bottomOffset);
  bool GetCaretStatus(bool caretPos, bool* bActive, int* pCoord);
  bool GetCaretsStatus(bool* bTopActive, int* pTopCoord, bool* bBtmActive, int* pBtmCoord);
  int GetCaretCoord(unsigned int idx);
  bool MoveCaretsToStart();
  bool WaitCaretStop();
  cScanScriptProgramm* LockManualMode();
  void UnlockManualMode();
  //void SetManualMode(bool bVal) {bManualMode = bVal;};
  //void SetWorkCycle(int value);
  eTuneState TuneGetState() {return tuneCurrState;};
  bool TuneSetState( eTuneState state );
  void Debug_CallEventSignProc(bool bVal) {eventSignProc(bVal);};
  //-------------------------------------------------

  void SetSearchMotion(TChangeWorkCycle Proc); // ����� "�����"
  void SetTestMotion(TChangeWorkCycle Proc);   // ����� "����"
  void SetAdjustMotion(TChangeWorkCycle Proc); // ����� "���������"
  void SetTuneMotion(TChangeWorkCycle Proc);   // ����� "���������"
  void SetManualMotion(TChangeWorkCycle Proc);   // ����� "������"
  void SetCavtkRequestProc(TSentCavtkRequest Proc);   //��������� ��������� cavtk
};

#endif

