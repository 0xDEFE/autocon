

#ifndef LANProtAVTK
#define LANProtAVTK

#include <stdio.h>
#include <stdlib.h>
#include <queue>
#include <vector>
#include "DataTr.h"
#include "Utils.h"
#include "LanProtUMU.h"
#include "kradef.h"

#ifdef LINUX_
#include <iostream>
typedef int eAlarmAlgorithm;
#else
#include <process.h>
#include <iostream.h>
#endif

#define DbgLog
#define Dbg

//const int LanDataMaxSize = 1019;

namespace LANProtDebugAVTK
{
#ifdef LINUX_
	typedef void ( *Add_Log)(unsigned char * msg,int length,int way);
	typedef void ( *Add_Log2)(unsigned int * a, unsigned int b);
	typedef void ( *Add_Log3)(unsigned int);
#else
	typedef void __fastcall (__closure *Add_Log)(unsigned char * msg,int length,int way);
	typedef void __fastcall (__closure *Add_Log2)(unsigned int * a, unsigned int b);
	typedef void __fastcall (__closure *Add_Log3)(unsigned int);
#endif

extern	Add_Log2 onAddLog2;
extern	Add_Log onAddLog;
extern	Add_Log3 onAddLog3;

extern	BOOL useLog;


#ifdef Dbg
void addbytcnt(unsigned int val); 
unsigned int rdbytcnt(void);
void initbytcnt(void); 
void deinitbytcnt(void);
#endif

}




//---------------------------------------------------------------------------

enum tKRAReadState         							
{
		rsSwitchedOff = 0,
		rsStart,
		rsHeaderSign,                  					
		rsDataLng
};

enum Qry_State
{
		QryOff = 0xFF,                              // ��� ������� �� �� �� ��������� �������         				
		QryASensor = 1,            	           // ������ �� �� �� ��������� ����������� �������         				

		QryDSensor = 0             	           // ������ �� �� �� ��������� ����������� �������         			
};


#define  DEVICENUMBER_ID  1         					
#define  VERSION_ID  2            					
#define DEVICENUMBER_MESSAGE_LENGTH 2
#define VERSION_MESSAGE_LENGTH 4


//---------------------------------------------------------------------------

#ifdef LINUX_
typedef void (*DataReceived)(int UMUIdx, void*, int);
#else
typedef void __fastcall (__closure *DSENSOR_PROC)(unsigned char num, BOOL onstate);
typedef void __fastcall (__closure *ASENSOR_PROC)(unsigned char num, unsigned short value);
typedef void __fastcall (__closure *ERRORMESSAGE_PROC)(unsigned char majorcode, unsigned char minorcode);
typedef void __fastcall (__closure *INFO_PROC)(unsigned char *sourceBuf, unsigned int dataLength);
typedef void __fastcall (__closure *ALARM_PROC)(unsigned char num);
typedef void __fastcall (__closure *VPROCPV)(unsigned char *pDataBuf);

#endif

#pragma pack(push,1)
//
#define cHeaderLng 2
#define cHeaderSign 0xFF
#define cDataLengthMax 0xFE

typedef struct
{
	unsigned char HeaderSign;
	unsigned char Size;
	unsigned char Data[LanDataMaxSize];
} tKRA_Message;

// �������� ����� � ���� Data tKRA_Message
#define DataIDOffs 0
#define DataBodyOffs 1


#pragma pack(pop)
//
#define AUTO_RESET_EVENT FALSE
#define MANUAL_RESET_EVENT TRUE

//
class KAVT
{
private:
		//    LAN'
		cDataTransfer* hrd;                	
		tKRAReadState RState;                 	
		unsigned short alreadyReceived;  
		unsigned short toBeReceived;

		tKRA_Message* OutBlock;          	 
		CRITICAL_SECTION outBlockCr;

		std::queue <tKRA_Message> OutBuffer;
//
		int OutStIdx;                     		
		int OutEdIdx;                     	


		tKRA_Message LAN_message;
						 unsigned short ReadBytesCount;

//
		int SendTickT;
//
#ifdef LINUX_  

#else
 		CRITICAL_SECTION QrySensorCr;
                                          HANDLE QrySensorEvent;
                                          HANDLE QryDataBlockEvent;
#endif
                                          Qry_State qrystate;
	                     unsigned char qryidx;
//
		DSENSOR_PROC DSensorProc;
		ASENSOR_PROC ASensorProc;
		INFO_PROC infoMessageProc;
		ERRORMESSAGE_PROC errorMessageProc;
		ALARM_PROC alarmProc;
		VPROCPV dataBlockProc;
		DSENSOR_PROC OutputProc;
                                          VPROCPV errorReportProc;
       


//
		void Unload(void);                		
		void AddToOutBuff();        		
                                          void UnPack();

public:
		int UMUIdx;                                      //  

		KAVT(int UMUIdx_, cDataTransfer* hrd_);
		~KAVT(void);

		void StartWork(void);                            //    
		void EndWork(void);                              //    

		void Tick(void);                  //  

		void TickSend(void);
                                         void TickReceive(void); 
//
//
// SetOutput() QrySensor()  - ������ ���������� �� ������ �����
//
// ��������� �������� ��������
// num - ������������� �������
// on_state ��������� ��������� ������� - ����������/ �������  (true/false)
		void SetOutput(unsigned char num,BOOL onstate,BOOL fForced);
//
// ������ ���������:
// - ����������� ��� ����������� ������� 
// a_sign - ��� ������� true/false - ����������/����������
// num - ������������� �������
// ������� �������� ��������� � ��������� ������� � ���������������(�������) num �
// ������� qrywaittime (� ��)
// ����������:  0, ���� � ������� ��������� ������ ��������� � ��� ����������
//                        -1, �� ��������� ���������  � ������� ��������� 
//                        -2, ������ �������� �� ��������
		int QrySensor(BOOL a_signe,unsigned char num, unsigned int qrywaittime);
//
// ������������� ������, ������������ ��������� ����������� ���������� 
// �� resetDuration ��
		void setMotorReset(unsigned short resetDuration);
//
// �������� �� ���������� ��������� � ������� number, ���������� ������ number � ����� ������� alarmProc
        void sendAlarmMessage(unsigned char number);

// ������������� ������� ��������� ������� ������� ��������� � ��������� ����������� �
// ����������� �������� � �.�. 
// ���� ��������� �� ����� - �� ���������������� �������� ������ NULL
                                          void SetProcedures(  ASENSOR_PROC asproc,  
                                                                            DSENSOR_PROC dsproc, 
                                                                            INFO_PROC infoproc, 
                                                                            ALARM_PROC alarmproc,
                                                                            VPROCPV datablockproc,
                                                                             DSENSOR_PROC outputproc,
                                                                            VPROCPV errorreportproc);


// ���� ��������� �� ����� - �� �������� ������ NULL
                                          void setErrorMessageProcedure(ERRORMESSAGE_PROC  emsgproc);
//
// ���������� ������ �� ���������:
// - ��������� ������ ����������, ����� dataID = DEVICENUMBER_ID
// - ������ �� �����������, ����� dataID = VERSION_ID
		void QryInfo(unsigned int dataID); 

// ���������� ������ �� ��������� ��������� dataBlock ������ ������ �� �����������
// ������� �������� ��������� ������� qrywaittime (� ��)
// ����������:  0, ���� � ������� ��������� ������ ���������  � ��� ����������
//                        -1, �� ��������� ���������  � ������� ��������� 
//                        -2, ������ �������� �� ��������
//
                                          int sendQryDataBlockMessage(unsigned int qrywaittime); 
//
// ������ dataBlock 
// ������� �������� ��������� ������� qrywaittime (� ��)
// ���� fNewFormat = false, ���������� ���� �������������� ������, ����� - ����� ��������� (������� � ���.3.1.3 �����������)
// ����������:  0, ���� � ������� ��������� ������ ���������  � ��� ����������
//                        -1, �� ��������� ���������  � ������� ��������� 
//                        -2, ������ �������� �� ��������
//
                            int sendQryDataBlockMessage(bool fNewFormat, unsigned int qrywaittime); 

};
//
//           BUMLog
#ifdef UMUDebug      
#define UMULog 
#endif
#ifdef UMUEventLog   
#define UMULog 
#endif
#ifdef UMUInDataLog  
#define UMULog 
#endif
#ifdef UMUOutDataLog 
#define UMULog
#endif


#endif


