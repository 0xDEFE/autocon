#ifndef _TMRH
#define _TMRH

#include "Utils.h"



class TMR
{
public:

TMR() {  t_start = 0;}
//
void setTimer(void);
void setTimer(unsigned long*);
//
unsigned long getDuration(void);
unsigned long getDuration(unsigned long*);

private:
DWORD t_start;
//
};


#endif
