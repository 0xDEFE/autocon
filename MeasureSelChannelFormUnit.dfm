object MeasureSelChannelForm: TMeasureSelChannelForm
  Left = 0
  Top = 0
  Caption = 'MeasureSelChannelForm'
  ClientHeight = 204
  ClientWidth = 523
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -22
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 27
  object GridPanel1: TGridPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 517
    Height = 157
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 4
    ColumnCollection = <
      item
        Value = 33.333333436039160000
      end
      item
        Value = 33.333333436039160000
      end
      item
        Value = 33.333333127921680000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = SelPanel1
        Row = 0
      end
      item
        Column = 1
        Control = SelPanel2
        Row = 0
      end
      item
        Column = 2
        Control = SelPanel3
        Row = 0
      end
      item
        Column = 0
        Control = SelPanel4
        Row = 1
      end
      item
        Column = 1
        Control = SelPanel5
        Row = 1
      end
      item
        Column = 2
        Control = SelPanel6
        Row = 1
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -22
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    RowCollection = <
      item
        Value = 50.000000000000000000
      end
      item
        Value = 50.000000000000000000
      end>
    TabOrder = 0
    object SelPanel1: TPanel
      Left = 4
      Top = 4
      Width = 169
      Height = 74
      Align = alClient
      BevelKind = bkFlat
      BevelOuter = bvNone
      BorderWidth = 4
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object SelCheck1: TCheckBox
        AlignWithMargins = True
        Left = 7
        Top = 7
        Width = 151
        Height = 17
        Align = alTop
        Caption = 'CheckBox1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object SelPB1: TPanel
        AlignWithMargins = True
        Left = 7
        Top = 30
        Width = 151
        Height = 33
        Align = alClient
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = clRed
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
      end
    end
    object SelPanel2: TPanel
      Left = 173
      Top = 4
      Width = 169
      Height = 74
      Align = alClient
      BevelKind = bkFlat
      BevelOuter = bvNone
      BorderWidth = 4
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object SelCheck2: TCheckBox
        AlignWithMargins = True
        Left = 7
        Top = 7
        Width = 151
        Height = 17
        Align = alTop
        Caption = 'CheckBox1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object SelPB2: TPanel
        AlignWithMargins = True
        Left = 7
        Top = 30
        Width = 151
        Height = 33
        Align = alClient
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = clRed
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
      end
    end
    object SelPanel3: TPanel
      Left = 342
      Top = 4
      Width = 171
      Height = 74
      Align = alClient
      BevelKind = bkFlat
      BevelOuter = bvNone
      BorderWidth = 4
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object SelCheck3: TCheckBox
        AlignWithMargins = True
        Left = 7
        Top = 7
        Width = 153
        Height = 17
        Align = alTop
        Caption = 'CheckBox1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object SelPB3: TPanel
        AlignWithMargins = True
        Left = 7
        Top = 30
        Width = 153
        Height = 33
        Align = alClient
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = clRed
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
      end
    end
    object SelPanel4: TPanel
      Left = 4
      Top = 78
      Width = 169
      Height = 75
      Align = alClient
      BevelKind = bkFlat
      BevelOuter = bvNone
      BorderWidth = 4
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      object SelCheck4: TCheckBox
        AlignWithMargins = True
        Left = 7
        Top = 7
        Width = 151
        Height = 17
        Align = alTop
        Caption = 'CheckBox1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object SelPB4: TPanel
        AlignWithMargins = True
        Left = 7
        Top = 30
        Width = 151
        Height = 34
        Align = alClient
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = clRed
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
      end
    end
    object SelPanel5: TPanel
      Left = 173
      Top = 78
      Width = 169
      Height = 75
      Align = alClient
      BevelKind = bkFlat
      BevelOuter = bvNone
      BorderWidth = 4
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      object SelCheck5: TCheckBox
        AlignWithMargins = True
        Left = 7
        Top = 7
        Width = 151
        Height = 17
        Align = alTop
        Caption = 'CheckBox1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object SelPB5: TPanel
        AlignWithMargins = True
        Left = 7
        Top = 30
        Width = 151
        Height = 34
        Align = alClient
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = clRed
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
      end
    end
    object SelPanel6: TPanel
      Left = 342
      Top = 78
      Width = 171
      Height = 75
      Align = alClient
      BevelKind = bkFlat
      BevelOuter = bvNone
      BorderWidth = 4
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      object SelCheck6: TCheckBox
        AlignWithMargins = True
        Left = 7
        Top = 7
        Width = 153
        Height = 17
        Align = alTop
        Caption = 'CheckBox1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object SelPB6: TPanel
        AlignWithMargins = True
        Left = 7
        Top = 30
        Width = 153
        Height = 34
        Align = alClient
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = clRed
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
      end
    end
  end
  object BitBtn1: TBitBtn
    AlignWithMargins = True
    Left = 3
    Top = 166
    Width = 517
    Height = 35
    Align = alBottom
    Caption = 'Apply'
    ModalResult = 1
    TabOrder = 1
  end
end
