﻿/**
 * @file BasicUtils.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef BasicUtilsH
#define BasicUtilsH

#include <memory>
#include <map>
#include <vector>
#include <list>
#include <string>

#define CLAMP(x, min, max) ((x<min) ? min : ((x>max) ? max : x))
#define LERP(a,b,c)     (((b) - (a)) * (c) + (a))
#define SMOOTH(a,b,c)     ((a)*(c) + (b)*(1-(c)))//(((b) - (a)) * (c) + (a))
#define INRANGE(a,b,c)      (((a)>(b)) && ((a)<(c)))
#define INRANGE_EQ(a,b,c)      (((a)>=(b)) && ((a)<=(c)))

#define COLORVALUE(x,ind) (((BYTE*)(&x))[ind])

//---------------Синглтон (пусть пока поживет тут)---------
template<class T>
class Singleton
{
public:
	static T& Instance()
	{
		if(_self.get()==0)
			_self.reset(new T);
		return *_self.get();
	}

protected:
	Singleton() {};
	virtual ~Singleton() {_self.release();}
private:
	static std::unique_ptr<T> _self;
};

template <class T>
std::unique_ptr<T> Singleton<T>::_self;
//------------------------------------------------------------------------------

//-----------------Критическая секция (старая угнетает)-------------------------

#ifdef WIN32
  typedef CRITICAL_SECTION MutexType;
  #define __CurrThreadId GetCurrentThreadId

#else
  typedef pthread_mutex_t MutexType;
  #define __CurrThreadId pthread_self
#endif

class Mutex
{
public:
	inline Mutex();
	inline ~Mutex();

	inline void Lock();
	inline void Unlock();
	inline bool TryLock();
private:
	MutexType __mutex;

	//Copy protection
	Mutex(const Mutex&);
	void operator=(const Mutex&);
};

#ifdef WIN32
// Реализация через Windows API
Mutex::Mutex()             { InitializeCriticalSection(&__mutex); }
Mutex::~Mutex()            { DeleteCriticalSection(&__mutex); }
void Mutex::Lock()         { EnterCriticalSection(&__mutex); }
void Mutex::Unlock()       { LeaveCriticalSection(&__mutex); }
bool Mutex::TryLock()	   { return TryEnterCriticalSection(&__mutex); }

#else // WIN32
// UNIX версия через pthread
Mutex::Mutex()             { pthread_mutex_init(&__mutex, NULL); }
Mutex::~Mutex()            { pthread_mutex_destroy(&__mutex); }
void Mutex::Lock()         { pthread_mutex_lock(&__mutex); }
void Mutex::Unlock()       { pthread_mutex_unlock(&__mutex); }
bool Mutex::TryLock()	   { return pthread_mutex_trylock(&__mutex)!=0; }
#endif // WIN32

//Автолок для использования в функции (Lock в конструкторе, Unlock в деструкторе)
template<class LockType>
class AutoLock
{
public:
	AutoLock(LockType& lock_) : lock(lock_) {lock.Lock();};
	~AutoLock() {lock.Unlock();};

private:
	LockType& lock;
};

template<class LockType>
class AutoLockWinCS
{
public:
	AutoLockWinCS(LockType& lock_) : lock(lock_) {lock.Enter();};
	~AutoLockWinCS() {lock.Release();};

private:
	LockType& lock;
};

#define AUTO_LOCK(m) AutoLock<Mutex> __AutoLock(m);
#define AUTO_LOCK_WCS(m) AutoLockWinCS<cCriticalSectionWin> __AutoLockWinCS(m);

//------------------------------------------------------------------------------
//! Нормальный форматтер для строк
template<typename StringType, typename CharType>
inline StringType StringFormatTpl(const CharType* Str, ...)
{
	va_list args;
	va_start(args, Str);

	//Определение размера (хак)
	int len = 0;
	if(sizeof(CharType) == 1)//ANSI
		len=vsnprintf_s(" ",1,(char*)Str, args)+ 1;
	else                     //UTF8
		len=vsnwprintf_s(L" ",1,(wchar_t*)Str, args)+ 1;


	CharType* buffer = (CharType*)malloc( len * sizeof(CharType) );

	if(sizeof(CharType) == 1)//ANSI
		vsprintf_s((char*)buffer, len, (char*)Str, args);
	else                     //UTF8
		vswprintf_s((wchar_t*)buffer, len, (wchar_t*)Str, args);

	va_end(args);

	StringType RetStr;
	RetStr=buffer;
	free(buffer);
	return RetStr;
}

#define StringFormatU StringFormatTpl<UnicodeString,wchar_t>
#define StringFormatA StringFormatTpl<AnsiString,char>

#ifdef _UNICODE
	#define StringFormatStd StringFormatTpl<std::wstring, TCHAR>
#else
	#define StringFormatStd StringFormatTpl<std::string, TCHAR>
#endif

#define StringFormatStdA StringFormatTpl<std::string, char>
#define StringFormatStdW StringFormatTpl<std::wstring, wchar_t>


std::wstring CharToWCharStr(const char* str);
std::string  WCharToCharStr(const wchar_t* str);

void TraceCallStack();

class ThTest : public Singleton<ThTest>
{
public:
    ThTest()
    {
        MainThId = 0;
    };

    static void check()
    {
        if(Instance().MainThId != GetCurrentThreadId())
        {
            //TRACE("ALARMO!!! MainThId = %d, CurrThId = %d",  Instance().MainThId, GetCurrentThreadId());
            void TraceCallStack();
        };

        assert( Instance().MainThId == GetCurrentThreadId() );
    };

    static void init()
    {
        Instance().MainThId = GetCurrentThreadId();
        //TRACE("MainThId = %d",  Instance().MainThId);
    };
private:
    unsigned int MainThId;
};

const char* GetAliveFilePath(const char* paths[], const int pathCount);
const char* GetAliveFolderPath(const char* paths[], const int pathCount);


#include <System.Types.hpp>

struct sPathData
{
	UnicodeString name;
	std::vector< UnicodeString > paths;
};

//! Позволяет преобразовывать пути в зависимости от расположения папки с ресурсами. Поддерживает теги.
class PathMgr
{
public:
	PathMgr() {};
	~PathMgr() {};

	static void PushPath(const wchar_t* name, const wchar_t* path, bool bTransform = true);
	static void RemovePath(const wchar_t* name);
	static UnicodeString GetPathFromName(const wchar_t* name);

	static UnicodeString GetPath(const wchar_t* path);

protected:
	static std::map<UINT, sPathData> pathItems;
	static bool isNameCharacter(const wchar_t c);
	static bool isName(const wchar_t* name);
	static UnicodeString transformPath(const wchar_t* path);
};

//! Кольцевой буффер.
template<typename T>
class ringbuf
{
public:
    typedef T          value_type;
    typedef T         *pointer;
    typedef const T   *const_pointer;
    typedef T         &reference;
    typedef const T   &const_reference;
    typedef size_t     size_type;
    typedef ptrdiff_t  difference_type;

public:
    explicit ringbuf(unsigned int buf_size = 128) :
        _array(new T[buf_size]), _array_size(buf_size),
        _head(0),_tail(0),_used_space(0), _data_lost_cnt(0)
    {
    };
    ~ringbuf()
    {
        delete [] _array;
    };

    reference front() { return _array[_tail]; }
    reference back()  { return _array[_head]; }
    const_reference front() const { return _array[_tail]; }
    const_reference back() const  { return _array[_head]; }

    size_type size() const {return _used_space;};//{return (_head >= _tail) ? (_head - _tail) + 1 : (_array_size - _tail) + _head + 1;};
    size_type capacity() const { return _array_size; };
    bool empty() const {return _used_space == 0;};//{ return _head == _tail; };
    size_type max_size() const {
      return size_type(-1) /
                     sizeof(value_type);
    }

    reference       &operator[](size_t idx)
    {
        if( idx >= size())
        {
            assert(false);
        }
        return _array[(_tail + idx) % _array_size];
    }
    const_reference &operator[](size_t idx) const
    {
        if( idx >= size())
        {
            assert(false);
        }
        return _array[(_tail + idx) % _array_size];
    }

    void push_back(value_type &item) {
      if (!_used_space) {
        _array[_head] = item;
        _tail = _head;
        ++_used_space;
      }
      else if (_used_space != _array_size)
      {
        inc_head();
        _array[_head] = item;
      }
      else {
        // We always accept data when full
        // and lose the front()
        inc_tail();
        inc_head();
        _array[_head] = item;
        _data_lost_cnt++;
      }
    }
    void pop_front() { inc_tail(); }

    unsigned int head_idx() const {return _head;};
    unsigned int tail_idx() const {return _tail;};
    unsigned int data_lost_size(bool bResetLostCounter = false)
    {
        unsigned int tmp_cnt = _data_lost_cnt;
        if(bResetLostCounter) _data_lost_cnt = 0;
        return tmp_cnt;
    };

private:
    void inc_head()
    {
        ++ _head;
        ++ _used_space;
        if(_head >= _array_size)
            _head = 0;
    }
    void inc_tail()
    {
        ++ _tail;
        -- _used_space;
        if(_tail >= _array_size)
            _tail = 0;
    }
private:
    T* _array;
    unsigned int _array_size;
    unsigned int _head;
    unsigned int _tail;
    unsigned int _used_space;
    volatile unsigned int _data_lost_cnt;
};

template<class T>
inline T random_range(T min, T max)
{
    return ((T(rand()) * (max - min)) / T(RAND_MAX + 1)
            + min);
}

inline float isqrt_safe(float x) {return 1.f/sqrtf(x+0.0000001f);}

//! Двумерный вектор.
template<class F>
class  Vec2T
{
public:
	inline Vec2T(void) : x(0), y(0) {};
	inline Vec2T(F vx,F vy)  : x(vx), y(vy) {assert(this->isValid());}

	inline static Vec2T<F> CreateLerp( const Vec2T<F> &p, const Vec2T<F> &q, F t ) {	return p*(1.0f-t) + q*t;}

	inline Vec2T<F>& set(F nx,F ny)
	{
		x=F(nx); y=F(ny);
		assert(this->isValid());
		return *this;
	};

//-------------------UTILS--------------------------------------------------

	inline F &operator [] (unsigned int index)			{ assert(index<=1); return ((F*)this)[index]; }
	inline F operator [] (unsigned int index) const		{ assert(index<=1); return ((F*)this)[index]; }

	inline bool isZero(F e = (F)0.0) const
	{
		return  (fabs(x) <= e) && (fabs(y) <= e);
	}

	inline Vec2T<F>& flip() { x=-x;y=-y; return *this; }
	inline Vec2T<F>& zero() { x=y=0; return *this; }
	inline Vec2T<F> rot90ccw() { return Vec2T<F>(-y,x); }
	inline Vec2T<F> rot90cw() { return Vec2T<F>(y,-x); }

	bool isValid() const
	{
//		if (!NumberValid(x)) return false;
//		if (!NumberValid(y)) return false;
		return true;
	}

	inline bool equals(const Vec2T<F> & v1, F epsilon=VEC_EPSILON) const
	{
		assert(v1.isValid());
		assert(this->isValid());
		return  ((fabs(x-v1.x) <= epsilon) && (fabs(y-v1.y) <= epsilon));
	}

//-------------------END UTILS-----------------------------------------------------

//-------------------OPERATORS-----------------------------------------------------

	inline Vec2T<F> &operator=(const Vec2T<F> &src) { x=src.x; y=src.y; return *this; }
	inline bool operator==(const Vec2T<F> &vec) {return (this->x==vec.x) && (this->y==vec.y);};
	inline bool operator!=(const Vec2T<F> &vec) { return !(*this == vec); }

	//Add,Sub number
	inline Vec2T<F> operator-() const { return Vec2T<F>(-x,-y); }
	inline Vec2T<F> operator+(F k) const { return Vec2T<F>(x+k,y+k); }
	inline Vec2T<F>& operator+=(F k) { x+=k;y+=k; return *this; }
	inline Vec2T<F> operator-(F k) const { return Vec2T<F>(x-k,y-k); }
	inline Vec2T<F>& operator-=(F k) { x-=k;y-=k; return *this; }

	//Mul,Div number
	inline Vec2T<F> operator*(F k) const { return Vec2T<F>(x*k,y*k); }
	inline Vec2T<F>& operator*=(F k) { x*=k;y*=k; return *this; }
	inline Vec2T<F> operator/(F k) const { return Vec2T<F>(x/k,y/k); }
	inline Vec2T<F>& operator/=(F k) { x/=k;y/=k; return *this; }

	//Add,Sub vec2
	inline Vec2T<F> operator+(const Vec2T<F> &k) const { return Vec2T<F>(x+k.x,y+k.y); }
	inline Vec2T<F>& operator+=(const Vec2T<F> &k) { x+=k.x;y+=k.y; return *this; }
	inline Vec2T<F> operator-(const Vec2T<F> &k) const { return Vec2T<F>(x-k.x,y-k.y); }
	inline Vec2T<F>& operator-=(const Vec2T<F> &k) { x-=k.x;y-=k.y; return *this; }

	//Mul,Div vec2
	inline Vec2T<F> operator*(const Vec2T<F> &k) const { return Vec2T<F>(x*k.x,y*k.y); }
	inline Vec2T<F>& operator*=(const Vec2T<F> &k) { x*=k.x;y*=k.y; return *this; }
	inline Vec2T<F> operator/(const Vec2T<F> &k) const { return Vec2T<F>(x/k.x,y/k.y); }
	inline Vec2T<F>& operator/=(const Vec2T<F> &k) { x/=k.x;y/=k.y; return *this; }

	//Dot,Cross
	inline F operator|(Vec2T<F> &k) const { return this->dot(k); }
	inline F& operator|=(Vec2T<F> &k) { *this=this->dot(k); return *this; }
	inline Vec2T<F> operator^(Vec2T<F> &k) const { return this->cross(k); }
	inline Vec2T<F>& operator^=(Vec2T<F> &k) { *this=this->cross(k); return *this; }

//----------------END OPERATORS----------------------------------------------------

//-------------------BASIC----------------------------------------------------------
	inline void minBounds(const Vec2T<F> &other)
	{
		x = matkit::min_tpl(other.x,x);
		y = matkit::min_tpl(other.y,y);
	}

	inline void maxBounds(const Vec2T<F> &other)
	{
		x = matkit::max_tpl(other.x,x);
		y = matkit::max_tpl(other.y,y);
	}

	Vec2T<F>& normalize()
	{
		F fInvLen = isqrt_safe(x*x+y*y);
		x *= fInvLen; y *= fInvLen;
		return *this;
	}

	Vec2T<F> getNormalized()
	{
		F fInvLen = isqrt_safe(x*x+y*y);
		return *this * fInvLen;
	}

	inline F getLength() const { return sqrt(x*x+y*y); }
	inline F getLength2() const { return x*x+y*y; }

	void setLength(F fLen)
	{
		double fLenMe = getLength2();
		if(fLenMe<0.00001f*0.00001f)
			return;
		fLenMe = fLen * (1.0 / sqrt(fLenMe));
		x*=fLenMe; y*=fLenMe;
	}

	inline F getDistance(const Vec2T<F>& vec1) const
	{
		return  sqrtf((x-vec1.x)*(x-vec1.x)+(y-vec1.y)*(y-vec1.y));
	}

	inline Vec2T<F> scale(F Scale)
	{
		return Vec2T<F>(x*Scale,y*Scale);
	};

	inline void rotate(F angle)
    {
        set(x*cos(angle)-y*sin(angle),x*sin(angle)+y*cos(angle));
    };

    inline Vec2T<F> getRotated(F angle)
    {
        Vec2T<F> v(*this);
        v.rotate(angle);
        return v;
    };

    inline F getAngle()
    {
		return (F)atan2((float)y, (float)x);
    };

	inline F dot(const Vec2T<F> &rhs) const {return x*rhs.x + y*rhs.y;}
	inline F cross (const Vec2T<F> &v) const {return x*v.y - y*v.x;}
//-----------------END BASIC--------------------------------------------------------

public:
	F x;
	F y;
};


typedef Vec2T<float> Vec2f;
typedef Vec2T<int> Vec2i;
typedef Vec2T<double> Vec2r;

typedef Vec2f Vec2;

//! Полигон. Поддерживает операции смещения, вращения и масштабирования.
template<class F>
class PolyT
{
    typedef Vec2T<F> PointT;
public:
    PolyT() {};
    ~PolyT() {};

    void clear() {points.clear();}
    PointT center() const {
        PointT centerPt;
        for(int i = 0; i < points.size(); i++)
            centerPt += points[i];
        centerPt /= points.size();
        return centerPt;
    }

    bool isClockwise()
    {
        int edgesSum = 0;
        for(int i = 0; i < points.size(); i++)
        {
            int nextPt = (i == points.size() - 1) ? 0 : i + 1;
            edgesSum += (points[nextPt].x - points[i].x) * (points[nextPt].y + points[i].y);
        }
        return edgesSum > 0;
    }

    void inflate(const PointT& value) {
        bool bCw = isClockwise();
        for(int i = 0; i < points.size(); i++)
        {
            int prevPtIdx = (i == 0) ? (points.size() - 1) : i - 1;
            int nextPtIdx = (i == points.size() - 1) ? 0 : i + 1;
            Vec2f pt(points[i].x,points[i].y);
            Vec2f nextPt(points[nextPtIdx].x,points[nextPtIdx].y);
            Vec2f prevPt(points[prevPtIdx].x,points[prevPtIdx].y);

            Vec2f n1 = (pt - prevPt).rot90cw().getNormalized();
            Vec2f n2 = (nextPt - pt).rot90cw().getNormalized();
            Vec2f normal = ((n1 + n2) / 2.f).getNormalized();

            if(bCw)
                normal *= -1;
            pt = pt + normal * Vec2f(value.x,value.y);

            points[i] = PointT(pt.x, pt.y);
        }
    }

    void inflate(F value)
    {
        inflate(PointT(value, value));
    }

    void deflate(F value) {
        inflate(-value);
    }

    void grow(const PointT& value)
    {
        inflate(value);
//        PointT centerPt = center();
//        for(int i = 0; i < points.size(); i++)
//        {
//            PointT inc;
//            PointT fromCenter = points[i] - centerPt;
//            inc.x = (fromCenter.x < 0) ? -value.x : (fromCenter.x > 0) ? value.x : 0;
//            inc.y = (fromCenter.y < 0) ? -value.y : (fromCenter.y > 0) ? value.y : 0;
//
//            points[i] += inc;
//        }
    }

    TRect bounds() const
    {
        TRect rect;
        if(!isValid())
            return rect;

        rect = TRect( points[0].x,points[0].y, points[0].x,points[0].y);
        for(int i = 1; i < points.size(); i++)
        {
            rect.left = std::min<F>(rect.left,points[i].x);
            rect.right = std::max<F>(rect.right,points[i].x);
            rect.top = std::min<F>(rect.top,points[i].y);
            rect.bottom = std::max<F>(rect.bottom,points[i].y);
        }
        return rect;
    }

    void offset(PointT val)
    {
        for(int i = 0; i < points.size(); i++)
            points[i] += val;
    }

    void rotate(float value)
    {
        Vec2f centerF( center().x, center().y);
        for(int i = 0; i < points.size(); i++)
        {
            Vec2f pointF (points[i].x,points[i].y);
            Vec2f fromCenter = pointF - centerF;
            fromCenter.rotate(value);
            Vec2f res = fromCenter + centerF;
            points[i] = PointT(res.x,res.y);
        }
    }

    bool isValid() const {
        for(int i = 0; i < points.size(); i++)
        {
            if(!points[i].isValid())
                return false;
        }
        return points.size() >= 3;
    }

    bool pointInside(const PointT& pt) const
    {
        static const int q_patt[2][2]= { {0,1}, {3,2} };
        if(!isValid())
            return false;

        std::vector<PointT>::const_iterator end=points.end();
        PointT pred_pt=points.back();
        pred_pt.x-=pt.x;
        pred_pt.y-=pt.y;

        int pred_q=q_patt[pred_pt.y<0][pred_pt.x<0];

        int w=0;

        for(std::vector<PointT>::const_iterator iter=points.begin(); iter!=points.end(); ++iter)
        {
            PointT cur_pt = *iter;

            cur_pt.x-=pt.x;
            cur_pt.y-=pt.y;

            int q=q_patt[cur_pt.y<0][cur_pt.x<0];

            switch (q - pred_q)
            {
                case -3:
                    ++w; break;
                case 3:
                    --w; break;
                case -2:
                    if(pred_pt.x * cur_pt.y >= pred_pt.y * cur_pt.x)
                        ++w;
                    break;
                case 2:
                    if(!(pred_pt.x * cur_pt.y >= pred_pt.y * cur_pt.x))
                        --w;
                    break;
            }

            pred_pt = cur_pt;
            pred_q = q;
        }

        return w!=0;
    }

public:
    std::vector<PointT> points;
};

#endif
