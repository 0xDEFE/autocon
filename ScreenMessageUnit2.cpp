//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ScreenMessageUnit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TScreenMessageForm2 *ScreenMessageForm2;
//---------------------------------------------------------------------------
__fastcall TScreenMessageForm2::TScreenMessageForm2(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TScreenMessageForm2::FormCreate(TObject *Sender)
{
	NeedUpdate = false;
}
//---------------------------------------------------------------------------
void __fastcall TScreenMessageForm2::FormShow(TObject *Sender)
{
	//StaticText1->Caption = "1243432fe\nstr2\r\nstr3";
	//Width = Screen->Width;
	//Height = Screen->Height;
	//MessageMemo->Left = (Screen->Width - MessageMemo->Width) / 2;
	//MessageMemo->Top = (Screen->Height - MessageMemo->Height) / 2;
}
//---------------------------------------------------------------------------
void __fastcall TScreenMessageForm2::FormDestroy(TObject *Sender)
{
//	delete Text;
}
//---------------------------------------------------------------------------
void __fastcall TScreenMessageForm2::Timer1Timer(TObject *Sender)
{
    //Remove outdated messages
    const int showTextInterval = 4000;
	int i = MessageList.size() - 1;
	while (i >= 0)
	{
		DWORD tmp = GetTickCount();
		if ((int)tmp - (int)MessageList[i].Time > showTextInterval)
		{
			//EnableParent
			if(MessageList[i].pParent)
				MessageList[i].pParent->Enabled = true;
			MessageList.erase(MessageList.begin() + i);
			NeedUpdate = true;
		}
		i--;
	}

	//----------------Draw progress image--------------------------
	if(MessageList.size())
	{
		PaintBox1->Canvas->Brush->Color = clWhite;
		PaintBox1->Canvas->FillRect(TRect(0,0,PaintBox1->Width,PaintBox1->Height));

		int workAngleDeg = (float(GetTickCount()-MessageList[0].Time)/(float)showTextInterval) * 360;
		if(workAngleDeg > 360)
			workAngleDeg = 360;
		const float PI = 3.14159265359;
		if(workAngleDeg<0)
			workAngleDeg+=360;
		float workAngle = float(workAngleDeg)* (PI/180.f);

		float posX_end = -sin(workAngle);
		float posY_end = +cos(workAngle);
		TRect drawRect = PaintBox1->ClientRect;
		drawRect.Inflate(-5,-5);
		posX_end = posX_end * drawRect.Width()/2 + PaintBox1->Width/2;
		posY_end = posY_end * drawRect.Height()/2 + PaintBox1->Height/2;

		SetArcDirection(PaintBox1->Canvas->Handle,AD_CLOCKWISE);

		PaintBox1->Canvas->Pen->Width = 2;
		PaintBox1->Canvas->Pen->Color = clBlue;
		PaintBox1->Canvas->Arc(drawRect.left,drawRect.top,drawRect.right,drawRect.bottom, 	//Draw rectangle
							drawRect.left+drawRect.Width()/2,drawRect.bottom,		//Start pos
							posX_end,posY_end);					//End pos

		//Image1->Canvas->FillRect(TRect(0,0,20,20));
	}
	//--------------------------------------------------------------

	NeedUpdate = NeedUpdate || (LastCount != (int)MessageList.size());

	if (MessageList.size() != 0)
	{
		if (NeedUpdate || (!Visible))
		{
			StaticText1->Caption = "";
			//MessageMemo->Lines->Clear();
			for (unsigned int i = 0; i < MessageList.size(); i++)
				AddColoredStr(MessageList[i].Text, MessageList[i].TextColor, MessageList[i].BGColor);

			NeedUpdate = false;
			Visible = true;
			BringToFront();
		}
	}
	else
    {
		  Visible = false;
          Timer1->Enabled = false;
    }

	LastCount = MessageList.size();
	//Panel1->Caption = "Text->Count: " + IntToStr((int)MessageList.size());

	if (Visible) BringToFront();
}
//---------------------------------------------------------------------------
void __fastcall TScreenMessageForm2::Add(TMessagesType Type, UnicodeString Text_, DWord Time, TForm* pParent)
{
	int i;
	tMessageItem2 tmp;

	tmp.Text = Text_;
	tmp.Time = Time;
	tmp.Type = Type;
	tmp.pParent = pParent;

	if (Type == TMessagesType::mtError)
	{
		tmp.TextColor = clWhite;
		tmp.BGColor = clRed;
	}
	else
	{
		tmp.TextColor = clBlack;
		tmp.BGColor = clWhite;
	}

	MessageList.push_back(tmp);

	NeedUpdate = true;

	//Disable parent
	if(pParent)
		pParent->Enabled = false;

    Timer1->Enabled = true;
}
//---------------------------------------------------------------------------

int __fastcall TScreenMessageForm2::AddColoredStr(UnicodeString str, TColor TextColor, TColor BGColor)
{
	StaticText1->Caption = str;



	/*int Line = MessageMemo->Lines->Add(str); //��������� ������
	MessageMemo->SelStart = MessageMemo->Lines->Text.Length() - str.Length() - 2; //������������� ������ ���������
	MessageMemo->SelLength = str.Length(); // ������� ��������

	TCharFormat2 lFmt;

	memset(&lFmt.cbSize, 0, sizeof(TCharFormat2));
	lFmt.cbSize = sizeof(TCharFormat2);

	//Background Color
	lFmt.crBackColor = BGColor;
	lFmt.dwMask = CFM_BACKCOLOR;
	SendMessage(MessageMemo->Handle, EM_SETCHARFORMAT, SCF_SELECTION, Integer(&lFmt));

	//Text Color
	lFmt.crTextColor = TextColor;
	lFmt.dwMask = CFM_COLOR;
	SendMessage(MessageMemo->Handle, EM_SETCHARFORMAT, SCF_SELECTION, Integer(&lFmt));

	MessageMemo->SelLength = 0;
	return Line;
	*/
	return 0;
}
void __fastcall TScreenMessageForm2::OnSelectionChange(TObject *Sender)
{
	//MessageMemo->SelLength = 0;
}
//---------------------------------------------------------------------------
void __fastcall TScreenMessageForm2::OnPanelClick(TObject *Sender)
{
	DWORD time = GetTickCount();

	while(MessageList.size())
	{
		if(MessageList.back().Time < time)
		{
			if(MessageList.back().pParent)
				MessageList.back().pParent->Enabled = true;
			MessageList.pop_back();
		}
		else
			break;
	}

	NeedUpdate = false;
	Visible = false;
}
//---------------------------------------------------------------------------

