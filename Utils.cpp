

#include "Utils.h"
#include "BasicUtils.h"
#include "AutoconLogging.h"
#include <memory>

bool bTraceEnabled = true;
bool EnableTRACE(bool bVal)
{
    bool bOld = bTraceEnabled;
    bTraceEnabled = bVal;
    return bOld;
}

void TRACE(const char* Str, ...)
{
    if(!bTraceEnabled)
        return;
	va_list args;
	va_start(args, Str);

	//����������� ������� (���)
	int len=vsnprintf_s(" ",1,Str, args)+ 1;
	char* buffer = (char*)malloc( len * sizeof(char) );
	vsprintf_s(buffer, len, Str, args);   //���������� ��������������
	va_end(args);

    std::string msgstr = StringFormatStdA("[%d] %s",GetCurrentThreadId(), buffer);
    len = msgstr.size();

    for(int i = 0; i < len; i+=1024)
	{
        const char* ptr = msgstr.c_str() + i;
		OutputDebugStringA(ptr);
	}

    std::wstring str = CharToWCharStr(msgstr.c_str());
    LOGINFO("Trace: %s",str.c_str());

	free(buffer);
}
