//---------------------------------------------------------------------------

#pragma hdrstop

#include "Utils.h"
#include "Logger.h"

using namespace Logger;
//---------------------------------------------------------------------------
//#pragma package(smart_init)


_tstring sLoggerMessage::toString()
{
	_tstring MsgType;

	switch(type)
	{
	case Logger::LOG_MSG_INFO: MsgType=_T("Message"); break;
	case Logger::LOG_MSG_WARNING: MsgType=_T("Warning"); break;
	case Logger::LOG_MSG_ERROR: MsgType=_T("ERROR"); break;
	case Logger::LOG_MSG_CRITICAL: MsgType=_T("CRITICAL ERROR"); break;
	case Logger::LOG_MSG_UNKNOWN: MsgType=_T("Unknown"); break;
	default:
		MsgType=_T("User");

		//MsgType+=strkit::toString(type);
		MsgType+=_T(")");
		break;
	};

	tm * timeinfo = localtime ( &msgTime );

	return StringFormatStd(_T("[%d:%d.%d]\t%s: %s\r\n"),timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec,MsgType.c_str(),data.c_str());
};

//---------------------------------------------------------------------------

CLoggerCore::CLoggerCore()
{
	messageLimit=256;

	MsgWarningCnt=0;
	MsgErrorCnt=0;
	MsgInfoCnt=0;
	MsgUnknownCnt=0;
}
CLoggerCore::~CLoggerCore()
{

}

int CLoggerCore::_findListener(CLoggerListener* pListener)
{
	for(unsigned int i=0;i<allListeners.size();i++)
	{
		if(allListeners[i] ==  pListener)
			return i;
	}
	return -1;
}

void CLoggerCore::addListener(CLoggerListener* pListener)
{
	AUTO_LOCK(msgLock);

	if(_findListener(pListener)!=-1)
	{
		return;
	};

	allListeners.push_back(pListener);
	return;
}
void CLoggerCore::removeListener(CLoggerListener* pListener)
{
	AUTO_LOCK(msgLock);

	ListenersArray::iterator it=allListeners.begin();
	while(it!=allListeners.end())
	{
		if((*it)==pListener)
			it=allListeners.erase(it);
		else
			it++;
	}
	return;
}

void CLoggerCore::addMessage(UINT channelMask, int type, UINT color, const TCHAR* str)
{
	AUTO_LOCK(msgLock);

	sLoggerMessage message;
	message.channel=channelMask;
	message.data=str;
	message.type=type;
	message.color=color;
	message.threadId=__CurrThreadId();
	time(&message.msgTime);
	//GetLocalTime(&message.time);

	if(type==LOG_MSG_INFO) MsgInfoCnt++;
	else if(type==LOG_MSG_WARNING) MsgWarningCnt++;
	else if(type==LOG_MSG_ERROR) MsgErrorCnt++;
	else if(type==LOG_MSG_UNKNOWN) MsgUnknownCnt++;
	else MsgOtherCnt++;


	if(allListeners.size()==0)//NO LISTENERS
	{
		OutputDebugString(message.toString().c_str());
	};


	 for(unsigned int i=0;i<allListeners.size();i++)
	 {
		if(allListeners[i]->getChannelsMask() & channelMask)
		{
			allListeners[i]->processMessage(message);
		}
	 }

	messages.push_back(message);
	while(messages.size()>messageLimit)
		messages.pop_front();
}

void CLoggerCore::addMessageVA(UINT channelMask, int type, UINT color, const TCHAR* str, va_list& args)
{
	//Определение размера (хак)
	#ifdef _UNICODE
	int len=vsnwprintf_s(L" ",1,str, args)+ 1;
	#else
	int len=vsnprintf_s(" ",1,str, args)+ 1;
	#endif

	//int len=_vscprintf(str, args)+ 1;
	TCHAR* buffer = (TCHAR*)malloc( len * sizeof(TCHAR) );

	#ifdef _UNICODE
	vswprintf_s(buffer, len, str, args);   //применение форматирования
	#else
	vsprintf_s(buffer, len, str, args);   //применение форматирования
	#endif

	addMessage(channelMask,type,color,buffer);

	free( buffer );
}

void CLoggerCore::addMessageFmt(UINT channelMask, int type, unsigned int color, const TCHAR* str, ...)
{
	va_list args;
	va_start(args, str);
	addMessageVA(channelMask,type,color,str,args);
	va_end(args);
}

void CLoggerCore::addPersistentMessage( UINT hash, UINT channelMask, int type, unsigned int color, const TCHAR* str)
{
	AUTO_LOCK(msgLock);

	sLoggerMessage message;
	message.channel=channelMask;
	message.data=str;
	message.type=type;
	message.color=color;
	message.threadId=__CurrThreadId();
	message.persistId = hash;
	time(&message.msgTime);

	for(unsigned int i=0;i<allListeners.size();i++)
	{
		if(allListeners[i]->getChannelsMask() & channelMask)
		{
			allListeners[i]->processMessage(message);
		}
	}
}
void CLoggerCore::addPersistentMessageVA( UINT hash, UINT channelMask, int type, unsigned int color, const TCHAR* str, va_list& args)
{
	//Определение размера (хак)
	#ifdef _UNICODE
	int len=vsnwprintf_s(L" ",1,str, args)+ 1;
	#else
	int len=vsnprintf_s(" ",1,str, args)+ 1;
	#endif

	//int len=_vscprintf(str, args)+ 1;
	TCHAR* buffer = (TCHAR*)malloc( len * sizeof(TCHAR) );

	#ifdef _UNICODE
	vswprintf_s(buffer, len, str, args);   //применение форматирования
	#else
	vsprintf_s(buffer, len, str, args);   //применение форматирования
	#endif

	addPersistentMessage(hash, channelMask,type,color,buffer);

	free( buffer );
}
void CLoggerCore::addPersistentMessageFmt( UINT hash, UINT channelMask, int type, unsigned int color, const TCHAR* str, ...)
{
	va_list args;
	va_start(args, str);
	addPersistentMessageVA(hash, channelMask,type,color,str,args);
	va_end(args);
}

//----------------------------------------------------------

void Logger::Info(UINT channelMask,const TCHAR* str, ...)
{
	va_list args;
	va_start(args, str);
	GetLogger().addMessageVA(channelMask,Logger::LOG_MSG_INFO,0xFF00FF00,str,args);
	va_end(args);
}
void Logger::Warning(UINT channelMask, const TCHAR* str, ...)
{
	va_list args;
    va_start(args, str);
	GetLogger().addMessageVA(channelMask,Logger::LOG_MSG_WARNING,0xFF00FFFF,str,args);
	va_end(args);
}
void Logger::Error(UINT channelMask, const TCHAR* str, ...)
{
	va_list args;
    va_start(args, str);
	GetLogger().addMessageVA(channelMask,Logger::LOG_MSG_ERROR,0xFF0000FF,str,args);
	va_end(args);
}
void Logger::CriticalError(UINT channelMask, const TCHAR* str, ...)
{
	va_list args;
    va_start(args, str);
	GetLogger().addMessageVA(channelMask,Logger::LOG_MSG_CRITICAL,0xFFFF00FF,str,args);
	va_end(args);
}
void Logger::Message(UINT channelMask, int type,UINT color, const TCHAR* str, ...)
{
	va_list args;
	va_start(args, str);
	GetLogger().addMessageVA(channelMask,type,color,str,args);
	va_end(args);
}


void Logger::PersistentInfo(UINT hash, UINT channelMask,const TCHAR* str, ...)
{
    va_list args;
	va_start(args, str);
	GetLogger().addPersistentMessageVA(hash, channelMask,Logger::LOG_MSG_INFO,0xFF00FF00,str,args);
	va_end(args);
}
//-----------------------Logger listener----------------------------------------

CLoggerListener::CLoggerListener(void)
{
	MaxMessageCount=255;
	MessageLostCount=0;
	channelsMask=NULL;
	hasPersistentVariables=false;

	Logger::GetLogger().addListener(this);
}

CLoggerListener::CLoggerListener(UINT channelMask)
{
	MaxMessageCount=255;
	MessageLostCount=0;
	channelsMask=channelMask;
	hasPersistentVariables=false;

	Logger::GetLogger().addListener(this);
};
CLoggerListener::~CLoggerListener()
{
	GetLogger().removeListener(this);
};

bool CLoggerListener::isNewPersistendData()
{
	return hasPersistentVariables;
}

bool LoggerMessageCompareFunc(sLoggerMessage& p1, sLoggerMessage& p2)
{
	return (p1.data) > (p2.data);
}

bool CLoggerListener::readPersistentData(sLoggerMessage** ppData, UINT* pSize, bool bSort)
{
	UINT tmpSize = persistVariables.size();

	(*ppData) = new sLoggerMessage[tmpSize];
	(*pSize)  = tmpSize;

	std::map<UINT,sLoggerMessage>::iterator it = persistVariables.begin();
	for(unsigned int i = 0; (i < tmpSize) && (it != persistVariables.end()); i++, it++)
	{
		(*ppData)[i] = it->second;
	}

	if(!bSort)
		return true;

	//sorting
	std::sort((*ppData), (*ppData)+*pSize,LoggerMessageCompareFunc);

	return true;
}

void CLoggerListener::push(sLoggerMessage& message)
{
	AUTO_LOCK(arrayLock);

	if(message.persistId != 0)
	{
		persistVariables[ message.persistId ] =  message;
		hasPersistentVariables = true;
	}
	else dataList.push(message);

	while(dataList.size()>MaxMessageCount)
	{
		int t=dataList.size();
		MessageLostCount++;
		dataList.pop();
	}
}
bool CLoggerListener::pop(sLoggerMessage& message)
{
	AUTO_LOCK(arrayLock);

	if(dataList.empty())
		return false;
	message=dataList.front();
	dataList.pop();

	return true;
}

void CLoggerListener::processMessage(sLoggerMessage& message)
{
	eListenerCollectType collectType=getCollectType();

	if((collectType==LCT_CALL_ON_MESSAGE) || (collectType==LCT_BOTH))
		if(message.persistId == 0)
			onNewMessage(message);
		else
			onPersistMessage(message);
	if((collectType==LCT_PUSH_MESSAGE) || (collectType==LCT_BOTH))
		push(message);

}

void CLoggerListener::setChannelByIndex(int channelIndex, bool bEnable)
{
	if(bEnable)
		channelsMask |=   1 << channelIndex;  //set bit
	else
		channelsMask &= ~(1 << channelIndex); //clear bit
}
void CLoggerListener::setChannelByMask(UINT channelMask, bool bEnable)
{
	if(bEnable)
		channelsMask |=   channelMask;  //set bits
	else
		channelsMask &= ~(channelMask); //clear bits
}
void CLoggerListener::setChannelMask(UINT channelMask)
{
	this->channelsMask = channelMask;
}
