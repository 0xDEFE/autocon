//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AutoconMain.h"
extern cAutoconMain *AutoconMain;

#include "ArchiveSelectDialUnit.h"
#include "CalibrationFormUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TCalibrationForm *CalibrationForm;
//---------------------------------------------------------------------------
__fastcall TCalibrationForm::TCalibrationForm(TComponent* Owner)
    : TForm(Owner)
{
    TableVisibleItemsMask = CALIB_DEFAULT_TABLE_VISIBLE;
    TableEditableItemsMask = CALIB_DEFAULT_TABLE_EDITABLE;

    changesCount = 0;
}
//---------------------------------------------------------------------------
void __fastcall TCalibrationForm::CloseBtnClick(TObject *Sender)
{
    this->ModalResult = mrClose;
}
//---------------------------------------------------------------------------
void __fastcall TCalibrationForm::FormShow(TObject *Sender)
{
    UpdateTable();
}
//---------------------------------------------------------------------------

void TCalibrationForm::UpdateCalibrationData(bool bFromDbToTable)
{
    sChannelDescription chanDesc;
    sScanChannelDescription scanChanDesc;

    if(bFromDbToTable)
    {
        currCalibData.clear();
        //Adding channels
        for (unsigned int i = 0; i < AutoconMain->Table->Count(); i++)
        {
            CID chId;
            if(!AutoconMain->Table->CIDByIndex(&chId,i))
                continue;
            if(!AutoconMain->Table->ItemByCID(chId, &chanDesc))
                continue;
            bool bIsScanChannel = AutoconMain->Config->ScanChannelExists(chId);
            bool bIsHandChannel = AutoconMain->Config->HandChannelExists(chId);
            if(!(bIsScanChannel || bIsHandChannel))
                continue;

            sCalibData channel;
            channel.ID = chId;
            channel.itemNumber = currCalibData.size() + 1;
            channel.TVG = AutoconMain->Calibration->GetTVG(dsNone, chId);
            channel.PrismDelay = float(AutoconMain->Calibration->GetPrismDelay(dsNone, chId)) / 10.f;
            channel.DataTime = AutoconMain->Calibration->GetCalibrationDateTime(dsNone, chId);
            channel.Mark = AutoconMain->Calibration->GetMark(dsNone, chId);
            channel.GateCount = chanDesc.cdGateCount;

            for(int j = 0; j < 2; j++)
            {
                channel.StGate[j] = AutoconMain->Calibration->GetStGate(dsNone, chId, j);
                channel.EdGate[j] = AutoconMain->Calibration->GetEdGate(dsNone, chId, j);
            }

            for(int j = 0; j < 2; j++)
            {
                channel.Sens[j] = AutoconMain->Calibration->GetSens(dsNone, chId, j);
                channel.Gain[j] = AutoconMain->Calibration->GetGain(dsNone, chId, j);
                channel.Calibrated[j] = AutoconMain->Calibration->GetState(dsNone, chId, j);
            }

            channel.ChannelName = chanDesc.Title;
            if(bIsHandChannel)
                channel.ChannelName = L"���. " + channel.ChannelName;

            channel.KpNumber = -1;
            if(AutoconMain->Config->getFirstSChannelbyID(chId, &scanChanDesc) != -1)
            {
                channel.KpNumber = scanChanDesc.BScanGroup;
            }

            currCalibData.push_back(channel);
        }
    }
    else
    {
        //if(currCalibData.size() != AutoconMain->Table->Count())
        //    return;
        for (unsigned int i = 0; i < currCalibData.size(); i++)
        {
            sCalibData& channel = currCalibData[i];
            AutoconMain->Calibration->SetTVG(dsNone, channel.ID,channel.TVG);
            AutoconMain->Calibration->SetPrismDelay(dsNone, channel.ID,channel.PrismDelay * 10.f);
            AutoconMain->Calibration->SetCalibrationDateTime(dsNone, channel.ID,channel.DataTime);
            AutoconMain->Calibration->SetMark(dsNone, channel.ID,channel.Mark);

            for(int j = 0; j < channel.GateCount; j++)
            {
                AutoconMain->Calibration->SetStGate(dsNone, channel.ID, j, channel.StGate[j]);
                AutoconMain->Calibration->SetEdGate(dsNone, channel.ID, j, channel.EdGate[j]);
            }

            for(int j = 0; j < channel.GateCount; j++)
            {
                AutoconMain->Calibration->SetSens(dsNone, channel.ID, j, channel.Sens[j]);
                AutoconMain->Calibration->SetGain(dsNone, channel.ID, j, channel.Gain[j]);
                AutoconMain->Calibration->SetState(dsNone, channel.ID, j, channel.Calibrated[j]);
            }
        }
    }

    changesCount = 0;

    m_CurrCalibName->Caption = UnicodeString("������� ���������: ") + AutoconMain->Calibration->GetName();
}

void TCalibrationForm::UpdateTable()
{
    UpdateCalibrationData();

    int colCount = 0;
    for(int i = 0; i < CALIB_COL_END; i++)
    {
        if(TableVisibleItemsMask & (1 << i)) //Check if column in mask
            colCount++;
    }

    CalibTable->ColCount = colCount;
    CalibTable->RowCount = currCalibData.size() + 1;

    //UnicodeString tmpStr[3];

    for(int i = 0; i < CalibTable->RowCount; i++)
    {
        UpdateTableItem(i);
    }

    AdjustTableColumnsWidth();
}

void TCalibrationForm::UpdateTableItem(int idx)
{
    for(int j = 0,ci=0; j < CALIB_COL_END; j++)
    {
        if(!(TableVisibleItemsMask & (1 << j)))
            continue;
        eCailbCol currCol = (eCailbCol)j;

        CalibTable->Cells[ci][idx] = idx ? GetTableItem(idx-1, currCol) :
                                        GetCalibColName(currCol);
        ci++;
    }
}

//-----------------------Table item management-------------------------------------------

UnicodeString TCalibrationForm::GetTableItem(unsigned int idx, eCailbCol col, int colSubIdx)
{
    if(idx >= currCalibData.size())
        return L"";

    sCalibData& item = currCalibData[idx];

    UnicodeString tmpStr;
    switch(col)
    {
        case CALIB_COL_ID:
                return StringFormatU(L"0x%X",item.ID);
        case CALIB_COL_NUMBER:
                return StringFormatU(L"%d",item.itemNumber);
        case CALIB_COL_CHNAME:
                return item.ChannelName;
        case CALIB_COL_KPNUM:
                return (item.KpNumber<1) ? UnicodeString(L"-") : IntToStr(item.KpNumber);
        case CALIB_COL_TVG:
                return IntToStr(item.TVG);
        case CALIB_COL_PRISM_DELAY:
                return FloatToStr(item.PrismDelay);
        case CALIB_COL_SENS:
                if(colSubIdx != -1)
                    return IntToStr( item.Sens[colSubIdx] );
                for(int i = 0; i < item.GateCount; i++)
                    tmpStr += (i ? L", " : L"") + IntToStr( item.Sens[i] );
                return tmpStr;
//                return StringFormatU(L"%d, %d", item.Sens[0],item.Sens[1]);
        case CALIB_COL_GAIN:
                if(colSubIdx != -1)
                    return IntToStr( item.Gain[colSubIdx] );
                for(int i = 0; i < item.GateCount; i++)
                    tmpStr += (i ? L", " : L"") + IntToStr( item.Gain[i] );
                return tmpStr;
                //return StringFormatU(L"%d, %d", item.Gain[0],item.Gain[1]);
        case CALIB_COL_ST_GATE:
                if(colSubIdx != -1)
                    return IntToStr( item.StGate[colSubIdx] );
                for(int i = 0; i < item.GateCount; i++)
                    tmpStr += (i ? L", " : L"") + IntToStr( item.StGate[i] );
                return tmpStr;
                //return StringFormatU(L"%d, %d, %d, %d", item.StGate[0],item.StGate[1],
                //                                        item.StGate[2],item.StGate[3]);
        case CALIB_COL_ED_GATE:
                if(colSubIdx != -1)
                    return IntToStr( item.EdGate[colSubIdx] );
                for(int i = 0; i < item.GateCount; i++)
                    tmpStr += (i ? L", " : L"") + IntToStr( item.EdGate[i] );
                return tmpStr;
                //return StringFormatU(L"%d, %d, %d, %d", item.EdGate[0],item.EdGate[1],
                //                                        item.EdGate[2],item.EdGate[3]);
        case CALIB_COL_IS_CALIB:
                if(colSubIdx != -1)
                    return item.Calibrated[colSubIdx] ? L"��" : L"���";
                for(int i = 0; i < item.GateCount; i++)
                    tmpStr += (i ? L", " : L"") + UnicodeString(item.Calibrated[i] ? L"��" : L"���");
                return tmpStr;
                //return StringFormatU(L"%s, %s", item.Calibrated[0] ? L"��" : L"���",
                //                                item.Calibrated[1] ? L"��" : L"���");
        case CALIB_COL_MARK:
                return IntToStr(item.Mark);
        case CALIB_COL_DATETIME:
                return DateTimeToStr(item.DataTime);
    }
    return "Unknown";
}

void TCalibrationForm::SetTableItem(unsigned int idx, eCailbCol col, UnicodeString val, int colSubIdx)
{
    if(idx >= currCalibData.size())
        return;

    if(val == GetTableItem(idx, col, colSubIdx))
        return;

    sCalibData& item = currCalibData[idx];

    switch(col)
    {
        case CALIB_COL_ID:
                TryStrToInt(val,item.ID);
                break;
        case CALIB_COL_NUMBER:
                TryStrToInt(val,item.itemNumber);
                break;
        case CALIB_COL_CHNAME:
                item.ChannelName = val;
                break;
        case CALIB_COL_KPNUM:
                item.KpNumber = (val==L"-") ? -1 : StrToInt(val);
                break;
        case CALIB_COL_TVG:
                TryStrToInt(val,item.TVG);
                break;
        case CALIB_COL_PRISM_DELAY:
                TryStrToFloat(val,item.PrismDelay);
                break;
        case CALIB_COL_SENS:
                if(colSubIdx != -1)
                    TryStrToInt(val,item.Sens[colSubIdx]);
                break;
        case CALIB_COL_GAIN:
                if(colSubIdx != -1)
                    TryStrToInt(val,item.Gain[colSubIdx]);
                break;
        case CALIB_COL_ST_GATE:
                if(colSubIdx != -1)
                    TryStrToInt(val,item.StGate[colSubIdx]);
                break;
        case CALIB_COL_ED_GATE:
                if(colSubIdx != -1)
                   TryStrToInt(val, item.EdGate[colSubIdx]);
                break;
        case CALIB_COL_IS_CALIB:
                if(colSubIdx != -1)
                    item.Calibrated[colSubIdx] = (val==L"��") ? true : false;
                break;
        case CALIB_COL_MARK:
                TryStrToInt(val,item.Mark);
                break;
        case CALIB_COL_DATETIME:
                item.DataTime = StrToDateTime(val);
                break;
    }

    changesCount++;
    UpdateTableItem(idx+1);
}

UnicodeString GetCalibColName(eCailbCol val)
{
    switch(val)
    {
        case CALIB_COL_ID:           return L"ID";
        case CALIB_COL_NUMBER:       return L"�";
        case CALIB_COL_CHNAME:       return L"���";
        case CALIB_COL_KPNUM:        return L"��";
        case CALIB_COL_TVG:          return L"���";
        case CALIB_COL_PRISM_DELAY:  return L"2��";
        case CALIB_COL_SENS:         return L"��";
        case CALIB_COL_GAIN:         return L"���";
        case CALIB_COL_ST_GATE:      return L"������ ������";
        case CALIB_COL_ED_GATE:      return L"����� ������";
        case CALIB_COL_IS_CALIB:     return L"���������";
        case CALIB_COL_MARK:         return L"�����";
        case CALIB_COL_DATETIME:     return L"����� ���������";
    }
    return "Unknown";
}

int TCalibrationForm::CalibColSubItemCount(unsigned int idx, eCailbCol val)
{
    if(idx >= currCalibData.size())
        return 0;

    sCalibData& item = currCalibData[idx];

    switch(val)
    {
        case CALIB_COL_ID:           return 1;
        case CALIB_COL_NUMBER:       return 1;
        case CALIB_COL_CHNAME:       return 1;
        case CALIB_COL_KPNUM:        return 1;
        case CALIB_COL_TVG:          return 1;
        case CALIB_COL_PRISM_DELAY:  return 1;
        case CALIB_COL_SENS:         return item.GateCount;
        case CALIB_COL_GAIN:         return item.GateCount;
        case CALIB_COL_ST_GATE:      return item.GateCount;
        case CALIB_COL_ED_GATE:      return item.GateCount;
        case CALIB_COL_IS_CALIB:     return item.GateCount;
        case CALIB_COL_MARK:         return 1;
        case CALIB_COL_DATETIME:     return 1;
    }
    return 0;
}

//---------------------END Table item management--------------------------------------

bool bDisableThisShit = false;
void __fastcall TCalibrationForm::CalibTableSelectCell(TObject *Sender, int ACol,
          int ARow, bool &CanSelect)
{
    int nItem = ARow - 1;
    if((nItem < 0) || (nItem >= (int)currCalibData.size()))
        return;

    bDisableThisShit = true;
    currEditedItem = nItem;
    ValueList->Strings->Clear();
    for(int i = 0; i < CALIB_COL_END; i++)
    {
        if(!(TableEditableItemsMask & (1 << i)))
            continue;
        eCailbCol colType = (eCailbCol)i;
        int colSubItemCount = CalibColSubItemCount(currEditedItem,colType);

        DWORD pair;
        UnicodeString name;

        switch(colType)
        {
            case CALIB_COL_ED_GATE:
                break;
            case CALIB_COL_ST_GATE:
                for(int j = 0; j < colSubItemCount; j++)
                    for(int t = CALIB_COL_ST_GATE; t <= CALIB_COL_ED_GATE; t++)
                    {
                        eCailbCol colType2 = (eCailbCol)t;
                        pair = CALIB_MAKE_PAIR( colType2, j);
                        name = GetCalibColName(colType2) + L" " + IntToStr(j+1);
                        ValueList->Strings->AddObject(name + L"=" + GetTableItem(nItem,colType2,j),(TObject*)pair);
                    }
                break;
            default:
                for(int j = 0; j < colSubItemCount; j++)
                {
                    DWORD pair = CALIB_MAKE_PAIR( colType, j);
                    UnicodeString name = GetCalibColName(colType);
                    if(colSubItemCount > 1)
                        name += L" " + IntToStr(j+1);
                    ValueList->Strings->AddObject(name + L"=" + GetTableItem(nItem,colType,j),(TObject*)pair);
                }
        }



    }
    bDisableThisShit = false;
}
//---------------------------------------------------------------------------


void __fastcall TCalibrationForm::ValueListSetEditText(TObject *Sender, int ACol,
          int ARow, const UnicodeString Value)
{
    if((ARow-1) >= ValueList->Strings->Count)
        return;
    if(bDisableThisShit)
        return;
    DWORD pair = (DWORD)ValueList->Strings->Objects[ARow-1];
    eCailbCol colType = (eCailbCol)CALIB_GET_PAIR(pair,0);
    int subItem = CALIB_GET_PAIR(pair,1);


    SetTableItem(currEditedItem, colType, Value,subItem);
    //ValueList->Strings->Objects
}
//---------------------------------------------------------------------------

void TCalibrationForm::AdjustTableColumnsWidth()
{
    const int DEFBORDER_HOR = 12;
    const int DEFBORDER_VER = 6;

    TFont* oldFont = Canvas->Font;

	int* colWidthMax = new int[ CalibTable->ColCount ];
    int* rowHeightMax = new int[ CalibTable->RowCount ];
	memset(colWidthMax,0,sizeof(int)*CalibTable->ColCount);
	memset(rowHeightMax,0,sizeof(int)*CalibTable->RowCount);

	//Getting column title width
	Canvas->Font = CalibTable->Font;
    TStringList* pStrList = new TStringList();
	for( int i = 0; i < CalibTable->ColCount; i++)
	{
        for( int j = 0; j < CalibTable->RowCount; j++)
        {
            pStrList->Text = CalibTable->Cells[i][j];

            int maxWidth = 0;
            int maxHeight = 0;
            for(int k = 0; k < pStrList->Count; k++)
            {
                maxWidth = std::max( maxWidth,Canvas->TextWidth( pStrList->Strings[k] ));
                maxHeight += Canvas->TextHeight( pStrList->Strings[k] );
            }

            colWidthMax[i] = std::max( colWidthMax[i], maxWidth + DEFBORDER_HOR);
            rowHeightMax[j] = std::max( rowHeightMax[j], maxHeight + DEFBORDER_VER);
        }
	}

    for( int i = 0; i < CalibTable->ColCount; i++)
        CalibTable->ColWidths[i] = colWidthMax[i];

    for( int i = 1; i < CalibTable->RowCount; i++)
        CalibTable->RowHeights[i] = rowHeightMax[i];

    Canvas->Font = oldFont;
    delete pStrList;
    delete [] colWidthMax;
}


void __fastcall TCalibrationForm::m_CurSetupBtnClick(TObject *Sender)
{
    for(int i = 0; i < AutoconMain->Calibration->Count(); i++)
    {
        UnicodeString setupName = AutoconMain->Calibration->GetName(i);
        if(!setupName.Length())
            setupName = "��� �����";
        ArchiveSelectDial->AddComponent(i, setupName);
    }
    ArchiveSelectDial->SetDialCaption("�������� ������� ���������:");
	int selBtnId = ArchiveSelectDial->ShowSelect();

	if(selBtnId == -1)
        return;

    Store();
    AutoconMain->Calibration->SetCurrent(selBtnId);

    UpdateTable();
}
//---------------------------------------------------------------------------

bool TCalibrationForm::CopyCalib(int fromIdx, int toIdx)
{
    if(AutoconMain->Calibration->GetCurrent() == 0) //���������
        return false;

    int prevCurrent = AutoconMain->Calibration->GetCurrent();
    if(prevCurrent == fromIdx)
        Store();

    int NewItemsStart = AutoconMain->Calibration->Count();
    AutoconMain->Calibration->CreateFrom( fromIdx );
    TRACE("Copy %d from %d\n", AutoconMain->Calibration->Count(), fromIdx);
    int CopyCount = AutoconMain->Calibration->Count() - toIdx;
    int CopyStart = toIdx+1;
    AnsiString prevName = AutoconMain->Calibration->GetName(toIdx);

    for(int i = CopyStart; i < CopyCount; i++)
    {
        AutoconMain->Calibration->CreateFrom( i );
        TRACE("Adding %d from %d\n", AutoconMain->Calibration->Count(), i);
    }

    while(AutoconMain->Calibration->Count() > NewItemsStart)
    {
        AutoconMain->Calibration->Delete(toIdx);
        TRACE("Delete %d idx, Count = %d\n", toIdx,AutoconMain->Calibration->Count());
    }

    AutoconMain->Calibration->SetCurrent(toIdx);
    AutoconMain->Calibration->ResetReadOnly();
    AutoconMain->Calibration->SetName(prevName.c_str());

    AutoconMain->Calibration->SetCurrent(prevCurrent);

    if(prevCurrent == toIdx)
        changesCount = 0;
    return true;
}

void __fastcall TCalibrationForm::m_CopyBtnClick(TObject *Sender)
{
    if(AutoconMain->Calibration->GetCurrent() == 0) //���������
        return;

    ArchiveSelectDial->AddComponent(0, "����������� �  ...");
    ArchiveSelectDial->AddComponent(1, "����������� �� ...");

    ArchiveSelectDial->SetDialCaption("�������� ��������:");
	int selBtnId = ArchiveSelectDial->ShowSelect();
    if(selBtnId == -1)
        return;
    bool bCopyTo = (selBtnId == 0);

    int startCalib = bCopyTo ? 1 : 0;

    for(int i = startCalib; i < AutoconMain->Calibration->Count(); i++)
    {
        if(i == AutoconMain->Calibration->GetCurrent())
            continue;

        UnicodeString setupName = AutoconMain->Calibration->GetName(i);
        if(!setupName.Length())
            setupName = "��� �����";
        ArchiveSelectDial->AddComponent(i, setupName);
    }

    ArchiveSelectDial->SetDialCaption("�������� ���������:");
    selBtnId = ArchiveSelectDial->ShowSelect();
    if(selBtnId == -1)
        return;

    if(Application->MessageBoxW(bCopyTo ? L"�� �������? ��� ������ ��� ������ �� ��������� ���������!" :
                                          L"�� �������? ��� ������ ��� ������ �� ������� ���������!",
                                L"��������������", MB_OKCANCEL | MB_ICONWARNING) != IDOK)
    {
        return;
    }

    int CopyFromIdx = bCopyTo ? AutoconMain->Calibration->GetCurrent() : selBtnId;
    int CopyToIdx = bCopyTo ? selBtnId : AutoconMain->Calibration->GetCurrent();

    CopyCalib(CopyFromIdx, CopyToIdx);

    UpdateTable();
}
//---------------------------------------------------------------------------


void __fastcall TCalibrationForm::m_ResetToDefBtnClick(TObject *Sender)
{
    if(AutoconMain->Calibration->GetCurrent() == 0) //���������
        return;

    if(Application->MessageBoxW(L"�� �������? ��� ������ ��� ������ �� ������� ���������!",
                                L"��������������", MB_YESNO | MB_ICONWARNING) != IDYES)
    {
        return;
    }

    CopyCalib(0, AutoconMain->Calibration->GetCurrent());
    UpdateTable();
}
//---------------------------------------------------------------------------

void TCalibrationForm::Store()
{
    if(AutoconMain->Calibration->GetCurrent() == 0) //���������
        return;
    if(changesCount == 0)
        return;

    if(Application->MessageBoxW(L"��������� ��������� � ������� ���������?",
                                L"��������������", MB_YESNO | MB_ICONWARNING) != IDYES)
    {
        UpdateTable();
        changesCount = 0;
        return;
    }


    UpdateCalibrationData(false);
    UpdateTable();
}

void __fastcall TCalibrationForm::FormHide(TObject *Sender)
{
    Store();
}
//---------------------------------------------------------------------------

void TCalibrationForm::HighlightChannel(CID chId, TColor col)
{
    if((int)channelHgData.size() <= chId)
        channelHgData.resize(chId + 1, clWhite);
    channelHgData[chId] = col;
}

void __fastcall TCalibrationForm::CalibTableDrawCell(TObject *Sender, int ACol, int ARow,
          TRect &Rect, TGridDrawState State)
{
    if((ARow < CalibTable->FixedRows) || (ACol < CalibTable->FixedCols))
        return;

    TColor fillColor = clWhite;
    int chId = currCalibData[ARow - 1].ID;
    if((int)channelHgData.size() > chId)
    {
        fillColor = channelHgData[chId];
    }

    if(State.Contains(gdSelected))
        fillColor = clHighlight;

    CalibTable->Canvas->Brush->Color = fillColor;
    CalibTable->Canvas->FillRect(Rect);

    TTextFormat tf;
    tf.Clear();
    tf << tfCenter << tfVerticalCenter;
    UnicodeString str = CalibTable->Cells[ACol][ARow];
    CalibTable->Canvas->Font->Color = clBlack;
    CalibTable->Canvas->TextRect(Rect, str, tf);
}
//---------------------------------------------------------------------------

void __fastcall TCalibrationForm::PageDownBtnClick(TObject *Sender)
{
    if(CalibTable->TopRow < ( CalibTable->RowCount - CalibTable->VisibleRowCount))
    {
        CalibTable->TopRow = std::min((int)( CalibTable->RowCount - CalibTable->VisibleRowCount),
                                        (int)(CalibTable->TopRow + CalibTable->VisibleRowCount));
    }
}
//---------------------------------------------------------------------------

void __fastcall TCalibrationForm::PageUpBtnClick(TObject *Sender)
{
    if(CalibTable->TopRow > 1)
    {
        CalibTable->TopRow = std::max(1, (int)(CalibTable->TopRow - CalibTable->VisibleRowCount));
    }

}
//---------------------------------------------------------------------------

