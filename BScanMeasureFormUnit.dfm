object BScanMeasureForm: TBScanMeasureForm
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'BScanMeasureForm'
  ClientHeight = 595
  ClientWidth = 277
  Color = clBlue
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 5
    Top = 4
    Width = 258
    Height = 579
    BevelOuter = bvNone
    BorderWidth = 3
    BorderStyle = bsSingle
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PageControl2: TPageControl
      Left = 3
      Top = 3
      Width = 248
      Height = 310
      ActivePage = TabSheet2
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 229
      object TabSheet1: TTabSheet
        Caption = #1058#1080#1087
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabVisible = False
        ExplicitWidth = 255
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 240
          Height = 276
          Align = alClient
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitWidth = 255
          object Label11: TLabel
            Left = 0
            Top = 43
            Width = 240
            Height = 25
            Align = alTop
            Caption = #1048#1079#1084#1077#1088#1077#1085#1080#1077':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            ExplicitWidth = 122
          end
          object Label13: TLabel
            Left = 0
            Top = 100
            Width = 240
            Height = 25
            Align = alTop
            Caption = #1055#1086' '#1089#1080#1075#1085#1072#1083#1072#1084':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            ExplicitWidth = 139
          end
          object Label14: TLabel
            Left = 0
            Top = 157
            Width = 240
            Height = 25
            Align = alTop
            Caption = #1055#1086' '#1082#1072#1085#1072#1083#1072#1084':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            ExplicitWidth = 130
          end
          object Label1: TLabel
            Left = 0
            Top = 214
            Width = 240
            Height = 25
            Align = alTop
            Caption = #1055#1086' '#1088#1077#1075#1080#1086#1085#1072#1084':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            ExplicitWidth = 140
          end
          object perChannelMeasTags: TComboBox
            Tag = 3
            Left = 0
            Top = 182
            Width = 240
            Height = 32
            Align = alTop
            Style = csDropDownList
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = 0
            ParentFont = False
            TabOrder = 0
            Text = #1084#1080#1085#1080#1084#1091#1084
            OnSelect = OnSelectMeasureTag
            Items.Strings = (
              #1084#1080#1085#1080#1084#1091#1084
              #1084#1072#1082#1089#1080#1084#1091#1084
              #1089#1088#1077#1076#1085#1077#1077
              #1089#1091#1084#1084#1091)
            ExplicitWidth = 255
          end
          object perSignalMeasTags: TComboBox
            Tag = 2
            Left = 0
            Top = 125
            Width = 240
            Height = 32
            Align = alTop
            Style = csDropDownList
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnSelect = OnSelectMeasureTag
            Items.Strings = (
              #1084#1080#1085#1080#1084#1091#1084
              #1084#1072#1082#1089#1080#1084#1091#1084
              #1089#1088#1077#1076#1085#1077#1077
              #1089#1091#1084#1084#1091)
            ExplicitWidth = 255
          end
          object signalMeasTags: TComboBox
            Tag = 1
            Left = 0
            Top = 68
            Width = 240
            Height = 32
            Align = alTop
            Style = csDropDownList
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnSelect = OnSelectMeasureTag
            Items.Strings = (
              #1072#1084#1087#1083#1080#1090#1091#1076#1072
              #1079#1072#1076#1077#1088#1078#1082#1072
              #1086#1073#1072)
            ExplicitWidth = 255
          end
          object MakeMeasureBtn: TBitBtn
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 234
            Height = 37
            Align = alTop
            Caption = #1048#1079#1084#1077#1088#1080#1090#1100
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
            OnClick = MakeMeasureBtnClick
            ExplicitWidth = 249
          end
          object perRegionsMeasTags: TComboBox
            Tag = 4
            Left = 0
            Top = 239
            Width = 240
            Height = 32
            Align = alTop
            Style = csDropDownList
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = 0
            ParentFont = False
            TabOrder = 4
            Text = #1084#1080#1085#1080#1084#1091#1084
            OnSelect = OnSelectMeasureTag
            Items.Strings = (
              #1084#1080#1085#1080#1084#1091#1084
              #1084#1072#1082#1089#1080#1084#1091#1084
              #1089#1088#1077#1076#1085#1077#1077
              #1089#1091#1084#1084#1091)
            ExplicitWidth = 255
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = #1044#1086#1087#1086#1083#1085'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ImageIndex = 2
        ParentFont = False
        ExplicitWidth = 255
        object addPolyBtn: TSpeedButton
          AlignWithMargins = True
          Left = 3
          Top = 98
          Width = 234
          Height = 46
          Margins.Top = 16
          Align = alTop
          Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1088#1077#1075#1080#1086#1085
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          Visible = False
          OnClick = addPolyBtnClick
          ExplicitTop = 133
          ExplicitWidth = 249
        end
        object GridPanel1: TGridPanel
          AlignWithMargins = True
          Left = 0
          Top = 157
          Width = 240
          Height = 41
          Margins.Left = 0
          Margins.Top = 10
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alTop
          BevelOuter = bvNone
          ColumnCollection = <
            item
              Value = 50.000000000000000000
            end
            item
              Value = 50.000000000000000000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = compareCombo
              Row = 0
            end
            item
              Column = 1
              Control = compareBtn
              Row = 0
            end>
          RowCollection = <
            item
              Value = 100.000000000000000000
            end
            item
              SizeStyle = ssAuto
            end>
          TabOrder = 0
          ExplicitWidth = 255
          object compareCombo: TComboBox
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 114
            Height = 32
            Align = alClient
            Style = csDropDownList
            Anchors = []
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            Visible = False
            ExplicitWidth = 121
          end
          object compareBtn: TSpeedButton
            Left = 120
            Top = 0
            Width = 120
            Height = 41
            Align = alClient
            Caption = #1057#1088#1072#1074#1085#1080#1090#1100
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            Visible = False
            OnClick = compareBtnClick
            ExplicitLeft = 130
            ExplicitTop = -3
            ExplicitWidth = 128
          end
        end
        object GridPanel3: TGridPanel
          Left = 0
          Top = 41
          Width = 240
          Height = 41
          Align = alTop
          BevelOuter = bvNone
          ColumnCollection = <
            item
              Value = 50.000000000000000000
            end
            item
              Value = 50.000000000000000000
            end>
          ControlCollection = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          RowCollection = <
            item
              Value = 100.000000000000000000
            end
            item
              SizeStyle = ssAuto
            end>
          TabOrder = 1
          ExplicitWidth = 255
        end
        object GridPanel5: TGridPanel
          Left = 0
          Top = 0
          Width = 240
          Height = 41
          Align = alTop
          BevelOuter = bvNone
          ColumnCollection = <
            item
              Value = 50.000000000000000000
            end
            item
              Value = 50.000000000000000000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = saveNameEdit
              Row = 0
            end
            item
              Column = 1
              Control = saveBtn
              Row = 0
            end>
          RowCollection = <
            item
              Value = 100.000000000000000000
            end
            item
              SizeStyle = ssAuto
            end>
          TabOrder = 2
          ExplicitWidth = 255
          object saveNameEdit: TEdit
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 114
            Height = 35
            Align = alClient
            Anchors = []
            TabOrder = 0
            ExplicitWidth = 121
            ExplicitHeight = 27
          end
          object saveBtn: TSpeedButton
            Left = 120
            Top = 0
            Width = 120
            Height = 41
            Align = alClient
            Anchors = []
            Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            OnClick = saveBtnClick
            ExplicitLeft = 3
            ExplicitTop = -5
            ExplicitWidth = 97
            ExplicitHeight = 46
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = #1055#1086#1083#1086#1078#1077#1085#1080#1077
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 255
        ExplicitHeight = 0
      end
    end
  end
  object Panel2: TPanel
    Left = 5
    Top = 5
    Width = 268
    Height = 582
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object CloseBtn: TSpeedButton
      Left = 151
      Top = 525
      Width = 110
      Height = 49
      Caption = #1047#1072#1074#1077#1088#1096#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -18
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = CloseBtnClick
    end
    object Panel4: TPanel
      Left = 1
      Top = 87
      Width = 266
      Height = 41
      Align = alTop
      Caption = #1055#1086#1083#1086#1078#1077#1085#1080#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      ExplicitLeft = 40
      ExplicitTop = 248
      ExplicitWidth = 185
    end
    object Panel5: TPanel
      Left = 1
      Top = 128
      Width = 266
      Height = 263
      Align = alTop
      TabOrder = 1
      ExplicitTop = 42
      object GridPanel4: TGridPanel
        Left = 1
        Top = 1
        Width = 264
        Height = 261
        Align = alClient
        ColumnCollection = <
          item
            Value = 33.333340304173380000
          end
          item
            Value = 33.333340304173380000
          end
          item
            Value = 33.333319391653240000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = BitBtn7
            Row = 0
          end
          item
            Column = 1
            Control = BitBtn8
            Row = 0
          end
          item
            Column = 2
            Control = BitBtn9
            Row = 0
          end
          item
            Column = 0
            Control = BitBtn10
            Row = 1
          end
          item
            Column = 1
            Control = BitBtn11
            Row = 1
          end
          item
            Column = 2
            Control = BitBtn12
            Row = 1
          end
          item
            Column = 0
            Control = MultX1Btn
            Row = 2
          end
          item
            Column = 1
            Control = MultX10Btn
            Row = 2
          end
          item
            Column = 2
            Control = SpeedButton4
            Row = 2
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        RowCollection = <
          item
            Value = 32.790186454121660000
          end
          item
            Value = 33.688453457748230000
          end
          item
            Value = 33.521360088130110000
          end>
        TabOrder = 0
        ExplicitLeft = 2
        ExplicitTop = 2
        object BitBtn7: TBitBtn
          Tag = 4
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 81
          Height = 78
          Align = alClient
          Caption = '<'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -22
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = OnChangeMeasPos
          ExplicitWidth = 82
          ExplicitHeight = 155
        end
        object BitBtn8: TBitBtn
          Tag = 5
          AlignWithMargins = True
          Left = 91
          Top = 4
          Width = 81
          Height = 78
          Align = alClient
          Caption = #1042#1050
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -22
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnClick = OnChangeMeasPos
          ExplicitLeft = 92
          ExplicitWidth = 82
          ExplicitHeight = 155
        end
        object BitBtn9: TBitBtn
          Tag = 6
          AlignWithMargins = True
          Left = 178
          Top = 4
          Width = 82
          Height = 78
          Align = alClient
          Caption = '>'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -22
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnClick = OnChangeMeasPos
          ExplicitLeft = 180
          ExplicitHeight = 155
        end
        object BitBtn10: TBitBtn
          Tag = 7
          AlignWithMargins = True
          Left = 4
          Top = 88
          Width = 81
          Height = 81
          Align = alClient
          Caption = '<'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -22
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnClick = OnChangeMeasPos
          ExplicitTop = 165
          ExplicitWidth = 82
          ExplicitHeight = 160
        end
        object BitBtn11: TBitBtn
          Tag = 8
          AlignWithMargins = True
          Left = 91
          Top = 88
          Width = 81
          Height = 81
          Align = alClient
          Caption = #1053#1050
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -22
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          OnClick = OnChangeMeasPos
          ExplicitLeft = 92
          ExplicitTop = 165
          ExplicitWidth = 82
          ExplicitHeight = 160
        end
        object BitBtn12: TBitBtn
          Tag = 9
          AlignWithMargins = True
          Left = 178
          Top = 88
          Width = 82
          Height = 81
          Align = alClient
          Caption = '>'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -22
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
          OnClick = OnChangeMeasPos
          ExplicitLeft = 180
          ExplicitTop = 165
          ExplicitHeight = 160
        end
        object MultX1Btn: TSpeedButton
          Tag = 1
          AlignWithMargins = True
          Left = 4
          Top = 175
          Width = 81
          Height = 82
          Align = alClient
          GroupIndex = 123
          Down = True
          Caption = 'x1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = OnChangePosMult
          ExplicitLeft = 88
          ExplicitTop = 296
          ExplicitWidth = 23
          ExplicitHeight = 22
        end
        object MultX10Btn: TSpeedButton
          Tag = 10
          AlignWithMargins = True
          Left = 91
          Top = 175
          Width = 81
          Height = 82
          Align = alClient
          GroupIndex = 123
          Caption = 'x10'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = OnChangePosMult
          ExplicitLeft = 88
          ExplicitTop = 296
          ExplicitWidth = 23
          ExplicitHeight = 22
        end
        object SpeedButton4: TSpeedButton
          Tag = 100
          AlignWithMargins = True
          Left = 178
          Top = 175
          Width = 82
          Height = 82
          Align = alClient
          GroupIndex = 123
          Caption = 'x100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = OnChangePosMult
          ExplicitLeft = 88
          ExplicitTop = 296
          ExplicitWidth = 23
          ExplicitHeight = 22
        end
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 42
      Width = 266
      Height = 45
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object GridPanel2: TGridPanel
        Left = 1
        Top = 1
        Width = 264
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        ColumnCollection = <
          item
            Value = 50.000000000000000000
          end
          item
            Value = 50.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = loadCombo
            Row = 0
          end
          item
            Column = 1
            Control = loadBtn
            Row = 0
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        RowCollection = <
          item
            Value = 100.000000000000000000
          end
          item
            SizeStyle = ssAuto
          end>
        TabOrder = 0
        ExplicitLeft = 2
        ExplicitTop = 9
        object loadCombo: TComboBox
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 126
          Height = 32
          Align = alClient
          Style = csDropDownList
          Anchors = []
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          ExplicitTop = 4
        end
        object loadBtn: TSpeedButton
          Left = 132
          Top = 0
          Width = 132
          Height = 41
          Margins.Top = 6
          Align = alClient
          Anchors = []
          Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = loadBtnClick
          ExplicitLeft = 2
          ExplicitTop = 9
          ExplicitWidth = 23
          ExplicitHeight = 320
        end
      end
    end
    object Panel7: TPanel
      Left = 1
      Top = 1
      Width = 266
      Height = 41
      Align = alTop
      Caption = #1056#1072#1079#1084#1077#1090#1082#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      ExplicitLeft = 3
      ExplicitTop = -28
    end
    object Panel8: TPanel
      Left = 1
      Top = 391
      Width = 266
      Height = 41
      Align = alTop
      Caption = #1040#1085#1072#1083#1080#1079
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      ExplicitLeft = 3
      ExplicitTop = 433
    end
    object GridPanel6: TGridPanel
      Left = 1
      Top = 473
      Width = 266
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      ColumnCollection = <
        item
          Value = 50.000000000000000000
        end
        item
          Value = 50.000000000000000000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = Panel9
          Row = 0
        end
        item
          Column = 1
          Control = SpeedButton1
          Row = 0
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      RowCollection = <
        item
          Value = 100.000000000000000000
        end
        item
          SizeStyle = ssAuto
        end>
      TabOrder = 5
      ExplicitLeft = 3
      ExplicitTop = 438
      DesignSize = (
        266
        41)
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 133
        Height = 41
        Anchors = []
        Caption = '-'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        ExplicitLeft = 40
      end
      object SpeedButton1: TSpeedButton
        Left = 133
        Top = 0
        Width = 133
        Height = 41
        Margins.Top = 6
        Align = alClient
        Anchors = []
        Caption = #1056#1072#1089#1095#1077#1090
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = SpeedButton1Click
        ExplicitLeft = 2
        ExplicitTop = 9
        ExplicitWidth = 23
        ExplicitHeight = 320
      end
    end
    object Panel10: TPanel
      Left = 1
      Top = 432
      Width = 266
      Height = 41
      Align = alTop
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      ExplicitLeft = 2
      ExplicitTop = 425
    end
  end
end
