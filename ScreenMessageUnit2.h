﻿/**
 * @file ScreenMessageUnit2.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef ScreenMessageUnit2H
#define ScreenMessageUnit2H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ComCtrls.hpp>

#include "AController.h"

//! Описывает выводимое на экран сообщение
struct tMessageItem2
{
	tMessageItem2()
	{
		pParent = NULL;
	};

	UnicodeString Text;  //!< Текст сообщения
	DWORD Time;          //!< Время добавления сообщения (для расчета времени показа)
	TMessagesType Type;  //!< Тип сообщения
	TColor TextColor;    //!< Цвет текста
	TColor BGColor;      //!< Цвет фона
	TForm* pParent;      //!< Указатель на форму из которой было отправлено сообщение
};

//---------------------------------------------------------------------------
//! Предназначен для отображения экранного сообщения. Измененный вариант ScreenMessageForm (<- ныне мертв).
class TScreenMessageForm2 : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TTimer *Timer1;
	TPanel *Panel1;
	TPanel *Panel2;
	TStaticText *StaticText1;
	TPaintBox *PaintBox1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall OnSelectionChange(TObject *Sender);
	void __fastcall OnPanelClick(TObject *Sender);
private:	// User declarations

	std::vector <tMessageItem2> MessageList;    //!< Список сообщений для отображения
	bool NeedUpdate;                            //!< Признак необходимости обновления. Возникает при получении нового сообщения или завершении интервала показа старого.
	int LastCount;                              //!< Какая то хрень. Вероятно, для обределения наличия новых сообщений.
public:		// User declarations
	__fastcall TScreenMessageForm2(TComponent* Owner);

	void __fastcall Add(TMessagesType Type, UnicodeString Text_, DWord Time, TForm* pParent = NULL); //!< Добавление нового сообщения
	int __fastcall AddColoredStr(UnicodeString str, TColor TextColor, TColor BGColor);  //!< Какая то хрень.
};
//---------------------------------------------------------------------------
extern PACKAGE TScreenMessageForm2 *ScreenMessageForm2;
//---------------------------------------------------------------------------
#endif
