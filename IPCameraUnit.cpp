//---------------------------------------------------------------------------

#pragma hdrstop

#include "IPCameraUnit.h"
//---------------------------------------------------------------------------
//#pragma package(smart_init)


TMemoryStream* MemoryStream;

bool GetPhoto(UnicodeString IP, int *PJpegAddr, int *PJpegSize)
{
  TIdHTTP * idHTTP;

  if ((!PJpegAddr) || (!PJpegSize)) return false;

  MemoryStream = new TMemoryStream();
  idHTTP = new TIdHTTP;
  idHTTP->ConnectTimeout = 4000;
  idHTTP->Request->BasicAuthentication = True;
  idHTTP->Request->Username = "admin";
  idHTTP->Request->Password = "12345";

  __try
  {
    __try{
        idHTTP->Get(Format("http://%s/Streaming/channels/1/picture", ARRAYOFCONST((IP))), MemoryStream);
    }
    __finally{
        delete idHTTP;
    }
  }
  __except (EXCEPTION_EXECUTE_HANDLER)
  {
    Application->MessageBox(L"���������� �� ������ �� ��������", L"������ �����", MB_OK | MB_ICONWARNING);
    return false;
  }


  *PJpegAddr = (int)MemoryStream->Memory;
  *PJpegSize = MemoryStream->Size;
  return true;
}

void FreePhoto()
{
  delete MemoryStream;
}

