//---------------------------------------------------------------------
#include <vcl.h>
#include <set>
#pragma hdrstop

#include "SelectChannelUnit.h"
//---------------------------------------------------------------------
#pragma resource "*.dfm"
TSelectChannelDial *SelectChannelDial;
//--------------------------------------------------------------------- 
__fastcall TSelectChannelDial::TSelectChannelDial(TComponent* AOwner)
	: TForm(AOwner)
{
	bSingleSelect = false;

	selectMode = SELM_BY_CHANNEL;
	sortMode[0] = SORTM_BY_KP;
	sortMode[1] = SORTM_BY_KP;
	groupMode[0] = GRT_KP_GROUP;
	groupMode[1] = GRT_KP_GROUP;

    filterType = SELM_NONE;
}
//---------------------------------------------------------------------

//---------------------------------------------------------------------------

void TSelectChannelDial::UpdateSizes()
{
	if(bSingleSelect)
	{
		m_ChannelsPanelDst->Visible = false;
		m_SelectBtnsPanel->Visible = false;

	  /*	m_ChannelsPanelSrc->BoundsRect =
					TRect(0,0,m_SelectDataPanel->Width,m_SelectDataPanel->Height);*/
	}
	else
	{
		m_ChannelsPanelDst->Visible = true;
		m_SelectBtnsPanel->Visible = true;

		/*m_ChannelsPanelSrc->BoundsRect =
					TRect(0,0,
					m_SelectDataPanel->Width/2-m_SelectBtnsPanel->Width/2,m_SelectDataPanel->Height);
		m_ChannelsPanelDst->BoundsRect =
					TRect(m_SelectDataPanel->Width/2+m_SelectBtnsPanel->Width/2,0,
					m_SelectDataPanel->Width,m_SelectDataPanel->Height);
		m_SelectBtnsPanel->BoundsRect =
					TRect(m_SelectDataPanel->Width/2-m_SelectBtnsPanel->Width/2,m_SelectDataPanel->Height/2-m_SelectBtnsPanel->Height/2,
					m_SelectDataPanel->Width/2+m_SelectBtnsPanel->Width/2,m_SelectDataPanel->Height/2+m_SelectBtnsPanel->Height/2);*/
	}
}

void __fastcall TSelectChannelDial::m_SelectDataPanelResize(TObject *Sender)
{
	UpdateSizes();
}
//---------------------------------------------------------------------------

void __fastcall TSelectChannelDial::FormCreate(TObject *Sender)
{
	UpdateSizes();
}
//---------------------------------------------------------------------------

void __fastcall TSelectChannelDial::FormShow(TObject *Sender)
{
	UpdateSizes();
	//UpdateChannelPanels(true);
	ResetChannels();
	ResetGroups();
	UpdateSelectionList(0);
    UpdateSelectionList(1);
}

void TSelectChannelDial::Filter(eSelectChannelMode type, int value)
{
    filterType = type;
    filterValue = value;
}

void TSelectChannelDial::ResetChannels()
{
	sChannelDescription chanDesc;
	sScanChannelDescription scanChanDesc;
	sHandChannelDescription handChanDesc;

	sSelChannelDesc channel;
	channels.clear();

    for(int i = 0; i < GRT_LAST; i++)
    {
        groupToChannel[i].clear();
        channelToGroup[i].clear();
    }


	//Adding channels
	for (unsigned int i = 0; i < AutoconMain->Table->Count(); i++)
	{
        CID chId;
        if(!AutoconMain->Table->CIDByIndex(&chId,i))
            continue;
		if(!AutoconMain->Table->ItemByCID(chId, &chanDesc))
			continue;


        bool bFound = false;
        switch(filterType)
        {
            case SELM_NONE:
                break;
            case SELM_BY_CHANNEL:
                if(chanDesc.id != filterValue)
                    continue;
                break;
            case SELM_BY_SCAN_GROUP:
                for(unsigned int j=0;j<AutoconMain->Config->GetAllScanChannelsCount();j++)
                {
                    if(!AutoconMain->Config->getSChannelbyIdx(j, &scanChanDesc))
                        continue;
                    if(scanChanDesc.Id != chanDesc.id)
                        continue;
                    if(scanChanDesc.StrokeGroupIdx == filterValue)
                    {
                        bFound = true; break;
                    }
                }
                if(!bFound)
                    continue;
                break;
            case SELM_BY_KP:
                for(unsigned int j=0;j<AutoconMain->Config->GetAllScanChannelsCount();j++)
                {
                    if(!AutoconMain->Config->getSChannelbyIdx(j, &scanChanDesc))
                        continue;
                    if(scanChanDesc.Id != chanDesc.id)
                        continue;

                    if(scanChanDesc.BScanGroup== filterValue)
                    {
                        bFound = true; break;
                    }
                }
                if(!bFound)
                    continue;
                break;
        }

        channel.itemId = channels.size();
		channel.channelId = chanDesc.id;
        channel.name = chanDesc.Name;
		channel.title = chanDesc.Title;
		channel.colomnIndex = 0;
		channels.push_back(channel);
	}

	//resetting panels
    m_ChannelsListSrc->Clear();
	m_ChannelsListDst->Clear();



	//Apply preselected channels
	for(unsigned int i=0;i<channels.size();i++)
		for(unsigned int j=0;j<inOutChannelsBuffer.size();j++)
		{
			if( channels[i].channelId == (int)inOutChannelsBuffer[j])
			{
				channels[i].colomnIndex = 1;
			}
		}


	if(selectMode == SELM_BY_CHANNEL)
	{
		SetSelectionListGroup(groupMode[0],0);
		SetSelectionListGroup(groupMode[1],1);
		m_GroupTypeComboSrc->ItemIndex = (int)groupMode[0];
		m_GroupTypeComboSrc->Enabled = true;
		m_GroupTypeComboDst->ItemIndex = (int)groupMode[1];
		m_GroupTypeComboDst->Enabled = true;
	}
	else
	{
		if(selectMode == SELM_BY_SCAN_GROUP)
		{
			SetSelectionListGroup(GRT_SCAN_GROUP,0);
			SetSelectionListGroup(GRT_SCAN_GROUP,1);
			m_GroupTypeComboSrc->ItemIndex = 1;
			m_GroupTypeComboDst->ItemIndex = 1;
		}
		else if(selectMode == SELM_BY_KP)
		{
			SetSelectionListGroup(GRT_KP_GROUP,0);
			SetSelectionListGroup(GRT_KP_GROUP,1);
			m_GroupTypeComboSrc->ItemIndex = 2;
			m_GroupTypeComboDst->ItemIndex = 2;
		}

		m_GroupTypeComboSrc->Enabled = false;
		m_GroupTypeComboDst->Enabled = false;
	}
}

void TSelectChannelDial::ResetGroups()
{
	sScanChannelDescription scanChanDesc;

	for (unsigned int i = 0; i < channels.size(); i++)
	{
		linkGroupAndChannel(-1,i,GRT_NONE);

		//if(!AutoconMain->Config->getSChannelbyID(channels[i].channelId, &scanChanDesc))//bad code
		if (AutoconMain->Config->getFirstSChannelbyID(channels[i].channelId, &scanChanDesc) == -1) //bad code
		{
			linkGroupAndChannel(-1,i,GRT_SCAN_GROUP);
			linkGroupAndChannel(-1,i,GRT_KP_GROUP);
		}
		else
		{
			for(unsigned int j=0;j<AutoconMain->Config->GetAllScanChannelsCount();j++)
			{
				if(!AutoconMain->Config->getSChannelbyIdx(j, &scanChanDesc))
					continue;

				if( scanChanDesc.Id == channels[i].channelId)
				{
					linkGroupAndChannel(scanChanDesc.StrokeGroupIdx,i,GRT_SCAN_GROUP);
					linkGroupAndChannel(scanChanDesc.BScanGroup,i,GRT_KP_GROUP);
                }
			}
		}
	}
}

void TSelectChannelDial::linkGroupAndChannel(int grNumber, UINT chNumber,eSortChannelGroupType grType)
{
	groupToChannel[(UINT)grType][grNumber].insert(chNumber);
	channelToGroup[(UINT)grType][chNumber].insert(grNumber);
}


void TSelectChannelDial::UpdateSelectionList(int listIndex)
{
	SetSelectionListGroup(groupMode[listIndex], listIndex);
	SetSelectionListSort(sortMode[listIndex], listIndex);
}

void TSelectChannelDial::SetSelectionListGroup(eSortChannelGroupType newGroupMode, int listIndex)
{
	std::vector<sSelListItemElemDesc> selected;
	//Store selected items
	for(int i=0;i<getSelectionList(listIndex)->Items->Count;i++)
	{
		sSelListItemElemDesc* pItem = (sSelListItemElemDesc*)getSelectionList(listIndex)->Items->Objects[i];

		if((getSelectionList(listIndex)->Selected[i]) && (!pItem->bIsGroup))
		{
			selected.push_back(*pItem);
		}
	}

	int listItemCnt = 0;
	std::map<int,std::set<UINT> >::iterator mapIt = groupToChannel[newGroupMode].begin();
	while(mapIt!=groupToChannel[newGroupMode].end())
	{
		bool bInsertGroup = true;
		std::set<UINT>::iterator setIt = mapIt->second.begin();
		while(setIt != mapIt->second.end())
		{
			int channelIndex = *setIt;
			sSelChannelDesc channel = channels[channelIndex];
			if(channel.colomnIndex!=listIndex)
			{
				setIt++;
				continue;
			}

			if(bInsertGroup)
			{
				//Adding group
				if(listItemCnt < getSelectionList(listIndex)->Items->Count)
				{
					getSelectionList(listIndex)->Items->Strings[listItemCnt] = GetGroupName(newGroupMode,mapIt->first);
					*((sSelListItemElemDesc*)getSelectionList(listIndex)->Items->Objects[listItemCnt]) = sSelListItemElemDesc(true,-1,mapIt->first);
				}
				else
					getSelectionList(listIndex)->AddItem(GetGroupName(newGroupMode,mapIt->first),
								(TObject*)new sSelListItemElemDesc(true,-1,mapIt->first));
				listItemCnt++;

				bInsertGroup=false;
            }

			//Adding channel
			if(listItemCnt < getSelectionList(listIndex)->Items->Count)
			{
				getSelectionList(listIndex)->Items->Strings[listItemCnt] = StringFormatU(L" - ����� 0x%X (%d): %s",channel.channelId,channel.channelId, channel.title);
				*((sSelListItemElemDesc*)getSelectionList(listIndex)->Items->Objects[listItemCnt]) = sSelListItemElemDesc(false,channelIndex,mapIt->first);
			}
			else
				getSelectionList(listIndex)->AddItem(StringFormatU(L" - ����� 0x%X (%d): %s",channel.channelId,channel.channelId, channel.title),
							(TObject*)new sSelListItemElemDesc(false,channelIndex,mapIt->first));
			listItemCnt++;


			setIt++;
		}
		mapIt++;
	}

	while(listItemCnt<getSelectionList(listIndex)->Items->Count)
	{
		delete (sSelListItemElemDesc*)getSelectionList(listIndex)->Items->Objects[getSelectionList(listIndex)->Items->Count-1];
		getSelectionList(listIndex)->Items->Delete(getSelectionList(listIndex)->Items->Count-1);
	}

	//Restore selected items
	for(int i=0;i<getSelectionList(listIndex)->Items->Count;i++)
	{
		sSelListItemElemDesc* pItem = (sSelListItemElemDesc*)getSelectionList(listIndex)->Items->Objects[i];
		if(!pItem->bIsGroup)
		{
			for(unsigned int j=0;j<selected.size();j++)
			{
				if((pItem->channelId == selected[j].channelId) && (pItem->groupId == selected[j].groupId))
					getSelectionList(listIndex)->Selected[i]=true;
				//else
				//	getSelectionList(listIndex)->Selected[i]=false;
			}
		}
	}

	groupMode[listIndex] = newGroupMode;
}

void TSelectChannelDial::SetSelectionListSort(eSortChannelMode newSortMode, int listIndex)
{
	sortMode[listIndex] = newSortMode;
}

TCustomListBox* TSelectChannelDial::getSelectionList(int index)
{
	switch(index)
	{
		case 0:	return m_ChannelsListSrc;
		case 1: return m_ChannelsListDst;
	}
	return NULL;
}

UnicodeString TSelectChannelDial::GetGroupName(eSortChannelGroupType groupMode, int groupId)
{
	if(groupId==-1)
		return L"��� ������:";

	switch(groupMode)
	{
		case GRT_NONE: 			return StringFormatU(L"��� ������: %d",groupId);
		case GRT_SCAN_GROUP: 	return StringFormatU(L"������ ������������: %d",groupId);
		case GRT_KP_GROUP: 		return StringFormatU(L"�� - %d:",groupId);
	}

	return L"Unknown:";
}

//---------------------------------------------------------------------------

void __fastcall TSelectChannelDial::m_GroupTypeComboSrcChange(TObject *Sender)
{
	eSortChannelGroupType newGroupMode;
	switch(m_GroupTypeComboSrc->ItemIndex)
	{
		case 0: newGroupMode = GRT_NONE; break;
		case 1: newGroupMode = GRT_SCAN_GROUP; break;
		case 2: newGroupMode = GRT_KP_GROUP; break;
		default:   //error
			return;
	}
	SetSelectionListGroup(newGroupMode,0);
}
//---------------------------------------------------------------------------

void __fastcall TSelectChannelDial::m_GroupTypeComboDstChange(TObject *Sender)
{
	eSortChannelGroupType newGroupMode;
	switch(m_GroupTypeComboDst->ItemIndex)
	{
		case 0: newGroupMode = GRT_NONE; break;
		case 1: newGroupMode = GRT_SCAN_GROUP; break;
		case 2: newGroupMode = GRT_KP_GROUP; break;
		default:   //error
			return;
	}
	SetSelectionListGroup(newGroupMode,1);
}
//---------------------------------------------------------------------------


void __fastcall TSelectChannelDial::m_ChannelsListSrcMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	int index = m_ChannelsListSrc->ItemAtPos(TPoint(X,Y),true);
	if(index == -1)
		return;
	OnListItemSelect(0,index);
}
//---------------------------------------------------------------------------

void __fastcall TSelectChannelDial::m_ChannelsListDstMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	int index = m_ChannelsListDst->ItemAtPos(TPoint(X,Y),true);
	if(index == -1)
		return;
	OnListItemSelect(1,index);

}

void TSelectChannelDial::OnListItemSelect(int listIndex, int index)
{
	sSelListItemElemDesc* pItem = (sSelListItemElemDesc*)getSelectionList(listIndex)->Items->Objects[index];
	if((pItem->bIsGroup) || (selectMode != SELM_BY_CHANNEL))
	{
		for(int i=0;i<getSelectionList(listIndex)->Count;i++)
		{
			sSelListItemElemDesc* pOtherItem = (sSelListItemElemDesc*)getSelectionList(listIndex)->Items->Objects[i];

			if(bSingleSelect)
			{
				if((pOtherItem->groupId == pItem->groupId) && (pOtherItem->bIsGroup))
					getSelectionList(listIndex)->Selected[i] = true;
				//else
				//	getSelectionList(listIndex)->Selected[i] = false;

			}
			else
			{
				if((pOtherItem->groupId == pItem->groupId) && (!pOtherItem->bIsGroup))
					getSelectionList(listIndex)->Selected[i] = true;
				else
					getSelectionList(listIndex)->Selected[i] = false;
			}
		}

		//if(pItem->bIsGroup)
		//	getSelectionList(listIndex)->Selected[index] = false;
	}
	else
	{
		for(int i=0;i<getSelectionList(listIndex)->Count;i++)
		{
			sSelListItemElemDesc* pOtherItem = (sSelListItemElemDesc*)getSelectionList(listIndex)->Items->Objects[i];

			if(channels[pOtherItem->channelId].channelId == channels[pItem->channelId].channelId)
			{
				getSelectionList(listIndex)->Selected[i] = 1;
            }
		}
    }

	//Show channel info
	if(!pItem->bIsGroup && (pItem->channelId>=0))
	{
		m_ChannelInfoMemo->Clear();

		sSelChannelDesc chInfo = channels[pItem->channelId];

		UnicodeString scanGroupsStr;
		UnicodeString kpGroupsStr;

		std::map<UINT, 	std::set<int> >::iterator it = channelToGroup[GRT_SCAN_GROUP].find(chInfo.itemId);
		if(it!=channelToGroup[GRT_SCAN_GROUP].end())
		{
			std::set<int>::iterator setIt = it->second.begin();
			while(setIt != it->second.end())
			{
				scanGroupsStr+=StringFormatU(L"%d ",*setIt);
				setIt++;
			}
		}

		it = channelToGroup[GRT_KP_GROUP].find(chInfo.itemId);
		if(it!=channelToGroup[GRT_KP_GROUP].end())
		{
			std::set<int>::iterator setIt = it->second.begin();
			while(setIt != it->second.end())
			{
				kpGroupsStr+=StringFormatU(L"%d ",*setIt);
				setIt++;
			}
		}

		m_ChannelInfoMemo->Text = StringFormatU(L"�������������: 0x%X\r\n���: \"%s\"\r\n�����������: \"%s\"\r\n����� ��: %s\r\n����� ����-������: %s\r\n",
											chInfo.channelId,chInfo.name,chInfo.title,
											kpGroupsStr.c_str(),scanGroupsStr.c_str());


        //------------TEST--------------//
        /*UnicodeString tempChList;

        std::map<int, 	std::set<UINT> >::iterator itn = groupToChannel[GRT_KP_GROUP].begin();
        while(itn!=groupToChannel[GRT_KP_GROUP].end())
        {
            std::set<UINT>::iterator setIt = itn->second.begin();
            int nSG = itn->first;
            tempChList += StringFormatU(L"�� - %d: \r\n",nSG);

            while(setIt != itn->second.end())
			{
                UINT i = *setIt;
                //if(channels[i].kpNumber == nSG)
                 {
                  tempChList += StringFormatU(L"  -  ����� 0x%X (%d): %s\r\n",
                             channels[i].channelId,channels[i].channelId,channels[i].title.c_str());
                 }


				setIt++;
			}

            itn++;
        }

        m_ChannelInfoMemo->Text = tempChList;  */
	}

}

int TSelectChannelDial::GetFirstSelection(int listIndex)
{
	int i;

	for (i=0; i < getSelectionList(listIndex)->Items->Count; i++)
	{
		sSelListItemElemDesc* pItem = (sSelListItemElemDesc*)getSelectionList(listIndex)->Items->Objects[i];
		if (getSelectionList(listIndex)->Selected[i] && !pItem->bIsGroup)
			return i;
	}

	return -1;
}

void TSelectChannelDial::MoveSelected(int listIndex, TStrings *Items)
{
	int i;

	for (i=getSelectionList(listIndex)->Items->Count-1; i >= 0; i--)
	{
		sSelListItemElemDesc* pItem = (sSelListItemElemDesc*)getSelectionList(listIndex)->Items->Objects[i];
		if (getSelectionList(listIndex)->Selected[i] && !pItem->bIsGroup)
		{
			channels[pItem->channelId].colomnIndex = !listIndex;
		}
	}
	UpdateSelectionList(0);
	UpdateSelectionList(1);
}

void TSelectChannelDial::SetItem(int listIndex, int Index)
{
	int MaxIndex;

	getSelectionList(listIndex)->SetFocus();
	MaxIndex = getSelectionList(listIndex)->Items->Count - 1;

	if (Index == -1)
		Index = 0;
	else if (Index > MaxIndex)
		Index = MaxIndex;

	if(Index>=getSelectionList(listIndex)->Items->Count)
		return;
	getSelectionList(listIndex)->Selected[Index] = true;

	SetButtons();
}

void TSelectChannelDial::SetButtons()
{
	bool SrcEmpty, DstEmpty;

	SrcEmpty = (m_ChannelsListSrc->Items->Count == 0);
	DstEmpty = (m_ChannelsListDst->Items->Count == 0);
	m_SingleMoveRightBtn->Enabled = (! SrcEmpty);
	m_MultMoveRightBtn->Enabled = (! SrcEmpty);
	m_SingleMoveLeftBtn->Enabled = (! DstEmpty);
	m_MultMoveLeftBtn->Enabled = (! DstEmpty);
}
//---------------------------------------------------------------------------

void __fastcall TSelectChannelDial::m_SingleMoveRightBtnClick(TObject *Sender)
{
	int Index;
	Index = GetFirstSelection(0);
	MoveSelected(0, m_ChannelsListDst->Items);

	UpdateSelectionList(0);
	UpdateSelectionList(1);
	SetItem(0, Index);
}
//---------------------------------------------------------------------------

void __fastcall TSelectChannelDial::m_SingleMoveLeftBtnClick(TObject *Sender)
{
	int Index;
	Index = GetFirstSelection(1);
	MoveSelected(1, m_ChannelsListSrc->Items);

	UpdateSelectionList(0);
	UpdateSelectionList(1);
	SetItem(1, Index);
}
//---------------------------------------------------------------------------

void __fastcall TSelectChannelDial::m_MultMoveRightBtnClick(TObject *Sender)
{
	for(unsigned int i=0;i<channels.size();i++)
		channels[i].colomnIndex = 1;
	UpdateSelectionList(0);
	UpdateSelectionList(1);
	SetItem(0, 0);
}
//---------------------------------------------------------------------------

void __fastcall TSelectChannelDial::m_MultMoveLeftBtnClick(TObject *Sender)
{
	for(unsigned int i=0;i<channels.size();i++)
		channels[i].colomnIndex = 0;
	UpdateSelectionList(0);
	UpdateSelectionList(1);
	SetItem(1, 0);
}




//---------------------------Initiualisation/Getting result------------------------

bool CIDStrToArray( UnicodeString str, std::vector<UINT>& arr)
{
    if(str.Length() == 0)
		return true;
    const wchar_t* symbols = L"0123456789ABCDEFx, \r\n";
	int len = wcslen(symbols);

	// check for wrong symbols
	for(int i=0;i<str.Length();i++)
	{
		if(!wcschr(symbols, str.c_str()[i]))
			return false;
	}

	// remove whitespaces
	for(int i=0;i<str.Length();i++)
	{
		switch(str.c_str()[i])
		{
			case L' ':
			case L'\r':
			case L'\n':
			str.Delete(i+1,1);
				break;
		};
	}

	UnicodeString temp;
	UINT nChannel;
	for(int i=0;i<str.Length();i++)
	{
		if((str.c_str()[i] == L',') && temp.Length())
		{
			nChannel = temp.ToInt();
			arr.push_back(nChannel);
			temp = L"";
		}
		else
			temp += str.c_str()[i];
	}
	nChannel = temp.ToInt();
	arr.push_back(nChannel);
	return true;
}

UnicodeString CIDArrayToStr(std::vector<UINT>& arr)
{
    UnicodeString str;
	for(unsigned int i = 0; i < arr.size(); i++)
	{
		const wchar_t* ending = (i!=(arr.size()-1)) ? L", " : L"";
		str+=StringFormatU(L"0x%X%s", arr[i],ending);
	}
	return str;
}

bool TSelectChannelDial::SetSelectedAsString(UnicodeString str)
{
	return CIDStrToArray(str, inOutChannelsBuffer);
}

bool TSelectChannelDial::SetSelectedAsArray(std::vector<UINT>& data)
{
	inOutChannelsBuffer = data;
	return true;
}

UnicodeString TSelectChannelDial::GetSelectedAsString()
{
	PushSelectedToChannelsBuffer();

	return CIDArrayToStr(inOutChannelsBuffer);
}

void TSelectChannelDial::PushSelectedToChannelsBuffer()
{
	inOutChannelsBuffer.clear();

	if(!bSingleSelect) //get all from dest list
	{
		for (unsigned int i=0; i < channels.size(); i++)
		{
			if(channels[i].colomnIndex==1)
				inOutChannelsBuffer.push_back(channels[i].channelId);
		}
	}
	else  //get only selected from src list
	{
		for (int i=0; i < m_ChannelsListSrc->Items->Count; i++)
		{
			sSelListItemElemDesc* pItem = (sSelListItemElemDesc*)m_ChannelsListSrc->Items->Objects[i];

			if(!pItem->bIsGroup && m_ChannelsListSrc->Selected[i])
				inOutChannelsBuffer.push_back(channels[pItem->channelId].channelId);
		}
    }
};

bool TSelectChannelDial::SetSelectedAsGroupIdx(int nGroup)
{
	sScanChannelDescription scanChanDesc;

	inOutChannelsBuffer.clear();
	std::map<int, 	std::set<UINT> >::iterator mapIt = groupToChannel[groupMode[0]].find(nGroup);
	if(mapIt==groupToChannel[groupMode[0]].end())
		return false;

	std::set<UINT>::iterator setIt =  mapIt->second.begin();
	while(setIt!=mapIt->second.end())
	{
		inOutChannelsBuffer.push_back(channels[(*setIt)].channelId);
		setIt++;
	}
	if(inOutChannelsBuffer.size()==0)
		return false;
	return true;
}

int TSelectChannelDial::GetSelectedAsGroupIdx()
{
	for (int i=0; i < m_ChannelsListSrc->Items->Count; i++)
	{
		sSelListItemElemDesc* pItem = (sSelListItemElemDesc*)m_ChannelsListSrc->Items->Objects[i];

		if(m_ChannelsListSrc->Selected[i])
			return pItem->groupId;
	}
	return -1;

	/*PushSelectedToChannelsBuffer();

	if(inOutChannelsBuffer.empty())
		return -1;

	int chId = inOutChannelsBuffer[0];

	eSortChannelGroupType gt;
	switch(selectMode)
	{
		//case SELM_BY_CHANNEL: return -1;
		case SELM_BY_KP: gt = GRT_KP_GROUP; break;
		case SELM_BY_SCAN_GROUP: gt = GRT_SCAN_GROUP; break;
		default:
			return -1;
	}
	std::map<UINT, 	std::set<int> >::iterator it = channelToGroup[gt].find(chId);
	if(it==channelToGroup[gt].end())
		return -1;

	 std::set<int>::iterator setIt = it->second.begin();
	 if(setIt!=it->second.end())
		return *setIt;
	 return -1;
	 */
}

std::vector<UINT> TSelectChannelDial::GetSelectedAsArray()
{
	PushSelectedToChannelsBuffer();
    return inOutChannelsBuffer;
}

void TSelectChannelDial::Init(bool bSingleSelect, eSelectChannelMode selectMode,eSortChannelMode newSortMode,eSortChannelGroupType newGroupMode)
{
	this->bSingleSelect = bSingleSelect;
	this->selectMode = selectMode;
	this->sortMode[0] = newSortMode;
	this->sortMode[1] = newSortMode;
	this->groupMode[0] = newGroupMode;
	this->groupMode[1] = newGroupMode;

	m_ChannelsListSrc->MultiSelect = !bSingleSelect;
}
//---------------------------------------------------------------------------

void __fastcall TSelectChannelDial::m_PrintBtnClick(TObject *Sender)
{
    m_ChannelInfoMemo->Text = m_ChannelsListSrc->Items->Text;
    m_ChannelInfoMemo->Refresh();
}
