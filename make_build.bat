@ECHO OFF


:choice
	set /P c=Make Debug or Release version [D/R]?
	if /I "%c%" EQU "D" goto :debug_ver
	if /I "%c%" EQU "R" goto :relase_ver
	goto :choice

:debug_ver
	set bin_folder=Debug
	set build_mark=d
	goto endchoice
:relase_ver
	set bin_folder=Release
	set build_mark=r
	goto endchoice
:endchoice


SET IncludeCppBuilderFiles=0
SET QUESTION="Include CppBuilder files [Y/N]? "
call :AskQuestion
SET IncludeCppBuilderFiles=%QUESTION_RESULT%

setlocal enableextensions
set dst_folder=.\Builds\Autocon_%build_mark%_%DATE:/=_%
mkdir ".\Builds"
mkdir %dst_folder%



copy ".\%bin_folder%\config.ini" ".\%dst_folder%\config.ini"
copy ".\%bin_folder%\Autocon.exe" ".\%dst_folder%\Autocon.exe"
copy ".\%bin_folder%\calibration.dat" ".\%dst_folder%\calibration.dat"
copy ".\%bin_folder%\calibration~.dat" ".\%dst_folder%\calibration~.dat"
copy ".\%bin_folder%\iconv.dll" ".\%dst_folder%\iconv.dll"
copy ".\%bin_folder%\libhpdf.dll" ".\%dst_folder%\libhpdf.dll"
copy ".\%bin_folder%\libxml2.dll" ".\%dst_folder%\libxml2.dll"
copy ".\%bin_folder%\zlib1.dll" ".\%dst_folder%\zlib1.dll"

copy ".\dependencies\cg32.dll" ".\%dst_folder%\cg32.dll"

xcopy ".\resources" ".\%dst_folder%\resources\" /S

:EOF

:AskQuestion
SETLOCAL
	:begin_question
	set /P c=%QUESTION%
	:: Y/N
	if /I "%c%" EQU "Y" (
		SET _result=1 
		goto :end_question
	)
	if /I "%c%" EQU "N" (
		SET _result=0 
		goto :end_question
	)
	
	:: D/R
	if /I "%c%" EQU "R" (
		SET _result=1 
		goto :end_question
	)
	if /I "%c%" EQU "D" (
		SET _result=0 
		goto :end_question
	)
	goto :begin_question
:end_question
ENDLOCAL & SET QUESTION_RESULT=%_result%
:end