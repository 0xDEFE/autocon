//---------------------------------------------------------------------------

#ifndef HTMLLoggerH
#define HTMLLoggerH
#include <fstream>

class CHTMLLogger : public Logger::CLoggerListener
{
public:
	CHTMLLogger(void);
	~CHTMLLogger(void);

	void Init(const char* FilePath, const char* LogName);
	void Release();


	void onNewMessage(Logger::sLoggerMessage& Msg);
	Logger::eListenerCollectType getCollectType() {return Logger::LCT_CALL_ON_MESSAGE;};
private:

	void  Write(const char* color, const char* string);
	void StartBanner(const char *logname);
	void EndBanner();
	void WriteTime();

	std::fstream  LogFile;
	bool bLogInit;
};
#endif
