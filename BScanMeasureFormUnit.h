//---------------------------------------------------------------------------

#ifndef BScanMeasureFormUnitH
#define BScanMeasureFormUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------

#include "BScanMeasureUnit.h"
#include "ConfigUnit.h";
#include "Autocon.inc"

enum eBScanMeasureMsgType
{
    BSMM_BTN_MEASURE,
    BSMM_BTN_CLOSE,
    BSMM_UPDATE,
    BSMM_ADD_REGION,
};

typedef void (__closure *TMeasureMsg)(eBScanMeasureMsgType Type, void* pValue);

struct sMeasLocationData
{
    sMeasLocationData(const UnicodeString& name, const UnicodeString& path)
    {
        this->name = name;
        this->path = path;
    };

    UnicodeString name;
    UnicodeString path;
};

class TBScanMeasureForm : public TForm
{
__published:	// IDE-managed Components
    TPageControl *PageControl2;
    TTabSheet *TabSheet1;
    TPanel *Panel3;
    TLabel *Label11;
    TLabel *Label13;
    TLabel *Label14;
    TComboBox *perChannelMeasTags;
    TComboBox *perSignalMeasTags;
    TComboBox *signalMeasTags;
    TBitBtn *MakeMeasureBtn;
    TPanel *Panel1;
    TTabSheet *TabSheet3;
    TLabel *Label1;
    TComboBox *perRegionsMeasTags;
    TSpeedButton *addPolyBtn;
    TSpeedButton *saveBtn;
    TEdit *saveNameEdit;
    TSpeedButton *compareBtn;
    TGridPanel *GridPanel1;
    TGridPanel *GridPanel3;
    TComboBox *compareCombo;
    TGridPanel *GridPanel5;
	TPanel *Panel2;
	TPanel *Panel4;
	TPanel *Panel5;
	TGridPanel *GridPanel4;
	TBitBtn *BitBtn7;
	TBitBtn *BitBtn8;
	TBitBtn *BitBtn9;
	TBitBtn *BitBtn10;
	TBitBtn *BitBtn11;
	TBitBtn *BitBtn12;
	TSpeedButton *MultX1Btn;
	TSpeedButton *MultX10Btn;
	TSpeedButton *SpeedButton4;
	TPanel *Panel6;
	TGridPanel *GridPanel2;
	TComboBox *loadCombo;
	TSpeedButton *loadBtn;
	TPanel *Panel7;
	TPanel *Panel8;
	TGridPanel *GridPanel6;
	TPanel *Panel9;
	TSpeedButton *SpeedButton1;
	TSpeedButton *CloseBtn;
	TTabSheet *TabSheet2;
	TPanel *Panel10;
    void __fastcall OnChangeMeasPos(TObject *Sender);
    void __fastcall OnChangePosMult(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall CloseBtnClick(TObject *Sender);
    void __fastcall MakeMeasureBtnClick(TObject *Sender);
    void __fastcall OnSelectMeasureTag(TObject *Sender);
    void __fastcall loadBtnClick(TObject *Sender);
    void __fastcall addPolyBtnClick(TObject *Sender);
    void __fastcall saveBtnClick(TObject *Sender);
    void __fastcall compareBtnClick(TObject *Sender);
	void __fastcall SpeedButton1Click(TObject *Sender);
private:	// User declarations
    TMeasureMsg measureMsgProc;
    BScanMeasure measure;
    BScanMeasure measureToCompare;
    bool bEnableCompare;
    float PosMultiplier;
    cJointTestingReport* pRep;

    void doMeasure();
    void locateMeasureData();
    void updateMeasureData();

    bool loadMeasure(int idx, BScanMeasure& meas);

    std::vector<sMeasLocationData> measLocationData;
public:		// User declarations
    __fastcall TBScanMeasureForm(TComponent* Owner);

    void initialize(cJointTestingReport* pRep, PolyPt pos, const std::vector<CID>& channels,TMeasureMsg msgProc);

    BScanMeasure& getMeasure() {return measure;}
//    void Initialize(TMeasureMsg measureMsgProc,
//                        TStringList* pMeasureItems,
//                        TStringList* pPerSignalItems,
//                        TStringList* pPerChannelItems,
//                        TStringList* pLoadItems);
};
//---------------------------------------------------------------------------
extern PACKAGE TBScanMeasureForm *BScanMeasureForm;
//---------------------------------------------------------------------------

extern cACConfig * ACConfig;

#endif
