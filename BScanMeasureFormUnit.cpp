//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BScanMeasureFormUnit.h"
#include "BScanMeasureViewResUnit.h"

#include "AutoconMain.h"

extern cAutoconMain *AutoconMain;

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TBScanMeasureForm *BScanMeasureForm;
//---------------------------------------------------------------------------
__fastcall TBScanMeasureForm::TBScanMeasureForm(TComponent* Owner)
    : TForm(Owner)
{
    measureMsgProc = NULL;
    PosMultiplier = 1;

    pRep = NULL;

    measure.addDriver<AmplitudeMeasDrv_db>();
    measure.addDriver<AmplitudeMeasDrv_raw>();
    measure.addDriver<DelayMeasDrv>();
    measure.addDriver<MaxMeasDrv>();
    measure.addDriver<MinMeasDrv>();
    measure.addDriver<SumMeasDrv>();
    measure.addDriver<AverageMeasDrv>();

    updateMeasureData();
}

void TBScanMeasureForm::updateMeasureData()
{
    signalMeasTags->Items->Clear();
    perSignalMeasTags->Items->Clear();
    perChannelMeasTags->Items->Clear();
    perRegionsMeasTags->Items->Clear();

    BScanMeasureDriver* drv = measure.firstDriver();
    while(drv)
    {
        if(drv->flags() & BSM_ABIL_SIGNAL)
            signalMeasTags->Items->Add(drv->tag());
        if(drv->flags() & BSM_ABIL_PER_SIGNAL)
            perSignalMeasTags->Items->Add(drv->tag());
        if(drv->flags() & BSM_ABIL_PER_CHANNEL)
            perChannelMeasTags->Items->Add(drv->tag());
        if(drv->flags() & BSM_ABIL_PER_REGION)
            perRegionsMeasTags->Items->Add(drv->tag());
        drv = measure.nextDriver();
    }
}
//---------------------------------------------------------------------------
void __fastcall TBScanMeasureForm::OnChangeMeasPos(TObject *Sender)
{
	TBitBtn* pBtn = dynamic_cast< TBitBtn* > (Sender);

	CID tmp; //FDV
	bool FLG; //FDV
	for(unsigned int i = 0; i < measure.regions.size(); i++)
	{
		if (measure.regions[i].measureData.size() != 0)    //FDV
		{                                                  //FDV
																		  //FDV
			tmp = *measure.regions[i].measureData[0].channels.begin();    //FDV
																		  //FDV
			FLG = (((tmp >= 0x3F) &&                                      //FDV
				 (tmp <= 0x56)) ||                                        //FDV
																		  //FDV
				((tmp >= 0x5E) &&                                         //FDV
				 (tmp <= 0x68)));                                         //FDV
																		  //FDV

            PolyReg& poly = measure.regions[i].poly;

            switch( pBtn->Tag )
            {
                case 1: //W-
                    poly.grow(PolyPt(-PosMultiplier,0));
                    break;
                case 2: //Up
                    poly.offset(PolyPt(0,PosMultiplier));
                    break;
                case 3: //W+
                    poly.grow(PolyPt(PosMultiplier,0));
                    break;
                case 4: //Left
                    if (FLG) poly.offset(PolyPt(-PosMultiplier,0));
                    break;
                case 5: //Rotate
                    poly.rotate((M_PI / 90.f) * PosMultiplier);
                    break;
                case 6: //Right
                    if (FLG) poly.offset(PolyPt(PosMultiplier,0));
                    break;
                case 7: //H-
                   //	poly.grow(PolyPt(0,-PosMultiplier)); //FDV
          			if (!FLG) poly.offset(PolyPt(-PosMultiplier,0));
                    break;
                case 8: //Down
                   //	poly.offset(PolyPt(0,-PosMultiplier)); //FDV
                    break;
                case 9: //H+
                    //poly.grow(PolyPt(0,PosMultiplier));
                    if (!FLG) poly.offset(PolyPt(PosMultiplier,0));
                    break;
            }
		}
	}

    if(measureMsgProc)
		measureMsgProc(BSMM_UPDATE, NULL);
	doMeasure();
}
//---------------------------------------------------------------------------
void __fastcall TBScanMeasureForm::OnChangePosMult(TObject *Sender)
{
    TSpeedButton* pBtn = dynamic_cast< TSpeedButton* > (Sender);

    PosMultiplier = pBtn->Tag;
}
//---------------------------------------------------------------------------
void __fastcall TBScanMeasureForm::FormShow(TObject *Sender)
{
    if(measureMsgProc)
        measureMsgProc(BSMM_UPDATE, NULL);

    OnSelectMeasureTag(signalMeasTags);
    OnSelectMeasureTag(perSignalMeasTags);
    OnSelectMeasureTag(perChannelMeasTags);
    OnSelectMeasureTag(perRegionsMeasTags);

    locateMeasureData();

    bEnableCompare = false;
}

void TBScanMeasureForm::locateMeasureData()
{
    PathMgr::PushPath(L"$compare_data", L"resources\\measure");

    TSearchRec sr;
	UnicodeString path = PathMgr::GetPath(L"($compare_data)\\") + L"*.xml";
	int iAttributes = faAnyFile;
	int nestingLevel = 1;

    measLocationData.clear();
    if (FindFirst(path, iAttributes, sr) == 0)
	{
		do
		{
			if(sr.Attr & faDirectory)
				continue;
			if(sr.Name.SubString(sr.Name.Length()-3,4) == L".xml")
			{
                BScanMeasure tmpMeas;
                AnsiString path = AnsiString(PathMgr::GetPath(L"($compare_data)\\") + sr.Name);
                if(tmpMeas.load(path.c_str(), AutoconMain->Table, AutoconMain->Config))
                {
					measLocationData.push_back(sMeasLocationData(tmpMeas.name, path));
				}
            }

		} while (FindNext(sr) == 0);
		FindClose(sr);
	}

	loadCombo->Items->Clear();
	for(unsigned int i = 0; i < measLocationData.size(); i++)
	{
		loadCombo->Items->Add(measLocationData[i].name);
	}

//	loadCombo->ItemIndex = loadCombo->Items->IndexOf(ACConfig->Test_EtalonModelName);

    compareCombo->Items->Clear();
    for(unsigned int i = 0; i < measLocationData.size(); i++)
    {
        compareCombo->Items->Add(measLocationData[i].name);
    }
}

//---------------------------------------------------------------------------

void TBScanMeasureForm::initialize(cJointTestingReport* pRep, Vec2f pos, const std::vector<CID>& channels,TMeasureMsg msgProc)
{
    this->pRep = pRep;
    measure.clear();

//    BScanRegion reg;
//    reg.poly.points.push_back(pos + Vec2i(-10,10));
//    reg.poly.points.push_back(pos + Vec2i(10,10));
//    reg.poly.points.push_back(pos + Vec2i(10,-10));
//    reg.poly.points.push_back(pos + Vec2i(-10,-10));
//
//    BScanMeasureData measData;
//    for(int i = 0; i < channels.size(); i++)
//        measData.channels.insert(channels[i]);
//    reg.measureData.push_back(measData);
//    measure.regions.push_back(reg);

    measureMsgProc = msgProc;
}
void __fastcall TBScanMeasureForm::CloseBtnClick(TObject *Sender)
{
    if(measureMsgProc)
        measureMsgProc(BSMM_BTN_CLOSE, NULL);
}
//---------------------------------------------------------------------------

void __fastcall TBScanMeasureForm::MakeMeasureBtnClick(TObject *Sender)
{
    doMeasure();
}
//---------------------------------------------------------------------------

void TBScanMeasureForm::doMeasure()
{
    measure.calculate(pRep);

    if(bEnableCompare)
    {
        double totalErr = measure.compare(measureToCompare);
//        measValLabel->Caption = L"Err: " + IntToStr((int)(totalErr*100.0)) + L"%";
	}
	else
	{
		TFloatFormat ff;
		ff << ffFixed;
//        measValLabel->Caption = FloatToStrF(measure.measureValue,ffFixed, 15, 8);
    }



//    if(!measure.regions.empty())
//    {
//        if(!measure.regions[0].measureData.empty())
//        {
//            double val = measure.regions[0].measureData[0].measureValue;
//
//
//        }
//        else measValLabel->Caption = "---";
//    }
//    else measValLabel->Caption = "---";
}

void __fastcall TBScanMeasureForm::OnSelectMeasureTag(TObject *Sender)
{
    TComboBox* pCombo = dynamic_cast<TComboBox*>(Sender);


    if(pCombo->Tag == 4)
    {
        measure.perRegionTag = AnsiString(pCombo->Text).c_str();
    }
    else
        for(unsigned int i = 0; i < measure.regions.size(); i++)
        {
            for(unsigned int j = 0; j < measure.regions[i].measureData.size(); j++)
            {
                switch(pCombo->Tag)
				{
                    case 1:
                        measure.regions[i].measureData[j].signalTag = AnsiString(pCombo->Text).c_str();
                        break;
                    case 2:
                        measure.regions[i].measureData[j].perSignalTag = AnsiString(pCombo->Text).c_str();
                        break;
                    case 3:
                        measure.regions[i].measureData[j].perChannelTag = AnsiString(pCombo->Text).c_str();
                        break;
                }
            }
        }
    doMeasure();

}
//---------------------------------------------------------------------------

void __fastcall TBScanMeasureForm::loadBtnClick(TObject *Sender)
{
	loadMeasure(loadCombo->ItemIndex, measure);
    if(measureMsgProc)
		measureMsgProc(BSMM_UPDATE, NULL);
	SpeedButton1->Enabled = true;
}

bool TBScanMeasureForm::loadMeasure(int idx, BScanMeasure& meas)
{
//    int idx = loadCombo->ItemIndex;

    if((idx < 0) || (idx >= (int)measLocationData.size()))
        return false;
//    if(measLocationData[idx].name != loadCombo->Items->Strings[idx])
//        return false;

    if(!meas.load(AnsiString(measLocationData[idx].path).c_str(), AutoconMain->Table, AutoconMain->Config))
    {
        return false;
	}
    return true;
}
//---------------------------------------------------------------------------

void __fastcall TBScanMeasureForm::addPolyBtnClick(TObject *Sender)
{
	if(measureMsgProc)
        measureMsgProc(BSMM_ADD_REGION, NULL);
}
//---------------------------------------------------------------------------

void __fastcall TBScanMeasureForm::saveBtnClick(TObject *Sender)
{
    measure.name = saveNameEdit->Text;
    measure.save(AnsiString(PathMgr::GetPath(L"($compare_data)\\") + saveNameEdit->Text + L".xml").c_str(), AutoconMain->Table);
}
//---------------------------------------------------------------------------

void __fastcall TBScanMeasureForm::compareBtnClick(TObject *Sender)
{
    bEnableCompare = true;
    if(!loadMeasure(compareCombo->ItemIndex, measureToCompare))
        return;

    measure.setRegionsFrom(measureToCompare);
//    measure.calculate(AutoconMain->Rep);

    doMeasure();

   if(measureMsgProc)
        measureMsgProc(BSMM_UPDATE, NULL);
}
//---------------------------------------------------------------------------

void __fastcall TBScanMeasureForm::SpeedButton1Click(TObject *Sender)
{
//	loadMeasure(loadCombo->ItemIndex, measure);
	doMeasure();

	sChannelDescription chanDesc;
	UnicodeString ChannelName; //!< ��� ������
	int Count = 0;
	int OKCount = 0;

	BScanMeasureViewRes->List->Clear();
	for (int i = 0; i <= 90; i++)
	{
	  AutoconMain->Table->ItemByCID(i + AutoconStartCID, &chanDesc);
	  ChannelName = chanDesc.Title;

	  for (int p = 0; p < measure.ResList[i].PolyCount; p++)
	  {
		if (ACConfig->BScanMeasure[i][p])
		{
			Count++;
			if (measure.ResList[i].EchoCount[p] > 0) OKCount++;
		}
		BScanMeasureViewRes->List->AddItem(Format("�����: %s; ������: %d; ����������: %d", ARRAYOFCONST((ChannelName, p, measure.ResList[i].EchoCount[p]))), (TObject*)((i << 4) + p));
		BScanMeasureViewRes->List->Checked[BScanMeasureViewRes->List->Count - 1] = ACConfig->BScanMeasure[i][p];
	  }
	}
	BScanMeasureViewRes->Label1->Caption = Format("���������: %d �� %d", ARRAYOFCONST((OKCount, Count)));
//	Panel9->Caption = Format("%d �� %d", ARRAYOFCONST((OKCount, Count)));
	Panel9->Caption = Format("%d%%", ARRAYOFCONST(((int)((100 * OKCount) / Count))));

	#ifdef TestRailFull�onclusion
	if ((int)((100 * OKCount) / Count) >= ACConfig->BScanMeasureTh)	 Panel10->Caption = "�������� ����� � ������";
						else Panel10->Caption = "�������� � ������ �� �����";
	#endif
	#ifndef TestRailFull�onclusion
//	Panel10->Caption = Format("%d%%", ARRAYOFCONST(((int)((100 * OKCount) / Count))));
	#endif


	if (ACConfig->ShowFiltrParam_)
		if (BScanMeasureViewRes->ShowModal() == 1)
		{
			for (int i = 0; i < BScanMeasureViewRes->List->Count; i++) {

			  int tmp = (int)BScanMeasureViewRes->List->Items->Objects[i];
			  ACConfig->BScanMeasure[tmp >> 4][tmp & 0x0F] = BScanMeasureViewRes->List->Checked[i];
			}
		}

	if(measureMsgProc)
		measureMsgProc(BSMM_UPDATE, NULL);

}
//---------------------------------------------------------------------------

