//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AScanViewUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TAScanViewForm *AScanViewForm;
//---------------------------------------------------------------------------
__fastcall TAScanViewForm::TAScanViewForm(TComponent* Owner)
    : TForm(Owner)
{
    OnLeftBtnClicked = NULL;
    OnRightBtnClicked = NULL;
    OnCloseBtnClicked = NULL;

    gateBtns[0] = SelGateBtn1;
    gateBtns[1] = SelGateBtn2;
    gateBtns[2] = SelGateBtn3;
    gateBtns[3] = SelGateBtn4;
    gateBtns[4] = SelGateBtn5;
    gateBtns[5] = SelGateBtn6;

    prevUpdateActiveBtn.first = -1;
    prevUpdateActiveBtn.second = "";
}
//---------------------------------------------------------------------------
void __fastcall TAScanViewForm::PointsCheckBoxClick(TObject *Sender)
{
    AScanSeries1->Pointer->Visible = PointsCheckBox->Checked;
    AScanSeries2->Pointer->Visible = PointsCheckBox->Checked;
    AScanSeries3->Pointer->Visible = PointsCheckBox->Checked;
    AScanSeries4->Pointer->Visible = PointsCheckBox->Checked;
    AScanSeries5->Pointer->Visible = PointsCheckBox->Checked;
    AScanSeries6->Pointer->Visible = PointsCheckBox->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TAScanViewForm::AscanChartClick(TObject *Sender)
{
    this->Close();
}
//---------------------------------------------------------------------------

void __fastcall TAScanViewForm::Button1Click(TObject *Sender)
{
    if(OnLeftBtnClicked)
    {
        OnLeftBtnClicked(this);
    }
}
//---------------------------------------------------------------------------

void __fastcall TAScanViewForm::Button2Click(TObject *Sender)
{
    if(OnRightBtnClicked)
    {
        OnRightBtnClicked(this);
    }
}
//---------------------------------------------------------------------------

void __fastcall TAScanViewForm::Button3Click(TObject *Sender)
{
    this->Close();
    if(OnCloseBtnClicked)
    {
        OnCloseBtnClicked(this);
    }
}
//---------------------------------------------------------------------------

void __fastcall TAScanViewForm::FormCreate(TObject *Sender)
{
    ChartTabSheet->TabVisible = false;
    InfoTabSheet->TabVisible = false;
    PageControl1->ActivePageIndex = 0;
}
//---------------------------------------------------------------------------
void TAScanViewForm::setInfoMode(bool bVal)
{
    if(bVal)
    {
        PageControl1->ActivePageIndex = 1;
        Button2->Visible = false;
        Button1->Visible = false;
        Panel5->Caption = "";
    }
    else
    {
        PageControl1->ActivePageIndex = 0;
        Button2->Visible = true;
        Button1->Visible = true;
        //Panel5->Visible = true;
    }
}

void TAScanViewForm::setInfo(UnicodeString chName, sScanChannelParams* pGate1, sScanChannelParams* pGate2)
{
    m_ChannelNameLabel->Caption = chName;
    if(!pGate2)
    {
        m_ChannelNameNameLabel->Caption = L"��� ������:";
        m_KuNameLabel->Caption = L"�������� ����������������:";
        m_AttNameLabel->Caption = L"����������:";
        m_StGateNameLabel->Caption = L"������ ������ ���:";
        m_EdGateNameLabel->Caption = L"����� ������ ���:";
        m_TVGNameLabel->Caption = L"���:";
        m_PrismDelayNameLabel->Caption = L"����� � ������:";

        m_KuLabel->Caption = IntToStr(pGate1->Sens) + L" ��";
        m_AttLabel->Caption = IntToStr(pGate1->Gain) + L" ��";
        m_StGateLabel->Caption = IntToStr(pGate1->StGate) + L" ���";
        m_EdGateLabel->Caption = IntToStr(pGate1->EdGate) + L" ���";
        m_TVGLabel->Caption = IntToStr(pGate1->TVG) + L" ���";
        m_PrismDelayLabel->Caption = FloatToStr(float(pGate1->PrismDelay)/10.f) + L" ���";
    }
    else
    {
        m_ChannelNameNameLabel->Caption = L"��� ������:";
        m_KuNameLabel->Caption = L"�������� ���������������� (1,2):";
        m_AttNameLabel->Caption = L"���������� (1,2):";
        m_StGateNameLabel->Caption = L"������ ������ ��� (1,2):";
        m_EdGateNameLabel->Caption = L"����� ������ ��� (1,2):";
        m_TVGNameLabel->Caption = L"��� (1,2):";
        m_PrismDelayNameLabel->Caption = L"����� � ������ (1,2):";

        m_KuLabel->Caption = IntToStr(pGate1->Sens) + L" ��, " + IntToStr(pGate2->Sens) + L" ��";
        m_AttLabel->Caption = IntToStr(pGate1->Gain) + L" ��, " + IntToStr(pGate2->Gain) + L" ��";
        m_StGateLabel->Caption = IntToStr(pGate1->StGate) + L" ���, " + IntToStr(pGate2->StGate) + L" ���";
        m_EdGateLabel->Caption = IntToStr(pGate1->EdGate) + L" ���, " + IntToStr(pGate2->EdGate) + L" ���";
        m_TVGLabel->Caption = IntToStr(pGate1->TVG) + L" ���, " + IntToStr(pGate2->TVG) + L" ���";
        m_PrismDelayLabel->Caption = FloatToStr(float(pGate1->PrismDelay)/10.f) + L" ���, " +
                                        FloatToStr(float(pGate2->PrismDelay)/10.f) + L" ���";
    }
}

void TAScanViewForm::setInfo(UnicodeString chName, int Ku, int Att, int gateCount, int StGate1, int EdGate1,int StGate2, int EdGate2, int TVG, int PrismDelay)
{
    m_ChannelNameLabel->Caption = chName;
    m_KuLabel->Caption = IntToStr(Ku) + L" ��";
    m_AttLabel->Caption = IntToStr(Att) + L" ��";
    if(gateCount == 1)
    {
        m_StGateNameLabel->Caption = L"������ ������ ���:";
        m_EdGateNameLabel->Caption = L"����� ������ ���:";
        m_StGateLabel->Caption = IntToStr(StGate1) + L" ���";
        m_EdGateLabel->Caption = IntToStr(EdGate1) + L" ���";
    }
    else
    {
        m_StGateNameLabel->Caption = L"������ ������ ��� [����,�����];";
        m_EdGateNameLabel->Caption = L"����� ������ ��� [����,�����]:";
        m_StGateLabel->Caption = IntToStr(StGate1) + L" ���, " + IntToStr(StGate2) + L" ���";
        m_EdGateLabel->Caption = IntToStr(EdGate1) + L" ���, " + IntToStr(EdGate2) + L" ���";
    }
    m_TVGLabel->Caption = IntToStr(TVG) + L" ���";
    m_PrismDelayLabel->Caption = FloatToStr(float(PrismDelay)/10.f) + L" ���";
}
void __fastcall TAScanViewForm::OnSelGateBtnClicked(TObject *Sender)
{
    TSpeedButton* pBtn = dynamic_cast<TSpeedButton*>(Sender);
    assert(pBtn->Tag < 6);

    prevUpdateActiveBtn.first = pBtn->Tag;
    prevUpdateActiveBtn.second = pBtn->Caption;

    const float GateBorderRectWidth = 0.3f;

    GateMainSeries1->Visible = true;
    GateStSeries1->Visible = true;
    GateEdSeries1->Visible = true;
    GateMainSeries2->Visible = (btnGateInfo[pBtn->Tag].gateCnt == 2);
    GateStSeries2->Visible = (btnGateInfo[pBtn->Tag].gateCnt == 2);
    GateEdSeries2->Visible = (btnGateInfo[pBtn->Tag].gateCnt == 2);

    GateMainSeries1->BeginUpdate();
    GateStSeries1->BeginUpdate();
    GateEdSeries1->BeginUpdate();

    this->GateMainSeries1->X0 =  btnGateInfo[pBtn->Tag].stGate[0];
    this->GateMainSeries1->X1 =  btnGateInfo[pBtn->Tag].edGate[0];

    this->GateStSeries1->X0 =  btnGateInfo[pBtn->Tag].stGate[0];
    this->GateStSeries1->X1 =  btnGateInfo[pBtn->Tag].stGate[0] + GateBorderRectWidth;
    this->GateEdSeries1->X0 =  btnGateInfo[pBtn->Tag].edGate[0] - GateBorderRectWidth;
    this->GateEdSeries1->X1 =  btnGateInfo[pBtn->Tag].edGate[0];

    GateMainSeries1->EndUpdate();
    GateStSeries1->EndUpdate();
    GateEdSeries1->EndUpdate();

    if(btnGateInfo[pBtn->Tag].gateCnt == 2)
    {
        GateMainSeries2->BeginUpdate();
        GateStSeries2->BeginUpdate();
        GateEdSeries2->BeginUpdate();

        this->GateMainSeries2->X0 =  btnGateInfo[pBtn->Tag].stGate[1];
        this->GateMainSeries2->X1 =  btnGateInfo[pBtn->Tag].edGate[1];

        this->GateStSeries2->X0 =  btnGateInfo[pBtn->Tag].stGate[1];
        this->GateStSeries2->X1 =  btnGateInfo[pBtn->Tag].stGate[1] + GateBorderRectWidth;
        this->GateEdSeries2->X0 =  btnGateInfo[pBtn->Tag].edGate[1] - GateBorderRectWidth;
        this->GateEdSeries2->X1 =  btnGateInfo[pBtn->Tag].edGate[1];

        GateMainSeries2->EndUpdate();
        GateStSeries2->EndUpdate();
        GateEdSeries2->EndUpdate();
    }
}
void TAScanViewForm::setGateBtn(unsigned int idx, UnicodeString name, TColor btnColor, int gateCnt, int stGate0, int edGate0, int stGate1, int edGate1)
{
    assert(idx < 6);
    gateBtns[idx]->Visible = true;
    gateBtns[idx]->Caption = name;
    gateBtns[idx]->Font->Color = btnColor;
    btnGateInfo[idx].gateCnt = gateCnt;
    btnGateInfo[idx].stGate[0] =  stGate0;
    btnGateInfo[idx].edGate[0] =  edGate0;
    btnGateInfo[idx].stGate[1] =  stGate1;
    btnGateInfo[idx].edGate[1] =  edGate1;

}
void TAScanViewForm::removeGateButtons()
{
    for(int i = 0; i < 6; i++)
    {
        gateBtns[i]->Visible = false;
    }
}
//---------------------------------------------------------------------------
void TAScanViewForm::updateButtons()
{
    int firstVisibleBtn = -1;
    if((prevUpdateActiveBtn.first == -1) ||
        (prevUpdateActiveBtn.second != gateBtns[prevUpdateActiveBtn.first]->Caption) || (!gateBtns[prevUpdateActiveBtn.first]->Visible))
    {
        for(int i = 0; i < 6; i++)
        {
            if(gateBtns[i]->Visible)
            {
                firstVisibleBtn = i;
                break;
            }
        }
    }
    else firstVisibleBtn = prevUpdateActiveBtn.first;

    ButtonsLabel->Visible = (firstVisibleBtn != -1);
    ButtonsPanel->Visible = (firstVisibleBtn != -1);

    if( firstVisibleBtn != -1)
    {
        gateBtns[ firstVisibleBtn ]->Click();
        gateBtns[ firstVisibleBtn ]->Down = true;
    }
}
//---------------------------------------------------------------------------

