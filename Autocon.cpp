//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include "Autocon.inc"

//---------------------------------------------------------------------------
#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
USEFORM("ReportViewerUnit.cpp", ReportViewerForm);
USEFORM("ScanScriptSimUnit.cpp", ScanScriptSimForm);
USEFORM("TuneTableFormUnit.cpp", TuneTableForm);
USEFORM("ScreenMessageUnit2.cpp", ScreenMessageForm2);
USEFORM("ScreenMessageUnit3.cpp", ScreenMessageForm3);
USEFORM("SelectChannelUnit.cpp", SelectChannelDial);
USEFORM("MeasureSelChannelFormUnit.cpp", MeasureSelChannelForm);
USEFORM("FotoUnit.cpp", FotoForm);
USEFORM("HeaderTestUnit.cpp", HeaderTestForm);
USEFORM("FiltrParamUnit.cpp", FiltrParamForm);
USEFORM("EnterParamFrameUnit.cpp", EnterParamFrame); /* TFrame: File Type */
USEFORM("MainUnit.cpp", MainUnit);
USEFORM("ManualCtrlPanelUnit.cpp", ManualCtrlPanel); /* TFrame: File Type */
USEFORM("ManualTuneFrameUnit.cpp", ManualTuneFrame); /* TFrame: File Type */
USEFORM("MainMenuUnit.cpp", MainMenuForm);
USEFORM("LogUnit.cpp", LogForm);
USEFORM("BScanMeasureFormUnit.cpp", BScanMeasureForm);
USEFORM("BScanFrameUnit.cpp", BScanFrame); /* TFrame: File Type */
USEFORM("BScanAnalyzeDebugUnit.cpp", BScanAnalyzeDebugForm);
USEFORM("ConfigUnit.cpp", ConfigForm);
USEFORM("ConsoleDlgUnit.cpp", ConsoleDlg);
USEFORM("CalibrationFormUnit.cpp", CalibrationForm);
USEFORM("BScanWriteProgressUnit.cpp", BScanWriteProgressForm);
USEFORM("BasicFrameUnit.cpp", BasicFrame); /* TFrame: File Type */
USEFORM("ArchiveSelectDialUnit.cpp", ArchiveSelectDial);
USEFORM("ArchiveSettingsDialUnit.cpp", ArchiveSettingsDial);
USEFORM("ArchiveUnit.cpp", ArchiveForm);
USEFORM("AdjustingFormUnit.cpp", AdjustingForm);
USEFORM("ArchiveFilterFormUnit.cpp", ArchiveFilterForm);
USEFORM("AScanFrameUnit.cpp", AScanFrame2); /* TFrame: File Type */
USEFORM("AScanViewUnit.cpp", AScanViewForm);
USEFORM("DebugStateUnit.cpp", DebugStateForm);
USEFORM("controllers\AControllerUnit.cpp", AControllerForm);
USEFORM("BScanMeasureViewResUnit.cpp", BScanMeasureViewRes);
//---------------------------------------------------------------------------
#ifdef DEBUG
USEFORM("LogUnit.cpp", LogForm);
#endif
USEFORM("MainUnit.cpp", MainForm);
USEFORM("ScreenMessageUnit.cpp", ScreenMessageForm);
USEFORM("MessageUnit.cpp", MessageForm);
USEFORM("AControllerUnit.cpp", AControllerForm);
USEFORM("ConfigUnit.cpp", ConfigForm);
#ifdef DEBUG
USEFORM("DebugStateUnit.cpp", DebugStateForm);
#endif
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
	try
	{
		 Application->Initialize();
		 Application->MainFormOnTaskBar = true;
		 Application->CreateForm(__classid(TMainMenuForm), &MainMenuForm);
		Application->CreateForm(__classid(TLogForm), &LogForm);
		Application->CreateForm(__classid(TConfigForm), &ConfigForm);
		Application->CreateForm(__classid(TDebugStateForm), &DebugStateForm);
		Application->CreateForm(__classid(TFotoForm), &FotoForm);
		Application->CreateForm(__classid(TAControllerForm), &AControllerForm);
		Application->CreateForm(__classid(TScreenMessageForm2), &ScreenMessageForm2);
		Application->CreateForm(__classid(TAScanViewForm), &AScanViewForm);
		Application->CreateForm(__classid(TSelectChannelDial), &SelectChannelDial);
		Application->CreateForm(__classid(TConsoleDlg), &ConsoleDlg);
		Application->CreateForm(__classid(TArchiveSettingsDial), &ArchiveSettingsDial);
		Application->CreateForm(__classid(TArchiveForm), &ArchiveForm);
		Application->CreateForm(__classid(TArchiveSelectDial), &ArchiveSelectDial);
		Application->CreateForm(__classid(TAdjustingForm), &AdjustingForm);
		Application->CreateForm(__classid(TScanScriptSimForm), &ScanScriptSimForm);
		Application->CreateForm(__classid(TTuneTableForm), &TuneTableForm);
		Application->CreateForm(__classid(TReportViewerForm), &ReportViewerForm);
		Application->CreateForm(__classid(TArchiveFilterForm), &ArchiveFilterForm);
		Application->CreateForm(__classid(TBScanWriteProgressForm), &BScanWriteProgressForm);
		Application->CreateForm(__classid(TCalibrationForm), &CalibrationForm);
		Application->CreateForm(__classid(TMainUnit), &MainUnit);
		Application->CreateForm(__classid(TAScanViewForm), &AScanViewForm);
		Application->CreateForm(__classid(TBScanMeasureForm), &BScanMeasureForm);
		Application->CreateForm(__classid(TMeasureSelChannelForm), &MeasureSelChannelForm);
		Application->CreateForm(__classid(THeaderTestForm), &HeaderTestForm);
		Application->CreateForm(__classid(TBScanAnalyzeDebugForm), &BScanAnalyzeDebugForm);
		Application->CreateForm(__classid(TFiltrParamForm), &FiltrParamForm);
		Application->CreateForm(__classid(TBScanMeasureViewRes), &BScanMeasureViewRes);
		Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    catch (...)
    {
         try
         {
             throw Exception("");
         }
         catch (Exception &exception)
         {
             Application->ShowException(&exception);
         }
    }
    return 0;
}
//---------------------------------------------------------------------------
