//---------------------------------------------------------------------------

#ifndef IPCameraUnitH
#define IPCameraUnitH

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <TChar.h>
#include <jpeg.hpp>
#include <ExtCtrls.hpp>
#include <IdComponent.hpp>
#include <IdHTTP.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>


bool GetPhoto(UnicodeString IP, int *PJpegAddr, int *PJpegSize);
void FreePhoto();

//---------------------------------------------------------------------------
#endif
