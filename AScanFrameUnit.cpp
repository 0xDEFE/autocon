//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BScanWriteProgressUnit.h"
#include "AScanFrameUnit.h"
#include "LogUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BasicFrameUnit"
#pragma resource "*.dfm"
TAScanFrame *AScanFrame;

int ThValList[16] = { 8, 10, 12, 16, 20, 25, 32, 40, 50, 63, 80, 101, 127, 160, 201, 254};

UnicodeString UMUSideToStr(UMUSide side)
{
	if (side == usLeft) return "�����"; else return "������";
}

UnicodeString UMULineToStr(int Line)
{
    if (Line == 0) return "��1"; else return "��2";
}

//---------------------------------------------------------------------------
__fastcall TAScanFrame::TAScanFrame(TComponent* Owner)
    : TBasicFrame(Owner)
{
    aquiredSignalsFlags = MU_SIGNAL_UMU_ASCAN_MEAS | MU_SIGNAL_UMU_ASCAN_DATA |  MU_SIGNAL_UMU_TVG_DATA |
                            MU_SIGNAL_UMU_ALARM_DATA | MU_SIGNAL_UMU_BSCAN2_DATA | MU_SIGNAL_FORM_SHORTCUT;
}
//---------------------------------------------------------------------------
void TAScanFrame::OnMFrameAttach(TBasicFrame* pMain)
{
    SaveParamH = 0;
    SaveParamKd = 0;
    SaveMax1 = SaveMax2 = -1;
    RullerMode = OldRullerMode = 0;
    DebugOut = false;
    NoiseRed = false;
    BScanView = false;
    EditMode = false;
    EnterModeState = false;

    SaveMax1 = - 1;
    SaveMax2 = - 1;

    bFirstTime = true;

    bsl->SetViewDelayZone(0, AutoconMain->DEV->GetAScanLen());


    #ifdef UMUDataDebug
        BScanDebugPanel->Visible = true;
        BScanChart2->Visible = true;
        AmplDebugPanel->Visible = true;
        ControlPanel->Visible = true;
        BScanDataPanel->Height = BScanDataPanel->Height + 250;
        BScanDebugSeries->Visible = true;
    #endif
    BScanPanel->Top = 0;
    HandChannelPanel->Top = this->Height + 100;

    #ifdef DEBUG
        DebugOut = true;
        AScanDebugPanel->Visible = true;
    #endif

    LastSelectChannels = - 1;

    ScrollBox1->VertScrollBar->Position = 0;
    NumPadPanel->Visible = !NumPadPanel->Visible;
    ScrollBox1->Refresh();
	ScrollBox1->Update();
    NumPadPanel->Visible = !NumPadPanel->Visible;

    EditPanel = NULL;
	PeakSeries->Visible = false;

	Series1->Visible = false;
	Series2->Visible = false;

    bMarkBtnState = AutoconMain->DEV->GetMarkState();
    updateMarkState();
}
void TAScanFrame::OnMFrameDetach(TBasicFrame* pMain)
{
    MarkMovementTimer->Enabled = false;
}
void TAScanFrame::OnMFrameCreate(TBasicFrame* pMain)
{
    if(frameFlags & MF_ASCAN_FLAG_SINGLEUSE)
        AutoconMain->Rep->ClearData(); // ������� ������
    if(frameFlags & MF_ASCAN_FLAG_RESTORE_CAL_TUNE_GATES)
        AutoconMain->Calibration->SetGateMode(gmSearch);


    bsl = new TBScanLine();
}
void TAScanFrame::OnMFrameDestroy(TBasicFrame* pMain)
{
    BScanDrawTimer->Enabled = false;
    if(bsl)
    {
        delete bsl;
        bsl = NULL;
    }

    if(EditPanel)
    {
        delete EditPanel;
        EditPanel = NULL;
    }
}
void TAScanFrame::OnUMUSignal(eMainUnitSignalType sigtype, sPtrListItem& sig)
{
    PtDEV_AScanMeasure DEV_AScanMeasure_ptr;
	PtDEV_AScanHead    DEV_AScanHead_ptr;
    PtDEV_BScan2Head   DEV_BScan2Head_ptr = NULL;
	PtDEV_AlarmHead    DEV_AlarmHead_ptr;
	PtUMU_AScanData    AScanData_ptr;
	PtUMU_AScanData    TVGData_ptr;
	PtUMU_AScanMeasure AScanMeasure_ptr;
	PtUMU_BScanData    PBScanData_ptr;

    switch(sig.Type)
    {
        case edAScanData:
        {
            AScanSeries->BeginUpdate();
            GateSeriesMain->BeginUpdate();
            GateSeriesLeft->BeginUpdate();
            GateSeriesRight->BeginUpdate();
            BScanGateMainSeries->BeginUpdate();
            BScanGateLeftSeries->BeginUpdate();
            BScanGateRightSeries->BeginUpdate();

            DEV_AScanHead_ptr = sig.Ptr1.pAScanHead;
            AScanData_ptr = sig.Ptr2.pAScanData;

          //  if ((DEV_AScanHead_ptr->Side == AutoconMain->DEV->GetSide()) &&
           //     (DEV_AScanHead_ptr->Channel == AutoconMain->DEV->GetChannel()))
          //  {

                if (OldRullerMode != RullerMode)
                {
                    switch(RullerMode)
                    {
                        case 0:
                        case 1: {
                                    AScanChart->BottomAxis->SetMinMax(0, DelayToChartValue(AutoconMain->DEV->GetAScanLen()));
                                    BScanChart2->BottomAxis->SetMinMax(0, DelayToChartValue(AutoconMain->DEV->GetAScanLen()));
                                    break;
                                }
                        case 2: {
                                    AScanChart->BottomAxis->SetMinMax(0, 232);
                                    BScanChart2->BottomAxis->SetMinMax(0, 232);
                                    break;
                                }
                    }
                    OldRullerMode = RullerMode;
                }

                {
                    if ((SaveMax1 != AScanChart->BottomAxis->Maximum) || (AScanSeries->XValues->Count == 0))
                    {
                        SaveMax1 = AScanChart->BottomAxis->Maximum;
                        AScanSeries->Clear();
                        for (int i = 0; i < 232; i++)
                            AScanSeries->AddXY(PixelToChartValue(i), Filter(AScanData_ptr->Data[i]));
                    }
                    else
                        for (int i = 0; i < std::min(232, AScanSeries->XValues->Count); i++)
                            AScanSeries->YValue[i] = Filter(AScanData_ptr->Data[i]);
                }

                GateSeriesMain->X0 = AutoconMain->DEV->DelayToDepth(AutoconMain->DEV->GetStGate(AutoconMain->DEV->GetGateIndex()));
                GateSeriesMain->X1 = AutoconMain->DEV->DelayToDepth(AutoconMain->DEV->GetEdGate(AutoconMain->DEV->GetGateIndex()));

                int GateLevet = AutoconMain->Config->EvaluationGateLevel;

                GateSeriesMain->Y0 = GateLevet - 1;
                GateSeriesMain->Y1 = GateLevet + 1;

                if(AutoconMain->DEV->GetMarkState())
                {
                    int markVal = AutoconMain->DEV->GetMark();
                    int markHalfWidth = AutoconMain->DEV->GetMarkWidth() / 2;
                    MarkSeries->X0 = DelayToChartValue(markVal) - DelayToChartValue(markHalfWidth);
                    MarkSeries->X1 = DelayToChartValue(markVal) + DelayToChartValue(markHalfWidth);
                    MarkSeries->Y0 = GateLevet - 2;
                    MarkSeries->Y1 = GateLevet + 2;
                    MarkSeries->Visible = true;
                }
                else
                {
                    MarkSeries->Visible = false;
                }

                GateSeriesLeft->X0 = AutoconMain->DEV->DelayToDepth(AutoconMain->DEV->GetStGate(AutoconMain->DEV->GetGateIndex()))
                    - DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 128;
                GateSeriesLeft->X1 = AutoconMain->DEV->DelayToDepth(AutoconMain->DEV->GetStGate(AutoconMain->DEV->GetGateIndex()));
                GateSeriesLeft->Y0 = GateLevet - 5;
                GateSeriesLeft->Y1 = GateLevet + 5;

                GateSeriesRight->X0 = AutoconMain->DEV->DelayToDepth(AutoconMain->DEV->GetEdGate(AutoconMain->DEV->GetGateIndex()));
                GateSeriesRight->X1 = AutoconMain->DEV->DelayToDepth(AutoconMain->DEV->GetEdGate(AutoconMain->DEV->GetGateIndex())) + DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 128;
                GateSeriesRight->Y0 = GateLevet - 5;
                GateSeriesRight->Y1 = GateLevet + 5;

                // ����� �-���������
                sChannelDescription ChanDesc;
                AutoconMain->Table->ItemByCID(DEV_AScanHead_ptr->Channel, &ChanDesc);
                BScanGateMainSeries->X0 = DelayToChartValue(ChanDesc.cdBScanGate.gStart);
                BScanGateMainSeries->X1 = DelayToChartValue(ChanDesc.cdBScanGate.gEnd);

                GateLevet = AutoconMain->Config->BScanGateLevel;

                BScanGateMainSeries->Y0 = GateLevet - 1;
                BScanGateMainSeries->Y1 = GateLevet + 1;

                BScanGateLeftSeries->X0 = DelayToChartValue(ChanDesc.cdBScanGate.gStart) - DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 128;
                BScanGateLeftSeries->X1 = DelayToChartValue(ChanDesc.cdBScanGate.gStart);
                BScanGateLeftSeries->Y0 = GateLevet - 5;
                BScanGateLeftSeries->Y1 = GateLevet + 5;

                BScanGateRightSeries->X0 = DelayToChartValue(ChanDesc.cdBScanGate.gEnd);
                BScanGateRightSeries->X1 = DelayToChartValue(ChanDesc.cdBScanGate.gEnd) + DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 128;
                BScanGateRightSeries->Y0 = GateLevet - 5;
                BScanGateRightSeries->Y1 = GateLevet + 5;
           //  }

            AScanSeries->EndUpdate();
            GateSeriesMain->EndUpdate();
            GateSeriesLeft->EndUpdate();
            GateSeriesRight->EndUpdate();
            BScanGateMainSeries->EndUpdate();
            BScanGateLeftSeries->EndUpdate();
            BScanGateRightSeries->EndUpdate();

            break;
        }
//edTVGData---------------------------------------------------------------------
        case edTVGData:
        {
            TVGSeries->BeginUpdate();

            DEV_AScanHead_ptr = sig.Ptr1.pAScanHead;
            TVGData_ptr = sig.Ptr2.pAScanData;

            //if ((DEV_AScanHead_ptr->Side == AutoconMain->DEV->GetSide()) &&
            //    (DEV_AScanHead_ptr->Channel == AutoconMain->DEV->GetChannel()))
            //{
                if ((SaveMax2 != AScanChart->BottomAxis->Maximum) ||
                    (TVGSeries->XValues->Count == 0))
                {
                    SaveMax2 = AScanChart->BottomAxis->Maximum;
                    TVGSeries->Clear();
                    for (int i = 0; i < 232; i++)
                        TVGSeries->AddXY(PixelToChartValue(i),
                                              255 * 0.2 + (255 * 0.75) * (TVGData_ptr->Data[i] - AutoconMain->Config->GainBase) / (AutoconMain->Config->GainMax - AutoconMain->Config->GainBase) );
                }
                else
                {
                    for (int i = 0; i < std::min(232, TVGSeries->XValues->Count); i++)
                        TVGSeries->YValue[i] = 255 * 0.2 + (255 * 0.75) * (TVGData_ptr->Data[i] - AutoconMain->Config->GainBase) / (AutoconMain->Config->GainMax - AutoconMain->Config->GainBase);
                }
            //}

            TVGSeries->EndUpdate();

            break;
        }
//edAScanMeas-------------------------------------------------------------------
        case edAScanMeas:
        {
            MaxPosSeries->BeginUpdate();

            DEV_AScanMeasure_ptr = sig.Ptr1.pAScanMeasure;

            MaxPosSeries->Visible = false;
            //if ((DEV_AScanMeasure_ptr->Side == AutoconMain->DEV->GetSide()) &&
            //    (DEV_AScanMeasure_ptr->Channel == AutoconMain->DEV->GetChannel()))
            //{
                if (DEV_AScanMeasure_ptr->ParamA >= AutoconMain->Config->EvaluationGateLevel)
                {
                    MaxPosSeries->Visible = true;
                    MaxPosSeries->X0 = DelayToChartValue(DEV_AScanMeasure_ptr->ParamM) - DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 25.6 / 2;
                    MaxPosSeries->X1 = DelayToChartValue(DEV_AScanMeasure_ptr->ParamM) + DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 25.6 / 2;
                    MaxPosSeries->Y0 = -5;
                    MaxPosSeries->Y1 = 6;
                }

				// ������� ------------------------------------------------------------------------------

				if ((AutoconMain->DEV->GetCalibrationMode() == cmPrismDelay) && (AutoconMain->DEV->GetMode() == dmCalibration))
				{
					HPanel->Caption = Format("R %d", ARRAYOFCONST((DEV_AScanMeasure_ptr->ParamR)));
					RPanel->Caption = "";
				}
				else
				{
					HPanel->Caption = Format("H %d", ARRAYOFCONST((DEV_AScanMeasure_ptr->ParamH)));
					RPanel->Caption = Format("R %d", ARRAYOFCONST((DEV_AScanMeasure_ptr->ParamR)));
				}

                LPanel->Caption = Format("L %d", ARRAYOFCONST((DEV_AScanMeasure_ptr->ParamL)));
                NPanel->Caption = Format("N %d", ARRAYOFCONST((DEV_AScanMeasure_ptr->ParamN)));



                SaveParamH = DEV_AScanMeasure_ptr->ParamH;
                SaveParamKd = DEV_AScanMeasure_ptr->ParamN - AutoconMain->DEV->GetSens();

                ChannelTitlePanel->Caption = AutoconMain->DEV->WorkChannelDescription.Title;
                if (!DebugOut)
                    DebugPanel->Caption = Format("UMU: �-%d(1,2,3), Side-%d:%s(0,1), Line-%d:%s(0,1), Stroke-%d, Gen-%d, Res-%d, ����.�����-%d ���, �������. %d ���; Device: CID-0x%X, GateIdx-%d(1,2), ������-%d",
                                        ARRAYOFCONST((AutoconMain->DEV->CurChannelUMUIndex + 1,
                                                                  AutoconMain->DEV->CurChannelUMUSide,
                                                                  UMUSideToStr(AutoconMain->DEV->CurChannelUMUSide),
                                                                           AutoconMain->DEV->CurChannelUMULine,
                                                                           UMULineToStr(AutoconMain->DEV->CurChannelUMULine),
                                                                                    AutoconMain->DEV->CurChannelUMUStroke,
                                                                                               AutoconMain->DEV->CurChannelUMUGen,
                                                                                                       AutoconMain->DEV->CurChannelUMURes,
                                                                                                               AutoconMain->DEV->CurChannelUMUStrokeDuration,
                                                                                                                                            AutoconMain->DEV->CurChannelUMUScanDuration,
                                                                                                                                                            AutoconMain->DEV->GetChannel(), AutoconMain->DEV->GetGateIndex(),
                                                                                                                                                                    AutoconMain->DEV->GetChannelGroup())));

                if (AutoconMain->DEV->GetMode() != dmCalibration)
                    InfoPanel->Caption = Format ("���: %d", ARRAYOFCONST((AutoconMain->DEV->GetGain())));
                    else InfoPanel->Caption = Format ("�� %d (%d)", ARRAYOFCONST((AutoconMain->DEV->GetSens(), AutoconMain->DEV->GetRecommendedSens() )));
            //}

            MaxPosSeries->EndUpdate();

            break;
        }
//edAlarmData-------------------------------------------------------------------
        case edAlarmData:
        {
            DEV_AlarmHead_ptr = sig.Ptr1.pAlarmHead;

            for (unsigned int Idx = 0; Idx < DEV_AlarmHead_ptr->Items.size(); Idx++)
                if ((DEV_AlarmHead_ptr->Items[Idx].Side == AutoconMain->DEV->GetSide()) &&
                    (DEV_AlarmHead_ptr->Items[Idx].Channel == AutoconMain->DEV->GetChannel()))
                {
                        if (DEV_AlarmHead_ptr->Items[Idx].State[AutoconMain->DEV->GetGateIndex()])
                        {
                            GateSeriesMain->Color = clRed;
                            GateSeriesLeft->Color = clRed;
                            GateSeriesRight->Color = clRed;
                        }
                        else
                        {
                            GateSeriesMain->Color = clBlue;
                            GateSeriesLeft->Color = clBlue;
                            GateSeriesRight->Color = clBlue;
                        }
                }
            break;
        }
//edBScan2Data in reverce order-------------------------------------------------
        case edBScan2Data:
        {
            int SaveMaxAmpl;
            int SaveMaxDelay;

            DEV_BScan2Head_ptr = sig.Ptr1.pBScan2Head;
            PBScanData_ptr = sig.Ptr2.pBScanData;

            //DebugPanel->Caption = Format("��: %d", ARRAYOFCONST((DEV_BScan2Head_ptr->XSysCrd[0])));

            //Hand BScan
            if (DEV_BScan2Head_ptr) {
                    bsl->SetViewCrdZone(DEV_BScan2Head_ptr->XSysCrd[0] - 500, DEV_BScan2Head_ptr->XSysCrd[0]);
                    bsl->SetViewDelayZone(0, AutoconMain->DEV->GetAScanLen());
            }

            if(bFirstTime)
            {
                bsl->Clear(); //Clear b-scan
                bsl->Draw(0,0,BScanPB->Canvas);
                bFirstTime = false;
            }

            if(frameFlags & MF_ASCAN_FLAG_BSCAN_PANEL)
            {
                BScanDrawTimer->Enabled = true;
            }


            for (unsigned int ItemIdx = 0; ItemIdx < DEV_BScan2Head_ptr->Items.size(); ItemIdx++)
                if ((DEV_BScan2Head_ptr->Items[ItemIdx].Side == AutoconMain->DEV->GetSide()) &&
                    (DEV_BScan2Head_ptr->Items[ItemIdx].Channel == AutoconMain->DEV->GetChannel()))
                {
                    // ����� �-���������
                    if (CheckBox6->Checked)

                    for (int SignalIdx = 0; SignalIdx < DEV_BScan2Head_ptr->Items[ItemIdx].Count; SignalIdx++)
                    {

                        // ����� ������ �� ������������ ���������
                        int Ampl;
                        int MaxDelay;
                        int MaxAmpl = - 1;
                        for (int AmplIdx = 6; AmplIdx < 10; AmplIdx++)
                        {
                            #ifdef BScanSize_12Byte
                            if (AmplIdx&1) Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                           Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                            #endif
                            #ifndef BScanSize_12Byte
                            Ampl = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx];
                            #endif
                            if (MaxAmpl < Ampl) {
                                MaxAmpl = Ampl;
                                MaxDelay = AmplIdx;
                            }
                            //MaxAmpl = std::max(MaxAmpl, Ampl);
                        }

                        // ����� ����� �������
                        if (MaxDelay != - 1) {

                            int StDelay = - 1;
                            int EndDelay = - 1;
                            for (int AmplIdx = MaxDelay; AmplIdx > 0; AmplIdx--)
                            {
                                #ifdef BScanSize_12Byte
                                if (AmplIdx&1) Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                               Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                                #endif
                                #ifndef BScanSize_12Byte
                                Ampl = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx];
                                #endif
                                if (Ampl < 32) break;
                                StDelay = AmplIdx;
                            }

                            // ����� ������ �������
                            for (int AmplIdx = MaxDelay; AmplIdx < 24; AmplIdx++)
                            {
                                #ifdef BScanSize_12Byte
                                if (AmplIdx&1) Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                               Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                                #endif
                                #ifndef BScanSize_12Byte
                                Ampl = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx];
                                #endif
                                if (Ampl < 32) break;
                                EndDelay = AmplIdx;
                            }
                             /*
                            int Ampl;
                            int MaxAmpl = - 1;
                            for (int AmplIdx = 0; AmplIdx < 24; AmplIdx++)
                            {
                                #ifdef BScanSize_12Byte
                                if (AmplIdx&1) Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                               Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                                #endif
                                #ifndef BScanSize_12Byte
                                Ampl = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx];
                                #endif
                                MaxAmpl = std::max(MaxAmpl, Ampl);
                            }
                               */


                            if (SelEchoIdx->ItemIndex == SignalIdx)
                            {
                                SaveMaxAmpl = MaxAmpl;
                                SaveMaxDelay = MaxDelay;
                            }

                            if ((StDelay != - 1) && (EndDelay != - 1)) {
                                //int Wth = 2;
                                int Dly = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Delay;
                                int BScanCrd = DEV_BScan2Head_ptr->XSysCrd[0];
                               // SetPoint(BScanCrd, Dly - Wth, Dly + Wth, MaxAmpl); // ����� �� �-���������
                                bsl->SetPoint(BScanCrd, Dly - (MaxDelay - StDelay) * 0.333, Dly + (EndDelay - MaxDelay) * 0.333, MaxAmpl);                                }
                        }
                    }


#ifdef UMUDataDebug
                    Label1->Caption = "A:" + IntToStr(SaveMaxAmpl) + " I:" + IntToStr(SaveMaxDelay);
                    // ����� ������ ���������� ������� �-���������
                    BScanSeries->Clear();
                    float Ampl;
                    for (int SignalIdx = 0; SignalIdx < DEV_BScan2Head_ptr->Items[ItemIdx].Count; SignalIdx++)
                        if (SelEchoIdx->ItemIndex == SignalIdx)
                        {
                            for (int AmplIdx = 0; AmplIdx < 24; AmplIdx++)
                            {
                                #ifdef BScanSize_12Byte
                                if (AmplIdx&1) Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                               Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                                #endif
                                #ifndef BScanSize_12Byte
                                float y = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx];
                                #endif
                                BScanSeries->AddXY(AmplIdx, Ampl);
                            }
                        }

                    // ����� �-��������� - ��� �-���������
                    BScanDebuugSeries->Clear();
                    BSSeries1->Clear();
                    BSSeries2->Clear();
                    BSSeries3->Clear();
                    BSSeries4->Clear();
                    BSSeries5->Clear();
                    BSSeries6->Clear();
                    BSSeries7->Clear();
                    BSSeries8->Clear();
                    float y;
                    for (int SignalIdx = 0; SignalIdx < DEV_BScan2Head_ptr->Items[ItemIdx].Count; SignalIdx++)
                    {
                        for (int AmplIdx = 0; AmplIdx < 24; AmplIdx++)
                        {
                            #ifdef BScanSize_12Byte
                            if (AmplIdx&1) y = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                           y = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                            #endif

                            #ifndef BScanSize_12Byte
                            float y = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx];
                            #endif

                            float x = DelayToChartValue((float)((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Delay) + 0.333 * (float)AmplIdx - (float)2);

                            switch (SignalIdx)
                            {
                                case 0: { BSSeries1->AddXY(x, y); break; }
                                case 1: { BSSeries2->AddXY(x, y); break; }
                                case 2: { BSSeries3->AddXY(x, y); break; }
                                case 3: { BSSeries4->AddXY(x, y); break; }
                                case 4: { BSSeries5->AddXY(x, y); break; }
                                case 5: { BSSeries6->AddXY(x, y); break; }
                                case 6: { BSSeries7->AddXY(x, y); break; }
                                case 7: { BSSeries8->AddXY(x, y); break; }
                            }
                        }
                        #ifdef BScanSize_12Byte
                        int AmplIdx = 9;
                        if (AmplIdx&1) y = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                       y = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                        BScanDebugSeries->AddXY(DelayToChartValue((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Delay), y);
                        #endif

                        #ifndef BScanSize_12Byte
                        BScanDebugSeries->AddXY(DelayToChartValue((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Delay),
                                                     (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[8]);
                        #endif
                    }
#endif
                };


            break;
        }
    }

    if(sigtype == MU_SIGNAL_UMU_BSCAN2_DATA)
        BScanDrawTimer->Enabled = true;
}

float TAScanFrame::DelayToChartValue(float Src)
{
    switch(RullerMode) {
        case 0: {
                    return Src;
                }
        case 1: {
                    return AutoconMain->DEV->DelayToDepthByHeight(Src);
            }
        case 2: {
                    return AutoconMain->DEV->DelayToPixel(Src);
			    }
	}
	return 0;
}

float TAScanFrame::ChartValueToDelay(float Src)
{
    switch(RullerMode) {
		case 0: {
                return Src;
                }
        case 1: {
                return AutoconMain->DEV-> DepthToDelay( Src);
            }
        case 2: {
                return /*AutoconMain->DEV->DelayScale*/ 1 * Src / 10; // !q!
                }
	}
	return 0;
}


float TAScanFrame::PixelToChartValue(int Pixel)
{
    switch(RullerMode) {
    case 0: {
            return AutoconMain->DEV->PixelToDelayFloat(Pixel);
        }
    case 1: {
            return AutoconMain->DEV->DelayToDepth(AutoconMain->DEV->PixelToDelayFloat(Pixel));
        }
    case 2: {
            return Pixel;
        }
    }
    return 0;
//System::TDateTime
}

unsigned char TAScanFrame::Filter(unsigned char Value)
{
    if (NoiseRed) {
        if (Value < 16) {
            return 0;
        }
        else if (Value < 32) {
            return(unsigned char)(32 * (Value - 15) / 16);
        }
        else {
            return Value;
        }
    }
    else
        return Value;
}
void __fastcall TAScanFrame::AScanChartClick(TObject *Sender)
{
    if(SpeedButton2->Enabled)
        SpeedButton2->Click();
}
//---------------------------------------------------------------------------

void __fastcall TAScanFrame::SpeedButton2Click(TObject *Sender)
{
    LastSelectChannels = - 1;

    if (frameFlags & MF_ASCAN_FLAG_SINGLEUSE)
    {
        pMainUnitForm->BackButton();
    }

    HandChannelPanel->Visible = false;
    pParentFrame->popFrame();
}
//---------------------------------------------------------------------------
void TAScanFrame::SetFrameFlags(DWORD flags)
{
    frameFlags = flags;

    AScanKuPanel->Visible = frameFlags & MF_ASCAN_FLAG_KU_PANEL;
    AScanAttPanel->Visible = frameFlags & MF_ASCAN_FLAG_ATT_PANEL;
    TuningPanel->Visible = frameFlags & MF_ASCAN_FLAG_TUNE_BTN;
    AScanPhotoPanel->Visible = frameFlags & MF_ASCAN_FLAG_PHOTO_BTN;
    WriteBScanPanel->Visible = frameFlags & MF_ASCAN_FLAG_BSCAN_BTN;
    BScanPanel->Visible = frameFlags & MF_ASCAN_FLAG_BSCAN_PANEL;

    SurfaceLabel->Visible = frameFlags & MF_ASCAN_FLAG_SURFACE_COMBO;
    cbSurface->Visible = frameFlags & MF_ASCAN_FLAG_SURFACE_COMBO;
    HandChannelPanel->Visible = frameFlags & MF_ASCAN_FLAG_HSCAN_BTNS_VISIBLE;

    BScanDataPanel->Visible = BScanDebugPanel->Visible || BScanPanel->Visible || ControlPanel->Visible;

    if(frameFlags & MF_ASCAN_FLAG_HSCAN_BTNS_VISIBLE)
    {
        Button_50echo->Down = true;
        Button_50echo->Click();
        ValueToControl();
    }
}

void __fastcall TAScanFrame::sbSideLineClick(TObject *Sender)
{
    TComponent *AComponent = dynamic_cast<TComponent*>(Sender);
    switch(AComponent->Tag) {
    case 1:
        { // ������� / ����
            break;
        }
    case 13:
        { // ���� ����� [����]
            break;
        }
    case 14: { // ���������

			if (AutoconMain->DEV->ChannelSensCalibration())
            {

                ValuePanel00->Caption = "��������";
                ACConfig->Last_SS3R_TestDateTime = Now();
            }  else ValuePanel00->Caption = "������";

			break;
        }
    case 18: { // ��������������
                NoiseRed = !NoiseRed;
                break;
             }
    case 19: { // ����� �������
                RullerMode++;
                if (RullerMode == 3) // 3
                    RullerMode = 0;
                AutoconMain->DEV->RequestTVGCurve();
                break;
             }
    case 38: { // ����������� ���� B-���������
                BScanView = !BScanView;
                // BScanChart->Visible = BScanView;
                BScanDataPanel->Visible = BScanView;
                BScanPanel->Top = 0;
                HandChannelPanel->Top = this->Height + 100;
                break;
             }
    case 39: { // ������ ������ �-���������
                TBitmap* pChartBitmap = AScanChart->TeeCreateBitmap();

                TBitmap* pImageBitmap = new TBitmap(); //Bitmap to draw chart and text
                pImageBitmap->SetSize(pChartBitmap->Width*2,pChartBitmap->Height);
                TRect clientRect(0,0,pChartBitmap->Width*2,pChartBitmap->Height);
                TRect drawRect(pChartBitmap->Width+40,0,pChartBitmap->Width*2,pChartBitmap->Height);

                CID curChannel = AutoconMain->DEV->GetChannel();
                sChannelDescription chanDesc;
                AutoconMain->Table->ItemByCID(curChannel, &chanDesc);

                UnicodeString methodStr;
                switch(chanDesc.cdMethod[0])
                {
                    case imEcho: methodStr="���"; break;
                    case imMirrorShadow: methodStr="��������� �������"; break;
                    case imMirror: methodStr="����������"; break;
                    case imShadow: methodStr="�������"; break;
                    default: methodStr="�� ��������";
                }

                int timeInChannelSec = AutoconMain->Rep->GetTimeInHandScanChannel(curChannel);
                UnicodeString timeInChannelStr = (timeInChannelSec < 60) ? L"������ ������" :
                                             StringFormatU(L"%d ���",int((float(timeInChannelSec)+0.5)/60.f)).c_str();

                UnicodeString imageText =
                        StringFormatU(L"���� �����: %d�\r\n�����: %s\r\n�����������: � %d\r\n\
�� = %d ��\r\n� = %d ���\r\n\
��� = %d ���\r\n2�� = %.1f ���\r\n�� = %d ��\r\n����� ��������: %s",
                chanDesc.cdEnterAngle,methodStr.c_str(),cbSurface->ItemIndex,   //���� �����, �����, �����������
                SaveParamKd, SaveParamH,      //��, �
                AutoconMain->DEV->GetTVG(),float(AutoconMain->DEV->GetPrismDelay())/10.f,//���, 2��,
                AutoconMain->DEV->GetSens(),timeInChannelStr.c_str());   //��, ����� ��������

                TStringList* pStrList = new TStringList();
                pStrList->Text = imageText;

                //Adjusting text size to fill left frame
                TSize textSize;
                pImageBitmap->Canvas->Font->Size = -12; //Initial size
                while((textSize.Width < drawRect.Width()) && (textSize.Height < drawRect.Height()))
                {
                    textSize.Width = 0;
                    textSize.Height = 0;
                    pImageBitmap->Canvas->Font->Size--;
                    for(int k = 0; k < pStrList->Count; k++)
                    {
                        textSize.Width = std::max((int)textSize.Width,pImageBitmap->Canvas->TextWidth(pStrList->Strings[k]));
                        textSize.Height += pImageBitmap->Canvas->TextHeight( pStrList->Strings[k] );
                    }
                }
                pImageBitmap->Canvas->Font->Size++;

                Vcl::Graphics::TTextFormat tf;
                tf.Clear();
                tf << tfLeft << tfTop;

                //Draw chart, frame and text
                pImageBitmap->Canvas->Brush->Color = clWhite;
                pImageBitmap->Canvas->FillRect(clientRect);
                pImageBitmap->Canvas->Draw(0,0,pChartBitmap);
                pImageBitmap->Canvas->Brush->Color = clBlack;
                pImageBitmap->Canvas->FrameRect(TRect(0,0,pChartBitmap->Width,pChartBitmap->Height));
                pImageBitmap->Canvas->Brush->Color = clWhite;
                pImageBitmap->Canvas->TextRect(drawRect, imageText, tf);

                //Convert to JPEG and cleanup
                TJPEGImage * ss = new TJPEGImage;
                ss->Assign(pImageBitmap);
                delete pChartBitmap;
                delete pImageBitmap;
                delete pStrList;

                AutoconMain->Rep->ScreenShotHandScanList.push_back(ss);
                AScanPhotoLabelPanel->Caption = StringFormatU(L" ������ ������ (%d)",AutoconMain->Rep->ScreenShotHandScanList.size());
                break;
             }
    case 40: { //������ �-���������
                BeginHandBScanWrite();
                break;
             }
    }
    ValueToControl();
}
//---------------------------------------------------------------------------

void __fastcall TAScanFrame::OnSpinDownBtnClick(TObject *Sender)
{

    TComponent *AComponent = dynamic_cast<TComponent*>(Sender);
    switch(AComponent->Tag)
    {
    case 6: { // [+]  ��������� ��
                int val = AutoconMain->Config->GetPulseAmplByCID(AutoconMain->DEV->GetChannel());
                val--;
                if (val < 0) { val = 0; }
                AutoconMain->Config->SetPulseAmplByCID(AutoconMain->DEV->GetChannel(), val);
                AutoconMain->DEV->Update(true);
                break;
            }
    case 7: { // [+]  ��
			AutoconMain->DEV->SetSens(AutoconMain->DEV->GetSens() - 1);
            break;
            }
	case 8: { // [+]  ���
			if (AutoconMain->DEV->GetTVG() - 1 >= 0) {
				AutoconMain->DEV->SetTVG(AutoconMain->DEV->GetTVG() - 1);
            }
            break;
        }
    case 9: { // [+]  ����� � ������
			if (AutoconMain->DEV->GetPrismDelay() - 1 > 0) {
				AutoconMain->DEV->SetPrismDelay(AutoconMain->DEV->GetPrismDelay() - 1);
            }
            break;
        }
    case 10: { // [+] ����� ���
         /*   AutoconMain->DEV->EvaluationGateLevel--;
            if (AutoconMain->DEV->EvaluationGateLevel < 0) {
                AutoconMain->DEV->EvaluationGateLevel = 0;
            }
            AutoconMain->DEV->TestInit();                  */
            break;
        }
	case 11: { // [+] ������ ������ ���
			if (AutoconMain->DEV->GetStGate() - 1 >= 0)
				AutoconMain->DEV->SetStGate(AutoconMain->DEV->GetStGate() - 1);
			break;
		}
	case 12: { // [+] ����� ������ ���
			if ((AutoconMain->DEV->GetEdGate() - 1 >= 0) &&
				(AutoconMain->DEV->GetEdGate() - 1 >= AutoconMain->DEV->GetStGate()))
				AutoconMain->DEV->SetEdGate(AutoconMain->DEV->GetEdGate() - 1);
			break;
		}
	case 22: { // [+] ����� ������ ���
                if (AutoconMain->DEV->GetGateCount() != 1)
                {
                    if (AutoconMain->DEV->GetGateIndex() == 1) AutoconMain->DEV->SetGateIndex(2); else AutoconMain->DEV->SetGateIndex(1);
                    AutoconMain->DEV->Update(false);
                }
                break;
             }
    case 77: { // [+]  ���
				AutoconMain->DEV->SetGain(AutoconMain->DEV->GetGain() - 1);
                break;
             }
    }
    ValueToControl();
}
//---------------------------------------------------------------------------

void __fastcall TAScanFrame::OnSpinUpBtnClick(TObject *Sender)
{
    TComponent *AComponent = dynamic_cast<TComponent*>(Sender);
    switch(AComponent->Tag) {
    case 6: { // [+]  ��������� ��
                int val = AutoconMain->Config->GetPulseAmplByCID(AutoconMain->DEV->GetChannel());
                val++;
                if (val > 7) { val = 7; }
                AutoconMain->Config->SetPulseAmplByCID(AutoconMain->DEV->GetChannel(), val);
				AutoconMain->DEV->Update(true);
                break;
            }
    case 7: { // [+]  ��
			AutoconMain->DEV->SetSens(AutoconMain->DEV->GetSens() + 1);
			break;
		}
	case 8: { // [+]  ���
			AutoconMain->DEV->SetTVG(AutoconMain->DEV->GetTVG() + 1);
			break;
		}
	case 9: { // [+]  ����� � ������
			AutoconMain->DEV->SetPrismDelay(AutoconMain->DEV->GetPrismDelay() + 1);
			break;
		}
    case 11: { // [+] ������ ������ ���
			if ((AutoconMain->DEV->GetStGate() + 1 <= AutoconMain->DEV->GetAScanLen()) &&
				(AutoconMain->DEV->GetStGate() + 1 <= AutoconMain->DEV->GetEdGate()))
				AutoconMain->DEV->SetStGate(AutoconMain->DEV->GetStGate() + 1);
            break;
		}
    case 12: { // [+] ����� ������ ���
			if (AutoconMain->DEV->GetEdGate() + 1 <= AutoconMain->DEV->GetAScanLen())
				AutoconMain->DEV->SetEdGate(AutoconMain->DEV->GetEdGate() + 1);
            break;
        }
	case 22: { // [+] ����� ������ ���
                if (AutoconMain->DEV->GetGateCount() != 1)
                {
                    if (AutoconMain->DEV->GetGateIndex() == 1) AutoconMain->DEV->SetGateIndex(2); else AutoconMain->DEV->SetGateIndex(1);
                    AutoconMain->DEV->Update(false);
                }
                break;
             }
	case 77: { // [+]  ���
				AutoconMain->DEV->SetGain(AutoconMain->DEV->GetGain() + 1);
                break;
            }
    }

	ValueToControl();
}
//---------------------------------------------------------------------------


void __fastcall TAScanFrame::NumPadClick(TObject *Sender)
{
    UnicodeString Text;
    int Val;
    float Val2;

    TSpeedButton* Btm = dynamic_cast<TSpeedButton*>(Sender);

    if (Btm)
    {
        switch (Btm->Tag)
        {
            case 0: { EditPanel->Caption = EditPanel->Caption + '0'; break; }
            case 1: { EditPanel->Caption = EditPanel->Caption + '1'; break; }
            case 2: { EditPanel->Caption = EditPanel->Caption + '2'; break; }
            case 3: { EditPanel->Caption = EditPanel->Caption + '3'; break; }
            case 4: { EditPanel->Caption = EditPanel->Caption + '4'; break; }
            case 5: { EditPanel->Caption = EditPanel->Caption + '5'; break; }
            case 6: { EditPanel->Caption = EditPanel->Caption + '6'; break; }
            case 7: { EditPanel->Caption = EditPanel->Caption + '7'; break; }
            case 8: { EditPanel->Caption = EditPanel->Caption + '8'; break; }
            case 9: { EditPanel->Caption = EditPanel->Caption + '9'; break; }
            case 10: {
                Text = EditPanel->Caption;
                if ((Text.Length() != 0) && (Text[1] == '-')) Text.Delete(1, 1);
                else Text = "-" + Text;
                EditPanel->Caption = Text;
                break;
            }
            case 11: {
                EditPanel->Caption = EditPanel->Caption + '.';
                break;
            }
            case 12: {
                if (TryStrToInt(StGatePanel->Caption, Val))
                    AutoconMain->DEV->SetStGate( CLAMP(ceil(AutoconMain->DEV->DepthToDelay(Val)), 0 , AutoconMain->DEV->GetEdGate()) );
                if (TryStrToInt(EdGatePanel->Caption, Val))
                    AutoconMain->DEV->SetEdGate( CLAMP(ceil(AutoconMain->DEV->DepthToDelay(Val)), AutoconMain->DEV->GetStGate(), 200) );


                if (TryStrToInt(AttPanel->Caption, Val)) AutoconMain->DEV->SetGain(Val);
                if (TryStrToInt(SensPanel->Caption, Val)) AutoconMain->DEV->SetSens(Val);
                if (TryStrToInt(TVGPanel->Caption, Val)) AutoconMain->DEV->SetTVG(Val);
                if (TryStrToFloat(PrismDelayPanel->Caption, Val2)) AutoconMain->DEV->SetPrismDelay(Val2 * 10);
                ValueToControl();
                break;
            }
            case 13: {
                Text = EditPanel->Caption;
                Text.Delete(Text.Length(), 1);
                EditPanel->Caption = Text;
                break;
            }
            case 14: {
                EditPanel->Caption = "";
                break;
            }
        }
    }
}
//---------------------------------------------------------------------------

void TAScanFrame::BeginHandBScanWrite()
{
	//this->Enabled = false; //Disable self
    BScanWriteProgressForm->Visible = true; // Show progress form

    AutoconMain->Rep->ClearStoredHandSignals();
    bsl->Clear(); //Clear b-scan
    bsl->Draw(0,0,BScanPB->Canvas);

    StartHandBScanTime = GetTickCount();
    BScanHandTimer->Enabled = true; //Enable timer

    AutoconMain->Rep->EnableHandScanStore(true);
}

void TAScanFrame::EndHandBScanWrite()
{
    BScanWriteProgressForm->Visible = false;  //Hide progress form
    this->Enabled = true; //Enable self
    BScanHandTimer->Enabled = false; // Disable timer

    // Write data
    CID curChannel = AutoconMain->DEV->GetChannel();
    sChannelDescription chanDesc;
    AutoconMain->Table->ItemByCID(curChannel, &chanDesc);
    sHandScanParams HParams;
    HParams.EnterAngle = chanDesc.cdEnterAngle;
    HParams.Method = chanDesc.cdMethod[0];
    HParams.ScanSurface = cbSurface->ItemIndex;
    HParams.Ku = AutoconMain->DEV->GetSens();
    HParams.Att =  AutoconMain->DEV->GetGain();
    HParams.StGate = AutoconMain->DEV->GetStGate();
    HParams.EdGate = AutoconMain->DEV->GetEdGate();
    HParams.PrismDelay = AutoconMain->DEV->GetPrismDelay();
    HParams.TVG = AutoconMain->DEV->GetTVG();
    int count = AutoconMain->Rep->AddStoredHandSignals(HParams);
    WriteBScanLabel->Caption = StringFormatU(L" ������ B-����. (%d)",count+1);
    AutoconMain->Rep->EnableHandScanStore(false);
}

void __fastcall TAScanFrame::BScanHandTimerTimer(TObject *Sender)
{
    DWORD CurrentTime = GetTickCount();
    DWORD ElapsedTime = CurrentTime - StartHandBScanTime;
    const DWORD scanTime = 40000;

    if( (ElapsedTime >= scanTime) || (BScanWriteProgressForm->IsCloseBtnClicked()) )
        EndHandBScanWrite();
    else
        BScanWriteProgressForm->SetPercent(ElapsedTime * 100 / scanTime);
}
//---------------------------------------------------------------------------

void TAScanFrame::ValueToControl(void)
{
    ZondAmplPanel->Caption = AutoconMain->Config->GetPulseAmplByCID(AutoconMain->DEV->GetChannel());
	SensPanel->Caption = IntToStr(AutoconMain->DEV->GetSens()); // �������� ���������������� [��]
	AttPanel->Caption = IntToStr(AutoconMain->DEV->GetGain());
	TVGPanel->Caption = IntToStr(AutoconMain->DEV->GetTVG()); // ��� [���]
	double tmp = (double)((float)AutoconMain->DEV->GetPrismDelay() / (float)10);
    PrismDelayPanel->Caption = Format("%f", ARRAYOFCONST((tmp)));

	StGatePanel->Caption = IntToStr((int)AutoconMain->DEV->DelayToDepth(AutoconMain->DEV->GetStGate())); // ������ ����� ��� [���]
	EdGatePanel->Caption = IntToStr((int)AutoconMain->DEV->DelayToDepth(AutoconMain->DEV->GetEdGate())); // ����� ������ ��� [���]
    GateIdxTextPanel->Caption = IntToStr(AutoconMain->DEV->GetGateIndex()); // ����� ������
    GateIdxPanel->Visible = AutoconMain->DEV->GetGateCount() != 1;

    switch(RullerMode) {
    case 0: {
            RullerModePanel->Caption = "���";
            break;
        }
    case 1: {
            RullerModePanel->Caption = "��";
            break;
        }
    case 2: {
            RullerModePanel->Caption = "��-��";
            break;
        }
    }

    if (BScanView)
        BScanViewPanel->Caption = "���";
    else
        BScanViewPanel->Caption = "����";

    if (NoiseRed) {
        NoiseRedPanel->Caption = "���";
    }
    else
        NoiseRedPanel->Caption = "����";
}
void __fastcall TAScanFrame::EditModePanelClick(TObject *Sender)
{
	TPanel* EnterModePanel = dynamic_cast<TPanel*>(Sender);

    if (EnterModePanel->Tag == 14)
    {
        if (EditPanel)
        {
            EditPanel->Color = clBtnHighlight;
            EditMode = False;
            EditPanel = NULL;
            NumPadPanel->Visible = EditMode;
        };
        return;
    }

	TPanel* SavePanel;
	SavePanel = EditPanel;
	if (EditPanel)
	{
		EditPanel->Color = clBtnHighlight;
		EditMode = False;
		EditPanel = NULL;
	};
	if (SavePanel != EnterModePanel)
	{
		EditPanel = EnterModePanel;
		EditPanel->Color = clLime;
		EditMode = True;
	};

	NumPadPanel->Visible = EditMode;

	if ((EnterModePanel->Tag == 9) && (EditPanel != NULL))
	{
	   AutoconMain->DEV->SetCalibrationMode(cmPrismDelay);
	   AutoconMain->DEV->Update(false);
       ValueToControl();
	}
	if (((EnterModePanel->Tag == 9) && (EditPanel == NULL)) || (EnterModePanel->Tag != 9))
	{
	   AutoconMain->DEV->SetCalibrationMode(cmSens);
	   AutoconMain->DEV->Update(false);
       ValueToControl();
	}
}
//---------------------------------------------------------------------------
void TAScanFrame::OnFormShortCut(TWMKey &Msg, bool &Handled)
{
    if ((EditPanel != NULL) && ((int)Msg.Msg == 45078)) {
		String tmp;
		tmp = EditPanel->Caption;

        switch(Msg.CharCode) {
        case 48: {
                tmp = tmp + "0";
                break;
            };
        case 49: {
                tmp = tmp + "1";
                break;
            };
        case 50: {
                tmp = tmp + "2";
                break;
            };
        case 51: {
                tmp = tmp + "3";
                break;
            };
        case 52: {
                tmp = tmp + "4";
                break;
            };
        case 53: {
                tmp = tmp + "5";
                break;
            };
		case 54: {
                tmp = tmp + "6";
                break;
            };
        case 55: {
                tmp = tmp + "7";
                break;
            };
        case 56: {
                tmp = tmp + "8";
                break;
            };
        case 57: {
                tmp = tmp + "9";
                break;
            };

        case 109: {
                UnicodeString ttt = "-";
                if (tmp.Length() != 0) {
                    if (tmp[1] != ttt[1]) {
                        tmp = ttt + tmp;
                    }
                    else {
                        tmp.Delete(1, 1);
                    }
                }
				else
                    tmp = "-";
                break;
            };
        case 8: {
                tmp.Delete(tmp.Length(), 1);
                break;
			};
        case 32: {
                tmp = "";
                break;
            };
		case 27: {
				EditPanel->Caption = EnterModeUndo;
                tmp = EnterModeUndo;
				// EnterMode = false;
				EditPanel->Color = clBtnHighlight;
				ValueToControl();
				break;
			};
		case 13: {
				// EnterMode = false;
				EditPanel->Color = clBtnHighlight;
				if (tmp == "")
                    tmp = "0";
                if (tmp == "-")
                    tmp = "0";
				switch(EditPanel->Tag)
				{
				case 77 : { // [+]  �� - 7

						int Val;
						if (TryStrToInt(tmp, Val)) AutoconMain->DEV->SetSens(Val);
						break;
					}
				case 8: { // [+]  ���
						int Val;
						if (TryStrToInt(tmp, Val)) AutoconMain->DEV->SetTVG(Val);
						break;
					}
				case 9: { // [+]  ����� � ������
						float Val;
						if (TryStrToFloat(tmp, Val)) AutoconMain->DEV->SetPrismDelay(Val * 10);
						break;
					}
				case 11: { // [+] ������ ������ ���
						int Val;
						if (TryStrToInt(tmp, Val) && (Val <= AutoconMain->DEV->GetStrokeLen())) AutoconMain->DEV->SetStGate(Val);
						break;
					}
				case 12: { // [+] ����� ������ ���
						int Val;
						if (TryStrToInt(tmp, Val) && (Val <= AutoconMain->DEV->GetStrokeLen())) AutoconMain->DEV->SetEdGate(Val);
						break;
					}
				}
	            Handled = true;
				ValueToControl();
				break;
			};
        }

		if (tmp != EditPanel->Caption)
		{
			EditPanel->Caption = tmp;
            Handled = true;
            EnterModeState = true;
        }
	}
}
void __fastcall TAScanFrame::BScanDrawTimerTimer(TObject *Sender)
{
    bsl->Draw(0, 0, BScanPB->Canvas);
    BScanDrawTimer->Enabled = false;
}
//---------------------------------------------------------------------------


void __fastcall TAScanFrame::ApplicationEvents1Message(tagMSG &Msg, bool &Handled)

{
    if (Msg.message == WM_MOUSEWHEEL) {
		if ((EditPanel == NULL)) {
            ScrollBox1->VertScrollBar->Position =
                ScrollBox1->VertScrollBar->Position - (short)(Msg.wParam >> 16);
        }
        else {
            if (((short)(Msg.wParam >> 16) > 0)
                /* && (Msg.hwnd == EnterModePanel->Handle) */ ) {
				OnSpinUpBtnClick(EditPanel);
                EnterModeState = true;
            }

            if (((short)(Msg.wParam >> 16) < 0)
                /* && (Msg.hwnd == EnterModePanel->Handle) */ ) {
				OnSpinDownBtnClick(EditPanel);
                EnterModeState = true;
			}
        }
    }
}
//---------------------------------------------------------------------------

void __fastcall TAScanFrame::Timer1Timer(TObject *Sender)
{
    if(!this->Visible)
        return;
    if ((AutoconMain) && (AutoconMain->DEV))
    {
        Panel36->Caption = "";
        int Summ = 0;
        float FAscan = 0;

        for (int UMUSide = 0; UMUSide < 2; UMUSide++)
        for (int UMULine = 0; UMULine < 2; UMULine++)
        {
            Summ = 0;
            for (int i = 0; i < 20; i++)
                Summ = Summ + AutoconMain->DEV->UMUList[0]->Lastd2[UMUSide][UMULine][i];

            FAscan = 0;
            if (Summ != 0)
                FAscan = (float)1 / ((float(Summ) / 20.f) / 1000.f);

            Panel36->Caption = Panel36->Caption + " S:" + IntToStr(UMUSide) + " L:" + IntToStr(UMULine);
            if (GetTickCount() - AutoconMain->DEV->UMUList[0]->AScanDebugArr[UMUSide][UMULine] < 250)
                Panel36->Caption = Panel36->Caption + Format("[%3.1f]", ARRAYOFCONST((FAscan)));
            else Panel36->Caption = Panel36->Caption + "[ X ]";
        }

        Summ = 0;
        for (int i = 0; i < 20; i++)
        Summ = Summ + AutoconMain->DEV->UMUList[0]->Lastd[i];

        FAscan = 0;
        if (Summ != 0)
        FAscan = (float)1 / ((float(Summ) / 20.f) / 1000.f);


        if (DebugOut)
            DebugPanelLabel->Caption = Format("CID: 0x%X | Fascan: %3.1f �� | devt: put-%d; get-%d; Device: %d; UMU: %d | Message Count - UMU:%d; DEV: %d | Error - Write: %d; Read: %d",
                                                ARRAYOFCONST((AutoconMain->DEV->GetChannel(),
                                                              FAscan,
                                                              AutoconMain->devt->Ptr_List.head_idx(),
                                                              AutoconMain->devt->Ptr_List.tail_idx(),
                                                              AutoconMain->DeviceEventManage->EventDataSize(),
                                                              AutoconMain->UMUEventManage->EventDataSize(),
                                                              AutoconMain->DEV->UMUList[AutoconMain->DEV->GetUsedUMUIdx()]->PostCount,
                                                              AutoconMain->devt->InTick,
                                                              AutoconMain->DEV->UMUList[AutoconMain->DEV->GetUsedUMUIdx()]->WriteErrorCount,
                                                              AutoconMain->DEV->UMUList[AutoconMain->DEV->GetUsedUMUIdx()]->ErrorMessageCount)));

    }
}
//---------------------------------------------------------------------------

void __fastcall TAScanFrame::HandScanTimeTimerTimer(TObject *Sender)
{
    if (AutoconMain)
        if (AutoconMain->Rep) {
            if (LastSelectChannels != - 1)
                AutoconMain->Rep->SetTimeInHandScanChannel(LastSelectChannels, AutoconMain->Rep->GetTimeInHandScanChannel(LastSelectChannels) + (GetTickCount() - LastSelectChannelsTickCount) / 1000);
            LastSelectChannelsTickCount = GetTickCount();
        }
}
//---------------------------------------------------------------------------

void __fastcall TAScanFrame::SetHandChannelBtn(TObject *Sender)
{
    ValuePanel00->Caption = "-";
    TComponent *AComponent = dynamic_cast<TComponent*>(Sender);

    if (LastSelectChannels != - 1)
    {
        AutoconMain->Rep->SetTimeInHandScanChannel(LastSelectChannels, AutoconMain->Rep->GetTimeInHandScanChannel(LastSelectChannels) + (GetTickCount() - LastSelectChannelsTickCount) / 1000);

        // Store hand channel params
        sChannelDescription chanDesc;
        assert( AutoconMain->Table->ItemByCID(LastSelectChannels, &chanDesc) );

        sHandScanParams HParams;
        HParams.EnterAngle = chanDesc.cdEnterAngle;
        HParams.Method = chanDesc.cdMethod[0];
        HParams.ScanSurface = cbSurface->ItemIndex;
        HParams.Ku = AutoconMain->DEV->GetSens();
        HParams.Att =  AutoconMain->DEV->GetGain();
        HParams.StGate = AutoconMain->DEV->GetStGate();
        HParams.EdGate = AutoconMain->DEV->GetEdGate();
        HParams.PrismDelay = AutoconMain->DEV->GetPrismDelay();
        HParams.TVG = AutoconMain->DEV->GetTVG();
        AutoconMain->Rep->AddHandSignals(HParams,NULL, 0);
    }
    LastSelectChannelsTickCount = GetTickCount();
    switch(AComponent->Tag)
    {

        case 0:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x1E); LastSelectChannels = 0x1E; break; }
        case 1:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x1D); LastSelectChannels = 0x1D; break; }
        case 2:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x1F); LastSelectChannels = 0x1F; break; }
        case 3:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x20); LastSelectChannels = 0x20; break; }
        case 4:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x21); LastSelectChannels = 0x21; break; }
        case 5:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x22); LastSelectChannels = 0x22; break; }
        case 6:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x23); LastSelectChannels = 0x23; break; }
    }

    bsl->Clear(); //Clear b-scan
    bsl->Draw(0,0,BScanPB->Canvas);

//    AutoconMain->DEV->PathEncoderSim(true);
    AutoconMain->DEV->Update(false);
    AScanChart->BottomAxis->Maximum = DelayToChartValue(AutoconMain->DEV->GetAScanLen());
    SaveMax1 = - 1;
    SaveMax2 = - 1;
    ValueToControl();
    bsl->SetViewDelayZone(0, AutoconMain->DEV->GetAScanLen());

    TRACE("Delay: %.2f mm",AutoconMain->DEV->DelayToDepthByHeight(AutoconMain->DEV->GetAScanLen()));
}
//---------------------------------------------------------------------------

void __fastcall TAScanFrame::BScanPanelResize(TObject *Sender)
{
    bsl->SetSize(TSize(BScanPB->Width, BScanPB->Height));
}
//---------------------------------------------------------------------------


void __fastcall TAScanFrame::m_MarkEnableBtnClick(TObject *Sender)
{
    bMarkBtnState = AutoconMain->DEV->GetMarkState();
    bMarkBtnState = !bMarkBtnState;

    updateMarkState();
}
void TAScanFrame::updateMarkState()
{
    if(bMarkBtnState)
    {
        AutoconMain->DEV->SetMarkWidth(10);
        AutoconMain->DEV->StartUseMark();
        MarkMovementTimer->Enabled = true;
        m_MarkOffsetGrid->Enabled = true;
        m_MarkOffsetLeftBtn->Enabled = true;
        m_MarkOffsetRightBtn->Enabled = true;
        m_MarkWidthPanel->Caption = StringFormatU(L"������ - %d ���", AutoconMain->DEV->GetMarkWidth());
        m_MarkOffsetPanel->Caption = StringFormatU(L"�������� (%d ���):", AutoconMain->DEV->GetMark());
        m_MarkEnableBtn->Caption = "����. �����";
    }
    else
    {
        AutoconMain->DEV->StopUseMark();
        MarkMovementTimer->Enabled = false;
        m_MarkOffsetGrid->Enabled = false;
        m_MarkOffsetLeftBtn->Enabled = false;
        m_MarkOffsetRightBtn->Enabled = false;
        m_MarkWidthPanel->Caption = "������ - 0 ���";
        m_MarkOffsetPanel->Caption = "��������:";
        m_MarkEnableBtn->Caption = "���. �����";
    }
}
//---------------------------------------------------------------------------

void __fastcall TAScanFrame::MarkMovementTimerTimer(TObject *Sender)
{
    if(MarkBtnDownState[0])
        AutoconMain->DEV->SetMark(AutoconMain->DEV->GetMark() - 1);
    if(MarkBtnDownState[1])
        AutoconMain->DEV->SetMark(AutoconMain->DEV->GetMark() + 1);

    updateMarkState();
}
//---------------------------------------------------------------------------
void __fastcall TAScanFrame::OnMarkBtnMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
    TSpeedButton* pBtn = dynamic_cast<TSpeedButton*>(Sender);
    MarkBtnDownState[pBtn->Tag] = true;
}
//---------------------------------------------------------------------------

void __fastcall TAScanFrame::OnMarkBtnMouseUp(TObject *Sender)
{
    TSpeedButton* pBtn = dynamic_cast<TSpeedButton*>(Sender);
    MarkBtnDownState[pBtn->Tag] = false;
}
//---------------------------------------------------------------------------

void __fastcall TAScanFrame::OnMarkBtnMouseUp2(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
    TSpeedButton* pBtn = dynamic_cast<TSpeedButton*>(Sender);
    MarkBtnDownState[pBtn->Tag] = false;
}
//---------------------------------------------------------------------------

void __fastcall TAScanFrame::DebugPanelDblClick(TObject *Sender)
{
    LogForm->LogLines->SaveToFile("log.txt");
    DebugOut = !DebugOut;
}
//---------------------------------------------------------------------------

void __fastcall TAScanFrame::rightPanelUpBtnClick(TObject *Sender)
{
    ScrollBox1->VertScrollBar->Position -= 100;
}
//---------------------------------------------------------------------------

void __fastcall TAScanFrame::rightPanelDownBtnClick(TObject *Sender)
{
    ScrollBox1->VertScrollBar->Position += 100;
}
//---------------------------------------------------------------------------


