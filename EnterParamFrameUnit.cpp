//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "EnterParamFrameUnit.h"
#include "ConfigUnit.h"
#include "AutoconMain.h"
extern cACConfig * ACConfig;
extern cAutoconMain *AutoconMain;

#include "BScanFrameUnit.h"
#include "AScanFrameUnit.h"
#include "ScreenMessageUnit3.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BasicFrameUnit"
#pragma resource "*.dfm"
//TEnterParamFrame *EnterParamFrame;
//---------------------------------------------------------------------------
__fastcall TEnterParamFrame::TEnterParamFrame(TComponent* Owner)
    : TBasicFrame(Owner)
{
    nextframe_flags = 0;
    bHardnessState = false;
    bIsFirstShow = true;

    currImageIndex = -1;
    //memset(LinearityImages, 0, sizeof(LinearityImages));
}
//---------------------------------------------------------------------------
void __fastcall TEnterParamFrame::ValueListSelectCell(TObject *Sender, int ACol,
          int ARow, bool &CanSelect)
{
    TouchKeyboard1->Visible = true;
}
//---------------------------------------------------------------------------

void TEnterParamFrame::SetFrameFlags(DWORD flags)
{
    this->frameFlags = flags;

    if(frameFlags & MF_EPF_FLAG_CAPTION_CONTROL)
    {
        CaptionPanel->Caption = "����� �������� ��������";
    }
    else if(frameFlags & MF_EPF_FLAG_CAPTION_TEST)
    {
        CaptionPanel->Caption = "����� �������� ������������";
    }
    else if(frameFlags & MF_EPF_FLAG_CAPTION_HAND)
    {
        CaptionPanel->Caption = "����� �������� ��������";
    }

    NoRepButton->Visible = frameFlags & MF_EPF_FLAG_BTN_NO_REPORT;
    CancelButton->Visible = frameFlags & MF_EPF_FLAG_BTN_CANCEL;
}

void TEnterParamFrame::OnMFrameAttach(TBasicFrame* pMain)
{
    LoadValueListData();

    if(AutoconMain->SkipRep && !bIsFirstShow)
        pParentFrame->popFrame();
}
void TEnterParamFrame::OnMFrameDetach(TBasicFrame* pMain)
{

}
void TEnterParamFrame::OnMFrameCreate(TBasicFrame* pMain)
{
    ValueListData = new TStringList();

    �onclusionSubData = new TStringList();
    �onclusionSubData->Add("���� �����");
    �onclusionSubData->Add("���� �������");

    HardnessSubData = new TStringList();
    HardnessSubData->Add("���������");
    HardnessSubData->Add("�� �����������");

}
void TEnterParamFrame::OnMFrameDestroy(TBasicFrame* pMain)
{
    delete ValueListData;
    delete �onclusionSubData;
    delete HardnessSubData;
}

void TEnterParamFrame::LoadValueListData()
{
    ValueListData->Clear();

//FDV /* 0 */  ValueListData->Add("���� / ����� ��������=" + (bIsFirstShow ? DateTimeToStr(Now()) : DateTimeToStr(ACConfig->Last_DateTime)));//DateTimeToStr(Now()));

//FDV
    if(frameFlags & MF_EPF_FLAG_SEARCH_TYPE)
 /* 0 */  ValueListData->Add("���� / ����� ��������=" + /*(bIsFirstShow ? */DateTimeToStr(Now()/*) : DateTimeToStr(ACConfig->Last_DateTime)*/));//DateTimeToStr(Now())); //FDV
    else if(frameFlags & MF_EPF_FLAG_TEST_TYPE)
 /* 0 */  ValueListData->Add("���� / ����� ��������=" + /*(bIsFirstShow ? */DateTimeToStr(Now()/*) : DateTimeToStr(ACConfig->Last_SSC_TestDateTime)*/));//DateTimeToStr(Now())); //FDV

 /* 1 */  ValueListData->Add("����� �����=" + IntToStr(ACConfig->Last_GangNumber));    // ����� �����

    if(frameFlags & MF_EPF_FLAG_SEARCH_TYPE)                                                                                         //FDV
 /* 2 */  ValueListData->Add("����� ��������� ��������=" + IntToStr(ACConfig->Last_ReportNumber));   // ����� ��������� ��������     //FDV
    else if(frameFlags & MF_EPF_FLAG_TEST_TYPE)                                                                                      //FDV
 /* 2 */  ValueListData->Add("����� ��������� ������������=" + IntToStr(ACConfig->Last_TestReportNumber));   // ����� ��������� �������� //FDV

 /* 3 */  ValueListData->Add("����� �����=" + IntToStr(ACConfig->Last_PletNumber)); // ����� �����
 /* 4 */  ValueListData->Add("����� �����=" + IntToStr(ACConfig->Last_JointNumber)); // ����� �����
 if (bIsFirstShow)
 {
    ACConfig->Last_TopStraightness = 0; // ��������������� ������ [��]
    ACConfig->Last_WF_Straightness = 0; // ��������������� ����� (��) [��]
    ACConfig->Last_NWF_Straightness = 0; // ��������������� ����� (���) [��]
 }
 /* 5 */  ValueListData->Add("��������������� ������ [��]=" + Format("%3.3f", ARRAYOFCONST((ACConfig->Last_TopStraightness)))); // ��������������� ������ [��]
 /* 6 */  ValueListData->Add("��������������� �� [��]=" + Format("%3.3f", ARRAYOFCONST((ACConfig->Last_WF_Straightness)))); // ��������������� ����� (��) [��]
 /* 7 */  ValueListData->Add("��������������� ��� [��]=" + Format("%3.3f", ARRAYOFCONST((ACConfig->Last_NWF_Straightness)))); // ��������������� ����� (���) [��]
 /* 8 */  ValueListData->Add("�������� ���=" + ACConfig->Last_Operator);  // �������� ���
 /* 9 */  ValueListData->Add("��� �������=" + ACConfig->Last_DefectCode);// ��� �������
/* 10 */  ValueListData->Add("��. ��������=" + ACConfig->Last_ValidityGroup);// ��. ��������
/* 11 */  ValueListData->Add("����������=" + ACConfig->Last_�onclusion); // ����������
/* 12 */  ValueListData->Add("��������� ��������=" + ((bIsFirstShow || (ACConfig->Last_HardnessState != 1))? AnsiString("�� �����������") : AnsiString("���������")));  // ��������
/* 13 */  ValueListData->Add("�������� (�����, ����� 1)=" + IntToStr(ACConfig->Last_Hardness[0]));  //
/* 14 */  ValueListData->Add("�������� (�����, ����� 2)=" + IntToStr(ACConfig->Last_Hardness[1]));  //
/* 15 */  ValueListData->Add("�������� (�����, ����� 3)=" + IntToStr(ACConfig->Last_Hardness[2]));  //
/* 16 */  ValueListData->Add("�������� (�����, ����� 1)=" + IntToStr(ACConfig->Last_Hardness[3]));  //
/* 17 */  ValueListData->Add("�������� (�����, ����� 2)=" + IntToStr(ACConfig->Last_Hardness[4]));  //
/* 18 */  ValueListData->Add("�������� (�����, ����� 3)=" + IntToStr(ACConfig->Last_Hardness[5]));  //
/* 19 */  ValueListData->Add("�������� (������, ����� 1)=" + IntToStr(ACConfig->Last_Hardness[6]));  //
/* 20 */  ValueListData->Add("�������� (������, ����� 2)=" + IntToStr(ACConfig->Last_Hardness[7]));  //
/* 21 */  ValueListData->Add("�������� (������, ����� 3)=" + IntToStr(ACConfig->Last_Hardness[8]));  //

  FiltrPanel->Visible = bIsFirstShow & ACConfig->ShowFiltrParam_;

  MinLenEdit->Value = ACConfig->MinLenEdit_;
  MaxSpace->Value = ACConfig->MaxSpace_;
  DelayDelta->Value = ACConfig->DelayDelta_;
  SameCount->Value = ACConfig->SameCount_;
  FiltrState->Checked = ACConfig->UseFiltr_;

    UpdateValueList(true);
}

UnicodeString InsertStrData(UnicodeString Src, UnicodeString Data)
{
    if (Src.Pos("=") != 0)
    {
        Src.Delete(Src.Pos("=") + 1, 255);
        return Src + Data;
    }
    return "";
}

UnicodeString GetStrData(UnicodeString Src)
{
    if (Src.Pos("=") != 0)
    {
        Src.Delete(1, Src.Pos("="));
        return Src;
    }
    return "";
}

UnicodeString GetStrName(UnicodeString Src)
{
    if (Src.Pos("=") != 0)
    {
        Src.Delete(Src.Pos("="), Src.Length() - Src.Pos("=") + 1);
        return Src;
    }
    return Src;
}

void TEnterParamFrame::StoreValueListData()
{
    bool Res;
    int Sel = - 1;
    int Tmp;
    UnicodeString Tmp2;

    UpdateValueList(false);
    if(frameFlags & MF_EPF_FLAG_SEARCH_TYPE)
	{
		if (FormatSettings.DecimalSeparator == ',') Tmp =  ValueListData->Strings[5].Pos(".");
		if (FormatSettings.DecimalSeparator == '.') Tmp =  ValueListData->Strings[5].Pos(",");
		if (Tmp != 0)
		{
			Tmp2 = ValueListData->Strings[5];
			Tmp2[Tmp] = FormatSettings.DecimalSeparator;
			ValueListData->Strings[5] = Tmp2;
		}

//		FormatSettings.DecimalSeparator

		Res = TryStrToFloat(GetStrData(ValueListData->Strings[5]), ACConfig->Last_TopStraightness); // ��������������� ������ [��]
		if (!Res) Sel = 5;

		if (FormatSettings.DecimalSeparator == ',') Tmp =  ValueListData->Strings[6].Pos(".");
		if (FormatSettings.DecimalSeparator == '.') Tmp =  ValueListData->Strings[6].Pos(",");
		if (Tmp != 0)
		{
			Tmp2 = ValueListData->Strings[6];
			Tmp2[Tmp] = FormatSettings.DecimalSeparator;
			ValueListData->Strings[6] = Tmp2;
		}
		Res = TryStrToFloat(GetStrData(ValueListData->Strings[6]), ACConfig->Last_WF_Straightness); // ��������������� ����� (��)[��]
		if (!Res) Sel = 6;

		if (FormatSettings.DecimalSeparator == ',') Tmp =  ValueListData->Strings[7].Pos(".");
		if (FormatSettings.DecimalSeparator == '.') Tmp =  ValueListData->Strings[7].Pos(",");
		if (Tmp != 0)
		{
			Tmp2 = ValueListData->Strings[7];
			Tmp2[Tmp] = FormatSettings.DecimalSeparator;
            ValueListData->Strings[7] = Tmp2;
        }
        Res = TryStrToFloat(GetStrData(ValueListData->Strings[7]), ACConfig->Last_NWF_Straightness); // ��������������� ����� (���)[��]
        if (!Res) Sel = 7;

		Res = TryStrToInt(GetStrData(ValueListData->Strings[1]), ACConfig->Last_GangNumber); // ����� �����
		if (!Res) Sel = 1;
		Res = TryStrToInt(GetStrData(ValueListData->Strings[2]), ACConfig->Last_ReportNumber); // ����� ��������� ��������
		if (!Res) Sel = 2;
		Res = TryStrToInt(GetStrData(ValueListData->Strings[3]), ACConfig->Last_PletNumber); // ����� �����
		if (!Res) Sel = 3;
		Res = TryStrToInt(GetStrData(ValueListData->Strings[4]), ACConfig->Last_JointNumber); // ����� �����
		if (!Res) Sel = 4;

        try {
		    ACConfig->Last_DateTime = StrToDateTime(GetStrData(ValueListData->Strings[0])); // ���� / ����� ��������
        }
        catch(...) {
           Sel = 0;
        }

		ACConfig->Last_Operator = GetStrData(ValueListData->Strings[8]); // �������� ���
		ACConfig->Last_DefectCode = GetStrData(ValueListData->Strings[9]); // ��� �������
        ACConfig->Last_�onclusion = GetStrData(ValueListData->Strings[11]); // ����������
		ACConfig->Last_ValidityGroup = GetStrData(ValueListData->Strings[10]); // ��. ��������
        ACConfig->Last_HardnessState = bHardnessState ? 1 : ACConfig->Last_HardnessState ? 2 : 0;

        if (bHardnessState)
        {
            for (int i = 13; i <= 21; i++)
            {
                Res = TryStrToInt(GetStrData(ValueListData->Strings[i]), ACConfig->Last_Hardness[i - 13]); // ����� �����
                if (!Res) Sel = i;
            }
            ACConfig->Last_HardnessMeasTime = ACConfig->Last_DateTime;
        }

		if (Sel != - 1)
		{
			ValueList->Row = Sel + 1;
			ValueList->SetFocus();
		};
    }
    else if(frameFlags & MF_EPF_FLAG_TEST_TYPE)
    {
        Res = TryStrToInt(GetStrData(ValueListData->Strings[1]), ACConfig->Last_GangNumber); // ����� �����
		if (!Res) Sel = 1;
		Res = TryStrToInt(GetStrData(ValueListData->Strings[2]), ACConfig->Last_TestReportNumber); // ����� ��������� ��������
		if (!Res) Sel = 2;
		ACConfig->Last_SSC_TestDateTime = StrToDateTime(GetStrData(ValueListData->Strings[0])); // ���� / ����� ��������
		ACConfig->Last_Operator = GetStrData(ValueListData->Strings[7]); // �������� ���

		if (Sel != - 1)
		{
			ValueList->Row = Sel + 1;
			ValueList->SetFocus();
			//return;
		};
    }
}

void TEnterParamFrame::UpdateValueList(bool bFromData)
{
    if(bFromData)
    {
        ValueList->Strings->Clear();
        if(frameFlags & MF_EPF_FLAG_SEARCH_TYPE)
        {
            for (int i = 0; i < 13; i++)
				ValueList->Strings->AddObject(ValueListData->Strings[i],(TObject*)i);
            if(ValueListData->Strings[12] != "��������� ��������=�� �����������")
            {
                for (int i = 13; i < 22; i++)
				    ValueList->Strings->AddObject(ValueListData->Strings[i],(TObject*)i);
            }

            ValueList->ItemProps[0]->EditStyle = esEllipsis;
            ValueList->ItemProps[8]->PickList = ACConfig->Operators;
            ValueList->ItemProps[11]->PickList = �onclusionSubData;
            ValueList->ItemProps[12]->PickList = HardnessSubData;
        }
        else if(frameFlags & MF_EPF_FLAG_TEST_TYPE)
        {
            ValueList->Strings->AddObject(ValueListData->Strings[0],(TObject*)0);
            ValueList->Strings->AddObject(ValueListData->Strings[1],(TObject*)1);
            ValueList->Strings->AddObject(ValueListData->Strings[2],(TObject*)2);
            ValueList->Strings->AddObject(ValueListData->Strings[8],(TObject*)7);

            ValueList->ItemProps[0]->EditStyle = esEllipsis;
            ValueList->ItemProps[3]->PickList = ACConfig->Operators;
        }
    }
    else
    {
        for(int i = 0; i < ValueList->Strings->Count; i++)
        {
            int idx = (int)ValueList->Strings->Objects[i];
            assert((idx >= 0) && (idx < ValueListData->Count));
            ValueListData->Strings[idx] = ValueList->Strings->Strings[i];
        }
    }
}
void __fastcall TEnterParamFrame::ValueListStringsChanging(TObject *Sender)
{
    if(frameFlags & MF_EPF_FLAG_SEARCH_TYPE)
    {
        if (ValueList->Strings->Count >= 13)
        {
            if ((ValueList->Strings->Strings[12] == "��������� ��������=���������") && (!bHardnessState))
            {
                UpdateValueList(false);
                bHardnessState = true;
                UpdateValueList(true);
            }
            else
            if ((ValueList->Strings->Strings[12] != "��������� ��������=���������") && (bHardnessState))
            {
                UpdateValueList(false);
                bHardnessState = false;
                UpdateValueList(true);
            }
        }
    }
}
//---------------------------------------------------------------------------

void __fastcall TEnterParamFrame::ValueListStringsChange(TObject *Sender)
{
    ValueListStringsChanging(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TEnterParamFrame::setRusLangBtnClick(TObject *Sender)
{
    UnicodeString Layout = "00000419";
    LoadKeyboardLayout( Layout.c_str(), KLF_ACTIVATE);
    TouchKeyboard1->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall TEnterParamFrame::setEngLangBtnClick(TObject *Sender)
{
    UnicodeString Layout = "00000409";
    LoadKeyboardLayout( Layout.c_str(), KLF_ACTIVATE);
    TouchKeyboard1->Refresh();
}
//---------------------------------------------------------------------------

void TEnterParamFrame::StartNextFrame()
{
    if(next_state != acNotSet)
        if(!SetAcState(next_state,5000))
        {
            return;
        };

    UpdateValueList(false);

    if(frameFlags & MF_EPF_FLAG_NEXTFRAME_BSCAN)
    {
        pParentFrame->pushFrame<TBScanFrame>(nextframe_flags);
    }
    else if(frameFlags & MF_EPF_FLAG_NEXTFRAME_ASCAN)
    {
        pParentFrame->pushFrame<TAScanFrame>(nextframe_flags);
    }

    SetFrameFlags(frameFlags & ~(MF_EPF_FLAG_BTN_NO_REPORT | MF_EPF_FLAG_BTN_CANCEL));
    StartButton->Caption = "���������";
	bIsFirstShow = false;
}

void __fastcall TEnterParamFrame::StartButtonClick(TObject *Sender)
{
	StoreValueListData();

	  ACConfig->MinLenEdit_  = MinLenEdit->Value;
	  ACConfig->MaxSpace_    = MaxSpace->Value   ;
	  ACConfig->DelayDelta_  = DelayDelta->Value ;
	  ACConfig->SameCount_   = SameCount->Value  ;
      ACConfig->UseFiltr_    = FiltrState->Checked;


	AutoconMain->DEV->UMUList[0]->SetStartAmpl(SpinEdit1->Value);
	AutoconMain->DEV->UMUList[1]->SetStartAmpl(SpinEdit1->Value);
	AutoconMain->DEV->UMUList[2]->SetStartAmpl(SpinEdit1->Value);

	if(bIsFirstShow)
	{
		if (FiltrState->Checked)
		{
			AutoconMain->Rep->fp.OKLength = MinLenEdit->Value;
			AutoconMain->Rep->fp.MaxSysCoordStep = MaxSpace->Value;
			AutoconMain->Rep->fp.DeltaDelay  = DelayDelta->Value;
			AutoconMain->Rep->fp.SameDelayCount = SameCount->Value;
//            Filter(AutoconMain->Rep, fp);
			AutoconMain->Rep->fp.State = true;
		}
		else AutoconMain->Rep->fp.State = false;

        AutoconMain->Rep->ResetFilter_();
		AutoconMain->Rep->SetFilterState_(AutoconMain->Rep->fp.State);

        AutoconMain->SkipRep = false;
        StartNextFrame();


    }
    else
    {
        SaveProtocol();
        pParentFrame->popFrame();
    }
}
//---------------------------------------------------------------------------

void __fastcall TEnterParamFrame::CancelButtonClick(TObject *Sender)
{
    pParentFrame->popFrame();
}
//---------------------------------------------------------------------------

#include <tlhelp32.h>
bool GetPIDByName(const wchar_t* name, std::vector<unsigned long>& pids)
{
    PROCESSENTRY32 entry;
    entry.dwSize = sizeof(PROCESSENTRY32);

    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
    bool bRes = false;
    if (Process32First(snapshot, &entry))
    {
        while (Process32Next(snapshot, &entry))
        {
            if (_wcsicmp(entry.szExeFile, name) == 0)
            {
                pids.push_back(entry.th32ProcessID);
                bRes=true;
            }
        }
    }

    CloseHandle(snapshot);
    return bRes;
}

void TEnterParamFrame::UpdatePhoneImage(int index)
{
    UnicodeString exeFile = Application->ExeName;
    UnicodeString exePath = ExtractFilePath(exeFile);

    PathMgr::PushPath(L"$screenshots", L"resources\\screenshots");
    UnicodeString prg_path = exePath+PathMgr::GetPath(L"($screenshots)\\phone_android\\screenshot.jar");
    UnicodeString adb_path = exePath+PathMgr::GetPath(L"($screenshots)\\phone_android\\adb.exe");
    UnicodeString screen_path = exePath+PathMgr::GetPath(L"($screenshots)\\phone_android\\screen.png");

    std::vector<unsigned long> pids;

#ifndef SIMULATE_PHONE_PHOTO
    if(!GetPIDByName(L"adb.exe", pids)) //Check if adb is not running
    {
        //Launching adb
        ShellExecute( NULL, L"open", adb_path.c_str(), L"shell ls -l", NULL, SW_HIDE);
        Sleep(3000);
    }

    DeleteFileW( screen_path.c_str() );
    UnicodeString shell_cmd = StringFormatU(L"-d %s",screen_path.c_str());

    SHELLEXECUTEINFO ShExecInfo = {0};
    ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
    ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
    ShExecInfo.hwnd = NULL;
    ShExecInfo.lpVerb = NULL;
    ShExecInfo.lpFile = prg_path.c_str();
    ShExecInfo.lpParameters = shell_cmd.c_str();
    ShExecInfo.lpDirectory = NULL;
    ShExecInfo.nShow = SW_HIDE;
    ShExecInfo.hInstApp = NULL;

    //Executing
    ShellExecuteEx(&ShExecInfo);
    //Wait for completion
    if(WaitForSingleObject(ShExecInfo.hProcess,8000) != WAIT_OBJECT_0)
    {
        Application->MessageBoxW(L"������� �� ��������.", L"������",MB_OK | MB_ICONERROR);
        return;
    }
    CloseHandle(ShExecInfo.hProcess);

    if(!FileExists(screen_path, false))
    {
        Application->MessageBoxW(L"������ �� �������.", L"������",MB_OK | MB_ICONERROR);
        return;
    }
#else
    screen_path = exePath+PathMgr::GetPath(L"($screenshots)\\phone_android\\screen_sim.png");
#endif

    if(!LinearityImages[index])
        LinearityImages[index] = new TPicture();
    LinearityImages[index]->LoadFromFile(screen_path);

    TJPEGImage** ppRepImage = NULL;
    switch(index)
    {
        case 0:
            ppRepImage = &AutoconMain->Rep->PhoneScreenShot;
            break;
        case 1:
            ppRepImage = &AutoconMain->Rep->PhoneScreenShot2;
            break;
        case 2:
            ppRepImage = &AutoconMain->Rep->PhoneScreenShot3;
            break;
    }
    if(!(*ppRepImage))
        (*ppRepImage) = new TJPEGImage;

    TBitmap* tempBitmap = new TBitmap;
    tempBitmap->Assign(LinearityImages[index]->Graphic);
    (*ppRepImage)->Assign(tempBitmap);
    delete tempBitmap;

    bImageIsAssigned[index] = true;
    AssignPhoneImage(index);
}
void TEnterParamFrame::AssignPhoneImage(int index)
{
    currLinearityImage->Picture->Assign(LinearityImages[index]);
}

void __fastcall TEnterParamFrame::LinearityBtnClick(TObject *Sender)
{
    TSpeedButton* pBtn = dynamic_cast<TSpeedButton*>(Sender);
    int idx = pBtn->Tag;

    if(!bImageIsAssigned[idx])
        UpdatePhoneImage(idx);
    else
    {
        AssignPhoneImage(idx);
    }
    currImageIndex = idx;
}
//---------------------------------------------------------------------------


void __fastcall TEnterParamFrame::currLinearityImageClick(TObject *Sender)
{
    if(currImageIndex >= 0)
        UpdatePhoneImage(currImageIndex);
}
//---------------------------------------------------------------------------

AnsiString __fastcall GetWVersion()
{
  AnsiString temp;
  VS_FIXEDFILEINFO VerValue;
  DWORD size, n;
  n = GetFileVersionInfoSize(Application->ExeName.c_str(), &size);
  if (n > 0)
  {
    char *pBuf = new char[n];
    GetFileVersionInfo(Application->ExeName.c_str(), 0, n, pBuf);
    LPVOID pValue;
    UINT Len;
    if (VerQueryValueA(pBuf, "\\", &pValue, &Len))
    {
      memmove(&VerValue, pValue, Len);
      temp = IntToStr((__int64)VerValue.dwFileVersionMS >> 16);
      temp += "." + IntToStr((__int64)VerValue.dwFileVersionMS & 0xFFFF);
      temp += "." + IntToStr((__int64)VerValue.dwFileVersionMS >> 16);
      temp += "." + IntToStr((__int64)VerValue.dwFileVersionMS & 0xFF);
    }
    delete [] pBuf;
  }
  return temp;
}

void TEnterParamFrame::SaveProtocol()
{
    TRACE("void TEnterParamFrame::SaveProtocol()");

    TDateTime lastDateTime;
    if(frameFlags & MF_EPF_FLAG_SEARCH_TYPE)
        lastDateTime = ACConfig->Last_DateTime;
    else if(frameFlags & MF_EPF_FLAG_TEST_TYPE)
        lastDateTime = ACConfig->Last_SSC_TestDateTime;
    UnicodeString str = DateTimeToStr(lastDateTime);

    while (Pos(":", str) != 0)  { str[Pos(":", str)] = '_'; }
    while (Pos(".", str) != 0)  { str[Pos(".", str)] = '_'; }

    // ������ �����
    AutoconMain->Rep->Header.Plaint = ACConfig->Current_Plaint;;   	  // �����������
    AutoconMain->Rep->Header.PlantNumber = ACConfig->Current_PlantNumber;//"13001";   // ��������� ����� ���������
    AutoconMain->Rep->Header.VerSoft = GetWVersion(); // ������ ��

    // ������ �����������
    int Last_ReportNumber = (frameFlags & MF_EPF_FLAG_SEARCH_TYPE) ?
                                (ACConfig->Last_ReportNumber) :
                                (ACConfig->Last_TestReportNumber);

    AutoconMain->Rep->Header.FileType = AutoconMain->ReportFileType; // �������� / ����
    AutoconMain->Rep->Header.DateTime = lastDateTime; // ���� / ����� ��������
    AutoconMain->Rep->Header.GangNumber = ACConfig->Last_GangNumber; // ����� �����
    AutoconMain->Rep->Header.ReportNumber = Last_ReportNumber; // ����� ��������� ��������
    AutoconMain->Rep->Header.PletNumber = ACConfig->Last_PletNumber; // ����� �����
    AutoconMain->Rep->Header.JointNumber = ACConfig->Last_JointNumber; // ����� �����
    AutoconMain->Rep->Header.TopStraightness_ = ACConfig->Last_TopStraightness; // ��������������� ������ [��] //FDV
//    AutoconMain->Rep->Header.SideStraightness = ACConfig->Last_WF_Straightness; // ��������������� ����� [��] //FDV
    AutoconMain->Rep->Header.WF_Straightness_ = ACConfig->Last_WF_Straightness; // ��������������� ����� [��] //FDV
    AutoconMain->Rep->Header.WNF_Straightness_ = ACConfig->Last_NWF_Straightness; // ��������������� ����� [��] //FDV


 /* 3 */  ValueListData->Add("����� �����=" + IntToStr(ACConfig->Last_PletNumber)); // ����� �����
 /* 4 */  ValueListData->Add("����� �����=" + IntToStr(ACConfig->Last_JointNumber)); // ����� �����
 /* 5 */  ValueListData->Add("��������������� ������ [��]=" + Format("%3.3f", ARRAYOFCONST((ACConfig->Last_TopStraightness)))); // ��������������� ������ [��]
 /* 6 */  ValueListData->Add("��������������� �� [��]=" + Format("%3.3f", ARRAYOFCONST((ACConfig->Last_WF_Straightness)))); // ��������������� ����� (��) [��]
 /* 7 */  ValueListData->Add("��������������� ��� [��]=" + Format("%3.3f", ARRAYOFCONST((ACConfig->Last_NWF_Straightness)))); // ��������������� ����� (���) [��]
 /* 8 */  ValueListData->Add("�������� ���=" + ACConfig->Last_Operator);  // �������� ���



    AutoconMain->Rep->Header.HardnessState = ACConfig->Last_HardnessState;
    AutoconMain->Rep->Header.Hardness[0] = ACConfig->Last_Hardness[0]; // ��������
    AutoconMain->Rep->Header.Hardness[1] = ACConfig->Last_Hardness[1]; // ��������
    AutoconMain->Rep->Header.Hardness[2] = ACConfig->Last_Hardness[2]; // ��������
    AutoconMain->Rep->Header.Hardness[3] = ACConfig->Last_Hardness[3]; // ��������
    AutoconMain->Rep->Header.Hardness[4] = ACConfig->Last_Hardness[4]; // ��������
    AutoconMain->Rep->Header.Hardness[5] = ACConfig->Last_Hardness[5]; // ��������
    AutoconMain->Rep->Header.Hardness[6] = ACConfig->Last_Hardness[6]; // ��������
    AutoconMain->Rep->Header.Hardness[7] = ACConfig->Last_Hardness[7]; // ��������
    AutoconMain->Rep->Header.Hardness[8] = ACConfig->Last_Hardness[8]; // ��������
    AutoconMain->Rep->Header.HardnessLastMeasureDateTime = ACConfig->Last_HardnessMeasTime;
    AutoconMain->Rep->Header.LastSSCTestDateTime = ACConfig->Last_SSC_TestDateTime;
    AutoconMain->Rep->Header.LastSS3RTestDateTime = ACConfig->Last_SS3R_TestDateTime;
    //AutoconMain->Rep->Header.HardnessUnit = ACConfig->Last_HardnessUnit; // ��. ���. ��������: 1 � HB, 2 - HV, 3 - HRC
    AutoconMain->Rep->Header.Operator = ACConfig->Last_Operator; // �������� ���
    AutoconMain->Rep->Header.DefectCode = ACConfig->Last_DefectCode; // ��� �������
    AutoconMain->Rep->Header.ValidityGroup = ACConfig->Last_ValidityGroup; // ��. ��������
    AutoconMain->Rep->Header.�onclusion = ACConfig->Last_�onclusion;

    sScanChannelParams tmp;
    for (int id = 0; id < ScanChannelCount; id++)
    {
         tmp.Sens = AutoconMain->Calibration->GetSens(dsNone, id + AutoconStartCID, 0);             // �������� ���������������� [��]
         tmp.Gain = AutoconMain->Calibration->GetGain(dsNone, id + AutoconStartCID, 0);             // ���������� [��]
         tmp.StGate = AutoconMain->Calibration->GetStGate(dsNone, id + AutoconStartCID, 0);         // ������ ����� ��� [���]
         tmp.EdGate = AutoconMain->Calibration->GetEdGate(dsNone, id + AutoconStartCID, 0);         // ����� ������ ��� [���]
         tmp.PrismDelay = AutoconMain->Calibration->GetPrismDelay(dsNone, id + AutoconStartCID);    // ����� � ������ [10 * ���]
         tmp.TVG = AutoconMain->Calibration->GetTVG(dsNone, id + AutoconStartCID);                  // ��� [���]

         AutoconMain->Rep->SetChannelParams(id + AutoconStartCID, 1, tmp);
                   // ���� � ������ 2 ������ �� ����� ��������� ��� ������� ������
         if (AutoconMain->Table->GetGateCount(id + AutoconStartCID) == 2) {
             tmp.Sens = AutoconMain->Calibration->GetSens(dsNone, id + AutoconStartCID, 1);             // �������� ���������������� [��]
             tmp.Gain = AutoconMain->Calibration->GetGain(dsNone, id + AutoconStartCID, 1);             // ���������� [��]
             tmp.StGate = AutoconMain->Calibration->GetStGate(dsNone, id + AutoconStartCID, 1);         // ������ ����� ��� [���]
             tmp.EdGate = AutoconMain->Calibration->GetEdGate(dsNone, id + AutoconStartCID, 1);         // ����� ������ ��� [���]
             AutoconMain->Rep->SetChannelParams(id + AutoconStartCID, 2, tmp);
         }
    }

    if(frameFlags & MF_EPF_FLAG_SEARCH_TYPE)
    {
        ACConfig->Last_ReportNumber++;
        ACConfig->Last_JointNumber++;
    }
    else if(frameFlags & MF_EPF_FLAG_TEST_TYPE)
    {
        ACConfig->Last_TestReportNumber++;
    }

    UnicodeString filename = str + L".acd";
    if(AutoconMain->Rep->WriteToFile(filename))
        TScreenMessageForm3::InfoMessageW(L"���������", StringFormatU(L"����� ������� �������� � ���� \'%s\'",filename.c_str()).c_str(), 1500);
    else
        TScreenMessageForm3::ErrorMessageW(L"������!", StringFormatU(L"������ ������ ������ � ���� \'%s\'!",filename.c_str()).c_str());
}

void __fastcall TEnterParamFrame::NoRepButtonClick(TObject *Sender)
{
    AutoconMain->SkipRep = true;
    StoreValueListData();
    StartNextFrame();
}
//---------------------------------------------------------------------------

void __fastcall TEnterParamFrame::PageUpBtnClick(TObject *Sender)
{
    if(ValueList->TopRow > 1)
    {
        ValueList->TopRow = std::max(1, (int)(ValueList->TopRow - ValueList->VisibleRowCount));
    }
}
//---------------------------------------------------------------------------

void __fastcall TEnterParamFrame::PageDownBtnClick(TObject *Sender)
{
    if(ValueList->TopRow < ( ValueList->RowCount - ValueList->VisibleRowCount))
    {
        ValueList->TopRow = std::min((int)( ValueList->RowCount - ValueList->VisibleRowCount),
                                        (int)(ValueList->TopRow + ValueList->VisibleRowCount));
    }
}
//---------------------------------------------------------------------------

void __fastcall TEnterParamFrame::ValueListEditButtonClick(TObject *Sender)
{
    switch (ValueList->Selection.Top)
	{
		case 1: //ValueList->Strings->Strings[0] = "���� / ����� ��������=" + DateTimeToStr(Now()); break;
                ValueList->Values["���� / ����� ��������"] = DateTimeToStr(Now()); break;
	}
}
//---------------------------------------------------------------------------


void __fastcall TEnterParamFrame::BitBtn1Click(TObject *Sender)
{
	  ACConfig->MinLenEdit_  = MinLenEdit->Value;
	  ACConfig->MaxSpace_    = MaxSpace->Value   ;
	  ACConfig->DelayDelta_  = DelayDelta->Value ;
	  ACConfig->SameCount_   = SameCount->Value  ;
      ACConfig->UseFiltr_    = FiltrState->Checked;
}
//---------------------------------------------------------------------------

