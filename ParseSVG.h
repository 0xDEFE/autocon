﻿// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

// ---------------------------------------------------------------------------

#ifndef ParseSVGH
#define ParseSVGH

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <string.h>

const NO_MORE_ELEMENTS = 1;
const DOC_IS_NULL = 2;

/** \struct Line_attribute
    Описывает линию
 */
struct Line_attribute
{
	float x1;
	float y1;
	float x2;
	float y2;
	float width;
};

/** \struct Line_attribute
    Описывает прямоугольник
 */
struct Rect_attribute
{
	Rect_attribute()
	{
		transform[0] = transform[3] = 1; //Identity matrix
	};

	char id[32]; //!< Текстовый идентификатор
	float x;     //!< Позиция по горизонтали
	float y;     //!< Позиция по вертикали
	float width;  //!< Ширина
	float height; //!< Высота
	float transform[6]; //!< Матрица трансформации (поворот + смещение + масштаб)
};

/** \struct Line_attribute
    Описывает текст
 */
struct Text_attribute
{
	Text_attribute()
	{
		transform[0] = transform[3] = 1; //Identity matrix
	};

	char id[32];  //!< Текстовый идентификатор
	float x;      //!< Позиция по горизонтали
	float y;      //!< Позиция по вертикали
	wchar_t text[100];  //!< Текст
	int font_size;      //!< Размер шрифта
	float transform[6]; //!< Матрица трансформации (поворот + смещение + масштаб)
};

/** \class PDFPage
    \brief Предназначен для парсинга SVG файла.
 */
class ParseSVG {
public:
	ParseSVG()
	{
    	ppi_scale = 1;
    }

	int start_parse(); 	   //!< парсим xml в doc
	void end_parse();      //!< заканчиваем парсить

	void SetFilename(const char* FN); //!< Установить имя файла
	AnsiString GetFilename();

	int get_rect_attribute(Rect_attribute *ra); //!< Получить следующий узел прямоугольника
	int get_text_attribute(Text_attribute *ta); //!< Получить следующий узел текста
	int get_line_attribute(Line_attribute *la); //!< Получить следующий узел линии
	int get_page_attribute(float* pWidth, float* pHeight); //!< Получить следующий узел страницы

    static void multiplyMatrixVec2(float* matr, float* vec, float* res); //!< Вспомогательная функция для умножения матрицы на вектор

	double ppi_scale; //!< Масштаб в дюймах
protected:
	xmlDocPtr doc;
	AnsiString filename; //!< имя файла который парсим

	xmlNodePtr last_rect_node; //!< ссылка на последний найденный <rect>
	xmlNodePtr last_line_node; //!< ссылка на последний найденный <line>
	xmlNodePtr last_text_node; //!< ссылка на последний найденный <text>
	xmlNodePtr last_tspan_node; //!< ссылка на последний найденный <tspan>

	xmlNodePtr find_child_by_id(xmlNodePtr node, xmlChar *id);
	xmlNodePtr find_child_by_name(xmlNodePtr node, xmlChar *name);

	xmlNodePtr find_layer_node(); //!< находим узел графического слоя
	xmlNodePtr find_layer_child_by_id( xmlChar *id); //!< находим узел "сына" графического слоя по его id
	xmlNodePtr find_page_node();  //!< находем корневой узел svg

	xmlNodePtr find_first_element(xmlChar *name); //!< ищет первый элемент по имени
	xmlNodePtr find_next_element(xmlNodePtr nodePtr); //!< ищет следующий элемент с таким же именем как у nodePtr
	xmlNodePtr find_next_tspan();


};
// ---------------------------------------------------------------------------
#endif
