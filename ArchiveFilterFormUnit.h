/**
 * @file ArchiveFilterFormUnit.h
 * @author Kirill Beliaevskiy
 */
 //---------------------------------------------------------------------------

#ifndef ArchiveFilterFormUnitH
#define ArchiveFilterFormUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <VCLTee.TeCanvas.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <vcl.h>
#include <Vcl.Buttons.hpp>
#include <Vcl.Samples.Spin.hpp>
#include <Vcl.Touch.Keyboard.hpp>
#include <Vcl.ImgList.hpp>

struct sAFDFilterQuery
{
	eADBFieldType fieldType;
	eAFDOpType opType1;
	eAFDOpType opType2;

	UnicodeString findValue1;
	UnicodeString findValue2;
    bool bTwoOp;
};

class cIFilterPanelData
{
public:
//Query
    eADBFieldType fieldType;
	eAFDOpType opType1;
	eAFDOpType opType2;
    bool m_bRanged;

//Controls
    TLabel* m_NameLabel;
    TLabel* m_FirstCtrlLabel;
    TLabel* m_SecondCtrlLabel;
    TGridPanel* m_RangeGrid;
    TGridPanel* m_TitleGrid;
    TButton* m_CloseBtn;
    TPanel* m_Panel;

    virtual void Release() = 0;
    virtual UnicodeString ValToString(int idx) = 0;
    sAFDFilterQuery GetQuery()
    {
        sAFDFilterQuery query;
        query.fieldType = fieldType;
        query.opType1 = opType1;
        query.opType2 = opType2;
        query.bTwoOp = m_bRanged;

        query.findValue1 = ValToString(0);
        if(m_bRanged)
            query.findValue2 = ValToString(1);
        return query;
    };

//Other
protected:
    TWinControl* pParent;
};

template<typename ControlType, typename ValueType>
class cFilterPanelData : public cIFilterPanelData
{
public:
//Controls:
    ControlType* m_FirstCtrl;
    ControlType* m_SecondCtrl;

    void SetValue(int idx, ValueType val);
    ValueType GetValue(int idx);
    UnicodeString ValToString(int idx);

    void Release()
    {
        m_TitleGrid->RemoveControl( m_NameLabel );
        m_TitleGrid->RemoveControl( m_CloseBtn );
        delete m_NameLabel;
        delete m_CloseBtn;

        m_RangeGrid->RemoveControl( m_FirstCtrl );
        m_RangeGrid->RemoveControl( m_FirstCtrlLabel );
        delete m_FirstCtrl;
        delete m_FirstCtrlLabel;

        if(m_bRanged)
        {
            m_RangeGrid->RemoveControl( m_SecondCtrl );
            m_RangeGrid->RemoveControl( m_SecondCtrlLabel );
            delete m_SecondCtrl;
            delete m_SecondCtrlLabel;
        };


        m_Panel->RemoveControl( m_TitleGrid );
        m_Panel->RemoveControl( m_RangeGrid );
        pParent->RemoveControl( m_Panel );

        delete m_TitleGrid;
        delete m_RangeGrid;
        delete m_Panel;
    };

    void Initialize(TWinControl* pParent, bool bRanged, UnicodeString name,
                        int ctrlLabelWidth = 0,
                        UnicodeString firstCtrlLabel = "",
                        UnicodeString secondCtrlLabel = "")
    {
        this->pParent = pParent;
        m_bRanged = bRanged;

        m_Panel = new TPanel(pParent);
        m_Panel->Parent = pParent;
        m_Panel->Width = 100;
        if(m_bRanged)
            m_Panel->Height = 120;
        else
            m_Panel->Height = 80;
        m_Panel->BevelKind = bkFlat;
        m_Panel->BevelOuter = bvNone;

        m_Panel->AlignWithMargins = true;
        m_Panel->ParentBackground = false;
        m_Panel->Color = clGradientInactiveCaption;

        //Title grid
        m_TitleGrid = new TGridPanel(m_Panel);
        //m_Panel->InsertControl(m_TitleGrid);
        m_TitleGrid->Parent = m_Panel;
        m_TitleGrid->Align = alTop;
        m_TitleGrid->AlignWithMargins = true;
        m_TitleGrid->Height = 30;
        m_TitleGrid->RowCollection->Clear();
        m_TitleGrid->ColumnCollection->BeginUpdate();
        m_TitleGrid->ColumnCollection->Clear();
        m_TitleGrid->BevelOuter = bvNone;
        TColumnItem* pColItem = m_TitleGrid->ColumnCollection->Add();
        pColItem->SizeStyle = ssPercent;
        pColItem->Value = 100;
        pColItem = m_TitleGrid->ColumnCollection->Add();
        pColItem->SizeStyle = ssAbsolute;
        pColItem->Value = 50;
        m_TitleGrid->ColumnCollection->EndUpdate();

        //Title label
        m_NameLabel = new TLabel(m_TitleGrid);
        m_TitleGrid->InsertControl(m_NameLabel);
        m_NameLabel->AutoSize = true;
        m_NameLabel->Align = alClient;
        m_NameLabel->Caption = name;

        //Close button
        m_CloseBtn = new TButton(m_TitleGrid);
        m_TitleGrid->InsertControl(m_CloseBtn);
        m_CloseBtn->Align = alClient;
        m_CloseBtn->Caption = "X";

        //Range grid
        m_RangeGrid = new TGridPanel(m_Panel);
        //m_Panel->InsertControl(m_RangeGrid);
        m_RangeGrid->Parent = m_Panel;
        m_RangeGrid->Align = alClient;
        m_RangeGrid->BevelOuter = bvNone;
        m_RangeGrid->AlignWithMargins = true;

        m_RangeGrid->ColumnCollection->Items[0]->SizeStyle = ssAbsolute;
        m_RangeGrid->ColumnCollection->Items[0]->Value = ctrlLabelWidth;
        if(!m_bRanged)
        {
            m_RangeGrid->RowCollection->Items[1]->SizeStyle = ssAbsolute;
            m_RangeGrid->RowCollection->Items[1]->Value = 0;
        }

        //Range controls
        m_FirstCtrl = new ControlType(m_RangeGrid);
        //m_FirstCtrl->Parent = m_RangeGrid;
        m_FirstCtrl->Align = alClient;
        m_FirstCtrlLabel = new TLabel(m_RangeGrid);
        m_FirstCtrlLabel->Caption = firstCtrlLabel;
        m_FirstCtrlLabel->Align = alClient;

        if(m_bRanged)
        {
            m_SecondCtrl = new ControlType(m_RangeGrid);
            //m_SecondCtrl->Parent = m_RangeGrid;
            m_SecondCtrl->Align = alClient;
            m_SecondCtrlLabel = new TLabel(m_RangeGrid);
            m_SecondCtrlLabel->Caption = secondCtrlLabel;
            m_SecondCtrlLabel->Align = alClient;
        }

        m_RangeGrid->InsertControl( m_FirstCtrlLabel );
        m_RangeGrid->InsertControl( m_FirstCtrl );
        //m_FirstCtrl->Parent = m_RangeGrid;

        if(m_bRanged)
        {
            m_RangeGrid->InsertControl( m_SecondCtrlLabel );
            m_RangeGrid->InsertControl( m_SecondCtrl );
            //m_SecondCtrl->Parent = m_RangeGrid;
        };
    };
};

template<> void cFilterPanelData<TDateTimePicker,TDateTime>::SetValue(int idx, TDateTime val) {
    if(!idx) ((TDateTimePicker*)m_FirstCtrl)->DateTime = val; else ((TDateTimePicker*)m_SecondCtrl)->DateTime = val;
}
template<> TDateTime cFilterPanelData<TDateTimePicker,TDateTime>::GetValue(int idx) {
    return (!idx) ? ((TDateTimePicker*)m_FirstCtrl)->DateTime : ((TDateTimePicker*)m_SecondCtrl)->DateTime;
}
template<> UnicodeString cFilterPanelData<TDateTimePicker,TDateTime>::ValToString(int idx) {
    return (!idx) ? DateTimeToStr(((TDateTimePicker*)m_FirstCtrl)->DateTime) : DateTimeToStr(((TDateTimePicker*)m_SecondCtrl)->DateTime);
}

template<> void cFilterPanelData<TEdit,UnicodeString>::SetValue(int idx, UnicodeString val) {
    if(!idx) ((TEdit*)m_FirstCtrl)->Text = val; else ((TEdit*)m_SecondCtrl)->Text = val;
}
template<> UnicodeString cFilterPanelData<TEdit,UnicodeString>::GetValue(int idx) {
    return (!idx) ? ((TEdit*)m_FirstCtrl)->Text : ((TEdit*)m_SecondCtrl)->Text;
}
template<> UnicodeString cFilterPanelData<TEdit,UnicodeString>::ValToString(int idx) {
    return GetValue(idx);
}

template<> void cFilterPanelData<TSpinEdit,int>::SetValue(int idx, int val) {
    if(!idx) ((TSpinEdit*)m_FirstCtrl)->Value = val; else ((TSpinEdit*)m_SecondCtrl)->Value = val;
}
template<> int cFilterPanelData<TSpinEdit,int>::GetValue(int idx) {
    return (!idx) ? ((TSpinEdit*)m_FirstCtrl)->Value : ((TSpinEdit*)m_SecondCtrl)->Value;
}
template<> UnicodeString cFilterPanelData<TSpinEdit,int>::ValToString(int idx) {
    return (!idx) ? IntToStr(((TSpinEdit*)m_FirstCtrl)->Value) : IntToStr(((TSpinEdit*)m_SecondCtrl)->Value);
}

template<> void cFilterPanelData<TComboFlat,UnicodeString>::SetValue(int idx, UnicodeString val) {
    if(!idx) ((TComboFlat*)m_FirstCtrl)->Items->SetText(val.c_str()); else ((TComboFlat*)m_SecondCtrl)->Items->SetText(val.c_str());
}
template<> UnicodeString cFilterPanelData<TComboFlat,UnicodeString>::GetValue(int idx) {
    return (!idx) ? ((TComboFlat*)m_FirstCtrl)->CurrentItem() : ((TComboFlat*)m_SecondCtrl)->CurrentItem();
}
template<> UnicodeString cFilterPanelData<TComboFlat,UnicodeString>::ValToString(int idx) {
    return GetValue(idx);
}

class cIFlowBtn
{
public:
    cIFlowBtn()
    {
        pButton = NULL;
    };

    UnicodeString name;
    eADBFieldType fieldType;
	eAFDOpType opType1;
	eAFDOpType opType2;

    bool bTwoOp;
    TBitBtn* pButton;
};

template<typename ControlType, typename ValueType>
class cFlowBtnData : public cIFlowBtn
{
public:
    cFilterPanelData<ControlType,ValueType>* CreateValuePanel(TWinControl* pParent)
    {
        cFilterPanelData<ControlType,ValueType>* pPanel = new cFilterPanelData<ControlType,ValueType>();

        if(bTwoOp)
            pPanel->Initialize(pParent, bTwoOp, ADBFieldTypeToName(fieldType), 40, L"��:", L"��:");
        else
            pPanel->Initialize(pParent, bTwoOp, ADBFieldTypeToName(fieldType), 40, AFDOpTypeToName(opType1), L"");

        pPanel->fieldType = fieldType;
        pPanel->opType1 = opType1;
        pPanel->opType2 = opType2;


        pPanel->SetValue(0,val1);
        if(bTwoOp) pPanel->SetValue(1,val2);
        return pPanel;
    }

    ValueType val1;
    ValueType val2;
};



//---------------------------------------------------------------------------
class TArchiveFilterForm : public TForm
{
__published:	// IDE-managed Components
    TScrollBox *m_PanelsScroll;
    TBitBtn *m_AddBtn;
    TPageControl *PageControl1;
    TTabSheet *ListPage;
    TTabSheet *SelectPage;
    TFlowPanel *m_FiltersFlowPanel;
    TPanel *Panel1;
    TTimer *DeleteLaterTimer;
    TTouchKeyboard *TouchKeyboard1;
    TBitBtn *DiscardFilterBtn;
    TBitBtn *ClearBtn;
    TBitBtn *ApplyFilterBtn;
    TLabel *Label1;
    TImageList *ImageList1;
    TPanel *Panel2;
    TPanel *Panel3;
    TPanel *Panel4;
    TLabel *Label2;
    TPanel *Panel5;
    void __fastcall m_AddBtnClick(TObject *Sender);
    void __fastcall DeleteLaterTimerTimer(TObject *Sender);
    void __fastcall ClearBtnClick(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
private:	// User declarations
    template<typename MyCtrl,typename MyType>
    int CreateFlowBtnOneOp(UnicodeString name, eADBFieldType fieldType,
                                        eAFDOpType opType1, MyType value)
    {
        cFlowBtnData<MyCtrl,MyType>* pData = new cFlowBtnData<MyCtrl,MyType>();
        pData->name = name;
        pData->val1 = value;
        pData->fieldType = fieldType;
        pData->opType1 = opType1;
        pData->bTwoOp = false;
        pData->pButton = NULL;
        flowButtonData.push_back(pData);
        return flowButtonData.size()-1;
    }

    template<typename MyCtrl,typename MyType>
    int CreateFlowBtnTwoOp(UnicodeString name, eADBFieldType fieldType,
                                        eAFDOpType opType1, MyType value1,
                                        eAFDOpType opType2, MyType value2)
    {
        cFlowBtnData<MyCtrl,MyType>* pData = new cFlowBtnData<MyCtrl,MyType>();
        pData->name = name;
        pData->val1 = value1;
        pData->val2 = value2;
        pData->fieldType = fieldType;
        pData->opType1 = opType1;
        pData->opType2 = opType2;
        pData->bTwoOp = true;
        pData->pButton = NULL;
        flowButtonData.push_back(pData);
        return flowButtonData.size()-1;
    }

    void addValuePanel( cIFilterPanelData* pPanel );

    TBitBtn* createFlowBtn(UnicodeString name, int tag);
    void removeFlowBtn(TBitBtn* pBtn);

    void __fastcall OnClosePanelBtnClicked(TObject *Sender);
    void __fastcall OnServiceFlowBtnClicked(TObject *Sender);
    void __fastcall OnFieldFlowBtnClicked(TObject *Sender);
    void generateFlowButtons(eADBFieldType fieldType);
    void removeFlowButtons();

    void flushDeleteLater();

    TBitmap* pBackBmp;
    TBitmap* pCancelBmp;

    std::vector<cIFlowBtn*> flowButtonData;
    std::vector<TBitBtn*> flowButtons;
    std::list<TBitBtn*> deleteLaterFlowBtnValues;
    std::vector<cIFilterPanelData*> currValuePanels;
    std::list<int> deleteLaterValuePanelIndices;

public:		// User declarations
    __fastcall TArchiveFilterForm(TComponent* Owner);

    bool getQueries(std::vector<sAFDFilterQuery>& query_array);
};
//---------------------------------------------------------------------------
extern PACKAGE TArchiveFilterForm *ArchiveFilterForm;
//---------------------------------------------------------------------------
#endif
