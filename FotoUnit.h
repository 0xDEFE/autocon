//---------------------------------------------------------------------------

#ifndef FotoUnitH
#define FotoUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <TChar.h>
#include <jpeg.hpp>
#include <ExtCtrls.hpp>
#include "IPCameraUnit.h"
#include <Vcl.Buttons.hpp>;
#include "ConfigUnit.h";

//---------------------------------------------------------------------------
class TFotoForm : public TForm
{
__published:	// IDE-managed Components
    TPanel *Panel1;
    TLabel *Label1;
    TComboBox *Edit1;
    TImage *Image1;
    TPanel *Panel2;
    TBitBtn *BackButton;
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall BackButtonClick(TObject *Sender);
    void __fastcall Edit1Change(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TFotoForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFotoForm *FotoForm;
//---------------------------------------------------------------------------
extern cACConfig * ACConfig;

#endif
