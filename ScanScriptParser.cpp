//---------------------------------------------------------------------------

//#pragma hdrstop

#include "AutoconMain.h"
extern cAutoconMain *AutoconMain;

#include "ScanScriptParser.h"
#include <utility>
#include <stdlib.h>
#include "Utils.h"




//---------------------------------------------------------------------------
//#pragma package(smart_init)


std::string sScanScriptError::toString()
{
	return StringFormatTpl<std::string,char>("[ERROR%d] Line %d: %s\r\n",code,line,desc.c_str());
}

std::string sScanScriptErrorBuffer::toString()
{
	std::string str;
	for(unsigned int j=0;j<errors.size();j++)
	{
		str+=errors[j].toString();
	}
	return str;
}

//--------------Parser utilites--------------------------

const char* getLexName(eLexType lex)
{
	switch(lex)
	{
	case LEXEM_UNKNOWN: return "LEXEM_UNKNOWN";
	case LEXEM_NUMBER: return "LEXEM_NUMBER";
	case LEXEM_NAME: return "LEXEM_NAME";
	case LEXEM_COLON: return "LEXEM_COLON";
	case LEXEM_ENDL: return "LEXEM_ENDL";
	case LEXEM_COMMA: return "LEXEM_COMMA";
	case LEXEM_COMMENT: return "LEXEM_COMMENT";
	}
	return "Unknown";
}

bool isDigit(char c)
{
	if((c>='0') && (c<='9'))
		return true;
	return false;
}

bool isNumber(char c)
{
	if(isDigit(c)) return true;
	if((c=='+') || (c=='-'))
		return true;
	return false;
}

bool isHex(char c)
{
	if(isNumber(c)) return true;
	if((c=='x') || ((c>='A') && (c<='F')) || ((c>='a') && (c<='f')))
		return true;
	return false;
}

bool isSymbol(char c)
{
	if((c>='A') && (c<='Z'))
		return true;
	if((c>='a') && (c<='z'))
		return true;
    if(c == '_')
        return true;
	return false;
}

//finds next lexem in codeline
const char* getLexem(const char* line, const char* endln, UINT* pSize, eLexType* pLexType)
{
	const char* pStart = line;
	while(strchr(" \r\n\t",*pStart) && (pStart<endln))
		pStart++;

	const char* pCur = pStart;

	char prevc = 0;
	UINT lexType = 0;
	while(pCur<=endln)
	{
		char c=*pCur;

		if((c=='/') && (prevc=='/'))//comment
		{
			*pLexType = LEXEM_COMMENT;
			*pSize =  2;
			return pStart;
		}
		else if(c=='#')//comment
		{
			*pLexType = LEXEM_COMMENT;
			*pSize =  1;
			return pStart;
		}
		else if(c==':')
		{
        	*pLexType = LEXEM_COLON;
			*pSize =  1;
			return pStart;
        }
		else if(c==';')//end of statement
		{
			*pLexType = LEXEM_ENDL;
			*pSize =  1;
			return pStart;
		}
		else if(c==',')//argument divider
		{
			*pLexType = LEXEM_COMMA;
			*pSize =  1;
			return pStart;
		}
		else if(isNumber(c))//number (0-9,+,-,0x)
		{
			while((isNumber(*pCur) || isHex(*pCur)) && (pCur<=endln))
			{
				pCur++;
			}

			*pLexType = LEXEM_NUMBER;
			*pSize =  (pCur - pStart);
			return pStart;
		}
		else if(isSymbol(c))//name
		{
			while((isSymbol(*pCur)) && (pCur<=endln))
			{
				pCur++;
			}

			*pLexType = LEXEM_NAME;
			*pSize =  (pCur - pStart);
			return pStart;
		}
		prevc=c;
		pCur++;
	}
	*pLexType = LEXEM_UNKNOWN;
	*pSize = (pCur - pStart)+1;
	return line;
}

//-------------------------------------------------------------

void ScanScriptMessageOut(std::ostream* pOutputStream,sScanScriptErrorBuffer* pErrBuf,
													const char* string, int code, int line)
{
	if(pOutputStream)
	{
		(*pOutputStream)<<"-> ";
		if(code!=-1)
			(*pOutputStream)<<"[ERROR] ";
		if(line!=-1)
			(*pOutputStream)<<"Line "<<line<<": ";
		(*pOutputStream)<<string<<"\r\n";
	}
	if(pErrBuf)
		pErrBuf->add(line,code,string);
	OutputDebugStringA(string);
    UnicodeString ustr = string;
    if(code!=-1)
    {
    	LOGERROR("%s",ustr.c_str());
    	TRACE("%s",ustr.c_str());
    }
    else
        LOGINFO("%s",ustr.c_str());
}

//-------------------------PARSER-------------------------------

int cScanScriptParser::ParseFile(const char* path, cScanScriptCmdBuffer* pCmdBuffer,
									std::ostream* pOutputStream, sScanScriptErrorBuffer* pErrBuff)
{
	FILE *stream;

    ScanScriptMessageOut(pOutputStream,pErrBuff,
			StringFormatStdA("Compiling file: %s",path).c_str(),-1,-1);

	stream = fopen( path, "rb" );
	if(!stream)
	{
		ScanScriptMessageOut(pOutputStream,pErrBuff,
			StringFormatStdA("Failed to open file: %s",path).c_str(),0,-1);
		return -1;
	}

	fseek(stream,0,SEEK_END);//seek to end
	long fsize = ftell(stream);//get file size
	fseek(stream,0,SEEK_SET);//seek to begin

	char* codeBuff = new char[fsize + 1];
	if(!fread(codeBuff,1,fsize,stream))
	{
		ScanScriptMessageOut(pOutputStream,pErrBuff,
			StringFormatStdA("Failed to read file: %s",path).c_str(),0,-1);
		delete [] codeBuff;
		return -1;
	}
	fclose(stream);
    codeBuff[fsize] = NULL;

	int ret = Parse(codeBuff,pCmdBuffer,pOutputStream,pErrBuff);

    if(ret == 0)
    {
        ScanScriptMessageOut(pOutputStream,pErrBuff,
                StringFormatStdA("Compiling file %s complete!",path).c_str(),-1,-1);
    }
    else
    {
        ScanScriptMessageOut(pOutputStream,pErrBuff,
                StringFormatStdA("Compiling file %s FAILED!",path).c_str(),-1,-1);
    }

	delete [] codeBuff;
	return ret;
}

int cScanScriptParser::Parse(const char* code, cScanScriptCmdBuffer* pCmdBuffer,
								std::ostream* pOutputStream, sScanScriptErrorBuffer* pErrBuff)
{
	int codeLen = strlen(code);
    ScanScriptMessageOut(pOutputStream,pErrBuff,
			StringFormatStdA("Compiling code: %d bytes",codeLen).c_str(),-1,-1);

	int first = 0;
	int linecnt = 0;

	if(pErrBuff)
		pErrBuff->errors.clear();
	pCmdBuffer->clear();
	//parsing every line
	for(int i=0;i<codeLen;i++)
	{
		if((code[i]=='\n') || (i==(codeLen-1)))
		{
			int ret = _parseLine(linecnt,code+first,i-first,pCmdBuffer,pOutputStream,pErrBuff);
            if(ret != 0)
            {
                ScanScriptMessageOut(pOutputStream,pErrBuff,
			        StringFormatStdA("Compiling code FAILED!",codeLen).c_str(),-1,-1);
                return ret;
            }
			first = i+1;
			linecnt++;
        }
	}
    ScanScriptMessageOut(pOutputStream,pErrBuff,
			StringFormatStdA("Compiling code complete!",codeLen).c_str(),-1,-1);
    return 0;
}

// Parse line of code and return result
// line - string to parse
// count - line size
// code - array to store bytecode
// pErrBuff - pointer to err buffer
int cScanScriptParser::_parseLine(UINT lineCnt, const char* line, UINT count,
				cScanScriptCmdBuffer* pCmdBuffer,
				std::ostream* pOutputStream,
				sScanScriptErrorBuffer* pErrBuff)
{
	if(!pCodeDecl)
		return -1;
	std::vector<sScanScriptLexeme> lexems;

	const char* pStart = line;
	const char* pEnd = line + count;

	std::vector<BYTE>& code = pCmdBuffer->bytecode;
	std::list<sScanScriptEntryPoint>* entryPoints = &(pCmdBuffer->entryPoints);

	while(pStart<=pEnd) //collecting lexems
	{
		UINT lexSize;
		eLexType lexType;
		const char* ret = getLexem(pStart,pEnd,&lexSize,&lexType);

		pStart = ret+(lexSize);

		if(lexType==LEXEM_COMMENT) //if connect - skip line
			break;
		if(lexType==-1) //if unknown symbol - go to next symbol
		{
			pStart++;
			continue;
		}
		sScanScriptLexeme lex;
		lex.type = lexType;
		lex.start = ret-line;
		lex.size = lexSize;
		lexems.push_back(lex);
	}
	if(lexems.size()==0)
		return 0;

	sScanScriptLexeme lex;
	lex.type = LEXEM_ENDL;
	lex.start=0;
	lex.size=0;
	lexems.push_back(lex); //endline lexeme at line end


	//temorary parsing variables
	//int curCmdIndex = -1;
	std::vector<int> curCmdArguments;

	const char* tmpNumberName;
	int tmpNumber = 0;
	int tmpNumberBase = 0;
	char * pStrtolEnd;

	UINT tmpFuncNameSize = 0;
	const char* tmpFuncName;
	int tmpFuncId = 0;
	int tmpFuncArgc = 0;
	bool bNewFunc = false;

	eLexType prevLex = LEXEM_ENDL;

	//converting to bytecode
	for(UINT i=0;i<lexems.size();i++)
	{
		bool bError = false;
		std::string error_text;

		//TRACE("Lexem: %s\r\n", getLexName(lexems[i].type));

		eLexType curLex = lexems[i].type;
		std::string curLexName(line+lexems[i].start,lexems[i].size);

		switch(curLex)
		{
			case LEXEM_UNKNOWN:
				error_text =
					StringFormatStdA("Unknown lexem: %s [%s]",
						curLexName.c_str(),getLexName(lexems[i].type));
				bError = true;
				break;
			case LEXEM_NAME:
				if(prevLex!=LEXEM_ENDL)
				{
					error_text =
					StringFormatStdA("Unknown statement: %s [%s]",
						curLexName.c_str(),getLexName(lexems[i].type));
					bError = true;
					break;
				}

				tmpFuncName = line + lexems[i].start;
				tmpFuncNameSize = lexems[i].size;
				tmpFuncId = -1;
				for(UINT j=0;j<pCodeDecl->commandsDecl.size();j++)
				{
					if(strlen(pCodeDecl->commandsDecl[j].name) != lexems[i].size)
						continue;
					if(memcmp(pCodeDecl->commandsDecl[j].name,tmpFuncName,lexems[i].size))
						continue;
					tmpFuncId = pCodeDecl->commandsDecl[j].code;
					tmpFuncArgc = pCodeDecl->commandsDecl[j].argc;
					break;
				}
				bNewFunc = true;
				break;
			case LEXEM_NUMBER:
				if((prevLex!=LEXEM_NAME) && (prevLex!=LEXEM_COMMA))
				{
					error_text =
					StringFormatStdA("Unknown statement: %s [%s]",
						curLexName.c_str(),getLexName(lexems[i].type));
					bError = true;
					break;
				}

				tmpNumber = 0;
				tmpNumberBase = 10;

				tmpNumberName = line + lexems[i].start;
				if((lexems[i].size>2) && (tmpNumberName[0]=='0') && (tmpNumberName[1]=='x'))
					tmpNumberBase = 16;

				tmpNumber = strtol(tmpNumberName,&pStrtolEnd,tmpNumberBase);
                if((pStrtolEnd - tmpNumberName) != (int)lexems[i].size)
                {
                    error_text =
					StringFormatStdA("Failed to convert lexeme \"%s\" to number",
						curLexName.c_str());
					bError = true;
                }
				curCmdArguments.push_back(tmpNumber);
				break;
			case LEXEM_COLON:
				if((prevLex!=LEXEM_NAME) && (prevLex!=LEXEM_NUMBER))
				{
					error_text =
					StringFormatStdA("Unknown statement: %s [%s]",
						curLexName.c_str(),getLexName(lexems[i].type));
					bError = true;
					break;
				};

				if(bNewFunc)
				{
                 	if(tmpFuncId!=-1)
					{
						error_text =
						StringFormatStdA("Entry point or function?: %s [%s]",
							std::string(tmpFuncName,tmpFuncNameSize).c_str(),getLexName(lexems[i].type));
						bError = true;
						bNewFunc = false;
						break;
					}

					sScanScriptEntryPoint ep;
					ep.name = std::string(tmpFuncName,tmpFuncNameSize);

					for(UINT i=0;i<curCmdArguments.size();i++)
					{
						ep.arguments.push_back(curCmdArguments[i]);
					}
					ep.codeOffset = code.size();
					(*entryPoints).push_back(ep);

					tmpFuncName=NULL;
					bNewFunc=false;
					tmpFuncId = -1;
				}
				else
				{
					error_text =
					StringFormatStdA("No entry point name!");
					bError = true;
					break;
                }
				break;
			case LEXEM_ENDL: //bytecode adds here
				if(bNewFunc)
				{
					if(tmpFuncId==-1)
					{
						error_text =
						StringFormatStdA("Unknown function: %s [%s]",
							std::string(tmpFuncName,tmpFuncNameSize).c_str(),getLexName(lexems[i].type));
						bError = true;
						bNewFunc=false;
						break;
					}

					if(((int)curCmdArguments.size()!=tmpFuncArgc) && (tmpFuncArgc!=0xFFFF))
					{
                    	error_text =
							StringFormatStdA("Wrong argument count: %s (%d/%d)",
								std::string(tmpFuncName,tmpFuncNameSize).c_str(),
								curCmdArguments.size(),tmpFuncArgc);
							bError = true;
							break;
					}

                    //Add line info
                    pCmdBuffer->pushBreak(lineCnt);

					for(UINT i=0;i<curCmdArguments.size();i++)
					{
                        //ScanScriptMessageOut(pOutputStream,pErrBuff,
                    	  //	StringFormatStdA(" -- PUSH NUMBER: %d",curCmdArguments[i]).c_str(),-1,-1);

                        pCmdBuffer->pushStack(curCmdArguments[i]);
					}

                    //Push argument count
                    pCmdBuffer->pushStack(curCmdArguments.size());

					std::string str;
					for(UINT j=0;j<tmpFuncNameSize;j++)
						str+=tmpFuncName[j];
                    //ScanScriptMessageOut(pOutputStream,pErrBuff,
                    //			StringFormatStdA(" -- CALL: %s[%d]",str.c_str(),tmpFuncId).c_str(),-1,-1);

					if(curLex==LEXEM_ENDL)
						code.push_back(SSP_BYTECODE_CALL);//command
					else if(curLex==LEXEM_COLON)
                        code.push_back(SSP_BYTECODE_ENTRY_POINT);//entry point
					code.push_back(2);                //call index size
					code.push_back(((BYTE*)&tmpFuncId)[0]);
					code.push_back(((BYTE*)&tmpFuncId)[1]);

					tmpFuncName=NULL;
					bNewFunc=false;
					tmpFuncId = -1;
					curCmdArguments.clear();
				}
				break;
			default:
				break;
		};
		prevLex = curLex;

		if(bError)
		{
			error_text = std::string(line,count)+"\r\n"+error_text;
			ScanScriptMessageOut(pOutputStream,pErrBuff,error_text.c_str(),0,lineCnt);
			return -1;
		}
	}
	return 0;
}

void sScanScriptCodeDecl::DeclareCmd(const char* name,UINT id,UINT argc)
{
	sScanScriptCmdDecl decl;
	decl.name = name;
	decl.code = id;
    decl.argc = argc;
	commandsDecl.push_back(decl);
}

//-------------------------LOAD FNCTNS--------------------------------------

bool LoadScanScriptFromFile(const char* path, sScanScriptCodeDecl* pCodeDecl, cScanScriptCmdBuffer* pCmdBuffer, std::ostream* pOutputStream, sScanScriptErrorBuffer* pErrBuff)
{
	cScanScriptParser parser;
	parser.SetCodeDecl(pCodeDecl);

	int ret = parser.ParseFile(path,pCmdBuffer,pOutputStream,pErrBuff);
	return ret==0;
}

bool LoadScanScriptFromMemory(const char* code, sScanScriptCodeDecl* pCodeDecl, cScanScriptCmdBuffer* pCmdBuffer, std::ostream* pOutputStream, sScanScriptErrorBuffer* pErrBuff)
{
	cScanScriptParser parser;
	parser.SetCodeDecl(pCodeDecl);

	int ret = parser.Parse(code,pCmdBuffer,pOutputStream,pErrBuff);
	return ret==0;
}

//-------------------------CMD BUFFER---------------------------------------

void cScanScriptCmdBuffer::pushCommand(int cmd,int argc,...)
{
	va_list args;
	va_start(args, argc);

	for(int i = 0; i < argc; i++)
	{
		int curArg = va_arg(args,int);
		pushStack( curArg );
    }

	va_end(args);

    //Push argument count
    pushStack(argc);

	pushCall(cmd);
}

void cScanScriptCmdBuffer::pushCall(int cmd)
{
	bytecode.push_back(SSP_BYTECODE_CALL);//command
	bytecode.push_back(2);                //call index size
	bytecode.push_back(((BYTE*)&cmd)[0]);
	bytecode.push_back(((BYTE*)&cmd)[1]);
}

void cScanScriptCmdBuffer::pushStack(int value)
{
	bytecode.push_back(SSP_BYTECODE_STACK_PUSH);//command
	bytecode.push_back(4);				 //arg 1 - size
	//arg 2 - variable
	bytecode.push_back(((BYTE*)&value)[0]);
	bytecode.push_back(((BYTE*)&value)[1]);
	bytecode.push_back(((BYTE*)&value)[2]);
	bytecode.push_back(((BYTE*)&value)[3]);
}

void cScanScriptCmdBuffer::pushEP(USHORT value)
{
	bytecode.push_back(SSP_BYTECODE_ENTRY_POINT);//ep
	bytecode.push_back(2);                //call index size
	bytecode.push_back(((BYTE*)&value)[0]);
	bytecode.push_back(((BYTE*)&value)[1]);
}

void cScanScriptCmdBuffer::pushEntryPoint(const char* name,int argc,...)
{
    sScanScriptEntryPoint ep;
	ep.name = name;
	ep.codeOffset = bytecode.size();
	USHORT epNum = entryPoints.size();
	entryPoints.push_back(ep);

	va_list args;
	va_start(args, argc);
	for(int i = 0; i < argc; i++)
	{
		int curArg = va_arg(args,int);
		ep.arguments.push_back(curArg);
	}
	va_end(args);

	pushEP( epNum );
}

void cScanScriptCmdBuffer::pushBreak(unsigned short line)
{
    if(line >= (int)lineToCodeInfo.size())
        lineToCodeInfo.resize(line+1, -1);

    if(lineToCodeInfo[line] == -1)
    {
        lineToCodeInfo[line] = bytecode.size();

        bytecode.push_back(SSP_BYTECODE_DEBUG_BREAK);
        bytecode.push_back(((BYTE*)&line)[0]);
        bytecode.push_back(((BYTE*)&line)[1]);
    }
}

void cScanScriptCmdBuffer::clear()
{
	bytecode.clear();
	entryPoints.clear();
    lineToCodeInfo.clear();
}

//-------------------------SCAN SCRIPT PRG----------------------------------

bool cScanScriptProgramm::createFromFile(const char* path)
{
	release();
	return LoadScanScriptFromFile(path,&codeDecl, &cmdBuffer,&outputStream,&errBuf);
}
bool cScanScriptProgramm::createFromMemory(const char* code)
{
	release();
	return LoadScanScriptFromMemory(code,&codeDecl, &cmdBuffer,&outputStream,&errBuf);
}
bool cScanScriptProgramm::createFromCode(cScanScriptCmdBuffer& cmdBuffer)
{
	release();
	this->cmdBuffer = cmdBuffer;
	return true;
}
void cScanScriptProgramm::release()
{
	cleanup();
	cmdBuffer.clear();
    debugBreaks.clear();
}

void cScanScriptProgramm::cleanup()
{
	IP = 0;
	while(stack.size())
		stack.pop();
	errBuf.errors.clear();
}
eScanScriptRetCode cScanScriptProgramm::executeAll()
{
	if(!isValid())
		return SSRC_RUNTIME_ERROR;

	cleanup();

	UINT IPPrev = 9999;
    eScanScriptRetCode excode;
	while((excode = executeNext()) == SSRC_RUNNING)
	{
		if(IP == IPPrev)//test
			return excode;
		IPPrev = IP;
	}
	return excode;
}
eScanScriptRetCode cScanScriptProgramm::executeNext()
{
	if(!isValid())
    {
        ScanScriptMessageOut(&outputStream,&errBuf,
			StringFormatStdA("Runtime error: Script is not valid").c_str(),0,getCurrentLine());
		return SSRC_RUNTIME_ERROR;
    }

	if(IP >= cmdBuffer.bytecode.size())
    {
		return SSRC_END_PROGRAMM;
    }

	BYTE code = cmdBuffer.bytecode[IP++];
	BYTE arg1;
    eScanScriptRetCode excode;

    try
    {
        switch(code)
        {
            case SSP_BYTECODE_NOOP:
                break;
            case SSP_BYTECODE_STACK_PUSH:
                arg1 = cmdBuffer.bytecode[IP++];//size, bytes
                pushStack(&(cmdBuffer.bytecode[IP]),arg1);
                IP+=arg1;
                break;
            case SSP_BYTECODE_STACK_POP:
                break;
            case SSP_BYTECODE_CALL:
                arg1 = cmdBuffer.bytecode[IP++];//func id size
                if(arg1==2)
                {
                    excode = executeCmd(*(USHORT*)(&(cmdBuffer.bytecode[IP])));
                    if(excode != SSRC_RUNNING)
                    {
                        IP+=arg1;
                        return excode;
                    }
                }
                IP+=arg1;
                break;
            case SSP_BYTECODE_ENTRY_POINT:
                break;
            case SSP_BYTECODE_DEBUG_BREAK:
                unsigned short breakpLine = 0;
                memcpy(&breakpLine, &(cmdBuffer.bytecode[IP]),sizeof(unsigned short));
                IP+=2;
                if(((int)breakpLine < (int)debugBreaks.size()) && debugBreaks[breakpLine])
                    return SSRC_DEBUG_BREAK;
                if((breakNextLine != -1) && (breakpLine != breakNextLine))
                {
                    breakNextLine = -1;
                    return SSRC_SYSCODE_ENDP;
                }
                break;
        }
    }
    catch(...)
    {
        ScanScriptMessageOut(&outputStream,&errBuf,
			StringFormatStdA("Runtime error: Exception").c_str(),0,getCurrentLine());
        return SSRC_RUNTIME_ERROR;
    }
	return SSRC_RUNNING;
}

eScanScriptRetCode cScanScriptProgramm::executeLine()
{
    breakNextLine = IPToLine(IP);

    eScanScriptRetCode excode;
	while((excode = executeNext()) == SSRC_RUNNING)
	{

    }
    if(excode == SSRC_SYSCODE_ENDP)
        return SSRC_RUNNING;
    return excode;
}

int cScanScriptProgramm::getCurrentLine()
{
    return IPToLine(IP);
}

int cScanScriptProgramm::getTotalLines()
{
    return cmdBuffer.lineToCodeInfo.size();
}

int cScanScriptProgramm::IPToLine(unsigned int ip_)
{
    std::vector<int>& lineInfo = cmdBuffer.lineToCodeInfo;
    int lasti = 0;
    for(unsigned int i = 0; i < lineInfo.size(); i++)
    {
        if(lineInfo[i] == -1)
            continue;
        if(lineInfo[i] < (int)ip_)
        {
            lasti = i;
            continue;
        }

        if(lineInfo[i] == (int)ip_)
            return i;

       return lasti;
    }
    return lasti;
}

int cScanScriptProgramm::LineToIP(unsigned int line)
{
    std::vector<int>& lineInfo = cmdBuffer.lineToCodeInfo;
    if(line >= lineInfo.size())
        return -1;

    return lineInfo[line];
}

void cScanScriptProgramm::insertBreakpoint(unsigned int line)
{
    if(line >= debugBreaks.size())
        debugBreaks.resize(line+1,false);

    debugBreaks[line] = true;
}

void cScanScriptProgramm::removeBreakpoint(unsigned int line)
{
    if(line >= debugBreaks.size())
        return;
    debugBreaks[line] = false;
}

bool cScanScriptProgramm::isValid()
{
	if(cmdBuffer.bytecode.size() == 0)
		return false;
	if(IP > cmdBuffer.bytecode.size())
		return false;
	return true;
}
UINT cScanScriptProgramm::getInstructionPointer()
{
	return IP;
}
UINT cScanScriptProgramm::getCodeSize()
{
	return cmdBuffer.bytecode.size();
}

bool cScanScriptProgramm::pushStack(void* pData, UINT size)
{
	for(UINT i=0;i<size;i++)
	{
		stack.push(((BYTE*)pData)[i]);
	}
	return true;
}

bool cScanScriptProgramm::popStack(void* pData, UINT size)
{
	if(stack.size()<size)
    {
        ScanScriptMessageOut(&outputStream,&errBuf,
			StringFormatStdA("Runtime error: stack pop failed!").c_str(),0,getCurrentLine());
		return false;
    }
	for(int i=size-1;i>=0;i--)
	{
		((BYTE*)pData)[i] = stack.top();
		stack.pop();
	}
	return true;
}

bool cScanScriptProgramm::setEntryPoint(bool bSetNext,UINT argc,const char* name, int index)
{
	if(!isValid())
		return false;
	cleanup();

	std::vector<int> args;
	args.push_back(index);

	//cScanScriptCmdBuffer* pCmdBuffer = getContext()->pCmdBuffer;


	sScanScriptEntryPoint* pFoundData = NULL;
	std::list<sScanScriptEntryPoint>::iterator it = cmdBuffer.entryPoints.begin();
	int j=0;
	for(it;it!=cmdBuffer.entryPoints.end();it++,j++)
	{
		if((*it).arguments.size()!=argc)
			continue;
		if(bSetNext)
		{
			if((*it).codeOffset <= IP)
				continue;
		}

		for(unsigned int i=0;i<args.size();i++)
		{
			if( (*it).arguments[i] != args[i])
				break;
			pFoundData = &(*it);
		}
	}


	if(pFoundData!=NULL)
	{
		IP = pFoundData->codeOffset;
	}
	else
	{
		TRACE("Unknown entry point: %s\r\n",name);
	}

	return pFoundData!=NULL;
}

sScanScriptErrorBuffer& cScanScriptProgramm::getLastError()
{
	return errBuf;
}
cScanScriptCmdBuffer& cScanScriptProgramm::getCode()
{
	return cmdBuffer;
}

std::ostringstream& cScanScriptProgramm::getOutputStream()
{
	return outputStream;
}

eScanScriptRetCode cScanScriptProgramm::handleCmd(USHORT cmdCode, UINT argc)
{
    UINT arg1, arg2, arg3;
	sScanChannelDescription scanChanDesc;
	unsigned int j = 0;

	bool bCaretActive;
	int iCaretCoord;

	switch(cmdCode)
	{
		case SSC_MOVCARET:
			popStack(&arg2,4);
			popStack(&arg1,4);

			AutoconMain->ac->CaretManualMove(arg1, arg2);
			break;
		case SSC_MOVTOBASE:
			AutoconMain->ac->MoveCaretsToStart();
			break;
		case SSC_SETSG:
			popStack(&arg1,4);
			AutoconMain->DEV->SetChannelGroup(arg1);
			AutoconMain->DEV->Update(false);
			break;
		case SSC_SETSGSENS:
		case SSC_SETSGGAIN:
		case SSC_SETSGPRISM:
			popStack(&arg2,4);
			popStack(&arg1,4);

			for(j=0;j<AutoconMain->Config->GetAllScanChannelsCount();j++)
			{
				if(!AutoconMain->Config->getSChannelbyIdx(j, &scanChanDesc))
					continue;
				if(arg1 == (unsigned int)scanChanDesc.StrokeGroupIdx)
				{
					if(cmdCode==SSC_SETSGSENS)
						AutoconMain->Calibration->SetSens(scanChanDesc.DeviceSide,
													scanChanDesc.Id,
													0,
													arg2);
					else if(cmdCode==SSC_SETSGGAIN)
						AutoconMain->Calibration->SetGain(scanChanDesc.DeviceSide,
													scanChanDesc.Id,
													0,
													arg2);
					else if(cmdCode==SSC_SETSGPRISM)
						AutoconMain->Calibration->SetPrismDelay(scanChanDesc.DeviceSide,
													scanChanDesc.Id,
													arg2);
				}
			}
			AutoconMain->DEV->Update(false);
			break;
        case SSC_SETCH:
			popStack(&arg1,4);
			AutoconMain->DEV->SetChannel(arg1);
			AutoconMain->DEV->Update(false);
			break;
		case SSC_SETCHSENS:
		case SSC_SETCHGAIN:
		case SSC_SETCHPRISM:
			popStack(&arg2,4);
			popStack(&arg1,4);

			if(AutoconMain->Config->getFirstSChannelbyID(arg1, &scanChanDesc) == -1)
					break;

			if(cmdCode==SSC_SETCHSENS)
				AutoconMain->Calibration->SetSens(scanChanDesc.DeviceSide,
											scanChanDesc.Id,
											0,
											arg2);
			else if(cmdCode==SSC_SETCHGAIN)
				AutoconMain->Calibration->SetGain(scanChanDesc.DeviceSide,
											scanChanDesc.Id,
											0,
											arg2);
			else if(cmdCode==SSC_SETCHPRISM)
				AutoconMain->Calibration->SetPrismDelay(scanChanDesc.DeviceSide,
											scanChanDesc.Id,
											arg2);
			AutoconMain->DEV->Update(false);
			break;
		case SSC_PRINTSG:
			popStack(&arg1,4);

			for(j=0;j<AutoconMain->Config->GetAllScanChannelsCount();j++)
			{
				if(!AutoconMain->Config->getSChannelbyIdx(j, &scanChanDesc))
					continue;
				if(arg1 == (unsigned int)scanChanDesc.StrokeGroupIdx)
				{
					outputStream << "-> Channel " << scanChanDesc.Id << " : ";
					outputStream << "Sensitivity=" << AutoconMain->Calibration->GetSens(scanChanDesc.DeviceSide,scanChanDesc.Id,0);
					outputStream << ", Gain=" << AutoconMain->Calibration->GetGain(scanChanDesc.DeviceSide,scanChanDesc.Id,0);
					outputStream << ", PrismDelay=" << AutoconMain->Calibration->GetPrismDelay(scanChanDesc.DeviceSide,scanChanDesc.Id);
					outputStream << "\r\n";
				}
			}
			break;
		case SSC_PRINTSGMAX:
			popStack(&arg1,4);

            for(j=0;j<AutoconMain->Config->GetAllScanChannelsCount();j++)
			{
				if(!AutoconMain->Config->getSChannelbyIdx(j, &scanChanDesc))
					continue;
				if(arg1 == (unsigned int)scanChanDesc.StrokeGroupIdx)
				{
					outputStream << "-> Channel "<<scanChanDesc.Id<<" maximum : ";
					outputStream << AutoconMain->Rep->GetChannelMaximum(scanChanDesc.Id);
					outputStream << "\r\n";
                }
			}
			break;
		case SSC_PRINTCH:
			popStack(&arg1,4);

			if(AutoconMain->Config->getFirstSChannelbyID(arg1, &scanChanDesc) == -1)
				break;

			outputStream <<"-> Channel "<<scanChanDesc.Id<<" : ";
			outputStream <<"Sensitivity="<<AutoconMain->Calibration->GetSens(scanChanDesc.DeviceSide,scanChanDesc.Id,0);
			outputStream <<", Gain="<<AutoconMain->Calibration->GetGain(scanChanDesc.DeviceSide,scanChanDesc.Id,0);
			outputStream <<", PrismDelay="<<AutoconMain->Calibration->GetPrismDelay(scanChanDesc.DeviceSide,scanChanDesc.Id);
			outputStream <<"\r\n";
			break;
		case SSC_PRINTCHMAX:
			popStack(&arg1,4);

			outputStream<<"-> Channel "<<arg1<<" maximum : ";
			outputStream<<AutoconMain->Rep->GetChannelMaximum(arg1);
			outputStream<<"\r\n";
			break;
		case SSC_PRINTCARETS:
			AutoconMain->ac->GetCaretStatus(false,&bCaretActive,&iCaretCoord);
			outputStream<<"-> Top Caret Status:\tactive-"<<bCaretActive<<", coord-"<<iCaretCoord<<"\r\n";
			AutoconMain->ac->GetCaretStatus(true,&bCaretActive,&iCaretCoord);
			outputStream<<"-> Bottom Caret Status:\tactive-"<<bCaretActive<<", coord-"<<iCaretCoord<<"\r\n";
			break;
		case SSC_TUNE:
			TuneChannels();
			break;
		case SSC_TUNESG:
            for(unsigned int i = 0; i < argc; i++)
            {
                popStack(&arg1,4);
			    TuneChannelGroup(arg1);
            }
			break;
		case SSC_TUNECH:
            for(unsigned int i = 0; i < argc; i++)
            {
                popStack(&arg1,4);
			    TuneChannel(arg1);
            }
            break;
		case SSC_TUNECLEAR:
			AutoconMain->Rep->ClearChannelMaximumsData();
			break;
		case SSC_TUNESETWCCNT:
			popStack(&arg1,4);
			//AutoconMain->ac->SetTuneWorCycleCount(arg1);
			break;
		case SSC_PATHRESET:
			AutoconMain->DEV->ResetPathEncoder();
			break;
		case SSC_SCAN:
			popStack(&arg1,4);
			if(arg1)
				AutoconMain->DEV->Update(false);//????
			else
				AutoconMain->DEV->DisableAll();
			break;
		case SSC_ENDPRG:
			return SSRC_END_PROGRAMM;
			break;
		case SSC_SLEEP:
			popStack(&arg1,4);
			Sleep(arg1);
			break;
		case SSC_SETWCCTL:
			popStack(&arg1,4);
			//AutoconMain->ac->EnableManualWorkCycleCtrl(arg1);
			break;
		case SSC_SETWC:
			popStack(&arg1,4);
			//AutoconMain->ac->SetWorkCycle(arg1);
			break;
        case SSC_INTERVAL:
			popStack(&arg3,4);
            popStack(&arg2,4);
            popStack(&arg1,4);
            AutoconMain->Rep->SetChannelCollectInterval(arg1, arg2, arg3);
			//AutoconMain->ac->SetWorkCycle(arg1);
			break;
		break;
	}
	return SSRC_RUNNING;
}

eScanScriptRetCode cScanScriptProgramm::executeCmd(USHORT cmdCode)
{
    UINT argc = 0;
    //Check argument count
    popStack(&argc,4); //pop argument count
    sScanScriptCmdDecl* pDecl = codeDecl.GetCmdDeclByCmdCode(cmdCode);
    if(!pDecl)
    {
        ScanScriptMessageOut(&outputStream,&errBuf,
			StringFormatStdA("Runtime error: command is not valid").c_str(),0,getCurrentLine());
        return SSRC_RUNTIME_ERROR;
    };
    if((pDecl->argc != argc) && (pDecl->argc != 0xFFFF))
    {
        ScanScriptMessageOut(&outputStream,&errBuf,
			StringFormatStdA("Runtime error: %d arguments in command \"%s\" (must be %d)",argc,pDecl->name,pDecl->argc).c_str(),0,getCurrentLine());
        return SSRC_RUNTIME_ERROR;
    };

    return handleCmd(cmdCode, argc);
}

sScanScriptCodeDecl cScanScriptProgramm::GetStdCodeDecl()
{
    sScanScriptCodeDecl cDecl;
    cDecl.ClearCmd();
	cDecl.DeclareCmd("movcarets",SSC_MOVCARET,2);	//move carets: movecaret [caret one offset], [caret two offset];
	cDecl.DeclareCmd("movtobase",SSC_MOVTOBASE,0);  //mov carets to initial placement: movtobase;

	cDecl.DeclareCmd("setsg",SSC_SETSG,1);      //set scan group: setsg [scan group id];
	cDecl.DeclareCmd("setsgsens",SSC_SETSGSENS,2);
	cDecl.DeclareCmd("setsggain",SSC_SETSGGAIN,2);
	cDecl.DeclareCmd("setsgprism",SSC_SETSGPRISM,2);

	cDecl.DeclareCmd("setch",SSC_SETCH,1);      //set scan channel: setch [scan channel id];
	cDecl.DeclareCmd("setchsens",SSC_SETCHSENS,2);
	cDecl.DeclareCmd("setchgain",SSC_SETCHGAIN,2);
	cDecl.DeclareCmd("setchprism",SSC_SETCHPRISM,2);

	cDecl.DeclareCmd("printsg",SSC_PRINTSG,1);
	cDecl.DeclareCmd("printsgmax",SSC_PRINTSGMAX,1);
	cDecl.DeclareCmd("printch",SSC_PRINTCH,1);
	cDecl.DeclareCmd("printchmax",SSC_PRINTCHMAX,1);
	cDecl.DeclareCmd("printcarets",SSC_PRINTCARETS,0);

	cDecl.DeclareCmd("tune",SSC_TUNE,0);       //tune from collected maximums: tune;
	cDecl.DeclareCmd("tunesg",SSC_TUNESG,0xFFFF);
	cDecl.DeclareCmd("tunech",SSC_TUNECH,0xFFFF);
	cDecl.DeclareCmd("tuneclear",SSC_TUNECLEAR,0);  //clear collected maximums: tuneclear;
	cDecl.DeclareCmd("tunesetwccnt",SSC_TUNESETWCCNT,1);

	cDecl.DeclareCmd("pathreset",SSC_PATHRESET,0);  //reset path encoder: pathreset;
	cDecl.DeclareCmd("scan",SSC_SCAN,1);		//enables/disables scan: scan [1,0];
	cDecl.DeclareCmd("sleep",SSC_SLEEP,1);		//sleep in ms: sleep [time ms];
	cDecl.DeclareCmd("setwcctl",SSC_SETWCCTL,1);
	cDecl.DeclareCmd("setwc",SSC_SETWC,1);

    cDecl.DeclareCmd("interval",SSC_INTERVAL,3);

	cDecl.DeclareCmd("end",SSC_ENDPRG,0);		//ends script: end;
    return cDecl;
}

void cScanScriptProgramm::initCodeDecl()
{
    codeDecl = GetStdCodeDecl();
}

//--------------------------------UTILS--------------------------------------

void TuneChannels()
{
	for(unsigned int i = 0; i < AutoconMain->Rep->GetChannelCtrlDataCount(); i++)
	{
		if(AutoconMain->Rep->GetChannelCtrlData(i)->currentMaximum != -1)
			TuneChannel(i);
	}
}

void TuneChannelGroup(int chGr)
{
	sScanChannelDescription scanChanDesc;
	for(unsigned int j=0;j<AutoconMain->Config->GetAllScanChannelsCount();j++)
	{
		if(!AutoconMain->Config->getSChannelbyIdx(j, &scanChanDesc))
			continue;

		if(scanChanDesc.StrokeGroupIdx == chGr)
		{
			TuneChannel(scanChanDesc.Id);
		}
	}
}

bool TuneChannel(int chId)
{
	int chMax = AutoconMain->Rep->GetChannelMaximum(chId);
	if(chMax==-1)
	{
		LOGERROR("Channel %d: maximum not found!",chId);
		return false;
	}

	return TuneChannel(chId, chMax);
}

bool TuneChannel(int chId, int chMax)
{
	if(chMax < 0)
		return false;
	AutoconMain->CS->Enter();

	LOGINFO("Tune channel %d: max-%d",chId,chMax);

	sChannelDescription ChanDesc;
	if(!AutoconMain->Table->ItemByCID(chId, &ChanDesc))
	{
		LOGERROR("Channel %d not found!",chId);
		AutoconMain->CS->Release();
		return false;
	}

	sScanChannelDescription sch_desc;
	if(AutoconMain->Config->getFirstSChannelbyID(chId, &sch_desc) == - 1)
	{
		LOGERROR("Scan channel %d not found!",chId);
		AutoconMain->CS->Release();
		return false;
	}

	const int currGateIndex = 0;

	//return true;
	int OldGain = AutoconMain->Calibration->GetGain(sch_desc.DeviceSide, chId,currGateIndex);
	int CurrGain = OldGain;
	int NewGain = OldGain;
	int ParamN_=0;    //���������� ��� �������

	float Tmp = (float)(chMax) / (float)32;
	if (Tmp != 0)
		ParamN_ = (int)(20 * log10 (Tmp)); // ���� - 18

	NewGain = CurrGain + (ChanDesc.RecommendedSens[currGateIndex] - ParamN_);
//	NewGain=CLAMP(NewGain, AutoconMain->Config->GainMin,AutoconMain->Config->GainMax);
	NewGain= CLAMP(NewGain, 0, AutoconMain->Config->GainMax * AutoconMain->Config->GainStep);

	AutoconMain->Calibration->SetSens(sch_desc.DeviceSide, chId, currGateIndex, ChanDesc.RecommendedSens[currGateIndex]);
	AutoconMain->Calibration->SetGain(sch_desc.DeviceSide, chId, currGateIndex, NewGain);
	AutoconMain->DEV->SetChannelParams(sch_desc.DeviceSide, chId, true, false, false, false);

    if((ParamN_ <= 0) || (ParamN_ >= 18))
        AutoconMain->Rep->GetChannelCtrlData(chId)->controlState = 2; //error
    else if( abs(ParamN_ - ChanDesc.RecommendedSens[0]) < 2)
        AutoconMain->Rep->GetChannelCtrlData(chId)->controlState = 0; //ok
    else
        AutoconMain->Rep->GetChannelCtrlData(chId)->controlState = 1; //warning

    bool bCalibrated = abs(ParamN_ - ChanDesc.RecommendedSens[0]) < 2;
    AutoconMain->Calibration->SetCalibrationDateTime(sch_desc.DeviceSide, chId, Now());
    AutoconMain->Calibration->SetState(sch_desc.DeviceSide, chId, currGateIndex, bCalibrated);


	LOGINFO("Channel 0x%X tuned: OldGain-%d, NewGain-%d, MaxValue-%d, N-%d",chId,OldGain,NewGain,chMax,ParamN_);

	AutoconMain->CS->Release();
	return true;
}

bool ReverseTranslateScanScriptCode(cScanScriptCmdBuffer* pCmdBuf, sScanScriptCodeDecl* pCodeDecl, char** ppCodeBuff, unsigned int* pCodeBufSize)
{
    std::list<int> stack;
    std::string prg_text;

    for(unsigned int IP = 0; IP < pCmdBuf->bytecode.size();)
    {
        BYTE code = pCmdBuf->bytecode[IP++];
        BYTE arg1;
        int stackVal;
        USHORT cmdCode;
        eScanScriptRetCode excode;

        switch(code)
        {
            case SSP_BYTECODE_NOOP:
                break;
            case SSP_BYTECODE_STACK_PUSH:
                arg1 = pCmdBuf->bytecode[IP++];//size, bytes
                switch(arg1)
                {
                    case 1: stackVal = *(BYTE*)&(pCmdBuf->bytecode[IP]); break;
                    case 2: stackVal = *(short*)&(pCmdBuf->bytecode[IP]); break;
                    case 4: stackVal = *(int*)&(pCmdBuf->bytecode[IP]); break;
                }
                stack.push_back(stackVal);
                IP+=arg1;
                break;
            case SSP_BYTECODE_STACK_POP:
                break;
            case SSP_BYTECODE_CALL:
                arg1 = pCmdBuf->bytecode[IP++];//func id size
                if(arg1==2)
                {
                    cmdCode = *(USHORT*)(&(pCmdBuf->bytecode[IP]));

                    sScanScriptCmdDecl* pCmdDecl = pCodeDecl->GetCmdDeclByCmdCode(cmdCode);
                    if(pCmdDecl)
                    {
                        prg_text += pCmdDecl->name;
                    }
                    else
                    {
                        prg_text += "UNKNOWN";
                    }

                    while(!stack.empty())
                    {
                        int val = stack.front();
                        stack.pop_front();
                        prg_text+= StringFormatStdA(" %d",val);
                        if(!stack.empty())
                            prg_text+=",";
                    }

                    prg_text+= ";\r\n";
                }
                IP+=arg1;
                break;
            case SSP_BYTECODE_ENTRY_POINT:
                break;
            case SSP_BYTECODE_DEBUG_BREAK:
                IP+=2;
                prg_text+= "\r\n";
                break;
        }
    }


    (*ppCodeBuff) = new char[prg_text.size() + 1];
    memcpy((*ppCodeBuff),prg_text.c_str(),prg_text.size());
    (*ppCodeBuff)[prg_text.size()] = 0;

    *pCodeBufSize = prg_text.size();
    return true;
}
