﻿/**
 * @file AutoconMain.h
 * @author Denis Fedorenko
 */

#ifndef AutoconMainH
#define AutoconMainH

//---------------------------------------------------------------------------
#include "Autocon.inc"
#include "Device.h"
#include "LanProtUMU.h"
#include "EventManager.h"
#include "EventManager_Win.h"
#include "DeviceConfig_Autocon.h"
#include "DataStorage.h"
#include "threadClassList_Win.h"
#include "DeviceCalibration.h"
#include "CriticalSection_Win.h"
#include "Platforms.h"
#include "TickCount.h"
#include "DataTr.h"
#include "BScanLines.h"
#include "BScanDraw.h"
#include "cavtk2.h"
#include "LanProtUMU.h"
#include "AController.h"
#include "TestThreadUnit.h"
//#include <Vcl.Dialogs.hpp>

#include "Utils.h"

/** \def PORT_out
 *  \brief Output port
 */
#define PORT_out 43000
/** \def PORT_in
 *  \brief Input port
 */
#define PORT_in  43001



/** \def AM_FLAG_USE_PROTOCOL
 *  \brief Not used
 *  \deprecated
 */
#define AM_FLAG_USE_PROTOCOL 0x02

/** \def AM_FLAG_SHOW_TEST_RAIL_MSG
 *  \brief Показывать сообщение о поднятии рельса
 */
#define AM_FLAG_SHOW_TEST_RAIL_MSG  0x04

/** \def AM_FLAG_TAKE_RAIL_PHOTO
 *  \brief Делать фотографию стыка
 */
#define AM_FLAG_TAKE_RAIL_PHOTO     0x08

/** \def AM_FLAG_TUNE_STATE_ON_END
 *  \brief Выходить из внутреннего состояния ручной настройки по выходу из режима.
 *  \note hack
 */
#define AM_FLAG_TUNE_STATE_ON_END     0x10

#define AM_SEARCH_FLAGS (AM_FLAG_USE_PROTOCOL | AM_FLAG_TAKE_RAIL_PHOTO)        //!< Флаги для режима Поиск
#define AM_TEST_FLAGS   (AM_FLAG_USE_PROTOCOL | AM_FLAG_SHOW_TEST_RAIL_MSG)     //!< Флаги для режима Тест
#define AM_TUNE_FLAGS (AM_FLAG_SHOW_TEST_RAIL_MSG | AM_FLAG_TUNE_STATE_ON_END)  //!< Флаги для режима Настройка
#define AM_HSCAN_FLAGS (AM_FLAG_USE_PROTOCOL | AM_FLAG_TAKE_RAIL_PHOTO)         //!< Флаги для режима ручного сканирования
#define AM_HSCAN_TUNE_FLAGS (0x00)                                              //!< Флаги для режима настройки ручников


//---------------------------------------------------------------------------

class cDeviceThread;

/** \struct RegCrdParams
 *  \brief Содержит параметры начала и длины участка сканирования для определенного движения.
 */
struct RegCrdParams
{
    int ChannelGroupIndex;
    int RegLength;
    int StartCoord;
    bool Used;
};

class cTestThread;  // определение см. в TestThreadUnit.h

/*! \brief AutoconMain
 * Содержит указатели на основные объекты системы
 * Устанавливает соединение с БУМами и контроллером
 */
class cAutoconMain
{
    public:

    int dip12maxdiff;
    int dip34maxdiff;
	BOOL CtrlWork;
	BOOL UMU_Connection_OK;
	BOOL QryThreadFlag;
//	int WorkCycle;
//  int AlignCycle;
    RegCrdParams RegCrdParamsList[15];
    int RegKPOffsets[9]; //!< Смещение колес относительно каретки

	//Added by KirillB
	CHTMLLogger logger;
    volatile int XSysCoord_Base;
    volatile int XSysCoord_Center;

	cDataTransfer *DT;                      //!< Для установки соединения с БУМами и контроллером
	cDevice *DEV;                           //!< Для обмена данными с БУМами
	cChannelsTable *Table;                  //!< Таблица каналов
    cDeviceConfig_Autocon *Config;          //!< Таблица тактов
	cDeviceCalibration *Calibration;        //!< Параметры каналов
	cEventManagerWin *DeviceEventManage;    //!< Менеджер эвентов для сообщений от #cDevice
	cEventManagerWin *UMUEventManage;       //!< Менеджер эвентов для сообщений от #UMU_3204
	cThreadClassList_Win *threadList;
    cJointTestingReport *Rep;
	cCriticalSectionWin *CS;
	cDeviceThread *devt;
//    cavtk *ctrl; // -
	cAController *ac;
    cTestThread *cavtk_test;
    //int ControlMode; // 1 - Поиск, 2 - Тест, 3 - Настройка;
    DWORD am_flags;
    int ReportFileType; // 1 - Поиск, 2 - Тест, 3 - Юстировка; 4 - контроль только ручик;
    bool SkipRep;

//    sFilterParams FilterParams;

    cAutoconMain(void);
    ~cAutoconMain(void);
    bool OpenConnection(int ModeIdx /*, INPARAMDEF param*/ );
    void SetFlags(DWORD flags) {am_flags = flags;};
    DWORD GetFlags() {return am_flags;};
    //void SetMode(int Mode_);
    void SetReportFileType(int Type);
    //int GetMode(void);
    void CleanupEventMgr();
    void Create();
    void Release();
};


// --- DeviceThread -----------------------------------------------------------
/** \def DEVTHREAD_LIST_SIZE
 *  \brief Максимальный размер кольцевого буффера cDeviceThread#Ptr_List
 */
#define DEVTHREAD_LIST_SIZE 2048


/** \union  uPtrListPointer
 *  Содержит все используемые типы эвентов.
 *  Облегчает доступ к эвенту из #cEventManagerWin по указателю.
 */
union uPtrListPointer
{
    void* pVoid;
    PtDEV_AScanMeasure pAScanMeasure;
    PtDEV_AScanHead pAScanHead;
    PtDEV_BScan2Head pBScan2Head;
    PtDEV_AlarmHead pAlarmHead;
    PtUMU_AScanData pAScanData;
    PtUMU_BScanData pBScanData;
    PtUMU_AlarmItem pAlarmItem;
};

/** \struct  sPtrListItem
 *  \brief Хранит эвент из #cEventManagerWin (как правило, один эвент содержит два указателя), и его тип.
 *  \sa \ref uPtrListPointer \ref EventDataType
 */
struct sPtrListItem
{
    sPtrListItem()
    {
        Ptr1.pVoid = NULL;
        Ptr2.pVoid = NULL;
        Type = NULL;
    };

    explicit sPtrListItem(uPtrListPointer Ptr1, uPtrListPointer Ptr2, EventDataType Type)
    {
        this->Ptr1 = Ptr1;
        this->Ptr2 = Ptr2;
        this->Type = Type;
    };

    explicit sPtrListItem(sPtrListItem& other)
    {
        this->Ptr1 = other.Ptr1;
        this->Ptr2 = other.Ptr2;
        this->Type = other.Type;

        other.Ptr1.pVoid = NULL;
        other.Ptr2.pVoid = NULL;
        other.Type = NULL;
    };

    void operator = (sPtrListItem& other)
    {
        free();

        this->Ptr1 = other.Ptr1;
        this->Ptr2 = other.Ptr2;
        this->Type = other.Type;

        other.Ptr1.pVoid = NULL;
        other.Ptr2.pVoid = NULL;
        other.Type = NULL;
    };

    //! Очистка с деаллокацией ненулевых указателей в зависимости от типа данных
    /*
        \sa _delPtrs()
    */
    void free()
    {
        _delPtrs(Ptr1,Ptr2);

        Type = NULL;
    };

    ~sPtrListItem()
    {
        free();
    };

    uPtrListPointer Ptr1;
    uPtrListPointer Ptr2;
    EventDataType Type;

private:
    //! Функция очистки с удалением данных для #uPtrListPointer
    /*
        Требует указания типа эвента в поле #Type
        \param ptr1 Первый указатель
        \param ptr2 Второй указатель
    */
    void _delPtrs(uPtrListPointer& ptr1,uPtrListPointer& ptr2)
    {
        switch(Type)
        {
        case edAScanMeas:
            if(ptr1.pAScanMeasure)
                delete ptr1.pAScanMeasure;
            break;
        case edAScanData:
        case edTVGData:
            if(ptr1.pAScanHead)
                delete ptr1.pAScanHead;
            if(ptr2.pAScanData)
                delete ptr2.pAScanData;
            break;
        case edAlarmData:
            if(ptr1.pAlarmHead)
                delete ptr1.pAlarmHead;
            if(ptr2.pAlarmItem)
                delete ptr2.pAlarmItem;
            break;
        case edBScan2Data:
        case edMScan2Data:
            if(ptr1.pBScan2Head)
                delete ptr1.pBScan2Head;
            if(ptr2.pBScanData)
                delete ptr2.pBScanData;
            break;
        default:
            if(ptr1.pVoid)
                delete ptr1.pVoid;
            if(ptr2.pVoid)
                delete ptr2.pVoid;
        };
        ptr1.pVoid = NULL;
        ptr2.pVoid = NULL;
    };
    sPtrListItem(const sPtrListItem&);
};

/** \class cDeviceThread
 *  \brief Класс cDeviceThread используется для приема и первичной обработки данных, поступающих из #cDevice.
    Берет данные из cAutoconMain::DeviceEventManage
    Из эвентов формирует структуры типа #sPtrListItem и кладет в кольцевой буффер #Ptr_List
    Для эвентов #edAlarmData #edBScan2Data существует дополнительная обработка (см. Execute())
 */
class cDeviceThread : public TThread
{
    DWORD DataID;
    void __fastcall Execute(void);
    cAutoconMain *main;

    #ifdef TextLog_def
    TStringList* TextLog;
    #endif

public:
    bool EndWorkFlag; //!< Флаг завершения функции Execute()
    int InTick;

    ringbuf<sPtrListItem> Ptr_List; //!< Кольцевой буфер для полученных эвентов

    __fastcall cDeviceThread(cAutoconMain *main_);
    __fastcall ~cDeviceThread(void);

};

extern TStringList* TextLog;

#endif
