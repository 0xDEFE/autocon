//---------------------------------------------------------------------------

#ifndef AScanViewUnitH
#define AScanViewUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.Series.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
//#include "MainUnit.h"
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Buttons.hpp>

#include <VCLTee.TeeShape.hpp>
#include "DataStorage.h"
//---------------------------------------------------------------------------
class TAScanViewForm : public TForm
{
__published:	// IDE-managed Components
    TPanel *Panel1;
    TCheckBox *ValueCountCheckBox;
    TPanel *Panel2;
    TCheckBox *PointsCheckBox;
    TPanel *Panel3;
    TPanel *Panel4;
    TCheckBox *DelayCountCheckBox;
    TChart *AscanChart;
    TLineSeries *AScanSeries1;
    TLineSeries *AScanSeries2;
    TLineSeries *AScanSeries3;
    TLineSeries *AScanSeries4;
    TLineSeries *AScanSeries5;
    TLineSeries *AScanSeries6;
    TPanel *Panel5;
    TPanel *Panel6;
    TButton *Button1;
    TButton *Button2;
    TButton *Button3;
    TPageControl *PageControl1;
    TTabSheet *ChartTabSheet;
    TTabSheet *InfoTabSheet;
    TGridPanel *GridPanel3;
    TLabel *m_ChannelNameNameLabel;
    TLabel *m_KuNameLabel;
    TLabel *m_AttNameLabel;
    TLabel *m_StGateNameLabel;
    TLabel *m_EdGateNameLabel;
    TLabel *m_TVGNameLabel;
    TLabel *m_ChannelNameLabel;
    TLabel *m_KuLabel;
    TLabel *m_AttLabel;
    TLabel *m_StGateLabel;
    TLabel *m_EdGateLabel;
    TLabel *m_TVGLabel;
    TPanel *Panel7;
    TLabel *m_PrismDelayNameLabel;
    TLabel *m_PrismDelayLabel;
    TChartShape *GateStSeries1;
    TChartShape *GateEdSeries1;
    TGridPanel *ButtonsPanel;
    TLabel *ButtonsLabel;
    TChartShape *GateMainSeries1;
    TChartShape *GateStSeries2;
    TChartShape *GateEdSeries2;
    TChartShape *GateMainSeries2;
    TSpeedButton *SelGateBtn2;
    TSpeedButton *SelGateBtn3;
    TSpeedButton *SelGateBtn4;
    TSpeedButton *SelGateBtn5;
    TSpeedButton *SelGateBtn6;
    TSpeedButton *SelGateBtn1;
    void __fastcall PointsCheckBoxClick(TObject *Sender);
    void __fastcall AscanChartClick(TObject *Sender);
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall Button2Click(TObject *Sender);
    void __fastcall Button3Click(TObject *Sender);
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall OnSelGateBtnClicked(TObject *Sender);
private:	// User declarations
    struct
    {
        int stGate[2];
        int edGate[2];
        int gateCnt;
    } btnGateInfo[6];
    std::pair<int,UnicodeString> prevUpdateActiveBtn;
//    int btnGateInfo[6][4];
    TSpeedButton* gateBtns[6];

public:
    TNotifyEvent OnLeftBtnClicked;
    TNotifyEvent OnRightBtnClicked;
    TNotifyEvent OnCloseBtnClicked;
public:		// User declarations
    __fastcall TAScanViewForm(TComponent* Owner);

    void setInfoMode(bool bVal);


    void setInfo(UnicodeString chName, sScanChannelParams* pGate1, sScanChannelParams* pGate2 = NULL);
    void setInfo(UnicodeString chName, int Ku, int Att, int gateCount, int StGate1, int EdGate1,int StGate2, int EdGate2, int TVG, int PrismDelay);
    void setGateBtn(unsigned int idx, UnicodeString name, TColor btnColor, int gateCnt, int stGate0, int edGate0, int stGate1 = 0, int edGate1 = 0);
    void removeGateButtons();
    void updateButtons();
};
//---------------------------------------------------------------------------
extern PACKAGE TAScanViewForm *AScanViewForm;
//---------------------------------------------------------------------------
#endif
