﻿/**
 * @file MainUnit.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Buttons.hpp>
//---------------------------------------------------------------------------

#include <deque>

#include "AutoconMain.h"
#include "BasicFrameUnit.h"

typedef std::pair<cavtk*, tCavtkMsg> cavtk_msg_pair;

/** Базовый класс для стекового отображения форм.
    Основной задачей является визуализация верхней формы, а так же передача эвентов от БУМов, cDevice и т.п.
 */
class TMainUnit : public TForm
{
__published:	// IDE-managed Components
    TTimer *SignalTimer;
    TTimer *ChangeModeTimer;
    TTimer *DeleteLaterTimer;
    TPanel *WaitPanel;
    TLabel *MessageLabel;
    TLabel *WaitLabel;
    TPanel *InfoPanel;
    TImage *RailBlockStateImage;
    TLabel *RailBlockStateLabel;
    TImageList *RailBlockStateImages;
    TTimer *UpdateRailBlockStateTimer;
    TGridPanel *GridPanel1;
    TPanel *Panel1;
    TSpeedButton *ShowDebugWindowsBtn;
    void __fastcall SignalTimerTimer(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall FormHide(TObject *Sender);
    void __fastcall ChangeModeTimerTimer(TObject *Sender);
    void __fastcall FormShortCut(TWMKey &Msg, bool &Handled);
    void __fastcall DeleteLaterTimerTimer(TObject *Sender);
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall UpdateRailBlockStateTimerTimer(TObject *Sender);
    void __fastcall ShowDebugWindowsBtnClick(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
private:	// User declarations
    void UpdateSignals();
    void ProcMotion(eMainUnitSignalType type, int WorkCycle);

    bool bMessageShowState;
    //UnicodeString currMessage;
    //cCriticalSectionWin msgCs;

    std::queue<cavtk_msg_pair> cavtk_messages;
    cCriticalSectionWin cavtkQueueLock;


    TBasicFrame* pTopFrame;
    std::list<TBasicFrame*> deleteLaterFrames;

    void updateRemove();

    bool bRailBlockCriticalMode;
    bool bRailBlockState;
    void _setRailBlockState(bool bVal);
    void updateRailBlockState();
    void procCavtkMessages();

    bool ChangeModeFlag;
public:		// User declarations
    __fastcall TMainUnit(TComponent* Owner);

    void GetMessages(TMessagesType Type, UnicodeString Text, DWord Time); //!< Передача сообщения от класса (legacy function)
    bool OnCavtkRequest_(cavtk *ctrl, tCavtkMsg& msg);
    void SearchMotion_(int WorkCycle); //!< Режим "Поиск"
    void TestMotion_(int WorkCycle);   //!< Режим "Тест"
	void AdjustMotion_(int WorkCycle); //!< Режим "Юстировка"
	void TuneMotion_(int WorkCycle); //!< Режим "Настройка"
	void ManualMotion_(int WorkCycle); //!< Режим "Ручной"

    void deleteLater(TBasicFrame* pFrame); //!< Помещает child фрейм в список удаления с удалением по таймеру

    void BackButton();      //!< Завершение режима с ожиданием окончания всех переходных процессов (legacy function)
    void DoMainUnitDestroy(); //!< Запуск завершения режима с ожиданием окончания всех переходных процессов

    eMotionState motionState;

    /** \brief Создает экземпляр шаблонного типа T и помещает его на верхушку стека (с отображением поверх текущей формы)
        \param T Шаблонный тип формы (должен быть наследован от TBasicFrame)
        \param frameFlags Флаги формы
        \returns Указатель на созданный экземпляр формы
     */
    template<typename T>
    T* pushFrame(DWORD frameFlags, bool bModal = true)
    {
        assert(pTopFrame);
        return pTopFrame->pushFrame<T>(frameFlags, bModal);
    }
    //! Убирает ферхнюю форму из стека
    void popFrame() {pTopFrame->popFrame();};

    /** \brief Не используется
        \deprecated
     */
    template<typename T>
    T* connectFrame(DWORD frameFlags,TWinControl* pOwner)
    {
        assert(pTopFrame);
        return pTopFrame->connectFrame<T>(frameFlags, pOwner);
    }
    /** \brief Не используется
        \deprecated
     */
    void disconnectFrame(TBasicFrame* pFrame) {pTopFrame->disconnectFrame(pFrame);};

    void Initialize();
};
//---------------------------------------------------------------------------
extern PACKAGE TMainUnit *MainUnit;
//---------------------------------------------------------------------------
extern cAutoconMain *AutoconMain;
#endif
