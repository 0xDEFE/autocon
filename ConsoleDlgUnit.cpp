//---------------------------------------------------------------------
#include <vcl.h>
//#pragma hdrstop

#include "ConsoleDlgUnit.h"
//---------------------------------------------------------------------
#pragma resource "*.dfm"
TConsoleDlg *ConsoleDlg;
//--------------------------------------------------------------------- 
__fastcall TConsoleDlg::TConsoleDlg(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------
void __fastcall TConsoleDlg::FormShow(TObject *Sender)
{
	if(!AutoconMain)
	{
		return;
	}
}
//---------------------------------------------------------------------------

void __fastcall TConsoleDlg::m_ConsoleExecBtnClick(TObject *Sender)
{
	std::string code = WCharToCharStr(m_ConsoleEdit->Text.c_str());
	m_ConsoleEdit->Text = UnicodeString();

	if(code == "help")
	{
		m_ConsoleMemo->Lines->Add("###############################################\r\n\
movcarets arg1, arg2;		# Moving carets: arg1 - caret one offset, arg2 - caret two offset. Only in tune mode.\r\n\
movtobase;					# Moving carets to start position. Only in tune mode.\r\n\
\r\n\
setsg arg1;				# Set scan group: arg1 - scan group index.\r\n\
setsgsens arg1, arg2;	# Set scan group sensitivity: arg1 - scan group index, arg2 - value.\r\n\
setsggain arg1, arg2;	# Set scan group gain: arg1 - scan group index, arg2 - value.\r\n\
setsgprism arg1, arg2;	# Set scan group prism delay: arg1 - scan group index, arg2 - value.\r\n\
setsgtvg arg1, arg2;	# Set scan group TVG: arg1 - scan group index, arg2 - value.\r\n\
\r\n\
setch arg1;				# Set scan channel: arg1 - scan channel index.\r\n\
setchsens arg1, arg2;	# Set scan group sensitivity: arg1 - scan channel index, arg2 - value.\r\n\
setchgain arg1, arg2;	# Set scan group gain: arg1 - scan channel index, arg2 - value.\r\n\
setchprism arg1, arg2;	# Set scan group prism delay: arg1 - scan channel index, arg2 - value.\r\n\
setchtvg arg1, arg2;	# Set scan group TVG: arg1 - scan channel index, arg2 - value.\r\n\
\r\n\
printsg arg1;			# Print scan group data: arg1 - scan group index.\r\n\
printsgmax arg1;			# Print scan group data: arg1 - scan group index.\r\n\
printch arg1;			# Print scan channel data: arg1 - scan channel index.\r\n\
printchmax arg1;			# Print scan channel data: arg1 - scan channel index.\r\n\
printcarets;			# Print carets position\r\n\
\r\n\
tune;			# Init tuning from collected maximums. \r\n\
tuneclear;		# Clears collected maximums. \r\n\
pathreset;		# Resets path encoder.\r\n\
scan arg1;		# Activates/deactivates scan process (DEV->DisableAll() / DEV->Update(true)): arg1 - [0,1] \r\n\
sleep arg1;		# Sleep function: arg1 - time in ms.\"\r\n\
setwcctl arg1;	# Enable/Disable work cycle control: arg1 - [0,1].\r\n\
setwc arg1;		# Set current workcycle: arg1 - work cycle number.\r\n\
end;			# Ends programm.\r\n\
###############################################\r\n");
		return;
	}
	else if(code == "clear")
	{
		m_ConsoleMemo->Clear();
		return;
    }

	scriptPrg.getOutputStream().str("");
	bool bRet = scriptPrg.createFromMemory(code.c_str());

	m_ConsoleMemo->Lines->Add(code.c_str());
	if(!scriptPrg.getOutputStream().str().empty())
		m_ConsoleMemo->Lines->Add(scriptPrg.getOutputStream().str().c_str());

	if(!bRet)
		return;

	UnicodeString MemoText = m_ConsoleMemo->Text;
	UnicodeString LineStr;
	scriptPrg.getOutputStream().str("");

	while(scriptPrg.executeNext())
	{
		LineStr = scriptPrg.getOutputStream().str().c_str();
		m_ConsoleMemo->Text = MemoText+LineStr;
		Sleep(1);
	}

	m_ConsoleMemo->Lines->Add("");
}
//---------------------------------------------------------------------------

void __fastcall TConsoleDlg::m_ExecFromFileBtnClick(TObject *Sender)
{
	TOpenDialog* fd = new TOpenDialog(this);
	fd->Filter = "ScanScript files (*.sc)|*.SC|Text files (*.txt)|*.TXT";
	fd->FilterIndex = 1;
	fd->InitialDir = GetCurrentDir();

	if(!fd->Execute())
		return;

	m_ConsoleMemo->Lines->Add(StringFormatU(L"# Parsing: %s...",fd->FileName));

	std::string ansiFileName = WCharToCharStr(fd->FileName.c_str());

	scriptPrg.getOutputStream().str("");
	bool bRet = scriptPrg.createFromFile(ansiFileName.c_str());

	if(!scriptPrg.getOutputStream().str().empty())
		m_ConsoleMemo->Lines->Add(scriptPrg.getOutputStream().str().c_str());
	if(!bRet)
		return;

	m_ConsoleMemo->Lines->Add(StringFormatU(L"# Executing: %s...",fd->FileName));
	UnicodeString MemoText = m_ConsoleMemo->Text;
	int lineIdx = m_ConsoleMemo->Lines->Add(L"[| | ] 0%%");
	UnicodeString LineStr;

	scriptPrg.getOutputStream().str("");

	while(scriptPrg.executeNext())
	{
		float completePerc = float( scriptPrg.getInstructionPointer() ) /
									float( scriptPrg.getCodeSize() );
		int completeSize = completePerc * 100.f;

		LineStr = L"\r\n";
		LineStr += scriptPrg.getOutputStream().str().c_str();
		LineStr += L"\r\n# Execute progress: [";
		for(int i=0;i<completeSize;i++)
		{
			LineStr+=L"|";
		}
		LineStr+=StringFormatU(L"] %d%%",completeSize);

		m_ConsoleMemo->Text = MemoText+LineStr;

		m_ConsoleMemo->Refresh();
		Sleep(1);
	}

    m_ConsoleMemo->Lines->Add("");
}
//---------------------------------------------------------------------------

