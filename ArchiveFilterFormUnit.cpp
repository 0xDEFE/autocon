//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ArchiveDefinitions.h"
#include "ArchiveFilterFormUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TArchiveFilterForm *ArchiveFilterForm;

TDateTime MakeEndOfDay(TDateTime dt)
{
    WORD currYear, currDay, currMonth;
    DecodeDate(dt, currYear, currMonth, currDay);

    return EncodeDate( currYear, currMonth, currDay) + EncodeTime(23, 59, 59, 59);
}

TDateTime MakeBeginOfDay(TDateTime dt)
{
    WORD currYear, currDay, currMonth;
    DecodeDate(dt, currYear, currMonth, currDay);

    return EncodeDate( currYear, currMonth, currDay) + EncodeTime(0, 0, 0, 0);
}

//---------------------------------------------------------------------------
__fastcall TArchiveFilterForm::TArchiveFilterForm(TComponent* Owner)
    : TForm(Owner)
{
    ListPage->TabVisible = false;
    SelectPage->TabVisible = false;
    PageControl1->ActivePage = ListPage;

    pCancelBmp = new TBitmap();
    pBackBmp = new TBitmap();

    pBackBmp->Transparent = true;
	pBackBmp->TransparentColor = clWhite;
	pBackBmp->TransparentMode = tmAuto;
	pBackBmp->AlphaFormat = afDefined;
	pBackBmp->PixelFormat = pf32bit;
    pBackBmp->SetSize(ImageList1->Width,ImageList1->Height);
	pBackBmp->Canvas->FillRect(TRect(0,0,ImageList1->Width,ImageList1->Height));

    ImageList1->GetBitmap(0,pCancelBmp);
    ImageList1->GetBitmap(1,pBackBmp);

    generateFlowButtons(ADB_FIELDTYPE_UNKNOWN);

    //���������� ����� ��������
    for(int i = 0; i < ADB_FIELDTYPE_END; i++)
    {
        DWORD mask = GetFieldAssigns((eADBFieldType)i);
        if(!(mask & ADB_FIELDASSIGN_SEARCH))
            continue;

        switch(i)
        {
            case ADB_FIELDTYPE_FILENAME:
            case ADB_FIELDTYPE_OPERATOR:
            case ADB_FIELDTYPE_CONCLUSION:
            case ADB_FIELDTYPE_HAS_HSCAN_IMAGES:
            case ADB_FIELDTYPE_HAS_HSCAN_BSCAN:
            case ADB_FIELDTYPE_HAS_LINEARITY:
            case ADB_FIELDTYPE_HAS_CAMERA_IMAGES:
                CreateFlowBtnOneOp<TEdit,UnicodeString>("������ ��������", (eADBFieldType)i, AFD_OPTYPE_EQUAL, UnicodeString(""));
                break;

            case ADB_FIELDTYPE_DATETIME:
                CreateFlowBtnOneOp<TDateTimePicker,TDateTime>("������ ��������", (eADBFieldType)i, AFD_OPTYPE_EQUAL, Now());
                CreateFlowBtnOneOp<TDateTimePicker,TDateTime>("������ ��� ...", (eADBFieldType)i, AFD_OPTYPE_GREAT_EQUAL, MakeBeginOfDay(Now()));
                CreateFlowBtnOneOp<TDateTimePicker,TDateTime>("������ ��� ...", (eADBFieldType)i, AFD_OPTYPE_LESS_EQUAL, MakeEndOfDay(Now()));
                CreateFlowBtnTwoOp<TDateTimePicker,TDateTime>("� ���������� ...", (eADBFieldType)i, AFD_OPTYPE_GREAT_EQUAL, MakeBeginOfDay(Now()), AFD_OPTYPE_LESS_EQUAL, MakeEndOfDay(Now()));
                break;

            case ADB_FIELDTYPE_FILETYPE_NAME:
            {
                UnicodeString filterVal;;
                for(int i = 1; i <= 4; i++)
                    filterVal += ADBFileTypeIdxToFileTypeName(i) + L"\n";
                CreateFlowBtnOneOp<TComboFlat,UnicodeString>("������ ��������",
                    (eADBFieldType)i, AFD_OPTYPE_EQUAL, filterVal);
                break;
            }

            default:
                CreateFlowBtnOneOp<TSpinEdit,int>("������ ��������", (eADBFieldType)i, AFD_OPTYPE_EQUAL, (int)0);
                CreateFlowBtnOneOp<TSpinEdit,int>("������ ��� ...", (eADBFieldType)i, AFD_OPTYPE_GREAT_EQUAL, (int)0);
                CreateFlowBtnOneOp<TSpinEdit,int>("������ ��� ...", (eADBFieldType)i, AFD_OPTYPE_LESS_EQUAL, (int)0);
                CreateFlowBtnTwoOp<TSpinEdit,int>("� ���������� ...", (eADBFieldType)i, AFD_OPTYPE_GREAT_EQUAL, (int)0, AFD_OPTYPE_LESS_EQUAL, (int)0);
                break;
        }
    }

    //����/�����
    WORD currYear, currDay, currMonth;
    DecodeDate(Now(), currYear, currMonth, currDay);
    TDateTime currDate = EncodeDate(currYear, currMonth, currDay);

    CreateFlowBtnOneOp<TDateTimePicker,TDateTime>("�������", ADB_FIELDTYPE_DATETIME, AFD_OPTYPE_GREAT_EQUAL, currDate);
    CreateFlowBtnOneOp<TDateTimePicker,TDateTime>("������� �����", ADB_FIELDTYPE_DATETIME, AFD_OPTYPE_GREAT_EQUAL, EncodeDate(currYear, currMonth, 1));
    CreateFlowBtnTwoOp<TDateTimePicker,TDateTime>("���������� �����", ADB_FIELDTYPE_DATETIME, AFD_OPTYPE_GREAT_EQUAL, IncMonth(EncodeDate(currYear, currMonth, 1),-1), AFD_OPTYPE_LESS, EncodeDate(currYear, currMonth, 1));
    CreateFlowBtnOneOp<TDateTimePicker,TDateTime>("������� ���", ADB_FIELDTYPE_DATETIME, AFD_OPTYPE_GREAT_EQUAL, EncodeDate(currYear, 1, 1));
    CreateFlowBtnTwoOp<TDateTimePicker,TDateTime>("���������� ���", ADB_FIELDTYPE_DATETIME, AFD_OPTYPE_GREAT_EQUAL, EncodeDate(currYear-1, 1, 1), AFD_OPTYPE_LESS, EncodeDate(currYear, 1, 1));
}
//---------------------------------------------------------------------------

void __fastcall TArchiveFilterForm::FormDestroy(TObject *Sender)
{
    if(pBackBmp)
    {
        delete pBackBmp;
        pBackBmp = NULL;
    }

    if(pCancelBmp)
    {
        delete pCancelBmp;
        pCancelBmp = NULL;
    }

    removeFlowButtons();
    ClearBtnClick(this);

    for(unsigned int i = 0; i < flowButtonData.size(); i++)
    {
        delete flowButtonData[i];
        flowButtonData[i] = NULL;
    }
    flowButtonData.clear();
}
//---------------------------------------------------------------------------

void __fastcall TArchiveFilterForm::m_AddBtnClick(TObject *Sender)
{
    generateFlowButtons(ADB_FIELDTYPE_UNKNOWN);
    PageControl1->ActivePage = SelectPage;
}
//---------------------------------------------------------------------------

void TArchiveFilterForm::generateFlowButtons(eADBFieldType fieldType)
{
    removeFlowButtons();
    if( fieldType == ADB_FIELDTYPE_UNKNOWN )
    {
        TBitBtn *pBackBtn = createFlowBtn( "�����", -2);
        pBackBtn->Glyph = pBackBmp;
        pBackBtn->Layout = blGlyphTop;
        pBackBtn->OnClick = OnServiceFlowBtnClicked;

        for(int i = 0; i < ADB_FIELDTYPE_END; i++)
        {
            DWORD mask = GetFieldAssigns((eADBFieldType)i);
            if(!(mask & ADB_FIELDASSIGN_SEARCH))
                continue;

            TBitBtn *pButton = createFlowBtn( ADBFieldTypeToName( (eADBFieldType)i ), i);
            pButton->OnClick = OnServiceFlowBtnClicked;
        }
    }
    else
    {
        TBitBtn *pBackBtn = createFlowBtn( "�����", -1);
        pBackBtn->Glyph = pBackBmp;
        pBackBtn->Layout = blGlyphTop;
        pBackBtn->OnClick = OnServiceFlowBtnClicked;

        for(unsigned int i = 0; i < flowButtonData.size(); i++)
        {
            if(flowButtonData[i]->fieldType == fieldType)
            {
                TBitBtn *pButton = createFlowBtn( flowButtonData[i]->name, i);
                pButton->OnClick = OnFieldFlowBtnClicked;
            }
        }
    }
}

TBitBtn* TArchiveFilterForm::createFlowBtn(UnicodeString name, int tag)
{
    bool bCreatedNew = true;
    TBitBtn *pButton = new TBitBtn( m_FiltersFlowPanel );

    pButton->Visible = true;
    pButton->Caption = name;
    pButton->Width = 126;
    pButton->Height = 126;
    pButton->WordWrap = true;
    pButton->Tag = tag;
    m_FiltersFlowPanel->InsertControl(pButton);

    for(unsigned int i = 0; i < flowButtons.size(); i++)  //Add in empty place
        if(!flowButtons[i])
        {
            flowButtons[i] = pButton;
            return pButton;
        }
    flowButtons.push_back(pButton); //If no empty places
    return pButton;
}

void TArchiveFilterForm::removeFlowBtn(TBitBtn* pBtn)
{
    for(unsigned int i = 0; i < flowButtons.size(); i++)
    {
        if(flowButtons[i] == pBtn)
        {
            pBtn->Visible = false;
            pBtn->Tag = -1;
            pBtn->OnClick = NULL;
            m_FiltersFlowPanel->RemoveControl(pBtn);
            pBtn->Parent = NULL;
            flowButtons[i] = NULL;
            deleteLaterFlowBtnValues.push_back(pBtn);
        }
    }
}

void TArchiveFilterForm::removeFlowButtons()
{
    for(unsigned int i = 0; i < flowButtons.size(); i++)
    {
        if(flowButtons[i])
            removeFlowBtn(flowButtons[i]);
    }
}

void __fastcall TArchiveFilterForm::OnServiceFlowBtnClicked(TObject *Sender)
{
    TBitBtn* pBtn = dynamic_cast<TBitBtn*>(Sender);

    if(pBtn->Tag == -1)
    {
        generateFlowButtons(ADB_FIELDTYPE_UNKNOWN);
    }
    else if(pBtn->Tag == -2)
    {
        PageControl1->ActivePage = ListPage;
    }
    else
    {
        generateFlowButtons((eADBFieldType)pBtn->Tag);
    }
}

void __fastcall TArchiveFilterForm::OnFieldFlowBtnClicked(TObject *Sender)
{
    TBitBtn* pBtn = dynamic_cast<TBitBtn*>(Sender);

    int nBtnData = pBtn->Tag;
    if((nBtnData < 0) || (nBtnData >= (int)flowButtonData.size()))
        return;

    cIFlowBtn* pBtnData = flowButtonData[nBtnData];
    if(!pBtnData)
        return;

    switch(pBtnData->fieldType)
    {
        case ADB_FIELDTYPE_FILENAME:
        case ADB_FIELDTYPE_OPERATOR:
        case ADB_FIELDTYPE_CONCLUSION:
        case ADB_FIELDTYPE_HAS_HSCAN_IMAGES:
        case ADB_FIELDTYPE_HAS_HSCAN_BSCAN:
        case ADB_FIELDTYPE_HAS_LINEARITY:
        case ADB_FIELDTYPE_HAS_CAMERA_IMAGES:
        {
            cFlowBtnData<TEdit,UnicodeString>* pData = (cFlowBtnData<TEdit,UnicodeString>*)pBtnData;
            cFilterPanelData<TEdit,UnicodeString>* pPanel = pData->CreateValuePanel(m_PanelsScroll);
            addValuePanel( pPanel );
            break;
        }

        case ADB_FIELDTYPE_DATETIME:
        {
            cFlowBtnData<TDateTimePicker,TDateTime>* pData = (cFlowBtnData<TDateTimePicker,TDateTime>*)pBtnData;
            cFilterPanelData<TDateTimePicker,TDateTime>* pPanel = pData->CreateValuePanel(m_PanelsScroll);
            addValuePanel( pPanel );
            break;
        }

        case ADB_FIELDTYPE_FILETYPE_NAME:
        {
            cFlowBtnData<TComboFlat,UnicodeString>* pData = (cFlowBtnData<TComboFlat,UnicodeString>*)pBtnData;
            cFilterPanelData<TComboFlat,UnicodeString>* pPanel = pData->CreateValuePanel(m_PanelsScroll);
            addValuePanel( pPanel );
            break;
        }

        default:
        {
            cFlowBtnData<TSpinEdit,int>* pData = (cFlowBtnData<TSpinEdit,int>*)pBtnData;
            cFilterPanelData<TSpinEdit,int>* pPanel = pData->CreateValuePanel(m_PanelsScroll);
            addValuePanel( pPanel );
            break;
        }
    }

    PageControl1->ActivePage = ListPage;
}
//---------------------------------------------------------------------------

void TArchiveFilterForm::addValuePanel( cIFilterPanelData* pPanel )
{

    pPanel->m_CloseBtn->OnClick = OnClosePanelBtnClicked;
    //m_PanelsScroll->InsertControl(pPanel->m_Panel);
    pPanel->m_Panel->Align = alTop;
    pPanel->m_Panel->Visible = true;
    pPanel->m_Panel->Top = 1;

    for(unsigned int i = 0; i < currValuePanels.size(); i++) //find empty
    {
        if(currValuePanels[i] == NULL)
        {
            pPanel->m_CloseBtn->Tag = i;
            currValuePanels[i] = pPanel;
            return;
        }
    }
    pPanel->m_CloseBtn->Tag = currValuePanels.size();
    currValuePanels.push_back(pPanel);
}

void __fastcall TArchiveFilterForm::OnClosePanelBtnClicked(TObject *Sender)
{
    TButton* pBtn = dynamic_cast<TButton*>(Sender);

    int nBtnData = pBtn->Tag;
    if((nBtnData < 0) || (nBtnData >= (int)currValuePanels.size()))
        return;
    if(!currValuePanels[ nBtnData ])
        return;

    deleteLaterValuePanelIndices.push_back( nBtnData );
    DeleteLaterTimer->Enabled = true;
}

void __fastcall TArchiveFilterForm::DeleteLaterTimerTimer(TObject *Sender)
{
    while(!deleteLaterValuePanelIndices.empty())
    {
        int idx = deleteLaterValuePanelIndices.back();
        deleteLaterValuePanelIndices.pop_back();

        currValuePanels[ idx ]->Release();
        delete currValuePanels[ idx ];
        currValuePanels[ idx ] = NULL;
    }

    while(!deleteLaterFlowBtnValues.empty())
    {
        TBitBtn* pBtn = deleteLaterFlowBtnValues.back();
        deleteLaterFlowBtnValues.pop_back();

        delete pBtn;
    }

    DeleteLaterTimer->Enabled = false;
}

bool TArchiveFilterForm::getQueries(std::vector<sAFDFilterQuery>& query_array)
{
    query_array.clear();
    flushDeleteLater();

    for(unsigned int i = 0; i < currValuePanels.size(); i++)
    {
        if(!currValuePanels[i])
            continue;

        query_array.push_back(currValuePanels[i]->GetQuery());
    }
    return true;
}

void TArchiveFilterForm::flushDeleteLater()
{
    //If there is items to delete
    if(!deleteLaterValuePanelIndices.empty() ||
        !deleteLaterFlowBtnValues.empty())
    {
        DeleteLaterTimerTimer(this); //Manually call 'delete later' timer
    }
}
//---------------------------------------------------------------------------

void __fastcall TArchiveFilterForm::ClearBtnClick(TObject *Sender)
{
    for(unsigned int i = 0; i < currValuePanels.size(); i++)
    {
        if(currValuePanels[i])
            deleteLaterValuePanelIndices.push_back( i );
    }
    flushDeleteLater();
}

//---------------------------------------------------------------------------


