//----------------------------------------------------------------------------
#ifndef ConsoleDlgUnitH
#define ConsoleDlgUnitH
//----------------------------------------------------------------------------
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <StdCtrls.hpp>
#include <Controls.hpp>
#include <Forms.hpp>
#include <Graphics.hpp>
#include <Classes.hpp>
#include <SysUtils.hpp>
#include <Windows.hpp>
#include <System.hpp>

#include "AutoconMain.h"
#include "ScanScriptParser.h"
#include <sstream>
//----------------------------------------------------------------------------
class TConsoleDlg : public TForm
{
__published:
	TPanel *Panel1;
	TButton *m_ConsoleExecBtn;
	TEdit *m_ConsoleEdit;
	TMemo *m_ConsoleMemo;
	TButton *m_ExecFromFileBtn;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall m_ConsoleExecBtnClick(TObject *Sender);
	void __fastcall m_ExecFromFileBtnClick(TObject *Sender);
private:
	cScanScriptProgramm scriptPrg;
	//std::ostringstream scriptOutput;
public:
	virtual __fastcall TConsoleDlg(TComponent* AOwner);
};
extern cAutoconMain *AutoconMain;
//----------------------------------------------------------------------------
extern PACKAGE TConsoleDlg *ConsoleDlg;
//----------------------------------------------------------------------------
#endif    
