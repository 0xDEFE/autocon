//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ConfigUnit.h";
extern cACConfig * ACConfig;

#include "BScanFrameUnit.h"
#include "AScanFrameUnit.h"
#include "ManualTuneFrameUnit.h"
#include "ArchiveSelectDialUnit.h"
#include "CalibrationFormUnit.h"
#include "AScanViewUnit.h"
#include "Autocon.inc"

#include "ScreenMessageUnit3.h"
#include "ConfigUnit.h"
extern cACConfig * ACConfig;
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BasicFrameUnit"
#pragma resource "*.dfm"
//TBScanFrame *BScanFrame;
//---------------------------------------------------------------------------
__fastcall TBScanFrame::TBScanFrame(TComponent* Owner)
    : TBasicFrame(Owner)
{
    aquiredSignalsFlags = MU_SIGNAL_AC_SEARCH_OR_TEST | MU_SIGNAL_AC_ADJUST | MU_SIGNAL_AC_TUNE | MU_SIGNAL_AC_MANUAL;
    MouseX = 0;
    MouseY = 0;
    AScanSysCoord = 0;
    OnBScanChannelBtnStateChanged = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TBScanFrame::FrameResize(TObject *Sender)
{
	BtnsPanel1->Left = BScanPBox->Width - 25 - BtnsPanel1->Width;
	BtnsPanel1->Top;
	BtnsPanel2->Left = BScanPBox->Width - 25 - BtnsPanel2->Width;
	BtnsPanel2->Top = BScanPBox->Height - BtnsPanel2->Height;


	int height = 0;
	height = std::max(height, CloseRepButton->Visible ? CloseRepButton->Top + CloseRepButton->Height : 0);
	height = std::max(height, HandScanButton->Visible ? HandScanButton->Top + HandScanButton->Height : 0);
	height = std::max(height, ManualCalibrationBtn->Visible ? ManualCalibrationBtn->Top + ManualCalibrationBtn->Height : 0);
//	height = std::max(height, MeasMovePanel->Visible ? MeasMovePanel->Top + MeasMovePanel->Height : 0);
	BtnsPanel1->Height = height;




//    CloseRepButton->Left = BScanPBox->Width - 25 - CloseRepButton->Width;
//    HandScanButton->Left = BScanPBox->Width - 25 - HandScanButton->Width;
//    ManualCalibrationBtn->Left = BScanPBox->Width - 25 - HandScanButton->Width;
//
//    ManualCalibrationBtn->Top = CloseRepButton->Top + CloseRepButton->Height + 4;

    m_DefectHgBtn->Top =    BScanPBox->Height - 25 - m_DefectHgBtn->Height;
    m_ActiveHgBtn->Top =    BScanPBox->Height - 25 - m_ActiveHgBtn->Height;
    m_NoHgBtn->Top =        BScanPBox->Height - 25 - m_NoHgBtn->Height;


    m_NoHgBtn->Left = 25;
    m_ActiveHgBtn->Left = m_NoHgBtn->BoundsRect.Right + 5;
    m_DefectHgBtn->Left = m_ActiveHgBtn->BoundsRect.Right + 5;

    BScanPainter.setBScanRect(TRect(0,0,BScanPBox->Width,BScanPBox->Height));
    UpdatePBoxData(false);
    DrawPBoxData();
    GroupBox1->Invalidate();
    Button18->Invalidate();

}
//---------------------------------------------------------------------------
void __fastcall TBScanFrame::DrawTimerTimer(TObject *Sender)
{

	if (DrawTimer->Tag == 0) UpdatePBoxData(true); //FDV
		else {
				UpdatePBoxData(false); //FDV
				DrawTimer->Tag = 0;   //FDV
			 }

    DrawPBoxData();
}
//---------------------------------------------------------------------------
void TBScanFrame::UpdatePBoxData(bool NewItemFlag)
{
    if(BScanPainter.isInitialized())
    	BScanPainter.updateBScan(NewItemFlag);
}
//��������� ������� �� �����
void TBScanFrame::DrawPBoxData()
{
	if(BScanPainter.isInitialized())
    	BScanPainter.drawBScan(BScanPBox->Canvas);
}
//---------------------------------------------------------------------------
void TBScanFrame::OnMFrameCreate(TBasicFrame* pMain)
{
	//AutoconMain->Rep->ClearData();
    BScanPainter.init(AutoconMain->Rep);
    HandScanButton->Enabled = false;
    ManualCalibrationBtn->Enabled = false;
    MeasMovePanel->Visible = false;
#ifdef DISABLE_DEFECT_HIGHLIGHTING
    m_DefectHgBtn->Visible = false;
#endif
}
void TBScanFrame::OnMFrameDestroy(TBasicFrame* pMain)
{
    //ClearData();
}
void TBScanFrame::OnMFrameAttach(TBasicFrame* pMain){
    //BScanPainter.setBScanRect(TRect(0,0,BScanPBox->Width,BScanPBox->Height));
    ThTrackBar->Position = ACConfig->BScan_ThTrackBarPos;
    UpdatePBoxData(false);
    DrawPBoxData();
    DrawTimer->Enabled = true;
    AutoconMain->Rep->UpdateChannelIndicationData();

    PathMgr::PushPath(L"$compare_data", L"resources\\measure");

    AnsiString path = PathMgr::GetPath((L"($compare_data)\\" + ACConfig->Test_EtalonModelName).c_str());
    if(etalonMeasure.load(path.c_str(), AutoconMain->Table,AutoconMain->Config))
    {
        currentMeasure.addDriver<AmplitudeMeasDrv_db>();
        currentMeasure.addDriver<AmplitudeMeasDrv_raw>();
        currentMeasure.addDriver<DelayMeasDrv>();
        currentMeasure.addDriver<MaxMeasDrv>();
        currentMeasure.addDriver<MinMeasDrv>();
        currentMeasure.addDriver<SumMeasDrv>();
        currentMeasure.addDriver<AverageMeasDrv>();

        currentMeasure.setRegionsFrom(etalonMeasure);


        BScanPainter.pScanDraw->SetMeasureData(&currentMeasure);
        BScanPainter.pScanDraw->EnableMeasureDataDraw(false);
    }
}
void TBScanFrame::OnMFrameDetach(TBasicFrame* pMain){
    DrawTimer->Enabled = false;
    AScanViewForm->OnCloseBtnClicked = NULL;
    AScanViewForm->OnLeftBtnClicked = NULL;
    AScanViewForm->OnRightBtnClicked = NULL;
    AScanViewForm->Visible = false;
}
void __fastcall TBScanFrame::CloseRepButtonClick(TObject *Sender)
{
	BScanPainter.pScanDraw->EnableMeasureDataDraw(false); //FDV
	BScanPainter.pScanDraw->SetMeasureData(NULL); //FDV
	pMainUnitForm->BackButton();
	pParentFrame->popFrame();
}
//---------------------------------------------------------------------------

void __fastcall TBScanFrame::ChangeLayoutBtnClick(TObject *Sender)
{
	BScanPainter.setBScanOrientation(!BScanPainter.getBScanOrientation());
	BScanPBox->Refresh();
	UpdatePBoxData(false);
	DrawPBoxData();
}
//---------------------------------------------------------------------------


void __fastcall TBScanFrame::m_NoHgBtnClick(TObject *Sender)
{
    BScanPainter.setHideChannelsMode(CFM_HIDE_NONE);
}
//---------------------------------------------------------------------------

void __fastcall TBScanFrame::m_ActiveHgBtnClick(TObject *Sender)
{
    if(frameFlags & MF_BSCAN_FLAG_HIDE_INACTIVE_CH)
        BScanPainter.setHideChannelsMode(CFM_HIDE_SELECTED);
    else
        BScanPainter.setHideChannelsMode(CFM_HIDE_INACTIVE);
}
//---------------------------------------------------------------------------

void __fastcall TBScanFrame::m_DefectHgBtnClick(TObject *Sender)
{
    BScanPainter.setHideChannelsMode(CFM_HIDE_WITH_NO_DEFECTS);
}
//---------------------------------------------------------------------------

void __fastcall TBScanFrame::ThTrackBarChange(TObject *Sender)
{
    UnicodeString TextList[17] = {"����", "-12", "-10", "-8", "-6", "-4", "-2", "0", "2", "4", "6", "8", "10", "12", "14", "16", "18"};
    int ValList[17] = {0, 8, 10, 12, 16, 20, 25, 32, 40, 50, 64, 80, 101, 128, 160, 202, 254};
    ThLabel->Caption = "����� �����������: " + TextList[ThTrackBar->Position];
    if (ThTrackBar->Position != 0) ThLabel->Caption = ThLabel->Caption + " ��";

    ACConfig->BScan_ThTrackBarPos  = ThTrackBar->Position;

    BScanPainter.pScanDraw->Th = ValList[ThTrackBar->Position];
    BScanPainter.pScanDraw->Draw(false);
}
//---------------------------------------------------------------------------
void TBScanFrame::ClearData()
{
    DrawTimer->Enabled = false;
    AutoconMain->Rep->ClearData(); // ������� ������
    DrawTimer->Enabled = true;

    //Added by KirillB
    BScanPainter.clearBScan();
    UpdatePBoxData(false);
    DrawPBoxData();
}

void TBScanFrame::OnAcMotion(eMainUnitSignalType sigtype, int WorkCycle, eMotionState state, bool bStateChanged)
{
    if(sigtype == MU_SIGNAL_AC_SEARCH_OR_TEST)
    {
        UnicodeString currCaptionNameStr;
        currCaptionNameStr = "����� ";

        switch (WorkCycle)
        {
            case 2: // ������ ������ - ����� �������
            {
                Button18->Caption = currCaptionNameStr+"(2 ��������)";
                AutoconMain->DEV->SetMode(dmSearch);
                AutoconMain->DEV->Update(false);
                AutoconMain->DEV->DisableAll();
                AutoconMain->XSysCoord_Center = AutoconMain->ac->GetCaretCoord(0); //Get real coord of joint center
                break;

            }
            case 3:
            {
                Button18->Caption = currCaptionNameStr+"(3 ��������)";
                #ifdef SIMULATION_CONTROLER_AND_MOTORS
                    cCaretMotionSim::Instance().ResetPathEncoder();
                #else
                    AutoconMain->DEV->ResetPathEncoder();
                #endif
                AutoconMain->DEV->SetChannelGroup(WorkCycle);
                AutoconMain->DEV->Update(true);
                AutoconMain->XSysCoord_Base = AutoconMain->ac->GetCaretCoord(0); //Get real coord
				TRACE("RealCoord[%d]: %d", WorkCycle, AutoconMain->XSysCoord_Base);
                Sleep(1000);
                break;
            };
            case 4:
			{
				DrawTimer->Tag = 1; //FDV - ������ ����������� �-���������

				Button18->Caption = currCaptionNameStr+"(4 ��������)";
				AutoconMain->DEV->DisableAll();
				break;
			};
			case 5:
			{
				Button18->Caption = currCaptionNameStr+"(5 ��������)";
				#ifdef SIMULATION_CONTROLER_AND_MOTORS
					cCaretMotionSim::Instance().ResetPathEncoder();
				#else
					AutoconMain->DEV->ResetPathEncoder();
				#endif
				AutoconMain->DEV->SetChannelGroup(WorkCycle);
				AutoconMain->DEV->Update(true);
				AutoconMain->XSysCoord_Base = AutoconMain->ac->GetCaretCoord(1); //Get real coord
				TRACE("RealCoord[%d]: %d", WorkCycle, AutoconMain->XSysCoord_Base);
				Sleep(1000);
				break;
			};
			case 6:
			{
				DrawTimer->Tag = 1; //FDV - ������ ����������� �-���������

				Button18->Caption = currCaptionNameStr+"(6 ��������)";
				#ifdef SIMULATION_CONTROLER_AND_MOTORS
					cCaretMotionSim::Instance().ResetPathEncoder();
				#else
					AutoconMain->DEV->ResetPathEncoder();
				#endif
				AutoconMain->DEV->SetChannelGroup(WorkCycle);
				AutoconMain->DEV->Update(true);
				AutoconMain->XSysCoord_Base = AutoconMain->ac->GetCaretCoord(1); //Get real coord
				TRACE("RealCoord[%d]: %d", WorkCycle, AutoconMain->XSysCoord_Base);
				Sleep(1000);
				break;
			};
			case 7:
			{
				DrawTimer->Tag = 1; //FDV - ������ ����������� �-���������

				Button18->Caption = currCaptionNameStr+"(7 ��������)";
				AutoconMain->DEV->DisableAll();
				break;
			};
			case 8:
			{
				Button18->Caption = currCaptionNameStr+"(8 ��������)";
				#ifdef SIMULATION_CONTROLER_AND_MOTORS
					cCaretMotionSim::Instance().ResetPathEncoder();
				#else
					AutoconMain->DEV->ResetPathEncoder();
				#endif
				AutoconMain->DEV->SetChannelGroup(WorkCycle);
				AutoconMain->DEV->Update(true);
				AutoconMain->XSysCoord_Base = AutoconMain->ac->GetCaretCoord(0); //Get real coord
				TRACE("RealCoord[%d]: %d", WorkCycle, AutoconMain->XSysCoord_Base);
				Sleep(1000);
				break;
			};
			case 9:
			{
				DrawTimer->Tag = 1; //FDV - ������ ����������� �-���������

				Button18->Caption = currCaptionNameStr+"(9 ��������)";
				AutoconMain->DEV->DisableAll();
				break;
			};
			case 10:
			{
				Button18->Caption = currCaptionNameStr+"(10 ��������)";
				#ifdef SIMULATION_CONTROLER_AND_MOTORS
					cCaretMotionSim::Instance().ResetPathEncoder();
				#else
					AutoconMain->DEV->ResetPathEncoder();
				#endif
				AutoconMain->DEV->SetChannelGroup(WorkCycle);
				AutoconMain->DEV->Update(true);
				AutoconMain->XSysCoord_Base = AutoconMain->ac->GetCaretCoord(0); //Get real coord
				TRACE("RealCoord[%d]: %d", WorkCycle, AutoconMain->XSysCoord_Base);
				Sleep(1000);
				break;
			};
			case 11:
			{
				DrawTimer->Tag = 1; //FDV - ������ ����������� �-���������

				Button18->Caption = currCaptionNameStr+"(11 ��������)";
				AutoconMain->DEV->DisableAll();
                break;
            };
            case 12:
            {
                Button18->Caption = currCaptionNameStr+"(12 ��������)";
                #ifdef SIMULATION_CONTROLER_AND_MOTORS
                    cCaretMotionSim::Instance().ResetPathEncoder();
                #else
                    AutoconMain->DEV->ResetPathEncoder();
                #endif
                AutoconMain->DEV->SetChannelGroup(WorkCycle);
                AutoconMain->DEV->Update(true);
                AutoconMain->XSysCoord_Base = AutoconMain->ac->GetCaretCoord(0);  //Get real coord
                TRACE("RealCoord[%d]: %d", WorkCycle, AutoconMain->XSysCoord_Base);
                Sleep(1000);
                break;
            };
            case 13:
			{
				DrawTimer->Tag = 1; //FDV - ������ ����������� �-���������

				Button18->Caption = currCaptionNameStr+"(13 ��������)";
                AutoconMain->DEV->DisableAll();
                break;
            };
            case 14:
			{
                Button18->Caption = currCaptionNameStr+"(14 ��������)";
                #ifdef SIMULATION_CONTROLER_AND_MOTORS
                    cCaretMotionSim::Instance().ResetPathEncoder();
                #else
                    AutoconMain->DEV->ResetPathEncoder();
                #endif
                AutoconMain->DEV->SetChannelGroup(WorkCycle);
                AutoconMain->DEV->Update(true);
                AutoconMain->XSysCoord_Base = AutoconMain->ac->GetCaretCoord(0); //Get real coord
                TRACE("RealCoord[%d]: %d", WorkCycle, AutoconMain->XSysCoord_Base);
                Sleep(1000);
                break;
            };
            case 15:
            {
				DrawTimer->Tag = 1; //FDV - ������ ����������� �-���������

                Button18->Caption = currCaptionNameStr+"(15 ��������)";
                AutoconMain->DEV->DisableAll();
                AutoconMain->Rep->CalcAlarmSignals();
                break;
            };
            case 16:
            {
                Button18->Caption = currCaptionNameStr;
                AutoconMain->DEV->DisableAll();
                break;
            };
        }

        if(bStateChanged && (state == MS_MOTION_END))
        {
            HandScanButton->Enabled = true;
            EndMotionTimer->Enabled = true;
        }
    }
    else if(sigtype == MU_SIGNAL_AC_TUNE)
    {
        UnicodeString currCaptionNameStr;
	    currCaptionNameStr =  (WorkCycle==-1) ? StringFormatU(L"��������� ��������") : StringFormatU(L"���������: %d%%", WorkCycle);
	    Button18->Caption =  currCaptionNameStr;

        if(bStateChanged && (state == MS_MOTION_END))
        {
            ManualCalibrationBtn->Enabled = true;
            EndMotionTimer->Enabled = true;
        }
    }
    else if(sigtype == MU_SIGNAL_AC_MANUAL)
    {
        UnicodeString currCaptionNameStr;
        if(WorkCycle==-1)
            currCaptionNameStr = L"�������� ��������";
        else
        {
            currCaptionNameStr = L"����. ���������: ";
            int cnt = WorkCycle % 5;
            for(int i = 0; i < cnt; i++)
                currCaptionNameStr += L'*';
        }
	    Button18->Caption =  currCaptionNameStr;
    }
}

void TBScanFrame::SetFrameFlags(DWORD flags)
{
    frameFlags = flags;

    if(frameFlags & MF_BSCAN_FLAG_CAPTION_CLOSE_PROTO)
        CloseRepButton->Caption = "������� ��������";
    else if(frameFlags & MF_BSCAN_FLAG_CAPTION_CLOSE)
        CloseRepButton->Caption = "�������";

    HandScanButton->Visible = frameFlags & MF_BSCAN_FLAG_HANDSCAN_BTN;
    ManualCalibrationBtn->Visible = frameFlags & MF_BSCAN_FLAG_MANUAL_TUNE_BTN;

    if(frameFlags & MF_BSCAN_FLAG_VISUALMODE_SEARCH)
        BScanPainter.setVisualMode(BS_VISUAL_SEARCH);
    else if(frameFlags & MF_BSCAN_FLAG_VISUALMODE_TUNE)
        BScanPainter.setVisualMode(BS_VISUAL_TUNE);
    else BScanPainter.setVisualMode(BS_VISUAL_UNKNOWN);

    if(frameFlags & MF_BSCAN_FLAG_ONLY_SHOW)
    {
        CloseRepButton->Visible = false;
        HandScanButton->Visible = false;
        ManualCalibrationBtn->Visible = false;
        //GroupBox1->Visible = fals;
        //aquiredSignalsFlags = 0;
    }
}
void __fastcall TBScanFrame::HandScanButtonClick(TObject *Sender)
{
    TAScanFrame* pFrame = pParentFrame->pushFrame<TAScanFrame>(MF_ASCAN_HAND_FLAGS & ~MF_ASCAN_FLAG_SINGLEUSE, false);
}
//---------------------------------------------------------------------------


void __fastcall TBScanFrame::BScanPBoxPaint(TObject *Sender)
{
    if(BScanPainter.isInitialized())
        DrawPBoxData();
}
//---------------------------------------------------------------------------

void __fastcall TBScanFrame::BScanPBoxMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
    MouseX = X;
    MouseY = Y;

	int id = - 1;
    int sl = -1;
    for (int idx = 1; idx < 9; idx++)
    {
        id = BScanPainter.click(idx, MouseX, MouseY);
        sl = idx;
        if (id != -1)
        {
            break;
        }
    }

     if (AutoconMain->DEV->ChannelInCurrentGroup(id) && (frameFlags & MF_BSCAN_FLAG_CHANNEL_BTN))
        if (id >= 0)
        {
            if (AutoconMain->DEV->SetChannel(id))
            {
                AutoconMain->DEV->SetSide(dsNone);
				AutoconMain->DEV->Update(false);
				TAScanFrame* pFrame = NULL;
				if(frameFlags & MF_BSCAN_FLAG_ASCAN_CALIB_MODE)
					pFrame = pParentFrame->pushFrame<TAScanFrame>(MF_ASCAN_CALIB_FLAGS,false);
				else
                    pFrame = pParentFrame->pushFrame<TAScanFrame>(MF_ASCAN_SEARCH_FLAGS,false);
                pFrame->AScanChart->BottomAxis->Maximum = pFrame->DelayToChartValue(AutoconMain->DEV->GetAScanLen());
                pFrame->ValueToControl();
            }
        }

	BScanPainter.pScanDraw->Prepare();
    if (id < - 1) // ���������� ���������� ���������� ������
    {
        if(OnBScanChannelBtnStateChanged)
        {
            _changedBtnId = abs(id);
            _changedBtnState = BScanPainter.scanLines[sl]->GetBtnState(abs(id));
            OnBScanChannelBtnStateChanged(this);
        }

		UpdatePBoxData(false);
        DrawPBoxData();
    }

    if (id == - 1) ShowAScanOnBScan(X, Y, -1);

    for (int idx = 1; idx < 9; idx++)
    {
		BScanPainter.scanLines[idx]->Refresh();
		BScanPainter.scanLines[idx]->PaintToBuffer();
	}
	DrawPBoxData();
}
//---------------------------------------------------------------------------

void __fastcall TBScanFrame::ManualCalibrationBtnClick(TObject *Sender)
{
    ArchiveSelectDial->AddComponent(0, "������������� ��������� �������");
    ArchiveSelectDial->AddComponent(1, "������ ���������");

    ArchiveSelectDial->SetDialCaption("�������� ��������:");
	int selBtnId = ArchiveSelectDial->ShowSelect();
    if(selBtnId == -1)
        return;

    if(selBtnId == 0)
    {
        AutoconMain->Rep->UpdateCtrlStateForTuneMode(AutoconMain->Table);
        for(unsigned int i = 0; i < AutoconMain->Rep->GetChannelCtrlDataCount(); i++)
        {
            switch(AutoconMain->Rep->GetChannelCtrlData(i)->controlState)
            {
                case -1:
                    CalibrationForm->HighlightChannel(i, clWhite);
                    break;
                case 0:
                    CalibrationForm->HighlightChannel(i, clMoneyGreen);
                    break;
                case 1:
                    CalibrationForm->HighlightChannel(i, (TColor)RGB(255,251,166));
                    break;
                case 2:
                    CalibrationForm->HighlightChannel(i, (TColor)RGB(255,200,200));
                    break;
            }
        }
        CalibrationForm->ShowModal();
    }
    else if(selBtnId == 1)
    {
        pParentFrame->pushFrame<TManualTuneFrame>(0);
    }
}
//---------------------------------------------------------------------------

void __fastcall TBScanFrame::EndMotionTimerTimer(TObject *Sender)
{
    EndMotionTimer->Enabled = false;

    if(frameFlags & MF_BSCAN_FLAG_VISUALMODE_TUNE)
    {
        ManualCalibrationBtn->Visible = true;
        ManualCalibrationBtn->Enabled = true;
        ManualCalibrationBtn->Click();
    }
    else if(frameFlags & MF_BSCAN_FLAG_COMPARE_WITH_MODEL)
    {

        #ifdef Replacement_Test_File                                      //FDV
        OpenDialog1->InitialDir = ExtractFilePath(Application->ExeName);  //FDV
        if (OpenDialog1->Execute())                                       //FDV
            AutoconMain->Rep->ReadFromFile(OpenDialog1->FileName, false); //FDV
        #endif                                                            //FDV

        MeasMovePanel->Visible = true;
        FrameResize(NULL);
        currentMeasure.calculate(AutoconMain->Rep);
        currentMeasure.compare(etalonMeasure);
        BScanPainter.pScanDraw->EnableMeasureDataDraw(true);
        UpdatePBoxData(false);
        DrawPBoxData();
       // TScreenMessageForm3::InfoMessageW(L"���������", L"��������� ��������� � ��������� �������.", 1500);
    }
}
//---------------------------------------------------------------------------
void __fastcall TBScanFrame::OnAScanFrameLeftBtnClick(TObject *Sender)
{
    AScanSysCoord--;
    ReDrawAScanOnBScan();
}
void __fastcall TBScanFrame::OnAScanFrameRightBtnClick(TObject *Sender)
{
    AScanSysCoord++;
    ReDrawAScanOnBScan();
}
void __fastcall TBScanFrame::OnAScanFrameCloseBtnClick(TObject *Sender)
{
    ReDrawAScanOnBScan();
}

void TBScanFrame::ShowAScanOnBScan(int X, int Y, int showInfoId)
{
    if (X > BScanPBox->Width / 2) AScanViewForm->Left = 0; else AScanViewForm->Left = BScanPBox->Width - AScanViewForm->Width;
    if (Y > BScanPBox->Height / 2) AScanViewForm->Top = 0; else AScanViewForm->Top = BScanPBox->Height - AScanViewForm->Height;
    AScanViewForm->OnLeftBtnClicked = OnAScanFrameLeftBtnClick;
    AScanViewForm->OnRightBtnClicked = OnAScanFrameRightBtnClick;
    AScanViewForm->OnCloseBtnClicked = OnAScanFrameCloseBtnClick;



    int MouseX = X;
    int MouseY = Y;

    if(showInfoId != -1)
    {
        //UnicodeString chName, int Ku, int Att, int StGate, int EdGate, int TVG, int PrismDelay

        sChannelDescription chDesc;
        if(AutoconMain->Table->ItemByCID(showInfoId, &chDesc))
        {
            UnicodeString chName = chDesc.Title;
            showInfoId -= 0x3F;
            AScanViewForm->setInfoMode(true);
            sScanChannelParams params_gate1, params_gate2;
            int gateCnt = AutoconMain->Table->GetGateCount(showInfoId + AutoconStartCID);
            params_gate1 = AutoconMain->Rep->GetChannelParams(showInfoId + AutoconStartCID, 1);
            if(gateCnt == 2)
                params_gate2 = AutoconMain->Rep->GetChannelParams(showInfoId + AutoconStartCID, 2);
            AScanViewForm->setInfo(chName, &params_gate1, (gateCnt == 2) ? &params_gate2 : NULL);
        }
    }
    else
    {
        AScanViewForm->setInfoMode(false);
        if (BScanPainter.pScanDraw->GetSysCoord(MouseX, MouseY, &AScanSysCoord, &Shift_CID, &Shift_CID_Cnt, &BSColor))
        {
            BScanPainter.pScanDraw->SetCursorXY(true,MouseX,MouseY,clRed);
            ReDrawAScanOnBScan();
            AScanViewForm->Visible = true;
        }
        else
        {
            AScanViewForm->Visible = false;
            BScanPainter.pScanDraw->SetCursorSC(false);
            DrawPBoxData();
        }
    }
}

void TBScanFrame::ReDrawAScanOnBScan()
{
    const unsigned char TableDB[256]=
    {
     0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x11,0x11,0x11,0x22,0x22,0x22,
     0x33,0x33,0x33,0x33,0x44,0x44,0x44,0x44,0x44,0x44,0x55,0x55,0x55,0x55,0x55,0x55,
     0x66,0x66,0x66,0x66,0x66,0x66,0x66,0x66,0x66,0x77,0x77,0x77,0x77,0x77,0x77,0x77,
     0x77,0x77,0x77,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,
     0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,
     0x99,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,
     0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,
     0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,
     0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,
     0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,
     0xCC,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,
     0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,
     0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,
     0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,
     0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,
     0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xFF
    };

    const int AmplId_to_DB[16] = { -12, -10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10, 12, 14, 16, 18 };

    bool res;

    // ����� �-��������� ��� ���������� ���������� �����

    AScanViewForm->AScanSeries1->Visible = false;
    AScanViewForm->AScanSeries2->Visible = false;
    AScanViewForm->AScanSeries3->Visible = false;
    AScanViewForm->AScanSeries4->Visible = false;
    AScanViewForm->AScanSeries5->Visible = false;
    AScanViewForm->AScanSeries6->Visible = false;


    AScanViewForm->Panel5->Caption = " ����������: " + IntToStr(AScanSysCoord - ScanLen/2) + " ��"; //  + " Cnt:" + IntToStr(Shift_CID_Cnt);

    sChannelDescription tmp;
    unsigned char buff[768];
    int Index;
    unsigned int aidx2;
    PsScanSignalsOneCrdOneChannel sgl;
    TLineSeries *LineSer;
    int LinkArr[24] = {0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7};

    AScanViewForm->removeGateButtons();
    for (int chidx = 0; chidx < Shift_CID_Cnt; chidx++)
    {
        //set gates
        sChannelDescription chDesc;
        CID absId = Shift_CID[chidx] + 0x3F;
		if(!AutoconMain->Table->ItemByCID(absId,&chDesc))
			continue;

		int stGate[2];
        int edGate[2];
        stGate[0] = AutoconMain->Calibration->GetStGate(dsNone, absId, 0);
        edGate[0] = AutoconMain->Calibration->GetEdGate(dsNone, absId, 0);
        stGate[1] = AutoconMain->Calibration->GetStGate(dsNone, absId, 1);
        edGate[1] = AutoconMain->Calibration->GetEdGate(dsNone, absId, 1);
        UnicodeString chName = chDesc.Title;

        AScanViewForm->setGateBtn(chidx, chName /*chDesc.Title*/, BSColor[chidx], chDesc.cdGateCount, stGate[0], edGate[0], stGate[1], edGate[1]);
        //
        //

        switch (chidx)
        {
            case 0: { LineSer = AScanViewForm->AScanSeries1; break; }
            case 1: { LineSer = AScanViewForm->AScanSeries2; break; }
            case 2: { LineSer = AScanViewForm->AScanSeries3; break; }
			case 3: { LineSer = AScanViewForm->AScanSeries4; break; }
            case 4: { LineSer = AScanViewForm->AScanSeries5; break; }
            case 5: { LineSer = AScanViewForm->AScanSeries6; break; }
        }
        LineSer->Clear();
        LineSer->Color = BSColor[chidx];
        sgl = AutoconMain->Rep->GetFirstScanSignals(AScanSysCoord, Shift_CID[chidx], &res);
        if (res)
        {
            memset(&(buff[0]), 0, 768);
            for (int idx = 0; idx < sgl->Count; idx++)
            {
                for (int aidx = 0; aidx < 24; aidx++)
                {
                    Index = (sgl->Signals[idx].Delay * 3) / chDesc.cdBScanDelayMultiply + (aidx - 11);
                    if (AScanViewForm->DelayCountCheckBox->Checked) aidx2 = LinkArr[aidx]; else aidx2 = aidx;
                    if ((Index >= 0) && (Index < 768)) buff[Index] = sgl->Signals[idx].Ampl[aidx2];
                }
            }

            AutoconMain->Table->ItemByCID(Shift_CID[chidx] + AutoconStartCID, &tmp);
            AScanViewForm->AscanChart->BottomAxis->SetMinMax(0, tmp.cdScanDuration);

            LineSer->Visible = true;
            for (int idx = 0; idx < 768; idx++)
                if (buff[idx] != 0)
                {
					if (AScanViewForm->ValueCountCheckBox->Checked)
                        LineSer->AddXY((float)idx / (float)3, AmplId_to_DB[TableDB[buff[idx]] >> 4]);
                    else
                        LineSer->AddXY((float)idx / (float)3, (float)20 * log10((float)buff[idx] / (float)32));
                } else LineSer->AddXY((float)idx / (float)3, -20);
        }
    }

    AScanViewForm->updateButtons();

    BScanPainter.pScanDraw->SetCursorSC(true,AScanSysCoord,-1,-1,clRed);
    DrawPBoxData();
}

void TBScanFrame::updateHighlightedChannels()
{
    BScanPainter.updateBScanHideChannelsMode();
}

void TBScanFrame::disableChannels()
{
    BScanPainter.setAllChannelsHide(true);
    BScanPainter.updateBScanHideChannelsMode();
}

void TBScanFrame::enableChannel(CID idx, bool bVal)
{
    BScanPainter.setChannelHide(idx, !bVal);
}

void __fastcall TBScanFrame::ThTrackBarLeftBtnClick(TObject *Sender)
{
    ThTrackBar->Position--;
}
//---------------------------------------------------------------------------

void __fastcall TBScanFrame::ThTrackBarRightBtnClick(TObject *Sender)
{
    ThTrackBar->Position++;
}
//---------------------------------------------------------------------------

void __fastcall TBScanFrame::moveMeasModel(TObject *Sender)
{
	TBitBtn* pBtn = dynamic_cast<TBitBtn*>(Sender);
    int offset;
	bool FLG1;

    if ((pBtn->Tag == 1) || (pBtn->Tag == 2))
    {
        FLG1 = true;
        offset = (pBtn->Tag == 1) ? -1 : (pBtn->Tag == 2) ? 1 : 0;
    }
    else
    {
        FLG1 = false;
        offset = (pBtn->Tag == 3) ? -1 : (pBtn->Tag == 4) ? 1 : 0;
    }

	CID tmp;
	bool FLG;

    for(unsigned int i = 0; i < currentMeasure.regions.size(); i++)

		if (currentMeasure.regions[i].measureData.size() != 0)    //FDV
		{                                                         //FDV
			tmp = *currentMeasure.regions[i].measureData[0].channels.begin();    //FDV
																		  //FDV
			FLG = (((tmp >= 0x3F) &&                                      //FDV
				 (tmp <= 0x56)) ||                                        //FDV
																		  //FDV
				((tmp >= 0x5E) &&                                         //FDV
				 (tmp <= 0x68)));                                         //FDV
																		  //FDV
			if (FLG == FLG1)                                                     //FDV
            {
                PolyReg& poly = currentMeasure.regions[i].poly;
                poly.offset(PolyPt(offset, 0));
            }
        }

    currentMeasure.calculate(AutoconMain->Rep);
    currentMeasure.compare(etalonMeasure);

    UpdatePBoxData(false);
    DrawPBoxData();
}
//---------------------------------------------------------------------------

void __fastcall TBScanFrame::SpeedButton1Click(TObject *Sender)
{
	currentMeasure.calculate(AutoconMain->Rep);

	sChannelDescription chanDesc;
	UnicodeString ChannelName; //!< ��� ������
	int Count = 0;
	int OKCount = 0;

	for (int i = 0; i <= 90; i++)
	{
	  for (int p = 0; p < currentMeasure.ResList[i].PolyCount; p++)
		if (ACConfig->BScanMeasure[i][p])
		{
			Count++;
			if (currentMeasure.ResList[i].EchoCount[p] > 0) OKCount++;
		}
	}
	if ((int)((100 * OKCount) / Count) >= ACConfig->BScanMeasureTh)
	{
		#ifdef TestRailFull�onclusion
		Panel9->Caption = "��������";
		Panel5->Caption = "����� � ������";
		ACConfig->Last_�onclusion = "�������� ����� � ������";
		#endif

		#ifndef TestRailFull�onclusion
		Panel5->Caption = Format("%d%%", ARRAYOFCONST(((int)((100 * OKCount) / Count))));
		ACConfig->Last_�onclusion = Format("%d", ARRAYOFCONST(((int)((100 * OKCount) / Count))));
		#endif

	}
	else
	{
		#ifdef TestRailFull�onclusion
		Panel9->Caption = "�������� � ������";
		Panel5->Caption = Format("�� ����� (%d%%)", ARRAYOFCONST(((int)((100 * OKCount) / Count))));
		ACConfig->Last_�onclusion = Format("�������� � ������ �� ����� (%d%%)", ARRAYOFCONST(((int)((100 * OKCount) / Count))));
		#endif

		#ifndef TestRailFull�onclusion
		Panel5->Caption = Format("%d%%", ARRAYOFCONST(((int)((100 * OKCount) / Count))));
		ACConfig->Last_�onclusion = Format("%d", ARRAYOFCONST(((int)((100 * OKCount) / Count))));
		#endif

	}

}
//---------------------------------------------------------------------------

