//---------------------------------------------------------------------------

#pragma hdrstop

#include "AutoconMain.h"
//---------------------------------------------------------------------------

#ifndef SAFE_DELETE
#define SAFE_DELETE(p)       { if (p) { delete (p);     (p)=NULL; } }
#endif


cAutoconMain::cAutoconMain(void)
{
    UMU_Connection_OK = false;
	CtrlWork = false;
	QryThreadFlag = false;

	DT = NULL;
	DEV = NULL;
	Table = NULL;
	Config = NULL;
	Calibration = NULL;
	DeviceEventManage = NULL;
	UMUEventManage = NULL;
	threadList = NULL;
	Rep = NULL;
	CS = NULL;
	devt = NULL;
	ac = NULL;
	cavtk_test = NULL;

    am_flags = 0;

    Create();
}

cAutoconMain::~cAutoconMain(void)
{
    Release();
}


void cAutoconMain::Create()
{
    Release();
    UMU_Connection_OK = false;
	CtrlWork = false;
	QryThreadFlag = false;

	DT = NULL;
	DEV = NULL;
	Table = NULL;
	Config = NULL;
	Calibration = NULL;
	DeviceEventManage = NULL;
	UMUEventManage = NULL;
	threadList = NULL;
	Rep = NULL;
	CS = NULL;
	devt = NULL;
	ac = NULL;
	cavtk_test = NULL;

  // ���� ��������� �����������

    XSysCoord_Base = 0;
    XSysCoord_Center = 0;

	RegCrdParamsList[0].Used = false;
	RegCrdParamsList[1].Used = false;
	RegCrdParamsList[2].Used = false;

	RegCrdParamsList[3].StartCoord = -465;
	RegCrdParamsList[3].RegLength = 860;
	RegCrdParamsList[3].Used = true;

    RegCrdParamsList[4].Used = false;

	RegCrdParamsList[5].StartCoord = -435;
    RegCrdParamsList[5].RegLength = 830;
    RegCrdParamsList[5].Used = true;

	//Modifyed by KirillB (fix)
	RegCrdParamsList[6].StartCoord = 420;
	//RegCrdParamsList[6].StartCoord = -315;
	RegCrdParamsList[6].RegLength = 840;
	RegCrdParamsList[6].Used = true;

	RegCrdParamsList[7].Used = false;

    RegCrdParamsList[8].StartCoord = 25;
    RegCrdParamsList[8].RegLength = 180;
	RegCrdParamsList[8].Used = true;

	RegCrdParamsList[9].Used = false;

	RegCrdParamsList[10].StartCoord = -155;
    RegCrdParamsList[10].RegLength = 180;
    RegCrdParamsList[10].Used = true;

	RegCrdParamsList[11].Used = false;

    RegCrdParamsList[12].StartCoord = 266;
    RegCrdParamsList[12].RegLength = 161;
	RegCrdParamsList[12].Used = true;

    RegCrdParamsList[13].Used = false;

	RegCrdParamsList[14].StartCoord = 105;
    RegCrdParamsList[14].RegLength = 161;
	RegCrdParamsList[14].Used = true;

    RegKPOffsets[1] = 155;
    RegKPOffsets[2] = -105;
    RegKPOffsets[3] = -105;
    RegKPOffsets[4] = 95;
    RegKPOffsets[5] = 95;
    RegKPOffsets[6] = -105;
    RegKPOffsets[7] = -105;
    RegKPOffsets[8] = 155;

    //Initialize
    Table = new cChannelsTable;
    Config = new cDeviceConfig_Autocon(Table, 32, 16);

    CS = new cCriticalSectionWin();

    TDateTime curDate = TDateTime::CurrentDateTime();
    AnsiString str = UnicodeString("log_")+curDate.FormatString("dd-mm-yyyy_hh-mm-ss") + L".html";
    logger.Init(str.c_str(),"Autocon Log");
    logger.setChannelMask(Logger::CHANNEL_ID_ALL);

    Calibration = new cDeviceCalibration("calibration.dat", Table, CS);
//    Calibration->loadValidationInfo();
    Calibration->SetGateMode(gmSearch);

    for(int i = Calibration->Count(); i < 3; i++)
    {
        Application->MessageBoxW(StringFormatU(L"��������� ���������: %d",i).c_str(),L"Info");
        Calibration->CreateNew();
    }

    Calibration->SetNameByIndex("��������� ���������",0);
    Calibration->SetNameByIndex("��������� �1",1);
    Calibration->SetNameByIndex("��������� �2",2);

    Calibration->SetCurrent(0);
    Calibration->SetReadOnly();
    Calibration->SetCurrent(1);



    /*if (Calibration->Count() == 0)
    {
        Calibration->CreateNew();
        Calibration->SetCurrent(0);
    }
    else
    {
        Calibration->SetCurrent(0);
    }*/

    Rep = new cJointTestingReport(ScanLen);
}



void cAutoconMain::Release()
{
//	TextLog->Add("~cAutoconMain"); TextLog->SaveToFile("log.txt");

//	TextLog->Add("delete cavtk_test"); TextLog->SaveToFile("log.txt");
	if ((cavtk_test) && (cavtk_test->Started) && (!cavtk_test->Finished))
	{
		cavtk_test->EndWorkFlag = true;
		while (!cavtk_test->Finished) { Application->ProcessMessages(); };
		delete cavtk_test;
		cavtk_test = NULL;
	}

    logger.Release();

	UMU_Connection_OK = false;


  //	TextLog->Add("delete devt"); TextLog->SaveToFile("log.txt");
	if ((devt) && (devt->Started) && (!devt->Finished))
	{
		devt->EndWorkFlag = true;
		while (!devt->Finished) { Application->ProcessMessages(); };
		delete devt;
	}

   //	TextLog->Add("delete DEV"); TextLog->SaveToFile("log.txt");
//    DEV->EndWork();
    SAFE_DELETE( DEV );

    if(CS && DeviceEventManage)
        CleanupEventMgr();

    SAFE_DELETE( threadList );
    SAFE_DELETE( DeviceEventManage );
    SAFE_DELETE( UMUEventManage );
    SAFE_DELETE( CS );
//    if(Calibration)
//        Calibration->storeValidationInfo();
    SAFE_DELETE( Calibration );
    SAFE_DELETE( Table );
    SAFE_DELETE( Config );
    SAFE_DELETE( Rep );
    SAFE_DELETE( ac );
    CtrlWork = false;
    SAFE_DELETE( DT );

}

bool cAutoconMain::OpenConnection(int ModeIdx)
{

	// -----------------------------------------------------------------------------

	//Logger::Info(Logger::CHANNEL_ID_ALL,"Started");
	int res = - 1;
    TDataChannelIdList IdList;


	// ---------- ����� �������� ������ -----------------

	DT = new cDataTransfer();

	// ---------- ���������� � ���-�� -------------------
    #ifndef SIMULATION

	if (ModeIdx == 0)
	{
		LOGINFO("New connection (mode-%d): 192.168.100.100", ModeIdx);
		IdList[0] = DT->AddConnection("", "192.168.100.100", 43001, 43000, false, true);
		UMU_Connection_OK = (IdList[0] >= 0);
	}
	if (ModeIdx == 1)
	{
		LOGINFO("New connection (mode-%d): 192.168.100.101", ModeIdx);
		IdList[0] = DT->AddConnection("", "192.168.100.101", 43001, PORT_out, false, true);
		UMU_Connection_OK = (IdList[0] >= 0);
	}
	if (ModeIdx == 2)
	{
		LOGINFO("New connection (mode-%d): 192.168.100.102", ModeIdx);
		IdList[0] = DT->AddConnection("", "192.168.100.102", 43002, PORT_out, false, true);
		UMU_Connection_OK = (IdList[0] >= 0);
	}
	if (ModeIdx == 3)
	{
		LOGINFO("New connection (mode-%d): 192.168.100.103", ModeIdx);
		IdList[0] = DT->AddConnection("", "192.168.100.103", 43003, PORT_out, false, true);
		UMU_Connection_OK = (IdList[0] >= 0);
	}
	if (ModeIdx == 4)
	{
		LOGINFO("New connection (mode-%d): 192.168.100.101", ModeIdx);
		LOGINFO("New connection (mode-%d): 192.168.100.102", ModeIdx);
		LOGINFO("New connection (mode-%d): 192.168.100.103", ModeIdx);
	   IdList[0] = DT->AddConnection("", "192.168.100.101", 43001, 43000, false, true);
	   IdList[1] = DT->AddConnection("", "192.168.100.102", 43002, 43000, false, true);
	   IdList[2] = DT->AddConnection("", "192.168.100.103", 43003, 43000, false, true);
	   if ((IdList[0] >= 0) && (IdList[1] >= 0) && (IdList[0] >= 0)) UMU_Connection_OK = true;
	}
	if (ModeIdx == 5)
	{
		LOGINFO("New connection (mode-%d): 192.168.100.100", ModeIdx);
		LOGINFO("New connection (mode-%d): 192.168.100.102", ModeIdx);
		LOGINFO("New connection (mode-%d): 192.168.100.103", ModeIdx);
	   IdList[0] = DT->AddConnection("", "192.168.100.100", 43001, 43000, false, true);
	   IdList[1] = DT->AddConnection("", "192.168.100.102", 43002, 43000, false, true);
	   IdList[2] = DT->AddConnection("", "192.168.100.103", 43003, 43000, false, true);
	   if ((IdList[0] >= 0) && (IdList[1] >= 0) && (IdList[2] >= 0)) UMU_Connection_OK = true;
	}
	if (ModeIdx == 6)
	{
		LOGINFO("New connection (mode-%d): 192.168.100.100", ModeIdx);
		LOGINFO("New connection (mode-%d): 192.168.100.102", ModeIdx);
	   IdList[0] = DT->AddConnection("", "192.168.100.100", 43001, 43000, false, true);
	   IdList[1] = DT->AddConnection("", "192.168.100.102", 43002, 43000, false, true);
	   if ((IdList[0] >= 0) && (IdList[1] >= 0)) UMU_Connection_OK = true;
	}
	#endif

	#ifdef SIMULATION
	LOGINFO_NF("Running in simulation mode");
	UMU_Connection_OK = true;
	IdList[0] = 0;
	IdList[1] = 1;
	IdList[2] = 2;
	#endif

	if(!UMU_Connection_OK)
	{
		LOGERROR("Failed to connect in ModeIdx = %d",ModeIdx);
    }

	// ���������� + ������ ---------------------------------------------------------

	// ���������� + ������ ---------------------------------------------------------

	ac = new cAController;
//	#ifndef SIMULATION_CONTROLER_AND_MOTORS
	CtrlWork = ac->Init(DT);
//	#endif
//	#ifdef SIMULATION_CONTROLER_AND_MOTORS
//    CtrlWork = true;
//	#endif
	if(!CtrlWork)
		LOGERROR_NF("Failed to initialize AC controller");
	else
		LOGINFO_NF("AC controller initialized");

	// -----------------------------------------------------------------------------

	if ((UMU_Connection_OK) && CtrlWork)
	{
		threadList = new cThreadClassList_Win;


		DeviceEventManage = new cEventManagerWin();
		UMUEventManage = new cEventManagerWin();

		DEV = new cDevice(threadList,
						  DT,
                          IdList,
						  Config,
						  Calibration,
						  Table,
						  (cEventManager*)DeviceEventManage,
						  (cEventManager*)UMUEventManage,
						  (cCriticalSection*)CS);

		devt = new cDeviceThread(this);
		devt->Resume();

		cavtk_test = new cTestThread(this);

        DEV->StartWork();
	}
	else
	{

		// ��� ������� ����������� ��������� ����������

		if (IdList[0] >= 0) DT->closeConnections(IdList[0]);
		if (IdList[1] >= 0) DT->closeConnections(IdList[1]);
		if (IdList[2] >= 0) DT->closeConnections(IdList[2]);

	// ���������� + ������ ---------------------------------------------------------

		delete ac;
        ac = NULL;

		if (res >= 0) DT->closeConnections(res);



		delete DT;
		DT = NULL;
	}

 	// -----------------------------------------------------------------------------


	return CtrlWork && UMU_Connection_OK;
}
// ---------------------------------------------------------------------------

/*void cAutoconMain::SetMode(int Mode_)
{
    ControlMode = Mode_;
}*/

void cAutoconMain::SetReportFileType(int Type)
{
    ReportFileType = Type;
}

/*int cAutoconMain::GetMode(void)
{
    return ControlMode;
}*/

void cAutoconMain::CleanupEventMgr()
{
    DWORD DataID;
    uPtrListPointer Ptr1;
    uPtrListPointer Ptr2;
    EventDataType DataType;

    CS->Enter();
    while (DeviceEventManage->EventDataSize() >= 12)
    {
        if (DeviceEventManage->ReadEventData(&DataID, 4, NULL))
        {
            switch(DataID)
            {
                case edAScanMeas: // �-���������, ������ �������� ��������� � ��������
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        Ptr2.pVoid = NULL;
                        DataType = edAScanMeas;
                        delete Ptr1.pAScanMeasure;
                        break;
                    }
                case edAScanData: // AScan
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                        DataType = edAScanData;
                        delete Ptr1.pAScanHead;
                        delete Ptr2.pAScanData;
                        break;
                    }
                case edTVGData: // TVG
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                        DataType = edTVGData;
                        delete Ptr1.pAScanHead;
                        delete Ptr2.pAScanData;
                        break;
                    }
                case edAlarmData:  // ������ ���
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                        DataType = edAlarmData;
                        delete Ptr1.pAlarmHead;
                        delete Ptr2.pAlarmItem;
                        break;
                    }
                case edBScan2Data: // ������ �-��������� (��� 2)
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                        DataType = edBScan2Data;
                        delete Ptr1.pBScan2Head;
                        delete Ptr2.pBScanData;
                        break;
                    }
                case edMScan2Data:  // ������ �-��������� (��� 2)
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                        DataType = edMScan2Data;
                        delete Ptr1.pBScan2Head;
                        delete Ptr2.pBScanData;
                        break;
                    }
            }
        }
    }
    CS->Release();
}

// --- DeviceThread -----------------------------------------------------------

__fastcall cDeviceThread::cDeviceThread(cAutoconMain *main_) :
    TThread(false),
    Ptr_List(DEVTHREAD_LIST_SIZE)
{
	main = main_;
	EndWorkFlag = False;
    InTick = 0;

    #ifdef TextLog_def
	TextLog = new TStringList();
    #endif
}

__fastcall cDeviceThread::~cDeviceThread(void)
{
    #ifdef TextLog_def
    TextLog->SaveToFile("Device Thread log.txt");
    delete TextLog;
    #endif
}



void __fastcall cDeviceThread::Execute(void)
{
    uPtrListPointer Ptr1;
    uPtrListPointer Ptr2;
    EventDataType DataType;

	while (!EndWorkFlag)
    {
		unsigned long EventId;

		if (main->DeviceEventManage->WaitForEvent())
		{
			while (main->DeviceEventManage->EventDataSize() >= 12)
			{
                main->CS->Enter();
				if (main->DeviceEventManage->ReadEventData(&DataID, 4, NULL))
				{
					//Lock (write to PtrList)
				  //	PtrListRWLock.Enter();
				  /*	if(Count_ptr>=DEVTHREAD_LIST_SIZE)
					{
						TRACE("Count_ptr>=DEVTHREAD_LIST_SIZE - %d",Count_ptr);
					} */
					switch(DataID)
					{
						case edAScanMeas: // �-���������, ������ �������� ��������� � ��������
							{
								main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
								main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
								Ptr2.pVoid = NULL;
								DataType = edAScanMeas;
                                //delete Ptr1.pAScanMeasure;
                                //break;

								//Added by KirillB
								//-------------Add channel maximum (tune)-----------
								/*if (main->ac->GetWorkCycle() != - 1)
								{
                                    if(Ptr_List_1[Ptr_List_Put_Idx])
                                    {
                                        main->Rep->AddChannelMaximum(((PtDEV_AScanMeasure)Ptr_List_1[Ptr_List_Put_Idx])->Channel,
                                                ((PtDEV_AScanMeasure)Ptr_List_1[Ptr_List_Put_Idx])->ParamA);
                                    }
								}*/

								//--------------------------------------------------
                                sPtrListItem item(Ptr1,Ptr2,DataType);
                                Ptr_List.push_back(item);
                                //Inc_Ptr_List_Put_Idx();
								break;
							}
						case edAScanData: // AScan
							{
								main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
								main->DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
								DataType = edAScanData;
                                //delete Ptr1.pAScanHead;
                                //delete Ptr2.pAScanData;
                                //break;

                                sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);
								break;
							}
						case edTVGData: // TVG
							{
								main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
								main->DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
								DataType = edTVGData;
                                //delete Ptr1.pAScanHead;
                                //delete Ptr2.pAScanData;
                                //break;

								sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);
								break;
							}
						case edAlarmData:  // ������ ���
							{
								//PtDEV_AlarmHead    Head;
								//PtUMU_AlarmItem    Item;

								main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
								main->DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                                DataType = edAlarmData;
                                //delete Ptr1.pAlarmHead;
                                //delete Ptr2.pAlarmItem;
                                //break;
								//Ptr_List_1[Ptr_List_Put_Idx] = Head;
								//Ptr_List_2[Ptr_List_Put_Idx] = Item;
								//Id_List[Ptr_List_Put_Idx] = edAlarmData;
								//Inc_Ptr_List_Put_Idx();

                                sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);

								// ���������� ��������
								if ((main->ac->GetWorkCycle() != - 1) ||
                                        (main->ac->GetMode() == acHandScan))
								{
									sAlarmRecord tmp;
                                    sChannelDescription chanDesc;
                                	for (unsigned int idx_ = 0; idx_ < Ptr1.pAlarmHead->Items.size(); idx_++)
									{
										int curChannelGroup = main->DEV->GetChannelGroup();
										tmp.Side = Ptr1.pAlarmHead->Items[idx_].Side;
										tmp.Channel = Ptr1.pAlarmHead->Items[idx_].Channel;

                                        main->Table->ItemByCID(tmp.Channel, &chanDesc); //���������� ������ �������� � ��������� ������ ���
										memcpy(tmp.State, Ptr1.pAlarmHead->Items[idx_].State, 4 * sizeof( bool ));

                                        /* //FDV
										if (main->RegCrdParamsList[curChannelGroup].Used)
											main->Rep->AddAlarmSignals( Ptr1.pAlarmHead->Items[idx_].Channel - AutoconStartCID, tmp);
                                        else*/ if(main->DEV->GetChannelType() == ctHandScan)
                                            main->Rep->AddAlarmSignals( 0, tmp, true);
                                    }
                                }

								break;
							}
						case edBScan2Data: // ������ �-��������� (��� 2)
							{
                                main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                                main->DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                                DataType = edBScan2Data;

                                sPtrListItem item(Ptr1,Ptr2,DataType);
                                Ptr_List.push_back(item);

                                //Hack for fast channel search
                                static std::vector<int> chanDescCache;

                                // Caret Sim
                                #ifdef SIMULATION_CONTROLER_AND_MOTORS
                                Ptr2.pBScanData->XSysCrd[0] = cCaretMotionSim::Instance().GetPathEncoderData(0);
                                Ptr2.pBScanData->XSysCrd[1] = cCaretMotionSim::Instance().GetPathEncoderData(1);
                                Ptr2.pBScanData->Dir[0] = cCaretMotionSim::Instance().GetPathEncoderDir(0);
                                Ptr2.pBScanData->Dir[1] = cCaretMotionSim::Instance().GetPathEncoderDir(1);
                                #endif

                                if ((main->ac->GetWorkCycle() == - 1) &&
                                    (main->ac->GetMode() != acAdjusting1) &&
                                    (main->ac->GetMode() != acAdjusting2) &&
                                    (main->ac->GetMode() != acTuning) &&
                                    (main->DEV->GetChannelType() != ctHandScan))
                                {
                                    break;
                                }

                                sScanSignalsOneCrdOneChannel tmp;
                                sHandSignalsOneCrd handSig;
                                PtUMU_BScanSignalList ptr;
                                int transformedCrd = 0;
                                int transformOffset = 0;
                                const bool bIsHandBScanEnabled = (main->DEV->GetChannelType() == ctHandScan);
                                const int curChannelGroup = main->DEV->GetChannelGroup();
                                for (unsigned int idx_ = 0; idx_ < Ptr1.pBScan2Head->Items.size(); idx_++)
                                {
                                    ptr = Ptr1.pBScan2Head->Items[idx_].BScanDataPtr;

                                    if(bIsHandBScanEnabled)
                                    {
                                        handSig.Count = Ptr1.pBScan2Head->Items[idx_].Count;
                                        for(int i = 0; i < handSig.Count; i++)
                                            handSig.Signals[i] = (*ptr)[i];
                                        main->Rep->StoreScanSignal(handSig);
                                    }
                                    else
                                    {
                                        tmp.Count = Ptr1.pBScan2Head->Items[idx_].Count;
                                        for(int i = 0; i < tmp.Count; i++)
                                            tmp.Signals[i] = (*ptr)[i];
                                        tmp.WorkCycle = main->ac->GetWorkCycle();

                                        if (!main->RegCrdParamsList[curChannelGroup].Used && !bIsHandBScanEnabled)
                                            continue;
                                        if (Ptr2.pBScanData->Dir[0] == 0)
                                            continue;
                                        if(curChannelGroup >= 15)
                                            continue;

                                        //Hack for fast channel search
                                        CID cid = Ptr1.pBScan2Head->Items[idx_].Channel;
                                        if(cid >= (int)chanDescCache.size())
                                            chanDescCache.resize(cid+1);
                                        if(!chanDescCache[cid])
                                        {
                                             sScanChannelDescription desc;
                                             if(main->Config->getFirstSChannelbyID(cid, &desc) != -1)
                                             {
                                                chanDescCache[cid] = desc.BScanGroup;
                                             }
                                        }

                                        //Calc wheel offset
                                        if(chanDescCache[cid])
                                            transformOffset = main->RegKPOffsets[chanDescCache[cid]];
                                        else transformOffset = 0;
                                        int offsetToJointCenter = ScanLen / 2 - main->XSysCoord_Center;

                                        if(main->ac->GetMode() == acTuning) // if tuning mode - no start coord for carets
                                            transformedCrd = Ptr2.pBScanData->XSysCrd[0] + transformOffset;
                                        else
											transformedCrd = Ptr2.pBScanData->XSysCrd[0] + transformOffset + main->XSysCoord_Base + offsetToJointCenter;
											//main->RegCrdParamsList[curChannelGroup].StartCoord + Ptr2.pBScanData->XSysCrd[0] + ScanLen / 2 + transformOffset;

										if ((main->ac->GetMode() != acTuning) &&
											(abs(Ptr2.pBScanData->XSysCrd[0]) >= abs(main->RegCrdParamsList[curChannelGroup].RegLength)))
											continue;

										main->Rep->PutScanSignals(transformedCrd, Ptr1.pBScan2Head->Items[idx_].Channel - AutoconStartCID, &tmp);
									}
								}
								break;
							}
						case edMScan2Data:  // ������ �-��������� (��� 2)
							{
                                main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
								main->DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                                DataType = edMScan2Data;
                                //delete Ptr1.pBScan2Head;
                                //delete Ptr2.pBScanData;
                                //break;

								sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);

							  /*
								dev->DEV_Event->ReadEventData(&DEV_BScan2Head_ptr, 4, NULL);
								dev->DEV_Event->ReadEventData(&PBScanData_ptr, 4, NULL);

								List_ptr[Count_ptr] = PBScanData_ptr;
								Count_ptr++;

								if (GetTickCount() - Time > 50 )
								{
									frm->DataIDFlag[edMScan2Data] = true;
									Synchronize(ValueToScreen);
									Time = GetTickCount();
									for (int idx = 0; idx < Count_ptr; idx++)
									{
									   delete List_ptr[idx];
									}
									Count_ptr = 0;
								}

								delete DEV_BScan2Head_ptr;
								DEV_BScan2Head_ptr = NULL;
							 */
								break;

							 //   delete PBScanData_ptr;
							 //   PBScanData_ptr = NULL;

							}
					}
					//Unlock (write to PtrList)
					//PtrListRWLock.Release();
                }
				main->CS->Release();
			}
		}
		//Added by KirillB
		//Sleep(1);
    }
}
