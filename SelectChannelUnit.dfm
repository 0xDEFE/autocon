object SelectChannelDial: TSelectChannelDial
  Left = 227
  Top = 108
  BorderStyle = bsSizeToolWin
  Caption = 'Select channel:'
  ClientHeight = 641
  ClientWidth = 850
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 544
    Width = 850
    Height = 10
    Cursor = crVSplit
    Align = alBottom
    Beveled = True
    ExplicitLeft = 8
    ExplicitTop = 416
    ExplicitWidth = 913
  end
  object GridPanel3: TGridPanel
    Left = 0
    Top = 0
    Width = 850
    Height = 544
    Align = alClient
    ColumnCollection = <
      item
        Value = 49.998779326803920000
      end
      item
        SizeStyle = ssAbsolute
        Value = 80.000000000000000000
      end
      item
        Value = 50.001220673196080000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = m_ChannelsPanelSrc
        Row = 0
      end
      item
        Column = 1
        Control = m_SelectBtnsPanel
        Row = 0
      end
      item
        Column = 2
        Control = m_ChannelsPanelDst
        Row = 0
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    RowCollection = <
      item
        Value = 100.000000000000000000
      end>
    TabOrder = 0
    ExplicitLeft = 64
    ExplicitTop = 240
    ExplicitWidth = 401
    ExplicitHeight = 297
    object m_ChannelsPanelSrc: TPanel
      Left = 1
      Top = 1
      Width = 383
      Height = 542
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitLeft = 0
      ExplicitTop = 56
      ExplicitWidth = 159
      ExplicitHeight = 41
      object Label1: TLabel
        Left = 1
        Top = 1
        Width = 381
        Height = 19
        Align = alTop
        Caption = #1048#1089#1093#1086#1076#1085#1099#1077' '#1082#1072#1085#1072#1083#1099
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitWidth = 130
      end
      object m_ChannelsListSrc: TListBox
        Left = 1
        Top = 20
        Width = 381
        Height = 448
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 19
        MultiSelect = True
        ParentFont = False
        TabOrder = 0
        OnMouseDown = m_ChannelsListSrcMouseDown
      end
      object GridPanel1: TGridPanel
        Left = 1
        Top = 468
        Width = 381
        Height = 73
        Align = alBottom
        BevelOuter = bvNone
        ColumnCollection = <
          item
            Value = 28.571428571428570000
          end
          item
            Value = 71.428571428571430000
          end>
        ControlCollection = <
          item
            Column = 1
            Control = m_SortTypeComboSrc
            Row = 0
          end
          item
            Column = 0
            Control = StaticText3
            Row = 0
          end
          item
            Column = 0
            Control = StaticText6
            Row = 1
          end
          item
            Column = 1
            Control = m_GroupTypeComboSrc
            Row = 1
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        RowCollection = <
          item
            Value = 50.142106372616720000
          end
          item
            Value = 49.857893627383280000
          end
          item
            SizeStyle = ssAuto
          end>
        TabOrder = 1
        ExplicitLeft = -4
        ExplicitTop = 464
        object m_SortTypeComboSrc: TComboBox
          Left = 108
          Top = 0
          Width = 273
          Height = 32
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Text = 'None'
          Items.Strings = (
            #1041#1077#1079' '#1089#1086#1088#1090#1080#1088#1086#1074#1082#1080
            #1043#1088#1091#1087#1087#1072' '#1089#1082#1072#1085#1080#1088#1086#1074#1072#1085#1080#1103
            #1050#1086#1083#1077#1089#1085#1099#1081' '#1087#1088#1077#1086#1073#1088#1072#1079#1086#1074#1072#1090#1077#1083#1100
            #1048#1085#1076#1077#1082#1089
            #1048#1084#1103
            '')
          ExplicitLeft = 116
          ExplicitWidth = 292
        end
        object StaticText3: TStaticText
          Left = 0
          Top = 0
          Width = 108
          Height = 36
          Align = alClient
          Caption = 'Sort by:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          ExplicitWidth = 43
          ExplicitHeight = 17
        end
        object StaticText6: TStaticText
          Left = 0
          Top = 36
          Width = 108
          Height = 36
          Align = alClient
          Anchors = []
          Caption = 'Group by:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          ExplicitWidth = 52
          ExplicitHeight = 17
        end
        object m_GroupTypeComboSrc: TComboBox
          Left = 108
          Top = 36
          Width = 273
          Height = 32
          Align = alClient
          Anchors = []
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          Text = 'None'
          OnChange = m_GroupTypeComboSrcChange
          Items.Strings = (
            #1041#1077#1079' '#1075#1088#1091#1087#1087#1080#1088#1086#1074#1082#1080
            #1043#1088#1091#1087#1087#1072' '#1089#1082#1072#1085#1080#1088#1086#1074#1072#1085#1080#1103
            #1050#1086#1083#1077#1089#1085#1099#1081' '#1087#1088#1077#1086#1073#1088#1072#1079#1086#1074#1072#1090#1077#1083#1100)
          ExplicitLeft = 116
          ExplicitWidth = 292
        end
      end
    end
    object m_SelectBtnsPanel: TPanel
      Left = 384
      Top = 1
      Width = 80
      Height = 542
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      ExplicitLeft = 379
      ExplicitTop = 6
      object Label3: TLabel
        Left = 1
        Top = 1
        Width = 5
        Height = 19
        Align = alTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object m_MultMoveLeftBtn: TButton
        Left = 1
        Top = 100
        Width = 78
        Height = 80
        Align = alTop
        Caption = '<<'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = m_MultMoveLeftBtnClick
        ExplicitTop = 81
      end
      object m_MultMoveRightBtn: TButton
        Left = 1
        Top = 260
        Width = 78
        Height = 80
        Align = alTop
        Caption = '>>'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = m_MultMoveRightBtnClick
        ExplicitTop = 211
      end
      object m_PrintBtn: TButton
        Left = 1
        Top = 340
        Width = 78
        Height = 80
        Align = alTop
        Caption = 'Prn'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = m_PrintBtnClick
        ExplicitTop = 201
      end
      object m_SingleMoveLeftBtn: TButton
        Left = 1
        Top = 180
        Width = 78
        Height = 80
        Align = alTop
        Caption = '<'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnClick = m_SingleMoveLeftBtnClick
        ExplicitTop = 131
      end
      object m_SingleMoveRightBtn: TButton
        Left = 1
        Top = 20
        Width = 78
        Height = 80
        Align = alTop
        Caption = '>'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnClick = m_SingleMoveRightBtnClick
        ExplicitTop = 1
      end
    end
    object m_ChannelsPanelDst: TPanel
      Left = 464
      Top = 1
      Width = 385
      Height = 542
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      ExplicitLeft = 246
      ExplicitTop = -108
      ExplicitWidth = 160
      ExplicitHeight = 474
      object Label2: TLabel
        Left = 1
        Top = 1
        Width = 383
        Height = 19
        Align = alTop
        Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1077' '#1082#1072#1085#1072#1083#1099
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitWidth = 141
      end
      object m_ChannelsListDst: TListBox
        Left = 1
        Top = 20
        Width = 383
        Height = 448
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 19
        MultiSelect = True
        ParentFont = False
        TabOrder = 0
        OnMouseDown = m_ChannelsListDstMouseDown
      end
      object GridPanel2: TGridPanel
        Left = 1
        Top = 468
        Width = 383
        Height = 73
        Align = alBottom
        BevelOuter = bvNone
        ColumnCollection = <
          item
            Value = 28.571428571428570000
          end
          item
            Value = 71.428571428571430000
          end>
        ControlCollection = <
          item
            Column = 1
            Control = m_SortTypeComboDst
            Row = 0
          end
          item
            Column = 0
            Control = StaticText5
            Row = 0
          end
          item
            Column = 0
            Control = StaticText7
            Row = 1
          end
          item
            Column = 1
            Control = m_GroupTypeComboDst
            Row = 1
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        RowCollection = <
          item
            Value = 50.142106372616720000
          end
          item
            Value = 49.857893627383280000
          end>
        TabOrder = 1
        ExplicitLeft = 0
        ExplicitTop = 17
        ExplicitWidth = 475
        object m_SortTypeComboDst: TComboBox
          Left = 109
          Top = 0
          Width = 274
          Height = 32
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Text = 'None'
          Items.Strings = (
            #1041#1077#1079' '#1089#1086#1088#1090#1080#1088#1086#1074#1082#1080
            #1043#1088#1091#1087#1087#1072' '#1089#1082#1072#1085#1080#1088#1086#1074#1072#1085#1080#1103
            #1050#1086#1083#1077#1089#1085#1099#1081' '#1087#1088#1077#1086#1073#1088#1072#1079#1086#1074#1072#1090#1077#1083#1100
            #1048#1085#1076#1077#1082#1089
            #1048#1084#1103
            '')
          ExplicitLeft = 135
          ExplicitWidth = 340
        end
        object StaticText5: TStaticText
          Left = 0
          Top = 0
          Width = 109
          Height = 36
          Align = alClient
          Caption = 'Sort by:'
          TabOrder = 1
          ExplicitWidth = 43
          ExplicitHeight = 17
        end
        object StaticText7: TStaticText
          Left = 0
          Top = 36
          Width = 109
          Height = 37
          Align = alClient
          Anchors = []
          Caption = 'Group by:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          ExplicitWidth = 52
          ExplicitHeight = 17
        end
        object m_GroupTypeComboDst: TComboBox
          Left = 109
          Top = 36
          Width = 274
          Height = 32
          Align = alClient
          Anchors = []
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          Text = 'None'
          OnChange = m_GroupTypeComboDstChange
          Items.Strings = (
            #1041#1077#1079' '#1075#1088#1091#1087#1087#1080#1088#1086#1074#1082#1080
            #1043#1088#1091#1087#1087#1072' '#1089#1082#1072#1085#1080#1088#1086#1074#1072#1085#1080#1103
            #1050#1086#1083#1077#1089#1085#1099#1081' '#1087#1088#1077#1086#1073#1088#1072#1079#1086#1074#1072#1090#1077#1083#1100)
          ExplicitLeft = 135
          ExplicitWidth = 340
        end
      end
    end
  end
  object GridPanel4: TGridPanel
    Left = 0
    Top = 554
    Width = 850
    Height = 87
    Align = alBottom
    ColumnCollection = <
      item
        Value = 100.000000000000000000
      end
      item
        SizeStyle = ssAbsolute
        Value = 80.000000000000000000
      end
      item
        SizeStyle = ssAbsolute
        Value = 80.000000000000000000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = m_ChannelInfoMemo
        Row = 0
      end
      item
        Column = 1
        Control = OKBtn
        Row = 0
      end
      item
        Column = 2
        Control = CancelBtn
        Row = 0
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    RowCollection = <
      item
        Value = 100.000000000000000000
      end>
    TabOrder = 1
    ExplicitTop = 688
    ExplicitWidth = 913
    object m_ChannelInfoMemo: TMemo
      Left = 1
      Top = 1
      Width = 688
      Height = 85
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Lines.Strings = (
        'm_ChannelInfoMemo')
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      ExplicitLeft = 64
      ExplicitTop = 542
      ExplicitWidth = 185
      ExplicitHeight = 108
    end
    object OKBtn: TButton
      Left = 689
      Top = 1
      Width = 80
      Height = 85
      Align = alClient
      Caption = 'OK'
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Myriad Pro'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 1
      ExplicitLeft = 49
      ExplicitTop = 65
      ExplicitWidth = 75
      ExplicitHeight = 50
    end
    object CancelBtn: TButton
      Left = 769
      Top = 1
      Width = 80
      Height = 85
      Align = alClient
      Cancel = True
      Caption = 'Cancel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Myriad Pro'
      Font.Style = []
      ModalResult = 2
      ParentFont = False
      TabOrder = 2
      ExplicitLeft = 73
      ExplicitTop = 65
      ExplicitWidth = 75
      ExplicitHeight = 50
    end
  end
end
