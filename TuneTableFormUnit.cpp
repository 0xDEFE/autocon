﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <algorithm>
#include <functional>

#include "SelectChannelUnit.h"
#include "ScanScriptParser.h"
#include "ScanScriptSimUnit.h"

#include "TuneTableFormUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TTuneTableForm *TuneTableForm;
//---------------------------------------------------------------------------
__fastcall TTuneTableForm::TTuneTableForm(TComponent* Owner)
    : TForm(Owner)
{
    m_GridTable->Cells[0][0] = L"№ движения";
    m_GridTable->Cells[0][1] = L"Каналы";
    m_GridTable->Cells[0][2] = L"Группа каналов";
    m_GridTable->Cells[0][3] = L"Модель дефекта";
    m_GridTable->Cells[0][4] = L"Нач. координата ▲";
    m_GridTable->Cells[0][5] = L"Нач. координата ▼";
    m_GridTable->Cells[0][6] = L"Дельта ▲";
    m_GridTable->Cells[0][7] = L"Дельта ▼";
    m_GridTable->Cells[0][8] = L"Начало строба";
    m_GridTable->Cells[0][9] = L"Конец строба";
    m_GridTable->Cells[0][10] = L"Примечание";

    m_GridTable->ColWidths[0] = 210;

    for(int i = 1; i < m_GridTable->RowCount; i++)
    {
        m_GridTable->Cells[1][i] = L"⥅";
    }

    bEditMode = false;

    editingState = TT_STATE_VIEW;

    tmpSelCol = -1;
    bTmpColIsSelected = false;
}

void TTuneTableForm::setEditingState(eTuneTableEditingState newState)
{
    switch(newState)
    {
        case TT_STATE_VIEW:
            m_EditPanel->Visible = false;
            break;
        case TT_STATE_EDIT:
            m_CancelBtn->Visible = true;
            m_ApplyBtn->Visible = true;
            m_AddBtn->Visible = false;
            m_RemoveBtn->Visible = true;
            break;
        case TT_STATE_ADD:
            m_EditPanel->Visible = true;

            m_CancelBtn->Visible = true;
            m_ApplyBtn->Visible = false;
            m_AddBtn->Visible = true;
            m_RemoveBtn->Visible = false;
            break;
    }
    editingState = newState;
}

void PrepareText(UnicodeString& text)
{
    text = StringReplace(text, " ", "\r\n", TReplaceFlags() << rfReplaceAll << rfIgnoreCase);
}

//--------------Table item functions-----------------------------------------

void TTuneTableForm::UpdateTable()
{
    m_GridTable->ColCount = tableItems.size() + 2;
    for(unsigned int i = 0; i < tableItems.size(); i++)
    {
        SetTableItem( TableItemToCol(i), tableItems[i] );
    }
    for(int j = 1; j < m_GridTable->RowCount; j++)
    {
        m_GridTable->Cells[m_GridTable->ColCount-1][j] = L"⥅";
    }

    adjustTableColumnsWidth();
    UpdateInfo();
}

void TTuneTableForm::UpdateInfo()
{
    UnicodeString text;
    unsigned int totalPathMm = 0;
    unsigned int totalTimeMs = 0;

    int i = 0;
    for(i = 0; i < ((int)tableItems.size()-1); i++)
    {
        totalPathMm += std::max(tableItems[i].caretEnabled[1] ?
                                    abs((tableItems[i].scanStart[0] + tableItems[i].scanDelta[0]) - tableItems[i+1].scanStart[0]) : 0,
                                tableItems[i].caretEnabled[1] ?
                                    abs((tableItems[i].scanStart[1] + tableItems[i].scanDelta[1]) - tableItems[i+1].scanStart[1]) : 0);
    }
    if(i < (int)tableItems.size())
        totalPathMm+=std::max(tableItems[i].scanDelta[0], tableItems[i].scanDelta[1]);

    const unsigned int speed = 100;//Speed - 100mm/sec ?
    const unsigned int delay = 1000;//Delay on change move - 1 sec ?
    totalTimeMs = totalPathMm * 1000 / speed + delay * tableItems.size();

    text += StringFormatU(L"Текущие движения: \r\n\t- Примерный путь: %.2fм\r\n\t- Примерное время: %d мин %d сек\r\n", float(totalPathMm)/1000.f, (totalTimeMs/1000)/60,(totalTimeMs/1000)%60);

    m_InfoMemo->Text = text;
}

void TTuneTableForm::SetTableItem(int colIndex, sTuneTableItem& item)
{
    m_GridTable->Cells[colIndex][0] = IntToStr(colIndex);
    m_GridTable->Cells[colIndex][1] = CIDArrayToStr( item.channelIndices );
    m_GridTable->Cells[colIndex][2] = IntToStr((int)item.nGroup);
    m_GridTable->Cells[colIndex][3] = item.model;

    m_GridTable->Cells[colIndex][4] = item.caretEnabled[0] ? IntToStr(item.scanStart[0]) : UnicodeString(L"-");
    m_GridTable->Cells[colIndex][6] = item.caretEnabled[0] ? IntToStr(item.scanDelta[0]) : UnicodeString(L"-");

    m_GridTable->Cells[colIndex][5] = item.caretEnabled[1] ? IntToStr(item.scanStart[1]) : UnicodeString(L"-");
    m_GridTable->Cells[colIndex][7] = item.caretEnabled[1] ? IntToStr(item.scanDelta[1]) : UnicodeString(L"-");

    m_GridTable->Cells[colIndex][8] = IntToStr((int)item.strobeStart);
    m_GridTable->Cells[colIndex][9] = IntToStr((int)item.strobeEnd);

    m_GridTable->Cells[colIndex][10] = item.info;
}

void TTuneTableForm::SetEditPanelItem(sTuneTableItem& item)
{
    switch(item.nGroup)
    {
        case 3: m_GroupCombo->ItemIndex = 0; break;
        case 5: m_GroupCombo->ItemIndex = 1; break;
        case 6: m_GroupCombo->ItemIndex = 2; break;
        case 8: m_GroupCombo->ItemIndex = 3; break;
        case 10: m_GroupCombo->ItemIndex = 4; break;
        case 12: m_GroupCombo->ItemIndex = 5; break;
        case 14: m_GroupCombo->ItemIndex = 6; break;
    }

    m_ChannelsEdit->Text = CIDArrayToStr( item.channelIndices );

    m_ModelEdit->Text = item.model;
    m_EnableTopCaret->Down = item.caretEnabled[0];
    m_EnableBotCaret->Down = item.caretEnabled[1];
    m_PosTopSpin->Value = item.scanStart[0];
    m_PosBtmSpin->Value = item.scanStart[1];
    m_DeltaTopSpin->Value = item.scanDelta[0];
    m_DeltaBtmSpin->Value = item.scanDelta[1];
    m_StrobeStartSpin->Value = item.strobeStart;
    m_StrobeEndSpin->Value = item.strobeEnd;
    m_ChInfoMemo->Text = item.info;
}

sTuneTableItem TTuneTableForm::GetEditPanelItem()
{
    sTuneTableItem item;

    switch(m_GroupCombo->ItemIndex)
    {
        case 0: item.nGroup = 3; break;
        case 1: item.nGroup = 5; break;
        case 2: item.nGroup = 6; break;
        case 3: item.nGroup = 8; break;
        case 4: item.nGroup = 10; break;
        case 5: item.nGroup = 12; break;
        case 6: item.nGroup = 14; break;
    }

    CIDStrToArray(m_ChannelsEdit->Text,item.channelIndices);

    item.model = m_ModelEdit->Text;
    item.caretEnabled[0] = m_EnableTopCaret->Down;
    item.caretEnabled[1] = m_EnableBotCaret->Down;
    item.scanStart[0] = m_PosTopSpin->Value;
    item.scanStart[1] = m_PosBtmSpin->Value;
    item.scanDelta[0] = m_DeltaTopSpin->Value;
    item.scanDelta[1] = m_DeltaBtmSpin->Value;
    item.strobeStart = m_StrobeStartSpin->Value;
    item.strobeEnd = m_StrobeEndSpin->Value;
    item.info = m_ChInfoMemo->Text;
    return item;
}

void TTuneTableForm::RemoveTableItem(unsigned int colIndex)
{
    std::vector<sTuneTableItem>::iterator it = tableItems.begin();
    int itemIndex = 0;
    while(it != tableItems.end())
    {
        if(TableItemToCol(itemIndex) == (int)colIndex)
        {
            it = tableItems.erase(it);
            break;
        }
        else it++;

        itemIndex++;
    }
}

sTuneTableItem* TTuneTableForm::GetTableItem(unsigned int colIndex)
{
    int itemIndex = TableColToItem(colIndex);
    if((itemIndex<0) || (itemIndex>=(int)tableItems.size()))
        return NULL;
    return &tableItems[itemIndex];
}

void TTuneTableForm::UpdateEditPanel(bool bFromTableToPanel)
{
    if(m_GridTable->Col < 1)
        return;

    sTuneTableItem* pItem = GetTableItem(m_GridTable->Col);
    if(!pItem)
        return;

    if(bFromTableToPanel)
    {
        SetEditPanelItem(*pItem);
    }
    else
    {
        sTuneTableItem tmpItem = GetEditPanelItem();

        eTuneItemStatus stat = CheckItem( tmpItem );
        if(isItemStatusCritical(stat))
        {
            Application->MessageBoxW((UnicodeString("Неверные данные: ") + ItemStatusToString(stat)).c_str(),
                                        L"Ошибка",MB_OK | MB_ICONWARNING);
        }
        else
        {
            *pItem = tmpItem;
            UpdateTable();
        }
    }
}

eTuneItemStatus TTuneTableForm::CheckItem(sTuneTableItem& item)
{
    if(item.channelIndices.empty())
        return TIS_NO_CHANNELS;
    if((item.nGroup!=3) && (item.nGroup!=5) && (item.nGroup!=6) && (item.nGroup!=8) &&
        (item.nGroup!=10) && (item.nGroup!=12) && (item.nGroup!=14))
        return TIS_WRONG_GROUP;

    sScanChannelDescription scanChanDesc;
    for(unsigned int i = 0; i < item.channelIndices.size(); i++)
    {
        bool bFound = false;
        for(unsigned int j=0;j<AutoconMain->Config->GetAllScanChannelsCount();j++)
        {
            if(!AutoconMain->Config->getSChannelbyIdx(j, &scanChanDesc))
                continue;
            if(scanChanDesc.Id == (int)item.channelIndices[i])
            {
                if(scanChanDesc.StrokeGroupIdx == (int)item.nGroup)
                    bFound = true;
            }
        }
        if(!bFound) return TIS_CHANNELS_FROM_DIFFERENT_GROUPS;
    }

    if(item.model.Length() == 0)
        return TIS_NO_MODEL;

    if((!item.caretEnabled[0]) && (!item.caretEnabled[1]))
        return TIS_NO_CARET_MOVEMENT;

    for(int i = 0; i < 2; i++)
    {
        if(!item.caretEnabled[i])
            continue;
        if((item.scanStart[i] < 50) || (item.scanStart[i] > 1300))
            return TIS_WRONG_SCAN_START;
        if(((item.scanStart[i]+item.scanDelta[i]) < 50) || ((item.scanStart[i]+item.scanDelta[i]) > 1300))
            return TIS_WRONG_SCAN_DELTA;
    }

    if((item.strobeStart < 0) || (item.strobeStart >= item.strobeEnd))
        return TIS_WRONG_START_STROBE;

    if((item.strobeEnd < 0) || (item.strobeEnd <= item.strobeStart))
        return TIS_WRONG_END_STROBE;

    return TIS_READY;
}
//---------------------------------------------------------------------------

UnicodeString TTuneTableForm::ConvertTextToTable(UnicodeString text)
{
    if(text.Length() == 0)
        return text;

    if(text == L"⥅")
        return text;

    TFont* oldFont = Canvas->Font;
    Canvas->Font = m_GridTable->Font;

    int initialWidth = Canvas->TextWidth( text );
    int initialHeight = Canvas->TextHeight( text );

    int square = initialWidth * initialHeight;
    float idW = 4.f;    float idH = 3.f;
    float coeff = square / (idW*idH);
    idW*=sqrt(coeff);   idH*=sqrt(coeff);
    int idealWidth = ceil(idW);
    int idealHeight = ceil(idH);
    int idealLineCount = idealHeight / initialHeight;


    TStringList * MyList = new TStringList();
    MyList->Delimiter = L' ';
    MyList->DelimitedText = text;

    UnicodeString strLine;
    UnicodeString strFinal;
    for(int i = 0; i < MyList->Count; i++)
    {
        if( Canvas->TextWidth( strLine + MyList->Strings[i] ) < idealWidth)
            strLine+=MyList->Strings[i] + " ";
        else
        {
            if(strFinal.Length()) strFinal += L"\r\n";
            strFinal +=  strLine;
            strLine = MyList->Strings[i] + " ";
        }
    }

    if(strFinal.Length()) strFinal += L"\r\n";
    strFinal +=  strLine;
    strFinal.Trim();
    Canvas->Font = oldFont;
    delete MyList;
    return strFinal;
}

void TTuneTableForm::adjustTableColumnsWidth()
{
    //ConvertTextToTable("Olo lo");
    const int DEFBORDER = 6;

    TFont* oldFont = Canvas->Font;

	int* colWidthMax = new int[ m_GridTable->ColCount ];
    int* rowHeightMax = new int[ m_GridTable->RowCount ];
	memset(colWidthMax,0,sizeof(int)*m_GridTable->ColCount);
	memset(rowHeightMax,0,sizeof(int)*m_GridTable->RowCount);

	//Getting column title width
	Canvas->Font = m_GridTable->Font;
    TStringList* pStrList = new TStringList();
	for( int i = 1; i < m_GridTable->ColCount; i++)
	{
        for( int j = 1; j < m_GridTable->RowCount; j++)
        {
            pStrList->Text = ConvertTextToTable(m_GridTable->Cells[i][j]);

            int maxWidth = 0;
            int maxHeight = 0;
            for(int k = 0; k < pStrList->Count; k++)
            {
                maxWidth = std::max( maxWidth,Canvas->TextWidth( pStrList->Strings[k] ));
                maxHeight += Canvas->TextHeight( pStrList->Strings[k] );
            }

            colWidthMax[i] = std::max( colWidthMax[i], maxWidth + DEFBORDER);
            rowHeightMax[j] = std::max( rowHeightMax[j], maxHeight + DEFBORDER);
        }
	}

    for( int i = 1; i < m_GridTable->ColCount; i++)
        m_GridTable->ColWidths[i] = colWidthMax[i];

    for( int i = 1; i < m_GridTable->RowCount; i++)
        m_GridTable->RowHeights[i] = rowHeightMax[i];

    Canvas->Font = oldFont;
    delete pStrList;
    delete [] colWidthMax;
}

void TTuneTableForm::OptimizeRoutes()
{
    if(tableItems.empty())
        return;

    int nItems = tableItems.size();

    int srcRouteLen = 0;
    int* routeIndices = new int[nItems];

    for(int i = 0; i < (nItems-1); i++)
    {
        int moveLen = std::max(abs((tableItems[i].scanStart[0] + tableItems[i].scanDelta[0]) - tableItems[i+1].scanStart[0]),
                                abs((tableItems[i].scanStart[1] + tableItems[i].scanDelta[1]) - tableItems[i+1].scanStart[1]));
        srcRouteLen+=moveLen;
    }

    for(int i = 0; i < nItems; i++)
        routeIndices[i] = i;

    int currRouteLen = srcRouteLen;
    int prevRouteLen = currRouteLen+1;
    int temp;
    int tempRouteLen;
    while(currRouteLen < prevRouteLen)
    {
        prevRouteLen = currRouteLen;
        currRouteLen = 0;
        for(int i = 0; i < nItems; i++)
        {
            for(int j = 0; j < nItems; j++)
            {
                temp = routeIndices[i];
                routeIndices[i] = routeIndices[j];
                routeIndices[j] = temp;

                tempRouteLen = 0;
                for(int k = 0; k < (nItems-1); k++)
                {
                    int i1 = routeIndices[k];
                    int i2 = routeIndices[k+1];
                    int moveLen = std::max(abs((tableItems[i1].scanStart[0] + tableItems[i1].scanDelta[0]) - tableItems[i2].scanStart[0]),
                                            abs((tableItems[i1].scanStart[1] + tableItems[i1].scanDelta[1]) - tableItems[i2].scanStart[1]));
                    tempRouteLen+=moveLen;
                }

                if(tempRouteLen >= prevRouteLen) //turn back
                {
                    temp = routeIndices[i];
                    routeIndices[i] = routeIndices[j];
                    routeIndices[j] = temp;
                }
            }
        }

        currRouteLen = 0;
        for(int k = 0; k < (nItems-1); k++)
        {
            int i1 = routeIndices[k];
            int i2 = routeIndices[k+1];
            int moveLen = std::max(abs((tableItems[i1].scanStart[0] + tableItems[i1].scanDelta[0]) - tableItems[i2].scanStart[0]),
                                    abs((tableItems[i1].scanStart[1] + tableItems[i1].scanDelta[1]) - tableItems[i2].scanStart[1]));
            currRouteLen+=moveLen;
        }

        LOGINFO("OptIter: curr=%d, prev=%d",currRouteLen, prevRouteLen);
    }

    LOGINFO_NF("Finally:");
    for(int i = 0; i < nItems; i++)
    {
        LOGINFO("%d (%d): %d, %d",i,routeIndices[i], tableItems[routeIndices[i]].scanStart[0],tableItems[routeIndices[i]].scanStart[1]);
    }

    std::vector<sTuneTableItem> tempItems = tableItems;
    tableItems.clear();
    for(int i = 0; i < nItems; i++) tableItems.push_back(tempItems[routeIndices[i]]);

    UpdateTable();
    delete [] routeIndices;
}
//---------------------------------------------------------------------------


//-------------------Message Processing--------------------------------------

void __fastcall TTuneTableForm::m_GridTableSelectCell(TObject *Sender, int ACol, int ARow,
          bool &CanSelect)
{
    tmpSelCol = ACol;
    bTmpColIsSelected = true;
    m_GridTable->Refresh();
}

void __fastcall TTuneTableForm::m_GridTableMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
    if(!bTmpColIsSelected)
    {
        m_EditPanel->Visible = false;
    }
    else
    {
        if(tmpSelCol == m_GridTable->ColCount -1)
        {
            setEditingState(TT_STATE_ADD);
        }
        else
        {
            setEditingState(TT_STATE_EDIT);
            m_EditPanel->Visible = true;
            UpdateEditPanel(true);
        }
    }
    bTmpColIsSelected = false;
}

void __fastcall TTuneTableForm::m_GridTableDrawCell(TObject *Sender, int ACol, int ARow,
          TRect &Rect, TGridDrawState State)
{
    if((ACol == 0) || (ARow == 0))
        return;

    TColor fillColor = clWhite;
    if(ACol == tmpSelCol)
    {
        fillColor = clSkyBlue;
    }

    m_GridTable->Canvas->Brush->Color = fillColor;
    m_GridTable->Canvas->FillRect(Rect);

    TTextFormat tf;
    tf.Clear();
    tf << tfCenter << tfVerticalCenter;
    UnicodeString str = ConvertTextToTable(m_GridTable->Cells[ACol][ARow]);
    m_GridTable->Canvas->TextRect(Rect, str, tf);
}

void __fastcall TTuneTableForm::OnTableMove(TObject *Sender)
{
    int moveOffset = 0;
    TComponent *AComponent = dynamic_cast<TComponent*>(Sender);
    switch(AComponent->Tag)
    {
        case 0: //Move left hi
            moveOffset = -5;
            break;
        case 1: //Move left low
            moveOffset = -1;
            break;
        case 2: //Move right low
            moveOffset = 1;
            break;
        case 3: //Move right hi
            moveOffset = 5;
            break;
    }

    m_GridTable->Col = std::max(1, std::min(m_GridTable->ColCount-1, m_GridTable->Col + moveOffset));

    if(m_EditPanel->Visible)
    {
        if(m_GridTable->Col == (m_GridTable->ColCount-1))
            setEditingState(TT_STATE_ADD);
        else
            setEditingState(TT_STATE_EDIT);

        UpdateEditPanel(true);
    }
}

void __fastcall TTuneTableForm::m_AddBtnClick(TObject *Sender)
{
    sTuneTableItem item = GetEditPanelItem();
    eTuneItemStatus stat = CheckItem( item );
    if(isItemStatusCritical(stat))
    {
        Application->MessageBoxW((UnicodeString("Неверные данные: ") + ItemStatusToString(stat)).c_str(),
                                        L"Ошибка",MB_OK | MB_ICONWARNING);
    }
    else
    {
        tableItems.push_back(item);
        UpdateTable();
        m_GridTable->Col = m_GridTable->ColCount-1;
    }
}

void __fastcall TTuneTableForm::m_CancelBtnClick(TObject *Sender)
{
    setEditingState( TT_STATE_VIEW );
}

void __fastcall TTuneTableForm::m_ApplyBtnClick(TObject *Sender)
{
    UpdateEditPanel(false);
}

void __fastcall TTuneTableForm::m_RemoveBtnClick(TObject *Sender)
{
    RemoveTableItem(m_GridTable->Col);
    UpdateTable();
}

void __fastcall TTuneTableForm::m_CloseBtnClick(TObject *Sender)
{
    this->Visible = false;
}

void __fastcall TTuneTableForm::m_ClearBtnClick(TObject *Sender)
{
    tableItems.clear();
    UpdateTable();
}

const unsigned int TuneTableFileMagic = 0xFA09C1DF;

void __fastcall TTuneTableForm::m_SaveBtnClick(TObject *Sender)
{
    SaveDialog1->InitialDir = GetCurrentDir();
    if(!SaveDialog1->Execute(NULL))
        return;

    TFileStream * out;
    out = new TFileStream(SaveDialog1->FileName, fmCreate /*fmOpenReadWrite*/);

    unsigned int arr_size = tableItems.size();
    unsigned int version = 1;
    out->Write(&TuneTableFileMagic,4);
    out->Write(&version,4);
    out->Write(&arr_size,4);

    for(unsigned int i = 0; i < tableItems.size(); i++)
    {
        sTuneTableItem& item = tableItems[i];
        out->Write(&item.number,4);
        out->Write(&item.nGroup,4);
        out->Write(item.caretEnabled,8);
        out->Write(item.scanStart,8);
        out->Write(item.scanDelta,8);
        out->Write(&item.strobeStart,4);
        out->Write(&item.strobeEnd,4);

        unsigned int cid_arr_size = item.channelIndices.size();
        out->Write(&cid_arr_size,4);
        for(unsigned int j = 0; j < cid_arr_size; j++)
        {
            out->Write(&(item.channelIndices[j]),4);
        }

        unsigned int string_size = item.model.Length();
        out->Write( &string_size, 4 );
        out->Write( item.model.c_str(),sizeof(wchar_t)*string_size);

        string_size = item.info.Length();
        out->Write( &string_size, 4 );
        out->Write( item.info.c_str(),sizeof(wchar_t)*string_size);
    }

    out->Write(&TuneTableFileMagic,4);

end_ok:
    goto finally;
end_fail:
    Application->MessageBox(L"Файл не сохранен!",L"Файл не сохранен!",MB_OK | MB_ICONERROR);
finally:
    delete out;
}

void __fastcall TTuneTableForm::m_LoadBtnClick(TObject *Sender)
{
    OpenDialog1->InitialDir = GetCurrentDir();
    if(!OpenDialog1->Execute(NULL))
        return;


    m_ClearBtnClick(NULL);

    TFileStream *input = new TFileStream(OpenDialog1->FileName, fmOpenRead);

    unsigned int tmp,tmp1;
    unsigned int arr_size;
    input->Read(&tmp,4);//magic
    if(tmp != TuneTableFileMagic)
        goto end_fail;

    input->Read(&tmp,4); //version
    if(tmp != 1)
        goto end_fail;

    input->Read(&arr_size,4);//item count

    for(unsigned int i = 0; i < arr_size; i++)
    {
        sTuneTableItem item;

        input->Read(&item.number,4);
        input->Read(&item.nGroup,4);
        input->Read(item.caretEnabled,8);
        input->Read(item.scanStart,8);
        input->Read(item.scanDelta,8);
        input->Read(&item.strobeStart,4);
        input->Read(&item.strobeEnd,4);

        input->Read(&tmp,4);
        for(unsigned int j = 0; j < tmp; j++)
        {
            input->Read(&tmp1,4);
            item.channelIndices.push_back(tmp1);
        }

        //Read model
        input->Read(&tmp,4);
        wchar_t* pStringBuf = new wchar_t[tmp+1];
        input->Read(pStringBuf,sizeof(wchar_t)*tmp);
        pStringBuf[tmp] = 0;
        item.model = pStringBuf;
        delete [] pStringBuf;

        //Read info
        input->Read(&tmp,4);
        pStringBuf = new wchar_t[tmp+1];
        input->Read(pStringBuf,sizeof(wchar_t)*tmp);
        pStringBuf[tmp] = 0;
        item.info = pStringBuf;
        delete [] pStringBuf;

        tableItems.push_back(item);
    }

    input->Read(&tmp,4);
    if(tmp != TuneTableFileMagic)
        goto end_fail;


    UpdateTable();

end_ok:
    UpdateInfo();
    goto finally;
end_fail:
    Application->MessageBox(L"Файл не загружен!",L"Файл не загружен!",MB_OK | MB_ICONERROR);
finally:
    delete input;
}

void __fastcall TTuneTableForm::m_SelChannelsBtnClick(TObject *Sender)
{
    sTuneTableItem item = GetEditPanelItem();

    SelectChannelDial->Filter(SELM_BY_SCAN_GROUP,item.nGroup);
    SelectChannelDial->Init(false, SELM_BY_CHANNEL, SORTM_BY_CHANNEL, GRT_NONE);

	if(SelectChannelDial->SetSelectedAsString(m_ChannelsEdit->Text))
	{
		if(SelectChannelDial->ShowModal() == mrOk)
		{
			m_ChannelsEdit->Text = SelectChannelDial->GetSelectedAsString();
		};
	}
	else
	{
		MessageBox(NULL,L"Wrong data format!",L"Error",MB_ICONWARNING);
	}
}

void __fastcall TTuneTableForm::m_BuildBtnClick(TObject *Sender)
{
    cScanScriptCmdBuffer cmdBuf;
    cmdBuf.pushCommand(SSC_SCAN,1,0);
    cmdBuf.pushCommand(SSC_MOVTOBASE,0);

    int curCrd[] = {50,50};
    int nextCrd_rel[2];

    AnsiString codeBuff = "scan 0;\r\nmovtobase;\r\n\r\n";

    for(unsigned int i = 0; i < tableItems.size(); i++)
    {
        sTuneTableItem& param = tableItems[i];

        nextCrd_rel[0] = (param.caretEnabled[0]) ? (param.scanStart[0] - curCrd[0]) : 0;
        nextCrd_rel[1] = (param.caretEnabled[1]) ? (param.scanStart[1] - curCrd[1]) : 0;
        curCrd[0] += nextCrd_rel[0];
        curCrd[1] += nextCrd_rel[1];

        codeBuff += StringFormatA("## Move №%d\r\n",i+1);
        codeBuff += StringFormatA("movcarets %d, %d;\t#Moving to %d, %d\r\n", nextCrd_rel[0],nextCrd_rel[1],curCrd[0],curCrd[1]);

        codeBuff += StringFormatA("setsg %d;\r\n",param.nGroup);

        codeBuff += "scan 1;\r\n";

        nextCrd_rel[0] = (param.caretEnabled[0]) ? param.scanDelta[0] : 0;
        nextCrd_rel[1] = (param.caretEnabled[1]) ? param.scanDelta[1] : 0;
        curCrd[0] += nextCrd_rel[0];
        curCrd[1] += nextCrd_rel[1];

        codeBuff += StringFormatA("movcarets %d, %d;\t#Scanning to %d, %d\r\n",
                        nextCrd_rel[0],
                        nextCrd_rel[1],
                        curCrd[0],curCrd[1]);

        codeBuff += "scan 0;\r\n";
        codeBuff += "tunech ";
        for(unsigned int j = 0; j < param.channelIndices.size(); j++)
            codeBuff += StringFormatA("%s0x%X",j ? ", " : " ", param.channelIndices[j]);
        codeBuff += ";\r\n";
        codeBuff += "tuneclear;\r\n";

        codeBuff += "\r\n";
    }

    ScanScriptSimForm->SetScriptCode(codeBuff);

    ScanScriptSimForm->ClearRailDefects();

    for(unsigned int i = 0; i < tableItems.size(); i++)
    {
        if(tableItems[i].caretEnabled[0])
        {
            ScanScriptSimForm->AddDefectToRail(tableItems[i].scanStart[0] + tableItems[i].scanDelta[0]/2,
                                                    tableItems[i].scanDelta[0],tableItems[i].strobeStart,tableItems[i].strobeEnd);
        }
        if(tableItems[i].caretEnabled[1])
        {
            ScanScriptSimForm->AddDefectToRail(tableItems[i].scanStart[1] + tableItems[i].scanDelta[1]/2,
                                                    tableItems[i].scanDelta[1],tableItems[i].strobeStart,tableItems[i].strobeEnd);
        }
    }
    ScanScriptSimForm->Visible = true;
}

void __fastcall TTuneTableForm::OnEditPanelSpinChanged(TObject *Sender)
{
    TComponent *AComponent = dynamic_cast<TComponent*>(Sender);
    int itemTag = AComponent->Tag;

    bool bItemIsTrack = itemTag % 2;
    int itemIndex = itemTag / 2;

    switch(itemIndex)
    {
        case 0: //Нач. координата Up
            if(m_PosTopTrack->Position != m_PosTopSpin->Value)
                if(!bItemIsTrack)
                    m_PosTopTrack->Position = m_PosTopSpin->Value;
                else
                    m_PosTopSpin->Value = m_PosTopTrack->Position;

            m_DeltaTopSpin->MaxValue = m_PosTopTrack->Max - m_PosTopTrack->Position;
            m_DeltaTopTrack->Max = m_DeltaTopSpin->MaxValue;
            m_DeltaTopSpin->MinValue = m_PosTopTrack->Min - m_PosTopTrack->Position;
            m_DeltaTopTrack->Min = m_DeltaTopSpin->MinValue;
            break;
        case 1: //Нач. координата Down
            if(m_PosBtmTrack->Position != m_PosBtmSpin->Value)
                if(!bItemIsTrack)
                    m_PosBtmTrack->Position = m_PosBtmSpin->Value;
                else
                    m_PosBtmSpin->Value = m_PosBtmTrack->Position;

            m_DeltaBtmSpin->MaxValue = m_PosBtmTrack->Max - m_PosBtmTrack->Position;
            m_DeltaBtmTrack->Max = m_DeltaBtmSpin->MaxValue;
            m_DeltaBtmSpin->MinValue = m_PosBtmTrack->Min - m_PosBtmTrack->Position;
            m_DeltaBtmTrack->Min = m_DeltaBtmSpin->MinValue;
            break;
        case 2: //Дельта Up
           if(m_DeltaTopTrack->Position != m_DeltaTopSpin->Value)
                if(!bItemIsTrack)
                    m_DeltaTopTrack->Position = m_DeltaTopSpin->Value;
                else
                    m_DeltaTopSpin->Value = m_DeltaTopTrack->Position;
            break;
        case 3: //Дельта Down
            if(m_DeltaBtmTrack->Position != m_DeltaBtmSpin->Value)
                if(!bItemIsTrack)
                    m_DeltaBtmTrack->Position = m_DeltaBtmSpin->Value;
                else
                    m_DeltaBtmSpin->Value = m_DeltaBtmTrack->Position;
            break;
        case 4: //Начало строба
            if(m_StrobeStartTrack->Position != m_StrobeStartSpin->Value)
                if(!bItemIsTrack)
                    m_StrobeStartTrack->Position = m_StrobeStartSpin->Value;
                else
                    m_StrobeStartSpin->Value = m_StrobeStartTrack->Position;

                m_StrobeStartSpin->Value = std::min(m_StrobeStartSpin->Value, m_StrobeEndSpin->Value);
                m_StrobeStartTrack->Position = std::min(m_StrobeStartTrack->Position, m_StrobeEndTrack->Position);

                m_StrobeStartTrack->SelStart = m_StrobeStartTrack->Position;
                m_StrobeStartTrack->SelEnd = m_StrobeEndTrack->Position;

                m_StrobeEndTrack->SelStart = m_StrobeStartTrack->Position;
                m_StrobeEndTrack->SelEnd = m_StrobeEndTrack->Position;
            break;
        case 5: //Конец строба
            if(m_StrobeEndTrack->Position != m_StrobeEndSpin->Value)
                if(!bItemIsTrack)
                    m_StrobeEndTrack->Position = m_StrobeEndSpin->Value;
                else
                    m_StrobeEndSpin->Value = m_StrobeEndTrack->Position;

                m_StrobeEndSpin->Value = std::max(m_StrobeEndSpin->Value, m_StrobeStartSpin->Value);
                m_StrobeEndTrack->Position = std::max(m_StrobeEndTrack->Position, m_StrobeStartTrack->Position);

                m_StrobeStartTrack->SelStart = m_StrobeStartTrack->Position;
                m_StrobeStartTrack->SelEnd = m_StrobeEndTrack->Position;

                m_StrobeEndTrack->SelStart = m_StrobeStartTrack->Position;
                m_StrobeEndTrack->SelEnd = m_StrobeEndTrack->Position;
            break;
    }

}

void __fastcall TTuneTableForm::m_OptimizeBtnClick(TObject *Sender)
{
    OptimizeRoutes();
}
//---------------------------------------------------------------------------
void __fastcall TTuneTableForm::m_EnableTopCaretClick(TObject *Sender)
{
    m_TopCaretGroupBox->Height = (m_EnableTopCaret->Down ? 154 : 0);
    m_BotCaretGroupBox->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall TTuneTableForm::m_EnableBotCaretClick(TObject *Sender)
{
    m_BotCaretGroupBox->Height = (m_EnableBotCaret->Down ? 154 : 0);
    m_BotCaretGroupBox->Refresh();
}
//---------------------------------------------------------------------------

