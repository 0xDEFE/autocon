// ---------------------------------------------------------------------------

//#pragma hdrstop


#include "ParseSVG.h"
#include <vcl.h>
#include <stdio.h>

int ParseSVG::start_parse()
{
	doc = xmlReadFile(filename.c_str(), NULL, 0);
	if (doc == NULL) {
		return 1;
	}

	last_rect_node = NULL;
	last_text_node = NULL;
	last_tspan_node = NULL;
	last_line_node = NULL;

	return 0;
}

// ---------------------------------------------------------------------------

void ParseSVG::end_parse()
{
	xmlFreeDoc(doc);

	last_rect_node = NULL;
	last_text_node = NULL;
	last_tspan_node = NULL;
	last_line_node = NULL;
}

// ---------------------------------------------------------------------------

void ParseSVG::SetFilename(const char* FN)
{
	filename = FN;
}

// ---------------------------------------------------------------------------

AnsiString ParseSVG::GetFilename()
{
	return filename;
}

// ---------------------------------------------------------------------------

xmlNodePtr ParseSVG::find_child_by_id(xmlNodePtr node, xmlChar *id)
{
	xmlNodePtr cur = node;

	cur = cur->children;
	while(cur != NULL)
	{
		if (cur->type == XML_ELEMENT_NODE)
			if ( !xmlStrcmp(xmlGetProp(cur, "id"), id) )
				return cur;
		cur = cur->next;
	}

	return NULL;
}

// ---------------------------------------------------------------------------

xmlNodePtr ParseSVG::find_child_by_name(xmlNodePtr node, xmlChar *name)
{
	xmlNodePtr cur = node;

	cur = cur->children;
	while(cur != NULL)
	{
		if (cur->type == XML_ELEMENT_NODE)
			if ( !xmlStrcmp(cur->name, name) )
				return cur;
		cur = cur->next;
	}

	return NULL;
}

// ---------------------------------------------------------------------------

xmlNodePtr ParseSVG::find_layer_node()
{
	xmlNodePtr nodePtr;

	// �������� ����
	nodePtr = xmlDocGetRootElement(doc);
	nodePtr = find_child_by_id(nodePtr, "layer2");

	return nodePtr;
}

xmlNodePtr ParseSVG::find_page_node()
{
	xmlNodePtr nodePtr;

	// �������� ����
	nodePtr = xmlDocGetRootElement(doc);

	return nodePtr;
}

// ---------------------------------------------------------------------------

xmlNodePtr ParseSVG::find_layer_child_by_id(xmlChar *id)
{
	xmlNodePtr nodePtr;

	nodePtr = find_layer_node();
	nodePtr = find_child_by_id(nodePtr, id);

	return nodePtr;
}

// ---------------------------------------------------------------------------

bool parseTransformMatrix(float* pData, const char* matrStr)
{
	if(matrStr==NULL)
		return false;

	AnsiString str = matrStr;
	if(str.Length() < 8)
		return false;
	if(str.SubString(0,6) != "matrix")
		return false;
	str = Trim(str.SubString(7,str.Length() - 5));

	if(str.c_str()[0] != '(')
		return false;

	if(str.c_str()[str.Length() - 1] != ')')
		return false;

	str = Trim(str.SubString(2,str.Length() - 2));

	AnsiString substr;
	int dataCnt = 0;
	for(int i = 0; i < str.Length(); i++)
	{
		if( str.c_str()[i] == ',' )
		{
			if(dataCnt >= 6)
				return false;
			pData[dataCnt] = atof(substr.c_str());
			dataCnt++;
			substr = "";
		}
		else
		{
			substr += str.c_str()[i];
        }
	}

	if(dataCnt >= 6)
		return false;
	pData[dataCnt] = atof(substr.c_str());
	dataCnt++;

	if(dataCnt!=6)
		return false;

	//float t = pData[1];
	//pData[1] = pData[2];
	//pData[2] = t;

	return true;
}

void invertMatrix(float* pMatr, float* pRes)
{
	#define A(x,y) (srcMatr[(x) + (y) * 3])
	#define result(x,y) (resMatr[(x) + (y) * 3])

	float resMatr[9];
	float srcMatr[9];
	memset(resMatr,0,sizeof(resMatr));
	memset(srcMatr,0,sizeof(srcMatr));
	for(int i = 0; i < 2; i++) //x
	{
		for(int j = 0; j < 3; j++) //y
		{
			srcMatr[i + j * 3] = pMatr[i + j * 2];
		}
	}
	A(2,2) = A(2,1) = A(2,0) = 1;

	float determinant =    +A(0,0)*(A(1,1)*A(2,2)-A(2,1)*A(1,2))
						-A(0,1)*(A(1,0)*A(2,2)-A(1,2)*A(2,0))
                        +A(0,2)*(A(1,0)*A(2,1)-A(1,1)*A(2,0));
	float invdet = 1.f / determinant;
	result(0,0) =  (A(1,1)*A(2,2)-A(2,1)*A(1,2))*invdet;
	result(0,1) = -(A(0,1)*A(2,2)-A(0,2)*A(2,1))*invdet;
	result(0,2) =  (A(0,1)*A(1,2)-A(0,2)*A(1,1))*invdet;
	result(1,0) = -(A(1,0)*A(2,2)-A(1,2)*A(2,0))*invdet;
	result(1,1) =  (A(0,0)*A(2,2)-A(0,2)*A(2,0))*invdet;
	result(1,2) = -(A(0,0)*A(1,2)-A(1,0)*A(0,2))*invdet;
	result(2,0) =  (A(1,0)*A(2,1)-A(2,0)*A(1,1))*invdet;
	result(2,1) = -(A(0,0)*A(2,1)-A(2,0)*A(0,1))*invdet;
	result(2,2) =  (A(0,0)*A(1,1)-A(1,0)*A(0,1))*invdet;

	#undef A
	#undef result

	for(int i = 0; i < 2; i++) //x
	{
		for(int j = 0; j < 3; j++) //y
		{
			pRes[i + j * 2] = resMatr[i + j * 3];
		}
	}
}

void ParseSVG::multiplyMatrixVec2(float* matr, float* vec, float* res)
{
	#define A(x,y) (matr[(x) + (y) * 2])

	res[0] = vec[0] * A(0,0) + vec[1] * A(1,0);
	res[1] = vec[0] * A(0,1) + vec[1] * A(1,1);

	#undef A
}

int ParseSVG::get_rect_attribute(Rect_attribute *ra)
{
	if (doc == NULL) {
		return DOC_IS_NULL;
	}

	if (last_rect_node == NULL)
		last_rect_node = find_first_element("rect");
	else
		last_rect_node = find_next_element(last_rect_node);

	if (last_rect_node == NULL) {
		return NO_MORE_ELEMENTS;
	}

	ra->x = atof(xmlGetProp(last_rect_node, "x") ) * ppi_scale;
	ra->y = atof(xmlGetProp(last_rect_node, "y") ) * ppi_scale;
	ra->width = atof(xmlGetProp(last_rect_node, "width") ) * ppi_scale;
	ra->height = atof(xmlGetProp(last_rect_node, "height") ) * ppi_scale;
	strcpy(ra->id,xmlGetProp(last_rect_node, "id") );

	if(!parseTransformMatrix( ra->transform,xmlGetProp(last_rect_node, "transform") ))
	{
		memset(ra->transform, 0, sizeof(ra->transform));
		ra->transform[0] = ra->transform[3] = 1; //Identity matrix
		ra->transform[4] = ra->x;
		ra->transform[5] = ra->y;
	}
	else
	{
		float invMatr[6];
		float oldPos[2];
		float newPos[2];
		oldPos[0] = ra->x;
		oldPos[1] = ra->y;
		invertMatrix(ra->transform,invMatr); //find inverce matrix
		multiplyMatrixVec2(invMatr, oldPos, newPos); //miltiply coords to inverce matrix -> find src coord

		ra->x = newPos[0];
		ra->y = newPos[1];
		ra->transform[4] = ra->x;
		ra->transform[5] = ra->y;
	}

	return 0;
}

// ---------------------------------------------------------------------------

int ParseSVG::get_text_attribute(Text_attribute *ta)
{
	if (doc == NULL) {
		return DOC_IS_NULL;
	}

	last_tspan_node = find_next_tspan();

	if (last_tspan_node == NULL) {
		return NO_MORE_ELEMENTS;
	}

	xmlNodePtr last_tspan_node_child = last_tspan_node->parent;
	if (last_tspan_node_child == NULL) {
		return NO_MORE_ELEMENTS;
	}

	ta->x = atof(xmlGetProp(last_tspan_node, "x") ) * ppi_scale;
	ta->y = atof(xmlGetProp(last_tspan_node, "y") ) * ppi_scale;
	strcpy(ta->id,xmlGetProp(last_tspan_node, "id") );

	// ������� ������ �� UTF8 � UTF-16 � ������ � ���� ���������
	char buf[200];
	strcpy(buf, xmlNodeGetContent(last_tspan_node->children) );
	Utf8ToUnicode(ta->text, buf, 99);

	// ����������� ������ ������
	char buf2[3];
	buf2[2] = 0;
	ta->font_size = 0;
	if(xmlHasProp(last_tspan_node, "style"))
	{
		char* prop = xmlGetProp(last_tspan_node, "style");
		if(prop && (strlen(prop)>=12))
		{
			strncpy(buf2, prop + 10, 2);
			ta->font_size = strtol(buf2,NULL,10);
        }
	}

	//if can't get font size - get from upper node
	if((ta->font_size == 0) && (xmlHasProp(last_text_node, "style")))
	{
		char* prop = xmlGetProp(last_text_node, "style");
		if(prop && (strlen(prop)>=12))
		{
			strncpy(buf2, prop + 10, 2);
			ta->font_size = atoi(buf2);
        }
	}

	//if can't get font size - set to 18
	if(ta->font_size == 0)
	{
		ta->font_size = 18;
    }

	//Get parent transform
	if(!parseTransformMatrix( ta->transform,xmlGetProp(last_tspan_node_child, "transform") ))
	{
		memset(ta->transform, 0, sizeof(ta->transform));
		ta->transform[0] = ta->transform[3] = 1; //Identity matrix
		ta->transform[4] = ta->x;
		ta->transform[5] = ta->y;
	}
	else
	{
		float invMatr[6];
		float oldPos[2];
		float newPos[2];
		oldPos[0] = ta->x;
		oldPos[1] = ta->y;
		invertMatrix(ta->transform,invMatr); //find inverce matrix
		multiplyMatrixVec2(invMatr, oldPos, newPos); //miltiply coords to inverce matrix -> find src coord

		ta->x = newPos[0];
		ta->y = newPos[1];
		ta->transform[4] = ta->x;
		ta->transform[5] = ta->y;
	}

	return 0;
}

// ---------------------------------------------------------------------------

int ParseSVG::get_line_attribute(Line_attribute *la)
{
	if (doc == NULL) {
		return DOC_IS_NULL;
	}

	if (last_line_node == NULL)
		last_line_node = find_first_element("line");
	else
		last_line_node = find_next_element(last_line_node);

	if (last_line_node == NULL) {
		return NO_MORE_ELEMENTS;
	}

	la->x1 = atof(xmlGetProp(last_line_node, "x1") ) * ppi_scale;
	la->y1 = atof(xmlGetProp(last_line_node, "y1") ) * ppi_scale;
	la->x2 = atof(xmlGetProp(last_line_node, "x2") ) * ppi_scale;
	la->y2 = atof(xmlGetProp(last_line_node, "y2") ) * ppi_scale;

	la->width = atof(xmlGetProp(last_line_node, "style") );

	// ����������� ������ line
	char buf[64], buf2[3];
	strcpy(buf, xmlGetProp(last_line_node, "style") );

	memset(buf2, 0, sizeof(buf2));
	strncpy(buf2, buf + StrLen(buf) - 2, 2);
	if (buf2[0] == ':')
    	buf2[0] = '0';
	la->width = atof(buf2);

	return 0;
}

// ---------------------------------------------------------------------------

xmlNodePtr ParseSVG::find_first_element(xmlChar *name)
{
	xmlNodePtr nodePtr;

	nodePtr = find_layer_node();
	nodePtr = find_child_by_name(nodePtr, name);

	return nodePtr;
}

// ---------------------------------------------------------------------------

xmlNodePtr ParseSVG::find_next_element(xmlNodePtr cur)
{
	xmlChar name[32];
	strcpy(name, cur->name);

	while(cur->next != NULL)
	{
		cur = cur->next;
		if (cur->type == XML_ELEMENT_NODE)
			if ( !xmlStrcmp(cur->name, name) )
				return cur;
	}

	return NULL;
}

// ---------------------------------------------------------------------------

xmlNodePtr ParseSVG::find_next_tspan()
{
	// ���� ������ ������� ������
	if (last_tspan_node == NULL) {
		last_text_node = find_first_element("text");
		if (last_text_node == NULL) {
			return NULL;
		}

		last_tspan_node = find_child_by_name(last_text_node, "tspan");
		return last_tspan_node;
	}

	// ��������� ������� <tspan> � ��� �� <text>
	if (last_tspan_node->next != NULL) {
		last_tspan_node = last_tspan_node->next;
		return last_tspan_node;
	}
    	// <tspan> � ����� <text>
	else{
		last_text_node = find_next_element(last_text_node);
		if (last_text_node == NULL) {
			return NULL;
		}

		last_tspan_node = find_child_by_name(last_text_node, "tspan");
		return last_tspan_node;
    }

}

// ---------------------------------------------------------------------------

int ParseSVG::get_page_attribute(float* pWidth, float* pHeight)
{
 	xmlNodePtr nodePtr;

	nodePtr = find_page_node();

	(*pWidth) = atof(xmlGetProp(nodePtr,"width")) * ppi_scale;
	(*pHeight) = atof(xmlGetProp(nodePtr,"height"))  * ppi_scale;
	return 0;
}



//#pragma package(smart_init)
