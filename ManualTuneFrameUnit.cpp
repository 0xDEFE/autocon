//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ManualTuneFrameUnit.h"
#include "BScanFrameUnit.h"
#include "AScanFrameUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "BasicFrameUnit"
#pragma resource "*.dfm"
//TManualTuneFrame *ManualTuneFrame;
//---------------------------------------------------------------------------
__fastcall TManualTuneFrame::TManualTuneFrame(TComponent* Owner)
    : TBasicFrame(Owner)
{
    aquiredSignalsFlags = 0xFFFFFFFF;
    defaultOwner = m_FramePanel;
    bPopOnEmpty = true;

    groupBtnsArr.push_back(ScanGroupBtn3);
    groupBtnsArr.push_back(ScanGroupBtn5);
    groupBtnsArr.push_back(ScanGroupBtn6);
    groupBtnsArr.push_back(ScanGroupBtn8);
    groupBtnsArr.push_back(ScanGroupBtn10);
    groupBtnsArr.push_back(ScanGroupBtn12);
    groupBtnsArr.push_back(ScanGroupBtn14);

    for(unsigned int i = 0; i < groupBtnsArr.size(); i++)
    {
        groupBtnsArr[i]->Caption = StringFormatU(L"������\r\n%d",groupBtnsArr[i]->Tag);
    }

    channelBtnsArr.push_back(ChanBtn1);
    channelBtnsArr.push_back(ChanBtn2);
    channelBtnsArr.push_back(ChanBtn3);
    channelBtnsArr.push_back(ChanBtn4);
    channelBtnsArr.push_back(ChanBtn5);
    channelBtnsArr.push_back(ChanBtn6);
    channelBtnsArr.push_back(ChanBtn7);
    channelBtnsArr.push_back(ChanBtn8);
    channelBtnsArr.push_back(ChanBtn9);
    channelBtnsArr.push_back(ChanBtn10);
    channelBtnsArr.push_back(ChanBtn11);
    channelBtnsArr.push_back(ChanBtn12);
    channelBtnsArr.push_back(ChanBtn13);
    channelBtnsArr.push_back(ChanBtn14);
    channelBtnsArr.push_back(ChanBtn15);
    channelBtnsArr.push_back(ChanBtn16);
    channelBtnsArr.push_back(ChanBtn17);
    channelBtnsArr.push_back(ChanBtn18);
    channelBtnsArr.push_back(ChanBtn19);
    channelBtnsArr.push_back(ChanBtn20);
    channelBtnsArr.push_back(ChanBtn21);

    curGroupIdx = -1;
    firstChannelIdx = 0;

    SelectChannelsPage->TabVisible = false;
    MovePage->TabVisible = false;
    OtherPage->TabVisible = false;
    LeftPanel->Visible = false;
    bToggleBtnState = true;

    sScanChannelDescription scanChanDesc;
    for(unsigned int j=0;j<AutoconMain->Config->GetAllScanChannelsCount();j++)
    {
        if(!AutoconMain->Config->getSChannelbyIdx(j, &scanChanDesc))
            continue;

        if(scanChanDesc.Id >= (int)selectedChannels.size())
            selectedChannels.resize(scanChanDesc.Id + 1);

        selectedChannels[scanChanDesc.Id].chGroups.insert( scanChanDesc.StrokeGroupIdx );
        selectedChannels[scanChanDesc.Id].bSelected = false;
        selectedChannels[scanChanDesc.Id].scanIdx = j;
        selectedChannels[scanChanDesc.Id].bValid = true;
    }

    BScanFrame = NULL;
}
//---------------------------------------------------------------------------
void TManualTuneFrame::OnMFrameCreate(TBasicFrame* pMain)
{
    TestTimer->Enabled = true;

    BScanFrame = this->pushFrame<TBScanFrame>(MF_BSCAN_MANUAL_TUNE_FLAGS);
    BScanFrame->setChBtnCallback(OnBScanChannelBtnStateChanged);
};
void TManualTuneFrame::OnMFrameDestroy(TBasicFrame* pMain)
{
    BScanFrame->setChBtnCallback(NULL);
    BScanFrame = NULL;
};
void TManualTuneFrame::OnMFrameAttach(TBasicFrame* pMain)
{
    AutoconMain->ac->TuneSetState(tmManInit);

    SetChGroup(3, true);
    UpdateChannelButtons();
    UpdateBScanFrameChannels();
    //UpdateChannelsBScan();
}
void TManualTuneFrame::OnMFrameDetach(TBasicFrame* pMain)
{
    AutoconMain->ac->TuneSetState(tmInit);
    AutoconMain->ac->SetMode(acReady);
}

void TManualTuneFrame::SetCurrentPanel(int idx)
{
    TToolButton* toolBtns[3];
    TTabSheet* panels[3];

    toolBtns[0] = ShowSelectChannelPanelBtn;
    toolBtns[1] = ShowMovePanelBtn;
    toolBtns[2] = ClearBtn;
    panels[0] = SelectChannelsPage;
    panels[1] = MovePage;
    panels[2] = OtherPage;

    if((!LeftPanel->Visible || (PageControl1->ActivePage != panels[idx])) && (idx != -1))
    {
        LeftPanel->Visible = true;
        PageControl1->ActivePage = panels[idx];
        toolBtns[idx]->Down = true;
        for(int i = 0; i < 3; i++)
            if(i != idx)
                toolBtns[i]->Down = false;
    }
    else
    {
        LeftPanel->Visible = false;
        for(int i = 0; i < 3; i++)
            toolBtns[i]->Down = false;
    }
}

void __fastcall TManualTuneFrame::ShowSelectChannelPanelBtnClick(TObject *Sender)
{
    SetCurrentPanel(0);
}
//---------------------------------------------------------------------------
void __fastcall TManualTuneFrame::ShowMovePanelBtnClick(TObject *Sender)
{
    SetCurrentPanel(1);
}
//---------------------------------------------------------------------------

void __fastcall TManualTuneFrame::ClearBtnClick(TObject *Sender)
{
    SetCurrentPanel(2);
}
//---------------------------------------------------------------------------
void __fastcall TManualTuneFrame::HidePanelBtnClick(TObject *Sender)
{
    SetCurrentPanel(-1);
}
//---------------------------------------------------------------------------
void __fastcall TManualTuneFrame::CloseBtnClick(TObject *Sender)
{
    pParentFrame->popFrame();
}
//---------------------------------------------------------------------------

void __fastcall TManualTuneFrame::ScanGroupBtnClick(TObject *Sender)
{
    curGroupIdx = ((TSpeedButton*)Sender)->Tag;
    firstChannelIdx = 0;

    bool bNoOneSelected = true;
    for(unsigned int i = 0; i < selectedChannels.size(); i++)
    {
        if(!selectedChannels[i].hasGroup(curGroupIdx))
            continue;

        if(selectedChannels[i].bSelected)
        {
            bNoOneSelected = false;
            break;
        }
    }

    SetChGroup(curGroupIdx, bNoOneSelected);

    UpdateChannelButtons(false, true);
    UpdateBScanFrameChannels();
    //UpdateChannelsBScan();
    AutoconMain->DEV->SetChannelGroup(curGroupIdx);
    AutoconMain->DEV->Update(false);

    BScanFrame->updateHighlightedChannels();


    UpdateChannelClearBtn();
}
//---------------------------------------------------------------------------

void __fastcall TManualTuneFrame::ChanLeftBtnClick(TObject *Sender)
{
    firstChannelIdx -= channelBtnsArr.size();
    firstChannelIdx = std::max(0, firstChannelIdx);

    int curPageNumber = firstChannelIdx / channelBtnsArr.size() + 1;
    int totPageNumbers = ceil(float(curGroupSelChIndices.size()) / float(channelBtnsArr.size()));

    ChanRightBtn->Caption = L"���. " + IntToStr(curPageNumber + 1);
    ChanLeftBtn->Caption = L"���. " + IntToStr(curPageNumber - 1);

    UpdateChannelButtons(false, true);
}
//---------------------------------------------------------------------------
void __fastcall TManualTuneFrame::ChanToggleBtnClick(TObject *Sender)
{
    if(bToggleBtnState)
    {
        for(unsigned int i = 0; i < curGroupSelChIndices.size(); i++)
            selectedChannels[curGroupSelChIndices[i]].bSelected = false;
        ChanToggleBtn->Caption = L"������� ��� ������";
    }
    else
    {
        for(unsigned int i = 0; i < curGroupSelChIndices.size(); i++)
            selectedChannels[curGroupSelChIndices[i]].bSelected = true;
        ChanToggleBtn->Caption = L"�������� ��� ������";
    }

    UpdateChannelButtons(false,true);
    bToggleBtnState = !bToggleBtnState;
    UpdateBScanFrameChannels();
}

void TManualTuneFrame::UpdateChannelClearBtn()
{
    bool bAllSelected = true;
    bool bAllUnselected = true;

    for(unsigned int i = 0; i < selectedChannels.size(); i++)
    {
        if(!selectedChannels[i].hasGroup(curGroupIdx))
            continue;

        if(selectedChannels[i].bSelected)
        {
            bAllUnselected = false;
        }
        else
        {
            bAllSelected = false;
        }
    }

    if(bAllUnselected)
    {
        bToggleBtnState = false;
        ChanToggleBtn->Caption = L"������� ��� ������";
    }
    else if(bAllSelected)
    {
        bToggleBtnState = true;
        ChanToggleBtn->Caption = L"�������� ��� ������";
    }

}
//---------------------------------------------------------------------------
void __fastcall TManualTuneFrame::ChanRightBtnClick(TObject *Sender)
{
    firstChannelIdx += channelBtnsArr.size();
    firstChannelIdx = std::min((int)curGroupSelChIndices.size(), firstChannelIdx);

    int curPageNumber = firstChannelIdx / channelBtnsArr.size() + 1;
    int totPageNumbers = ceil(float(curGroupSelChIndices.size()) / float(channelBtnsArr.size()));

    ChanRightBtn->Caption = L"���. " + IntToStr(curPageNumber + 1);
    ChanLeftBtn->Caption = L"���. " + IntToStr(curPageNumber - 1);

    UpdateChannelButtons(false, true);
}
//---------------------------------------------------------------------------

void __fastcall TManualTuneFrame::ChanBtnClick(TObject *Sender)
{
    TSpeedButton* pChBtn = ((TSpeedButton*)Sender);
    int curIndex = pChBtn->Tag;

    SetChannel(curIndex, pChBtn->Down);
    UpdateChannelClearBtn();
    UpdateBScanFrameChannels();
}
//---------------------------------------------------------------------------
UnicodeString ConvertToBtnTitle(UnicodeString str)
{
    int spaceCnt = 0;
    for(int i = 0; i < str.Length(); i++)
    {
        if(str.c_str()[i] == L' ')
            spaceCnt++;
        if(spaceCnt == 2)
        {
            str.Insert(L"\r\n",i+1);
            break;
        }
    }
    return str;
}

void TManualTuneFrame::UpdateChannelButtons(bool bUpdateGroupBtns, bool bUpdateChBtns)
{
    if(bUpdateGroupBtns)
    {
        for(unsigned int i = 0; i < groupBtnsArr.size(); i++)
        {
            groupBtnsArr[i]->Down = (groupBtnsArr[i]->Tag == curGroupIdx);
        }
    }

    if(bUpdateChBtns)
    {
        if(curGroupIdx == -1)
        {
            for(unsigned int i = 0; i < channelBtnsArr.size(); i++)
                channelBtnsArr[i]->Visible = false;
            ChanLeftBtn->Visible = false;
            ChanToggleBtn->Visible = false;
            ChanRightBtn->Visible = false;
            ActiveChannelsLabel->Visible = false;
        }
        else
        {
            ChanLeftBtn->Visible = true;
            ChanToggleBtn->Visible = true;
            ChanRightBtn->Visible = true;
            ActiveChannelsLabel->Visible = true;

            int btnIdx = 0;
            sChannelDescription chanDesc;
            for(unsigned int i = firstChannelIdx; i < curGroupSelChIndices.size(); i++)
            {
                int chId = curGroupSelChIndices[i];
                if(btnIdx >= (int)channelBtnsArr.size())
                    break;
                if(!AutoconMain->Table->ItemByCID(chId, &chanDesc))
                    continue;

                channelBtnsArr[btnIdx]->Tag = curGroupSelChIndices[i];
                channelBtnsArr[btnIdx]->GroupIndex = 100 + btnIdx;
                channelBtnsArr[btnIdx]->Down = selectedChannels[chId].bSelected;
                channelBtnsArr[btnIdx]->Caption = ConvertToBtnTitle(chanDesc.Title);
                channelBtnsArr[btnIdx]->Visible = true;
                btnIdx++;
            }

            for(unsigned int i = btnIdx; i < channelBtnsArr.size(); i++)
            {
                channelBtnsArr[i]->Visible = false;
            }

            ChanLeftBtn->Visible = firstChannelIdx > 0;
            ChanRightBtn->Visible = (btnIdx >= (int)channelBtnsArr.size());
        }
    }
}

void TManualTuneFrame::SetChGroup(int groupIdx, bool bSelAll)
{
//    if(bSelAll)
//    {
//        for(int i = 0; i < selectedChannels.size(); i++)
//            selectedChannels[i].bSelected = false;
//    }

    curGroupIdx = groupIdx;

    curGroupSelChIndices.clear();
    //sScanChannelDescription scanChanDesc;
    for(unsigned int i=0;i<selectedChannels.size();i++)
    {
        if(!selectedChannels[i].bValid)
            continue;
//        if(!AutoconMain->Config->getSChannelbyIdx(j, &scanChanDesc))
//            continue;
        //if(selectedChannels[scanChanDesc.Id].hasGroup(groupIdx))
        //if(scanChanDesc.StrokeGroupIdx == curGroupIdx)
        if(selectedChannels[i].hasGroup(curGroupIdx))
        {
            TRACE("SetChGroup: groupIdx-%d, id-%d, selected-%d",curGroupIdx, i, selectedChannels[i].bSelected || bSelAll);
            selectedChannels[i].bSelected =
                selectedChannels[i].bSelected || bSelAll;

//            bool bChannelAlreadyExists = false;
//            for(int j = 0; j < curGroupSelChIndices.size(); j++)
//            {
//                int sgIdx = curGroupSelChIndices[j];
//                if(sgIdx == i)
//                    continue;
//
//                if(selectedChannels[sgIdx].chId == selectedChannels[i].chId)
//                    bChannelAlreadyExists = true;
//            }
            //if(!bChannelAlreadyExists)
                curGroupSelChIndices.push_back(i);
        }
//        if(bSelAll)
//            selectedChannels[scanChanDesc.Id].bSelected =
//                selectedChannels[scanChanDesc.Id].bSelected || (scanChanDesc.StrokeGroupIdx == curGroupIdx);
//        else
//            selectedChannels[scanChanDesc.Id].bSelected =
//                selectedChannels[scanChanDesc.Id].bSelected && (scanChanDesc.StrokeGroupIdx == curGroupIdx);
//
//        if(scanChanDesc.StrokeGroupIdx == curGroupIdx)
//        {
//
//            curGroupSelChIndices.push_back(scanChanDesc.Id);
//        }
    }
}

void TManualTuneFrame::SetChannel(int chIdx, bool bVal)
{
    if(selectedChannels[chIdx].chGroups.find(curGroupIdx) !=
                    selectedChannels[chIdx].chGroups.end())
    {
        selectedChannels[chIdx].bSelected = bVal;
    }
}

//void TManualTuneFrame::OnAcMotion(eMainUnitSignalType sigtype, int WorkCycle)
//{
//    int t = 0;
//    t++;
//}
void __fastcall TManualTuneFrame::CaretSpinChange(TObject *Sender)
{
    TComponent* pComponent = (TComponent*)Sender;
    static bool bValueInProcess = false;
    if(bValueInProcess)
        return;
    bValueInProcess = true;


    switch(pComponent->Tag)
    {
		case 0:
            if(TopCaretSpin->Value < 0)
            {
                TopCaretMoveLeftBtn->Down = true;
                TopCaretMoveRightBtn->Down = false;
            }
            else if(TopCaretSpin->Value > 0)
            {
                TopCaretMoveLeftBtn->Down = false;
                TopCaretMoveRightBtn->Down = true;
            }
            else
            {
                TopCaretMoveLeftBtn->Down = false;
                TopCaretMoveRightBtn->Down = false;
            }
            break;
        case 1:
            if(BtmCaretSpin->Value < 0)
            {
                BtmCaretMoveLeftBtn->Down = true;
                BtmCaretMoveRightBtn->Down = false;
            }
            else if(BtmCaretSpin->Value > 0)
            {
                BtmCaretMoveLeftBtn->Down = false;
                BtmCaretMoveRightBtn->Down = true;
            }
            else
            {
                BtmCaretMoveLeftBtn->Down = false;
                BtmCaretMoveRightBtn->Down = false;
            }
            break;
        case 2:
            if(TopCaretMoveLeftBtn->Down)
            {
                TopCaretSpin->Value = -abs(TopCaretSpin->Value);
			}
			else TopCaretSpin->Value = 0;
            break;
        case 3:
            if(TopCaretMoveRightBtn->Down)
            {
                TopCaretSpin->Value = abs(TopCaretSpin->Value);
			}
			else TopCaretSpin->Value = 0;
            break;
        case 4:
            if(BtmCaretMoveLeftBtn->Down)
            {
                BtmCaretSpin->Value = -abs(BtmCaretSpin->Value);
			}
			else BtmCaretSpin->Value = 0;
            break;
        case 5:
            if(BtmCaretMoveRightBtn->Down)
            {
				BtmCaretSpin->Value = abs(BtmCaretSpin->Value);
			}
			else BtmCaretSpin->Value = 0;
            break;
    }


    bValueInProcess = false;
}
//---------------------------------------------------------------------------

void __fastcall TManualTuneFrame::BtmCaretSpinClick(TObject *Sender)
{
    BtmCaretSpin->SelectAll();
    KeyboardPanel->Visible = true;
}
//---------------------------------------------------------------------------

void __fastcall TManualTuneFrame::TopCaretSpinClick(TObject *Sender)
{
    TopCaretSpin->SelectAll();
    KeyboardPanel->Visible = true;
}
//---------------------------------------------------------------------------

void __fastcall TManualTuneFrame::m_MoveCaretsBtnClick(TObject *Sender)
{
    if((!TopCaretSpin->Value && !BtmCaretSpin->Value) || TopCaretSpin->Text.IsEmpty() || BtmCaretSpin->Text.IsEmpty())
    {
        Application->MessageBoxW(L"�� ������� �������� ��� �������!",
                                    L"��������������", MB_OK | MB_ICONWARNING);
        return;
    }

    cScanScriptProgramm* pPrg = AutoconMain->ac->LockManualMode();
	cScanScriptCmdBuffer& commandBuf = pPrg->getCode();

    //commandBuf.pushCommand(SSC_SCAN,1,0);

    int topOffset = TopCaretSpin->Value;
    int btmOffset = BtmCaretSpin->Value;

    if(topOffset || btmOffset)
        commandBuf.pushCommand(SSC_MOVCARET,2,topOffset,btmOffset);

    AutoconMain->ac->UnlockManualMode();
    UpdateCaretPos();
}

void TManualTuneFrame::UpdateCaretPos()
{
    bool bCaretActive[2];
	int iCaretPos[2];

    if(!AutoconMain->ac->GetCaretStatus(false, bCaretActive,iCaretPos))
        return;
	if(!AutoconMain->ac->GetCaretStatus(true, bCaretActive+1,iCaretPos+1))
        return;

	const int maxRange = 1300;

	m_TopCaretTrack->Position = iCaretPos[0] * 100 / maxRange;
	m_BotCaretTrack->Position = iCaretPos[1] * 100 / maxRange;

	m_TopCaretLabel->Caption = StringFormatU(L"Top: %d", iCaretPos[0]);
	m_BotCaretLabel->Caption = StringFormatU(L"Bot: %d", iCaretPos[1]);
}
//---------------------------------------------------------------------------

void __fastcall TManualTuneFrame::m_CalibrateOnMoveBtnClick(TObject *Sender)
{
    if((curGroupIdx == -1) || (selectedChannels.empty()))
    {
        Application->MessageBoxW(L"�� ���� ����� �� ������!",
                                    L"��������������", MB_OK | MB_ICONWARNING);
        return;
    }

    if(!TopCaretSpin->Value && !BtmCaretSpin->Value)
    {
        Application->MessageBoxW(L"�� ������� �������� ��� �������!",
                                    L"��������������", MB_OK | MB_ICONWARNING);
        return;
    }

    AutoconMain->Rep->UpdateChannelIndicationData();

    cScanScriptProgramm* pPrg = AutoconMain->ac->LockManualMode();
	cScanScriptCmdBuffer& commandBuf = pPrg->getCode();

    commandBuf.pushCommand(SSC_TUNECLEAR,0);
    commandBuf.pushCommand(SSC_SETSG,1, curGroupIdx);
    commandBuf.pushCommand(SSC_SCAN,1,1);

    int topOffset = TopCaretSpin->Value;
    int btmOffset = BtmCaretSpin->Value;

    if(topOffset || btmOffset)
        commandBuf.pushCommand(SSC_MOVCARET,2,topOffset,btmOffset);

    for(unsigned int i = 0; i < selectedChannels.size(); i++)
    {
        if(selectedChannels[i].bSelected)
            commandBuf.pushCommand(SSC_TUNECH,1,i);
    }

    commandBuf.pushCommand(SSC_TUNECLEAR,0);

    AutoconMain->ac->UnlockManualMode();
    UpdateCaretPos();
}
//---------------------------------------------------------------------------

void __fastcall TManualTuneFrame::m_MoveToHomeBtnClick(TObject *Sender)
{
    cScanScriptProgramm* pPrg = AutoconMain->ac->LockManualMode();
	cScanScriptCmdBuffer& commandBuf = pPrg->getCode();

    //commandBuf.pushCommand(SSC_SCAN,1,0);
    commandBuf.pushCommand(SSC_MOVTOBASE,0);
    AutoconMain->ac->UnlockManualMode();
    UpdateCaretPos();
}
//---------------------------------------------------------------------------

void __fastcall TManualTuneFrame::m_UpdateCaretsPosBtnClick(TObject *Sender)
{
    UpdateCaretPos();
}
//---------------------------------------------------------------------------

void __fastcall TManualTuneFrame::ClearBScanBtnClick(TObject *Sender)
{
    BScanFrame->ClearData();
}
//---------------------------------------------------------------------------

void __fastcall TManualTuneFrame::ResetPathEncoderBtnClick(TObject *Sender)
{
    AutoconMain->DEV->ResetPathEncoder();
}
//---------------------------------------------------------------------------

void __fastcall TManualTuneFrame::OnBScanChannelBtnStateChanged(TObject *Sender)
{
    if(Sender != BScanFrame)
        return;

    int chId = BScanFrame->getChChangedBtnId();
    bool chState = BScanFrame->getChChangedBtnState();
    if((chId < 0) || (chId >= (int)selectedChannels.size()))
        return;
    if(!selectedChannels[chId].bValid)
        return;
    selectedChannels[chId].bSelected = chState;
    UpdateChannelButtons(false,true);
}

void TManualTuneFrame::UpdateBScanFrameChannels()
{
    BScanFrame->disableChannels();

    for(unsigned int i = 0; i < curGroupSelChIndices.size(); i++)
    {
        TRACE("UpdateBScanFrameChannels: chid-%d, val-%d",curGroupSelChIndices[i],selectedChannels[curGroupSelChIndices[i]].bSelected);
        BScanFrame->enableChannel(curGroupSelChIndices[i], selectedChannels[curGroupSelChIndices[i]].bSelected);
    }

    BScanFrame->updateHighlightedChannels();
    BScanFrame->UpdatePBoxData(false);
    BScanFrame->DrawPBoxData();
}

void __fastcall TManualTuneFrame::onSetOffsetValueBtn(TObject *Sender)
{
    TSpeedButton* pBtn = dynamic_cast<TSpeedButton*>(Sender);
    if(TopCaretSpin->Focused())
    {
        int spinSign = TopCaretMoveLeftBtn->Down ? -1 : TopCaretMoveRightBtn->Down ? 1 : TopCaretSpin->Value < 0 ? -1 : 1;
        TopCaretSpin->Value = int(pBtn->Tag) * spinSign;
//        TopCaretSpin->Color = clMoneyGreen;
//        BtmCaretSpin->Color = clWhite;
    }
    else if(BtmCaretSpin->Focused())
    {
        int spinSign = BtmCaretMoveLeftBtn->Down ? -1 : BtmCaretMoveRightBtn->Down ? 1 : BtmCaretSpin->Value < 0 ? -1 : 1;
        BtmCaretSpin->Value = int(pBtn->Tag) * spinSign;
    }
}
//---------------------------------------------------------------------------

