﻿/**
 * @file BScanFrameUnit.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef BScanFrameUnitH
#define BScanFrameUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>

#include "MainUnit.h"
#include "BScanPainter.h"
#include "BasicFrameUnit.h"
#include <Vcl.Dialogs.hpp>

#include "Autocon.inc"


#define MF_BSCAN_FLAG_HANDSCAN_BTN          0x01    //!< Кнопка перехода в ручники
#define MF_BSCAN_FLAG_CHANNEL_BTN           0x02    //!< Разрешить нажатие на кнопку канала
#define MF_BSCAN_FLAG_CAPTION_CLOSE_PROTO   0x04    //!< Кнопка завершения будет называться "Закрыть протокол"
#define MF_BSCAN_FLAG_CAPTION_CLOSE         0x08    //!< Кнопка завершения будет называться "Закрыть"
#define MF_BSCAN_FLAG_MANUAL_TUNE_BTN       0x10    //!< Кнопка ручной настройки
#define MF_BSCAN_FLAG_ONLY_SHOW             0x20    //!< Режим отображения: без дополнительных кнопок
#define MF_BSCAN_FLAG_VISUALMODE_SEARCH     0x40    //!< Визуальный режим: поиск
#define MF_BSCAN_FLAG_VISUALMODE_TUNE       0x80    //!< Визуальный режим: настройка
#define MF_BSCAN_FLAG_HIDE_INACTIVE_CH      0x100   //!< Скрыть неактивные каналы
#define MF_BSCAN_FLAG_ASCAN_CALIB_MODE      0x200   //!< Показавать а-развертку в режиме настройки
#define MF_BSCAN_FLAG_COMPARE_WITH_MODEL    0x400   //!< Сравнивать с эталонной моделью по завершению сканирования

//! Флаги для режима Поиск
#define MF_BSCAN_SEARCH_FLAGS (MF_BSCAN_FLAG_HANDSCAN_BTN | MF_BSCAN_FLAG_CHANNEL_BTN | MF_BSCAN_FLAG_VISUALMODE_SEARCH)
//! Флаги для режима Тест
#define MF_BSCAN_TEST_FLAGS (MF_BSCAN_FLAG_CHANNEL_BTN | MF_BSCAN_FLAG_VISUALMODE_SEARCH | MF_BSCAN_FLAG_COMPARE_WITH_MODEL)
//! Флаги для режима Настройка
#define MF_BSCAN_TUNE_FLAGS (MF_BSCAN_FLAG_CHANNEL_BTN | MF_BSCAN_FLAG_MANUAL_TUNE_BTN | MF_BSCAN_FLAG_VISUALMODE_TUNE | MF_BSCAN_FLAG_ASCAN_CALIB_MODE)
//! Флаги для режима Ручная настройка
#define MF_BSCAN_MANUAL_TUNE_FLAGS (MF_BSCAN_FLAG_HIDE_INACTIVE_CH | MF_BSCAN_FLAG_CHANNEL_BTN | MF_BSCAN_FLAG_ONLY_SHOW | MF_BSCAN_FLAG_VISUALMODE_TUNE | MF_BSCAN_FLAG_ASCAN_CALIB_MODE)

//---------------------------------------------------------------------------
//! Отображает B-развертку
class TBScanFrame : public TBasicFrame
{
__published:	// IDE-managed Components
    TGroupBox *GroupBox1;
    TLabel *ThLabel;
    TTrackBar *ThTrackBar;
    TPanel *Button18;
    TBitBtn *CloseRepButton;
    TBitBtn *ManualCalibrationBtn;
    TButton *Button12;
    TBitBtn *HandScanButton;
    TPaintBox *BScanPBox;
    TSpeedButton *ChangeLayoutBtn;
    TSpeedButton *m_DefectHgBtn;
    TTimer *DrawTimer;
    TSpeedButton *m_NoHgBtn;
    TSpeedButton *m_ActiveHgBtn;
    TTimer *EndMotionTimer;
    TGridPanel *GridPanel1;
    TBitBtn *ThTrackBarLeftBtn;
    TBitBtn *ThTrackBarRightBtn;
	TPanel *BtnsPanel1;
    TOpenDialog *OpenDialog1;
	TPanel *BtnsPanel2;
	TPanel *MeasMovePanel;
	TLabel *Label1;
	TSpeedButton *SpeedButton1;
	TPanel *Panel1;
	TBitBtn *moveMeasModelRight;
	TBitBtn *moveMeasModelLeft;
	TPanel *Panel4;
	TPanel *Panel2;
	TBitBtn *BitBtn1;
	TBitBtn *BitBtn2;
	TPanel *Panel3;
	TPanel *Panel9;
	TPanel *Panel5;
    void __fastcall FrameResize(TObject *Sender);
    void __fastcall DrawTimerTimer(TObject *Sender);
    void __fastcall CloseRepButtonClick(TObject *Sender);
    void __fastcall ChangeLayoutBtnClick(TObject *Sender);
    void __fastcall m_DefectHgBtnClick(TObject *Sender);
    void __fastcall ThTrackBarChange(TObject *Sender);
    void __fastcall m_NoHgBtnClick(TObject *Sender);
    void __fastcall m_ActiveHgBtnClick(TObject *Sender);
    void __fastcall HandScanButtonClick(TObject *Sender);
    void __fastcall BScanPBoxPaint(TObject *Sender);
    void __fastcall BScanPBoxMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
    void __fastcall ManualCalibrationBtnClick(TObject *Sender);
    void __fastcall EndMotionTimerTimer(TObject *Sender);
    void __fastcall ThTrackBarLeftBtnClick(TObject *Sender);
    void __fastcall ThTrackBarRightBtnClick(TObject *Sender);
    void __fastcall moveMeasModel(TObject *Sender);
	void __fastcall SpeedButton1Click(TObject *Sender);
private:	// User declarations
    cBScanPainter BScanPainter;

    void OnMFrameAttach(TBasicFrame* pMain);
    void OnMFrameDetach(TBasicFrame* pMain);
    void OnMFrameCreate(TBasicFrame* pMain);
    void OnMFrameDestroy(TBasicFrame* pMain);
    void OnAcMotion(eMainUnitSignalType sigtype, int WorkCycle, eMotionState state, bool bStateChanged);

    int MouseX;
	int MouseY;

    int AScanSysCoord;
    Shift_CID_List Shift_CID;
    int Shift_CID_Cnt;
    BScan_Color_List BSColor;

    int _changedBtnId;
    bool _changedBtnState;
    TNotifyEvent OnBScanChannelBtnStateChanged;

    BScanMeasure etalonMeasure;
    BScanMeasure currentMeasure;

    void __fastcall OnAScanFrameLeftBtnClick(TObject *Sender);
    void __fastcall OnAScanFrameRightBtnClick(TObject *Sender);
    void __fastcall OnAScanFrameCloseBtnClick(TObject *Sender);
    void ReDrawAScanOnBScan();
    void ShowAScanOnBScan(int X, int Y, int showInfoId);
public:		// User declarations
    __fastcall TBScanFrame(TComponent* Owner);
    void ClearData();
    void SetFrameFlags(DWORD flags);
    UnicodeString GetFrameName() {return L"TBScanFrame";};
    void UpdatePBoxData(bool NewItemFlag);
    void DrawPBoxData();
    void updateHighlightedChannels();

    void disableChannels();
    void enableChannel(CID idx, bool bVal);

    int getChChangedBtnId() {return _changedBtnId;}
    bool getChChangedBtnState() {return _changedBtnState;}
    void setChBtnCallback(TNotifyEvent evt) {OnBScanChannelBtnStateChanged = evt;}
};
//---------------------------------------------------------------------------
//extern PACKAGE TBScanFrame *BScanFrame;
//---------------------------------------------------------------------------
#endif
