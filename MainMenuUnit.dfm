object MainMenuForm: TMainMenuForm
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'MainMenuForm'
  ClientHeight = 1024
  ClientWidth = 1217
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LeftMainPanel: TPanel
    Left = 0
    Top = 0
    Width = 397
    Height = 1024
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 8
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object GroupBox2: TGroupBox
      Left = 8
      Top = 175
      Width = 381
      Height = 841
      Align = alClient
      Caption = ' '#1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Panel3: TPanel
        Left = 2
        Top = 34
        Width = 377
        Height = 713
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Memo1: TMemo
          Left = 12
          Top = 48
          Width = 353
          Height = 161
          Align = alTop
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object Panel1: TPanel
          Left = 12
          Top = 12
          Width = 353
          Height = 36
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1069#1083#1077#1082#1090#1088#1086#1085#1085#1099#1077' '#1073#1083#1086#1082#1080':'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object GridPanel6: TGridPanel
          Left = 12
          Top = 209
          Width = 353
          Height = 320
          Align = alTop
          BevelOuter = bvNone
          ColumnCollection = <
            item
              Value = 50.000000000000000000
            end
            item
              SizeStyle = ssAbsolute
              Value = 10.000000000000000000
            end
            item
              Value = 50.000000000000000000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = Panel9
              Row = 0
            end
            item
              Column = 2
              Control = Panel12
              Row = 0
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -27
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          RowCollection = <
            item
              Value = 100.000000000000000000
            end>
          TabOrder = 2
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 171
            Height = 320
            Align = alClient
            BevelOuter = bvNone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -27
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object Label6: TLabel
              Left = 0
              Top = 76
              Width = 171
              Height = 26
              Align = alTop
              Alignment = taRightJustify
              Caption = #1055#1088#1086#1074#1077#1088#1077#1085#1086':'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = [fsUnderline]
              ParentFont = False
              Visible = False
              ExplicitLeft = 57
              ExplicitTop = 75
              ExplicitWidth = 114
            end
            object Label5: TLabel
              Left = 0
              Top = 102
              Width = 171
              Height = 50
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1053#1072#1088#1072#1073#1086#1090#1082#1072':'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = [fsUnderline]
              ParentFont = False
              ExplicitTop = 26
            end
            object Label2: TLabel
              Left = 0
              Top = 152
              Width = 171
              Height = 50
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1042#1077#1088#1089#1080#1103' '#1055#1054':'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = [fsUnderline]
              ParentFont = False
              ExplicitTop = 104
            end
            object m_BUM1NameLabel: TLabel
              Left = 0
              Top = 202
              Width = 171
              Height = 26
              Align = alTop
              Alignment = taRightJustify
              Caption = #1041#1059#1052' '#8470
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 105
              ExplicitTop = 200
              ExplicitWidth = 66
            end
            object m_BUM2NameLabel: TLabel
              Left = 0
              Top = 228
              Width = 171
              Height = 26
              Align = alTop
              Alignment = taRightJustify
              Caption = #1041#1059#1052' '#8470
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 105
              ExplicitTop = 225
              ExplicitWidth = 66
            end
            object m_BUM3NameLabel: TLabel
              Left = 0
              Top = 254
              Width = 171
              Height = 26
              Align = alTop
              Alignment = taRightJustify
              Caption = #1041#1059#1052' '#8470
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = 105
              ExplicitTop = 250
              ExplicitWidth = 66
            end
            object Label1: TLabel
              Left = 0
              Top = 26
              Width = 171
              Height = 50
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1058#1077#1089#1090'. '#1085#1072' '#1057#1054'-3'#1056':'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = [fsUnderline]
              ParentFont = False
              ExplicitLeft = 4
              ExplicitTop = -6
            end
            object Label4: TLabel
              Left = 0
              Top = 0
              Width = 171
              Height = 26
              Align = alTop
              Alignment = taRightJustify
              Caption = #1058#1077#1089#1090'. '#1085#1072' '#1057#1054#1055':'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = [fsUnderline]
              ParentFont = False
              ExplicitLeft = 49
              ExplicitWidth = 122
            end
          end
          object Panel12: TPanel
            Left = 181
            Top = 0
            Width = 172
            Height = 320
            Align = alClient
            BevelOuter = bvNone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -27
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            object m_JointsCheckedLabel: TLabel
              Left = 0
              Top = 76
              Width = 172
              Height = 26
              Align = alTop
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = []
              ParentFont = False
              Visible = False
              ExplicitTop = 75
              ExplicitWidth = 15
            end
            object m_WorkTimeLabel: TLabel
              Left = 0
              Top = 102
              Width = 172
              Height = 50
              Align = alTop
              AutoSize = False
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = []
              ParentFont = False
              ExplicitTop = 26
            end
            object m_SoftwareVersionLabel: TLabel
              Left = 0
              Top = 152
              Width = 172
              Height = 50
              Align = alTop
              AutoSize = False
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = -4
              ExplicitTop = 144
            end
            object m_BUM1StatusLabel: TLabel
              Left = 0
              Top = 202
              Width = 172
              Height = 26
              Align = alTop
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = []
              ParentFont = False
              ExplicitTop = 200
              ExplicitWidth = 15
            end
            object m_BUM2StatusLabel: TLabel
              Left = 0
              Top = 228
              Width = 172
              Height = 26
              Align = alTop
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = []
              ParentFont = False
              ExplicitTop = 225
              ExplicitWidth = 15
            end
            object m_BUM3StatusLabel: TLabel
              Left = 0
              Top = 254
              Width = 172
              Height = 26
              Align = alTop
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = []
              ParentFont = False
              ExplicitTop = 250
              ExplicitWidth = 15
            end
            object m_TestSO3PLabel: TLabel
              Left = 0
              Top = 26
              Width = 172
              Height = 50
              Align = alTop
              AutoSize = False
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = []
              ParentFont = False
              ExplicitLeft = -4
              ExplicitTop = -6
            end
            object m_TestSOPLabel: TLabel
              Left = 0
              Top = 0
              Width = 172
              Height = 26
              Align = alTop
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Myriad Pro'
              Font.Style = []
              ParentFont = False
              ExplicitWidth = 15
            end
          end
        end
      end
      object Panel7: TPanel
        Left = 2
        Top = 747
        Width = 377
        Height = 92
        Margins.Left = 1
        Margins.Top = 1
        Margins.Right = 1
        Margins.Bottom = 1
        Align = alBottom
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object ExitButton: TButton
          AlignWithMargins = True
          Left = 189
          Top = 10
          Width = 178
          Height = 72
          Margins.Left = 10
          Margins.Top = 10
          Margins.Right = 10
          Margins.Bottom = 10
          Align = alClient
          Caption = #1042#1099#1082#1083#1102#1095#1077#1085#1080#1077
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = ExitButtonClick
        end
        object OnButton: TButton
          AlignWithMargins = True
          Left = 10
          Top = 10
          Width = 159
          Height = 72
          Margins.Left = 10
          Margins.Top = 10
          Margins.Right = 10
          Margins.Bottom = 10
          Align = alLeft
          Caption = #1042#1082#1083#1102#1095#1077#1085#1080#1077
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = OnButtonClick
        end
      end
    end
    object Panel5: TPanel
      Left = 8
      Top = 8
      Width = 381
      Height = 94
      Align = alTop
      BevelOuter = bvNone
      Caption = #1040#1042#1058#1054#1050#1054#1053'-'#1057
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -53
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = Panel5Click
    end
    object Panel4: TPanel
      Left = 8
      Top = 129
      Width = 381
      Height = 27
      Align = alTop
      BevelOuter = bvNone
      Caption = ' '#1054#1040#1054' "'#1056#1072#1076#1080#1086#1072#1074#1080#1086#1085#1080#1082#1072'"'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object Panel6: TPanel
      Left = 8
      Top = 102
      Width = 381
      Height = 27
      Align = alTop
      BevelOuter = bvNone
      Caption = ' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075' 2014-2015'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object Panel2: TPanel
      Left = 8
      Top = 156
      Width = 381
      Height = 19
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
    end
  end
  object Panel8: TPanel
    Left = 397
    Top = 0
    Width = 820
    Height = 1024
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel8'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object InfoPanel: TPanel
      Left = 0
      Top = 983
      Width = 820
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object DebugMenuPanel: TGroupBox
      Left = 0
      Top = 899
      Width = 820
      Height = 84
      Align = alBottom
      Caption = 'Debug'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'Myriad Pro'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Visible = False
      object Button1: TButton
        Left = 157
        Top = 56
        Width = 75
        Height = 25
        Caption = 'Button1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Visible = False
        OnClick = Button1Click
      end
      object IPComboBox: TComboBox
        Left = 6
        Top = 31
        Width = 226
        Height = 26
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = []
        ItemIndex = 4
        ParentFont = False
        TabOrder = 1
        Text = '3 '#1073#1083#1086#1082#1072' (101,102,103)'
        Items.Strings = (
          '192.168.100.100'
          '192.168.100.101'
          '192.168.100.102'
          '192.168.100.103'
          '3 '#1073#1083#1086#1082#1072' (101,102,103)'
          '3 '#1073#1083#1086#1082#1072' (100,102,103)'
          '2 '#1073#1083#1086#1082#1072' (100,102)')
      end
      object ManualModeBtn: TButton
        Left = 344
        Top = 28
        Width = 100
        Height = 50
        Caption = 'Manual mode'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        WordWrap = True
        OnClick = OnManualModeBtnClicked
      end
      object OpenConnectionButton: TButton
        Left = 238
        Top = 28
        Width = 100
        Height = 50
        Caption = 'Open connection'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        WordWrap = True
        OnClick = OpenConnectionButtonClick
      end
      object ShowHideDebugMenus: TButton
        Left = 524
        Top = 28
        Width = 100
        Height = 50
        Caption = #1057#1082#1088#1099#1090#1100' '#1084#1077#1085#1102
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        WordWrap = True
        OnClick = ShowHideDebugMenusClick
      end
      object TuneTableBtn: TButton
        Left = 453
        Top = 28
        Width = 65
        Height = 50
        Caption = 'Tune Table'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        WordWrap = True
        OnClick = TuneTableBtnClick
      end
      object Button2: TButton
        Left = 76
        Top = 57
        Width = 75
        Height = 25
        Caption = 'Open File'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        OnClick = Button2Click
      end
    end
    object GridPanel7: TGridPanel
      Left = 0
      Top = 0
      Width = 820
      Height = 899
      Align = alClient
      BevelOuter = bvNone
      ColumnCollection = <
        item
          Value = 100.000000000000000000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = GroupBox1
          Row = 0
        end
        item
          Column = 0
          Control = GroupBox3
          Row = 1
        end
        item
          Column = 0
          Control = GroupBox4
          Row = 2
        end>
      RowCollection = <
        item
          Value = 25.000000000000000000
        end
        item
          Value = 50.000000000000000000
        end
        item
          Value = 25.000000000000000000
        end>
      TabOrder = 2
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 820
        Height = 224
        Align = alClient
        Caption = #1050#1086#1085#1090#1088#1086#1083#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object GridPanel8: TGridPanel
          Left = 2
          Top = 34
          Width = 816
          Height = 188
          Align = alClient
          BevelOuter = bvNone
          ColumnCollection = <
            item
              Value = 60.000000000000000000
            end
            item
              Value = 10.000000000000000000
            end
            item
              Value = 30.000000000000000000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = SearchButton
              Row = 0
            end
            item
              Column = 1
              Control = Panel10
              Row = 0
            end
            item
              Column = 2
              Control = Button5
              Row = 0
            end>
          RowCollection = <
            item
              Value = 100.000000000000000000
            end>
          TabOrder = 0
          object SearchButton: TBitBtn
            AlignWithMargins = True
            Left = 6
            Top = 6
            Width = 477
            Height = 176
            Margins.Left = 6
            Margins.Top = 6
            Margins.Right = 6
            Margins.Bottom = 6
            Align = alClient
            Anchors = []
            Caption = #1055#1086#1080#1089#1082
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -38
            Font.Name = 'Myriad Pro'
            Font.Style = []
            Glyph.Data = {
              222B0000424D222B000000000000360000002800000036000000430000000100
              180000000000EC2A000000000000000000000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8787878080808080
              8080808080808080808080808080808080808080808080808080808080808080
              8080808080808080808080808080808080808080808080808080808080808080
              8080808080808080808080808080808080808080808080808080808080808787
              87FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF86868680808080808080808080808080808080808080808080808080
              8080808080808080808080808080808080808080808080808080808080808080
              8080808080808080808080808080808080808080808080808080808080808080
              80808080808080808080868686FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFA2A2A29494949E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
              9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
              9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
              9E9E9E9E9E9E9E9E9E9E9E9696969A9A9AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFC7C7C79090909E9E9E9E9E9E9E9E9E9E9E9E9E9E
              9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
              9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
              9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E909090BEBEBEFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E78C8C8C9E9E9E9E9E9E9E9E9E9E
              9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
              9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
              9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E8E8E8EDFDFDFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF838383808080808080
              8080808080808080808080808080808080808080808080808080808080808080
              8080808080808080808080808080808080808080808080808080808080808080
              8080808080808080808080808080808080808080808080838383FBFBFBFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
              8080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB80
              8080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
              8080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB80
              8080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
              8080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB80
              8080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              F3F3F38585858080808080808080808080808080808080808080808080808080
              8080808080808080808080808080808080808080808080808080808080808080
              8080808080808080808080808080808080808080808080808080808080808080
              808080858585FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFB4B4B49292929E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
              9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
              9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
              9E9E9E9E9E9E9E808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF838383808080808080808080808080808080808080808080
              8080808080808080808080808080808080808080808080808080808080808080
              8080808080808080808080808080808080808080808080808080808080808080
              8080808080808080808080838383FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
              DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8585858080808080
              8080808080808080808080808080808080808080808080808080808080808080
              8080808080808080808080808080808080808080808080808080808080808080
              808080808080808080808080808080808080808080808080808080858585FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080AC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC80
              8080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              808080ACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACAC808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF808080ACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACAC808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF949494808080808080808080808080808080808080808080
              8080808080808080808080808080807882845C8790408D9C408D9C408D9C408D
              9C4C8B9768858B80808080808080808080808080808080808080808080808080
              8080808080808080808080949494FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFF97D6E238B0C810A0BD00A9CC00B3DB00B3DB01
              B4DB03B4DB03AFD502A4C618A4BF60C0D3D7EFF4FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFAFDFE920A7C100AFD500C2EF00CDFF00CDFF02CEFF
              05CEFF08CFFF0CD0FF0ED0FF11D1FF11CCF80DBDE428AAC368C3D5E7F5F8FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFF7FCFD68C3D500AACF00C6F600CDFF00CDFF01CDFF04CE
              FF07CFFF0BD0FF0ED0FF11D1FF13D2FF16D2FF18D3FF1BD4FF1DD4FF12BDE230
              ADC6C7E9EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFEFF9FB40B4CA00B8E200CDFF00CDFF00CDFF02CEFF05
              CEFF19CCF739C5E75EBED55FBFD553C2DD3DC9EA1CD4FF1ED4FF20D5FF22D5FF
              24D6FF22D0F80AAACA9FD9E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFF7FCFD40B4CA00C1ED00CDFF00CDFF00CDFF02CEFF
              2BC8ED91B6BEA9B0B2ADB9BCC2C2C2C2C2C2B7BEC0ACB7BAA8AEB073C0D129D4
              FC27D7FF2AD7FF2CD8FF2CD6FD0FAECFAFDFE9FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF58BDD100BBE600CDFF00CDFF00CDFF03CE
              FF63BED3A9B5B8CDCDCDD8D8D8D8D8D8D6D6D6D3D3D3D8D8D8D8D8D8D5D5D5BE
              C3C5A0B5B94ACFED30D9FF31D9FF33DAFF32D8FD38B0C8CFECF2FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF97D6E200AFD500CDFF00CDFF00CDFF19
              CAF595B4BCB8C2C4D8D8D8D1D1D1B7B7B7AEAEAEACACACABABABACACACB2B2B2
              C6C6C6D8D8D8D1D1D1ABB6B875C4D637DBFF39DBFF3BDBFF34D3F628AAC3F7FC
              FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FCFD20A7C100C8F800CDFF00CDFF
              08CDFC97B4BBCDCDCDD8D8D8C0C0C0AAAAAA8E8E8E8686867F7F7F7F7F7F8282
              828A8A8A9D9D9DB0B0B0D1D1D1D8D8D8B6BDBF6DC8DC3FDCFF40DDFF41DDFF1D
              B8D88FD2E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FD2E000B1D800CDFF00CD
              FF03CEFF63BED3B8C2C4D8D8D8B4B4B49B9B9B8080807F7F7F7F7F7F7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F898989AAAAAAC9C9C9D6D6D6A9B3B551D8F546DEFF
              48DFFF42D8F820A7C1F7FCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFAFEFCF4FCF8FFFFFFFFFFFF20A7C100C6F600
              CDFF02CEFF25C8EFA6B5B9D6D6D6C0C0C09B9B9B7F7F7F7F7F7F7F7F7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F838383ACACACD4D4D4CDCDCD9DB8
              BE4CE0FF4EE0FF4EE0FF1EB4D39FD9E4FFFFFFFAFEFCDEF6EAFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFAFEFCB2EACF66D6A0EFFBF5FFFFFFD7EFF400A2C3
              00CDFF01CDFF06CFFF95B4BCCFCFCFD1D1D1AAAAAA8080807F7F7F7F7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F909090B6B6B6D8
              D8D8B2BABC6ED0E553E1FF54E2FF38C9E658BDD1FFFFFFFFFFFF71D9A787DFB4
              DEF6EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFAFEFCB2EACF61D59C50D09287DFB4FFFFFFFFFFFF8FD2
              E000B1D800CDFF05CEFF1FCBF5A9B2B4D8D8D8B7B7B78E8E8E7F7F7F7F7F7F7F
              7F7F7F7F7F8080808D8D8D8E8E8E8282827F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
              A7A7A7CFCFCFC8C8C896BFC75AE3FF5BE3FF55DFFB10A0BDFFFFFFFFFFFFADE9
              CB50D09250D09287DFB4DEF6EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
              FFFFFFFFFFFFFFFAFEFCB2EACF61D59C50D09250D09250D092B2EACFFFFFFFFF
              FFFF70C7D800B6E003CEFF08CFFF4AC3E0B4BDBFD8D8D8AEAEAE8585857F7F7F
              7F7F7F7F7F7F838383AAAAAAB5B5B5B7B7B7AAAAAA8585857F7F7F7F7F7F7F7F
              7F7F7F7F9B9B9BC4C4C4D6D6D6ACAEAE5FE4FF60E5FF61E5FF10A0BDEFF9FBFF
              FFFFD3F3E450D09250D09250D09250D09287DFB4DEF6EAFFFFFFFFFFFFFFFFFF
              0000FFFFFFFAFEFCB2EACF61D59C50D09250D09250D09250D09250D09250D092
              50D09250D0921CADAB01BBE606CFFF0CD0FF5EBED5C2C2C2D5D5D5ABABAB9595
              95959595959595959595A8A8A8CBCBCBD8D8D8D8D8D8CDCDCDABABAB95959595
              95959595959595959E9E9EB6B6B6D8D8D8ACAEAE67E4FC66E6FF68E6FF10A6C3
              43C79850D09250D09250D09250D09250D09250D09250D09250D09287DFB4DEF6
              EAFFFFFF0000EFFBF56CD8A350D09250D09250D09250D09250D09250D09250D0
              9250D09250D09250D09214A8AF03C1ED0ACFFF0FD1FF67BED3C3C3C3CFCFCFA9
              A9A9ACACACACACACACACACACACACADADADD6D6D6D8D8D8D8D8D8D8D8D8ACACAC
              ACACACACACACACACACACACACACACACAFAFAFD8D8D8ADB3B474E0F56CE7FF6DE8
              FF1CAECA3CC39B50D09250D09250D09250D09250D09250D09250D09250D09250
              D09261D59CD9F5E70000FFFFFFF4FCF8A7E7C85BD39950D09250D09250D09250
              D09250D09250D09250D09250D0921EAEAA04BBE40CD0FF12D1FF5DC1D8BEC1C1
              D6D6D6ACACAC959595959595959595959595ABABABCFCFCFD8D8D8D8D8D8D1D1
              D1ACACAC9595959595959595959595959F9F9FB9B9B9D8D8D8ACACAC71E9FF72
              E9FF73E9FF0FA4C146C99750D09250D09250D09250D09250D09250D09250D092
              61D59CB2EACFFAFEFCFFFFFF0000FFFFFFFFFFFFFFFFFFF4FCF8A7E7C85BD399
              50D09250D09250D092B2EACFFFFFFFFFFFFF78CADA05B6DD0FD1FF14D2FF4CC5
              E2B2BBBED8D8D8AFAFAF8787877F7F7F7F7F7F7F7F7F878787ACACACC2C2C2C2
              C2C2AEAEAE8787877F7F7F7F7F7F7F7F7F7F7F7FA0A0A0C8C8C8D5D5D5ABAFB0
              77EAFF78EAFF7AEBFF089DBAF7FCFDFFFFFFCEF2E050D09250D09250D09261D5
              9CB2EACFFAFEFCFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFF4FCF8A7E7C85BD39950D0928DE1B8FFFFFFFFFFFF9FD9E405AED311D1FF16
              D2FF26D2FAA8B0B2D6D6D6BCBCBC9494947F7F7F7F7F7F7F7F7F7F7F7F838383
              9595959595958686867F7F7F7F7F7F7F7F7F7F7F7F808080AAAAAAD1D1D1C8C8
              C89EC4CA7CEBFF7EECFF6FE1F620A7C1FFFFFFFFFFFFA3E7C650D09261D59CB2
              EACFFAFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFF4FCF8A7E7C861D59CF4FCF8FFFFFFE7F5F801A0BF
              14D2FF1AD3FF1FD5FF8CB9C4CACACAD5D5D5ACACAC8383837F7F7F7F7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F999999BEBEBED8
              D8D8B0B7B88ADEED82EDFF83EDFF4FCCE268C3D5FFFFFFFFFFFF71D9A7B2EACF
              FAFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4FCF8EFFBF5FFFFFFFFFF
              FF38B0C811C6EF1CD4FF21D5FF32D2F7A6B3B6D5D5D5CBCBCBA5A5A57F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F898989AFAFAF
              D6D6D6C4C6C6A0C5CC85EEFF88EEFF8AEFFF27B2CCB7E2EBFFFFFFFAFEFCFAFE
              FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFAFDFE908ACCF1ED4FF23D6FF29D7FF62C8DFB4BDC0D8D8D8BFBFBF
              A7A7A78686867F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F979797AEAE
              AED3D3D3D3D3D3ABB5B689ECFC8BEFFF8DF0FF74E0F230ADC6FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF30ADC61ACAF225D6FF2BD7FF31D9FF92BAC3BFC4
              C5D8D8D8CFCFCFAEAEAE9D9D9D9191918989898585858D8D8D959595AAAAAABD
              BDBDD6D6D6D4D4D4B1B9BA92DEEA8CEFFF90F0FF92F1FF2FB6CFAFDFE9FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFE6ED08A9CA27D7FF2DD8FF33DAFF3C
              D9FC7FC2D1AEB8BAD4D4D4D6D6D6C6C6C6BBBBBBB0B0B0AEAEAEB4B4B4C2C2C2
              D1D1D1D8D8D8C6C8C8ACB4B692DEEA8DF0FF91F1FF94F1FF72DCED40B4CAFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FD2E012B5D82FD9FF
              34DAFF3BDBFF40DDFF5FD1EAA7B4B6C0C2C2D1D1D1D8D8D8D8D8D8D8D8D8D8D8
              D8D8D8D8CACACAB4BABCA2C4CA8AEAFA8DF0FF91F1FF96F2FF87E8F630ADC6EF
              F9FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF68C3
              D519BBDD35DAFF3CDCFF41DDFF48DFFF53DDFA81C6D5A6B4B7ACAEAEB0B8BAB0
              B8BAADB4B5ACACACA2BFC48CD9E785EEFF8BEFFF90F0FF96F2FF87E8F623AEC8
              D7EFF4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFF70C7D813B1D13BDAFD43DDFF49DFFF4FE0FF54E2FF5BE3FF62E3FC
              77D7EA7CD8EA7BDFF278EAFF7DECFF83EDFF89EFFF8EF0FF93F1FF73DCED1EAB
              C6CFECF2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFA7DCE608A3C12AC4E44ADFFF50E1FF55E2FF5CE4
              FF62E5FF68E6FF6EE8FF73E9FF7AEBFF7FECFF85EEFF8BEFFF7EE5F634B9D140
              B4CAEFF9FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7F5F860C0D30CA5C32BBFDD46
              D4F25DE4FF63E5FF69E7FF6FE8FF74EAFF7BEBFF79E8FB58D1E635BAD328AAC3
              A7DCE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7EFF4
              78CADA30ADC6039CBA19ADCA1BAECA1CAECA1DAFCA13A7C3089FBD58BDD19FD9
              E4F7FCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFF7FCFDBFE6EDBFE6EDBFE6EDBFE6EDD7EFF4FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF0000}
            Layout = blGlyphTop
            ParentFont = False
            Spacing = 0
            TabOrder = 1
            WordWrap = True
            OnClick = SearchButtonClick
          end
          object Panel10: TPanel
            Left = 489
            Top = 0
            Width = 81
            Height = 188
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
          end
          object Button5: TBitBtn
            AlignWithMargins = True
            Left = 576
            Top = 6
            Width = 234
            Height = 176
            Margins.Left = 6
            Margins.Top = 6
            Margins.Right = 6
            Margins.Bottom = 6
            Align = alClient
            Anchors = []
            Caption = #1056#1091#1095#1085#1086#1081
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -38
            Font.Name = 'Myriad Pro'
            Font.Style = []
            Glyph.Data = {
              6E370000424D6E3700000000000036000000280000004B0000003E0000000100
              1800000000003837000000000000000000000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF171616B6B6B6FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF353432B7A89B34312FCFCFCFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7464443A99B8FE2CFBFD4C2B334
              3230D7D7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E5D5B8E837AE2CFBFE2CFBF
              E2CFBFC6B5A73C3A38DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8584846C645EE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFB0A2953E3C3BF7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A6504B46E2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF80766D6E6D6CFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B7B73B3835DBC9B9
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF494440AEAEAEFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2E2DA295
              89E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFCDBCAD2D
              2B2AE7E7E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7
              E7E7353331C6B5A7E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBF867C72666665FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF100F0F
              F7F7F7FFFFFFCFCFCF34312FDBC9B9E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBF2C2A28CFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF3B38
              368E82784E4D4CFFFFFFFFFFFFA6A6A6504B46E2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFB0A295363534FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF3B
              3836D4C2B3E2CFBF6C645E858484FFFFFFFFFFFF767574797068E2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF494440AFAFAFFFFFFFFFFF
              FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF
              3B3836D4C2B3E2CFBFE2CFBFE2CFBF494440AEAEAEFFFFFFFFFFFF464544A295
              89E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFA99B8F3E3D3CFF
              FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBF
              BF3B3836D4C2B3E2CFBFE2CFBFE2CFBFE2CFBFD4C2B3343230D7D7D7FFFFFFCF
              CFCF2C2A28E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
              2C2A28CFCFCFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7
              F7F7262524BFAFA1E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFB7A89B2E2D2C
              FFFFFFFFFFFF4E4D4C9B8E83E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBF9B8E834E4D4CFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFBFBFBF423E3BDBC9B9E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BF494440AFAFAFFFFFFFCFCFCF2C2A28E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBFE2CFBFDBC9B91E1D1BE7E7E7FFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFBFBFBFFFFFFFFFFFFFFFFFFF9695955E5852E2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBFC6B5A7262524F7F7F7FFFFFF6F6E6D7F756CE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF5D5650979797FFFFFFFFFFFF000000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFCFCFCF1E1D1B767574FFFFFFFFFFFFFFFFFF666564877C73E2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBFE2CFBF655D578E8E8EFFFFFFE7E7E71E1D1BDBC9B9E2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFA99B8F3E3D3CFFFFFFFFFFFF00
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFCFCFCF3B3836D4C2B3504B46A6A6A6FFFFFFFFFFFFF7F7F71E1D1CCDBC
              ADE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFCDBCAD1E1D1CF7F7F7FFFFFF5E5E5D8D
              8177E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF1E1C1BDFDFDF
              FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFCFCFCF3B3836D4C2B3E2CFBFDBC9B93B3835C7C7C7FFFFFFFFFFFF8E
              8E8E655D57E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF655D578E8E8EFFFFFF
              AFAFAF494440E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF5750
              4B9F9F9FFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFCFCFCF3B3836D4C2B3E2CFBFE2CFBFE2CFBFB7A89B2E2D2CFFFFFF
              FFFFFFF7F7F7262524C6B5A7E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFA99B8F3E3D
              3CFFFFFFEFEFEF171615DBC9B9E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBF8D81775E5E5DFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCFCFCF3B3836D4C2B3E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF5E57
              51969696FFFFFFFFFFFFAFAFAF494440E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBF0F0E0EEFEFEFFFFFFF3E3D3CA99B8FE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFC6B5A71F1E1DFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFBFBFBF1010101010107F7F7FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFF7F7F70F0F0ED4C2B3E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFCDBCAD1E1D1CF7F7F7FFFFFFFFFFFF0F0F0ED4C2B3E2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBF57504B9F9F9FFFFFFF8F8F8F645C56E2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBF1E1C1BDFDFDF000000FFFFFFFFFFFFFFFFFFFF
              FFFF979797404040B7B7B7EFEFEF7F7F7F383838DFDFDFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF8584846C645EE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBF5D5650979797FFFFFFFFFFFF5E5E5D8D8177E2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBF94887D565555FFFFFFDFDFDF1E1C1BE2CFBFE2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF494440AFAFAF000000FFFFFFFFFFFF
              FFFFFF9F9F9F585858FFFFFFFFFFFFFFFFFFFFFFFFD7D7D7303030CFCFCFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFF0C0F1000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0C0F10FFFFFFFFFFFFAFAFAF171717F7F7F7FFFFFF464544A29589E2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBFA99B8F3E3D3CFFFFFFFFFFFFAFAFAF494440E2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFD4C2B3171615F7F7F7FFFFFF171616CDBCAD
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF645C568F8F8F000000FFFF
              FFFFFFFFE7E7E7282828EFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF38
              3838D7D7D7FFFFFF0E0E0E00000000000002020200C0FF00C0FF00C0FF00C0FF
              00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0
              FF00C0FF000000FFFFFF9E9D9D423E3B655D577E7D7CFFFFFFCFCFCF2C2A28E2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFDBC9B90F0E0EF7F7F7FFFFFFF7F7F7
              0F0E0EDBC9B9E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF252321D7D7D7FFFFFF3E3D
              3CA99B8FE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF7F746B6F6F6E00
              0000FFFFFFFFFFFF7070708F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFB7B7B7484848FFFFFF0000007F7F7F7F7F7F00000000C0FF00C0FF00C0
              FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00
              C0FF00C0FF00C0FF00000076757457514CDBC9B9DBC9B934312FCFCFCFFFFFFF
              4E4D4C9B8E83E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF494440AFAFAFFFFF
              FFFFFFFF363534B0A295E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF3B3734BFBFBFFF
              FFFF56555594887DE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFA29589
              464544000000FFFFFFFFFFFF080808EFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF505050AFAFAF0000007F7F7F7F7F7F00000000C0FF00
              C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF
              00C0FF00C0FF00C0FF00C0FF000000797068E2CFBFE2CFBFE2CFBFA99B8F3E3D
              3CFFFFFF9F9F9F57504BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF786E6578
              7777FFFFFFFFFFFF5E5E5D8D8177E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF57504B
              9F9F9FFFFFFF676666867B71E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BFA99B8F3E3D3C000000FFFFFFCFCFCF303030FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E72020200000007F7F7F7F7F7F000000
              00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0
              FF00C0FF00C0FF00C0FF00C0FF00C0FF000000E2CFBFE2CFBFE2CFBFE2CFBFDB
              C9B9080707FFFFFFE7E7E7161514E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
              8D81775E5E5DFFFFFFFFFFFF787777786E65E2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BF6B625B878787FFFFFF7F7F7F71675FE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBFBFAFA1262525000000FFFFFF8F8F8F707070FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7070700000007F7F7F7F7F
              7F00000000C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00
              C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF000000E2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBF2C2A28CFCFCFFFFFFF080707DBC9B9E2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBFA99B8F3E3D3CFFFFFFFFFFFF8F8F8F645C56E2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBF7F746B6F6F6EFFFFFF9F9F9F57504BE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBFC6B5A71F1E1D000000FFFFFF505050AFAFAFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E70000007F
              7F7F7F7F7F00000000C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF
              00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF000000E2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBF504A45A7A7A7FFFFFF1F1E1DC6B5A7E2CFBFE2CFBFE2
              CFBFE2CFBFE2CFBFBFAFA1262525FFFFFFFFFFFF9F9F9F57504BE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBF8D81775E5E5DFFFFFFB7B7B7423E3AE2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBFC6B5A71F1E1D000000FFFFFF404040BFBFBFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              0000007F7F7F7F7F7F00000000C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0
              FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF000000E2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBF494440AFAFAFFFFFFF1F1E1DC6B5A7E2CFBF
              E2CFBFE2CFBFE2CFBFE2CFBFB7A89B2E2D2CFFFFFFFFFFFF9F9F9F57504BE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBF8D81775E5E5DFFFFFFAFAFAF494440E2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFC6B5A71F1E1D000000FFFFFF303030
              CFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF0000007F7F7F7F7F7F00000000C0FF00C0FF00C0FF00C0FF00C0FF00
              C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF
              000000E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF161514E7E7E7FFFFFF000000E2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFA29589464544FFFFFFFFFFFF7F7F7F71
              675FE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF71675F7F7F7FFFFFFF9797975D5650
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFC6B5A71F1E1D000000FFFF
              FF202020DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF0000007F7F7F7F7F7F00000000C0FF00C0FF00C0FF00C0FF
              00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0
              FF00C0FF000000B7A89BE2CFBFE2CFBFE2CFBFCDBCAD171616FFFFFFCFCFCF2C
              2A28E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF8D81775E5E5DFFFFFFFFFFFF
              6F6F6E7F746BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF645C568F8F8FFFFFFF7F7F
              7F71675FE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFB7A89B2E2D2C00
              0000F7F7F7080808F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0000007F7F7F7F7F7F00000000C0FF00C0FF00C0
              FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00
              C0FF00C0FF00C0FF0000003D3B3AB7A89BE2CFBFE2CFBF867C72666665FFFFFF
              8686866B635CE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF6B625B878787FFFF
              FFFFFFFF56555594887DE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF504A45A7A7A7FF
              FFFF5E5E5D8D8177E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFA99B8F
              3E3D3C000000DFDFDF202020FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000007F7F7F7F7F7F00000000C0FF00
              C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF
              00C0FF00C0FF00C0FF00C0FF000000EFEFEF3D3B3AB7A89BCDBCAD262423EFEF
              EFFFFFFF2E2D2CB7A89BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF3B3734BF
              BFBFFFFFFFFFFFFF2E2D2CB7A89BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF3B3734
              BFBFBFFFFFFF464544A29589E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BF9B8E834E4D4C000000C7C7C7383838FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2A2A2A000000000000060606
              00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0
              FF00C0FF00C0FF00C0FF00C0FF00C0FF000000FFFFFFEFEFEF3D3B3A33302DB7
              B7B7FFFFFFAFAFAF494440E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFD4C2B3
              0F0F0EFFFFFFFFFFFFDFDFDF1E1C1BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BF161514E7E7E7FFFFFF2E2D2CB7A89BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBF786E65787777000000BFBFBF404040FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FF242D3000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000242D30FFFFFFFFFFFFDFDFDF
              585757FFFFFFEFEFEF2E2C2BC6B5A7E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BF8D81775E5E5DFFFFFFFFFFFF9797975D5650E2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFC6B5A71F1E1DFFFFFFFFFFFF0F0F0ED4C2B3E2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBF5D5650979797000000BFBFBF404040FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF464543A3968BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBF494440AFAFAFFFFFFFFFFFFF464544A29589E2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBF7F746B6F6F6EFFFFFFBFBFBF3B3734E2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBF3B3734BFBFBF000000CFCFCF303030FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFDFDFDF2E2D2BB7A89BE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFA29589464544FFFFFFFFFFFFE7E7E71E1D1BDBC9B9E2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBF3B3734BFBFBFFFFFFF777776786F66E2CFBFE2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF0F0E0EEFEFEF000000FFFFFF282828
              D7D7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF3D3B3AB7A89BE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBF2C2A28CFCFCFFFFFFFFFFFFF6F6E6D7F756CE2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFD4C2B3171615F7F7F7FFFFFF2E2D2CB7A89BE2CFBF
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFB7A89B2E2D2CFFFFFF000000FFFF
              FFD7D7D7303030F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF3D3B3AB7
              A89BE2CFBFE2CFBFE2CFBF95897E565554FFFFFFFFFFFFE7E7E71E1D1BDBC9B9
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF94887D565555FFFFFFDFDFDF1E1C1BE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF7F746B6F6F6EFFFFFF00
              0000FFFFFFFFFFFFB7B7B7383838EFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              EFEFEF3D3B3AB7A89BE2CFBFBFAFA1353432EFEFEFFFFFFFFFFFFF6F6E6D7F75
              6CE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF494440AFAFAFFFFFFF9797975D
              5650E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF494440AFAFAF
              FFFFFF000000FFFFFFFFFFFFFFFFFFCFCFCF303030DFDFDFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFEFEFEF3D3B3AA99B8F343230D7D7D7FFFFFFFFFFFFDFDFDF25
              2422DBC9B9E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFB7A89B2E2D2CFFFFFFFFFFFF
              464544A29589E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFDBC9B90F0E
              0EF7F7F7FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFDFDFDF303030B7B7B7FF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF1F1E1EB6B6B6FFFFFFFFFFFFF7F7F7
              363433B7A89BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF33312EC7C7C7FFFF
              FFBFBFBF3B3734E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF94
              887D565555FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF
              4040408F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7FFFFFFFFFFFFFFFF
              FF56555495897EE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF9B8E834E4D4CFF
              FFFFFFFFFF2E2D2CB7A89BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBF423E3AB7B7B7FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFF7F7F7404040C7C7C7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFF8584846C645EE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF2C2A28
              CFCFCFFFFFFFAFAFAF494440E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBFCDBCAD1E1D1CF7F7F7FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFCFCFCF303030FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFEFEFEF0F0F0EB7A89BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF9589
              7E565554FFFFFFFFFFFF2E2D2CB7A89BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBFE2CFBF7F756C6F6E6DFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF282828D7D7D7FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFEFEFEF3D3B3AB7A89BE2CFBFE2CFBFE2CFBFE2CFBFB7
              A89B363433F7F7F7FFFFFFAFAFAF494440E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBFDBC9B91E1D1BE7E7E7FFFFFFFFFFFFFFFFFF000000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF878787787878FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF3D3B3AB7A89BE2CFBFE2CFBF
              D4C2B32D2B29DFDFDFFFFFFFEFEFEF2E2C2BC6B5A7E2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBF7F756C6F6E6DFFFFFFFFFFFFFFFFFFFFFFFF00
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9F606060
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF3D3B3AB7A8
              9BDBC9B9423E3BBFBFBFFFFFFFF7F7F7464443A99B8FE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF2C2A28CFCFCFFFFFFFFFFFFFFFFFFF
              FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAF
              AF505050FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEF
              EFEF3D3B3A48433F969595FFFFFFFFFFFF666564877C73E2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF7F756C6F6E6DFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFAFAFAF505050FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFDFDFDF585757FFFFFFFFFFFF8E8D8D655E57E2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFCDBCAD2D2B2AE7E7E7FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF7F7F7F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF423E3BDBC9B9E2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF655D578E8E8EFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF404040BFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F1E1EA99B8FE2CFBF
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFB0A295363534FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF686868F7F7F7FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E73533
              31C6B5A7E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFD4C2B32D2B29DF
              DFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFCFCFCF3B3836D4C2B3E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF494440
              AEAEAEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFB6B6B6423E3AE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF8076
              6D6E6D6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9695955E5852E2CFBFE2CFBFE2CFBF9C
              8F844E4D4CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6D6C80766DE2CFBF
              A99B8F464443F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4E4D
              4C786F663D3B3AEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFF7F7F75F5E5EEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
            Layout = blGlyphTop
            ParentFont = False
            Spacing = 0
            TabOrder = 2
            WordWrap = True
            OnClick = Button5Click
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 224
        Width = 820
        Height = 449
        Align = alClient
        Caption = #1055#1086#1076#1075#1086#1090#1086#1074#1082#1072'/'#1087#1088#1086#1074#1077#1088#1082#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object GridPanel9: TGridPanel
          Left = 2
          Top = 34
          Width = 816
          Height = 413
          Align = alClient
          BevelOuter = bvNone
          ColumnCollection = <
            item
              Value = 60.000000000000000000
            end
            item
              Value = 10.000000000000000000
            end
            item
              Value = 30.000000000000000000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = GridPanel10
              Row = 0
            end
            item
              Column = 1
              Control = Panel11
              Row = 0
            end
            item
              Column = 2
              Control = Button6
              Row = 0
            end>
          RowCollection = <
            item
              Value = 100.000000000000000000
            end>
          TabOrder = 0
          object GridPanel10: TGridPanel
            Left = 0
            Top = 0
            Width = 489
            Height = 413
            Align = alClient
            BevelOuter = bvNone
            ColumnCollection = <
              item
                Value = 50.000000000000000000
              end
              item
                Value = 50.000000000000000000
              end>
            ControlCollection = <
              item
                Column = 0
                Control = TestButton
                Row = 0
              end
              item
                Column = 1
                Control = AlignButton
                Row = 0
              end
              item
                Column = 0
                Control = CalibButton
                Row = 1
              end
              item
                Column = 1
                Control = Button8
                Row = 1
              end>
            RowCollection = <
              item
                Value = 50.000000000000000000
              end
              item
                Value = 50.000000000000000000
              end>
            TabOrder = 0
            object TestButton: TBitBtn
              AlignWithMargins = True
              Left = 6
              Top = 6
              Width = 232
              Height = 194
              Margins.Left = 6
              Margins.Top = 6
              Margins.Right = 6
              Margins.Bottom = 6
              Align = alClient
              Anchors = []
              Caption = #1058#1077#1089#1090
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -38
              Font.Name = 'Myriad Pro'
              Font.Style = []
              Glyph.Data = {
                C62B0000424DC62B000000000000360000002800000036000000440000000100
                180000000000902B000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F9F67BA97864A26367A56667A466
                67A46667A46667A46667A46667A46667A46667A46667A46667A46667A46667A4
                6667A46663A062A0C09DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                87878780808080808080808080808080808080808073777207800C08BC1808B2
                1508B41508B41508B41508B41508B41508B41508B41508B41508B41508B41508
                B41508B41508B31608BB17257024808080808080808080808080808080808080
                808080878787FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBC6C9C51090170E
                94165F8E5D689B66669964679A6572A1716F9E6D679A65669964669964669964
                6699646699646A9B6842843F08B116459245DBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF868686808080808080808080808080808080808080747773
                0B8B120C8A137877778080808080808080805B6F596E726D8080808080808080
                808080808080808080808080804D6E4A08B1152B782B80808080808080808080
                8080808080808080808080858585FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFA2A2A29494949E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E8F928E0C8C130D8D148B8E8A9E9E9E9E9E9E898F88077B0A1085146881659E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E597F5608B1153380339E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E808080FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7C7C79090909E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E8F928E0C8C130D8D148B8E8A9E9E9E9E9E9E56785308AC1508CE1D
                1079119192909E9E9E9E9E9E9E9E9E9E9E9E9E9E9E597F5608B1153380339E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E808080FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E78C8C8C9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E8F928E0C8C130D8D148B8E8A9E9E9E9999991F771F08C2
                1A08C81C08A7135C7A599E9E9E9E9E9E9E9E9E9E9E9E9E9E9E597F5608B11533
                80339E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E808080FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8383838080
                808080808080808080808080807477730B8B120C8C1371747080808056695408
                9C1108BB19078F0E08C51B167517787877808080808080808080808080486E45
                08B1152B782B808080808080808080808080808080808080808080838383FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
                8080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBC6C9C50E8E150F8F16C7C7C6C8C9C7
                287F2808C91C0A97143F6E3A08AA1508B2166B9167DBDBDBDBDBDBDBDBDBDBDB
                DB7AA07708B115459245DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB80
                8080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBC6C9C50E8E1516931DA8B1
                A623762208BF1908C31A3B833AC7C6C632813108C81B0F8813B1B9B0DBDBDBDB
                DBDBDBDBDB7AA07708B115459245DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBC6C9C50E8E1513
                911AB9BDB8629860158E1A2A832AB5BFB4DBDBDBA9B4A70C871108C61B357F33
                D4D4D4DBDBDBDBDBDB7AA07708B115459245DBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBC6C9C5
                0E8E150E8E15C5C7C4DBDBDBC1C3C0CECECEDBDBDBDBDBDBDBDBDB7B997808A2
                1208B216638D5FDBDBDBDBDBDB7AA07708B115459245DBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBC6C9C50E8E150F8F16C1C4C0DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDB59895608B416089B118AA188DBDBDB7AA07708B115459245DBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBC6C9C50E8E150F8F16C1C4C0DBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBD4D4D442834008BA180F8E14ADB6AB88A88508B115459245DBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBC6C9C50E8E150F8F16C7C8C6DBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBD5D5D546874408B717238E275A835608AC1545
                9245DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBC6C9C50E8E15108B16AFB1AEC6C7C5C4C5C3C4
                C5C3C4C5C3C4C5C3C4C5C3C4C5C3C4C5C3C9C9C8BBBBBA3B7B3708A2130C7C0F
                08A714479247DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
                8080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBC5C8C40E8E1508AC16168B1C16901C
                158F1B158F1B158F1B158F1B158F1B158F1B158F1B158F1B1B93211C8E22078E
                0F08C51B08C51A41883FDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB80
                8080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBCBCECA1D7C1E0796100795
                1007951007951007951007951007951007951007951007951007951007951007
                95100992110A8E1107991107920F60945FD8D8D8DBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBBBC4BAAC
                B7ABAEB8ADAEB8ADAEB8ADAEB8ADAEB8ADAEB8ADAEB8ADAEB8ADAEB8ADAEB8AD
                AEB8ADAEB8ADAEB8ADB2BAB197A9952185232A7D29D7D7D7DBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBC9CBC8C3CAC2DBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
                8080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB80
                8080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFF3F3F385858580808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                80808080808080858585FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFB4B4B49292929E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF8383838080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                808080808080808080808080808080838383FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF85858580
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808085
                8585FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                808080ACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACAC808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF808080ACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACAC808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF808080ACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACAC808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF9494948080808080808080808080808080808080
                808080808080808080808080808080808080808080806C848960878E54899344
                8C9A60878E60878E7C8182808080808080808080808080808080808080808080
                808080808080808080808080808080949494FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7E9EF68C3D520A7C100A2C300A7CA
                00ACD101B3DA02A8CA02A8CA089DBA48B7CC97D6E2EFF9FBFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFECF248B7CC00A5C800B8E200C6F600CD
                FF02CEFF05CEFF08CFFF0BD0FF0DD0FF10D1FF0DC2EB09B1D528AAC38FD2E0F7
                FCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FD2E000A4C600BEEB00CDFF00CDFF00
                CDFF03CEFF07CFFF0ACFFF0DD0FF10D1FF12D2FF16D2FF18D3FF1AD3FF19CEF8
                0DB4D838B0C8DFF2F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FCFD68C3D500B1D800CDFF00CDFF00CDFF
                01CDFF06CEFF09CFFF2FC8ED36C8EA38C8EA3BC8EA21D1FA1BD4FF1DD4FF1FD5
                FF21D5FF24D6FF1FCBF230ADC6BFE6EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF58BDD100B8E200CDFF00CDFF00CD
                FF02CEFF11CCFA6EBCCFA5B0B2A9B5B8ABB7BAABB7BAABB7BAA9AEB095B7BE52
                C6E226D6FF28D7FF2AD7FF2BD8FF29D4FB0AA7C8C7E9EFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF87CFDD00B2DA00CDFF00CDFF00
                CDFF02CEFF35C5E7A2B2B6C0C4C5D4D4D4D8D8D8D8D8D8D8D8D8D8D8D8D8D8D8
                CDCDCDB2BCBE93B9C139D4F72FD9FF32D9FF33D9FF2FD3F838B0C8DFF2F6FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7E2EB00A9CC00CDFF00CDFF
                00CDFF0ECCFA7BBBCAACB9BCD4D4D4D4D4D4C2C2C2B2B2B2ACACACACACACB0B0
                B0BBBBBBC8C8C8D8D8D8CBCBCBA7B3B661CAE137DAFF38DBFF3ADBFF2ECEEF38
                B0C8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF30ADC600C3F200CD
                FF00CDFF03CEFF8BB7C1C2C6C6D8D8D8C6C6C6ACACAC9595958A8A8A8282827F
                7F7F898989919191A2A2A2B4B4B4D4D4D4D5D5D5AFB7B95BCEE73EDCFF40DDFF
                41DDFF15AFCFAFDFE9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFE900AACF00
                CDFF00CDFF02CEFF4AC3DFB1BDC0D8D8D8B7B7B7A1A1A18282827F7F7F7F7F7F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F8B8B8BACACACCFCFCFD4D4D4A6B3B64BDB
                FA46DEFF47DEFF41D8F820A7C1F7FCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4FCF8FFFFFFFFFFFF48B7CC
                00BEEB00CDFF01CDFF16CCF7A4B2B6D5D5D5C6C6C6A1A1A17F7F7F7F7F7F7F7F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F858585ADADADD6D6D6C5
                C8C890C0CA4BDFFF4DE0FF4DE0FF14ACCABFE6EDFFFFFFEFFBF5EFFBF5FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8F0DD71D9A7D9F5E7FFFFFFF7FC
                FD009DBD00CCFD00CDFF06CEFF85B8C4CACACAD3D3D3ABABAB8080807F7F7F7F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F939393
                BABABAD8D8D8ADB6B866D6ED53E1FF54E2FF30C2E070C7D8FFFFFFFAFEFC61D5
                9C9DE5C2EFFBF5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8F0DD71D9A750D0926CD8A3FFFFFFFF
                FFFFB7E2EB00A9CC00CDFF05CEFF13CDFAA7B0B2D6D6D6B7B7B78E8E8E7F7F7F
                7F7F7F7F7F7F7F7F7F7F7F7F8A8A8A8A8A8A7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                7F7F7F7FABABABD3D3D3C6C6C68EC2CC59E3FF5AE3FF4DD7F428AAC3FFFFFFFF
                FFFF98E4BF50D09256D2959DE5C2EFFBF5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFC8F0DD71D9A750D09250D09250D0929DE5C2
                FFFFFFFFFFFF97D6E200AFD502CEFF08CFFF44C3E2B1BBBED8D8D8AEAEAE8585
                857F7F7F7F7F7F7F7F7F828282A5A5A5B2B2B2B2B2B2A7A7A78282827F7F7F7F
                7F7F7F7F7F7F7F7FA2A2A2C8C8C8D1D1D1A6B4B75FE4FF60E4FF5EE3FD089DBA
                FFFFFFFFFFFFBDEDD650D09250D09250D09256D2959DE5C2EFFBF5FFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFC8F0DD71D9A750D09250D09250D09250D09250D0
                9269D6A27CDCAE7CDCAE3ABAB400B5DD06CEFF0BD0FF4FC2DDB7BEC0D5D5D5AB
                ABAB959595959595959595959595A6A6A6C6C6C6D8D8D8D8D8D8CACACAAAAAAA
                9595959595959595959595959F9F9FB9B9B9D8D8D8ACACAC65E6FF66E6FF67E6
                FF0AA2BF48CA9550D09250D09250D09250D09250D09250D09250D09256D2959D
                E5C2EFFBF5FFFFFF0000EFFBF57CDCAE50D09250D09250D09250D09250D09250
                D09250D09250D09250D09250D0921EAEAA02BAE409CFFF0ED0FF60BFD5C2C2C2
                CFCFCFA9A9A9ACACACACACACACACACACACACADADADD6D6D6D8D8D8D8D8D8D8D8
                D8ACACACACACACACACACACACACACACACACACACB0B0B0D8D8D8ACACAC6AE7FF6D
                E8FF6DE8FF0EA4C146C99750D09250D09250D09250D09250D09250D09250D092
                50D09250D09266D6A0E9F9F10000FFFFFFF4FCF8A7E7C85BD39950D09250D092
                50D09250D09250D09250D09150D09250D09228B5A503B4DB0CD0FF11D1FF54C3
                DDB7BEC0D5D5D5ABABAB959595959595959595959595ABABABCFCFCFD8D8D8D8
                D8D8D3D3D3ADADAD959595959595959595959595A0A0A0BABABAD6D6D6ACAEAE
                71E9FF72E9FF73E9FF079FBD4BCD9550D09250D09250D09250D09250D09250D0
                9250D09261D59CB2EACFFAFEFCFFFFFF0000FFFFFFFFFFFFFFFFFFF4FCF8A7E7
                C85BD39950D09250D09250D092A3E7C6FFFFFFFFFFFF97D6E204AFD50ED0FF14
                D2FF47C7E5AFBABCD8D8D8AEAEAE8686867F7F7F7F7F7F7F7F7F898989AEAEAE
                C4C4C4C6C6C6AFAFAF8A8A8A7F7F7F7F7F7F7F7F7F7F7F7FA3A3A3CACACAD1D1
                D1A9B5B776EAFF78EAFF75E9FD089DBAFFFFFFFFFFFFBDEDD650D09250D09250
                D09261D59CB2EACFFAFEFCFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFF4FCF8A7E7C85BD39950D0927CDCAEFFFFFFFFFFFFB7E2EB03AACC
                11D1FF17D3FF20D2FCA9AEB0D6D6D6BABABA9393937F7F7F7F7F7F7F7F7F7F7F
                7F8686869B9B9B9D9D9D8787877F7F7F7F7F7F7F7F7F7F7F7F808080ABABABD3
                D3D3C2C3C497C9D27DECFF7EECFF67DDF230ADC6FFFFFFFFFFFF8DE1B850D092
                61D59CB2EACFFAFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4FCF8A7E7C85BD399E9F9F1FFFFFFF7FC
                FD01A0BF13CFFB19D3FF1ED4FF84BDCAC4C8C8D4D4D4ACACAC8383837F7F7F7F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F999999
                BEBEBED8D8D8AFB6B788E0EF82EDFF84EDFF46C6DD78CADAFFFFFFFAFEFC66D6
                A0B2EACFFAFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4FCF8E4F8EEFF
                FFFFFFFFFF58BDD10EBFE61BD4FF21D5FF33D3F7A4B5B8D4D4D4CBCBCBA5A5A5
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F8989
                89AFAFAFD6D6D6C1C4C59CCAD185EEFF87EEFF8AEFFF23B0CABFE6EDFFFFFFF4
                FCF8FAFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFAFDFE908ACCF1DD4FF24D6FF29D7FF5DC8E1B1BCBED8D8
                D8BFBFBFA7A7A78686867F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F97
                9797AEAEAED3D3D3D1D1D1ABB7B988ECFC8BEFFF8DEFFF74E0F230ADC6FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF50BACF15C2E926D6FF2BD8FF30D9FF8E
                BCC7C1C4C5D8D8D8CFCFCFAEAEAE9D9D9D9191918989898585858D8D8D959595
                AAAAAABDBDBDD6D6D6D4D4D4B0B7B890E0ED8DEFFF90F0FF93F1FF2FB6CFAFDF
                E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFECF207A7C826D5FD2DD8FF
                33D9FF3BD9FC8CBECAB0BABCD4D4D4D6D6D6C6C6C6BBBBBBB0B0B0AEAEAEB4B4
                B4C2C2C2D1D1D1D8D8D8C6C8C8ACB4B691DDEA8DEFFF91F1FF95F2FF73DCED40
                B4CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7DCE60EAF
                D12ED8FF35DAFF3ADBFF41DDFF62CFE7A7B3B6C0C2C2D1D1D1D8D8D8D8D8D8D8
                D8D8D8D8D8D6D6D6C8C8C8B4BABCA2C4CA8AEAFA8DEFFF91F1FF95F2FF87E8F6
                30ADC6EFF9FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFF70C7D817B8DA36DAFF3CDCFF42DDFF47DEFF53DDFA81C6D5A6B4B7ACAEAE
                B0B8BAB0B8BAADB4B5ACAEAEA1C1C78DDAE786EEFF8CEFFF91F1FF95F2FF87E8
                F623AEC8D7EFF4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF7FCCDB13B0D13BDAFD43DDFF49DFFF4FE0FF55E2FF5AE3
                FF63E3FC77D7EA7DD9EA7BDFF278EAFF7EECFF83EDFF88EEFF8FF0FF94F1FF77
                DFEF1EABC6CFECF2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7DCE60AA5C32DC6E64ADFFF50E1FF56
                E2FF5CE4FF62E5FF68E7FF6EE8FF74E9FF79EBFF80ECFF85EEFF8CEFFF83E8F8
                38BBD338B0C8EFF9FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF2F658BDD10CA5C3
                2BBFDD47D4F25EE4FF63E5FF69E7FF6FE8FF75EAFF7CEBFF79E8FB58D1E635BA
                D312A5C19FD9E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFD7EFF478CADA30ADC6009AB819AECA1AAECA1CAECA1EAFCA13A7C3089FBD58
                BDD19FD9E4F7FCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFE6EDBFE6EDBFE6EDBFE6EDD7EFF4
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0000}
              Layout = blGlyphTop
              ParentFont = False
              Spacing = 0
              TabOrder = 0
              WordWrap = True
              OnClick = TestButtonClick
            end
            object AlignButton: TBitBtn
              AlignWithMargins = True
              Left = 250
              Top = 6
              Width = 233
              Height = 194
              Margins.Left = 6
              Margins.Top = 6
              Margins.Right = 6
              Margins.Bottom = 6
              Align = alClient
              Anchors = []
              Caption = #1070#1089#1090#1080#1088#1086#1074#1082#1072
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -38
              Font.Name = 'Myriad Pro'
              Font.Style = []
              Glyph.Data = {
                222F0000424D222F0000000000003600000028000000340000004D0000000100
                180000000000EC2E000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8787878080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                808080808080808080808080808080808080808080808080878787FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF8686868080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                808080808080808080808080858585FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFA2A2A29494949E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFC7C7C79090909E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E7
                8C8C8C9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83838380
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                80808080808080808080808080808080808080808080808080808080838383FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBB4C7DEDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD4D8DB217BE9
                B4C7DEDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFF3F3F38585858080808080808080808080808080808080
                80808080808080808080808080808080808080808080587BA40D70EA2674D380
                8080808080808080808080808080808080808080808080808080808080808080
                808080808080808080808080858585FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFB4B4B49292929E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E2979DC0D70EA0D70EA708FB59E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF83838380808080808080808080808080808080808080808080808080808080
                80808080808080808080806A7D940D70EA0D70EA0D70EA1872E07C8083808080
                8080808080808080808080808080808080808080808080808080808080808080
                80808080838383FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDB5596E60D70EA0D70EA0D70EA0D70EA7AA9E2DBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                C7D1DC1474EA2D81E70D70EA619CE42D81E71474EAC7D1DCDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8585858080808080808080
                80808080808080808080808080808080808080808080808080808080757F8A75
                7F8A8080800D70EA808080808080797F87757F8A808080808080808080808080
                808080808080808080808080808080808080808080808080858585FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080ACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                AC0D70EAACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACAC808080FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF808080ACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC0D70EA
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACAC808080FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF808080ACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACAC0D70EAACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACAC808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFF808080ACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACAC81A8AF7BA6AF0685D156A3B266A5B181A8
                AF96A9ADACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACAC808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF94949480808080808080808080808080808080808080808080808080808080
                808060878E3090A30898B500A7CA00A9CC00B3DB01B4DB02AFD502A8CA1097B1
                1895AE448C9A7882848080808080808080808080808080808080808080808080
                80808080949494FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF2F650BACF00A7
                CA00BAE400CAFB00CDFF02CEFF06CEFF09CFFF0BD0FF0FD1FF11D1FF0FC7F20A
                B5DA28AAC397D6E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7E2EB00A4C600BDE900CDFF00CDFF
                00CDFF05CEFF08CFFF0BD0FF0FD1FF11D1FF14D2FF16D2FF19D3FF1BD4FF1ACE
                F80DB2D550BACFEFF9FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF8FD2E000A9CC00CAFB00CDFF00CDFF01CDFF06CEFF1E
                CBF553C0DA5EBED560BFD562BFD548C7E520D2FC1FD5FF22D5FF24D6FF25D6FF
                1BC4E938B0C8DFF2F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFA7DCE600ACD100CDFF00CDFF00CDFF02CEFF2BC8ED9AB4BAA9B2B4BBC0
                C1C2C2C2C2C2C2C2C2C2AFBABCA9AEB074C0D12BD5FC29D7FF2CD8FF2DD8FF28
                CFF430ADC6EFF9FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFE6ED00
                A9CC00CCFD00CDFF00CDFF08CDFC67BDD1A9B5B8CFCFCFD8D8D8D8D8D8CDCDCD
                CDCDCDD3D3D3D8D8D8D6D6D6BEC3C5A1B5B94BCFED31D9FF33DAFF35DAFF28CA
                ED48B7CCF7FCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FCFD20A7C100C8F800CD
                FF00CDFF13CBF79BB4B9BDC3C5D8D8D8CBCBCBB0B0B0ACACACA6A6A6A6A6A6AB
                ABABADADADBDBDBDD6D6D6D1D1D1ABB6B86ACADF39DBFF3BDCFF3CDCFF1DBADA
                87CFDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF87CFDD00B2DA00CDFF00CDFF03CEFF
                8BB7C1CBCBCBD5D5D5B8B8B8A5A5A58989897F7F7F7F7F7F7F7F7F7F7F7F8383
                83979797AEAEAECFCFCFD8D8D8ACB6B858D2ED41DDFF43DDFF42DCFD20A7C1E7
                F5F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFF7FCFD009DBD00CCFD00CDFF02CEFF4AC3DFB1BDC0D8
                D8D8B0B0B09292927F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                868686A8A8A8CDCDCDD1D1D1A3B5B94ADDFC49DFFF4ADFFF2AC0E070C7D8FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFA7DCE600ACD100CDFF01CDFF15CBF7A4B2B6D5D5D5C0C0C09B9B
                9B7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F85
                8585ADADADD6D6D6BFC4C585C4D14FE0FF50E1FF49DAF818A4BFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8F2FDF0F6FEFFFFFFFFFFFFFFFFFFFF
                FFFF58BDD100BBE600CDFF06CEFF66BDD2C0C3C4D1D1D1AAAAAA8080807F7F7F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F9393
                93BABABAD6D6D6ABB1B25BDFFA56E2FF57E2FF13AAC8C7E9EFFFFFFFFFFFFFFF
                FFFFFFFFFFCAE0FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFF7FBFE8DBCF5247EECF7FBFEFFFFFFFFFFFFFFFFFFFFFFFF20A7
                C100C6F603CEFF09CFFF9EB2B7D1D1D1C4C4C49B9B9B7F7F7F7F7F7F7F7F7F7F
                7F7F8B8B8BA6A6A6A6A6A68F8F8F7F7F7F7F7F7F7F7F7F7F7F7F828282ACACAC
                D8D8D8B5BCBE77D0E25DE4FF5EE4FF27B8D597D6E2FFFFFFFFFFFFFFFFFFFFFF
                FF3B8BEE4290EFA4C9F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB3
                D2F82B82ED0D70EA4A94F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10A0BD02CBFB
                07CFFF0CD0FFACACACD8D8D8B4B4B48D8D8D7F7F7F7F7F7F7F7F7F8B8B8BB1B1
                B1CDCDCDCDCDCDB3B3B38F8F8F7F7F7F7F7F7F7F7F7F7F7F7FA9A9A9CFCFCFC2
                C2C286C8D562E5FF63E5FF32C0DB7FCCDBFFFFFFFFFFFFFFFFFFFFFFFF77AFF4
                0D70EA0D70EA247EEC86B7F4E8F2FDFFFFFFFFFFFFD1E4FB4A94F00D70EA0D70
                EA0D70EA77AFF4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF009AB806CEFF0ACFFF1E
                CDF7A9B0B2D8D8D8ADADAD999999959595959595959595A9A9A9CFCFCFD8D8D8
                D8D8D8CFCFCFACACAC959595959595959595959595A4A4A4C8C8C8C6C6C694C3
                CC69E7FF6AE7FF43CAE4058ACB0D70EA0D70EA0D70EA0D70EA0D70EA0D70EA0D
                70EA0D70EA0D70EA247EECD9E9FCE1EDFC4290EF0D70EA0D70EA0D70EA0D70EA
                0D70EB0D70EA0D70EA0D70EA0D70EA0D70EA009AB808CFFF0ED0FF1DD0FAA9AE
                B0D8D8D8AEAEAE999999959595959595959595ABABABCFCFCFD8D8D8D8D8D8D1
                D1D1ACACAC959595959595959595959595A5A5A5CACACAC4C4C494C6D06EE8FF
                71E9FF3FC6E070C7D8FFFFFFFFFFFFFFFFFFFFFFFF95C0F50D70EA0D70EA1575
                EB7FB4F4F0F6FEFFFFFFFFFFFFFFFFFFCAE0FA68A6F20D70EA0D70EA3B8BEEFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF18A4BF0ACAF811D1FF16D2FFABAEAED6D6D6
                BDBDBD9292927F7F7F7F7F7F7F7F7F929292B5B5B5D4D4D4D4D4D4B8B8B89393
                937F7F7F7F7F7F7F7F7F7F7F7FABABABD5D5D5BFC1C18ECCD875EAFF76EAFF3B
                C2DB7FCCDBFFFFFFFFFFFFFFFFFFFFFFFF68A6F20D70EA599DF1E1EDFCFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8F2FD86B7F4247EECF7FBFEFFFF
                FFFFFFFFFFFFFFFFFFFF28AAC30CC7F413D2FF19D3FF94B6BECDCDCDC8C8C8A0
                A0A07F7F7F7F7F7F7F7F7F7F7F7F929292ABABABABABAB9696967F7F7F7F7F7F
                7F7F7F7F7F7F858585AEAEAED8D8D8B1B8BA86DBEA7BEBFF7DECFF2BB6D1A7DC
                E6FFFFFFFFFFFFFFFFFFFFFFFF599DF1C2DBFAFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FBFEFFFFFFFFFFFF
                FFFFFFFFFFFF78CADA08B7DD16D2FF1CD4FF5AC4DDB8BEC0D6D6D6AEAEAE8686
                867F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                7F7FA3A3A3C6C6C6D4D4D4ABB1B27FECFF81EDFF82EDFF11A5C1DFF2F6FFFFFF
                FFFFFFFFFFFFF7FBFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFBFE6ED05A8CA19D3FF1ED4FF28D4FC9CB5BBCFCFCFCFCFCFA7A7A77F7F7F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F909090B6B6
                B6D8D8D8B6BDBE92D6E185EEFF87EEFF6BDDEF38B0C8FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF30ADC616C9F220D5FF26D6FF41D1F2A9B5B7D6D6D6C1C1C1A7A7A78585857F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F979797AFAFAFD5D5D5C6C8C8
                A3C4CA88EEFF8AEFFF8EF0FF3FC0D88FD2E0FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFE9
                09ADCF23D6FF28D7FF2FD9FF73C4D7B6BDBFD6D6D6CFCFCFADADAD9898988B8B
                8B8383837F7F7F898989919191A5A5A5BDBDBDD6D6D6CFCFCFACB7B98BEBFA8E
                F0FF90F0FF77E1F238B0C8F7FCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF58BDD118
                C1E62AD7FF31D9FF37DAFF76C5D7ACB8BAD1D1D1D5D5D5C3C3C3B4B4B4ADADAD
                ACACACB0B0B0BBBBBBCBCBCBD8D8D8C1C4C5A9B9BB8FE7F58EF0FF92F1FF95F2
                FF2BB3CCB7E2EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFF9FB07A5C626D0
                F632D9FF38DBFF3FDCFF5BD3EDA4B5B9BDC1C2D1D1D1D8D8D8D8D8D8D8D8D8D8
                D8D8D6D6D6CACACAB0B7B89DCAD18AEDFC8EF0FF93F1FF97F2FF49C4DA87CFDD
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7EFF40AA7C82DD2F6
                3ADBFF40DDFF46DEFF4FDEFC7FC8D7A6B4B7ACAEAEB0B8BAB0B8BAADB4B5ACAE
                AEA3BFC48BDFED87EEFF8DEFFF92F1FF97F2FF5CCFE260C0D3FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFECF20CA8C82ECEEF42
                DDFF47DFFF4EE0FF54E2FF5AE3FF62E3FC78D7EA7CD9EA7CDFF278EBFF7EECFF
                85EEFF8AEFFF90F0FF90EFFD3FBED568C3D5FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7F5F838B0C817B1D142D8
                F84FE0FF55E2FF5BE3FF62E5FF68E6FF6EE8FF74E9FF7BEBFF80ECFF87EEFF8D
                EFFF64D5E918A8C397D6E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7DCE609A3C11CB3D1
                36C7E457E0FB62E5FF69E7FF70E8FF75EAFF7CEBFF69DDF244C4DB16A8C350BA
                CFDFF2F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FCFDA7DCE660C0D306
                9EBD009AB80DA4C11CAECA009AB8009AB830ADC67FCCDBD7EFF4FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF0B75E4BFE6EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0D70EA
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0D70EAFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0D70EAFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0D70EAFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFA4C9F71575EB4A94F00D70EA86B7F45199F01575EBA4C9F7FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F7FBFE247EEC0D70EA0D70EA0D70EA0D70EA4290EFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF95
                C0F50D70EA0D70EA0D70EA1575EBD9E9FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FBFE247E
                EC0D70EA0D70EA8DBCF5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF95C0F50D70EA
                4290EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FBFE2B82EDD9E9FCFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8F2FDFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF}
              Layout = blGlyphTop
              ParentFont = False
              Spacing = 0
              TabOrder = 1
              WordWrap = True
              OnClick = AlignButtonClick
            end
            object CalibButton: TBitBtn
              AlignWithMargins = True
              Left = 6
              Top = 212
              Width = 232
              Height = 195
              Margins.Left = 6
              Margins.Top = 6
              Margins.Right = 6
              Margins.Bottom = 6
              Align = alClient
              Anchors = []
              Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -38
              Font.Name = 'Myriad Pro'
              Font.Style = []
              Glyph.Data = {
                A6290000424DA629000000000000360000002800000034000000440000000100
                1800000000007029000000000000000000000000000000000000FFFFFF878787
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                80808080808080808080808080808080878787FFFFFFFFFFFF808080DBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFF808080DBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDB808080FFFFFFFFFFFF868686808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                80808080868686FFFFFFFFFFFFA2A2A29494949E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E969696
                9A9A9AFFFFFFFFFFFFC7C7C79090909E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E909090BEBEBEFF
                FFFFFFFFFFE7E7E78C8C8C9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E8E8E8EDFDFDFFFFFFFFFFF
                FFFFFFFF83838380808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080838383FBFBFBFFFFFFFFFFFFFFFFFF
                808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFF808080DB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBB2B2B2A4A4A4C0C0C0DBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBC6C6C633334013132D3737371B1B1B484854D4D4D4DBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DB808080FFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBD4D4D43030309595952C2CF82C2CF8D9D9D97A7A7A484854DBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080
                FFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDB8282
                82181864D9D9D9D9D9D92C2CF82C2CF8D9D9D9292929B2B2B2DBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFF
                FFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDB5959591A1A93
                2C2CF8D9D9D9D9D9D92C2CF82C2CF85F5F5F7B7B7BDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFF
                FFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDB6D6D6D6C6C6C2C2CF82C
                2CF8D9D9D9D9D9D92C2CF818184C969696DBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFF
                808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBB9B9B9292929D2D2D22C2CF82C2C
                F8D9D9D9BEBEBE171724D4D4D4DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBEABE46EABE46E1CE98DBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFF808080DB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB82828A3030308787872121BA19198B
                2021239CA6B6DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDED4BAECBB34EDB620F0B000E0D0A4DBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBABABAB5252523737375B5B61C6C6C630
                80F7679DEECDD3DDDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBE1CE98F0B000E8C155DBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB9DBAE50066
                FF0E6EFD7BA8EBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBEDB620EABE46DBDBDBDBDBDBE8C155EDB620E8C155
                DBDBDBDBDBDBE0D0A4F0B000EDB620DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DB808080FFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB458BF40066FF
                0066FF1B74FA96B6E6DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBEDB620ECBB34DBDBDBDED7C6EDB620EDB620DED7C6DBDBDBDB
                DBDBE8C155F0B000F0B000E3CB8CDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080
                FFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBB2C5E20066FF0066FF00
                66FF0066FF3784F6ABC1E3DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBEDB620ECBB34DBDBDBE5C672F0B000E8C155DBDBDBDBDBDBDBDBDBEDB6
                20E8C155EDB620EABE46DBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFF
                FFFFFFFFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB6099EF0066FF0066FF0066
                FF0066FF0066FFABC1E3DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBED
                B620ECBB34DFD2AFEDB620EDB620DCD9D1DBDBDBDBDBDBE4C97FF0B000E0D0A4
                E8C155EDB620DED7C6DBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFF
                FFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBC6D0DE076AFE0066FF0066FF0066FF
                89AFE9DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBEDB620F0B0
                00EDB620EDB620E0D0A4DBDBDBDBDBDBDBDBDBECBB34EDB620DBDBDBE0D0A4F0
                B000E4C97FDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFF
                808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB74A4EC0066FF0066FF6FA1EDDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBEDB620ECBB34E7C464
                F0B000E1CE98DBDBDBDBDBDBDED4BAF0B000E8C155DBDBDBDBDBDBEDB620ECBB
                34DBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFF808080DB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBD4D7DC1571FC80AAEA3784F6B2C5E2DBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBEDB620ECBB34DBDBDBECBB34EC
                BB34DBDBDBDBDBDBE3CB8CECBB34E0D0A4DBDBDBDBDBDBE7C464ECBB34DED7C6
                DBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFFFFFFFFFFFFFF808080DBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBC6D0DEDBDBDBCDD3DD3784F6B2C5E2DBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBEDB620ECBB34DBDBDBE4C97FF0B000E1CE
                98DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDB808080FFFFFFFFFFFFFFFFFFF3F3F3858585808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                80808080808080808080808080808080787E88206DDF687B9880808080808080
                8080808080808080808080E0A913D6A51E808080808080D6A51EF0B000CDA129
                8080808080808080808080808080808080808080808080808080808080808080
                80858585F3F3F3FFFFFFFFFFFFB4B4B49292929E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E949AA42875E78093B09E9E9E9E9E9E9E9E
                9E9E9E9E9E9E9EAEA17EAAA0869E9E9E9E9E9E9E9E9EAEA17EAAA0869E9E9E9E
                9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E9E929292
                B4B4B4FFFFFFFFFFFF8383838080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080787E88206DDF687B988080808080807C7F84
                5C79A48080808080808080808080808080808080808080808080808080808080
                80808080808080808080808080808080808080808080808080808080838383FF
                FFFFFFFFFF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBCDD3DD3784F6B2C5E2C6D0DE3080F7076AFEC6
                D0DEDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFF
                FF808080DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBCDD3DD2B7DF8116FFC0066FF0066FF6DA0EDDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFF808080
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDB82ABEA066AFE0066FF0066FF0066FF1571FCD4D7DCDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB
                DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDB808080FFFFFFFFFFFF86868680808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                806C7C941069EF0066FF0066FF0066FF0066FF0066FF4C75B480808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                808080808080808080808080858585FFFFFFFFFFFF9F9F9F9F9F9FACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                A7AAAF5B8BD30B6BFA0066FF0066FF0066FF2073EFACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACA4A4A4939393FFFFFFFFFFFFBBBBBB979797ACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACA1A7B14C85DB0066FF0066FF0066FF7696C6ACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                AC9B9B9BACACACFFFFFFFFFFFFD7D7D7939393ACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACAC96A3B6367CE50066FF367CE5ACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC969696
                C7C7C7FFFFFFFFFFFFEFEFEF8A8A8AACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
                ACACACACACAC819BC12676ED699BBEA7ACADACACACACACACACACACACACACACAC
                ACACACACACACACACACACACACACACACACACACACACACACACACAC8F8F8FE3E3E3FF
                FFFFFFFFFFFFFFFF949494808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808080808080808080
                8080808080808080808080808080808080808080808080808068858B3C8E9E14
                96AF009AB8009AB800A1CF0499B6009AB81496AF388FA060878E808080808080
                808080808080808080808080808080808080808080939393FBFBFBFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFE6ED30ADC603A4C608B7DD0CC7F40CD0
                FF08CFFF06CFFF03CEFF00CDFF00C5F400B6E000A7CA28AAC3AFDFE9FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFEFF9FB50BACF07A9CA15C8F218D3FF16D3FF13D2FF11D1FF0ED0FF
                0CD0FF08CFFF05CEFF02CDFF00CDFF00CDFF00C5F400AACF38B0C8E7F5F8FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF2
                F640B4CA18C3E922D5FF20D5FF1ED4FF1CD4FF19D3FF16D3FF13D2FF11D1FF0E
                D0FF0BD0FF06CFFF03CEFF00CDFF00CDFF00CDFF00C2EF38B0C8CFECF2FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF2F630ADC623CCF2
                2AD7FF28D7FF26D6FF23D6FF40CCED80BCCAA8AEB0ACACACACACACACACACA7AE
                B07ABBCA30C6EA03CEFF00CDFF00CDFF00CDFF00C6F640B4CACFECF2FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFF9FB38B0C82BCFF431D9FF2FD9FF2D
                D8FF2FD6FC76C0D1AAB5B8C8C8C8D5D5D5D8D8D8D8D8D8D8D8D8D5D5D5C8C8C8
                AAB7BA7BBBCA0FCCFA01CDFF00CDFF00CDFF00C8F838B0C8DFF2F6FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF40B4CA2BCBED38DBFF37DAFF36DAFF4FD0EDA1B5
                B9BEC4C5D8D8D8CFCFCFC3C3C3B7B7B7B2B2B2B4B4B4C2C2C2CFCFCFD8D8D8C3
                C8C8A2B2B634C4E701CDFF00CDFF00CDFF00C5F430ADC6F7FCFDFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFAFDFE914AFCF40DDFF3EDCFF3CDCFF54D2EDAAB6B9D1D1D1D5D5D5
                B8B8B8A9A9A99898988E8E8E8A8A8A8D8D8D959595A7A7A7B4B4B4D4D4D4D4D4
                D4AAB7BA3EC4E401CDFF00CDFF00CDFF00B1D88FD2E0FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF30ADC63AD1F246DEFF44DEFF46DBFCA3B5B9D1D1D1CFCFCFACACAC9292927F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F8B8B8BACACACC9C9C9D4D4D4
                A1B4B814CBF700CDFF00CDFF00C8F820A7C1F7FCFDFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7E2EB16AECC
                4CE0FF4BDFFF4ADFFF83C3D1BFC4C5D6D6D6ADADAD8585857F7F7F7F7F7F7F7F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F828282ACACACD4D4D4C4C8C981
                B9C704CEFF00CDFF00CDFF00AFD597D6E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF68C3D532C5E253E1FF52
                E1FF64D6EDADB6B8D8D8D8BABABA9393937F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F8B8B8BB4B4B4D8D8D8AEB9BC3BC4
                E503CEFF00CDFF00BEEB48B7CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF20A7C14EDAF658E3FF57E2FF92C1
                CAC8C8C8CFCFCFA9A9A97F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F85858580
                80807F7F7F7F7F7F7F7F7F7F7F7F7F7F7FA3A3A3CACACACBCBCB8CB7C106CFFF
                02CDFF00CCFD10A0BDF7FCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFF7FCFD039CBA60E5FF5FE4FF5EE4FFAAAFB0D5D5D5
                C4C4C49B9B9B7F7F7F7F7F7F7F7F7F7F7F7F828282A7A7A7AEAEAEACACAC9696
                967F7F7F7F7F7F7F7F7F7F7F7F929292BDBDBDD6D6D6A7B0B215CEFA05CEFF00
                CDFF00A2C3D7EFF4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFDFF2F60DA4C166E6FF65E6FF65E3FCACAEAED8D8D8B4B4B49D
                9D9D959595959595959595959595A8A8A8CBCBCBD8D8D8D8D8D8B7B7B79F9F9F
                9595959595959595959A9A9AAFAFAFD8D8D8A9B2B422CCF508CFFF03CEFF00A7
                CABFE6EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFBFE6ED1BAECA6CE7FF6BE7FF73E0F5ADB3B4D8D8D8ADADADACACACACAC
                ACACACACACACACACACACAEAEAED8D8D8D8D8D8D8D8D8CBCBCBAFAFAFACACACAC
                ACACACACACACACACACACACD8D8D8ABB7BA38C8EA0CD0FF06CFFF00ADD39FD9E4
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7
                EFF412A7C372E9FF71E9FF71E6FCACAEAED8D8D8B2B2B29C9C9C959595959595
                959595979797ACACACD8D8D8D8D8D8D8D8D8CACACAA9A9A99595959595959595
                95999999AEAEAED8D8D8A9B3B52CCCF20FD1FF0ACFFF01A9CCB7E2EBFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FCFD049C
                BA78EAFF77EAFF76EAFFACAEAED6D6D6C3C3C39898987F7F7F7F7F7F7F7F7F7F
                7F7F989898B8B8B8CDCDCDCACACAAEAEAE8686867F7F7F7F7F7F7F7F7F919191
                BBBBBBD8D8D8A9AEB020D1FA11D1FF0CD0FF01A2C3D7EFF4FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0CA2BF72E4F8
                7DECFF7BEBFFA0C1C7C8C8C8CDCDCDA6A6A67F7F7F7F7F7F7F7F7F7F7F7F7F7F
                7F959595A6A6A6A3A3A38686867F7F7F7F7F7F7F7F7F7F7F7FA0A0A0C8C8C8CF
                CFCF98B5BC1AD3FF14D2FF0FCFFD019FBFEFF9FBFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF60C0D353CEE483EDFF80
                ECFF8CD8E5B4BBBCD8D8D8B6B6B69090907F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F898989B0B0B0D8D8D8B9C0C261C2
                D91CD4FF16D3FF0DC4ED40B4CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFE92BB5CF89EFFF87EEFF84ED
                FFA3C1C7C6C8C9D4D4D4ACACAC8383837F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                7F7F7F7F7F7F7F7F7F7F7F808080AAAAAAD1D1D1CFCFCF9AB5BC25D6FF1ED4FF
                19D3FF08B3D88FD2E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FCFD12A5C181E7F88CEFFF89EFFF89E8F7
                ADB7B8D4D4D4C9C9C9ACACAC8B8B8B7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
                7F7F7F7F898989AAAAAAC4C4C4D6D6D6A9B5B740D1F227D6FF20D5FF1AD2FD03
                A1C1E7F5F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF97D6E23CBDD592F1FF8EF0FF8BEFFF95D7E1B4
                BABCD5D5D5D4D4D4B4B4B4A2A2A29292928A8A8A7F7F7F8989899191919D9D9D
                B0B0B0D1D1D1D6D6D6B6BDBF6EC5D92ED8FF29D7FF22D5FF0FB8DD78CADAFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF38B0C876DFEF94F1FF8FF0FF8CEFFF97D5DFADB5
                B6CBCBCBD8D8D8C8C8C8BDBDBDB2B2B2ACACACB0B0B0BBBBBBC6C6C6D8D8D8CF
                CFCFAAB5B772C6D936DAFF30D9FF2AD7FF20CEF628AAC3F7FCFDFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFDFF2F61DABC68EEDFB95F1FF8FF0FF8BEFFF8AE8F7A6BEC1
                B6BDBECFCFCFD8D8D8D8D8D8D8D8D8D8D8D8D8D8D8D1D1D1BAC0C29FB7BC54D6
                F23DDCFF38DBFF31D9FF2BD6FD08A7C8CFECF2FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFBFE6ED28B1CA8EEDFB94F1FF8EF0FF8AEFFF85EEFF8ED6E2A9
                B7BAADB3B4B1B8BAB0B8BAB0B8BAACB4B5A6B4B77BC9D94BDFFF45DEFF40DDFF
                39DBFF32D8FD0EAECFA7DCE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFBFE6ED1DABC67FE4F492F1FF8CEFFF87EEFF81EDFF7CEBFF7DE3
                F57FD9EA7BD8EA76D7EA6BDBF258E3FF53E1FF4CE0FF47DEFF41DDFF35D4F80C
                A8C8A7DCE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFDFF2F61DABC641C1D886EBFB89EFFF84EDFF7DECFF78EAFF71E9FF
                6CE7FF65E6FF60E5FF59E3FF54E2FF4EE0FF46DDFD21BBDB09A5C3CFECF2FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFF7FCFD8FD2E012A5C139BCD55BD3E97BEAFD79EBFF73E9FF6DE8FF66
                E6FF61E5FF5BE3FF3DCDEB23B9D809A3C17FCCDBEFF9FBFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFEFF9FB97D6E250BACF049CBA17A9C61DAECA1CAECA1AAECA15AA
                C8009AB848B7CC8FD2E0E7F5F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFCFECF2BFE6EDBFE6EDBFE6EDC7E9EFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF}
              Layout = blGlyphTop
              ParentFont = False
              Spacing = 0
              TabOrder = 2
              WordWrap = True
              OnClick = CalibButtonClick
            end
            object Button8: TBitBtn
              AlignWithMargins = True
              Left = 250
              Top = 212
              Width = 233
              Height = 195
              Margins.Left = 6
              Margins.Top = 6
              Margins.Right = 6
              Margins.Bottom = 6
              Align = alClient
              Anchors = []
              Caption = #1050#1072#1085#1072#1083#1099
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -38
              Font.Name = 'Myriad Pro'
              Font.Style = []
              Glyph.Data = {
                36300000424D3630000000000000360000002800000040000000400000000100
                1800000000000030000027040000270400000000000000000001FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7C2C2C2868686C6C6C6FFFF
                FFFFFFFFFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFAFAFA747474393939060606000000000000767676FFFF
                FFF9F9F92F2F2F797979F6F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF1F1F1F0000000000000000000000001111114F4F
                4F4B4B4B000000000000212121B7B7B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFC2C2C2FFFFFFEEEEEE2B2B2B0000000000000000000000000000000000
                00000000000000000000000000000000555555F3F3F3FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                A6A6A60000004C4C4C1E1E1E0000000000000000000000000000000000000000
                000000000000000000000000000000003F3F3FFEFEFEFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF
                1717170000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000969696FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF686868
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000111111C0C0C09B9B9BE6E6E6FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8C8C8020202
                0000000000000000000000000000000000000000000808084141414A4A4A1010
                10000000000000000000000000000000000000000000000000A2A2A2FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCBCBC1B1B1B
                000000000000000000000000000000000000595959EBEBEBFFFFFFFFFFFFF7F7
                F77A7A7A000000000000000000000000000000000000000000666666FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDADADA
                0000000000000000000000000000003D3D3DFBFBFBFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF6B6B6B0000000000000000000000000000000000002A2A2AFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBDBD
                000000000000000000000000000000A9A9A9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFE1E1E1000000000000000000000000000000000000010101EDEDEDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E94D4D4D121212
                000000000000000000000000000000D5D5D5FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF030303000000000000000000000000000000050505C1C1C1FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFB0B0B0B000000
                000000000000000000000000000000C1C1C1FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFEEEEEE0202020000000000000000000000006B6B6BF5F5F5FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF444444000000
                000000000000000000000000000000393939FEFEFEFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF939393000000000000000000000000000000A9A9A9FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF828282000000
                0000000000000000000000000000000000008A8A8AFDFDFDFFFFFFFFFFFFFFFF
                FFBEBEBE090909000000000000000000000000000000393939D3D3D3FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF000000
                0000000000000000000000000000000000000000001B1B1B7272729999994D4D
                4D0101010000000000000000000000000000000000000000006E6E6EFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F64B4B4B
                7F7F7F1010100000000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000000001A1A1AF1F1F1FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFDADADA9E9E9EE2E2E2FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFAAAAAA0000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000000000AAAAAAFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9
                F9C7C7C78B8B8B505050151515000000000000999999FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF9898980000000000000000000000000000000000000000000000000000
                00000000000000000000000000181818000000494949FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F1F
                1F0000000000000000000000000000000000005C5C5CFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFE9E9E91818187C7C7CF7F7F7FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF3F3F3F0000000000000000000000000000000000000000000000000000
                00000000000000080808A6A6A6FAFAFA878787DEDEDEFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5252
                52000000000000000000000000000000000000202020FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF5C5C5C000000000000222222B6B6B6FFFFFFFFFFFFFFFFFF
                FFFFFFFBFBFB8C8C8C0B0B0B0000000000001010100E0E0E0000000000000000
                00000000000000060606F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F
                8F000000000000000000000000000000000000000000B1B1B1E8E8E8FFFFFFFF
                FFFFFFFFFFBDBDBD000000000000000000000000000000545454E4E4E4FFFFFF
                FFFFFFFFFFFFFFFFFFE2E2E24F4F4F010101C4C4C4FFFFFF8A8A8A0000000000
                00000000000000212121DADADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCBCB
                CB0000000000000000000000000000000000000000000000000000001212123B
                3B3B6565652323230000000000000000000000000000000000000D0D0DADADAD
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD0D0D0FFFFFFFFFFFFCFCFCF3333336E6E
                6EAAAAAAE5E5E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB6B6
                B600000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000050505D6D6D6
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFDEDEDE3A3A3AC9C9C9FFFFFFFFFFFFFFFFFFFAFAFA7575750101
                0100000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000797979FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8C3C3C3E5E5E5FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF4A4A4A0000000202026B6B6BF0F0F0DBDBDB3434340000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000202020F6F6F6FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFCFCFCF3B3B3B070707000000939393FFFFFFFFFFFFFF
                FFFFF4F4F4A8A8A8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFABABAB0000000000000000000000001818180C0C0C0000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000717171FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFE0E0E00000000000000000005A5A5AFFFFFFFFFFFFFF
                FFFF737373000000424242D9D9D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F1F1F11A1A1A0000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000050505CDCDCDFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF1C1C1C0000000000000505053131315E5E5E80
                80800404040000000000000C0C0CEEEEEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                6D6D6D0000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000282828F5F5F5
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6
                A6DFDFDFFFFFFFFAFAFA76767601010100000000000000000000000000000000
                00000000000000000000007A7A7AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC
                0202020000000000000000000000000000000000000000000000000000000000
                000000000000000000000E0E0E737373C8C8C8E6E6E6E7E7E7BDBDBD93939334
                3434000000000000000000000000000000000000000000000000000000686868
                FFFFFFFFFFFFF0F0F0B6B6B6B7B7B7FFFFFFFFFFFFFFFFFFFFFFFFD9D9D90707
                070A0A0A82828236363600000000000000000000000000000000000000000000
                0000000000000000050505EFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B9B9
                1919190000000000000000000000000000000000000000000000000000000000
                000000000606068A8A8AF1F1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
                FEFEA2A2A21A1A1A000000000000000000000000000000000000000000000000
                5A5A5A2E2E2E020202000000393939FFFFFFFFFFFFFFFFFFFFFFFF4242420000
                000000000000000000000000000000000000000707071D1D1D22222201010100
                00000000000000000000006D6D6DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F1F1F16D6D6D0303030000000000000000000000000000000000000000000000
                00020202ABABABFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFDFDFDF191919000000000000000000000000000000000000000000
                000000000000000000000000060606F7F7F7FFFFFFFFFFFFDEDEDE1515150000
                00000000000000000000000000070707828282ECECECFFFFFFFFFFFFDBDBDB67
                6767000000000000000000000000B0B0B0CCCCCC8F8F8FDEDEDEFFFFFFFFFFFF
                FFFFFFFFFFFFCDCDCD0303030000000000000000000000000000000000000000
                009A9A9AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFD2D2D20F0F0F000000000000000000000000000000000000
                000000000000000000000000000000C1C1C1FFFFFFFFFFFFFFFFFFEDEDED5F5F
                5F000000000000000000030303AEAEAEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFF8D8D8D000000000000000000000000000000000000959595FFFFFFFFFFFF
                FFFFFFFFFFFFE7E7E70000000000000000000000000000000000000000004A4A
                4AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF848484000000000000000000000000000000000000
                000000000000000000000000000000848484FFFFFFFFFFFFFFFFFFFFFFFFBABA
                BA000000000000000000676767FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFF393939000000000000000000000000000000595959FFFFFFFFFFFF
                FFFFFFFFFFFFBDBDBD000000000000000000000000000000000000000000ABAB
                ABFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFE9E9E9060606000000000000000000000000000000
                000000000000000000000000000000484848FFFFFFFFFFFFFFFFFFFFFFFF9090
                90000000000000000000CACACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFA5A5A50000000000000000000000000000001C1C1CFFFFFFFFFFFF
                FFFFFFFFFFFF939393000000000000000000000000000000000000131313F9F9
                F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF535353000000000000000000000000000000
                0000000000000000000000000000000E0E0EFDFDFDFFFFFFFFFFFFF9F9F95858
                58000000000000000000EAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFBFBFBF000000000000000000434343ACACACE9E9E9FFFFFFFFFFFF
                FFFFFFFFFFFF6969690000000000000000000000000000000000004B4B4BFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF707070000000000000000000000000000000
                000000000000000000000000040404343434E7E7E7F2F2F23A3A3A0707070000
                00000000000000000000E0E0E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFB9B9B9000000000000000000A8A8A8FFFFFFFFFFFFFFFFFFFFFFFF
                E8E8E8ADADAD272727000000000000000000000000000000000000434343FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF7A7A7A000000000000000000000000000000
                000000050505808080BCBCBCF4F4F4FFFFFFFFFFFFFFFFFF1B1B1B0000000000
                00000000000000000000949494FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFF636363000000000000000000D3D3D3FFFFFFFFFFFF696969252525
                000000000000000000000000000000000000000000000000000000393939FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF838383000000000000000000000000000000
                0000002C2C2CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5757570000000000
                00000000000000000000202020ECECECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFD2D2D20B0B0B000000000000000000CDCDCDFFFFFFFFFFFF3C3C3C000000
                000000000000000000000000000000000000000000000000000000232323FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF515151000000000000000000000000000000
                000000565656FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9999993939397676
                76404040000000000000000000373737DDDDDDFFFFFFFFFFFFFFFFFFFFFFFFCB
                CBCB181818000000000000000000000000050505797979F9F9F9787878000000
                000000000000000000000000000000000000000000000000000000000000C3C3
                C3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFEAEAEA050505000000000000000000000000000000
                000000818181FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFE8E8E816161600000000000000000006060655555585858587878742424200
                0000000000000000000000000000000000000000313131FCFCFCB5B5B5000000
                0000000000000000000000000000000000000000000000000000000000005757
                57FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF8E8E8E000000000000000000000000000000000000
                000000ABABABFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFB0B0B000000000000000000000000000000000000000000000000000
                00000000000000000F0F0F1D1D1D000000020202CACACAFFFFFFEFEFEF020202
                0000000000000000000000000000000000000000000000000000000000000404
                04BBBBBBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFD9D9D9151515000000000000000000000000000000000000
                000000AAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF85858500000000000000000000000000000000000000000000000000
                00000000003C3C3CE0E0E0F3F3F37070706E6E6EFFFFFFFFFFFFFFFFFF2D2D2D
                0000000000000000000000000000000000000000000000000000000000000000
                000D0D0DCECECEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFE2E2E21D1D1D000000000000000000000000000000000000000000
                000000010101676767EFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFE0E0E00A0A0A00000000000001010123232302020200000000000000000000
                00003B3B3BFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6A6A6A
                0000000000000808083D3D3D0404040000000000000000000000000000000000
                00000000151515A1A1A1FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFBCBCBC262626000000000000000000000000000000000000000000000000
                000000000000000000181818ADADADFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFEFEFEF686868010101000000616161FFFFFFF5F5F5CACACA17171700000000
                0000212121FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1C1C1
                898989C5C5C5F9F9F9FFFFFF8282820000000000000000000000000000000000
                00000000000000000000303030A0A0A0D0D0D0F8F8F8FFFFFFEAEAEAA5A5A531
                3131000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000008A8A8AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFC7C7C7414141EBEBEBFFFFFFFFFFFFFFFFFF53535300000000
                0000000000EAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFDFDFD3D3D3D0000000000000000000000000000
                000000000000000000000000000000000000000404040A0A0A00000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000002D2D2DFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABABAB737373AD
                ADADE7E7E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E11010100000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000010101C5C5C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9F0000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000000000000D0D0D000000000000
                000000000000656565FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6161610000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000D0D0DA2A2A2F2F2F2707070030303
                000000151515EEEEEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1C1C10101010000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000003A3A3ADEDEDEFFFFFFFFFFFFFFFFFFCFCFCF
                383838A2A2A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFAFAFA2A2A2A0000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000747474FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFCECECE1111110000000000000000000000000000
                000000000404044D4D4D27272704040400000000000000000000000000000000
                0000000000000000000000A0A0A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E95A5A5A0000000000000000000000
                00000000747474FFFFFFFFFFFFFAFAFAD6D6D6AEAEAE13131300000000000000
                0000000000000000000000646464FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBBBBBB2424240000000000
                001F1F1FF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4F4F4F00000000000000
                0000000000000000000000282828FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F77C7C7C0606
                06B5B5B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8A8A8A00000000000000
                0000000000000000000000000000EBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E4
                E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC4C4C400000000000001
                0101292929656565A2A2A2DEDEDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8747474B0B0B0EB
                EBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              Layout = blGlyphTop
              ParentFont = False
              Spacing = 0
              TabOrder = 3
              WordWrap = True
              OnClick = Button8Click
            end
          end
          object Panel11: TPanel
            Left = 489
            Top = 0
            Width = 81
            Height = 413
            Align = alClient
            BevelOuter = bvNone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -27
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object Button6: TBitBtn
            AlignWithMargins = True
            Left = 576
            Top = 6
            Width = 234
            Height = 401
            Margins.Left = 6
            Margins.Top = 6
            Margins.Right = 6
            Margins.Bottom = 6
            Align = alClient
            Anchors = []
            Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1056#1091#1095#1085#1099#1093' '#1055#1069#1055
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -38
            Font.Name = 'Myriad Pro'
            Font.Style = []
            Glyph.Data = {
              1A3A0000424D1A3A00000000000036000000280000004B000000410000000100
              180000000000E439000000000000000000000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E9F1F1F1FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C1414144B4B4B626262777777EE
              EEEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF565656161616000000000000000000
              000000A8A8A8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF171616
              B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6767670000000000001C1C1C4444
              44080808000000323232B8B8B8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF3534
              32B7A89B494542B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE898989000000191919DE
              DEDEFFFFFFA8A8A80101010000004D4D4DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F746
              4443A99B8FE2CFBFD4C2B33B3836CFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF292929000000
              313131FFFFFFFFFFFFE5E5E50101010606069C9C9CFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              5E5D5B8E837AE2CFBFE2CFBFE2CFBFCDBCAD353331DFDFDFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFC3232
              320000000202027A7A7AC0C0C0555555000000000000B5B5B5FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FF8584846C645EE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFB0A2953E3C3BF7F7F7FF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFAFAFAD4D4D4B5B5B5F8F8F8FFFFFFFFFFFFFFFFFFFF
              FFFFD5D5D53C3C3C000000000000000000000000000000101010CFCFCFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFA6A6A6504B46E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF80766D
              6E6D6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFEFEFE535353050505000000DDDDDDFFFFFFD5D5D5
              626262D9D9D9FFFFFF8B8B8B0000000A0A0A000000000000767676CFCFCFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFB7B7B73B3835DBC9B9E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBF494440AEAEAEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5151510000000000003B3B3B7373
              733D3D3D0000000F0F0FAFAFAFFFFFFFBFBFBFD4D4D47F7F7F868686F2F2F2FF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF2F2E2DA29589E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBFE2CFBFD4C2B3262423E7E7E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFF6F6F66E6E6EC8C8C89D9D9D14141400000000000000
              00000000000000000000000F0F0FD1D1D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              E1E1E1888888FCFCFCFBFBFBE9E9E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFE7E7E7353331C6B5A7E2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF8E82785E5D5CFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF848484000000020202000000000000000000
              0000000000000000000000000000001D1D1DECECECFFFFFFFFFFFFFFFFFFFEFE
              FEFFFFFF9C9C9C0000005A5A5A4D4D4D353535F7F7F7FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFBFBFBF100F0FF7F7F7FFFFFFCFCFCF34312FDBC9B9E2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF33312EC7C7C7FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFC2626260000000000000000000000
              004C4C4CB4B4B4CDCDCD8F8F8F1919190000000000004949498F8F8F9E9E9EFF
              FFFF8585853838381616161212121717170000002F2F2FFAFAFAFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFCFCFCF3B38368E82784E4D4CFFFFFFFFFFFFA6A6A6504B46E2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFB7A89B2E2D2C
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEDDDDDD2E2E2E00000000
              00005B5B5BFFFFFFFFFFFFFFFFFFFFFFFFD1D1D1131313000000000000000000
              2A2A2AFFFFFFB0B0B00000002C2C2CE2E2E2FBFBFB929292040404303030FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFCFCFCF3B3836D4C2B3E2CFBF6C645E858484FFFFFFFFFFFF
              767574797068E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BF494440AFAFAFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3A3A3A
              0000000A0A0ADBDBDBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7070700000000000
              00000000000000E3E3E3D2D2D2010101A5A5A5FFFFFFFFFFFFFFFFFF2525250E
              0E0EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFCFCFCF3B3836D4C2B3E2CFBFE2CFBFE2CFBF494440AEAE
              AEFFFFFFFFFFFF464544A29589E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBFB7A89B2E2D2CFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5F5F59292
              920C0C0C0000001F1F1FF8F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9A9A9A00
              00000000003E3E3EB1B1B1DFDFDF111111000000848484FFFFFFFFFFFFEDEDED
              0C0C0C717171FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFBFBFBF3B3836D4C2B3E2CFBFE2CFBFE2CFBFE2CFBFD4
              C2B3343230D7D7D7FFFFFFCFCFCF2C2A28E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBFE2CFBF33312EC7C7C7FFFFFFFFFFFFFFFFFF000000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDB
              DBDB0000000000000000000B0B0BE0E0E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              727272000000000000ACACACFFFFFFFDFDFDADADAD5A5A5A090909818181A6A6
              A63B3B3B000000111111FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFF7F7F72E2D2CB7A89BE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFB7A89B2E2D2CFFFFFFFFFFFF4E4D4C9B8E83E2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF9B8E834E4D4CFFFFFFFFFFFFFFFFFF00
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFEAEAEA000000000000000000000000666666FFFFFFFFFFFFFFFFFFFFFF
              FFD4D4D41515150000000000008B8B8BFBFBFBFFFFFFFFFFFFA6A6A600000000
              0000000000040404999999989898FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7C7C73B3835DBC9B9E2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBF494440AFAFAFFFFFFFCFCFCF2C2A28E2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF161514E7E7E7FFFFFF
              FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFF7F7F7737373818181050505000000000000595959BEBEBED6
              D6D69696961D1D1D000000000000000000000000949494FFFFFFFFFFFFC4C4C4
              6F6F6FDDDDDD4E4E4E3F3F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBFFFFFFFFFFFFFFFFFFFA6A6A6504B46E2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFCDBCAD1E1D1CF7F7F7FFFFFF6F6E6D
              7F756CE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF5D56509797
              97FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF828282000000000000000000
              000000000000000000000000000000000000000000111111DADADAFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFEEEEEEEDEDEDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF1E1D1B767574FFFFFFFFFFFFFFFFFF
              6E6D6C80766DE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF655D578E8E8EFFFF
              FFE7E7E71E1D1BDBC9B9E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFB0
              A295363534FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE7474740000000000
              000000000000000000000000000000003F3F3FC7C7C78181819E9E9EFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF3B3836D4C2B3504B46A6A6A6FFFF
              FFFFFFFFF7F7F71E1D1CCDBCADE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFCDBCAD1E
              1D1CF7F7F7FFFFFF5E5E5D8D8177E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBF1E1C1BDFDFDFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F84C4C4C00
              00000000006666665D5D5D0F0F0F000000000000C8C8C8FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF3B3836D4C2B3E2CFBFDBC9B93B
              3835C7C7C7FFFFFFFFFFFFA6A6A6504B46E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBF655D578E8E8EFFFFFFAFAFAF494440E2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBF57504B9F9F9FFFFFFF000000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FDFDFD8A8A8A6D6D6DFFFFFFFFFFFF646464000000121212B3B3B3FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF3B3836D4C2B3E2CFBFE2CFBF
              E2CFBFB7A89B2E2D2CFFFFFFFFFFFFFFFFFF2E2D2CB7A89BE2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFA99B8F3E3D3CFFFFFFEFEFEF171615DBC9B9E2CFBFE2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBF8D81775E5E5DFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD1D1D1B0B0B0E2E2E2FDFDFDFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF3B3836D4C2B3E2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBF655D578E8E8EFFFFFFFFFFFFAFAFAF494440E2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBF0F0E0EEFEFEFFFFFFF3E3D3CA99B8FE2CFBF
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFC6B5A71F1E1DFFFFFF000000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFB7B7B71010103030308F8F8FEFEFEFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F70F0F0ED4C2B3E2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFCDBCAD1E1D1CF7F7F7FFFFFFFFFFFF171616
              CDBCADE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF57504B9F9F9FFFFFFF8F8F8F645C
              56E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF1E1C1BDFDFDF00
              0000FFFFFFFFFFFFFFFFFFFFFFFF878787484848BFBFBFCFCFCF707070383838
              DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E7FFFFFF8584846C645E
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF655D578E8E8EFFFFFFFFFF
              FF5E5E5D8D8177E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF94887D565555FFFFFFDF
              DFDF1E1C1BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF494440
              AFAFAF000000FFFFFFFFFFFFFFFFFF979797606060FFFFFFFFFFFFFFFFFFFFFF
              FFD7D7D7303030CFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0C0F1000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000C0F10FFFFFFFFFFFFA7A7A70F0F0FF7F7F7FFFF
              FF464544A29589E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFA99B8F3E3D3CFF
              FFFFFFFFFFAFAFAF494440E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFD4C2B3171615
              F7F7F7FFFFFF171616CDBCADE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BF645C568F8F8F000000FFFFFFFFFFFFE7E7E7202020F7F7F7FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFEFEFEF383838D7D7D7FFFFFF0E0E0E000000000000020202
              00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0
              FF00C0FF00C0FF00C0FF00C0FF00C0FF000000FFFFFF9E9D9D423E3B6B635C77
              7675FFFFFFCFCFCF2C2A28E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFDBC9B9
              171615EFEFEFFFFFFFF7F7F70F0E0EDBC9B9E2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BF252321D7D7D7FFFFFF3E3D3CA99B8FE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBF7F746B6F6F6E000000FFFFFFFFFFFF7070708F8F8FFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B7B7484848FFFFFF0000007F7F7F7F7F
              7F00000000C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00
              C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00000076757457514CDBC9B9
              DBC9B934312FCFCFCFFFFFFF4E4D4C9B8E83E2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBF504A45A7A7A7FFFFFFFFFFFF363534B0A295E2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBF3B3734BFBFBFFFFFFF56555594887DE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBFA29589464544000000FFFFFFFFFFFF080808EFEFEFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF505050AFAFAF0000007F
              7F7F7F7F7F00000000C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF
              00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF000000797068E2CF
              BFE2CFBFE2CFBFA99B8F3E3D3CFFFFFF9F9F9F57504BE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBFE2CFBF786E65787777FFFFFFFFFFFF5E5E5D8D8177E2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBF57504B9F9F9FFFFFFF676666867B71E2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBFA99B8F3E3D3C000000FFFFFFCFCFCF303030FF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E7202020
              0000007F7F7F7F7F7F00000000C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0
              FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF000000E2
              CFBFE2CFBFE2CFBFE2CFBFDBC9B9080707FFFFFFE7E7E7161514E2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBFE2CFBF8D81775E5E5DFFFFFFFFFFFF787777786E65E2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBF6B625B878787FFFFFF7F7F7F71675FE2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFC6B5A71F1E1D000000FFFFFF8F8F8F
              707070FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FF7070700000007F7F7F7F7F7F00000000C0FF00C0FF00C0FF00C0FF00C0FF00
              C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF
              000000E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF2C2A28CFCFCFFFFFFF080707DBC9
              B9E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFA99B8F3E3D3CFFFFFFFFFFFF8F8F8F64
              5C56E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF7F746B6F6F6EFFFFFF9F9F9F57504B
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFC6B5A71F1E1D000000FFFF
              FF505050AFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFE7E7E70000007F7F7F7F7F7F00000000C0FF00C0FF00C0FF00C0FF
              00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0
              FF00C0FF000000E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF504A45A7A7A7FFFFFF1F
              1E1DC6B5A7E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFBFAFA1262525FFFFFFFFFFFF
              9F9F9F57504BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF8D81775E5E5DFFFFFFB7B7
              B7423E3AE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFC6B5A71F1E1D00
              0000FFFFFF404040BFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0000007F7F7F7F7F7F00000000C0FF00C0FF00C0
              FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00
              C0FF00C0FF00C0FF000000E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF494440AFAFAF
              FFFFFF1F1E1DC6B5A7E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFB7A89B2E2D2CFFFF
              FFFFFFFF9F9F9F57504BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF8D81775E5E5DFF
              FFFFAFAFAF494440E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFC6B5A7
              1F1E1D000000FFFFFF303030CFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000007F7F7F7F7F7F00000000C0FF00
              C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF
              00C0FF00C0FF00C0FF00C0FF000000E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF1615
              14E7E7E7FFFFFF000000E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFA2958946
              4544FFFFFFFFFFFF7F7F7F71675FE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF71675F
              7F7F7FFFFFFF9797975D5650E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BFC6B5A71F1E1D000000FFFFFF202020DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000007F7F7F7F7F7F000000
              00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0
              FF00C0FF00C0FF00C0FF00C0FF00C0FF000000B7A89BE2CFBFE2CFBFE2CFBFCD
              BCAD171616FFFFFFCFCFCF2C2A28E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
              8D81775E5E5DFFFFFFFFFFFF6F6F6E7F746BE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BF645C568F8F8FFFFFFF7F7F7F71675FE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBFB7A89B2E2D2C000000F7F7F7080808F7F7F7FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000007F7F7F7F7F
              7F00000000C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00
              C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF0000003D3B3AB7A89BE2CFBF
              E2CFBF867C72666665FFFFFF8686866B635CE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBF6B625B878787FFFFFFFFFFFF56555594887DE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBF504A45A7A7A7FFFFFF5E5E5D8D8177E2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBFA99B8F3E3D3C000000DFDFDF202020FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000007F
              7F7F7F7F7F00000000C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF
              00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF000000EFEFEF3D3B
              3AB7A89BCDBCAD262423EFEFEFFFFFFF2E2D2CB7A89BE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBFE2CFBF3B3734BFBFBFFFFFFFFFFFFF2E2D2CB7A89BE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBF3B3734BFBFBFFFFFFF464544A29589E2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBF94887D565555000000C7C7C7383838FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              2A2A2A00000000000006060600C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0
              FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF000000FF
              FFFFEFEFEF3D3B3A33302DB7B7B7FFFFFFAFAFAF494440E2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBFD4C2B30F0F0EFFFFFFFFFFFFDFDFDF1E1C1BE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBF161514E7E7E7FFFFFF2E2D2CB7A89BE2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF786E65787777000000BFBFBF404040
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFF242D3000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              242D30FFFFFFFFFFFFDFDFDF585757FFFFFFEFEFEF2E2C2BC6B5A7E2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBF8D81775E5E5DFFFFFFFFFFFF9797975D5650E2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFC6B5A71F1E1DFFFFFFF7F7F7171615D4C2B3
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF5D5650979797000000BFBF
              BF404040FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF464543A3968BE2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF494440AFAFAFFFFFFFFFFFFF464544
              A29589E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF7F746B6F6F6EFFFFFFBFBFBF3B37
              34E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF3B3734BFBFBF00
              0000CFCFCF303030FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF2E2D2BB7A89B
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF9B8E834E4D4CFFFFFFFFFFFFE7E7
              E71E1D1BDBC9B9E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF3B3734BFBFBFFFFFFF6F
              6F6E7F746BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF0F0E0E
              EFEFEF000000FFFFFF282828D7D7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEF
              EF3D3B3AB7A89BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF2C2A28CFCFCFFFFFFFFF
              FFFF6F6E6D7F756CE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFD4C2B3171615F7F7F7
              FFFFFF262525BFAFA1E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFB7A8
              9B2E2D2CFFFFFF000000FFFFFFD7D7D7303030EFEFEFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFEFEFEF3D3B3AB7A89BE2CFBFE2CFBFE2CFBF95897E565554FFFFFF
              FFFFFFE7E7E71E1D1BDBC9B9E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF94887D5655
              55FFFFFFDFDFDF1E1C1BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBF7F746B6F6F6EFFFFFF000000FFFFFFFFFFFFB7B7B7404040DFDFDFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFEFEFEF3D3B3AB7A89BE2CFBFB7A89B363433F7F7
              F7FFFFFFFFFFFF6F6E6D7F756CE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF49
              4440AFAFAFFFFFFF8F8F8F645C56E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBF494440AFAFAFFFFFFF000000FFFFFFFFFFFFFFFFFFCFCFCF4040
              40D7D7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF3D3B3AA99B8F2D2B29DF
              DFDFFFFFFFFFFFFFDFDFDF252422DBC9B9E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
              B7A89B2E2D2CFFFFFFFFFFFF2E2D2CB7A89BE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFDBC9B90F0E0EF7F7F7FFFFFF000000FFFFFFFFFFFFFFFFFFFF
              FFFFDFDFDF383838B7B7B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF272625
              BFBFBFFFFFFFFFFFFFF7F7F7363433B7A89BE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBF33312EC7C7C7FFFFFFAFAFAF494440E2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBF8D81775E5E5DFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFEFEFEF4040408F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFF56555495897EE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBF9B8E834E4D4CFFFFFFFFFFFF2E2D2CB7A89BE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBF423E3AB7B7B7FFFFFFFFFFFF000000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7404040C7C7C7FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF8584846C645EE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBF2C2A28CFCFCFFFFFFFAFAFAF494440E2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFCDBCAD1E1D1CF7F7F7FFFFFFFFFFFF00
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF303030FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF0F0F0EB7A89BE2CFBFE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBF95897E565554FFFFFFFFFFFF2E2D2CB7A89BE2CFBFE2
              CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF7F756C6F6E6DFFFFFFFFFFFF
              FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3030
              30CFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF3D3B3AB7A89BE2
              CFBFE2CFBFE2CFBFE2CFBFB7A89B363433F7F7F7FFFFFF8E8E8E655D57E2CFBF
              E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFDBC9B91E1D1BE7E7E7FFFF
              FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFF878787787878FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF
              3D3B3AB7A89BE2CFBFE2CFBFD4C2B32D2B29DFDFDFFFFFFFDFDFDF353331CDBC
              ADE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF7F756C6F6E6DFF
              FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF9F9F9F606060FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFEFEFEF3D3B3AB7A89BDBC9B9423E3BBFBFBFFFFFFFF7F7F73E3C3BB0
              A295E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF2C2A28
              CFCFCFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFAFAFAF505050FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFEFEFEF3D3B3A48433F969595FFFFFFFFFFFF565554
              95897EE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF7F75
              6C6F6E6DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAF505050FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF585757FFFFFFFFFFFF8584
              846C645EE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFCD
              BCAD2D2B2AE7E7E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7F7F7F7FFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAE
              AEAE494440E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
              E2CFBF655D578E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF404040BFBFBFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF2F2E2DA99B8FE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
              BFE2CFBFB0A295363534FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
              0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF686868F7F7F7
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFE7E7E7353331C6B5A7E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
              CFBFE2CFBFD4C2B32D2B29DFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF3B3836D4C2B3E2CFBFE2CFBFE2CFBF
              E2CFBFE2CFBFE2CFBF494440AEAEAEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB6B6B6423E3AE2CFBFE2CF
              BFE2CFBFE2CFBFE2CFBF80766D6E6D6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9695955E
              5852E2CFBFE2CFBFE2CFBF9C8F844E4D4CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF6E6D6C80766DE2CFBFA99B8F464443F7F7F7FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF4E4D4C786F663D3B3AEFEFEFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F75F5E5EEFEFEFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
            Layout = blGlyphTop
            ParentFont = False
            Spacing = 0
            TabOrder = 2
            WordWrap = True
            OnClick = Button6Click
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 673
        Width = 820
        Height = 226
        Align = alClient
        Caption = #1040#1085#1072#1083#1080#1079
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        object ArchButton: TBitBtn
          AlignWithMargins = True
          Left = 8
          Top = 40
          Width = 804
          Height = 178
          Margins.Left = 6
          Margins.Top = 6
          Margins.Right = 6
          Margins.Bottom = 6
          Align = alClient
          Anchors = []
          Caption = #1040#1088#1093#1080#1074
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -38
          Font.Name = 'Myriad Pro'
          Font.Style = []
          Glyph.Data = {
            A62F0000424DA62F00000000000036000000280000003A000000450000000100
            180000000000702F000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFB7B7B7AFAFAFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF9F9F9F686868282828101010
            3B3B3B303030606060E7E7E71010100000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000101010FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D76868
            682020204949495D5D5D6262625A5A5A5A5A5AFFFFFF5F5F5F202020000000FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFAFAFAF4040402424245E5E5E5F5F5F5B5B5B5B5B5B5B5B5B5B5B5B
            5B5B5BFFFFFF5B5B5B5D5D5D000000FFFFFF10C4FF00C0FF00C0FF00C0FF00C0
            FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF10
            C4FFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D74040404040406363635B5B5B5B5B
            5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5B5BFFFFFF5B5B5B5B5B5B000000FF
            FFFF00C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C0FFFFFFFF000000FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F77878782A
            2A2A6464645C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5C5C
            5C5C5CFFFFFF5C5C5C5C5C5C000000FFFFFF0030FF0000FF0000FF0000FF0000
            FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF4040FF00
            C0FFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFFFFFFFC7C7C72020205959595F5F5F5D5D5D5D5D5D5D5D5D5D5D5D5D5D
            5D5D5D5D5A5A5A3D3D3D3A3A3A5D5D5D5D5D5DFFFFFF5C5C5C5C5C5C000000FF
            FFFF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF
            00C0FF00C0FF00C0FF00C0FF00C0FF00C0FFFFFFFF000000FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFAFAFAF3636366262625D5D5D5D
            5D5D5D5D5D5D5D5D5D5D5D5D5D5D4343432020200606062020204646465D5D5D
            5D5D5DFFFFFF5D5D5D5D5D5D000000FFFFFF00C0FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFE7E7FFCC0538FFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
            C0FFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            8F8F8F4040406060605E5E5E5E5E5E5E5E5E5E5E5E5B5B5B3838380C0C0C1B1B
            1B3E3E3E5B5B5B5E5E5E5858585E5E5E5E5E5EFFFFFF5D5D5D5D5D5D000000FF
            FFFF00C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7070FFAF3F8FFF5050FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C0FFFFFFFF000000FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFF6868685151515E5E5E5E5E5E5E5E5E5E5E5E5E
            5E5E4949491212122626265252525E5E5E5555553838380F0F0F2323235E5E5E
            5E5E5EFFFFFF5E5E5E5E5E5E000000FFFFFF00C0FF00C0FF00C0FF00C0FF00C0
            FF00C0FF0054FF08A9F7A7425800C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00
            C0FFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000DFDFDF080808
            5D5D5D5E5E5E5F5F5F5F5F5F5C5C5C2D2D2D1515155050505F5F5F4444442121
            210909092727275050505F5F5F5F5F5F5F5F5FFFFFFF5E5E5E5E5E5E000000FF
            FFFF00C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7FFCC0538FFCFCFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C0FFFFFFFF000000FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFF0000005D5D5D5E5E5E5F5F5F4A4A4A12121232
            32325F5F5F3B3B3B0909091B1B1B3E3E3E5F5F5F5656563B3B3B3232325F5F5F
            5F5F5FFFFFFF5F5F5F5F5F5F000000FFFFFF00C0FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFF7070FFAF3F8FFF5050FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
            C0FFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF000000
            5D5D5D5B5B5B2D2D2D1515155151515757571B1B1B2424245757576060603C3C
            3C1818180909092424244B4B4B606060606060FFFFFF5F5F5F5F5F5F000000FF
            FFFF00C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7070FFE8E0F7FF5858FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C0FFFFFFFF0000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000001010100000FFFFFF0000005D5D5D4F4F4F3333336161613C3C3C0F
            0F0F4646465B5B5B303030090909252525494949616161616161616161616161
            616161FFFFFF606060606060000000FFFFFF00C0FF00C0FF00C0FF00C0FF00C0
            FF00C0FF00AEFFC70438309CCF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00
            C0FFFFFFFF00000000C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF
            00C0FF00C0FF00C0FF00C0FF00C0FF10C4FFFFFFFF0000000000FFFFFF000000
            5D5D5D5E5E5E5757571B1B1B2525255B5B5B4646460F0F0F3030305858586161
            61616161616161616161616161616161616161FFFFFF606060606060000000FF
            FFFF00C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7070FFAF3F8FFF5050FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C0FFFFFFFF000000FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
            C0FFFFFFFF0000000000FFFFFF0000005D5D5D4C4C4C0F0F0F4646465C5C5C25
            25251C1C1C595959626262626262626262626262626262626262626262626262
            626262FFFFFF616161616161000000FFFFFF00C0FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFF7070FFE8E0F7FF5858FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
            C0FFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C0FFFFFFFF0000000000FFFFFF000000
            5D5D5D5E5E5E5A5A5A4646460F0F0F3D3D3D6262626262626262626262626262
            62626262626262626262626262626262626262FFFFFF616161616161000000FF
            FFFF30CCFF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF
            00C0FF00C0FF00C0FF00C0FF00C0FF30CCFFFFFFFF0000000000FF0000FF0000
            FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF4040FF00
            C0FFFFFFFF0000000000FFFFFF0000005D5D5D5B5B5B2424241B1B1B5A5A5A63
            6363636363636363636363636363636363636363636363636363636363636363
            636363FFFFFF626262626262000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF00000000C0FF00C0FF00C0FF00C0FF00AEFFC70438309CCF00C0FF
            00C0FF00C0FF00C0FF00C0FF00C0FF00C0FFFFFFFF0000000000FFFFFF000000
            5D5D5D5252523F3F3F6161616363636363636363636363636363636363636363
            63636363636363636363636363636363636363FFFFFF626262626262000000FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF1010100000000000000000000000000000000F0F0FFFFFFFFFFFFFFFFF
            FFFFFFFF7070FFAF3F8FFF5050FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
            C0FFFFFFFF0000000000FFFFFF0000005D5D5D5E5E5E60606061616163636364
            6464646464646464646464646464646464646464646464646464646464646464
            646464FFFFFF636363636363000000FFFFFF40D0FF00C0FF00C0FF00C0FF00C0
            FF00C0FF00C0FF00C0FF00C0FF40D0FFFFFFFF000000FFFFFFFFFFFFFFFFFFCF
            CFCF1C2122E0E0E0FFFFFFFFFFFFFFFFFFFFFFFF7070FFE8E0F7FF5858FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C0FFFFFFFF0000000000FFFFFF000000
            5D5D5D5E5E5E6060606161616363636464646565656565656565656565656565
            65656565656565656565656565656565656565FFFFFF636363636363000000FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF000000FFFFFFFFFFFFCFCFCF22222200A9E000C0FF00C0FF00C0FF00C0
            FF00C0FF00AEFFC70438309CCF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00
            C0FFFFFFFF0000000000FFFFFF0000005D5D5D5E5E5E60606061616163636364
            6464656565656565656565656565656565656565656565656565656565656565
            656565FFFFFF646464646464000000FFFFFF40D0FF00C0FF00C0FF00C0FF00C0
            FF00C0FF00C0FF00C0FF00C0FF40D0FFFFFFFF000000FFFFFFCFCFCF1C1C1CE0
            E0E000C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7070FFAF3F8FFF5050FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C0FFFFFFFF0000000000FFFFFF000000
            5D5D5D5E5E5E6060606161616363636464645F5F5F3333334C4C4C6666666666
            66666666666666666666666666666666666666FFFFFF646464646464000000FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF000000CFCFCF1C1C1C0000000000000000000000000000000000000000
            000000000000000F0F10FF5858FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
            C0FFFFFFFF0000000000FFFFFF0000005D5D5D5E5E5E60606061616163636364
            64641919192E2E2E131313565656666666666666666666666666666666666666
            666666FFFFFF656565656565000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000001C212200A9E000C0FF00
            C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF10C4FF00000000C0FF00C0FF
            00C0FF00C0FF00C0FF00C0FF00C0FF00C0FFFFFFFF0000000000FFFFFF000000
            5D5D5D5E5E5E6060606161616363634141412424246666662525254040406767
            67676767676767676767676767676767676767FFFFFF6565656565651A1A1A00
            0000000000000000000000000000000000000000000000000000000000000000
            000000171717E0E0E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFF00C0FF000000FFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
            C0FFFFFFFF0000000000FFFFFF0000005D5D5D5E5E5E60606061616163636345
            45452222225C5C5C0F0F0F5A5A5A676767676767676767676767676767676767
            676767FFFFFF6666666666666666666666666666666666666666666666666666
            6600000000C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C0FF000000FF5050FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C0FFFFFFFF0000000000FFFFFF000000
            5D5D5D5E5E5E6060606161616363636464642626260909094B4B4B6868686868
            68686868686868686868686868686868686868FFFFFF66666666666666666666
            666666666666666666666666666666666600000000C0FF4040FF0000FF0000FF
            0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
            FF4040FF00C0FF000000FF5858FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
            C0FFFFFFFF0000000000FFFFFF0000005D5D5D5E5E5E60606061616163636364
            6464656565676767686868696969696969696969696969696969696969696969
            696969FFFFFF6767676767676767676767676767676767676767676767676767
            6700000000C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF0018FFC72A3830
            9CCF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00000000C0FF00C0FF
            00C0FF00C0FF00C0FF00C0FF00C0FF30CCFFFFFFFF0000000000FFFFFF000000
            5D5D5D5E5E5E6060606161616363636464646565656767676868686969696969
            69696969696969696969696969696969696969FFFFFF68686868686868686868
            686868686868686868686868686868686800000000C0FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFF0000FFFF8F8FFF5050FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFF00C0FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF0000000000FFFFFF0000005D5D5D5E5E5E60606061616163636364
            64646565656767676868686A6A6A6A6A6A6A6A6A6A6A6A9494941414146A6A6A
            6A6A6AFFFFFF6868686868686868686868686868686868686868686868686868
            6800000000C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6060FFFFF7F7FF
            5858FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C0FF000000FFFFFFFFFFFF
            FFFFFF1010100000000000000000000000000000000F0F0F0000FFFFFF000000
            5D5D5D5E5E5E6060606161616363636464646565656767676868686A6A6A7878
            78B0B0B0ECECECD4D4D40000006A6A6A6A6A6AFFFFFF68686868686868686868
            686868686868686868686868686868686800000000C0FF00C0FF00C0FF00C0FF
            00C0FF00C0FF00C0FF0018FFC72A38309CCF00C0FF00C0FF00C0FF00C0FF00C0
            FF00C0FF00C0FF00000000C0FF40D0FFFFFFFF000000FFFFFFFFFFFFFFFFFFCF
            CFCF222222E0E0E00000FFFFFF0000005D5D5D5E5E5E60606061616163636364
            6464656565676767939393CCCCCCF1F1F1B8B8B87B7B7B6565650000006B6B6B
            6B6B6BFFFFFF6969696969696969696969696969696969696969696969696969
            6900000000C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF8F8FFF
            5050FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C0FF000000FFFFFFFFFFFF
            FFFFFF000000FFFFFFFFFFFFCFCFCF222222E0E0E0FFFFFF0000FFFFFF000000
            5D5D5D5E5E5E6060606161616363636464647D7D7DDEDEDED5D5D59E9E9E6B6B
            6B6464645E5E5E4E4E4E0000006B6B6B6B6B6BFFFFFF6A6A6A6A6A6A6A6A6A6A
            6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00000000C0FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFF6060FFFFF7F7FF5858FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFF00C0FF00000000C0FF40D0FFFFFFFF000000FFFFFFCFCFCF222222E0
            E0E0FFFFFFFFFFFF0000FFFFFF0000005D5D5D5E5E5E606060616161686868AD
            ADADE3E3E38C8C8C6868686161615959593C3C3C1717171111112929296C6C6C
            6C6C6CFFFFFF6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A
            6A00000000C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00
            C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF000000FFFFFFFFFFFF
            FFFFFF000000CFCFCF222222E0E0E0FFFFFFFFFFFFFFFFFF0000FFFFFF000000
            5D5D5D5E5E5E606060848484E2E2E2B7B7B76D6D6D5E5E5E5050502A2A2A0A0A
            0A2929295555556D6D6D6D6D6D6D6D6D6D6D6DFFFFFF6A6A6A6A6A6A6A6A6A6A
            6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A6A00000000C0FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFF2020FFFF3838FFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFF00C0FF000000FFFFFFFFFFFFFFFFFF000000222222E0E0E0FFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFF0000005D5D5D636363B4B4B4DDDDDD7C7C7C61
            61615454542727271010103F3F3F6464646D6D6D6D6D6D6D6D6D6D6D6D6D6D6D
            6D6D6DFFFFFF6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B
            6B00000000C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF8F8FFF
            5050FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00C0FF000000000000000000
            000000171717E0E0E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF000000
            5D5D5D777777FFFFFF6262625E5E5E3B3B3B1010104040406868686A6A6A6B6B
            6B6666664141411313130D0D0D6464646E6E6EFFFFFF6C6C6C6C6C6C6C6C6C6C
            6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C00000000C0FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFF6060FFFFF7F7FF5858FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFF00C0FF0000005B5B5B000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFF0000005D5D5D5E5E5EFFFFFF5353531C1C1C26
            2626757575B3B3B3B4B4B48484842424240909090E0E0E1111111A1A1A676767
            6E6E6EFFFFFF6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C
            6C00000030CCFF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00
            C0FF00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF30CCFF0000005B5B5B000000
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF000000
            5D5D5D5E5E5E8787870F0F0F474747ADADADE7E7E7B4B4B4E8E8E87878780E0E
            0E0909091A1A1A4545456868686F6F6F6F6F6FFFFFFF6C6C6C6C6C6C6C6C6C6C
            6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C000000FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF0000005B5B5B000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFF0000005D5D5D4C4C4C5C5C5C5B5B5B636363EA
            EAEAA3A3A3B7B7B7B2B2B21414143535355F5F5F6F6F6F6F6F6F6F6F6F6F6F6F
            6F6F6FFFFFFF6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D
            6D000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF1010100000000000000000000000000000000A0A0A5B5B5B000000
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF000000
            5D5D5D5E5E5E6060606161613E3E3E303030A2A2A28787875656566A6A6A6B6B
            6B6D6D6D6F6F6F707070707070707070707070FFFFFF6E6E6E6E6E6E6E6E6E6E
            6E6E6E6E6E6E6E6E6E6E6E6E6E6E6D6D6D00000040D0FF00C0FF00C0FF00C0FF
            00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF40D0FF000000FFFFFFFFFFFFFFFF
            FFCFCFCF1E1E1E5050505B5B5B000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFF0000005D5D5D5E5E5E5757571B1B1B09090909
            09093F3F3F6767676868686A6A6A6B6B6B6D6D6D6F6F6F7070708C8C8CAFAFAF
            D2D2D2E4E4E4C8C8C87C7C7C6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6D6D
            6D000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF000000FFFFFFFFFFFFCFCFCF1E1E1E5151515B5B5B5B5B5B000000
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF000000
            5D5D5D4646460909090808081B1B1B5B5B5B6565656767676868686A6A6A6B6B
            6B929292CDCDCDF2F2F2FAFAFAE4E4E4BEBEBE7D7D7DC9C9C9EDEDEDC0C0C078
            78786F6F6F6F6F6F6F6F6F6E6E6E6D6D6D00000040D0FF00C0FF00C0FF00C0FF
            00C0FF00C0FF00C0FF00C0FF00C0FF00C0FF40D0FF000000FFFFFFCFCFCF1E1E
            1E5252525C5C5C5B5B5B5B5B5B000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFF0000005D5D5D1414140909093C3C3C63636364
            6464656565676767686868A2A2A2E3E3E3F6F6F6C3C3C38D8D8D747474747474
            7474747474747474747C7C7CD3D3D3EDEDEDB7B7B77474746F6F6F6E6E6E6D6D
            6D000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF000000CFCFCF1E1E1E5252525D5D5D5C5C5C5B5B5B5B5B5B000000
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF000000
            5D5D5D555555575757616161636363646464656565AFAFAFF1F1F1EBEBEBA2A2
            A273737373737373737373737373737373737373737373737373737373737384
            8484DCDCDCEDEDEDAEAEAE6E6E6E6D6D6D000000FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000001E1E1E5353535E5E
            5E5D5D5D5C5C5C5B5B5B5B5B5B000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFF0000005D5D5D5E5E5E6060606161616363637C
            7C7CDDDDDDE0E0E08B8B8B727272727272727272727272727272727272727272
            7272727272727272727272727272727272727272728B8B8BE4E4E4EDEDED9F9F
            9F1B1B1B00000000000000000000000000000000000000000000000000000000
            00000000001010105454545F5F5F5E5E5E5D5D5D5C5C5C5B5B5B5B5B5B000000
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF000000
            5D5D5D5E5E5E606060616161ACACACF9F9F9A1A1A17171717171717171717171
            7171717171717171717171717171717171717171717171717171717171717171
            7171717171717171717171939393EEEEEEE3E3E39595956A6A6A6A6A6A696969
            6868686767676666666565656464646363636262626161616060605F5F5F5E5E
            5E5D5D5D5C5C5C5B5B5B5B5B5B000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFF0000005D5D5D5E5E5E747474DCDCDCDBDBDB74
            7474707070707070707070707070707070707070707070707070707070707070
            7070707070707070707070707070707070707070707070707070707070707070
            70A1A1A1F4F4F4DADADA8B8B8B69696968686867676766666665656564646463
            63636262626161616060605F5F5F5E5E5E5D5D5D5C5C5C5B5B5B5B5B5B000000
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF000000
            5D5D5D7C7C7CD7D7D7A1A1A16F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F
            6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F
            6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6FAEAEAEF8F8F8D0D0D0
            8080806767676666666565656464646363636262626161616060605F5F5F5E5E
            5E5D5D5D5C5C5C5B5B5B5B5B5B000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFF0000007B7B7BF6F6F68888886E6E6E6E6E6E6E
            6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E
            6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E
            6E6E6E6E6E6E6E6E6E6E6E6E6EBABABAE7E7E7C6C6C674747465656564646463
            63636262626161616060605F5F5F5E5E5E5D5D5D5C5C5C5B5B5B5B5B5B000000
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF000000
            F3F3F37F7F7F6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D
            6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D
            6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D
            6D6D6DC6C6C6ECECECBBBBBB6E6E6E6363636262626161616060605F5F5F5E5E
            5E5D5D5D5C5C5C5B5B5B5B5B5B000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFF3030305353536F6F6F6C6C6C6C6C6C6C6C6C6C
            6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C
            6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C
            6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C757575D1D1D1ECECECB1
            B1B16767676161616060605F5F5F5E5E5E5D5D5D5C5C5C5B5B5B5B5B5B000000
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            9797971E1E1E5A5A5A6C6C6C6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B
            6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B
            6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B6B
            6B6B6B6B6B6B6B6B6B6B6B6B7D7D7DDADADAEBEBEBA6A6A66060605F5F5F5E5E
            5E5D5D5D5C5C5C5B5B5B5B5B5B000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFF7F7F77F7F7F1E1E1E62626269
            6969696969696969696969696969696969696969696969696969696969696969
            6969696969696969696969696969696969696969696969696969696969696969
            6969696969696969696969696969696969696969696969696969696969696969
            6969848484E2E2E2EBEBEB9696965E5E5E5D5D5D5C5C5C5B5B5B5B5B5B000000
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFE7E7E76868682424246969696868686868686868686868
            6868686868686868686868686868686868686868686868686868686868686868
            6868686868686868686868686868686868686868686868686868686868686868
            6868686868686868686868686868686868686868686868688C8C8CEDEDEDE1E1
            E18B8B8B5C5C5C5B5B5B5B5B5B000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7
            D7D74848483636366B6B6B676767676767676767676767676767676767676767
            6767676767676767676767676767676767676767676767676767676767676767
            6767676767676767676767676767676767676767676767676767676767676767
            67676767676767676767676767679B9B9BF3F3F3D6D6D67F7F7F5B5B5B000000
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF3030304646466969
            6966666666666666666666666666666666666666666666666666666666666666
            6666666666666666666666666666666666666666666666666666666666666666
            6666666666666666666666666666666666666666666666666666666666666666
            66666666A9A9A9F8F8F8CCCCCC000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFA7A7A71E1E1E565656686868656565656565656565
            6565656565656565656565656565656565656565656565656565656565656565
            6565656565656565656565656565656565656565656565656565656565656565
            6565656565656565656565656565656565656565656565686868666666303030
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7
            F78787871E1E1E5E5E5E64646464646464646464646464646464646464646464
            6464646464646464646464646464646464646464646464646464646464646464
            6464646464646464646464646464646464646464646464646464646464646464
            646767675959591E1E1E8F8F8FF7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF7070701D1D1D666666
            6363636363636363636363636363636363636363636363636363636363636363
            6363636363636363636363636363636363636363636363636363636363636363
            63636363636363636363636565655B5B5B1D1D1D7F7F7FF7F7F7FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFDFDFDF50505031313167676762626262626262626262
            6262626262626262626262626262626262626262626262626262626262626262
            6262626262626262626262626262626262626262626262625E5E5E1D1D1D7878
            78EFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            C7C7C73838384040406666666161616161616161616161616161616161616161
            6161616161616161616161616161616161616161616161616161616161616161
            61616161611D1D1D707070E7E7E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAF1D1D1D51515163
            6363606060606060606060606060606060606060606060606060606060606060
            6060606060606060606060606464642A2A2A606060DFDFDFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF8F8F8F1D1D1D5A5A5A6161615F5F5F5F5F5F5F5F
            5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F656565303030505050CF
            CFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEF
            EFEF7878781D1D1D6262625E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E
            656565363636404040C7C7C7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E75858582A2A2A6464
            645D5D5D5D5D5D5D5D5D636363404040303030BFBFBFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFCFCFCF4040403B3B3B636363494949282828AFAFAF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFB7B7B74848489F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000}
          Layout = blGlyphTop
          ParentFont = False
          Spacing = 0
          TabOrder = 0
          WordWrap = True
          OnClick = ArchButtonClick
        end
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 72
    Top = 288
  end
  object OpenConnectionTimer: TTimer
    Enabled = False
    OnTimer = OpenConnectionTimerTimer
    Left = 72
    Top = 360
  end
  object ChangeModeTimer: TTimer
    Interval = 300
    OnTimer = ChangeModeTimerTimer
    Left = 176
    Top = 288
  end
  object OpenDialog: TOpenDialog
    Left = 176
    Top = 360
  end
end
