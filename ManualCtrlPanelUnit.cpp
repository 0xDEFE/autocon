//---------------------------------------------------------------------------

#include <vcl.h>
#include "ScanScriptParser.h"
#pragma hdrstop

#include "ManualCtrlPanelUnit.h"
//#include "MainUnit.h"

#include "Utils.h"
//helpviewer
#include "Vcl.HTMLHelpViewer.hpp"
#pragma package(smart_init)
#pragma link "Vcl.HTMLHelpViewer"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TManualCtrlPanel *ManualCtrlPanel = NULL;
//---------------------------------------------------------------------------
__fastcall TManualCtrlPanel::TManualCtrlPanel(TComponent* Owner)
	: TFrame(Owner)
{
	curChGroup = 0;
}
//---------------------------------------------------------------------------

TForm* TManualCtrlPanel::FormDial = NULL;
TForm* TManualCtrlPanel::HelpDial = NULL;

void TManualCtrlPanel::ShowHelpDial()
{
     if(!HelpDial)
	{
		HelpDial = new TForm((TComponent*)0);
		HelpDial->Width = ManualCtrlPanel->Width + 10;
		HelpDial->Height = ManualCtrlPanel->Height + 10;
		HelpDial->FormStyle = fsStayOnTop;
        HelpDial->BorderStyle = bsSizeToolWin;
	}
}

void TManualCtrlPanel::DialogShow()
{
    if(!ManualCtrlPanel)
	{
		ManualCtrlPanel = new TManualCtrlPanel(NULL);
	}

	if(!FormDial)
	{
		FormDial = new TForm((TComponent*)0);
		FormDial->Width = ManualCtrlPanel->Width + 10;
		FormDial->Height = ManualCtrlPanel->Height + 10;
		//FormDial->BorderStyle = bsToolWindow;
		//FormDial->BorderIcons = TBorderIcons();
		FormDial->FormStyle = fsStayOnTop;
        FormDial->BorderStyle = bsSizeToolWin;
        FormDial->OnShow = &(ManualCtrlPanel->OnFormShow);
        FormDial->OnHide = &(ManualCtrlPanel->OnFormHide);

        FormDial->InsertControl(ManualCtrlPanel);
        ManualCtrlPanel->Align = alClient;
        ManualCtrlPanel->Show();
	}
	FormDial->Show();
};

void TManualCtrlPanel::DialogHide()
{
    FormDial->Hide();
}

void __fastcall TManualCtrlPanel::OnFormShow(TObject *Sender)
{
    //AutoconMain->ac->SetManualMode(true);
	ManualCtrlPanel->m_UpdateCaretsPosBtnClick(NULL);
}
void __fastcall TManualCtrlPanel::OnFormHide(TObject *Sender)
{
    //AutoconMain->ac->SetManualMode(false);
    AutoconMain->ac->SetMode(acReady);

    ManualCtrlPanel->m_ChangeModeTimer->Enabled = true;
}

void __fastcall TManualCtrlPanel::m_ChangeModeTimerTimer(TObject *Sender)
{
    if ((AutoconMain) && (AutoconMain->ac))
	{
		if (AutoconMain->ac->GetMode() == acReady)
		{
            m_ChangeModeTimer->Enabled = false;
			if(FormDial)
            {
                //FormDial->RemoveControl(ManualCtrlPanel);
                FormDial->Hide();
            }
		}
	}
}
//---------------------------------------------------------------------------


void __fastcall TManualCtrlPanel::m_ExecBtnClick(TObject *Sender)
{
	cScanScriptProgramm* pPrg = AutoconMain->ac->LockManualMode();

	cScanScriptCmdBuffer& commandBuf = pPrg->getCode();

	bool bNoScan = false;
	if(curChGroup)
		commandBuf.pushCommand(SSC_SETSG,1,curChGroup);
	else
	{
		bNoScan = true;
		commandBuf.pushCommand(SSC_SCAN,1,0);
	}

	if(!bNoScan)
	{
    	commandBuf.pushCommand(SSC_SCAN,1,1);
    }

	if(m_CaretsHomeBtn->Down)
		commandBuf.pushCommand(SSC_MOVTOBASE,0);
	else
	{
		int topOffset = 0;
		int botOffset = 0;


		if(m_MoveTopCaretToLeftBtn->Down)
			topOffset = -abs(m_TopCaretOffset->Value);
		else if(m_MoveTopCaretToRightBtn->Down)
        	topOffset = abs(m_TopCaretOffset->Value);

		if(m_MoveBotCaretToLeftBtn->Down)
			botOffset = -abs(m_BotCaretOffset->Value);
		else if(m_MoveBotCaretToRightBtn->Down)
			botOffset = abs(m_BotCaretOffset->Value);

        if(topOffset || botOffset)
    		commandBuf.pushCommand(SSC_MOVCARET,2,topOffset,botOffset);
	}

	AutoconMain->ac->UnlockManualMode();

	m_UpdateCaretsPosBtnClick(NULL);
}
//---------------------------------------------------------------------------


void __fastcall TManualCtrlPanel::m_MoveTopCaretToLeftBtnClick(TObject *Sender)
{
	if( m_CaretsHomeBtn->Down )
		m_CaretsHomeBtn->Down = false;

	m_TopCaretOffset->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TManualCtrlPanel::m_MoveBotCaretToLeftBtnClick(TObject *Sender)
{
	if( m_CaretsHomeBtn->Down )
		m_CaretsHomeBtn->Down = false;

	m_BotCaretOffset->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TManualCtrlPanel::m_CaretsHomeBtnClick(TObject *Sender)
{
	m_MoveTopCaretToLeftBtn->Down = false;
	m_MoveBotCaretToLeftBtn->Down = false;
	m_MoveBotCaretToRightBtn->Down = false;
	m_MoveTopCaretToRightBtn->Down = false;
}
//---------------------------------------------------------------------------

void __fastcall TManualCtrlPanel::Timer1Timer(TObject *Sender)
{
	UpdateLabels();
}
//---------------------------------------------------------------------------
void __fastcall TManualCtrlPanel::m_TuneBtnClick(TObject *Sender)
{
	if(curChGroup)
		TuneChannelGroup(curChGroup);
	else
		TuneChannels();

	UpdateLabels();
}
//---------------------------------------------------------------------------

void __fastcall TManualCtrlPanel::m_ScanGroup3_BtnClick(TObject *Sender)
{
	if(m_ScanGroup3_Btn->Down)
		curChGroup = 3;
	else if(m_ScanGroup5_Btn->Down)
		curChGroup = 5;
	else if(m_ScanGroup6_Btn->Down)
		curChGroup = 6;
	else if(m_ScanGroup8_Btn->Down)
		curChGroup = 8;
	else if(m_ScanGroup10_Btn->Down)
		curChGroup = 10;
	else if(m_ScanGroup12_Btn->Down)
		curChGroup = 12;
	else if(m_ScanGroup14_Btn->Down)
		curChGroup = 14;
	else
	{
		curChGroup = 0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TManualCtrlPanel::m_TuneClearBtnClick(TObject *Sender)
{
	AutoconMain->Rep->ClearChannelMaximumsData();
	UpdateLabels();
}
//---------------------------------------------------------------------------

void TManualCtrlPanel::UpdateLabels()
{
	/*m_TuneMaxCountLabel->Caption = StringFormatU(L"Tune maximums: %d",
											AutoconMain->Rep->GetChannelMaximumAliveCount());*/

//	m_XSysCoordLabel->Caption = StringFormatU(L"XSysCrd: %d, %d",
//											AutoconMain->XSysCrd_Stored[0],AutoconMain->XSysCrd_Stored[1]);
//	m_XSysDirLabel->Caption = StringFormatU(L"XSysDir: %d, %d",
//											AutoconMain->XSysDir_Stored[0],AutoconMain->XSysDir_Stored[1]);
//	m_XSysCrdTrLabel->Caption = StringFormatU(L"XSysCrdTr: %d, %d",
//											AutoconMain->XSysCrd_Transformed[0],AutoconMain->XSysCrd_Transformed[1]);
}


void __fastcall TManualCtrlPanel::m_ResetPathEncoderBtnClick(TObject *Sender)
{
	AutoconMain->DEV->ResetPathEncoder();
}
//---------------------------------------------------------------------------


void __fastcall TManualCtrlPanel::m_UpdateCaretsPosBtnClick(TObject *Sender)
{
	bool bCaretActive[2];
	int iCaretPos[2];

	AutoconMain->ac->GetCaretStatus(false, bCaretActive,iCaretPos);
	AutoconMain->ac->GetCaretStatus(true, bCaretActive+1,iCaretPos+1);

	const float maxRange = 2000;

	m_TopCaretTrack->Position = float(iCaretPos[0])*100.f/maxRange;
	m_BotCaretTrack->Position = float(iCaretPos[1])*100.f/maxRange;

	m_TopCaretLabel->Caption = StringFormatU(L"Top: %d", iCaretPos[0]);
	m_BotCaretLabel->Caption = StringFormatU(L"Bot: %d", iCaretPos[1]);

	UpdateLabels();
}
//---------------------------------------------------------------------------


void __fastcall TManualCtrlPanel::m_ClearBScanBtnClick(TObject *Sender)
{
     //MainForm->Clear_Data();
}
//---------------------------------------------------------------------------

void __fastcall TManualCtrlPanel::m_HelpBtnClick(TObject *Sender)
{
    PathMgr::PushPath(L"$help", L"resources\\help");
    UnicodeString help_path = PathMgr::GetPath(L"($help)\\AutoconHelp.chm");

    Application->HelpFile = help_path;
    Application->HelpContext(7);
}
//---------------------------------------------------------------------------

void __fastcall TManualCtrlPanel::m_TopSpinPanelResize(TObject *Sender)
{
    m_TopCaretOffset->Left = 4;
    m_TopCaretOffset->Top = 4;
    m_TopCaretOffset->Width = m_TopSpinPanel->Width - 8;
    m_TopCaretOffset->Height = m_TopSpinPanel->Height - 8;
    m_TopCaretOffset->Font->Height = - (m_TopSpinPanel->Height - 18);
}
//---------------------------------------------------------------------------

void __fastcall TManualCtrlPanel::m_BotSpinPanelResize(TObject *Sender)
{
    m_BotCaretOffset->Left = 4;
    m_BotCaretOffset->Top = 4;
    m_BotCaretOffset->Width = m_BotSpinPanel->Width - 8;
    m_BotCaretOffset->Height = m_BotSpinPanel->Height - 8;
    m_BotCaretOffset->Font->Height = - (m_BotSpinPanel->Height - 18);
}
//---------------------------------------------------------------------------
