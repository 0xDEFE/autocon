inherited ManualTuneFrame: TManualTuneFrame
  Width = 1229
  Height = 908
  Font.Height = -22
  Font.Name = 'Verdana'
  ExplicitWidth = 1229
  ExplicitHeight = 908
  object m_MainToolBar: TToolBar
    Left = 0
    Top = 0
    Width = 1229
    Height = 38
    AutoSize = True
    ButtonHeight = 38
    ButtonWidth = 260
    Caption = 'm_MainToolBar'
    DrawingStyle = dsGradient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Myriad Pro'
    Font.Style = [fsBold]
    Images = ImageList1
    List = True
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    Transparent = False
    object ShowSelectChannelPanelBtn: TToolButton
      Left = 0
      Top = 0
      AllowAllUp = True
      AutoSize = True
      Caption = #1042#1099#1073#1086#1088' '#1082#1072#1085#1072#1083#1086#1074
      Grouped = True
      ImageIndex = 0
      OnClick = ShowSelectChannelPanelBtnClick
    end
    object ToolButton6: TToolButton
      Left = 194
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ShowMovePanelBtn: TToolButton
      Left = 202
      Top = 0
      AllowAllUp = True
      AutoSize = True
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1080' '#1085#1072#1089#1090#1088#1086#1081#1082#1072
      Grouped = True
      ImageIndex = 1
      OnClick = ShowMovePanelBtnClick
    end
    object ToolButton2: TToolButton
      Left = 466
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 3
      Style = tbsSeparator
    end
    object ClearBtn: TToolButton
      Left = 474
      Top = 0
      AllowAllUp = True
      AutoSize = True
      Caption = #1054#1095#1080#1089#1090#1082#1072
      Grouped = True
      ImageIndex = 3
      OnClick = ClearBtnClick
    end
    object ToolButton9: TToolButton
      Left = 598
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object CloseBtn: TToolButton
      Left = 606
      Top = 0
      AutoSize = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 2
      OnClick = CloseBtnClick
    end
    object ToolButton3: TToolButton
      Left = 734
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 3
      Style = tbsSeparator
    end
  end
  object Panel2: TPanel
    Left = 361
    Top = 38
    Width = 868
    Height = 870
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object GridPanel3: TGridPanel
      Left = 0
      Top = 826
      Width = 868
      Height = 44
      Align = alBottom
      BevelOuter = bvNone
      ColumnCollection = <
        item
          SizeStyle = ssAbsolute
          Value = 140.000000000000000000
        end
        item
          SizeStyle = ssAbsolute
          Value = 120.000000000000000000
        end
        item
          Value = 100.000000000000000000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = m_UpdateCaretsPosBtn
          Row = 0
        end
        item
          Column = 1
          Control = GridPanel4
          Row = 0
        end
        item
          Column = 2
          Control = GridPanel5
          Row = 0
        end>
      RowCollection = <
        item
          Value = 100.000000000000000000
        end>
      TabOrder = 0
      Visible = False
      object m_UpdateCaretsPosBtn: TSpeedButton
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 134
        Height = 38
        Align = alClient
        Caption = #1054#1073#1085#1086#1074#1080#1090#1100
        OnClick = m_UpdateCaretsPosBtnClick
        ExplicitLeft = 40
        ExplicitTop = 32
        ExplicitWidth = 23
        ExplicitHeight = 22
      end
      object GridPanel4: TGridPanel
        Left = 140
        Top = 0
        Width = 120
        Height = 44
        Align = alClient
        BevelOuter = bvNone
        ColumnCollection = <
          item
            Value = 100.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_TopCaretLabel
            Row = 0
          end
          item
            Column = 0
            Control = m_BotCaretLabel
            Row = 1
          end>
        RowCollection = <
          item
            Value = 50.000000000000000000
          end
          item
            Value = 50.000000000000000000
          end>
        TabOrder = 0
        object m_TopCaretLabel: TStaticText
          Left = 0
          Top = 0
          Width = 60
          Height = 24
          Align = alClient
          Alignment = taCenter
          Anchors = []
          Caption = 'Top: ???'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object m_BotCaretLabel: TStaticText
          Left = 0
          Top = 22
          Width = 57
          Height = 24
          Align = alClient
          Alignment = taCenter
          Anchors = []
          Caption = 'Bot: ???'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object GridPanel5: TGridPanel
        Left = 260
        Top = 0
        Width = 608
        Height = 44
        Align = alClient
        BevelOuter = bvNone
        ColumnCollection = <
          item
            Value = 100.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_TopCaretTrack
            Row = 0
          end
          item
            Column = 0
            Control = m_BotCaretTrack
            Row = 1
          end>
        RowCollection = <
          item
            Value = 50.000000000000000000
          end
          item
            Value = 50.000000000000000000
          end>
        TabOrder = 1
        object m_TopCaretTrack: TTrackBar
          Left = 0
          Top = 0
          Width = 608
          Height = 22
          Align = alClient
          Anchors = []
          Ctl3D = True
          Max = 100
          ParentCtl3D = False
          ShowSelRange = False
          TabOrder = 0
          ThumbLength = 10
          TickMarks = tmTopLeft
        end
        object m_BotCaretTrack: TTrackBar
          Left = 0
          Top = 22
          Width = 608
          Height = 22
          Align = alClient
          Anchors = []
          Ctl3D = True
          Max = 100
          ParentCtl3D = False
          ShowSelRange = False
          TabOrder = 1
          ThumbLength = 10
        end
      end
    end
    object m_FramePanel: TPanel
      Left = 0
      Top = 0
      Width = 868
      Height = 826
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  object LeftPanel: TPanel
    Left = 0
    Top = 38
    Width = 361
    Height = 870
    Align = alLeft
    BevelInner = bvRaised
    BevelOuter = bvNone
    Caption = 'LeftPanel'
    TabOrder = 2
    object PageControl1: TPageControl
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 353
      Height = 813
      ActivePage = MovePage
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object SelectChannelsPage: TTabSheet
        Caption = 'SelectChannelsPage'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label1: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 165
          Height = 23
          Align = alTop
          Caption = #1043#1088#1091#1087#1087#1072' '#1082#1072#1085#1072#1083#1086#1074':'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object ActiveChannelsLabel: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 235
          Width = 182
          Height = 23
          Align = alTop
          Caption = #1040#1082#1090#1080#1074#1085#1099#1077' '#1082#1072#1085#1072#1083#1099':'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object GridPanel1: TGridPanel
          AlignWithMargins = True
          Left = 3
          Top = 32
          Width = 339
          Height = 197
          Align = alTop
          BevelOuter = bvNone
          ColumnCollection = <
            item
              Value = 33.333333333333330000
            end
            item
              Value = 33.333333333333330000
            end
            item
              Value = 33.333333333333330000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = ScanGroupBtn3
              Row = 0
            end
            item
              Column = 1
              Control = ScanGroupBtn5
              Row = 0
            end
            item
              Column = 2
              Control = ScanGroupBtn6
              Row = 0
            end
            item
              Column = 0
              Control = ScanGroupBtn8
              Row = 1
            end
            item
              Column = 1
              Control = ScanGroupBtn10
              Row = 1
            end
            item
              Column = 2
              Control = ScanGroupBtn12
              Row = 1
            end
            item
              Column = 0
              Control = ScanGroupBtn14
              Row = 2
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -22
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          RowCollection = <
            item
              Value = 33.333333333333330000
            end
            item
              Value = 33.333333333333330000
            end
            item
              Value = 33.333333333333330000
            end>
          TabOrder = 0
          object ScanGroupBtn3: TSpeedButton
            Tag = 3
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 106
            Height = 59
            Align = alClient
            GroupIndex = 10
            Caption = #1043#1088#1091#1087#1087#1072' 3'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = ScanGroupBtnClick
            ExplicitLeft = 40
            ExplicitTop = 104
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ScanGroupBtn5: TSpeedButton
            Tag = 5
            AlignWithMargins = True
            Left = 115
            Top = 3
            Width = 106
            Height = 59
            Align = alClient
            GroupIndex = 10
            Caption = #1043#1088#1091#1087#1087#1072' 5'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = ScanGroupBtnClick
            ExplicitLeft = 91
            ExplicitTop = 11
            ExplicitWidth = 74
          end
          object ScanGroupBtn6: TSpeedButton
            Tag = 6
            AlignWithMargins = True
            Left = 227
            Top = 3
            Width = 109
            Height = 59
            Align = alClient
            GroupIndex = 10
            Caption = #1043#1088#1091#1087#1087#1072' 6'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = ScanGroupBtnClick
            ExplicitLeft = 166
            ExplicitTop = 11
            ExplicitWidth = 76
          end
          object ScanGroupBtn8: TSpeedButton
            Tag = 8
            AlignWithMargins = True
            Left = 3
            Top = 68
            Width = 106
            Height = 59
            Align = alClient
            GroupIndex = 10
            Caption = #1043#1088#1091#1087#1087#1072' 8'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = ScanGroupBtnClick
            ExplicitLeft = 40
            ExplicitTop = 104
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ScanGroupBtn10: TSpeedButton
            Tag = 10
            AlignWithMargins = True
            Left = 115
            Top = 68
            Width = 106
            Height = 59
            Align = alClient
            GroupIndex = 10
            Caption = #1043#1088#1091#1087#1087#1072' 10'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = ScanGroupBtnClick
            ExplicitLeft = 40
            ExplicitTop = 104
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ScanGroupBtn12: TSpeedButton
            Tag = 12
            AlignWithMargins = True
            Left = 227
            Top = 68
            Width = 109
            Height = 59
            Align = alClient
            GroupIndex = 10
            Caption = #1043#1088#1091#1087#1087#1072' 12'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = ScanGroupBtnClick
            ExplicitLeft = 40
            ExplicitTop = 104
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ScanGroupBtn14: TSpeedButton
            Tag = 14
            AlignWithMargins = True
            Left = 3
            Top = 133
            Width = 106
            Height = 61
            Align = alClient
            GroupIndex = 10
            Caption = #1043#1088#1091#1087#1087#1072' 14'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = ScanGroupBtnClick
            ExplicitLeft = 40
            ExplicitTop = 104
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
        end
        object GridPanel2: TGridPanel
          AlignWithMargins = True
          Left = 3
          Top = 264
          Width = 339
          Height = 505
          Align = alClient
          BevelOuter = bvNone
          ColumnCollection = <
            item
              Value = 33.333777832440670000
            end
            item
              Value = 33.333198142604150000
            end
            item
              Value = 33.333024024955190000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = ChanLeftBtn
              Row = 0
            end
            item
              Column = 1
              Control = ChanToggleBtn
              Row = 0
            end
            item
              Column = 2
              Control = ChanRightBtn
              Row = 0
            end
            item
              Column = 0
              Control = ChanBtn1
              Row = 1
            end
            item
              Column = 1
              Control = ChanBtn2
              Row = 1
            end
            item
              Column = 2
              Control = ChanBtn3
              Row = 1
            end
            item
              Column = 0
              Control = ChanBtn4
              Row = 2
            end
            item
              Column = 1
              Control = ChanBtn5
              Row = 2
            end
            item
              Column = 2
              Control = ChanBtn6
              Row = 2
            end
            item
              Column = 0
              Control = ChanBtn7
              Row = 3
            end
            item
              Column = 1
              Control = ChanBtn8
              Row = 3
            end
            item
              Column = 2
              Control = ChanBtn9
              Row = 3
            end
            item
              Column = 0
              Control = ChanBtn10
              Row = 4
            end
            item
              Column = 1
              Control = ChanBtn11
              Row = 4
            end
            item
              Column = 2
              Control = ChanBtn12
              Row = 4
            end
            item
              Column = 0
              Control = ChanBtn13
              Row = 5
            end
            item
              Column = 1
              Control = ChanBtn14
              Row = 5
            end
            item
              Column = 2
              Control = ChanBtn15
              Row = 5
            end
            item
              Column = 0
              Control = ChanBtn16
              Row = 6
            end
            item
              Column = 1
              Control = ChanBtn17
              Row = 6
            end
            item
              Column = 2
              Control = ChanBtn18
              Row = 6
            end
            item
              Column = 0
              Control = ChanBtn19
              Row = 7
            end
            item
              Column = 1
              Control = ChanBtn20
              Row = 7
            end
            item
              Column = 2
              Control = ChanBtn21
              Row = 7
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -22
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          RowCollection = <
            item
              Value = 12.500000000214860000
            end
            item
              Value = 12.499999999884940000
            end
            item
              Value = 12.499999999763640000
            end
            item
              Value = 12.499999999846580000
            end
            item
              Value = 12.500000000003350000
            end
            item
              Value = 12.500000000108330000
            end
            item
              Value = 12.500000000117040000
            end
            item
              Value = 12.500000000061250000
            end>
          TabOrder = 1
          object ChanLeftBtn: TBitBtn
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 107
            Height = 57
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'Verdana'
            Font.Style = []
            Glyph.Data = {
              36080000424D3608000000000000360400002800000020000000200000000100
              0800000000000004000076040000760400000001000000010000000000000101
              0100020202000303030004040400050505000606060007070700080808000909
              09000A0A0A000B0B0B000C0C0C000D0D0D000E0E0E000F0F0F00101010001111
              1100121212001313130014141400151515001616160017171700181818001919
              19001A1A1A001B1B1B001C1C1C001D1D1D001E1E1E001F1F1F00202020002121
              2100222222002323230024242400252525002626260027272700282828002929
              29002A2A2A002B2B2B002C2C2C002D2D2D002E2E2E002F2F2F00303030003131
              3100323232003333330034343400353535003636360037373700383838003939
              39003A3A3A003B3B3B003C3C3C003D3D3D003E3E3E003F3F3F00404040004141
              4100424242004343430044444400454545004646460047474700484848004949
              49004A4A4A004B4B4B004C4C4C004D4D4D004E4E4E004F4F4F00505050005151
              5100525252005353530054545400555555005656560057575700585858005959
              59005A5A5A005B5B5B005C5C5C005D5D5D005E5E5E005F5F5F00606060006161
              6100626262006363630064646400656565006666660067676700686868006969
              69006A6A6A006B6B6B006C6C6C006D6D6D006E6E6E006F6F6F00707070007171
              7100727272007373730074747400757575007676760077777700787878007979
              79007A7A7A007B7B7B007C7C7C007D7D7D007E7E7E007F7F7F00808080008181
              8100828282008383830084848400858585008686860087878700888888008989
              89008A8A8A008B8B8B008C8C8C008D8D8D008E8E8E008F8F8F00909090009191
              9100929292009393930094949400959595009696960097979700989898009999
              99009A9A9A009B9B9B009C9C9C009D9D9D009E9E9E009F9F9F00A0A0A000A1A1
              A100A2A2A200A3A3A300A4A4A400A5A5A500A6A6A600A7A7A700A8A8A800A9A9
              A900AAAAAA00ABABAB00ACACAC00ADADAD00AEAEAE00AFAFAF00B0B0B000B1B1
              B100B2B2B200B3B3B300B4B4B400B5B5B500B6B6B600B7B7B700B8B8B800B9B9
              B900BABABA00BBBBBB00BCBCBC00BDBDBD00BEBEBE00BFBFBF00C0C0C000C1C1
              C100C2C2C200C3C3C300C4C4C400C5C5C500C6C6C600C7C7C700C8C8C800C9C9
              C900CACACA00CBCBCB00CCCCCC00CDCDCD00CECECE00CFCFCF00D0D0D000D1D1
              D100D2D2D200D3D3D300D4D4D400D5D5D500D6D6D600D7D7D700D8D8D800D9D9
              D900DADADA00DBDBDB00DCDCDC00DDDDDD00DEDEDE00DFDFDF00E0E0E000E1E1
              E100E2E2E200E3E3E300E4E4E400E5E5E500E6E6E600E7E7E700E8E8E800E9E9
              E900EAEAEA00EBEBEB00ECECEC00EDEDED00EEEEEE00EFEFEF00F0F0F000F1F1
              F100F2F2F200F3F3F300F4F4F400F5F5F500F6F6F600F7F7F700F8F8F800F9F9
              F900FAFAFA00FBFBFB00FCFCFC00FDFDFD00FEFEFE00FFFFFF00FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFEB8A0F7FFFFFFFFFFFFF39CC1FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFE6900007BFFFFFFFFFFEA2C0000D3FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFE690000006DFFFFFFFFEA2C000000C5FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFE690000001FE3FFFFFFE92C0000005BFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFE690000001FDFFFFFFFE92B0000005BFCFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFE690000001FDFFFFFFFE92B0000005BFCFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFE690000001FDFFFFFFFE82A0000005BFCFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFE690000001FDFFFFFFFE82A0000005BFCFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FE690000001FDFFFFFFFE8290000005BFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
              690000001FDFFFFFFFE7290000005BFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF69
              0000001FDFFFFFFFE9280000005BFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCB00
              00001FDFFFFFFFFF730000005BFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCB00
              00001FDFFFFFFFFF730000005BFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF69
              0000001FDFFFFFFFE9280000005BFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
              690000001FDFFFFFFFE7290000005BFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FE690000001FDFFFFFFFE8290000005BFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFE690000001FDFFFFFFFE82A0000005BFCFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFE690000001FDFFFFFFFE82A0000005BFCFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFE690000001FDFFFFFFFE92B0000005BFCFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFE690000001FDFFFFFFFE92B0000005BFCFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFE690000001FE3FFFFFFE92C0000005BFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFE690000006DFFFFFFFFEA2C000000C5FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFE6900007BFFFFFFFFFFEA2C0000D3FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFEB8A0F7FFFFFFFFFFFFF39CC1FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            Layout = blGlyphTop
            ParentFont = False
            TabOrder = 0
            WordWrap = True
            OnClick = ChanLeftBtnClick
          end
          object ChanToggleBtn: TBitBtn
            AlignWithMargins = True
            Left = 116
            Top = 3
            Width = 106
            Height = 57
            Align = alClient
            Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1074#1089#1102' '#1075#1088#1091#1087#1087#1091
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            WordWrap = True
            OnClick = ChanToggleBtnClick
          end
          object ChanRightBtn: TBitBtn
            AlignWithMargins = True
            Left = 228
            Top = 3
            Width = 108
            Height = 57
            Align = alClient
            Caption = #1057#1090#1088'. 2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'Verdana'
            Font.Style = []
            Glyph.Data = {
              36080000424D3608000000000000360400002800000020000000200000000100
              0800000000000004000076040000760400000001000000010000000000000101
              0100020202000303030004040400050505000606060007070700080808000909
              09000A0A0A000B0B0B000C0C0C000D0D0D000E0E0E000F0F0F00101010001111
              1100121212001313130014141400151515001616160017171700181818001919
              19001A1A1A001B1B1B001C1C1C001D1D1D001E1E1E001F1F1F00202020002121
              2100222222002323230024242400252525002626260027272700282828002929
              29002A2A2A002B2B2B002C2C2C002D2D2D002E2E2E002F2F2F00303030003131
              3100323232003333330034343400353535003636360037373700383838003939
              39003A3A3A003B3B3B003C3C3C003D3D3D003E3E3E003F3F3F00404040004141
              4100424242004343430044444400454545004646460047474700484848004949
              49004A4A4A004B4B4B004C4C4C004D4D4D004E4E4E004F4F4F00505050005151
              5100525252005353530054545400555555005656560057575700585858005959
              59005A5A5A005B5B5B005C5C5C005D5D5D005E5E5E005F5F5F00606060006161
              6100626262006363630064646400656565006666660067676700686868006969
              69006A6A6A006B6B6B006C6C6C006D6D6D006E6E6E006F6F6F00707070007171
              7100727272007373730074747400757575007676760077777700787878007979
              79007A7A7A007B7B7B007C7C7C007D7D7D007E7E7E007F7F7F00808080008181
              8100828282008383830084848400858585008686860087878700888888008989
              89008A8A8A008B8B8B008C8C8C008D8D8D008E8E8E008F8F8F00909090009191
              9100929292009393930094949400959595009696960097979700989898009999
              99009A9A9A009B9B9B009C9C9C009D9D9D009E9E9E009F9F9F00A0A0A000A1A1
              A100A2A2A200A3A3A300A4A4A400A5A5A500A6A6A600A7A7A700A8A8A800A9A9
              A900AAAAAA00ABABAB00ACACAC00ADADAD00AEAEAE00AFAFAF00B0B0B000B1B1
              B100B2B2B200B3B3B300B4B4B400B5B5B500B6B6B600B7B7B700B8B8B800B9B9
              B900BABABA00BBBBBB00BCBCBC00BDBDBD00BEBEBE00BFBFBF00C0C0C000C1C1
              C100C2C2C200C3C3C300C4C4C400C5C5C500C6C6C600C7C7C700C8C8C800C9C9
              C900CACACA00CBCBCB00CCCCCC00CDCDCD00CECECE00CFCFCF00D0D0D000D1D1
              D100D2D2D200D3D3D300D4D4D400D5D5D500D6D6D600D7D7D700D8D8D800D9D9
              D900DADADA00DBDBDB00DCDCDC00DDDDDD00DEDEDE00DFDFDF00E0E0E000E1E1
              E100E2E2E200E3E3E300E4E4E400E5E5E500E6E6E600E7E7E700E8E8E800E9E9
              E900EAEAEA00EBEBEB00ECECEC00EDEDED00EEEEEE00EFEFEF00F0F0F000F1F1
              F100F2F2F200F3F3F300F4F4F400F5F5F500F6F6F600F7F7F700F8F8F800F9F9
              F900FAFAFA00FBFBFB00FCFCFC00FDFDFD00FEFEFE00FFFFFF00FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1
              9CF3FFFFFFFFFFFFF7A0B8FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD300
              002CEAFFFFFFFFFF7B000069FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC500
              00002CEAFFFFFFFF6D00000069FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5B
              0000002CE9FFFFFFE31F00000069FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC
              5B0000002BE9FFFFFFDF1F00000069FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FC5B0000002BE9FFFFFFDF1F00000069FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFC5B0000002AE8FFFFFFDF1F00000069FEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFC5B0000002AE8FFFFFFDF1F00000069FEFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFC5B00000029E8FFFFFFDF1F00000069FEFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFC5B00000029E7FFFFFFDF1F00000069FEFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFC5B00000028E9FFFFFFDF1F00000069FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFC5B00000073FFFFFFFFDF1F000000CBFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFC5B00000073FFFFFFFFDF1F000000CBFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFC5B00000028E9FFFFFFDF1F00000069FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFC5B00000029E7FFFFFFDF1F00000069FEFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFC5B00000029E8FFFFFFDF1F00000069FEFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFC5B0000002AE8FFFFFFDF1F00000069FEFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFC5B0000002AE8FFFFFFDF1F00000069FEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FC5B0000002BE9FFFFFFDF1F00000069FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC
              5B0000002BE9FFFFFFDF1F00000069FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5B
              0000002CE9FFFFFFE31F00000069FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC500
              00002CEAFFFFFFFF6D00000069FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD300
              002CEAFFFFFFFFFF7B000069FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1
              9CF3FFFFFFFFFFFFF7A0B8FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            Layout = blGlyphTop
            ParentFont = False
            TabOrder = 2
            WordWrap = True
            OnClick = ChanRightBtnClick
          end
          object ChanBtn1: TSpeedButton
            AlignWithMargins = True
            Left = 3
            Top = 66
            Width = 107
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 11
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn2: TSpeedButton
            AlignWithMargins = True
            Left = 116
            Top = 66
            Width = 106
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 12
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn3: TSpeedButton
            AlignWithMargins = True
            Left = 228
            Top = 66
            Width = 108
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn4: TSpeedButton
            AlignWithMargins = True
            Left = 3
            Top = 129
            Width = 107
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn5: TSpeedButton
            AlignWithMargins = True
            Left = 116
            Top = 129
            Width = 106
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn6: TSpeedButton
            AlignWithMargins = True
            Left = 228
            Top = 129
            Width = 108
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 201
            ExplicitTop = 95
            ExplicitWidth = 94
            ExplicitHeight = 40
          end
          object ChanBtn7: TSpeedButton
            AlignWithMargins = True
            Left = 3
            Top = 192
            Width = 107
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn8: TSpeedButton
            AlignWithMargins = True
            Left = 116
            Top = 192
            Width = 106
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn9: TSpeedButton
            AlignWithMargins = True
            Left = 228
            Top = 192
            Width = 108
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 201
            ExplicitTop = 141
            ExplicitWidth = 94
            ExplicitHeight = 40
          end
          object ChanBtn10: TSpeedButton
            AlignWithMargins = True
            Left = 3
            Top = 255
            Width = 107
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn11: TSpeedButton
            AlignWithMargins = True
            Left = 116
            Top = 255
            Width = 106
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn12: TSpeedButton
            AlignWithMargins = True
            Left = 228
            Top = 255
            Width = 108
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn13: TSpeedButton
            AlignWithMargins = True
            Left = 3
            Top = 318
            Width = 107
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn14: TSpeedButton
            AlignWithMargins = True
            Left = 116
            Top = 318
            Width = 106
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn15: TSpeedButton
            AlignWithMargins = True
            Left = 228
            Top = 318
            Width = 108
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn16: TSpeedButton
            AlignWithMargins = True
            Left = 3
            Top = 381
            Width = 107
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn17: TSpeedButton
            AlignWithMargins = True
            Left = 116
            Top = 381
            Width = 106
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn18: TSpeedButton
            AlignWithMargins = True
            Left = 228
            Top = 381
            Width = 108
            Height = 57
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn19: TSpeedButton
            AlignWithMargins = True
            Left = 3
            Top = 444
            Width = 107
            Height = 58
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn20: TSpeedButton
            AlignWithMargins = True
            Left = 116
            Top = 444
            Width = 106
            Height = 58
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object ChanBtn21: TSpeedButton
            AlignWithMargins = True
            Left = 228
            Top = 444
            Width = 108
            Height = 58
            Align = alClient
            AllowAllUp = True
            GroupIndex = 13
            OnClick = ChanBtnClick
            ExplicitLeft = 56
            ExplicitTop = 392
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
        end
      end
      object MovePage: TTabSheet
        Caption = 'MovePage'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Verdana'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 345
          Height = 321
          Align = alTop
          Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1082#1072#1088#1077#1090#1086#1082
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -22
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object Label2: TLabel
            Left = 2
            Top = 28
            Width = 222
            Height = 22
            Align = alTop
            Caption = #1057#1084#1077#1097#1077#1085#1080#1077' '#1074#1077#1088#1093#1085#1077#1081' '#1082#1072#1088#1077#1090#1082#1080':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'Myriad Pro'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 2
            Top = 100
            Width = 218
            Height = 22
            Align = alTop
            Caption = #1057#1084#1077#1097#1077#1085#1080#1077' '#1085#1080#1078#1085#1077#1081' '#1082#1072#1088#1077#1090#1082#1080':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'Myriad Pro'
            Font.Style = []
            ParentFont = False
          end
          object GridPanel6: TGridPanel
            Left = 2
            Top = 122
            Width = 341
            Height = 50
            Align = alTop
            BevelOuter = bvNone
            ColumnCollection = <
              item
                Value = 50.000000279396770000
              end
              item
                SizeStyle = ssAbsolute
                Value = 100.000000000000000000
              end
              item
                Value = 49.999999720603230000
              end>
            ControlCollection = <
              item
                Column = 0
                Control = BtmCaretMoveLeftBtn
                Row = 0
              end
              item
                Column = 1
                Control = BtmCaretSpin
                Row = 0
              end
              item
                Column = 2
                Control = BtmCaretMoveRightBtn
                Row = 0
              end>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            RowCollection = <
              item
                Value = 100.000000000000000000
              end>
            TabOrder = 0
            DesignSize = (
              341
              50)
            object BtmCaretMoveLeftBtn: TSpeedButton
              Tag = 4
              AlignWithMargins = True
              Left = 3
              Top = 1
              Width = 114
              Height = 48
              Margins.Top = 1
              Margins.Bottom = 1
              Align = alClient
              AllowAllUp = True
              GroupIndex = 51
              Caption = '<<'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -28
              Font.Name = 'Myriad Pro'
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = CaretSpinChange
              ExplicitLeft = 176
              ExplicitTop = 208
              ExplicitWidth = 23
              ExplicitHeight = 22
            end
            object BtmCaretSpin: TSpinEdit
              Tag = 1
              Left = 120
              Top = 4
              Width = 100
              Height = 41
              Anchors = []
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -26
              Font.Name = 'Tahoma'
              Font.Style = []
              Increment = 10
              MaxValue = 1300
              MinValue = -1300
              ParentFont = False
              TabOrder = 0
              Value = 0
              OnChange = CaretSpinChange
              OnClick = BtmCaretSpinClick
            end
            object BtmCaretMoveRightBtn: TSpeedButton
              Tag = 5
              AlignWithMargins = True
              Left = 223
              Top = 1
              Width = 115
              Height = 48
              Margins.Top = 1
              Margins.Bottom = 1
              Align = alClient
              AllowAllUp = True
              GroupIndex = 51
              Caption = '>>'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -28
              Font.Name = 'Myriad Pro'
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = CaretSpinChange
              ExplicitLeft = 277
              ExplicitTop = 18
              ExplicitWidth = 23
              ExplicitHeight = 22
            end
          end
          object GridPanel7: TGridPanel
            Left = 2
            Top = 50
            Width = 341
            Height = 50
            Align = alTop
            BevelOuter = bvNone
            ColumnCollection = <
              item
                Value = 49.999994891030280000
              end
              item
                SizeStyle = ssAbsolute
                Value = 100.000000000000000000
              end
              item
                Value = 50.000005108969720000
              end>
            ControlCollection = <
              item
                Column = 0
                Control = TopCaretMoveLeftBtn
                Row = 0
              end
              item
                Column = 1
                Control = TopCaretSpin
                Row = 0
              end
              item
                Column = 2
                Control = TopCaretMoveRightBtn
                Row = 0
              end>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            RowCollection = <
              item
                Value = 100.000000000000000000
              end>
            TabOrder = 1
            DesignSize = (
              341
              50)
            object TopCaretMoveLeftBtn: TSpeedButton
              Tag = 2
              AlignWithMargins = True
              Left = 3
              Top = 1
              Width = 114
              Height = 48
              Margins.Top = 1
              Margins.Bottom = 1
              Align = alClient
              AllowAllUp = True
              GroupIndex = 50
              Caption = '<<'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -28
              Font.Name = 'Myriad Pro'
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = CaretSpinChange
              ExplicitLeft = 56
              ExplicitTop = 18
              ExplicitWidth = 23
              ExplicitHeight = 22
            end
            object TopCaretSpin: TSpinEdit
              Left = 120
              Top = 4
              Width = 100
              Height = 41
              Anchors = []
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -26
              Font.Name = 'Tahoma'
              Font.Style = []
              Increment = 10
              MaxValue = 1300
              MinValue = -1300
              ParentFont = False
              TabOrder = 0
              Value = 0
              OnChange = CaretSpinChange
              OnClick = TopCaretSpinClick
            end
            object TopCaretMoveRightBtn: TSpeedButton
              Tag = 3
              AlignWithMargins = True
              Left = 223
              Top = 1
              Width = 115
              Height = 48
              Margins.Top = 1
              Margins.Bottom = 1
              Align = alClient
              AllowAllUp = True
              GroupIndex = 50
              Caption = '>>'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -28
              Font.Name = 'Myriad Pro'
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = CaretSpinChange
              ExplicitLeft = 176
              ExplicitTop = 208
              ExplicitWidth = 23
              ExplicitHeight = 22
            end
          end
          object Panel1: TPanel
            Left = 2
            Top = 172
            Width = 341
            Height = 12
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 2
          end
          object GridPanel9: TGridPanel
            Left = 2
            Top = 224
            Width = 341
            Height = 95
            Align = alBottom
            BevelOuter = bvNone
            ColumnCollection = <
              item
                Value = 33.333340304173380000
              end
              item
                Value = 33.333340304173380000
              end
              item
                Value = 33.333319391653230000
              end>
            ControlCollection = <
              item
                Column = 0
                Control = m_MoveCaretsBtn
                Row = 0
              end
              item
                Column = 1
                Control = m_CalibrateOnMoveBtn
                Row = 0
              end
              item
                Column = 2
                Control = m_MoveToHomeBtn
                Row = 0
              end>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -22
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            RowCollection = <
              item
                Value = 100.000000000000000000
              end>
            TabOrder = 3
            object m_MoveCaretsBtn: TBitBtn
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 107
              Height = 89
              Align = alClient
              Caption = #1044#1074#1080#1078#1077#1085#1080#1077
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              Glyph.Data = {
                C6330000424DC633000000000000360000002800000057000000320000000100
                18000000000090330000890B0000890B00000000000000000001FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9
                D9D94C4C4C5252526B6B6B8282826767674E4E4E4F4F4FF8F8F8FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF9E9E9E6060608A8A8AB3B3B3D4D4D4AAAAAA8181814C4C4CE7
                E7E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9E9E9E5D5D5D868686ADADADCDCDCDA5A5
                A57D7D7D494949E7E7E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB0
                B0B0A3A3A3A5A5A5A6A6A6A8A8A8AAAAAAABABABADADADA2A2A24E4E4E535353
                646464757575626262505050515151ABABABADADADABABABA9A9A9A8A8A8A6A6
                A6A5A5A5A2A2A2C2C2C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF9B9B9B7A7A7A8888889090909797979E9E9EA6A6A6ADADADB4B4B4BB
                BBBBC3C3C3CACACAD2D2D2D7D7D7D0D0D0C9C9C9C1C1C1BABABAB3B3B3ABABAB
                A4A4A49D9D9D9595958E8E8E878787757575B1B1B1FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9595955B5B5B5F5F5F
                6363636767676A6A6A6E6E6E7272728181818888889090909797979E9E9EA6A6
                A6ADADADB4B4B4BBBBBBC3C3C3CACACAD2D2D2D7D7D7D0D0D0C9C9C9C1C1C1BA
                BABAB3B3B3ABABABA4A4A49D9D9D9595958E8E8E878787808080707070656565
                6060605B5B5B575757525252505050B2B2B2FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFC3C3C1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFD6D6D66D6D6D353839080A0A02
                0303020304020304020304020404030404030404030404030404030404030404
                0304040304040304040404050404050405050405050405050405050405050405
                0504050504050504050504040504040503040403040403040403040403040403
                04040304040304040304040204040203040203040203040203030E1011404243
                7B7B7BEBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF736F6BFCFCFCFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFDFDFD8888880507071736
                3D2C70803E9AAF44A2B746A2B749A3B74DA4B74FA4B752A5B756A6B758A7B75B
                A7B75EA8B761A9B764A9B767AAB76AABB76CACB76FACB772ADB775AEB778AEB7
                7BAEB77EB0B77AAEB778AEB774AEB771ADB76FACB76CACB769ABB766A9B763A9
                B760A9B75DA7B75AA7B758A7B756A5B751A5B74FA4B74DA4B749A3B746A2B744
                A2B73A93A828687712262B121516AEAEAEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA79C93BBB8B5FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF868686
                060B0C31879C52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6
                FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9F
                F0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF
                92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF56DEFF4FD7F8266878010202BCBCBCFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFADA298ACA39AFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000D1D1D10405062E8AA14EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF
                6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93ED
                FF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9E
                F0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF
                71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4DDDFF205B
                691A1E1EF1F1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFADA298DDCABBBBB8B5FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF00000066666614363E4ADBFF4EDDFF52DEFF56DEFF5AE0FF5F
                E1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF
                8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3
                FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7D
                E9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF
                51DEFF4DDDFF46D2F20810129C9C9CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADA298E2CFBFACA29AFCFCFCFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000002F34352470824ADBFF4EDDFF52DE
                FF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7F
                E9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FF
                ABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AEC
                FF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF56DEFF51DEFF4DDDFF49DBFF17404A686868FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADA298E2CFBFDD
                CABBBAB6B4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000060808319AB3
                4ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6
                FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9F
                F0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF
                92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4DDDFF49DBFF216A7B373B3CFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFADA298E2CFBFE2CFBFAEA39BFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                000000000034A4BF4ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF
                6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93ED
                FF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9E
                F0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF
                71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4DDDFF49DB
                FF257487313637FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFADA298E2CFBFE2CFBFDDCABBBAB7B4FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF00000000000034A4BF4ADBFF4EDDFF52DEFF56DEFF5AE0FF5F
                E1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF
                8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3
                FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7D
                E9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF
                51DEFF4DDDFF49DBFF257487303536F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7
                F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7
                F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7ADA298E2CFBFE2CFBFE2CFBFADA39A
                FCFCFCFFFFFFFFFFFFFFFFFFFFFFFF00000000000034A4BF4ADBFF4EDDFF52DE
                FF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7F
                E9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FF
                ABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AEC
                FF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF56DEFF51DEFF4DDCFD43D1F0216E7F262827B3A59AB3A59AB3A59A
                B3A59AB3A59AB3A59AB3A59AB3A59AB3A59AB3A59AB3A59AB3A59AB3A59AB3A5
                9AB3A59AB3A59AB3A59AB3A59AB3A59AB3A59AB3A59AB3A59AD4C4B5E2CFBFE2
                CFBFE2CFBFDECBBBBAB7B4FFFFFFFFFFFFFFFFFFFFFFFF00000000000034A4BF
                4ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6
                FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9F
                F0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF
                92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4CDCFC42D0EF216E7E2B2D2BE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFAEA39BFBFBFBFFFFFFFFFFFFFFFFFF00
                000000000034A4BF4ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF
                6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93ED
                FF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9E
                F0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF
                71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4CDCFC42D0
                EF216E7E2B2D2BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFDECBBBBAB6B3FFFF
                FFFFFFFFFFFFFF00000000000034A4BF4ADBFF4EDDFF52DEFF56DEFF5AE0FF5F
                E1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF
                8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3
                FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7D
                E9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF
                51DEFF4CDCFC42D0EF216E7E2B2D2BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFAFA49BFBFBFBFFFFFFFFFFFF00000000000034A4BF4ADBFF4EDDFF52DE
                FF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7F
                E9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FF
                ABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AEC
                FF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF56DEFF51DEFF4CDCFC42D0EF216E7E2B2D2BE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFDECCBCBAB6B3FFFFFFFFFFFF00000000000034A4BF
                4ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6
                FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9F
                F0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF
                92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4CDCFC42D0EF216E7E2B2D2BE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFAEA49BFBFBFBFFFFFF00
                000000000034A4BF4ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF
                6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93ED
                FF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9E
                F0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF
                71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4CDCFC42D0
                EF216E7E2B2D2BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFDECC
                BCB9B6B3FFFFFF00000000000034A4BF4ADBFF4EDDFF52DEFF56DEFF5AE0FF5F
                E1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF
                8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3
                FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7D
                E9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF
                51DEFF4CDCFC42D0EF216E7E2B2D2BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFB0A49BFBFBFB00000000000034A4BF4ADBFF4EDDFF52DE
                FF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7F
                E9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FF
                ABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AEC
                FF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF56DEFF51DEFF4CDCFC42D0EF216E7E2B2D2BE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFDECCBCB9B6B300000000000034A4BF
                4ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6
                FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9F
                F0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF
                92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4CDCFC42D0EF216E7E2B2D2BE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFABA09800
                000000000034A4BF4ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF
                6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93ED
                FF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9E
                F0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF
                71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4CDCFC42D0
                EF216E7E2B2D2BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFADA7A100000000000034A4BF4ADBFF4EDDFF52DEFF56DEFF5AE0FF5F
                E1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF
                8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3
                FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7D
                E9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF
                51DEFF4CDCFC42D0EF216E7E2B2D2BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFBEB0A3E2E1E000000000000034A4BF4ADBFF4EDDFF52DE
                FF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7F
                E9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FF
                ABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AEC
                FF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF56DEFF51DEFF4CDCFC42D0EF216E7E2B2D2BE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFADA6A1FFFFFF00000000000034A4BF
                4ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6
                FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9F
                F0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF
                92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4CDCFC42D0EF216E7E2B2D2BE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFBDAFA2E2E1E0FFFFFF00
                000000000034A4BF4ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF
                6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93ED
                FF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9E
                F0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF
                71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4CDCFC42D0
                EF216E7E2B2D2BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFADA8
                A2FFFFFFFFFFFF00000000000034A4BF4ADBFF4EDDFF52DEFF56DEFF5AE0FF5F
                E1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF
                8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3
                FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7D
                E9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF
                51DEFF4CDCFC42D0EF216E7E2B2D2BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFBCAEA2E3E2E1FFFFFFFFFFFF00000000000034A4BF4ADBFF4EDDFF52DE
                FF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7F
                E9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FF
                ABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AEC
                FF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF56DEFF51DEFF4CDCFC42D0EF216E7E2B2D2BE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFAEA7A2FFFFFFFFFFFFFFFFFF00000000000034A4BF
                4ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6
                FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9F
                F0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF
                92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4CDCFC42D0EF216E7E2B2D2BE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFBDAEA3E3E2E1FFFFFFFFFFFFFFFFFF00
                000000000034A4BF4ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF
                6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93ED
                FF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9E
                F0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF
                71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4CDCFC42D0
                EF216E7E2B2D2BE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFADA8A2FFFFFFFFFF
                FFFFFFFFFFFFFF00000000000034A4BF4ADBFF4EDDFF52DEFF56DEFF5AE0FF5F
                E1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF
                8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3
                FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7D
                E9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF
                51DEFF4CDCFE47D8FA237183262A2AB8B4B0B8B4B0B8B4B0B8B4B0B8B4B0B8B4
                B0B8B4B0B8B4B0B8B4B0B8B4B0B8B4B0B8B4B0B8B4B0B8B4B0B8B4B0B8B4B0B8
                B4B0B8B4B0B8B4B0B8B4B0B8B4B0B8B4B0B5A89DE2CFBFE2CFBFE2CFBFBDADA2
                E3E2E2FFFFFFFFFFFFFFFFFFFFFFFF00000000000034A4BF4ADBFF4EDDFF52DE
                FF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7F
                E9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FF
                ABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AEC
                FF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF56DEFF51DEFF4DDDFF49DBFF257487313637FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADA298E2CFBFE2
                CFBFE2CFBFAEA8A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000034A4BF
                4ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6
                FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9F
                F0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF
                92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4DDDFF49DBFF257487313637FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFADA298E2CFBFE2CFBFBDAFA3E4E3E3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000060808319AB34ADBFF4EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF
                6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93ED
                FF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9E
                F0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF
                71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4DDDFF49DB
                FF216A7B373B3CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFADA298E2CFBFE2CFBFAEA8A3FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF0000002F34352470824ADBFF4EDDFF52DEFF56DEFF5AE0FF5F
                E1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF
                8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3
                FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7D
                E9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF
                51DEFF4DDDFF49DBFF17404A686868FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADA298E2CFBFBBADA2E4E3E3FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000066666614363E4ADBFF4EDDFF52DE
                FF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6FF77E7FF7AE7FF7F
                E9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9FF0FFA3F2FFA7F3FF
                ABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF92EDFF8EECFF8AEC
                FF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF56DEFF51DEFF4DDDFF46D2F20810129C9C9CFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADA298E2CFBFAE
                A8A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000D1D1D1040506
                2E8AA14EDDFF52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF6BE4FF6EE4FF72E6
                FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93EDFF97EFFF9BF0FF9F
                F0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9EF0FF9AEFFF96EFFF
                92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF71E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF56DEFF51DEFF4DDDFF205B691A1E1EF1F1F1FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFADA298BCADA2E5E5E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFF868686060B0C31879C52DEFF56DEFF5AE0FF5FE1FF62E1FF66E3FF
                6BE4FF6EE4FF72E6FF77E7FF7AE7FF7FE9FF83EAFF87EAFF8BECFF8FEDFF93ED
                FF97EFFF9BF0FF9FF0FFA3F2FFA7F3FFABF3FFAFF5FFAAF3FFA7F2FFA2F2FF9E
                F0FF9AEFFF96EFFF92EDFF8EECFF8AECFF86EAFF82E9FF7DE9FF7AE7FF76E6FF
                71E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF56DEFF4FD7F82668780102
                02BCBCBCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFADA298AEA9A4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFDFDFD88888805070717363D2C70803E9AAF44
                A2B746A2B749A3B74DA4B74FA4B752A5B756A6B758A7B75BA7B75EA8B761A9B7
                64A9B767AAB76AABB76CACB76FACB772ADB775AEB778AEB77BAEB77EB0B77AAE
                B778AEB774AEB771ADB76FACB76CACB769ABB766A9B763A9B760A9B75DA7B75A
                A7B758A7B756A5B751A5B74FA4B74DA4B749A3B746A2B744A2B73A93A8286877
                12262B121516AEAEAEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF847C76E6E6E5FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFD6D6D66D6D
                6D353839080A0A02030302030402030402030402040403040403040403040403
                0404030404030404030404030404030404040405040405040505040505040505
                0405050405050405050405050405050405050404050404050304040304040304
                0403040403040403040403040403040403040402040402030402030402030402
                03030E10114042437B7B7BEBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B8886FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9595955F5F5F6363636767676B6B6B6F6F
                6F7272727272728181818888889090909797979E9E9EA6A6A6ADADADB4B4B4BB
                BBBBC3C3C3CACACAD2D2D2D7D7D7D0D0D0C9C9C9C1C1C1BABABAB3B3B3ABABAB
                A4A4A49D9D9D9595958E8E8E8787878080806F6F6F6262625D5D5D5858585353
                534F4F4F4E4E4EB4B4B4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF9A9A9A7A7A7A8888889090909797979E9E9EA6A6
                A6ADADADB4B4B4BBBBBBC3C3C3CACACAD2D2D2D7D7D7D0D0D0C9C9C9C1C1C1BA
                BABAB3B3B3ABABABA4A4A49D9D9D9595958E8E8E878787757575B1B1B1FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB0B0B0A3A3A3A5A5A5
                A6A6A6A8A8A8AAAAAAABABABADADADA2A2A24C4C4C5454546A6A6A7B7B7B6767
                675252524F4F4FABABABADADADABABABA9A9A9A8A8A8A6A6A6A5A5A5A2A2A2C2
                C2C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9E9E9E5D5D5D868686
                ADADADCDCDCDA5A5A57D7D7D494949E7E7E7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9E
                9E9E6060608A8A8AB3B3B3D4D4D4AAAAAA8181814C4C4CE7E7E7FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFD9D9D94848484E4E4E6666667C7C7C6363634A4A4A4D4D4DF8
                F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000}
              Layout = blGlyphTop
              ParentFont = False
              TabOrder = 0
              WordWrap = True
              OnClick = m_MoveCaretsBtnClick
            end
            object m_CalibrateOnMoveBtn: TBitBtn
              AlignWithMargins = True
              Left = 116
              Top = 3
              Width = 107
              Height = 89
              Align = alClient
              Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              Glyph.Data = {
                C6330000424DC633000000000000360000002800000057000000320000000100
                18000000000090330000610B0000610B00000000000000000001FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFC6C6C6929292A1A1A1B0B0B0A6A6A69797979C9C9CFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFF9F9F9393939595959787878979797808080616161464646AA
                AAAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F74444447F7F7FA9A9A9D3D3D3B3B3
                B38989895F5F5FA1A1A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD74303FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
                FEFEF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F3F3F3505050575757
                7979799A9A9A8383836161614A4A4AC2C2C2F7F7F7F7F7F7F7F7F7F7F7F7F7F7
                F7F7F7F7F7F7F7FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFDFDFD8080807171717676767B7B7B8181818585858C8C8C90909095
                95959797979898989F9F9FA5A5A5A0A0A09A9A9A9696969797979292928C8C8C
                8787878282827C7C7C777777727272727272E4E4E4FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9D9D9D929292
                9494949696969898989999997F7F7F7B7B7B8686868D8D8D9494949C9C9CA3A3
                A3ABABABB2B2B2B9B9B9C1C1C1C8C8C8D0D0D0D7D7D7D2D2D2CACACAC3C3C3BB
                BBBBB4B4B4ACACACA5A5A59E9E9E9696968F8F8F878787808080747474909090
                8E8E8E8B8B8B898989878787848484F2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFF0F0F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEEB5B5B58080802D
                2D2D1A1A1A1E1E1E2121212424242626262929292C2C2C2E2E2E313131343434
                3636363939393C3C3C3F3F3F4141414444444747474949494C4C4C4F4F4F4D4D
                4D4A4A4A4747474444444242423F3F3F3C3C3C3A3A3A3737373535353131312F
                2F2F2C2C2C2929292727272424242121211F1F1F1C1C1C2121216F6F6FA8A8A8
                E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFA7A5A2D0CFCDFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9D9D9D0C0F
                0F0509091934391F4A531E4952204953214A53224A53234A53254C53274C5329
                4C532A4C532B4D532C4D532D4E532E4E53304E53314E53324E53354E53364F53
                374F53384F53374F53364F53354E53344E53324E53304E532F4E532E4E532C4D
                532B4D532A4D53294C53284C53254C53244A53224A53214A532049531E49521E
                49521D3E450A1113010202717272F6F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B4B1ACA59EFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                6062630407082D7B8E54DBFC58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6
                FF75E6FF79E7FF7DE9FF81E9FF86EAFF89ECFF8EECFF92EDFF95EFFF9AEFFF9E
                F0FFA2F2FFA6F2FFAAF3FFAEF5FFABF3FFA7F3FFA3F2FF9FF0FF9BEFFF97EFFF
                92EDFF8FECFF8AECFF86EAFF83EAFF7EE9FF7AE7FF76E7FF72E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF55DEFF3B9FB70C1A1D292E2EF5F5F5FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B4
                B1CCBCAED0CFCDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFAFAFAF0203043BACC650DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF
                68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FF81E9FF86EAFF89ECFF8EECFF92ED
                FF95EFFF9AEFFF9EF0FFA2F2FFA6F2FFAAF3FFAEF5FFABF3FFA7F3FFA3F2FF9F
                F0FF9BEFFF97EFFF92EDFF8FECFF8AECFF86EAFF83EAFF7EE9FF7AE7FF76E7FF
                72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF48CDED0D1B
                1E6E6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFB9B4B1E2CFBFACA59EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF191D1F22697A4CDDFF50DDFF54DEFF58E0FF5C
                E0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FF81E9FF86EAFF
                89ECFF8EECFF92EDFF95EFFF9AEFFF9EF0FFA2F2FFA6F2FFAAF3FFAEF5FFABF3
                FFA7F3FFA3F2FF9FF0FF9BEFFF97EFFF92EDFF8FECFF8AECFF86EAFF83EAFF7E
                E9FF7AE7FF76E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF
                51DDFF4DDDFF37A1B9010202DCDCDCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B4B1E2CFBFCCBCAECFCFCDFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E10000003DBBD94CDDFF50DD
                FF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF7D
                E9FF80E7FC86EAFE89ECFF8EECFF92EDFF94EEFE99EDFC9EF0FFA2F2FFA6F2FF
                A4E9F59EDBE392CAD397D9E3A2F1FE9FF0FF99EDFC96EEFE92EDFF8FECFF8AEC
                FF86EAFE81E8FC7EE9FF7AE7FF76E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF55DEFF51DDFF4DDDFF49DBFF0B161AA1A1A1FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B4B1E2CFBFE2CFBFAC
                A49EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAEAEAE060B0D
                47DBFE4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6
                FF75E6FF79E7FF7DE9FF80E7FC84E6F987E8FB8EECFF92EDFF95EFFF98EBFB9A
                ECFBA2F2FF98DCE6373A3A33333333343433333363848A9FF0FF99EDFC97E8F2
                B3CF9FC4C477C0C478A9D1AA83E6F87CE6FB7AE7FF76E7FF72E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4DDDFF49DBFF18424C6E6E6EFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B4
                B1E2CFBFE2CFBFCCBCAECFCFCDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFF9F9F9F0D1C1F47DBFF4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF
                68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FF81E9FF85E9FC84E3F58BE5F891ED
                FE95EFFF9AEFFF99E9F89EECF88ECCD6333333333333333333343434576F739F
                F0FFA3E7E3DFAD2DE89C04EC9F02EFA103C4C06C86E7F57DE4F979E5FC76E7FF
                72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4DDDFF49DB
                FF19515F5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFB9B4B1E2CFBFE2CFBFE2CFBFACA49EFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF9F9F9F0D1C1F47DBFF4CDDFF50DDFF54DEFF58E0FF5C
                E0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FF81E9FF86EAFF
                88EBFE8BE5F665969F45565A88CEDC9DEFFE85C0CA576F733333333333333333
                333333333D434485C3CDD5B84EF1A100EE9F01E19F139ED3BD86EAFE83EAFF7E
                E9FF79E6FC76E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF
                51DDFF4DDDFF49DBFF19515F5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B4B1E2CFBFE2CFBFE2CFBFCBBCACCECECC
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9F0D1C1F47DBFF4CDDFF50DD
                FF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF7D
                E9FF81E8FE86EAFF89ECFF5C87903434343434343B4243495A5D333333343434
                34343433333333333333333333333373A4ACEAA20CF1A100F1A100CABD6087E5
                F683E3F683E9FE7EE9FF7AE7FF76E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF55DEFF51DDFF4CDBFC45D4F4174E5A423E3CB1A79FB1A79FB1A79F
                B1A79FB1A79FB1A79FB1A79FB1A79FB1A79FB1A79FB1A79FB1A79FB1A79FB1A7
                9FB1A79FB1A79FB1A79FB1A79FB1A79FB1A79FB1A79FACA39AE2CFBFE2CFBFE2
                CFBFE2CFBFABA49DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9F0D1C1F
                47DBFF4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6
                FF75E6FF79E7FF7DE9FF81E7FC86EAFE73BDCB33333333333334343434343433
                3333333333333333343434343434333333333333333333709FA6E19F13F1A100
                F1A100EAA3118CE9F682E2F581E4F88FDED991D9CC76E7FF72E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4CDAFB42D0EF164C59544D47E2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFCCBCADCECECCFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFF9F9F9F0D1C1F47DBFF4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF
                68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FF81E7FC84E6F951747A3333333333
                333333333434343333333333333333333435353A3D3E42494B383C3D33333373
                A3ABDCA728EA9D03F1A100F1A100C1C37686E6F5A5C79CE5A61697D6C076E7FF
                72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4CDAFB42D0
                EF164C59544D47E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFABA49DFFFFFFFFFF
                FFFFFFFFFFFFFF0000009F9F9F0D1C1F47DBFF4CDDFF50DDFF54DEFF58E0FF5C
                E0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FF81E9FF84E5F8
                84E0F2567A813333333333333333333434343E464778A5ACA2E5F1ACF3FCA7EC
                F6A2EAF58ECDD8C0CC93F1A101EA9D03EA9D03F1A100F1A100F0A101EE9F01E3
                A0127EE4F476E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF
                51DDFF4CDAFB42D0EF164C59544D47E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                CCBCADCECDCCFFFFFFFFFFFFFFFFFF92AD9E9F9F9F0D1C1F47DBFF4CDDFF50DD
                FF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF7D
                E9FF81E9FF86EAFF87E6F870AFBA3333333333333333334A5D6095DBE6A4EFFC
                AAF3FFAEF5FFAAF3FEA3EBF6BDCA94F0A101F1A100F1A100EA9D03E79C04EE9F
                01F1A100F0A100AACB9A79E5FC76E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF55DEFF51DDFF4CDAFB42D0EF164C59544D47E2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFABA49DFFFFFFFFFFFFFFFFFFBFE2CF9F9F9F0D1C1F
                47DBFF4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6
                FF75E6FF79E7FF7DE9FF81E9FF86EAFF89ECFF4C64693434343333333E464796
                E1EFA1EFFCA0E8F5A8F0FCADF4FEABF3FFC3D29AE99D04EA9D03F1A100F1A100
                E5A91DC0BF72CBAF48CFB44AA3D4B37EE9FF7AE7FE76E7FF72E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4CDAFB42D0EF164C59544D47E2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFCCBCADCECDCCFFFFFFFFFFFFBF
                E2CF9F9F9F0D1C1F47DBFF4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF
                68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FF77D2E676C8D86AAAB63434353333
                333434346FA1AB9EF0FFA2F2FFA4EFFCA5E9F5AAEDF6C4D39DF0A203F1A100EA
                9D03EA9D03E1AC298BC3BE4E696F54787F51757C6CB7C770C7D878E4FB76E7FF
                72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4CDAFB42D0
                EF164C59544D47E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFABA3
                9DFFFFFFFFFFFFBFE2CF9F9F9F0D1C1F47DBFF4CDDFF50DDFF54DEFF58E0FF5C
                E0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF78DFF2353939333434
                3333333333333333333434358FDBE99CEDFCA2F2FFA6F2FFADEAE8C1CE9AE89D
                06F1A100F1A100F1A100D7AC379AE2E55D858D33333333333333333334343433
                3333568C9776E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF
                51DDFF4CDAFB42D0EF164C59544D47E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFCCBCADCECDCCFFFFFFBFE2CF9F9F9F0D1C1F47DBFF4CDDFF50DD
                FF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF6D
                C4D53333333434343434343333333333333F4A4C99ECFC98E6F5A1EFFCADEAE3
                E4AD2BF0A101EA9D03EA9D03F1A100D9B7469EECF293E8F6699EA83333333333
                33333333333333343434496B7276E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF55DEFF51DDFF4CDAFB42D0EF164C59544D47E2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFABA39DFFFFFFBFE2CF9F9F9F0D1C1F
                47DBFF4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6
                FF75E6FF79E7FF6DC4D63333333333333434343434343333333F4A4C9AEFFF9C
                EDFCA8DCCFE5A920F1A100F1A100F1A100EA9D03CFB656A1EFF89BEFFF96EEFE
                6AA0A9343434333333333333333333333333496C7276E7FF72E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4CDAFB42D0EF164C59544D47E2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFCCBDAECFCECCBF
                E2CF9F9F9F0D1C1F47DBFF4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF
                68E3FF6DE4FF71E6FF75E6FF79E7FF75D6EA3333333333333333333434343434
                3435373899EEFE9EEEFBDFAB2AE79C04F1A100F1A100F1A100E5AC279EE7F19C
                EBF89BEFFF97EFFF628F983434343434343333333333333333334F7A8376E7FF
                72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4CDAFB42D0
                EF164C59544D47E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFBFB1A5E1E0E0BFE2CF9F9F9F0D1C1F47DBFF4CDDFF50DDFF54DEFF58E0FF5C
                E0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF7DE8FE537D864C6A70
                404F5233333334343436373791DBE5D3BB58F1A100EA9D03EA9D03EE9F01EAAA
                1BB2E5D6A2EFFC9AE7F599ECFC97EFFF4E686D333333343434343434465D624B
                6C7269BBCC76E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF
                51DDFF4CDAFB42D0EF164C59544D47E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFAFA9A4FFFFFFBFE2CF9F9F9F0D1C1F47DBFF4CDDFF50DD
                FF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF7D
                E9FF81E7FC85E9FC86E6F85679804C646979AEB2D2B047F1A100F1A100F1A100
                EA9D03E0A31CB4E4D5A7F3FFA3F2FF9EEDFC96E5F584CCD83434353333333333
                333C45477FE1F57EE9FF7AE7FF76E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF55DEFF51DDFF4CDAFB42D0EF164C59544D47E2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFBFB1A5E2E1E1FFFFFFBFE2CF9F9F9F0D1C1F
                47DBFF4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6
                FF75E6FF79E7FF7DE9FF80E7FC95D7CDBCC074D1B84FC8C06CDDB035EA9D03EA
                9D03F1A100F1A100D2C56FB7E4D4A5EAF5A5F0FCA3F2FF9FF0FF95E3F2455457
                3333333333333333335B8C9680E2F67CE3F87AE7FF76E7FF72E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4CDAFB42D0EF164C59544D47E2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFAFA9A4FFFFFFFFFFFFBF
                E2CF9F9F9F0D1C1F47DBFF4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF
                68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FF9CD8C2EBA20AE79C04EA9D03F1A1
                00F1A100F1A100EA9D03EA9D03D0C26CABF2FEAEF5FFA9F0FCA2E9F5A2EFFC8C
                D0DC4656593333333434343434343333336FB8C783E9FE7DE4F979E5FC76E7FF
                72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4CDAFB42D0
                EF164C59544D47E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFC0B1A6E2E1
                E1FFFFFFFFFFFFBFE2CF9F9F9F0D1C1F47DBFF4CDDFF50DDFF54DEFF58E0FF5C
                E0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FFDCAD2DF1A100
                F1A100EA9D03EA9D03F1A100F1A100F1A100CABE6970999F698A8F779FA47FAD
                B47099A052696D3435353333333333333333333434343434343A424477D0E27E
                E9FF79E6FC76E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF
                51DDFF4CDAFB42D0EF164C59544D47E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFB0AAA5FFFFFFFFFFFFFFFFFFBFE2CF9F9F9F0D1C1F47DBFF4CDDFF50DD
                FF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF8A
                E0E2EDA307BBC57D93E4E39EDFD5DCA626EA9D03F1A100EFA40883BCC5343434
                3434343333333333333333333333333434343333333333333333333333333434
                3434343470BECD7EE9FF7AE7FF76E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF55DEFF51DDFF4CDAFB42D0EF164C59544D47E2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFBFB1A5E3E2E2FFFFFFFFFFFFFFFFFFBFE2CF9F9F9F0D1C1F
                47DBFF4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6
                FF75E6FF79E7FF85E3EE92DAD385E9FC89ECFF8EECFFAFD4AEEA9D03EA9D03F1
                A10089B9B73333333434343434343333333333333333333333333A3E3F475A5D
                33333333333333333358848D80E2F67EE9FF7AE7FF76E7FF72E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4CDAFB42D0EF164C59544D47E2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFB1ABA6FFFFFFFFFFFFFFFFFFFFFFFFBF
                E2CF9F9F9F0D1C1F47DBFF4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF
                68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FF80E7FC83E4F887E6F88DEBFEA8DA
                C2F1A100EA9D03E79F0A99DDE25870743333333333333434343333333C434471
                A0A892E0EE92E4F25B81883E4B4E6297A286EAFE80E2F67CE3F87AE7FF76E7FF
                72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4CDAFB42D0
                EF164C59544D47E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFBEB0A3E3E3E2FFFFFFFFFF
                FFFFFFFFFFFFFFBFE2CF9F9F9F0D1C1F47DBFF4CDDFF50DDFF54DEFF58E0FF5C
                E0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FF81E9FF84E5F8
                84E3F58DEAFBC2C57AF1A100EEA001DAA72BA1EFFC85BCC53333333333333434
                343434344D60639DEDFC9AEEFE94E9F880CCD96CA7B383DEEF86EAFF81E7FC7C
                E2F878E6FC76E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF
                51DDFF4DDCFD47D7F9184F5C444341B7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1
                ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7
                B1ADB7B1ADB7B1ADB7B1ADB7B1ADA09893E2CFBFE2CFBFE2CFBFE2CFBFB0AAA5
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFE2CF9F9F9F0D1C1F47DBFF4CDDFF50DD
                FF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF7D
                E9FF81E9FF87EAFCA3D5B5D3AF3FF0A100F1A100E9A817ADDECAA0EDF98FCCD7
                333333333333333333333434566E729FF0FF9BEFFF97EFFF8FE9FB8DE7F989EA
                FC86EAFF83EAFF7EE7FC79E5FC76E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF55DEFF51DDFF4DDDFF49DBFF19515F5F5F5FFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B4B1E2CFBFE2CFBFE2
                CFBFBEB0A3E4E4E3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFE2CF9F9F9F0D1C1F
                47DBFF4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6
                FF75E6FF79E7FF7DE9FF81E9FF86EAFEA4D8BCBDC682CABF68BECA8AA1E8EA9E
                F0FFA1F1FEA3ECF86A8B925F777B54696C5C777C87C3CC9EF0FE9BEFFF97EFFF
                92EDFF8DEAFC89EAFC86EAFF83EAFF7EE9FF7AE7FE76E7FF72E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4DDDFF49DBFF19515F5F5F5FFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B4
                B1E2CFBFE2CFBFE2CFBFB1ABA6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF
                E2CFA5A5A50B151847DBFF4CDDFF50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF
                68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FF81E9FF86EAFF89ECFF8EECFF92ED
                FF95EFFF9AEFFF9EF0FFA2F2FFA6F2FFAAF3FFAEF5FFABF3FFA7F3FFA3F2FF9F
                F0FF9BEFFF97EFFF92EDFF8FECFF8AECFF86EAFF83EAFF7EE9FF7AE7FF76E7FF
                72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4DDDFF49DB
                FF194B58656565FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFB9B4B1E2CFBFE2CFBFBEB0A4E4E4E3FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF5D6D65D1D1D100000040C9E94CDDFF50DDFF54DEFF58E0FF5C
                E0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FF81E9FF86EAFF
                89ECFF8EECFF92EDFF95EFFF9AEFFF9EF0FFA2F2FFA6F2FFAAF3FFAEF5FFABF3
                FFA7F3FFA3F2FF9FF0FF9BEFFF97EFFF92EDFF8FECFF8AECFF86EAFF83EAFF7E
                E9FF7AE7FF76E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF
                51DDFF4DDDFF49DBFF11242A919191FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B4B1E2CFBFE2CFBFB2ACA7FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFC07090A2E8EA54CDDFF50DD
                FF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6FF75E6FF79E7FF7D
                E9FF81E9FF86EAFF89ECFF8EECFF92EDFF95EFFF9AEFFF9EF0FFA2F2FFA6F2FF
                AAF3FFAEF5FFABF3FFA7F3FFA3F2FF9FF0FF9BEFFF97EFFF92EDFF8FECFF8AEC
                FF86EAFF83EAFF7EE9FF7AE7FF76E7FF72E6FF6EE4FF6AE4FF65E3FF62E1FF5D
                E1FF59E0FF55DEFF51DDFF4DDDFF41C4E3010202C6C6C6FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B4B1E2CFBFBEB0A5E6
                E6E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF757676
                0B161A47CFEE50DDFF54DEFF58E0FF5CE0FF60E1FF65E3FF68E3FF6DE4FF71E6
                FF75E6FF79E7FF7DE9FF81E9FF86EAFF89ECFF8EECFF92EDFF95EFFF9AEFFF9E
                F0FFA2F2FFA6F2FFAAF3FFAEF5FFABF3FFA7F3FFA3F2FF9FF0FF9BEFFF97EFFF
                92EDFF8FECFF8AECFF86EAFF83EAFF7EE9FF7AE7FF76E7FF72E6FF6EE4FF6AE4
                FF65E3FF62E1FF5DE1FF59E0FF55DEFF51DDFF4DDDFF183F48353939FDFDFDFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B4
                B1E2CFBFB2ACA8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFF8F8F8262B2B14313845BEDB54DEFF58E0FF5CE0FF60E1FF65E3FF
                68E3FF6DE4FF71E6FF75E6FF79E7FF7DE9FF81E9FF86EAFF89ECFF8EECFF92ED
                FF95EFFF9AEFFF9EF0FFA2F2FFA6F2FFAAF3FFAEF5FFABF3FFA7F3FFA3F2FF9F
                F0FF9BEFFF97EFFF92EDFF8FECFF8AECFF86EAFF83EAFF7EE9FF7AE7FF76E7FF
                72E6FF6EE4FF6AE4FF65E3FF62E1FF5DE1FF59E0FF55DEFF4DD2F2215561080A
                0ADADADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFB9B4B1BEB0A5E6E6E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E6E6474A4A05090A21505C307B8D3F
                99AE439AAF459CAF489CAF4B9DAF4E9EAF519EAF539FAF56A0AF59A0AF5CA1AF
                5EA2AF62A2AF64A3AF66A4AF6AA4AF6DA5AF6FA5AF72A5AF75A7AF77A8AF75A8
                AF73A7AF70A5AF6DA5AF6AA4AF68A4AF64A3AF62A2AF5FA2AF5CA1AF5AA1AF57
                A0AF549FAF519FAF4E9EAF4C9DAF499DAF459CAF439BAF409AAF368699245C69
                0E1C1E222728C9C9C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B4B1B2ADA8FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6
                A65F6060292E2F05080805080806080806080906080906080907090907090907
                090907090A08090A080A0A080A0A080A0A090A0A090A0B090B0B090B0B0A0B0B
                0A0B0B0A0B0C0A0B0B0A0B0B090B0B090B0B090A0B090A0B080A0A080A0A080A
                0A08090A07090A07090907090907090906090906080906080906080805080805
                07081A1F205052538C8C8CF6F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADAAA8E7E7E6FFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8E86C6C6C6969696D6D6D7171717575
                757878787373737E7E7E8686868D8D8D9494949C9C9CA3A3A3ABABABB2B2B2B9
                B9B9C1C1C1C8C8C8D0D0D0D7D7D7D2D2D2CACACAC3C3C3BBBBBBB4B4B4ACACAC
                A5A5A59E9E9E9696968F8F8F8787878080807070706767676363635E5E5E5A5A
                5A565656575757C4C4C4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFE6E6E67878788686868D8D8D9494949C9C9CA3A3
                A3ABABABB2B2B2B9B9B9C1C1C1C8C8C8D0D0D0D7D7D7D2D2D2CACACAC3C3C3BB
                BBBBB4B4B4ACACACA5A5A59E9E9E9696968F8F8F878787777777B7B7B7FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEA1A1A1A2A2A2
                A4A4A4A5A5A5A6A6A6A8A8A8A9A9A9A9A9A94F4F4F4747475E5E5E7676766565
                654E4E4E3E3E3E9B9B9BA9A9A9A8A8A8A6A6A6A6A6A6A4A4A4A3A3A3A1A1A1C6
                C6C6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F74444447F7F7F
                A9A9A9D3D3D3B3B3B38989895F5F5FA1A1A1FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7
                F7F74444447F7F7FA9A9A9D3D3D3B3B3B38989895F5F5FA1A1A1FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFEFEFE7474745353536A6A6A8181817272725B5B5B515151E8
                E8E8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF}
              Layout = blGlyphTop
              ParentFont = False
              TabOrder = 1
              WordWrap = True
              OnClick = m_CalibrateOnMoveBtnClick
            end
            object m_MoveToHomeBtn: TBitBtn
              AlignWithMargins = True
              Left = 229
              Top = 3
              Width = 109
              Height = 89
              Align = alClient
              Caption = #1044#1086#1084#1086#1081
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              Glyph.Data = {
                FA310000424DFA31000000000000360000002800000056000000310000000100
                180000000000C43100003A0B00003A0B00000000000000000001FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8
                F8F88282829A9A9AABABABA8A8A8989898878787FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF6262625151517474749595958E8E8E6C6C6C4C4C4C898989FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF6565657272729D9D9DC8C8C8BDBDBD929292686868888888FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8E8DFDFDFDFDFDFDFDFDFDF
                DFDFDFDFDFDFDFDFDFDFDF818181444444686868898989838383626262424242
                A0A0A0DFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFF0F0F0FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBABABA7070707979797F7F7F
                8686868C8C8C939393999999A0A0A0A6A6A6ADADADB4B4B4BABABAB9B9B9B2B2
                B2ABABABA5A5A59E9E9E9898989191918A8A8A8484847D7D7D777777767676D8
                D8D8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFD2D2D27F7F7F8282828484848787878989898C8C8C7474748282828A8A
                8A919191999999A0A0A0A8A8A8AFAFAFB7B7B7BEBEBEC6C6C6CECECED5D5D5D3
                D3D3CCCCCCC4C4C4BDBDBDB5B5B5AEAEAEA6A6A69E9E9E9797978F8F8F888888
                8080807575758383838080807C7C7C797979767676767676EEEEEEFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFD6D5D4D5D4D3FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEBBBB
                BB838383414445131819141919161A1C171C1D181D1E1A1F201C20211E21221F
                222320242521252623262725282826292927292A292B2B292C2C2A2D2E2C2F2F
                2D2F302D2F302B2E2E2A2D2D292C2C282A2B27292A2529292427282326272125
                252023241E22231D21221C2020191E1F181D1E171C1D15191C141819141A1B56
                5858909090D1D1D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA69F9AC4C3C2FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E13F
                434305090A1B39402865722F707F31717F34717F35727F37727F3A737F3B737F
                3D747F3F747F41757F44757F46767F48767F4A777F4C777F4F787F50787F5279
                7F54797F567A7F567A7F54797F52797F50787F4D787F4C777F49777F48767F45
                767F43757F41757F3F747F3D747F3B737F38737F37727F34717F33717F30717F
                2E707F255C67172E340203036B6C6DF5F5F5FFFFFFFFFFFF5684FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD8D7D6C0B1A4C4C3C2FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E4E4
                0F1213193E4748C6E356DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6
                FF77E7FF7BE7FF7FE9FF83EAFF88EAFF8CECFF90EDFF95EDFF98EFFF9DF0FFA1
                F0FFA5F2FFA9F3FFADF3FFACF3FFA8F3FFA4F2FFA0F0FF9BF0FF98EFFF93EDFF
                8FEDFF8BECFF86EAFF83EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3
                FF61E1FF5CE0FF59E0FF54DEFF3DAAC40F2125363A3BFCFCFCFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADA6A0DBC9B9C4C3C2FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF4245461637404DDBFE51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6E
                E4FF73E6FF77E7FF7BE7FF7FE9FF83EAFF88EAFF8CECFF90EDFF95EDFF98EFFF
                9DF0FFA1F0FFA5F2FFA9F3FFADF3FFACF3FFA8F3FFA4F2FFA0F0FF9BF0FF98EF
                FF93EDFF8FEDFF8BECFF86EAFF83EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69
                E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF45C9E9091214848585FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD8D7D6C6B6AADBC9B9
                C4C3C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFC5C5C502030340C0DE4DDDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF
                6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EAFF88EAFF8CECFF90EDFF95ED
                FF98EFFF9DF0FFA1F0FFA5F2FFA9F3FFADF3FFACF3FFA8F3FFA4F2FFA0F0FF9B
                F0FF98EFFF93EDFF8FEDFF8BECFF86EAFF83EAFF7EE9FF7AE7FF76E7FF71E6FF
                6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF2E8CA20709
                0AFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADA6A0E2CF
                BFDBC9B9C4C3C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF919191132A2E49DBFF4DDDFF51DEFF56DEFF59E0FF5EE1FF62E1
                FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EAFF88EAFF8CECFF90
                EDFF95EDFF98EFFF9DF0FFA1F0FFA5F2FFA9F3FFADF3FFACF3FFA8F3FFA4F2FF
                A0F0FF9BF0FF98EFFF93EDFF8FEDFF8BECFF86EAFF83EAFF7EE9FF7AE7FF76E7
                FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF42
                CCED000000D1D1D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD8D7D5C5
                B6A9E2CFBFDBC9B9C4C3C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF6566661C556149DBFF4DDDFF51DEFF56DEFF59E0FF5E
                E1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EAFF88EAFF
                8CECFF90EDFF95EDFF98EFFF9DF0FFA1F0FFA5F2FFA9F3FFADF3FFACF3FFA8F3
                FFA4F2FFA0F0FF9BF0FF98EFFF93EDFF8FEDFF8BECFF86EAFF83EAFF7EE9FF7A
                E7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF
                4CDDFF47DBFF0F1E21A6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                AEA7A1E2CFBFE2CFBFDBC9B9C4C3C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF5D5F5F1E5F6F49DBFF4DDDFF51DEFF56DEFF
                59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EA
                FF88EAFF8CECFF90EDFF95EDFF98EFFF9DF0FFA1F0FFA5F2FFA9F3FFADF3FFAC
                F3FFA8F3FFA4F2FFA0F0FF9BF0FF98EFFF93EDFF8FEDFF8BECFF86EAFF83EAFF
                7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DE
                FF50DDFF4CDDFF47DBFF142A2F9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFD7D6D5C8B8ABE2CFBFE2CFBFDBC9B9C4C3C2FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5D5F5F1E5F6F49DBFF4DDDFF51DE
                FF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7F
                E9FF83EAFF88EAFF6FBCCB23393D1E2F321E30321F3032203032A5F2FFA9F3FF
                ADF3FFACF3FFA8F3FF8BCDD71B27291F30321E30321E2F321E2F3269B1BF86EA
                FF83EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59
                E0FF54DEFF50DDFF4CDDFF47DBFF142A2F9F9F9FFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFAEA7A0E2CFBFE2CFBFE2CFBFDECBBB9C9793B7B1ADB7B1ADB7B1AD
                B7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1
                ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1ADB7B1AD51514D1D5D6D48D9FB4D
                DDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF
                7BE7FF7FE9FF83EAFF88EAFF1E30341E2F321E2F321E30321F3032203032A5F2
                FFA9F3FFADF3FFACF3FFA8F3FF8BCDD71B27291F30321E30321E2F321E2F321D
                2F327CDAED83EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF
                5CE0FF59E0FF54DEFF50DDFF4CDDFF47DBFF142A2F9F9F9F9689FFFFFFFFFFFF
                FFFFFFFFFFFFD7D6D5C8B8ABE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF524D481B5A68
                45D4F54DDDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6
                FF77E7FF7BE7FF7FE9FF83EAFF86E7FC1522251E2F321E2F321E30321F303220
                3032A5F2FFA9F3FFADF3FFACF3FFA8F3FF8BCDD71B27291F30321E30321E2F32
                1E2F321D2F325B9FAD83EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3
                FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF47DBFF142A2F9F9F9FBFE2FFFF
                FFFFFFFFFFFFFFFFFFFFAEA7A1E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF524D
                481B5A6845D4F54DDDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6E
                E4FF73E6FF77E7FF7BE7FF7FE9FF83EAFF86E7FC1522251E2F321E2F321E3032
                1F3032203032A5F2FFA9F3FFADF3FFACF3FFA8F3FF8BCDD71B27291F30321E30
                321E2F321E2F321D2F325B9FAD83EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69
                E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF47DBFF142A2F9F9F9F
                CFBFFFFFFFFFFFFFFFFFFFD7D5D5C9B9ABE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBF524D481B5A6845D4F54DDDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF
                6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EAFF86E7FC1522251E2F321E2F
                321E30321F3032203032A5F2FFA9F3FFADF3FFACF3FFA8F3FF8BCDD71B27291F
                30321E30321E2F321E2F321D2F325B9FAD83EAFF7EE9FF7AE7FF76E7FF71E6FF
                6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF47DBFF142A
                2F9F9F9FE2CFFFFFFFFFFFFFFFFFFFAFA7A1E2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBF524D481B5A6845D4F54DDDFF51DEFF56DEFF59E0FF5EE1FF62E1
                FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EAFF86E7FC1522251E
                2F321E2F321E30321F3032203032A5F2FFA9F3FFADF3FFACF3FFA8F3FF8BCDD7
                1D2A2D1F30321E30321E2F321E2F321D2F325B9FAD83EAFF7EE9FF7AE7FF76E7
                FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF47
                DBFF142A2F9F9F9FBFE2FFFFFFFFFFFFD5D4D3C9B8ACE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBF524D481B5A6845D4F54DDDFF51DEFF56DEFF59E0FF5E
                E1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EAFF86E7FC
                1522251E2F321E2F321E30321F303220303276ACB5A9F3FFADF3FFACF3FFA8F3
                FF628F972030321F30321E30321E2F321E2F321D2F325B9FAD83EAFF7EE9FF7A
                E7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF
                4CDDFF47DBFF142A2F9F9F9FCFBFFFFFFFFFFFFFB0A8A2E2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBF524D481B5A6845D4F54DDDFF51DEFF56DEFF
                59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EA
                FF86E7FC1522251E2F321E2F321E30321F3032203032213032608A9168919867
                91985980862130322030321F30321E30321E2F321E2F321D2F325B9FAD83EAFF
                7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DE
                FF50DDFF4CDDFF47DBFF142A2F9F9F9FE2CFFFFFFFD6D5D3C8B9ABE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF524D481B5A6845D4F54DDDFF51DE
                FF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7F
                E9FF83EAFF86E7FC1522251E2F321E2F321E30321F3032203032213032223032
                2331322231322230322130322030321F30321E30321E2F321E2F321D2F325B9F
                AD83EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59
                E0FF54DEFF50DDFF4CDDFF47DBFF142A2F9F9F9FBFE2FFFFFFAFA7A1E2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF524D481B5A6845D4F54D
                DDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF
                7BE7FF7FE9FF83EAFF86E7FC1522241E2F321E2F321E30321F30322030322130
                322230322331322231322230322130322030321F30321E30321E2F321E2F321D
                2F325B9FAD83EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF
                5CE0FF59E0FF54DEFF50DDFF4CDDFF47DBFF142A2F9F9F9FCFBFD6D4D3C9BAAC
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF524D481B5A68
                45D4F54DDDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6
                FF77E7FF69C6DA7EE6FC83EAFF87EAFE2C4A4F1622251E2F321E30321F303220
                30322130322230322331322231322230322130322030321F30321E30321E2F32
                1E2F322D4B5170C3D583EAFF7EE9FF67C4D776E7FF71E6FF6EE4FF69E3FF65E3
                FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF47DBFF142A2F9F9F9F544DDCDB
                DBC1B3A6E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF524D
                481B5A6845D4F54DDDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6E
                E4FF73E6FF77E7FF24424825434A6CC1D288EAFF8CECFF3A5F66111B1D1E2E31
                1F30322030322130322230322331322231322230322130322030321F30321E30
                321E2F32304E548AEBFE86EAFF79D9ED335B6316282C76E7FF71E6FF6EE4FF69
                E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF47DBFF142A2F9F9F9F
                EF16FFFFFFAEA7A1E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBF524D481B5A6845D4F54DDDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF
                6BE4FF6EE4FF73E6FF77E7FF6ECEE328474E1220235B9CAA8CECFF90EDFF6099
                A51F30331C292C2030322130322230322331322231322230322130322030321F
                30321B2A2D5588928FEDFF8BECFF66B4C420383C2340465FB5C676E7FF71E6FF
                6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF47DBFF142A
                2F9F9F9FD1FBFFFFFFDDDCDCC1B3A6E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBF524D481B5A6845D4F54DDDFF51DEFF56DEFF59E0FF5EE1FF62E1
                FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF467D88101B1D385F668A
                E3F595EDFF7BC2CF213336182325213032223032233132223132223032213032
                2030321D2C2E75B8C593EDFF8FEDFF45747E1C2F322F525A7BE3F87AE7FF76E7
                FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF47
                DBFF142A2F9F9F9F17D2FFFFFFFFFFFFAFA7A2E2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBF524D481B5A6845D4F54DDDFF51DEFF56DEFF59E0FF5E
                E1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EAFF55929F
                0C131535585E89DBEB98EFFF9BEDFC354D521822232230322331322231322230
                3221303238535889D5E298EFFF92EDFE4069701D2F32355D6583EAFF7EE9FF7A
                E7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF
                4CDDFF47DBFF142A2F9F9F9FFF1CFFFFFFFFFFFFDFDEDDC1B2A5E2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBF524D481B5A6845D4F54DDDFF51DEFF56DEFF
                59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EA
                FF88EAFF88E6F82B464C16222469A5B09DF0FFA1F0FF74AAB30E1415212E3022
                31321E2B2D557D84A0F0FF9BF0FF79BDCA1E2F321E2F321D2F3268B6C683EAFF
                7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DE
                FF50DDFF4CDDFF47DBFF142A2F9F9F9FD7FFFFFFFFFFFFFFFFFFFFB0AAA4E2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF524D481B5A6845D4F54DDDFF51DE
                FF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7F
                E9FF83EAFF88EAFF8CECFF8FECFE34545A121D1E4D747BA1F0FFA5F2FF8AC6D0
                0F1415141C1D7FB7C0A4F2FFA0F0FF69A2AC1E2F311E2F321E2F321D2F3268B6
                C683EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59
                E0FF54DEFF50DDFF4CDDFF47DBFF142A2F9F9F9F2DD8FFFFFFFFFFFFFFFFFFE0
                DFDEC2B2A5E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF524D481B5A6845D4F54D
                DDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF
                7BE7FF7FE9FF83EAFF88EAFF8CECFF90EDFF95EDFF73B6C20E15161D2A2D8ACA
                D5A9F3FFADF3FFACF3FFA8F3FF9BE5F13F5F641A282A2C44491B2B2E1E2F321D
                2F3267B5C583EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF
                5CE0FF59E0FF54DEFF50DDFF4CDDFF47DBFF142A2F9F9F9FFF31FFFFFFFFFFFF
                FFFFFFFFFFFFB1AAA4E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CF
                BFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF524D481B5A68
                45D4F54DDDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6
                FF77E7FF7BE7FF7FE9FF83EAFF88EAFF8CECFF90EDFF95EDFF98EFFF88D1DE2B
                40441B26287BB1BAADF3FFACF3FF86C1CB2B3D41293C4079BBC674B8C4284045
                23393D34575E84E6F983EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3
                FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF47DBFF142A2F9F9F9FDCFFFFFF
                FFFFFFFFFFFFFFFFFFFFE1E0DFC2B2A5E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2
                CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF
                E2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBFE2CFBF524D
                481B5A6845D4F54DDDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6E
                E4FF73E6FF77E7FF7BE7FF7FE9FF83EAFF88EAFF8CECFF90EDFF95EDFF98EFFF
                9DF0FFA1F0FF547A80101718476468577A80202E303D5A5E9FEFFC9BF0FF98EF
                FF93EDFF8FEDFF8BECFF86EAFF83EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69
                E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF47DBFF142A2F9F9F9F
                43DEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB2ABA6E2CFBFE2CFBFE2CFBFDECBBB
                9F9B98C1BCB9C1BCB9C1BCB9C1BCB9C1BCB9C1BCB9C1BCB9C1BCB9C1BCB9C1BC
                B9C1BCB9C1BCB9C1BCB9C1BCB9C1BCB9C1BCB9C1BCB9C1BCB9C1BCB9C1BCB9C1
                BCB953524F1E5E6D47D9FB4DDDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF
                6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EAFF88EAFF8CECFF90EDFF95ED
                FF98EFFF9DF0FFA1F0FFA5F2FF80B7C00E1314141C1E70A2AAA4F2FFA0F0FF9B
                F0FF98EFFF93EDFF8FEDFF8BECFF86EAFF83EAFF7EE9FF7AE7FF76E7FF71E6FF
                6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF47DBFF142A
                2F9F9F9FFC48FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E0DFC2B3A7E2CFBFE2CF
                BFDBC9B9C4C3C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF5D5F5F1E5F6F49DBFF4DDDFF51DEFF56DEFF59E0FF5EE1FF62E1
                FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EAFF88EAFF8CECFF90
                EDFF95EDFF98EFFF9DF0FFA1F0FFA5F2FFA9F3FFADF3FFACF3FFA8F3FFA4F2FF
                A0F0FF9BF0FF98EFFF93EDFF8FEDFF8BECFF86EAFF83EAFF7EE9FF7AE7FF76E7
                FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF4CDDFF47
                DBFF142A2F9F9F9FE0FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB2ACA6E2
                CFBFE2CFBFDBC9B9C4C3C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF5E60601D5C6B49DBFF4DDDFF51DEFF56DEFF59E0FF5E
                E1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EAFF88EAFF
                8CECFF90EDFF95EDFF98EFFF9DF0FFA1F0FFA5F2FFA9F3FFADF3FFACF3FFA8F3
                FFA4F2FFA0F0FF9BF0FF98EFFF93EDFF8FEDFF8BECFF86EAFF83EAFF7EE9FF7A
                E7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DEFF50DDFF
                4CDDFF47DBFF13272BA0A0A059E3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                E2E1E0C2B4A7E2CFBFDBC9B9C4C3C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF82828216373F49DBFF4DDDFF51DEFF56DEFF
                59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7FE9FF83EA
                FF88EAFF8CECFF90EDFF95EDFF98EFFF9DF0FFA1F0FFA5F2FFA9F3FFADF3FFAC
                F3FFA8F3FFA4F2FFA0F0FF9BF0FF98EFFF93EDFF8FEDFF8BECFF86EAFF83EAFF
                7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59E0FF54DE
                FF50DDFF4CDDFF45D6F8030506C2C2C2FF5DFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFB3ADA7E2CFBFDBC9B9C4C3C2FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB4B4B4060B0C47D5F64DDDFF51DE
                FF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF7BE7FF7F
                E9FF83EAFF88EAFF8CECFF90EDFF95EDFF98EFFF9DF0FFA1F0FFA5F2FFA9F3FF
                ADF3FFACF3FFA8F3FFA4F2FFA0F0FF9BF0FF98EFFF93EDFF8FEDFF8BECFF86EA
                FF83EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF5CE0FF59
                E0FF54DEFF50DDFF4CDDFF36A9C4010101F2F2F2E6FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFE3E2E1C1B4A7DBC9B9C4C3C2FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F3F3181C1E2365754D
                DDFF51DEFF56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6FF77E7FF
                7BE7FF7FE9FF83EAFF88EAFF8CECFF90EDFF95EDFF98EFFF9DF0FFA1F0FFA5F2
                FFA9F3FFADF3FFACF3FFA8F3FFA4F2FFA0F0FF9BF0FF98EFFF93EDFF8FEDFF8B
                ECFF86EAFF83EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3FF61E1FF
                5CE0FF59E0FF54DEFF50DDFF4BDAFC1532384D5051FFFFFF6DE8FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB4AEA9DBC9B9C4C3C2FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBDBD
                0304042C7F9251DDFE56DEFF59E0FF5EE1FF62E1FF66E3FF6BE4FF6EE4FF73E6
                FF77E7FF7BE7FF7FE9FF83EAFF88EAFF8CECFF90EDFF95EDFF98EFFF9DF0FFA1
                F0FFA5F2FFA9F3FFADF3FFACF3FFA8F3FFA4F2FFA0F0FF9BF0FF98EFFF93EDFF
                8FEDFF8BECFF86EAFF83EAFF7EE9FF7AE7FF76E7FF71E6FF6EE4FF69E3FF65E3
                FF61E1FF5CE0FF59E0FF54DEFF4CD3F2205560111415E9E9E9FFFFFFFF74FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E3E2BBADA0C4C3C2FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF9E9E9E0A0D0D17363E32819446AEC64FBED653BFD656BFD65AC0D65D
                C0D661C2D664C3D668C4D66BC4D66EC4D673C6D676C7D679C8D67EC8D680CAD6
                84CAD688CBD68BCBD68FCDD692CED691CED68ECDD68ACBD687CBD683CAD680CA
                D67CC8D679C8D675C7D671C6D66EC4D66AC4D667C4D664C3D65FC2D65DC0D659
                BFD655BFD653BFD64EBDD641A4B92E76870F1E211E2324C8C8C8FFFFFFFFFFFF
                ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAEA7A2
                C4C3C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFE1E1E1646666292E2F020303000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000000000000000000080A0B363C3C7E7E7EF5F5F5FFFFFFFFFF
                FFFFFFFF5293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFE3E2E1DEDDDCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFC9494945555
                555858585C5C5C6060606666666969697272727E7E7E8686868C8C8C9494949B
                9B9BA3A3A3AAAAAAB1B1B1B8B8B8C0C0C0C8C8C8CECECECCCCCCC6C6C6BEBEBE
                B7B7B7AFAFAFA9A9A9A1A1A19999999292928B8B8B8484847C7C7C7070705E5E
                5E5A5A5A5454545050504B4B4B4D4D4DBBBBBBFEFEFEFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF0807FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8D8D8D7E7E7E8A8A8A919191
                999999A0A0A0A8A8A8AFAFAFB7B7B7BEBEBEC6C6C6CECECED5D5D5D3D3D3CCCC
                CCC4C4C4BDBDBDB5B5B5AEAEAEA6A6A69E9E9E9797978F8F8F888888797979AE
                AEAEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFD7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEA4A4A4A3A3
                A3A5A5A5A7A7A7A8A8A8ABABABACACACAEAEAE8585853E3E3E5A5A5A7070706C
                6C6C5757573E3E3E979797AEAEAEACACACAAAAAAA8A8A8A6A6A6A5A5A5A3A3A3
                BCBCBCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD0D0FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6565657272729D9D9D
                C8C8C8BDBDBD929292686868888888FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF243DFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6565657272
                729D9D9DC8C8C8BDBDBD929292686868888888FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB2
                B2B24343435F5F5F7575757373735C5C5C454545D2D2D2FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              Layout = blGlyphTop
              ParentFont = False
              TabOrder = 2
              WordWrap = True
              OnClick = m_MoveToHomeBtnClick
            end
          end
        end
        object KeyboardPanel: TPanel
          Left = 0
          Top = 360
          Width = 345
          Height = 412
          Align = alBottom
          BevelKind = bkFlat
          BevelOuter = bvNone
          Color = clGradientActiveCaption
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object Label4: TLabel
            Left = 0
            Top = 0
            Width = 341
            Height = 22
            Align = alTop
            Caption = #1047#1085#1072#1095#1077#1085#1080#1077' '#1089#1084#1077#1097#1077#1085#1080#1103':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -18
            Font.Name = 'Myriad Pro'
            Font.Style = []
            ParentFont = False
            ExplicitWidth = 163
          end
          object GridPanel8: TGridPanel
            Left = 0
            Top = 22
            Width = 341
            Height = 56
            Align = alTop
            BevelOuter = bvNone
            ColumnCollection = <
              item
                Value = 19.999999995410910000
              end
              item
                Value = 19.999999998711160000
              end
              item
                Value = 20.000000003057640000
              end
              item
                Value = 20.000000002583230000
              end
              item
                Value = 20.000000000237070000
              end>
            ControlCollection = <
              item
                Column = 0
                Control = off1_Btn
                Row = 0
              end
              item
                Column = 1
                Control = off2_Btn
                Row = 0
              end
              item
                Column = 2
                Control = off3_Btn
                Row = 0
              end
              item
                Column = 3
                Control = off4_Btn
                Row = 0
              end
              item
                Column = 4
                Control = off5_Btn
                Row = 0
              end>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -22
            Font.Name = 'Verdana'
            Font.Style = []
            ParentBackground = False
            ParentColor = True
            ParentFont = False
            RowCollection = <
              item
                Value = 100.000000000000000000
              end>
            TabOrder = 0
            object off1_Btn: TSpeedButton
              Tag = 10
              AlignWithMargins = True
              Left = 1
              Top = 1
              Width = 66
              Height = 54
              Margins.Left = 1
              Margins.Top = 1
              Margins.Right = 1
              Margins.Bottom = 1
              Align = alClient
              Caption = '10'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Verdana'
              Font.Style = []
              ParentFont = False
              OnClick = onSetOffsetValueBtn
              ExplicitLeft = 24
              ExplicitTop = 16
              ExplicitWidth = 23
              ExplicitHeight = 22
            end
            object off2_Btn: TSpeedButton
              Tag = 50
              AlignWithMargins = True
              Left = 69
              Top = 1
              Width = 66
              Height = 54
              Margins.Left = 1
              Margins.Top = 1
              Margins.Right = 1
              Margins.Bottom = 1
              Align = alClient
              Caption = '50'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Verdana'
              Font.Style = []
              ParentFont = False
              OnClick = onSetOffsetValueBtn
              ExplicitLeft = 24
              ExplicitTop = 16
              ExplicitWidth = 23
              ExplicitHeight = 22
            end
            object off3_Btn: TSpeedButton
              Tag = 100
              AlignWithMargins = True
              Left = 137
              Top = 1
              Width = 66
              Height = 54
              Margins.Left = 1
              Margins.Top = 1
              Margins.Right = 1
              Margins.Bottom = 1
              Align = alClient
              Caption = '100'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Verdana'
              Font.Style = []
              ParentFont = False
              OnClick = onSetOffsetValueBtn
              ExplicitLeft = 24
              ExplicitTop = 16
              ExplicitWidth = 23
              ExplicitHeight = 22
            end
            object off4_Btn: TSpeedButton
              Tag = 200
              AlignWithMargins = True
              Left = 205
              Top = 1
              Width = 66
              Height = 54
              Margins.Left = 1
              Margins.Top = 1
              Margins.Right = 1
              Margins.Bottom = 1
              Align = alClient
              Caption = '200'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Verdana'
              Font.Style = []
              ParentFont = False
              OnClick = onSetOffsetValueBtn
              ExplicitLeft = 206
              ExplicitTop = 3
              ExplicitHeight = 52
            end
            object off5_Btn: TSpeedButton
              Tag = 300
              AlignWithMargins = True
              Left = 273
              Top = 1
              Width = 67
              Height = 54
              Margins.Left = 1
              Margins.Top = 1
              Margins.Right = 1
              Margins.Bottom = 1
              Align = alClient
              Caption = '300'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -22
              Font.Name = 'Verdana'
              Font.Style = []
              ParentFont = False
              OnClick = onSetOffsetValueBtn
              ExplicitLeft = 24
              ExplicitTop = 16
              ExplicitWidth = 23
              ExplicitHeight = 22
            end
          end
          object TouchKeyboard1: TTouchKeyboard
            AlignWithMargins = True
            Left = 3
            Top = 81
            Width = 335
            Height = 324
            Align = alClient
            GradientEnd = clActiveCaption
            GradientStart = clActiveCaption
            Layout = 'NumPad'
          end
        end
      end
      object OtherPage: TTabSheet
        Caption = 'OtherPage'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object ClearBScanBtn: TBitBtn
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 339
          Height = 73
          Align = alTop
          Caption = #1054#1095#1080#1089#1090#1080#1090#1100' B-'#1088#1072#1079#1074#1077#1088#1090#1082#1091
          TabOrder = 0
          WordWrap = True
          OnClick = ClearBScanBtnClick
        end
        object ResetPathEncoderBtn: TBitBtn
          AlignWithMargins = True
          Left = 3
          Top = 82
          Width = 339
          Height = 73
          Align = alTop
          Caption = #1057#1073#1088#1086#1089#1080#1090#1100' '#1076#1072#1090#1095#1080#1082' '#1087#1091#1090#1080
          TabOrder = 1
          WordWrap = True
          OnClick = ResetPathEncoderBtnClick
        end
      end
    end
    object HidePanelBtn: TBitBtn
      AlignWithMargins = True
      Left = 4
      Top = 823
      Width = 353
      Height = 43
      Align = alBottom
      Caption = #1059#1073#1088#1072#1090#1100' '#1087#1072#1085#1077#1083#1100
      TabOrder = 1
      WordWrap = True
      OnClick = HidePanelBtnClick
    end
  end
  object TestTimer: TTimer
    Enabled = False
    Left = 345
    Top = 55
  end
  object ImageList1: TImageList
    Height = 32
    Width = 32
    Left = 416
    Top = 56
    Bitmap = {
      494C0101040014008C0020002000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000800000004000000001002000000000000080
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F6FAF900B7DA
      CC006DB5980043A07B002590660014885A0014885A002590660043A07B006DB5
      9800B7DACC00F6FAF90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E0EFE9007EBEA400168B5D001E9D
      710026B088002AB892002CBF9A002DC29E002DC29E002CBF9A002AB8920026B0
      88001E9D7100168B5D007EBEA400E0EFE9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E6E6E600A7A7A700A7A7A700A7A7A700A7A7
      A700A7A7A700A7A7A700A7A7A700A7A7A700A7A7A700A7A7A700A7A7A700A7A7
      A700A7A7A700A7A7A700A7A7A700A7A7A700E6E6E60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000084C0A800188D600025AB83002DC19D002DC2
      9E002DC29E002DC29E002DC29E002DC29E002DC29E002DC29E002DC29E002DC2
      9E002DC29E002DC19D0025AB8300188D600084C0A80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000004B4B4B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004B4B4B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DBEBE50073B69B0023A57B002EC19C002DC29E002DC29E002DC2
      9E002DC29E002DC29E002DC29E002DC29E002DC29E002DC29E002DC29E002DC2
      9E002DC29E002DC29E002DC29E002DC19C0023A57B006EB69900CEE6DC000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F3F3F0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D3D3D30044836A002FBB930037D1AE002FC6A2002DC29E002DC29E002DC2
      9E002DC29E002DC29E002DC29E002DC29E002DC29E002DC29E002DC29E002DC2
      9E002DC29E002DC29E002DC29E002DC29E002EC39F0028B18A00198C5F00B5D9
      CB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F3F3F0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E4E4
      E4007B7B7B00B3C4BE00219A6E003CD8B40036D0AD002FC6A2002DC29E002DC2
      9E002DC29E002DC29E002DC29E002DC29E002DC29E002DC29E002DC29E002DC2
      9E002DC29E002DC29E002DC29E002DC29E002DC29E002DC39F002BB892001B8D
      5F00D1E7DE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D1D1D1009898980067676700363636001E1E1E004B4B4B007D7D7D00AEAE
      AE00EFEFEF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F0000000000000000006F6F6F006F6F
      6F002D2D2D0000000000141414006F6F6F006F6F6F0014141400000000002D2D
      2D006F6F6F006F6F6F0000000000000000003F3F3F0000000000000000000000
      0000000000000000000000000000000000000000000000000000FDFDFD00ADAD
      AD00C0C0C000EEEEEE0086BDA700229A6E003DD8B40036D0AD002FC6A2002EC3
      9F002FC6A20032C9A60034CDA90035CEAB0035CEAB0034CDA90032CAA6002FC6
      A2002EC39F002DC29E002DC29E002DC29E002DC29E002DC29E002EC39F002AB2
      8B0074B89C00FCFDFD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F2F2F2006666
      6600000000000000000000000000000000000000000000000000000000000000
      000017171700AEAEAE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      000067676700000000002F2F2F0000000000000000002F2F2F00000000006767
      6700000000000000000000000000000000003F3F3F0000000000000000000000
      0000000000000000000000000000000000000000000000000000BBBBBB00ACAC
      AC00EFEFEF00EFEFEF00EFEFEF0087BEA800239B6F003DD8B40037D2AF0034CD
      AA0038D3B0003EDCB90041E2BF0041E0BD0040DFBB0042E3C0003EDDBA0039D5
      B10033CCA8002FC5A1002DC29E002DC29E002DC29E002DC29E002DC29E002EC4
      A00027A9800089C3AC0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BBBBBB001F1F1F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000058585800ECECEC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      000067676700000000002F2F2F0000000000000000002F2F2F00000000006767
      6700000000000000000000000000000000003F3F3F0000000000000000000000
      00000000000000000000000000000000000000000000EEEEEE0085858500E7E7
      E700EFEFEF00EFEFEF00EFEFEF00EFEFEF0088BFA800249B700040DCB80041E1
      BE003ACEA8002BAB810020926500299268002E956B001F8F63002AA97F0038C8
      A2003EDDBA0036D0AC002FC6A2002DC29E002DC29E002DC29E002DC29E002DC2
      9E0030C49F0020926600E1F0EA00000000000000000000000000000000000000
      0000000000000000000000000000ECECEC00555555001F1F1F009E9E9E000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EBEBEB004E4E4E0029292900BCBCBC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B2B2B20001010100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000025252500F6F6F60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      000067676700000000002F2F2F0000000000000000002F2F2F00000000006767
      6700000000000000000000000000000000003F3F3F0000000000000000000000
      00000000000000000000000000000000000000000000B8B8B800BBBBBB00F0F0
      F000F0F0F000F0F0F000F0F0F000F0F0F000F0F0F00089BFA900259C71002CAB
      81005CAC8C00B8DACD00F4F9F7000000000000000000F9FCFB00BDDDD0006CB4
      970028A379003DD8B40036D0AC002FC5A1002DC29E002DC29E002DC29E002DC2
      9E002EC39F002BB18A0083C0A700000000000000000000000000000000000000
      00000000000000000000EFEFEF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000002020200000000000000000000000000C8C8C800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EFEFEF001A1A1A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007171710000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      000067676700000000002F2F2F0000000000000000002F2F2F00000000006767
      6700000000000000000000000000000000003F3F3F0000000000000000000000
      000000000000000000000000000000000000F7F7F70087878700EDEDED00F1F1
      F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100ECECEC0084958E0099BA
      AD00000000000000000000000000000000000000000000000000000000000000
      000091C7B20025996F003BD5B10033CCA8002EC39F002DC29E002DC29E002DC2
      9E002DC29E0031C7A40023936800F1F8F5000000000000000000000000000000
      000000000000F0F0F00035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000808080000000000000000000000000010101000D2D2D2000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005F5F5F000000000000000000000000000000
      000015151500B0B0B0005F5F5F000000000000000000040404008F8F8F006D6D
      6D000000000000000000000000000000000002020200C4C4C400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      000067676700000000002F2F2F0000000000000000002F2F2F00000000006767
      6700000000000000000000000000000000003F3F3F0000000000000000000000
      000000000000000000000000000000000000D1D1D100ABABAB00F1F1F100F1F1
      F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100AAAAAA00BBBBBB000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000095C9B4002BA47B0039D5B20030C6A2002DC29E002DC29E002DC2
      9E002DC29E0030C6A2002BA98100AFD6C6000000000000000000000000000000
      0000F3F3F3000000000000000000000000000000000003030300A6A6A6000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F5F5F5003737370000000000000000000000000000000000E3E3
      E300000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C2C2C200000000000000000000000000000000001515
      1500D2D2D20000000000FDFDFD006161610004040400ABABAB00000000000000
      000078787800000000000000000000000000000000002A2A2A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      000067676700000000002F2F2F0000000000000000002F2F2F00000000006767
      6700000000000000000000000000000000003F3F3F0000000000000000000000
      000000000000000000000000000000000000AFAFAF00CBCBCB00F2F2F200F2F2
      F200F2F2F200F2F2F200F2F2F200F2F2F200D9D9D900A0A0A000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006FB5990036C39E0032CAA6002DC29E002DC29E002DC2
      9E002DC29E002EC4A00030BA940071B69B00000000000000000000000000F5F5
      F5003E3E3E0000000000000000000000000000000000A0A0A000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F0F0F000000000000000000000000000000000002525
      2500EAEAEA000000000000000000000000000000000000000000000000000000
      000000000000000000007E7E7E0000000000000000000000000000000000C3C3
      C300000000000000000000000000FDFDFD00C9C9C90000000000000000000000
      0000000000005B5B5B0000000000000000000000000000000000E8E8E8000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      000067676700000000002F2F2F0000000000000000002F2F2F00000000006767
      6700000000000000000000000000000000003F3F3F0000000000000000000000
      0000000000000000000000000000000000009A9A9A00E0E0E000F2F2F200F2F2
      F200F2F2F200F2F2F200F2F2F200F2F2F200ADADAD00D3D3D300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C9E3D9002DA47B0034CEAA002DC29E002DC29E002DC2
      9E002DC29E002DC29E0033C4A0004AA27F000000000000000000F7F7F7000000
      0000000000000000000000000000010101009B9B9B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E3E3E3001D1D1D0000000000000000000000
      000000000000F5F5F50000000000000000000000000000000000000000000000
      000000000000000000004B4B4B00000000000000000000000000000000009797
      9700000000000000000000000000000000000000000000000000000000000000
      0000FAFAFA004444440000000000000000000000000000000000B7B7B7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      000067676700000000002F2F2F0000000000000000002F2F2F00000000006767
      6700000000000000000000000000000000003F3F3F0000000000000000000000
      0000000000000000000000000000000000008B8B8B00F0F0F000F3F3F300F3F3
      F300F3F3F300F3F3F300F3F3F300F3F3F30095959500F1F1F100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FCFDFD002892680036CFAA002EC39F002DC29E002DC2
      9E002DC29E002DC29E0034CCA8002D946B0000000000F9F9F900484848000000
      0000000000000000000000000000020202001111110011111100111111001111
      1100111111001111110011111100080808000303030011111100111111001111
      1100111111001111110011111100111111000909090000000000000000000000
      00000000000043434300F9F9F900000000000000000000000000000000000000
      0000000000000000000018181800000000000000000000000000000000000101
      01009D9D9D00000000000000000000000000000000000000000000000000FBFB
      FB00515151000000000000000000000000000000000000000000868686000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      000067676700000000002F2F2F0000000000000000002F2F2F00000000006767
      6700000000000000000000000000000000003F3F3F0000000000000000000000
      0000000000000000000000000000000000008E8E8E00F4F4F400F4F4F400F4F4
      F400F4F4F400F4F4F400F4F4F400F4F4F4008D8D8D00FCFCFC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000429F7A0034C9A4002EC39F002DC29E002DC2
      9E002DC29E002DC29E0034CDAA002A956B000000000018181800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000020202000000000000000000000000000000000000000
      000000000000F4F4F40000000000000000000000000000000000000000000000
      000006060600E1E1E10000000000000000000000000000000000000000009797
      9700000000000000000000000000000000000000000000000000646464000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      000067676700000000002F2F2F0000000000000000002F2F2F00000000006767
      6700000000000000000000000000000000003F3F3F0000000000000000000000
      00000000000000000000000000000000000091919100F4F4F400F4F4F400F4F4
      F400F4F4F400F4F4F400F4F4F400F4F4F4008D8D8D0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000047A07D0033C6A2002DC29E002DC29E002DC2
      9E002DC29E002DC29E0035CEAB002C956D000000000011111100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000018181800000000000000000000000000000000000000
      000000000000000000001E1E1E00000000000000000000000000000000000404
      0400ABABAB00000000000000000000000000000000000000000000000000FDFD
      FD006161610000000000000000000000000000000000000000008C8C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      000067676700000000002F2F2F0000000000000000002F2F2F00000000006767
      6700000000000000000000000000000000003F3F3F0000000000000000000000
      00000000000000000000000000000000000092929200F2F2F200F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F5009D9D9D00EFEFEF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FAFCFB002D956D0032C9A5002DC29E002DC29E002DC2
      9E002DC29E002EC4A00037D0AC0032956F0000000000F9F9F900484848000000
      0000000000000000000000000000020202001111110011111100111111001111
      1100111111001111110011111100080808000303030011111100111111001111
      1100111111001111110011111100111111000909090000000000000000000000
      00000000000043434300F9F9F900000000000000000000000000000000000000
      000000000000000000005151510000000000000000000000000000000000A0A0
      A000000000000000000000000000000000000000000000000000000000000000
      0000FDFDFD004C4C4C0000000000000000000000000000000000BDBDBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      000067676700000000002F2F2F0000000000000000002F2F2F00000000006767
      6700000000000000000000000000000000003F3F3F0000000000000000000000
      000000000000000000000000000000000000A3A3A300E5E5E500F6F6F600F6F6
      F600F6F6F600F6F6F600F6F6F600F6F6F600B7B7B700D3D3D300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C4E0D50031A57E002FC6A2002DC29E002DC29E002DC2
      9E002DC29E0030C6A20038CBA70051A584000000000000000000F7F7F7000000
      0000000000000000000000000000010101009999990000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E3E3E3001C1C1C0000000000000000000000
      000000000000F5F5F50000000000000000000000000000000000000000000000
      000000000000000000008484840000000000000000000000000000000000AFAF
      AF00000000000000000000000000FAFAFA00B5B5B50000000000000000000000
      0000FCFCFC004949490000000000000000000000000000000000EEEEEE000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      000067676700000000002F2F2F0000000000000000002F2F2F00000000006767
      6700000000000000000000000000000000003F3F3F0000000000000000000000
      000000000000000000000000000000000000B9B9B900D3D3D300F6F6F600F6F6
      F600F6F6F600F6F6F600F6F6F600F6F6F600E3E3E30094949400FDFDFD000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006FB59A0032BA95002EC39F002DC29E002DC29E002DC2
      9E002DC29E0032CAA60039C39E0079BAA000000000000000000000000000F5F5
      F5003E3E3E0000000000000000000000000000000000A0A0A000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F0F0F000000000000000000000000000000000002525
      2500EAEAEA000000000000000000000000000000000000000000000000000000
      00000000000000000000CECECE00020202000000000000000000000000000A0A
      0A00BDBDBD0000000000FAFAFA0050505000010101009696960000000000FCFC
      FC005B5B5B000000000000000000000000000000000039393900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F0000000000000000007F7F7F007F7F
      7F003333330000000000171717007F7F7F007F7F7F0017171700000000003333
      33007F7F7F007F7F7F0000000000000000003F3F3F0000000000000000000000
      000000000000000000000000000000000000D9D9D900B7B7B700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700BBBBBB00C3C3C3000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000095C8B40033A47F002FC4A0002DC29E002DC29E002DC29E002DC2
      9E002EC4A00035CFAC0038B18B00B6D9CC000000000000000000000000000000
      0000F3F3F3000000000000000000000000000000000003030300A6A6A6000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F5F5F5003737370000000000000000000000000000000000E3E3
      E300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000747474000000000000000000000000000000
      00000A0A0A009595950050505000000000000000000001010100777777005353
      53000000000000000000000000000000000007070700D5D5D500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F3F3F0000000000000000000000
      000000000000000000000000000000000000FAFAFA009B9B9B00F3F3F300F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F5F5F500B1B1B100C1C1
      C100F0F0F000000000000000000000000000000000000000000000000000F0F7
      F4008CC3AE0033A079002FC29D002DC29E002DC29E002DC29E002DC29E002DC2
      9E0031C8A4003BD6B300349A7300F5FAF8000000000000000000000000000000
      000000000000F0F0F00035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000808080000000000000000000000000010101000D2D2D2000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F7F7F7002727270000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008686860000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F3F3F0000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600CBCBCB00F8F8
      F800F8F8F800F8F8F800F8F8F800F8F8F800F8F8F800F8F8F800F8F8F800CDCD
      CD009C9C9C00D2D2D200EFEFEF000000000000000000E4F1EC00B0D6C7003598
      710033AF8A002EC39F002DC29E002DC29E002DC29E002DC29E002DC29E002EC4
      A00036D0AC003CBF9A008FC5B000000000000000000000000000000000000000
      00000000000000000000EFEFEF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000002020200000000000000000000000000C8C8C800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C5C5C50007070700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000039393900FCFCFC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003F3F3F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F3F3F0000000000000000000000
      00000000000000000000000000000000000000000000F3F3F300A0A0A000F2F2
      F200F9F9F900F9F9F900F9F9F900F9F9F900F9F9F900F9F9F900F9F9F900F9F9
      F900EAEAEA00C2C2C200A9A9A9009A9A9A0060988200379F790035AA850031BC
      97002DC29E002DC29E002DC29E002DC29E002DC29E002DC29E002EC39F0033CB
      A7003DD7B300389C7600E7F2EE00000000000000000000000000000000000000
      0000000000000000000000000000ECECEC005757570025252500A0A0A0000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EBEBEB00505050002E2E2E00BCBCBC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D5D5D500353535000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000303030078787800F8F8F8000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D9D9D900D2D2D200D0D0D000D0D0D000D0D0D000D0D0
      D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0
      D000D0D0D000D0D0D000D0D0D000D0D0D000D2D2D200D9D9D900000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00C0C0
      C000F9F9F900F9F9F900F9F9F900F9F9F900F9F9F900F9F9F900F9F9F900F9F9
      F900F9F9F900F9F9F900F9F9F900F9F9F9003898730034CDAA002DC29E002DC2
      9E002DC29E002DC29E002DC29E002DC29E002DC29E002DC39F0031C9A5003BD7
      B4003DB7930098C9B60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FCFCFC008686
      8600060606000000000000000000000000000000000000000000000000000000
      00002B2B2B00CBCBCB0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9C9C000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C600D4D4D400FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00FAFAFA00FAFAFA003999730034CDAA002DC29E002DC2
      9E002DC29E002DC29E002DC29E002DC29E002EC39F0031C9A5003AD6B2003FC6
      A1008AC3AC000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EBEBEB00B8B8B80087878700565656003E3E3E006B6B6B009D9D9D00CECE
      CE00FBFBFB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007F7F7F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007F7F7F000000
      000000000000000000000000000000000000000000000000000000000000EEEE
      EE00A1A1A100E1E1E100FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00FAFAFA00FAFAFA003B99740034CDAA002DC29E002DC2
      9E002DC29E002DC29E002DC29E002EC4A00033CBA7003BD8B40041D0AD003B9B
      7600DCEDE6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D2D2D200424242003F3F3F003F3F3F003F3F3F00252525000000
      0000000000001F1F1F001F1F1F001F1F1F001F1F1F001F1F1F001F1F1F000000
      000000000000252525003F3F3F003F3F3F003F3F3F0042424200D2D2D2000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E1E1E100A3A3A300D5D5D500FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFB
      FB00FBFBFB00FBFBFB00FBFBFB00FBFBFB003C9A750034CDAA002DC29E002DC2
      9E002DC29E002EC4A00031C8A40036D0AC003DDCB80041C6A3003C9C7700C1DF
      D300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000979797000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009797970000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EEEEEE00C8C8C800C5C5C500F5F5F500FBFBFB00FBFBFB00FBFB
      FB00FBFBFB00FBFBFB00FBFBFB00FBFBFB003D9A760035CEAB002FC4A00030C7
      A30032CAA60036D0AC003CD9B50042DEBB0041B996008DC3AE00DFEEE8000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000979797000000
      0000000000001717170017171700171717001717170017171700171717000000
      0000000000009797970000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D0D0D000A9A9A900D1D1D100F7F7F700FBFB
      FB00FBFBFB00FBFBFB00FBFBFB00FBFBFB003E9B77003BD8B50038D3AF003AD7
      B4003FDEBA0043E1BE0042C4A0003FA07C009BCBB80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009B9B9B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009B9B9B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F5F5F500CECECE00A7A7A700BEBE
      BE00D9D9D900EAEAEA00F5F5F500FCFCFC004FA3830045E2C00044D8B50043CB
      A80042B390003F9D7A0097C9B600EAF4F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F8F8F8008F8F
      8F00878787008787870087878700878787008787870087878700878787008787
      87008F8F8F00F8F8F80000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FCFCFC00E4E4
      E400C8C8C800B7B7B700ADADAD00A6A6A60072A08F004C9F7F0066B093008AC2
      AC00C7E1D700FAFCFB0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000080000000400000000100010000000000000400000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFC003FFFFFFFFFFFFFFFFFFFFFFFFFF
      FF0000FFFFFFFFFFFFFFFFFFFE00007FFE00007FFFFFFFFFFFFFFFFFFE00007F
      F800001FFFFFFFFFFFFFFFFFFE00007FF000000FFFFFFFFFFFFFFFFFFE00007F
      E0000007FFFFFFFFFFF007FFFE00007FC0000003FFFFFFFFFFC003FFFE318C7F
      C0000003FFFFFFFFFF8000FFFE318C7F80000001FE1FF87FFF00007FFE318C7F
      80018001FC1FF83FFE00007FFE318C7F000FF000F81FF81FFE00003FFE318C7F
      001FF800F01FF80FFC04303FFE318C7F003FFC00E03FFC07FC0E781FFE318C7F
      003FFC00C07FFE03FC0FF01FFE318C7F003FFC0080000001FC07E01FFE318C7F
      003FFE0080000001F803E01FFE318C7F007FFE0080000001FC07E01FFE318C7F
      003FFC0080000001FC0FF01FFE318C7F003FFC00C07FFE03FC0E701FFE318C7F
      001FFC00E03FFC07FC04203FFE00007F001FF800F01FF80FFE00003FFE00007F
      0007E000F81FF81FFE00007FFE00007F80018001FC1FF83FFF00007FFE00007F
      80000001FE1FF87FFF8000FFFC00003FC0000003FFFFFFFFFFC003FFF800001F
      E0000007FFFFFFFFFFF007FFF800001FE0000007FFFFFFFFFFFFFFFFF800001F
      F000000FFFFFFFFFFFFFFFFFFFC7E3FFF800001FFFFFFFFFFFFFFFFFFFC003FF
      FE00007FFFFFFFFFFFFFFFFFFFC003FFFF0000FFFFFFFFFFFFFFFFFFFFC003FF
      FFC003FFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
end
