﻿/**
 * @file EnterParamFrameUnit.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef EnterParamFrameUnitH
#define EnterParamFrameUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "BasicFrameUnit.h"
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.Touch.Keyboard.hpp>
#include <Vcl.ValEdit.hpp>
#include <Vcl.Imaging.jpeg.hpp>
#include <Vcl.Samples.Spin.hpp>

#define MF_EPF_FLAG_SEARCH_TYPE         0x01    //!< Режим "Поиск"
#define MF_EPF_FLAG_TEST_TYPE           0x02    //!< Режим "Тест"
#define MF_EPF_FLAG_NEXTFRAME_BSCAN     0x04    //!< Следующая форма - В-развертка (hack)
#define MF_EPF_FLAG_NEXTFRAME_ASCAN     0x08    //!< Следующая форма - А-развертка (hack)
#define MF_EPF_FLAG_CAPTION_CONTROL     0x10    //!< Заголовок "Новый протокол контроля"
#define MF_EPF_FLAG_CAPTION_TEST        0x20    //!< Заголовок "Новый протокол тестирования"
#define MF_EPF_FLAG_CAPTION_HAND        MF_EPF_FLAG_CAPTION_CONTROL  //!< Заголовок "Новый протокол контроля"
#define MF_EPF_FLAG_BTN_NO_REPORT       0x40    //!< Отключить конпку "Без отчета"
#define MF_EPF_FLAG_BTN_CANCEL          0x80    //!< Отключить конпку "Отмена"
//#define MF_EPF_FLAG_TAKE_PHOTO          0x100

//! Флаги для режима Поиск
#define MF_EPF_SEARCH_FLAGS (MF_EPF_FLAG_SEARCH_TYPE | MF_EPF_FLAG_CAPTION_CONTROL | MF_EPF_FLAG_BTN_NO_REPORT | MF_EPF_FLAG_BTN_CANCEL | MF_EPF_FLAG_NEXTFRAME_BSCAN)
//! Флаги для режима Тест
#define MF_EPF_TEST_FLAGS (MF_EPF_FLAG_TEST_TYPE | MF_EPF_FLAG_CAPTION_TEST | MF_EPF_FLAG_BTN_NO_REPORT | MF_EPF_FLAG_BTN_CANCEL | MF_EPF_FLAG_NEXTFRAME_BSCAN)
//! Флаги для режима Ручной
#define MF_EPF_HANDSCAN_FLAGS (MF_EPF_FLAG_SEARCH_TYPE | MF_EPF_FLAG_CAPTION_HAND | MF_EPF_FLAG_BTN_NO_REPORT | MF_EPF_FLAG_BTN_CANCEL | MF_EPF_FLAG_NEXTFRAME_ASCAN)


//---------------------------------------------------------------------------
//! Отображает форму ввода параметров для режима сканирования
class TEnterParamFrame : public TBasicFrame
{
__published:	// IDE-managed Components
    TPanel *CaptionPanel;
    TPanel *Panel1;
    TValueListEditor *ValueList;
    TTouchKeyboard *TouchKeyboard1;
    TPanel *RightButtonPanel;
    TImage *currLinearityImage;
    TPanel *Panel4;
    TButton *NoRepButton;
    TPanel *Panel5;
    TPanel *Panel6;
    TBitBtn *CancelButton;
    TBitBtn *StartButton;
    TBitBtn *setEngLangBtn;
    TBitBtn *setRusLangBtn;
    TLabel *Label1;
    TGridPanel *GridPanel1;
    TSpeedButton *LinearityBtn_Top;
    TSpeedButton *LinearityBtn_Left;
    TSpeedButton *LinearityBtn_Right;
    TPanel *Panel2;
    TBitBtn *PageDownBtn;
    TBitBtn *PageUpBtn;
    TPanel *FiltrPanel;
    TPanel *FiltrPanel_;
    TImage *Image1;
    TLabel *Label2;
    TLabel *Label3;
    TLabel *Label4;
    TLabel *Label5;
    TSpinEdit *SameCount;
    TSpinEdit *DelayDelta;
    TSpinEdit *MaxSpace;
    TSpinEdit *MinLenEdit;
    TCheckBox *FiltrState;
    TBitBtn *BitBtn1;
	TSpinEdit *SpinEdit1;
    void __fastcall ValueListSelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
    void __fastcall ValueListStringsChanging(TObject *Sender);
    void __fastcall ValueListStringsChange(TObject *Sender);
    void __fastcall setRusLangBtnClick(TObject *Sender);
    void __fastcall setEngLangBtnClick(TObject *Sender);
    void __fastcall StartButtonClick(TObject *Sender);
    void __fastcall CancelButtonClick(TObject *Sender);
    void __fastcall LinearityBtnClick(TObject *Sender);
    void __fastcall currLinearityImageClick(TObject *Sender);
    void __fastcall NoRepButtonClick(TObject *Sender);
    void __fastcall PageUpBtnClick(TObject *Sender);
    void __fastcall PageDownBtnClick(TObject *Sender);
    void __fastcall ValueListEditButtonClick(TObject *Sender);
    void __fastcall BitBtn1Click(TObject *Sender);

private:	// User declarations
    void OnMFrameAttach(TBasicFrame* pMain);
    void OnMFrameDetach(TBasicFrame* pMain);
    void OnMFrameCreate(TBasicFrame* pMain);
    void OnMFrameDestroy(TBasicFrame* pMain);

    void LoadValueListData();  //!< Добавить выводимые параметры из конфига в лист значений
    void StoreValueListData(); //!< Добавить выводимые параметры из листа значений в конфиг
    void UpdateValueList(bool bFromData); //!< Обновить лист значений из/в экранную таблицу параметров (в форме)

    void UpdatePhoneImage(int index); //!< Обновить изобрвжение с телефона (загрузить новое)
    void AssignPhoneImage(int index); //!< Вывести в форму изображение с телефона с индексом index

    void StartNextFrame();  //!< Вывести следующую форму (В-разв, А-разв., и т.п.)

    void SaveProtocol();  //!< Сохранить протокол.

    TStringList* ValueListData;
    TStringList *СonclusionSubData;
	TStringList *HardnessSubData;

    TPicture* LinearityImages[3];
    bool bImageIsAssigned[3];
    int currImageIndex;


    bool bIsFirstShow;
    bool bHardnessState;
    DWORD nextframe_flags;
    TACState next_state;
public:		// User declarations
    __fastcall TEnterParamFrame(TComponent* Owner);
    UnicodeString GetFrameName() {return L"TEnterParamFrame";};
    void SetFrameFlags(DWORD flags);
    void SetNextFrameFlags(DWORD flags) {nextframe_flags = flags;}
    void SetNextState(TACState state) {next_state = state;}
};
//---------------------------------------------------------------------------
//extern PACKAGE TEnterParamFrame *EnterParamFrame;
//---------------------------------------------------------------------------
#endif
