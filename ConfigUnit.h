/**
 * @file ConfigUnit.h
 * @author Denis Fedorenko
 */
//---------------------------------------------------------------------------

#ifndef ConfigUnitH
#define ConfigUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.IniFiles.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <TChar.h>
#include <jpeg.hpp>
#include <ExtCtrls.hpp>
#include "IPCameraUnit.h";

//#pragma pack(push)
//#pragma pack(8)

class cACConfig
{
    private:


    public:

	TIniFile * Ini;
	TIniFile * Ini2;

    cACConfig(void);
    ~cACConfig(void);

    void LoadData(void);
    void SaveData(void);

	TDateTime Last_DateTime;     	  //!< ���� / ����� ��������
	int	Last_GangNumber;			  //!< ����� �����
	int	Last_ReportNumber;	 	      //!< ����� ��������� ��������
	int	Last_TestReportNumber;	 	  //!< ����� ��������� ������������
	int Last_PletNumber;        	  //!< ����� �����
	int Last_JointNumber;    	      //!< ����� �����
	float Last_TopStraightness; 	  //!< ��������������� ������ [��]
	float Last_WF_Straightness;	      //!< ��������������� (������� �����) [��]
	float Last_NWF_Straightness;	  //!< ��������������� (��������� �����) [��]
	int	Last_Hardness_; 			  //!< ��������
	int	Last_HardnessUnit; 			  //!< ��. ���. ��������: 1 � HB, 2 - HV, 3 - HRC
	UnicodeString Last_VerSoft;		  //!< ������ ��
	UnicodeString Last_Operator; 	  //!< �������� ���
	UnicodeString Last_DefectCode;    //!< ��� �������
	UnicodeString Last_ValidityGroup; //!< ��. ��������
	UnicodeString Last_�onclusion;    //!< ����������
    int	Last_Hardness[9];             //!< �������� [9 ��������]
    int Last_HardnessState;           //!< �������� - ���������� ��� ���

    //Added by KirillB
    TDateTime Last_HardnessMeasTime;
    TDateTime Last_SSC_TestDateTime;
    TDateTime Last_SS3R_TestDateTime;
    UnicodeString Test_EtalonModelName;


    int BScan_ThTrackBarPos;

	UnicodeString Current_Plaint;   	  //!< �����������
	UnicodeString Current_PlantNumber;   //!< ��������� ����� ���������


	int Buffer[1023];


	bool FotoSide;                    //!< ������� ��������� ���������� (true - ������� ���������)
	int Buffer1[1023];
	TStringList *Operators;
	int Buffer2[1023];

	unsigned int Last_WorkTimeSec;
	TDateTime Last_WorkStartTime;

	int MinLenEdit_;
	int MaxSpace_;
	int DelayDelta_;
	int SameCount_;
	bool ShowFiltrParam_; // ���������� ��������� �������
	bool UseFiltr_;       // ������������ ������ ��� ��������

	bool BScanMeasure[90][4];
	int BScanMeasureTh; // ����� �������� ��������� �� �������� ������

};

//#pragma pack(pop)
//#pragma pack()

//---------------------------------------------------------------------------
class TConfigForm : public TForm
{
__published:	// IDE-managed Components
    TPanel *Panel1;
    TLabel *Label1;
    TButton *Button1;
    TImage *Image1;
    TButton *BackButton;
    TComboBox *Edit1;
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall BackButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TConfigForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TConfigForm *ConfigForm;
//---------------------------------------------------------------------------
#endif
