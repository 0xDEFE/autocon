object ConfigForm: TConfigForm
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'ConfigForm'
  ClientHeight = 486
  ClientWidth = 652
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 0
    Top = 41
    Width = 652
    Height = 445
    Align = alClient
    Stretch = True
    ExplicitTop = 45
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 652
    Height = 41
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 269
      Top = 13
      Width = 30
      Height = 13
      Caption = 'State:'
    end
    object Button1: TButton
      Left = 9
      Top = 6
      Width = 95
      Height = 25
      Caption = 'GetPhoto'
      TabOrder = 0
      OnClick = Button1Click
    end
    object BackButton: TButton
      Tag = -1
      Left = 566
      Top = 2
      Width = 81
      Height = 37
      Caption = #1053#1072#1079#1072#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      WordWrap = True
      OnClick = BackButtonClick
    end
    object Edit1: TComboBox
      Left = 126
      Top = 14
      Width = 115
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 2
      Text = '192.168.100.10'
      Items.Strings = (
        '192.168.100.10'
        '192.168.100.11')
    end
  end
end
