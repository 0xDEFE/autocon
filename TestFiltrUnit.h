//---------------------------------------------------------------------------

#ifndef TestFiltrUnitH
#define TestFiltrUnitH

#include "DataStorage.h"
#include "AutoconMain.h"

void Filter(cJointTestingReport *SrcReps, sFilterParams fp);

extern cAutoconMain *AutoconMain;

#endif
