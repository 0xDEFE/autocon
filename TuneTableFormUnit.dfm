object TuneTableForm: TTuneTableForm
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'TuneTableForm'
  ClientHeight = 993
  ClientWidth = 1004
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object m_EditPanel: TPanel
    AlignWithMargins = True
    Left = 638
    Top = 41
    Width = 363
    Height = 949
    Align = alRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    object Label7: TLabel
      Left = 1
      Top = 40
      Width = 361
      Height = 22
      Align = alTop
      Caption = #1050#1072#1085#1072#1083#1099':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -18
      Font.Name = 'Myriad Pro'
      Font.Style = []
      ParentFont = False
      ExplicitWidth = 63
    end
    object Label8: TLabel
      Left = 1
      Top = 103
      Width = 361
      Height = 22
      Align = alTop
      Caption = #1052#1086#1076#1077#1083#1100' '#1074' '#1053#1054':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -18
      Font.Name = 'Myriad Pro'
      Font.Style = []
      ParentFont = False
      ExplicitWidth = 106
    end
    object m_GroupCombo: TComboFlat
      Left = 1
      Top = 1
      Width = 361
      Height = 39
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -26
      Font.Name = 'Myriad Pro'
      Font.Style = []
      ItemIndex = 0
      ParentFont = False
      TabOrder = 0
      Text = #1043#1088#1091#1087#1087#1072' 3'
      Items.Strings = (
        #1043#1088#1091#1087#1087#1072' 3'
        #1043#1088#1091#1087#1087#1072' 5'
        #1043#1088#1091#1087#1087#1072' 6'
        #1043#1088#1091#1087#1087#1072' 8'
        #1043#1088#1091#1087#1087#1072' 10'
        #1043#1088#1091#1087#1087#1072' 12'
        #1043#1088#1091#1087#1087#1072' 14')
    end
    object m_ModelEdit: TEdit
      Left = 1
      Top = 125
      Width = 361
      Height = 39
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -26
      Font.Name = 'Myriad Pro'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = #1041#1077#1079' '#1084#1086#1076#1077#1083#1080
    end
    object GridPanel7: TGridPanel
      Left = 1
      Top = 62
      Width = 361
      Height = 41
      Align = alTop
      ColumnCollection = <
        item
          Value = 100.000000000000000000
        end
        item
          SizeStyle = ssAbsolute
          Value = 60.000000000000000000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = m_ChannelsEdit
          Row = 0
        end
        item
          Column = 1
          Control = m_SelChannelsBtn
          Row = 0
        end>
      RowCollection = <
        item
          Value = 100.000000000000000000
        end>
      TabOrder = 2
      object m_ChannelsEdit: TEdit
        Left = 1
        Top = 1
        Width = 299
        Height = 39
        Align = alTop
        Anchors = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -26
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object m_SelChannelsBtn: TSpeedButton
        Left = 300
        Top = 1
        Width = 60
        Height = 39
        Align = alClient
        Anchors = []
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -26
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        OnClick = m_SelChannelsBtnClick
        ExplicitLeft = 320
        ExplicitTop = 72
        ExplicitWidth = 23
        ExplicitHeight = 22
      end
    end
    object m_TopCaretGroupBox: TGroupBox
      AlignWithMargins = True
      Left = 4
      Top = 223
      Width = 355
      Height = 154
      Align = alTop
      Caption = #1042#1077#1088#1093#1085#1103#1103' '#1082#1072#1088#1077#1090#1082#1072':'
      DoubleBuffered = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentDoubleBuffered = False
      ParentFont = False
      TabOrder = 3
      object Label1: TLabel
        Left = 2
        Top = 21
        Width = 209
        Height = 22
        Align = alTop
        Caption = #1053#1072#1095#1072#1083#1100#1085#1072#1103' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1072' '#9650':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -18
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 2
        Top = 84
        Width = 79
        Height = 22
        Align = alTop
        Caption = #1044#1077#1083#1100#1090#1072' '#9650':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -18
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
      end
      object GridPanel1: TGridPanel
        Left = 2
        Top = 43
        Width = 351
        Height = 41
        Align = alTop
        ColumnCollection = <
          item
            SizeStyle = ssAbsolute
            Value = 80.000000000000000000
          end
          item
            Value = 100.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_PosTopSpin
            Row = 0
          end
          item
            Column = 1
            Control = m_PosTopTrack
            Row = 0
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 0
        DesignSize = (
          351
          41)
        object m_PosTopSpin: TSpinEdit
          Left = 1
          Top = 1
          Width = 79
          Height = 39
          Anchors = []
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -26
          Font.Name = 'Tahoma'
          Font.Style = []
          Increment = 10
          MaxValue = 1300
          MinValue = 50
          ParentFont = False
          TabOrder = 0
          Value = 50
          OnChange = OnEditPanelSpinChanged
        end
        object m_PosTopTrack: TTrackBar
          Tag = 1
          Left = 82
          Top = 2
          Width = 266
          Height = 37
          Anchors = []
          Max = 1300
          Min = 50
          PageSize = 10
          Frequency = 100
          Position = 50
          ShowSelRange = False
          TabOrder = 1
          ThumbLength = 30
          OnChange = OnEditPanelSpinChanged
        end
      end
      object GridPanel3: TGridPanel
        Left = 2
        Top = 106
        Width = 351
        Height = 41
        Align = alTop
        ColumnCollection = <
          item
            SizeStyle = ssAbsolute
            Value = 80.000000000000000000
          end
          item
            Value = 100.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_DeltaTopSpin
            Row = 0
          end
          item
            Column = 1
            Control = m_DeltaTopTrack
            Row = 0
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 1
        DesignSize = (
          351
          41)
        object m_DeltaTopSpin: TSpinEdit
          Tag = 4
          Left = 1
          Top = 1
          Width = 79
          Height = 39
          Anchors = []
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -26
          Font.Name = 'Tahoma'
          Font.Style = []
          Increment = 10
          MaxValue = 1300
          MinValue = 0
          ParentFont = False
          TabOrder = 0
          Value = 0
          OnChange = OnEditPanelSpinChanged
        end
        object m_DeltaTopTrack: TTrackBar
          Tag = 5
          Left = 82
          Top = 2
          Width = 266
          Height = 37
          Anchors = []
          Max = 1300
          Min = -1300
          PageSize = 10
          Frequency = 100
          ShowSelRange = False
          TabOrder = 1
          ThumbLength = 30
          OnChange = OnEditPanelSpinChanged
        end
      end
    end
    object m_BotCaretGroupBox: TGroupBox
      AlignWithMargins = True
      Left = 4
      Top = 383
      Width = 355
      Height = 154
      Align = alTop
      Caption = #1053#1080#1078#1085#1103#1103' '#1082#1072#1088#1077#1090#1082#1072':'
      DoubleBuffered = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentDoubleBuffered = False
      ParentFont = False
      TabOrder = 4
      object Label2: TLabel
        Left = 2
        Top = 21
        Width = 209
        Height = 22
        Align = alTop
        Caption = #1053#1072#1095#1072#1083#1100#1085#1072#1103' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1072' '#9660':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -18
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 2
        Top = 84
        Width = 79
        Height = 22
        Align = alTop
        Caption = #1044#1077#1083#1100#1090#1072' '#9660':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -18
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
      end
      object GridPanel2: TGridPanel
        Left = 2
        Top = 43
        Width = 351
        Height = 41
        Align = alTop
        ColumnCollection = <
          item
            SizeStyle = ssAbsolute
            Value = 80.000000000000000000
          end
          item
            Value = 100.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_PosBtmSpin
            Row = 0
          end
          item
            Column = 1
            Control = m_PosBtmTrack
            Row = 0
          end>
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 0
        DesignSize = (
          351
          41)
        object m_PosBtmSpin: TSpinEdit
          Tag = 2
          Left = 1
          Top = 1
          Width = 79
          Height = 39
          Anchors = []
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -26
          Font.Name = 'Tahoma'
          Font.Style = []
          Increment = 10
          MaxValue = 1300
          MinValue = 50
          ParentFont = False
          TabOrder = 0
          Value = 50
          OnChange = OnEditPanelSpinChanged
        end
        object m_PosBtmTrack: TTrackBar
          Tag = 3
          Left = 82
          Top = 2
          Width = 266
          Height = 37
          Anchors = []
          Max = 1300
          Min = 50
          PageSize = 10
          Frequency = 100
          Position = 50
          ShowSelRange = False
          TabOrder = 1
          ThumbLength = 30
          OnChange = OnEditPanelSpinChanged
        end
      end
      object GridPanel4: TGridPanel
        Left = 2
        Top = 106
        Width = 351
        Height = 41
        Align = alTop
        ColumnCollection = <
          item
            SizeStyle = ssAbsolute
            Value = 80.000000000000000000
          end
          item
            Value = 100.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_DeltaBtmSpin
            Row = 0
          end
          item
            Column = 1
            Control = m_DeltaBtmTrack
            Row = 0
          end>
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 1
        DesignSize = (
          351
          41)
        object m_DeltaBtmSpin: TSpinEdit
          Tag = 6
          Left = 1
          Top = 1
          Width = 79
          Height = 39
          Anchors = []
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -26
          Font.Name = 'Tahoma'
          Font.Style = []
          Increment = 10
          MaxValue = 1300
          MinValue = 0
          ParentFont = False
          TabOrder = 0
          Value = 0
          OnChange = OnEditPanelSpinChanged
        end
        object m_DeltaBtmTrack: TTrackBar
          Tag = 7
          Left = 82
          Top = 2
          Width = 266
          Height = 37
          Anchors = []
          Max = 1300
          Min = -1300
          PageSize = 10
          Frequency = 100
          ShowSelRange = False
          TabOrder = 1
          ThumbLength = 30
          OnChange = OnEditPanelSpinChanged
        end
      end
    end
    object GridPanel10: TGridPanel
      Left = 1
      Top = 164
      Width = 361
      Height = 56
      Align = alTop
      BevelOuter = bvNone
      ColumnCollection = <
        item
          Value = 50.000000000000000000
        end
        item
          Value = 50.000000000000000000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = m_EnableTopCaret
          Row = 0
        end
        item
          Column = 1
          Control = m_EnableBotCaret
          Row = 0
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      RowCollection = <
        item
          Value = 100.000000000000000000
        end>
      TabOrder = 5
      object m_EnableTopCaret: TSpeedButton
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 174
        Height = 50
        Align = alClient
        AllowAllUp = True
        GroupIndex = 10
        Down = True
        Caption = #1042#1077#1088#1093'. '#8593
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = m_EnableTopCaretClick
        ExplicitLeft = 4
        ExplicitTop = 0
        ExplicitWidth = 109
        ExplicitHeight = 33
      end
      object m_EnableBotCaret: TSpeedButton
        AlignWithMargins = True
        Left = 183
        Top = 3
        Width = 175
        Height = 50
        Align = alClient
        AllowAllUp = True
        GroupIndex = 11
        Down = True
        Caption = #1053#1080#1078'. '#8595
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = m_EnableBotCaretClick
        ExplicitTop = 2
        ExplicitWidth = 174
        ExplicitHeight = 33
      end
    end
    object GroupBox1: TGroupBox
      AlignWithMargins = True
      Left = 4
      Top = 543
      Width = 355
      Height = 154
      Align = alTop
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1089#1090#1088#1086#1073#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      object Label5: TLabel
        Left = 2
        Top = 21
        Width = 120
        Height = 22
        Align = alTop
        Caption = #1053#1072#1095#1072#1083#1086' '#1089#1090#1088#1086#1073#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -18
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 2
        Top = 84
        Width = 111
        Height = 22
        Align = alTop
        Caption = #1050#1086#1085#1077#1094' '#1089#1090#1088#1086#1073#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -18
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
      end
      object GridPanel5: TGridPanel
        Left = 2
        Top = 43
        Width = 351
        Height = 41
        Align = alTop
        ColumnCollection = <
          item
            SizeStyle = ssAbsolute
            Value = 80.000000000000000000
          end
          item
            Value = 100.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_StrobeStartSpin
            Row = 0
          end
          item
            Column = 1
            Control = m_StrobeStartTrack
            Row = 0
          end>
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 0
        DesignSize = (
          351
          41)
        object m_StrobeStartSpin: TSpinEdit
          Tag = 8
          Left = 1
          Top = 1
          Width = 79
          Height = 39
          Anchors = []
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -26
          Font.Name = 'Tahoma'
          Font.Style = []
          Increment = 5
          MaxValue = 185
          MinValue = 0
          ParentFont = False
          TabOrder = 0
          Value = 10
          OnChange = OnEditPanelSpinChanged
        end
        object m_StrobeStartTrack: TTrackBar
          Tag = 9
          Left = 82
          Top = 2
          Width = 266
          Height = 37
          Anchors = []
          Max = 185
          PageSize = 10
          Frequency = 5
          TabOrder = 1
          ThumbLength = 30
          OnChange = OnEditPanelSpinChanged
        end
      end
      object GridPanel6: TGridPanel
        Left = 2
        Top = 106
        Width = 351
        Height = 41
        Align = alTop
        ColumnCollection = <
          item
            SizeStyle = ssAbsolute
            Value = 80.000000000000000000
          end
          item
            Value = 100.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_StrobeEndSpin
            Row = 0
          end
          item
            Column = 1
            Control = m_StrobeEndTrack
            Row = 0
          end>
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 1
        DesignSize = (
          351
          41)
        object m_StrobeEndSpin: TSpinEdit
          Tag = 10
          Left = 1
          Top = 1
          Width = 79
          Height = 39
          Anchors = []
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -26
          Font.Name = 'Tahoma'
          Font.Style = []
          Increment = 5
          MaxValue = 185
          MinValue = 0
          ParentFont = False
          TabOrder = 0
          Value = 185
          OnChange = OnEditPanelSpinChanged
        end
        object m_StrobeEndTrack: TTrackBar
          Tag = 11
          Left = 82
          Top = 2
          Width = 266
          Height = 37
          Anchors = []
          Max = 185
          PageSize = 10
          Frequency = 5
          TabOrder = 1
          ThumbLength = 30
          OnChange = OnEditPanelSpinChanged
        end
      end
    end
    object FlowPanel1: TFlowPanel
      Left = 1
      Top = 805
      Width = 361
      Height = 143
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      object m_AddBtn: TSpeedButton
        Left = 0
        Top = 0
        Width = 118
        Height = 44
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        OnClick = m_AddBtnClick
      end
      object m_ApplyBtn: TSpeedButton
        Left = 118
        Top = 0
        Width = 118
        Height = 44
        Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        OnClick = m_ApplyBtnClick
      end
      object m_RemoveBtn: TSpeedButton
        Left = 236
        Top = 0
        Width = 118
        Height = 44
        Caption = #1059#1076#1072#1083#1080#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        OnClick = m_RemoveBtnClick
      end
      object m_CancelBtn: TSpeedButton
        Left = 0
        Top = 44
        Width = 118
        Height = 44
        Caption = #1054#1090#1084#1077#1085#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        OnClick = m_CancelBtnClick
      end
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 700
      Width = 361
      Height = 105
      Align = alTop
      Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      object m_ChInfoMemo: TMemo
        AlignWithMargins = True
        Left = 5
        Top = 24
        Width = 351
        Height = 76
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object m_MainToolBar: TToolBar
    Left = 0
    Top = 0
    Width = 1004
    Height = 38
    AutoSize = True
    ButtonHeight = 38
    ButtonWidth = 204
    Caption = 'm_MainToolBar'
    DrawingStyle = dsGradient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Myriad Pro'
    Font.Style = [fsBold]
    Images = ImageList1
    List = True
    ParentFont = False
    ShowCaptions = True
    TabOrder = 1
    Transparent = False
    object m_LoadBtn: TToolButton
      Left = 0
      Top = 0
      AutoSize = True
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
      ImageIndex = 0
      Indeterminate = True
      OnClick = m_LoadBtnClick
    end
    object ToolButton2: TToolButton
      Left = 141
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object m_SaveBtn: TToolButton
      Left = 149
      Top = 0
      AutoSize = True
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      ImageIndex = 1
      OnClick = m_SaveBtnClick
    end
    object ToolButton7: TToolButton
      Left = 297
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object m_ClearBtn: TToolButton
      Left = 305
      Top = 0
      AutoSize = True
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      ImageIndex = 2
      OnClick = m_ClearBtnClick
    end
    object ToolButton8: TToolButton
      Left = 439
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object m_OptimizeBtn: TToolButton
      Left = 447
      Top = 0
      AutoSize = True
      Caption = #1054#1087#1090#1080#1084#1080#1079#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 3
      OnClick = m_OptimizeBtnClick
    end
    object ToolButton3: TToolButton
      Left = 655
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object m_BuildBtn: TToolButton
      Left = 663
      Top = 0
      AutoSize = True
      Caption = #1057#1086#1073#1088#1072#1090#1100
      ImageIndex = 4
      OnClick = m_BuildBtnClick
    end
    object ToolButton9: TToolButton
      Left = 789
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object m_CloseBtn: TToolButton
      Left = 797
      Top = 0
      AutoSize = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 5
      OnClick = m_CloseBtnClick
    end
  end
  object GridPanel9: TGridPanel
    AlignWithMargins = True
    Left = 3
    Top = 41
    Width = 629
    Height = 949
    Align = alClient
    Caption = 'GridPanel9'
    ColumnCollection = <
      item
        Value = 100.000000000000000000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = m_GridTable
        Row = 0
      end
      item
        Column = 0
        Control = GridPanel8
        Row = 1
      end
      item
        Column = 0
        Control = Panel1
        Row = 2
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    RowCollection = <
      item
        Value = 100.000000000000000000
      end
      item
        SizeStyle = ssAbsolute
        Value = 40.000000000000000000
      end
      item
        SizeStyle = ssAbsolute
        Value = 200.000000000000000000
      end>
    TabOrder = 2
    object m_GridTable: TStringGrid
      Left = 1
      Top = 1
      Width = 627
      Height = 707
      Align = alClient
      ColCount = 2
      DefaultColWidth = 84
      DefaultRowHeight = 36
      DrawingStyle = gdsGradient
      RowCount = 11
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Cambria'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goDrawFocusSelected, goRowSizing, goColSizing]
      ParentFont = False
      ScrollBars = ssHorizontal
      TabOrder = 0
      OnDrawCell = m_GridTableDrawCell
      OnMouseDown = m_GridTableMouseDown
      OnSelectCell = m_GridTableSelectCell
    end
    object GridPanel8: TGridPanel
      Left = 1
      Top = 708
      Width = 627
      Height = 40
      Align = alClient
      ColumnCollection = <
        item
          Value = 25.000083451707840000
        end
        item
          Value = 25.000001283872690000
        end
        item
          Value = 24.999898574078760000
        end
        item
          Value = 25.000016690340710000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = m_TableMovLeftHiBtn
          Row = 0
        end
        item
          Column = 1
          Control = m_TableMovLeftLowBtn
          Row = 0
        end
        item
          Column = 2
          Control = m_TableMovRightLowBtn
          Row = 0
        end
        item
          Column = 3
          Control = m_TableMovRightHiBtn
          Row = 0
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      RowCollection = <
        item
          Value = 100.000000000000000000
        end>
      TabOrder = 1
      object m_TableMovLeftHiBtn: TSpeedButton
        Left = 1
        Top = 1
        Width = 156
        Height = 38
        Align = alClient
        Caption = #8606
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -40
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        OnClick = OnTableMove
        ExplicitLeft = 0
        ExplicitTop = 6
        ExplicitWidth = 147
      end
      object m_TableMovLeftLowBtn: TSpeedButton
        Tag = 1
        Left = 157
        Top = 1
        Width = 156
        Height = 38
        Align = alClient
        Caption = #8672
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -40
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = OnTableMove
        ExplicitLeft = 71
        ExplicitTop = 548
        ExplicitWidth = 57
        ExplicitHeight = 44
      end
      object m_TableMovRightLowBtn: TSpeedButton
        Tag = 2
        Left = 313
        Top = 1
        Width = 156
        Height = 38
        Align = alClient
        Caption = #8674
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -40
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = OnTableMove
        ExplicitLeft = 134
        ExplicitTop = 548
        ExplicitWidth = 57
        ExplicitHeight = 44
      end
      object m_TableMovRightHiBtn: TSpeedButton
        Tag = 3
        Left = 469
        Top = 1
        Width = 157
        Height = 38
        Align = alClient
        Caption = #8608
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -40
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        OnClick = OnTableMove
        ExplicitLeft = 430
        ExplicitTop = 6
        ExplicitWidth = 143
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 748
      Width = 627
      Height = 200
      Align = alClient
      TabOrder = 2
      object m_InfoMemo: TMemo
        Left = 1
        Top = 1
        Width = 625
        Height = 198
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
    end
  end
  object ImageList1: TImageList
    BlendColor = clWhite
    BkColor = 16750899
    DrawingStyle = dsTransparent
    Height = 32
    Width = 32
    Left = 16
    Top = 88
    Bitmap = {
      494C010106006801AC01200020003399FF00FF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000800000004000000001002000000000000080
      000000000000000000000000000000000000FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300D1D1D1009898980067676700363636001E1E1E004B4B4B007D7D7D00AEAE
      AE00EFEFEF00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300F2F2F2006666
      6600000000000000000000000000000000000000000000000000000000000000
      000017171700AEAEAE00FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300565656008888
      8800888888008888880088888800888888008888880088888800888888008888
      8800888888008888880088888800888888008888880088888800888888008888
      8800888888008888880088888800888888008888880088888800888888008888
      88008888880056565600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300BBBBBB001F1F1F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000058585800ECECEC00FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300B2B2B20001010100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000025252500F6F6F600FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300EFEFEF001A1A1A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000071717100FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933005F5F5F000000000000000000000000000000
      000015151500B0B0B0005F5F5F000000000000000000040404008F8F8F006D6D
      6D000000000000000000000000000000000002020200C4C4C400FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00BFBFBF006B6B6B009B9B9B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300C2C2C200000000000000000000000000000000001515
      1500D2D2D200FF993300FDFDFD006161610004040400ABABAB00FF993300FF99
      330078787800000000000000000000000000000000002A2A2A00FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800D1D1
      D100D5D5D500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FDFDFD006B6B6B00FDFDFD0089898900DCDCDC00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF9933007E7E7E0000000000000000000000000000000000C3C3
      C300FF993300FF993300FF993300FDFDFD00C9C9C900FF993300FF993300FF99
      3300FF9933005B5B5B0000000000000000000000000000000000E8E8E800FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800ECEC
      EC007575750089898900F5F5F500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00EFEFEF0048484800C1C1C1005F5F5F00E6E6E600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF9933004B4B4B00000000000000000000000000000000009797
      9700FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FAFAFA004444440000000000000000000000000000000000B7B7B700FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00C2C2C2005F5F5F00BEBEBE00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00D0D0D00060606000C5C5C500A9A9A9009C9C9C007D7D7D00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF99330018181800000000000000000000000000000000000101
      01009D9D9D00FF993300FF993300FF993300FF993300FF993300FF993300FBFB
      FB0051515100000000000000000000000000000000000000000086868600FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00F7F7F7008F8F8F0073737300949494006C6C6C007F7F
      7F0071717100F2F2F200FFFFFF00FFFFFF00FFFFFF009F9F9F0095959500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300F4F4F40000000000000000000000000000000000000000000000
      000006060600E1E1E100FF993300FF993300FF993300FF993300FF9933009797
      970000000000000000000000000000000000000000000000000064646400FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B7B7B7008F8F8F00FFFFFF005F5F
      5F00FCFCFC00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0087878700AFAF
      AF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF9933001E1E1E00000000000000000000000000000000000404
      0400ABABAB00FF993300FF993300FF993300FF993300FF993300FF993300FDFD
      FD006161610000000000000000000000000000000000000000008C8C8C00FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F4F4F40063636300ADADAD007171
      7100FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFDFD007373
      7300C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F3F3F3009D9D
      9D00C8C8C80088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF9933005151510000000000000000000000000000000000A0A0
      A000FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FDFDFD004C4C4C0000000000000000000000000000000000BDBDBD00FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00ECECEC00BBBBBB00F8F8
      F800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F8F8
      F80067676700B4B4B400B5B5B500FDFDFD00EEEEEE009292920064646400B9B9
      B900FDFDFD0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF9933008484840000000000000000000000000000000000AFAF
      AF00FF993300FF993300FF993300FAFAFA00B5B5B500FF993300FF993300FF99
      3300FCFCFC004949490000000000000000000000000000000000EEEEEE00FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CECECE0067676700B7B7B7005A5A5A006C6C6C00C7C7C700FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300CECECE00020202000000000000000000000000000A0A
      0A00BDBDBD00FF993300FAFAFA00505050000101010096969600FF993300FCFC
      FC005B5B5B000000000000000000000000000000000039393900FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CECECE0096969600E8E8E80063636300FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300747474000000000000000000000000000000
      00000A0A0A009595950050505000000000000000000001010100777777005353
      53000000000000000000000000000000000007070700D5D5D500FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009292920069696900C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300F7F7F7002727270000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000086868600FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300C5C5C50007070700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000039393900FCFCFC00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300D5D5D500353535000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000303030078787800F8F8F800FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF99330088888800FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0088888800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FCFCFC008686
      8600060606000000000000000000000000000000000000000000000000000000
      00002B2B2B00CBCBCB00FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300565656008888
      8800888888008888880088888800888888008888880088888800888888008888
      8800888888008888880088888800888888008888880088888800888888008888
      8800888888008888880088888800888888008888880088888800888888008888
      88008888880056565600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300EBEBEB00B8B8B80087878700565656003E3E3E006B6B6B009D9D9D00CECE
      CE00FBFBFB00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF9933000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF993300FEFEFE00F7F7F700F6F6
      F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6
      F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6
      F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6
      F600F6F6F600F7F7F700FEFEFE00FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300F6F6F600D2D2D200CCCC
      CC00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCD
      CD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCD
      CD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCD
      CD00CCCCCC00D2D2D200F6F6F600FF993300FF993300FF993300DB8F6500D171
      3D00D1744100D1744100D1744100D1744100D1744100D1754100D1754100D175
      4100D1754100D1754100D1754100D1754100D1754100D1754100D1754100D175
      4100D1754100D1754100D1754100D1744100D1744100D1744100D1744100D174
      4100D06B3400F8EDE700FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300E6E6E600A7A7A700A7A7A700A7A7A700A7A7
      A700A7A7A700A7A7A700A7A7A700A7A7A700A7A7A700A7A7A700A7A7A700A7A7
      A700A7A7A700A7A7A700A7A7A700A7A7A700E6E6E600FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300F7F7F700DEDEDE00C6C3
      C00090846F007463480076644900766449007664490076644900766449007664
      4900766449007664490076644900766449007664490076644900766449007664
      4900766449007664490076644900766449007664490076644900756349008677
      6200C1BDB800D2D2D200F6F6F600FF993300FF993300FF993300D3784800E577
      1500E6751700E5731600E5721400E5700500E46D0900E46D0900E46D0900E46D
      0900E46D0900E46D0900E46D0900E46D0900E46D0900E46D0900E46D0900E46D
      0900E46D0900E46D0900E46D0900E56D0A00E5710A00E5731600E6751300E776
      1600D2641E00F9ECE400FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933004B4B4B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004B4B4B00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300E5E5
      E500FF993300FF993300FF993300FF993300FF993300FEFEFE00F9F9F900A698
      800097815F00A9916C00AF966F00AF966F00AF966F00AF966F00AF966F00AF96
      6F00AF966F00AF966F00AF966F00AF966F00AF966F00AF966F00AF966F00AF96
      6F00AF966F00AF966F00AF966F00AF966F00AF966F00AF966F00AC926C009B84
      62009A896E00F7F7F700FEFEFE00FF993300FF993300FF993300D37A4900E574
      1700EB791300EA781000E97F2000E0C6A500E0C29D00E0C29D00E0C29D00E0C2
      9D00E0C29D00E0C29D00E0C29D00E0C29D00E0C29D00E0C29D00E0C29D00E0C2
      9D00E0C29D00E0C29D00E0C29D00E0C4A000E5AC7700EA750500EB781000EC7C
      0F00D1632000F8ECE500FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF9933005858
      5800F9F9F900FF993300FF993300FF993300FF993300FF993300FF9933009C87
      6500B0987200B69C7500B69C7500B69C7500B69C7500B69C7500B69C7500B69C
      7500B69C7500B69C7500B69C7500B69C7500B69C7500B69C7500B69C7500B69C
      7500B69C7500B69C7500B69C7500B69C7500B69C7500B69C7500B69C7500B39A
      73009E886600FCFBFA00FF993300FF993300FF993300FF993300D37A4900E577
      1500EB7B1100EA790A00EA822B00DFF3ED00EFF7F400EFF7F300EFF7F300EFF7
      F300EFF7F300EFF7F300EFF7F300EFF7F300EFF7F300EFF7F300EFF7F300EFF7
      F300EFF7F300EFF7F300EFF7F300EFF8F500DFCAAA00EA750000EB791300EC7D
      1100D1632000F8ECE500FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300F1F1F100E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700F1F1F100FF9933002F2F
      2F0082828200FF993300FF993300FF993300FF993300FF993300FBFBF900A791
      6E00BDA47C00BDA47C00BDA47C00BDA47C00BDA47C00BDA47C00BDA47C00BDA4
      7C00BDA47C00BDA47C00BDA47C00BDA47C00BDA47C00BDA47C00BDA47C00BDA4
      7C00BDA47C00BDA47C00BDA47C00BDA47C00BDA47C00BDA47C00BDA47C00BDA4
      7C00A8926E00EFECE600FF993300FF993300FF993300FF993300D37A4900E678
      1700EC7C1300EB7A1400E9832B00DFF1EB00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00F0F8F400E1C8A800EB770000EC7C1300ED7E
      1400D1632000F8ECE500FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F0000000000000000006F6F6F006F6F
      6F002D2D2D0000000000141414006F6F6F006F6F6F0014141400000000002D2D
      2D006F6F6F006F6F6F0000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300F5F5
      F500101010001717170017171700171717001717170017171700171717001717
      1700171717001717170017171700171717001717170017171700171717001717
      170017171700171717001717170017171700171717003F3F3F00FEFEFE002F2F
      2F0020202000FF993300FF993300FF993300FF993300FF993300EDEAE400B09A
      7600C4AB8400C4AB8400C4AB8400C4AB8400C4AB8400C4AB8400C4AB8400C4AB
      8400C4AB8400C4AB8400C4AB8400C4AB8400C4AB8400C4AB8400C4AB8400C4AB
      8400C4AB8400C4AB8400C4AB8400C4AB8400C4AB8400C4AB8400C4AB8400C4AB
      8400B29B7800E1DAD000FF993300FF993300FF993300FF993300D37A4900E679
      1800ED7E1400EC7D0D00EB852C00DAEAE300EDE6E400ECE6E400ECE6E400ECE6
      E400ECE6E400ECE6E400ECE6E400ECE6E400ECE6E400ECE6E400ECE6E400ECE6
      E400ECE6E400ECE6E400ECE6E400E7E9E600E1C9A700EC790100EC7E1300ED80
      1000D1642000F8ECE500FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000FF993300FF99
      330067676700000000002F2F2F00FF993300FF9933002F2F2F00000000006767
      6700FF993300FF99330000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF9933002F2F
      2F00A0A0A000FF993300FF993300FF993300FF993300FF993300DFD7CC00B9A2
      8000CCB28C00CCB28C00CCB28C00CCB28C00CCB28C00CCB28C00CCB28C00CCB2
      8C00CCB28C00CCB28C00CCB28C00CCB28C00CCB28C00CCB28C00CCB28C00CCB2
      8C00CCB28C00CCB28C00CCB28C00CCB28C00CCB28C00CCB28C00CCB28C00CCB2
      8C00BDA58100D3C9BA00FF993300FF993300FF993300FF993300D37A4900E77B
      1900EE821100ED7F1200EC8A2A00DFF1EB00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00EFF7F300E2C59C00EE7B0800ED811300EF84
      1600D1642000F8ECE500FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000FF993300FF99
      330067676700000000002F2F2F00FF993300FF9933002F2F2F00000000006767
      6700FF993300FF99330000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF9933007070
      7000FF993300FF993300FF993300FF993300FF993300FF993300D1C7B700C5AD
      8900D2BA9400D2BA9400D2BA9400D2BA9400D2BA9400D2BA9400D2BA9400D2BA
      9400D2BA9400D2BA9400D2BA9400D2BA9400D2BA9400D2BA9400D2BA9400D2BA
      9400D2BA9400D2BA9400D2BA9400D2BA9400D2BA9400D2BA9400D2BA9400D2BA
      9400C6AF8B00C9BBA700FF993300FF993300FF993300FF993300D37A4900E87D
      1600F0851500EE821500ED8B2E00DFF1EB00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00EFF7F300E3C59D00EF800200EF831A00F186
      1300D3662000F7E5DC00FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000FF993300FF99
      330067676700000000002F2F2F00FF993300FF9933002F2F2F00000000006767
      6700FF993300FF99330000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300F4F4
      F400FF993300FF993300FF993300FF993300FF993300FF993300C8BBA600CBB5
      9200D7BF9A00D7BF9A00D7BF9A00D7BF9A00D7BF9A00D7BF9A00D7BF9A00D7BF
      9A00D7BF9A00D7BF9A00D7BF9A00D7BF9A00D7BF9A00D7BF9A00D7BF9A00D7BF
      9A00D7BF9A00D7BF9A00D7BF9A00D7BF9A00D7BF9A00D7BF9A00D7BF9A00D7BF
      9A00CFB89400C3B39A00FF993300FF993300FF993300FF993300D37A4900E97F
      1B00F1881700F0861300EF8F2D00DDE9DB00F3E2D300F2E2D300F2E2D300F2E2
      D300F2E2D300F2E2D300F2E2D300F2E2D300F2E2D300F2E2D300F2E2D300F2E2
      D300F2E2D300F2E2D300F3E2D300EBE6D700E4C79F00F1830200F0871500F189
      1900D3672000F5E0D500FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000FF993300FF99
      330067676700000000002F2F2F00FF993300FF9933002F2F2F00000000006767
      6700FF993300FF99330000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300444444000707070007070700070707000A0A0A003D3D
      3D00D3D3D300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300C2B29900D1BB
      9900D9C29F00D9C29F00D9C29F00D9C29F00D9C29F00D9C29F00D9C29F00D9C2
      9F00D9C29F00D9C29F00D9C29F00D9C29F00D9C29F00D9C29F00D9C29F00D9C2
      9F00D9C29F00D9C29F00D9C29F00D9C29F00D9C29F00D9C29F00D9C29F00D9C2
      9F00D4BD9C00C0AC9000FF993300FF993300FF993300FF993300D37A4900E982
      1B00F28B1A00F1891900F0923100DFF1EB00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00EFF7F300E4C89E00F2860B00F18A1B00F38C
      1C00D4682100F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000FF993300FF99
      330067676700000000002F2F2F00FF993300FF9933002F2F2F00000000006767
      6700FF993300FF99330000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FBFBFB00F7F7F700F7F7F700F7F7F700F2F2F2008686
      860017171700ECECEC00FF993300FF993300FF993300FF993300FF993300D3D3
      D300999999009797970097979700979797009B9B9B00F7F7F700FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300BFAC9000D7C2
      A000DBC5A300DBC5A300DBC5A300DBC5A300DBC5A300DBC5A300DBC5A300DBC5
      A300DBC5A300DBC5A300DBC5A300DBC5A300DBC5A300DBC5A300DBC5A300DBC5
      A300DBC5A300DBC5A300DBC5A300DBC5A300DBC5A300DBC5A300DBC5A300DBC5
      A300DAC4A200C0AD8F00FF993300FF993300FF993300FF993300D37A4900EA84
      1B00F38F1D00F28E1A00F0963000DEF0EA00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00EFF7F200E4C89F00F38B0A00F28F1C00F490
      1E00D4692000F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000FF993300FF99
      330067676700000000002F2F2F00FF993300FF9933002F2F2F00000000006767
      6700FF993300FF99330000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      33006A6A6A0063636300FF993300FF993300FF993300FF9933009E9E9E000D0D
      0D00636363006767670067676700676767006B6B6B00E9E9E900FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FBFAF900C3B09400DCC7
      A700DCC7A700DCC7A700DCC7A700DCC7A700DCC7A700DCC7A700DCC7A700DCC7
      A700DCC7A700DCC7A700DCC7A700DCC7A700DCC7A700DCC7A700DCC7A700DCC7
      A700DCC7A700DCC7A700DCC7A700DCC7A700DCC7A700DCC7A700DCC7A700DCC7
      A700DCC7A700C3B09400F6F4F200FF993300FF993300FF993300D37A4900EB86
      2200F4921C00F4901E00F2993400DFF1EB00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00EFF7F400E5CBA000F58C1200F4911D00F594
      1D00D4692000F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000FF993300FF99
      330067676700000000002F2F2F00FF993300FF9933002F2F2F00000000006767
      6700FF993300FF99330000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300EFEFEF0018181800C6C6C600FF993300FF993300F7F7F70015151500D8D8
      D800FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300F0EDE900C5B59900DDCA
      AB00DDCAAB00DDCAAB00DDCAAB00DDCAAB00DDCAAB00DDCAAB00DDCAAB00DDCA
      AB00DDCAAB00DDCAAB00DDCAAB00DDCAAB00DDCAAB00DDCAAB00DDCAAB00DDCA
      AB00DDCAAB00DDCAAB00DDCAAB00DDCAAB00DDCAAB00DDCAAB00DDCAAB00DDCA
      AB00DDCAAB00C7B69A00EBE7E100FF993300FF993300FF993300D37A4A00EC88
      1D00F5942000F5931E00F39B3500DBDFCD00E9C5A000E8C6A200E8C6A200E8C6
      A200E8C6A200E8C6A200E8C6A200E8C6A200E8C6A200E8C6A200E8C6A200E8C6
      A200E8C6A200E8C6A200E9C5A100E4D4B900E5CDA400F5900B00F4942000F696
      2100D46B2200F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000FF993300FF99
      330067676700000000002F2F2F00FF993300FF9933002F2F2F00000000006767
      6700FF993300FF99330000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300A2A2A20030303000FCFCFC00FF9933009F9F9F0050505000FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300E3DED600CCBA9F00DFCC
      AF00DFCCAF00DFCCAF00DFCCAF00DFCCAF00DFCCAF00DFCCAF00DFCCAF00DFCC
      AF00DFCCAF00DFCCAF00DFCCAF00DFCCAF00DFCCAF00DFCCAF00DFCCAF00DFCC
      AF00DFCCAF00DFCCAF00DFCCAF00DFCCAF00DFCCAF00DFCCAF00DFCCAF00DFCC
      AF00DFCCAF00CDBBA000DFD9CF00FF993300FF993300FF993300D37A4A00EB8C
      2100F5982400F5971F00F2A34600E0F2EB00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFCFD00FCFC
      FD00FCFCFD00FCFCFD00FCFCFD00EFF7F400E6CDA200F6950A00F5972200F799
      2000D46B2200F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000FF993300FF99
      330067676700000000002F2F2F00FF993300FF9933002F2F2F00000000006767
      6700FF993300FF99330000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF9933005C5C5C002F2F2F002F2F2F002F2F2F00323232006A6A
      6A00EBEBEB00FF9933003F3F3F0091919100FF99330036363600B9B9B900FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300D8D0C600CFBFA600E0CE
      B300E0CEB300E0CEB300E0CEB300E0CEB300E0CEB300E0CEB300E0CEB300E0CE
      B300E0CEB300E0CEB300E0CEB300E0CEB300E0CEB300E0CEB300E0CEB300E0CE
      B300E0CEB300E0CEB300E0CEB300E0CEB300E0CEB300E0CEB300E0CEB300E0CE
      B300E0CEB300D1C1A700D5CDC100FF993300FF993300FF993300D37A4900EC8B
      2400F6982400F5972200F2A64B00E0F2ED00E9F4EE00E8F4EE00E9F4EE00E9F4
      EE00E9F4EE00E9F4EE00E9F4EE00E9F4EE00E9F4EE00E9F4EE00E9F4EE00E9F4
      EE00E9F4EE00E9F4EE00E9F4EE00E9F5F000E5CEA500F6961700F5982400F89A
      2500D5692000F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000FF993300FF99
      330067676700000000002F2F2F00FF993300FF9933002F2F2F00000000006767
      6700FF993300FF99330000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300E3E3E300CFCFCF00CFCFCF00CFCFCF00CACACA005E5E
      5E002C2C2C00F8F8F800D5D5D50014141400B4B4B40024242400FEFEFE00F7F7
      F700C9C9C900C7C7C700C7C7C700C7C7C700CBCBCB00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300CFC6B700D5C4AC00E1D0
      B600E1D0B600E1D0B600E1D0B600E1D0B600E1D0B600E1D0B600E1D0B600E1D0
      B600E1D0B600E1D0B600E1D0B600E1D0B600E1D0B600E1D0B600E1D0B600E1D0
      B600E1D0B600E1D0B600E1D0B600E1D0B600E1D0B600E1D0B600E1D0B600E1D0
      B600E1D0B600D6C5AD00CFC5B600FF993300FF993300FF993300D37A4900EB9A
      4000F5AC4D00F4AD5000F4B25F00EACA9300EBC89200EBC89200EBC89200EBC8
      9200EBC89200EBC89200EBC89200EBC89200EBC89200EBC89200EBC89200EBC8
      9200EBC89200EBC89200EBC89200EBC89300EFBE7A00F6AD4E00F4AC5100F6AC
      4D00D46F2700F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000FF993300FF99
      330067676700000000002F2F2F00FF993300FF9933002F2F2F00000000006767
      6700FF993300FF99330000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      33005151510081818100FF993300858585000A0A0A00A0A0A000C0C0C0000D0D
      0D00333333003737370037373700373737003B3B3B00E2E2E200FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300CABFAE00D9CAB200E2D2
      B900E2D2B900E2D2B900E2D2B900E2D2B900E2D2B900E2D2B900E2D2B900E2D2
      B900E2D2B900E2D2B900E2D2B900E2D2B900E2D2B900E2D2B900E2D2B900E2D2
      B900E2D2B900E2D2B900E2D2B900E2D2B900E2D2B900E2D2B900E2D2B900E2D2
      B900E2D2B900DACBB200C9BEAC00FF993300FF993300FF993300D37A4900EB9A
      4000F5AC4F00F4AE5100F5AF5200F5AF5200F5B05300F5B05300F5B05300F5B0
      5300F5B05300F5B05300F5B05300F5B05300F5B05300F5B05300F5B05300F5B0
      5300F5B05300F5B05300F5B05300F5B15100F5B05200F5AF5200F4AC4F00F6AD
      4D00D46E2900F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000FF993300FF99
      330067676700000000002F2F2F00FF993300FF9933002F2F2F00000000006767
      6700FF993300FF99330000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300E1E1E10013131300DDDDDD00FF993300E3E3E300FDFDFD0020202000B4B4
      B400FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300CBC0AD00E4D8C600E9DD
      CA00E9DDCA00E9DDCA00E9DDCA00E9DDCA00E9DDCA00E9DDCA00E9DDCA00E9DD
      CA00E9DDCA00E9DDCA00E9DDCA00E9DDCA00E9DDCA00E9DDCA00E9DDCA00E9DD
      CA00E9DDCA00E9DDCA00E9DDCA00E9DDCA00E9DDCA00E9DDCA00E9DDCA00E9DD
      CA00E9DDCA00E4D8C600CBBFAC00FF993300FF993300FF993300D37A4900EB9B
      4200F5AD5300F4AF5500F7B35800FCB75D00FDB95F00FDB95F00FDB95F00FDB9
      5F00FDB95F00FDB95F00FDB95F00FDB95F00FDB95F00FDB95F00FDB95F00FDB9
      5F00FDB95F00FDB95F00FDB95F00FCB95C00F9B45B00F6B15800F4AD5300F6AD
      5000D46E2900F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000FF993300FF99
      330067676700000000002F2F2F00FF993300FF9933002F2F2F00000000006767
      6700FF993300FF99330000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300888888004A4A4A00FF993300FF993300B2B2B2003C3C3C00FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300E3DFD8009A896F009D8C
      72009D8C72009D8C72009D8C72009D8C72009D8C72009D8C72009D8C72009D8C
      72009D8C72009D8C72009D8C72009D8C72009D8C72009D8C72009D8C72009D8C
      72009D8C72009D8C72009D8C72009D8C72009D8C72009D8C72009D8C72009D8C
      72009D8C72009A896F00E3DFD800FF993300FF993300FF993300D37A4900EB9C
      4500F6AF5400F7B15700CC9449006E4E25007353260073512600735126007351
      2600735126007351260073512600735126007351260073512600735126007351
      260073512600735126007353260070502400A2763900FAB45B00F5AF5500F7AF
      5300D56F2900F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F0000000000000000007F7F7F007F7F
      7F003333330000000000171717007F7F7F007F7F7F0017171700000000003333
      33007F7F7F007F7F7F0000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FAFAFA0029292900AEAEAE00FF99330048484800A6A6A600FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300A794
      7700FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300AF9B7C00FF993300FF993300FF993300FF993300FF993300D3794700EB9E
      4500F6AF5700F9B35B00AF7F4300020202000202020002020200020202000202
      0200020202000202020002020200020202000202020002020200020202000202
      02000202020002020200020202000202020062462300FDB85F00F4B05600F7B0
      5700D56F2B00F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300BEBEBE001E1E1E00D1D1D10017171700F9F9F900FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300B3A0
      8400B09E810098886F00BAB6AF00C9C9C900C9C9C900C9C9C900C9C9C900C9C9
      C900C9C9C900C9C9C900C7C5C300BBB7B000ABA497009E917F0097886F009F8F
      7400A6947900AD9B7E00B2A08200B5A28400B6A38500B6A38500B6A38500B6A3
      8500B3A08400FF993300FF993300FF993300FF993300FF993300D3794700EB9D
      4700F6B15800F9B55D00B3834600070707000A0A06000A0A06000A0A06000A0A
      06000A0A0600201E1C007673750072706F0072706F0072706F0072706F007270
      6F0072706F00716F6E0094929200A6A6A7005A401F00FEBB6000F4B05A00F7B1
      5700D56F2B00F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933005D5D5D001717170081818100FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300B8A6
      8A00BAA88C00AE9D8300B5ADA000FF993300FF993300F6F5F300DBD7D200BCB6
      AC00A79E8E009B8E78009F907800A6967D00AE9D8300B4A38800B9A78B00BAA8
      8C00BAA88C00BAA88C00BAA88C00BAA88C00BAA88C00BAA88C00BAA88C00BAA8
      8C00B8A68A00FF993300FF993300FF993300FF993300FF993300D3794700ED9F
      4A00F6B15C00F9B66100B6864B00141411001715140017151400171514001715
      1400171514003F3C3C00F3F1EF00EDEAE800EEEBE900EEEBE900EEEBE900EEEC
      EA00F2F0EE00F2EFEC00ECE9E700F1EEEE005F432000FEBB6400F5B15C00F7B2
      5A00D56F2D00F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF9933003F3F3F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003F3F3F00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FDFDFD00BCBCBC00FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300B8A8
      8E00BBAA9000BAA99000B2A59000BAB1A400A3988600A0917C00A8998200AFA0
      8800B7A68E00BCAB9200BEAD9300BFAE9400BFAE9400BFAE9400BFAE9400BFAE
      9400BFAE9400BFAE9400BFAE9400BFAE9400BFAE9400BFAE9400BFAE9400BBAA
      9000B8A88E00FF993300FF993300FF993300FF993300FF993300D3794700ECA0
      4B00F6B35E00F9B86000B88A50001E1E1C0023201E0023201E0023201E002320
      1E00211E1D003F3C3C00EAE8E400E5E2DE00E5E2DE00E5E2DE00E5E2DE00DDDC
      D7003E3C42006C6A6C00EAE7E200E9E8E40063472400FEBC6500F5B35D00F7B3
      5B00D56F2D00F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300D9D9D900D2D2D200D0D0D000D0D0D000D0D0D000D0D0
      D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0D000D0D0
      D000D0D0D000D0D0D000D0D0D000D0D0D000D2D2D200D9D9D900FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EFEF
      EF000F0F0F00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EAE1
      D400BBAB9400C3B39A00BFB09700BCAC9400BFAF9700C3B39A00C4B49B00C4B4
      9B00C4B49B00C4B49B00C0AF9700B6A79000AE9F8A00AB9C8700AB9C8700AB9C
      8700AB9C8700AB9C8700AB9C8700AB9C8700AB9C8700AB9C8700AB9C8700AB9C
      8700D8D0C400FF993300FF993300FF993300FF993300FF993300D3794700ECA1
      4C00F6B35F00F9B86300BB8F5200282828002D2B29002D2B29002D2B29002D2B
      29002C29270045424100E7E4E000E1DDD900E1DDD900E1DDD900E2DFDB00ACA8
      A9000000040000000400E0DCD900E5E3E000674D2B00FDBB6600F5B25F00F6B1
      5C00D56F2D00F5E1D600FF993300FF993300FF993300FF993300FF993300FF99
      3300FF9933009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9C9C00FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300C0C0C000FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300C1B29C00C7B8A100C7B8A100C7B8A100C7B8A100C7B8A100C7B8A100C7B8
      A100C7B8A100C3B49E00B9AB9600E4DCD100FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300D3794700EEA1
      4C00F6B45F00FAB96300BD905600323133003935340039353400393534003935
      3400363231004A474400E3E0DA00DDDAD400DDDAD500DDDAD500DFDBD700ACA8
      A6000000030000000400DCD8D200E2E0DC006C503100FEBC6500F5B35E00F8B3
      5B00D5702C00F5E0D500FF993300FF993300FF993300FF993300FF993300FF99
      3300FF9933007F7F7F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007F7F7F00FF99
      3300FF993300FF993300FF993300FF993300FF993300E9E9E900B7B7B700B7B7
      B700B7B7B700B7B7B700B8B8B800F3F3F300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300C4B7A100C7B8A400CBBDA700CBBDA700CBBDA700CBBDA700CBBDA700CBBD
      A700C6B8A300BDB09C00E8E1D700FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300D3794700ECA1
      4D00F6B35E00F9B86200C09358003C3C3B0043403C0043403C0043403C004340
      3C003F3C3A0054534E00F5F1F300F0EDEF00F1EEEF00F1EEEF00F2EFF000BBB7
      BC000000030000000300EDE9EC00F4F1F40070553300FEBA6300F6B25D00F0A4
      4F00CA5B1E00F8EAE200FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300D2D2D200424242003F3F3F003F3F3F003F3F3F00252525000000
      0000000000001F1F1F001F1F1F001F1F1F001F1F1F001F1F1F001F1F1F000000
      000000000000252525003F3F3F003F3F3F003F3F3F0042424200D2D2D200FF99
      3300FF993300FF993300FF993300FF993300FF993300FF9933009E9E9E001313
      13000000000020202000B6B6B600FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300E7E1D700B3A79500B3A79500B3A79500B3A79500B3A79500B3A79500B3A7
      9500B3A79500E9E3DA00FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300D3794700EC9E
      4C00F6B15D00F9B66100C2945B00464545004C4846004C4846004C4846004C48
      46004845420059575600F6F4F600F2F0F200F2F0F200F2F0F200F3F1F200D2D0
      D3000000030015151C00F4F2F300F5F5F60073583700FEB96200F0A65400DC7F
      3500EAC2AB00FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300979797000000
      000000000000FF993300FF993300FF993300FF993300FF993300FF9933000000
      00000000000097979700FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300EBEB
      EB0098989800F4F4F400FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300D37A4700EE9E
      4D00F8B25D00FAB56000C6965C004F4D4B0055504C0055504C0055504C005550
      4C00534D490064635F00F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700E6E6E600F1F1F100F7F7F700F4F4F40080634000FBB25900DC7F3500E8B8
      9D00FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300979797000000
      0000000000001717170017171700171717001717170017171700171717000000
      00000000000097979700FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300D2733E00DD80
      3800DC7E3700DD803600B3734200555653005957520059575200595752005957
      53005856510057554F006C6C6800666564006765640067656400676564006765
      6500686764006766650067656400696969008A604200DD7D3300E3AB8C00FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF9933009B9B9B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009B9B9B00FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300EECDBA00EAC0
      A800EAC1A900EBC2AA00D7B19B00B0918000B3938200B3938200B3938200B393
      8200B3938200B1918000AD8F7C00AA8C7B00AB8D7B00AB8D7B00AB8D7B00AB8D
      7B00AB8D7B00AB8D7B00AA8C7B00AB8D7B00C8A39100EECAB600FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300F8F8F8008F8F
      8F00878787008787870087878700878787008787870087878700878787008787
      87008F8F8F00F8F8F800FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300FF993300FF993300FF993300FF99
      3300FF993300FF993300FF993300FF993300424D3E000000000000003E000000
      2800000080000000400000000100010000000000000400000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFF007FF0000000000000000FFFFFFFFFFC003FF0000000000000000
      C0000003FF8000FF0000000000000000C0000003FF00007F0000000000000000
      C0000003FE00007F0000000000000000C0000003FE00003F0000000000000000
      C0000003FC04303F0000000000000000C0000003FC0E781F0000000000000000
      C0000003FC0FF01F0000000000000000C0000003FC07E01F0000000000000000
      C0000003F803E01F0000000000000000C0000003FC07E01F0000000000000000
      C0000003FC0FF01F0000000000000000C0000003FC0E701F0000000000000000
      C0000003FC04203F0000000000000000C0000003FE00003F0000000000000000
      C0000003FE00007F0000000000000000C0000003FF00007F0000000000000000
      C0000003FF8000FF0000000000000000C0000003FFC003FF0000000000000000
      C0000003FFF007FF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFF0000000000000000
      FFFFFFFFFFFFFFFF000000000000000080000001FFFFFFFFFFFFFFFFFFFFFFFF
      80000001C0000003FE00007FFFFFFFFF80000001C0000003FE00007FFFFFFFEF
      80000001C0000003FE00007FFFFFFFE7E0000003C0000003FE00007FF0000027
      C0000003C0000003FE00007FE0000007C0000003C0000003FE318C7FE7FFFFE7
      C0000003C0000003FE318C7FE7FFFFEFC0000003C0000003FE318C7FE7FFFFEF
      C0000003C0000003FE318C7FE407FFFFC0000003C0000003FE318C7FE403E03F
      C0000003C0000003FE318C7FE7F3C03F80000001C0000003FE318C7FE7F18FFF
      80000001C0000003FE318C7FE7F89FFF80000001C0000003FE318C7FE4049FFF
      80000001C0000003FE318C7FE400007F80000001C0000003FE318C7FE7F2003F
      80000001C0000003FE318C7FE7F10FFF80000001C0000003FE318C7FE7F99FFF
      80000001C0000003FE00007FE7F89FFFEFFFFFF7C0000003FE00007FE7FC1FFF
      E0000007C0000003FE00007FE7FE3FFFE1800007C0000003FE00007FE7FE7FFF
      E0000007C0000003FC00003FE7FFFFFFE0000007C0000003F800001FF7FFFFFF
      F000FFFFC0000003F800001F80FFFFFFF001FFFFC0000003F800001FC1FFFFFF
      F003FFFFC0000007FFC7E3FFE3FFFFFFFFFFFFFFC000000FFFC003FFFFFFFFFF
      FFFFFFFFC000001FFFC003FFFFFFFFFFFFFFFFFFC000003FFFC003FFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'tdb'
    Filter = 'TuneTable database|*.tdb|All files|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 186
    Top = 153
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'tdb'
    Filter = 'TuneTable database|*.tdb|All files|*.*'
    Left = 240
    Top = 152
  end
end
