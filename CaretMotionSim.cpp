//---------------------------------------------------------------------------

#pragma hdrstop

#include "CaretMotionSim.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

void CALLBACK CaretMotionSimOnTimer(HWND v1, UINT v2, UINT_PTR v3, DWORD v4)
{
    //cCaretMotionSim::Instance().OnMovementTimer(NULL);
}

cCaretMotionSim::cCaretMotionSim()
{
    hCaretMoveEndEvent = CreateEvent(   NULL,               // default security attributes
                                    FALSE,               // manual-reset event
                                    FALSE,              // initial state is nonsignaled
                                    TEXT("CATVKCaretsSim")    // object name
                                    );

//    TForm* pFakeForm = new TForm(Application);
//
//    pMotionTimer = new TTimer(pFakeForm);
//    pMotionTimer->Enabled = false;
//    pMotionTimer->Interval = 200;
//    TNotifyEvent notifyOnMotionTimer = &(this->OnMovementTimer);
//    pMotionTimer->OnTimer = notifyOnMotionTimer;

//        timerId = SetTimer(NULL, WM_USER + 165, 200, &CaretMotionSimOnTimer);

    endMovementCallback = NULL;
    iCurrMotion = 0;

    carets[0].pos = carets[0].target = 50;
    carets[1].pos = carets[1].target = 50;
    carets[0].bActive = carets[1].bActive = false;
    carets[0].dir = carets[1].dir = 0;
    carets[0].pe = carets[1].pe = 0;

    initSearchMotionTable();

    //pMotionTimer->Enabled = true;
};


bool cCaretMotionSim::waitStop()
{
    while(processCarets())
    {
        Sleep(50);
    }
    return true;
}

void __fastcall cCaretMotionSim::OnMovementTimer(TObject *Sender)
{
    processCarets();
}

bool cCaretMotionSim::processCarets()
{
    AUTO_LOCK_WCS(mainLock);

    static DWORD prevTime = GetTickCount();
    DWORD currTime = GetTickCount();
    const float caretSpeed = 100;

    float dT = float(currTime - prevTime)/1000.f;
    float dS = caretSpeed * dT;
    prevTime = currTime;

    bool bAllStatus = false;
    bool bPrevStatus = carets[0].bActive && carets[1].bActive;
    for(int i = 0; i < 2; i++)
    {
//        if(!carets[i].bActive && (carets[i].pos != carets[i].target))
//            carets[i].bActive = true;

        bAllStatus = bAllStatus || carets[i].bActive;
        if(!carets[i].bActive)
            continue;
        if(abs(long(carets[i].pos - carets[i].target)) < dS)
        {
            carets[i].pos = carets[i].target;
            carets[i].bActive = false;
            carets[i].dir = 0;
        }
        else
        {
            if(carets[i].pos < carets[i].target)
            {
                carets[i].pos += dS;
                carets[i].pe += dS;
                carets[i].dir = 1;
            }
            else if(carets[i].pos > carets[i].target)
            {
                carets[i].pos -= dS;
                carets[i].pe -= dS;
                carets[i].dir = -1;
            }
        }
    }
    bool bCurrStatus = carets[0].bActive && carets[1].bActive;

    static DWORD timemm = GetTickCount();
    if((GetTickCount() - timemm) > 100)
    {
        if( carets[0].bActive && carets[1].bActive)
            TRACE("MotionSim01[%d]: {%.1f, %.1f, %.1f, %d}, {%.1f, %.1f, %.1f, %d}",
                    iCurrMotion, carets[0].pos, carets[0].target,carets[0].pe, carets[0].dir,
                    carets[1].pos, carets[1].target,carets[1].pe, carets[1].dir);
        else if(carets[0].bActive)
            TRACE("MotionSim0[%d]: %.1f, %.1f, %.1f, %d", iCurrMotion, carets[0].pos, carets[0].target,carets[0].pe, carets[0].dir);
        else if(carets[1].bActive)
            TRACE("MotionSim1[%d]: %.1f, %.1f, %.1f, %d", iCurrMotion, carets[1].pos, carets[1].target,carets[1].pe, carets[1].dir);

        timemm = GetTickCount();
    };

    if(!bPrevStatus && bCurrStatus)
        onStartMovement();
    else if(bPrevStatus && !bCurrStatus)
        onEndMovement();

    return bAllStatus;
}

void cCaretMotionSim::moveCaretsAbs(int topOffset, int btmOffset)
{
    //AUTO_LOCK_WCS(mainLock);

    waitStop();

    carets[0].bActive = carets[1].bActive = true;
    carets[0].target = topOffset;
    carets[1].target = btmOffset;

    waitStop();
}

void cCaretMotionSim::moveCaretsRel(int topOffset, int btmOffset)
{
    moveCaretsAbs(carets[0].pos + topOffset, carets[1].pos + btmOffset);
}

void cCaretMotionSim::onStartMovement()
{
}

void cCaretMotionSim::onEndMovement()
{
    if(endMovementCallback)
    {
        MOTIONRESULT r = {0, ERROR_NO, 0, 0, 0};
        endMovementCallback(r);
        endMovementCallback = NULL;
    }
}

void cCaretMotionSim::setCallback(tCallBack proc)
{
    endMovementCallback = proc;
}

int cCaretMotionSim::motionStart2(int bottomTrolleyShift, int upperTrolleyShift, tCallBack proc)
{
    //AUTO_LOCK_WCS(mainLock);

    waitStop();
    setCallback(proc);
    moveCaretsRel(bottomTrolleyShift, upperTrolleyShift);
    return 0;
}

int cCaretMotionSim::trolleysReturn(tCallBack proc)
{
    //AUTO_LOCK_WCS(mainLock);

    waitStop();
    setCallback(proc);
    moveCaretsAbs(50, 50);
    return 0;
}

int cCaretMotionSim::motionBreak(tCallBack proc)
{
    //AUTO_LOCK_WCS(mainLock);

    setCallback(proc);
    carets[0].target = carets[0].pos;
    carets[1].target = carets[1].pos;
    assert(!processCarets());
    return 0;
}

int cCaretMotionSim::getCariagesStatus(tCARIAGESTAT *pDest)
{
    //AUTO_LOCK_WCS(mainLock);

    if((!carets[0].bActive) && (!carets[1].bActive))
		pDest->status = 0;
	else if(!carets[0].bActive)
		pDest->status = 2;
	else if(!carets[1].bActive)
		pDest->status = 1;
	pDest->upperCoord = carets[0].pos;
	pDest->bottomCoord = carets[1].pos;
    return 0;
}

void cCaretMotionSim::initSearchMotionTable()
{
    AUTO_LOCK_WCS(mainLock);

    iCurrMotion = 0;
    motionsTable.clear();

    float X = 650;
    carets[0].pos = carets[0].target = X;
    carets[1].pos = carets[1].target = 50;
    //motionsTable.push_back(sCaretMotion(50,50));                    //0
    //motionsTable.push_back(sCaretMotion(X,0,true,false));           //1
    motionsTable.push_back(sCaretMotion(X-465,0,true,false));       //2
    motionsTable.push_back(sCaretMotion(1000,0,true,false));        //3
    motionsTable.push_back(sCaretMotion(0,X-435,false,true));       //4
    motionsTable.push_back(sCaretMotion(0,X+525,false,true));       //5
    motionsTable.push_back(sCaretMotion(0,X-315,false,true));       //6
    motionsTable.push_back(sCaretMotion(X+25,X-155));               //7
    motionsTable.push_back(sCaretMotion(X-155,X+25));               //8
    motionsTable.push_back(sCaretMotion(0,X-335,false,true));       //9
    motionsTable.push_back(sCaretMotion(X-335,X-155));              //10
    motionsTable.push_back(sCaretMotion(X+266,X+105));              //11
    motionsTable.push_back(sCaretMotion(X+105,X+266));              //12
    motionsTable.push_back(sCaretMotion(0,X-56,false,true));        //13
    motionsTable.push_back(sCaretMotion(X-56,X+105));               //14
    motionsTable.push_back(sCaretMotion(50,50));                    //15
    motionsTable.push_back(sCaretMotion(50,50));                    //16
}

int cCaretMotionSim::motionStart(tCallBack proc)
{
    //AUTO_LOCK_WCS(mainLock);

    if(iCurrMotion >= (int)motionsTable.size())
        return -1;

    waitStop();
    setCallback(proc);

    int offsetTop = motionsTable[iCurrMotion].topOffset;
    int offsetBtm = motionsTable[iCurrMotion].btmOffset;

    if(!motionsTable[iCurrMotion].bIsAbsOffsetTop)
        offsetTop = carets[0].pos + offsetTop;
    if(!motionsTable[iCurrMotion].bIsAbsOffsetBtm)
        offsetBtm = carets[1].pos + offsetBtm;

    moveCaretsAbs(offsetTop, offsetBtm);
    iCurrMotion++;

    if(iCurrMotion >= (int)motionsTable.size())
        iCurrMotion = 0;
    return 0;
}

void cCaretMotionSim::SetPathEncoderData(char PathEncoderIdx, int NewValue)
{
    AUTO_LOCK_WCS(mainLock);

    assert(PathEncoderIdx <= 1);


    if(carets[0].bActive && carets[1].bActive)
        carets[0].pe = NewValue;
    else if(carets[1].bActive)
        carets[1].pe = NewValue;
    carets[0].pe = NewValue;
}

int cCaretMotionSim::GetPathEncoderData(char PathEncoderIdx)
{
    AUTO_LOCK_WCS(mainLock);

    assert(PathEncoderIdx <= 1);

    if(carets[0].bActive && carets[1].bActive)
        return carets[0].pe;
    else if(carets[1].bActive)
        return carets[1].pe;
    return carets[0].pe;
}

int  cCaretMotionSim::GetPathEncoderDir(char PathEncoderIdx)
{
    AUTO_LOCK_WCS(mainLock);

    assert(PathEncoderIdx <= 1);

    if(carets[0].bActive && carets[1].bActive)
        return carets[0].dir;
    else if(carets[1].bActive)
        return carets[1].dir;
    return carets[0].dir;
}

void cCaretMotionSim::ResetPathEncoder()
{
    AUTO_LOCK_WCS(mainLock);

    carets[0].pe = carets[1].pe = 0;
}
