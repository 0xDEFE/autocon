object DebugStateForm: TDebugStateForm
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'Debug'
  ClientHeight = 454
  ClientWidth = 134
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 133
    Height = 454
    Align = alLeft
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitHeight = 446
    object btnSet25: TSpeedButton
      AlignWithMargins = True
      Left = 3
      Top = 283
      Width = 127
      Height = 22
      Align = alTop
      Caption = 'Set to 25'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = btnSet25Click
      ExplicitLeft = 50
      ExplicitTop = 276
      ExplicitWidth = 41
    end
    object btnSet0: TSpeedButton
      AlignWithMargins = True
      Left = 3
      Top = 255
      Width = 127
      Height = 22
      Align = alTop
      Caption = 'Set to 0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = btnSet0Click
      ExplicitLeft = 11
      ExplicitTop = 248
      ExplicitWidth = 41
    end
    object btnSet100: TSpeedButton
      AlignWithMargins = True
      Left = 3
      Top = 311
      Width = 127
      Height = 22
      Align = alTop
      Caption = 'Set to 100'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = btnSet100Click
      ExplicitLeft = 0
    end
    object Panel1: TPanel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 127
      Height = 25
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  '#1050#1086#1085#1090'-'#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Green1: TShape
        Left = 55
        Top = 7
        Width = 12
        Height = 12
        Brush.Color = clLime
      end
      object Red1: TShape
        Left = 55
        Top = 7
        Width = 12
        Height = 12
        Brush.Color = clRed
      end
    end
    object PanelUMU1: TPanel
      AlignWithMargins = True
      Left = 3
      Top = 34
      Width = 127
      Height = 25
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  '#1041#1059#1052' '#8470'1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object Green2: TShape
        Left = 55
        Top = 7
        Width = 12
        Height = 12
        Brush.Color = clLime
        Visible = False
      end
      object Red2: TShape
        Left = 55
        Top = 7
        Width = 12
        Height = 12
        Brush.Color = clRed
      end
      object m_DownloadSpeedLabel1: TLabel
        Left = 73
        Top = 0
        Width = 35
        Height = 10
        Caption = 'D: 0 KB/s'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object m_UploadSpeedLabel1: TLabel
        Left = 73
        Top = 7
        Width = 35
        Height = 10
        Caption = 'U: 0 KB/s'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object m_ErrorsLabel1: TLabel
        Left = 73
        Top = 15
        Width = 21
        Height = 10
        Caption = 'E: 0/0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object PanelUMU2: TPanel
      AlignWithMargins = True
      Left = 3
      Top = 65
      Width = 127
      Height = 25
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  '#1041#1059#1052' '#8470'2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object Green3: TShape
        Left = 55
        Top = 7
        Width = 12
        Height = 12
        Brush.Color = clLime
        Visible = False
      end
      object Red3: TShape
        Left = 55
        Top = 7
        Width = 12
        Height = 12
        Brush.Color = clRed
      end
      object m_DownloadSpeedLabel2: TLabel
        Left = 73
        Top = 0
        Width = 35
        Height = 10
        Caption = 'D: 0 KB/s'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object m_UploadSpeedLabel2: TLabel
        Left = 73
        Top = 7
        Width = 35
        Height = 10
        Caption = 'U: 0 KB/s'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object m_ErrorsLabel2: TLabel
        Left = 73
        Top = 15
        Width = 21
        Height = 10
        Caption = 'E: 0/0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object PanelUMU3: TPanel
      AlignWithMargins = True
      Left = 3
      Top = 96
      Width = 127
      Height = 25
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  '#1041#1059#1052' '#8470'3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      object Green4: TShape
        Left = 55
        Top = 7
        Width = 12
        Height = 12
        Brush.Color = clLime
        Visible = False
      end
      object Red4: TShape
        Left = 55
        Top = 7
        Width = 12
        Height = 12
        Brush.Color = clRed
      end
      object m_DownloadSpeedLabel3: TLabel
        Left = 73
        Top = 0
        Width = 35
        Height = 10
        Caption = 'D: 0 KB/s'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object m_UploadSpeedLabel3: TLabel
        Left = 73
        Top = 7
        Width = 35
        Height = 10
        Caption = 'U: 0 KB/s'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object m_ErrorsLabel3: TLabel
        Left = 73
        Top = 15
        Width = 21
        Height = 10
        Caption = 'E: 0/0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object m_ShowConsole: TCheckBox
      AlignWithMargins = True
      Left = 3
      Top = 127
      Width = 127
      Height = 17
      Align = alTop
      Caption = 'Show console'
      TabOrder = 4
      OnClick = m_ShowConsoleClick
    end
    object m_ManualModeCheck: TCheckBox
      AlignWithMargins = True
      Left = 3
      Top = 150
      Width = 127
      Height = 17
      Align = alTop
      Caption = 'Manual mode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = m_ManualModeCheckClick
    end
    object PECheck: TCheckBox
      AlignWithMargins = True
      Left = 3
      Top = 173
      Width = 127
      Height = 17
      Align = alTop
      Caption = 'Test Pathencode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnClick = PECheckClick
    end
    object ScreenshotBtn: TBitBtn
      Left = 0
      Top = 193
      Width = 133
      Height = 25
      Align = alTop
      Caption = #1057#1082#1088#1080#1085#1096#1086#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = ScreenshotBtnClick
    end
    object GridPanel1: TGridPanel
      AlignWithMargins = True
      Left = 3
      Top = 221
      Width = 127
      Height = 28
      Align = alTop
      ColumnCollection = <
        item
          Value = 50.000000000000000000
        end
        item
          Value = 50.000000000000000000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = btnSave
          Row = 0
        end
        item
          Column = 1
          Control = btnLoad
          Row = 0
        end>
      RowCollection = <
        item
          Value = 100.000000000000000000
        end>
      TabOrder = 8
      object btnSave: TSpeedButton
        Left = 1
        Top = 1
        Width = 62
        Height = 26
        Align = alClient
        Caption = 'Save'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = btnSaveClick
        ExplicitLeft = 48
        ExplicitTop = 25
        ExplicitWidth = 41
        ExplicitHeight = 22
      end
      object btnLoad: TSpeedButton
        Left = 63
        Top = 1
        Width = 63
        Height = 26
        Align = alClient
        Caption = 'Load'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = btnLoadClick
        ExplicitLeft = 56
        ExplicitTop = 224
        ExplicitWidth = 41
        ExplicitHeight = 22
      end
    end
    object GridPanel2: TGridPanel
      AlignWithMargins = True
      Left = 3
      Top = 339
      Width = 127
      Height = 36
      Align = alTop
      ColumnCollection = <
        item
          Value = 33.333333596781890000
        end
        item
          Value = 33.333333596781890000
        end
        item
          Value = 33.333332806436220000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = cSaveBtn
          Row = 0
        end
        item
          Column = 1
          Control = cLoadBtn
          Row = 0
        end
        item
          Column = 2
          Control = cCheckBtn
          Row = 0
        end>
      RowCollection = <
        item
          Value = 100.000000000000000000
        end
        item
          SizeStyle = ssAuto
        end>
      TabOrder = 9
      object cSaveBtn: TSpeedButton
        AlignWithMargins = True
        Left = 4
        Top = 4
        Width = 35
        Height = 28
        Align = alClient
        Caption = 'cSave'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = cSaveBtnClick
        ExplicitLeft = 48
        ExplicitTop = 25
        ExplicitWidth = 41
        ExplicitHeight = 22
      end
      object cLoadBtn: TSpeedButton
        AlignWithMargins = True
        Left = 45
        Top = 4
        Width = 35
        Height = 28
        Align = alClient
        Caption = 'cLoad'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = cLoadBtnClick
        ExplicitTop = 0
      end
      object cCheckBtn: TSpeedButton
        AlignWithMargins = True
        Left = 86
        Top = 4
        Width = 37
        Height = 28
        Align = alClient
        Caption = 'cCheck'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = cCheckBtnClick
        ExplicitLeft = 56
        ExplicitTop = 224
        ExplicitWidth = 41
        ExplicitHeight = 22
      end
    end
    object GridPanel3: TGridPanel
      Left = 0
      Top = 378
      Width = 133
      Height = 31
      Align = alTop
      ColumnCollection = <
        item
          Value = 40.019654359659420000
        end
        item
          Value = 59.980345640340570000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = exceptChannelEdit
          Row = 0
        end
        item
          Column = 1
          Control = IgnoreExceptBtn
          Row = 0
        end>
      RowCollection = <
        item
          Value = 100.000000000000000000
        end>
      TabOrder = 10
      object exceptChannelEdit: TEdit
        AlignWithMargins = True
        Left = 4
        Top = 4
        Width = 46
        Height = 23
        Align = alClient
        TabOrder = 0
        Text = '0'
        ExplicitHeight = 21
      end
      object IgnoreExceptBtn: TSpeedButton
        AlignWithMargins = True
        Left = 56
        Top = 4
        Width = 73
        Height = 23
        Align = alClient
        Caption = 'IgnoreExcept'
        OnClick = IgnoreExceptBtnClick
        ExplicitLeft = 104
        ExplicitTop = 8
        ExplicitWidth = 23
        ExplicitHeight = 22
      end
    end
    object DisableAScanStrobeCheck: TCheckBox
      Left = 0
      Top = 409
      Width = 133
      Height = 17
      Align = alTop
      Caption = 'DisableAScanStrobeCheck'
      TabOrder = 11
      OnClick = DisableAScanStrobeCheckClick
      ExplicitLeft = 24
      ExplicitTop = 416
      ExplicitWidth = 97
    end
    object DisableBScanStrobeCheck: TCheckBox
      Left = 0
      Top = 426
      Width = 133
      Height = 17
      Align = alTop
      Caption = 'DisableBScanStrobeCheck'
      TabOrder = 12
      OnClick = DisableBScanStrobeCheckClick
      ExplicitLeft = 48
      ExplicitTop = 432
      ExplicitWidth = 97
    end
  end
  object Panel3: TPanel
    Left = 133
    Top = 0
    Width = 1
    Height = 454
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 446
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 1
      Height = 31
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GreenShape: TShape
        Left = 239
        Top = 11
        Width = 12
        Height = 12
        Brush.Color = clLime
      end
      object RedShape: TShape
        Left = 239
        Top = 11
        Width = 12
        Height = 12
        Brush.Color = clRed
      end
      object ClearButton: TButton
        Left = 51
        Top = 5
        Width = 75
        Height = 23
        Caption = 'Clear'
        TabOrder = 0
        OnClick = ClearButtonClick
      end
      object StartStopButton: TButton
        Left = 155
        Top = 5
        Width = 75
        Height = 23
        Caption = 'Start / Stop'
        TabOrder = 1
        OnClick = StartStopButtonClick
      end
    end
    object Chart1: TChart
      Left = 0
      Top = 31
      Width = 1
      Height = 423
      AllowPanning = pmNone
      LeftWall.Visible = False
      Legend.Visible = False
      ScrollMouseButton = mbLeft
      Title.Text.Strings = (
        'TChart')
      BottomAxis.LabelsFormat.TextAlignment = taCenter
      DepthAxis.LabelsFormat.TextAlignment = taCenter
      DepthTopAxis.LabelsFormat.TextAlignment = taCenter
      LeftAxis.LabelsFormat.TextAlignment = taCenter
      Panning.MouseWheel = pmwNone
      RightAxis.LabelsFormat.TextAlignment = taCenter
      TopAxis.LabelsFormat.TextAlignment = taCenter
      View3D = False
      Zoom.Allow = False
      Zoom.MouseButton = mbRight
      Zoom.Pen.Mode = pmNotXor
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitHeight = 415
      DefaultCanvas = 'TGDIPlusCanvas'
      ColorPaletteIndex = 13
      object Series1: TBarSeries
        Marks.Visible = False
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Bar'
        YValues.Order = loNone
      end
    end
  end
  object ScreenshotTimer: TTimer
    Enabled = False
    Interval = 40
    OnTimer = ScreenshotTimerTimer
    Left = 96
    Top = 120
  end
  object ValidationTimer: TTimer
    Interval = 4000
    OnTimer = ValidationTimerTimer
    Left = 96
    Top = 160
  end
end
