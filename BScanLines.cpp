//---------------------------------------------------------------------------

#pragma hdrstop

#include "BScanLines.h"
#include "Autocon.inc"
//---------------------------------------------------------------------------
//#pragma package(smart_init)

TRect GetTop(TRect src, int Percent)
{
    return Rect(src.Left, src.Top, src.Right, src.Top + (src.Bottom - src.Top) * Percent / 100);
}

TRect GetBtm(TRect src, int Percent)
{
    return Rect(src.Left, src.Bottom - (src.Bottom - src.Top) * Percent / 100, src.Right, src.Bottom);
}

TRect DelTopBtm(TRect src, int TopPercent, int BtmPercent)
{
    return Rect(src.Left, src.Top + (src.Bottom - src.Top) * TopPercent / 100, src.Right, src.Bottom - (src.Bottom - src.Top) * BtmPercent / 100);
}

TRect DelTop(TRect src, int TopPercent)
{
    return Rect(src.Left, src.Top + src.Height() * TopPercent / 100, src.Right, src.Bottom);
}

TRect DelBtm(TRect src, int Percent)
{
    return Rect(src.Left, src.Top, src.Right, src.Bottom - src.Height() * Percent / 100);
}

cBScanLines::cBScanLines(void)
{
//    Buffer = NULL;
    Buffer = new TBitmap();
	Buffer->SetSize(100, 100);
	//Added by KirillB
	//Tranparency by Color
	Buffer->Transparent = true;
	Buffer->TransparentColor = clWebDeepPink; //transparent color
	Buffer->TransparentMode = tmFixed;
    Used = true;


	for (int x = 0; x < 6; x++)
	{
		//Added by KirillB
		LineDrawType[x] = LINEDRAW_STD;
		for (int y = 0; y < 6; y++)
		{
			BtnVisible[x][y] = false;
			BtnBigText[x][y] = "";
			BtnColor[x][y] = clWhite;
			BtnBtnState[x][y] = true;
			BtnHighlight[x][y] = HIGHLIGHT_NONE;
			BtnChannelBehind[x][y]=true;
            BtnChannelIsActive[x][y] = true;
		}
	}

    bOnlyOneCol = false;

    BtnBtnColor = clSilver;
	LineHilightType = HIGHLIGHT_NONE;
}

cBScanLines::~cBScanLines(void)
{
    delete Buffer;
}

int cBScanLines::Click(int X, int Y)
{
    TPoint pt = Point(X, Y);
    for (int LineIdx = 0; LineIdx < BScanLineCount; LineIdx++)
    {
        for (int BtnIdx = 0; BtnIdx < BtnColumnCount * 2; BtnIdx++)
            if (BtnVisible[BtnIdx][LineIdx])
            {
                if (PtInRect(BtnBtnRect[BtnIdx][LineIdx], pt) ||                    //���� ������ � �������. ������
                    (PtInRect(BtnSmallTextRect[BtnIdx][LineIdx], pt) && (LineDrawType[LineIdx]!=LINEDRAW_SMALL_LINE)))//��� � ������������� ������, � ��� ��������� �� �������� LINEDRAW_SMALL_LINE
                {
                    BtnBtnState[BtnIdx][LineIdx] = !BtnBtnState[BtnIdx][LineIdx];
                    return - BtnId[BtnIdx][LineIdx];
				}

				if (PtInRect(BtnBigTextRect[BtnIdx][LineIdx], pt) ||
                    (PtInRect(BtnSmallTextRect[BtnIdx][LineIdx], pt) && (LineDrawType[LineIdx]==LINEDRAW_SMALL_LINE)))
                {
                    return BtnId[BtnIdx][LineIdx];
                }
            }
    }
    return -1;
}

bool cBScanLines::GetBtnState(int id)
{
    for (int LineIdx = 0; LineIdx < BScanLineCount; LineIdx++)
        for (int BtnIdx = 0; BtnIdx < BtnColumnCount * 2; BtnIdx++)
            if(BtnId[BtnIdx][LineIdx] == id)
                return BtnBtnState[BtnIdx][LineIdx];
    return false;
}

void cBScanLines::SetBtnState(int id, bool bState)
{
    for (int LineIdx = 0; LineIdx < BScanLineCount; LineIdx++)
        for (int BtnIdx = 0; BtnIdx < BtnColumnCount * 2; BtnIdx++)
            if(BtnId[BtnIdx][LineIdx] == id)
            {
                BtnBtnState[BtnIdx][LineIdx] = bState;
                break;
            }
}

bool cBScanLines::GetSysCoord(int X, int Y, int MaxSysCoord, int* SysCoord, int* BScanLine)
{
    TPoint pt = Point(X - BoundsRect.Left, Y - BoundsRect.Top);
    for (int LineIdx = 0; LineIdx < BScanLineCount; LineIdx++)
    {
        if (PtInRect(LineRect[LineIdx], pt))
        {
            *SysCoord = MaxSysCoord * (pt.X - LineRect[LineIdx].Left) / (LineRect[LineIdx].Right - LineRect[LineIdx].Left);
            *BScanLine = LineIdx;
            return true;
        }
    }
    return false;
}

void cBScanLines::Refresh(void)
{
    // ������

//    if (Buffer) delete Buffer;
//    Buffer = new TBitmap();


    // ������ ��� ������

	LineHeight = BoundsRect.Size.Height / BScanLineCount;
	SmallLineHeight = LineHeight / 5;
    BtnWidth = LineHeight * 0.6;

    if(!bOnlyOneCol)
    	LineWidth = BoundsRect.Size.Width - BtnColumnCount * 2 * BtnWidth;
    else
        LineWidth = BoundsRect.Size.Width - BtnWidth;

	int Y = 0;
	for (int LineIdx = 0; LineIdx < BScanLineCount; LineIdx++)
	{
		eBScanLineDrawType lineType = LineDrawType[LineIdx];

		int currLineHeight = 0;
		switch(lineType)
		{
			case LINEDRAW_STD: 			currLineHeight = LineHeight; break;
			case LINEDRAW_SMALL_LINE: 	currLineHeight = SmallLineHeight; break;
			default:
				currLineHeight = LineHeight;
		}

		LineRect[LineIdx] = Rect(BtnColumnCount * BtnWidth, Y, BtnColumnCount * BtnWidth + LineWidth, Y + currLineHeight);
        for (int BtnIdx = 0; BtnIdx < BtnColumnCount; BtnIdx++)
        {
			BtnRect[BtnIdx][LineIdx] = Rect(BtnIdx * BtnWidth, Y, BtnIdx * BtnWidth + BtnWidth, Y + currLineHeight);
			BtnRect[BtnIdx + BtnColumnCount][LineIdx] = Rect(BoundsRect.Size.Width - (BtnColumnCount - BtnIdx) * BtnWidth, Y, BoundsRect.Size.Width - (BtnColumnCount - BtnIdx) * BtnWidth + BtnWidth, Y + currLineHeight);
        }
		Y = Y + currLineHeight;
	}

	Buffer->FreeImage();
	Buffer->SetSize(BoundsRect.Size.Width, Y);
	//Added by KirillB
	//Filling with transparent color
	Buffer->Canvas->Brush->Color = clWebDeepPink;
	Buffer->Canvas->FillRect(TRect(0,0,Buffer->Width,Buffer->Height));

    for (int LineIdx = 0; LineIdx < BScanLineCount; LineIdx++)
        for (int BtnIdx = 0; BtnIdx < BtnColumnCount * 2; BtnIdx++)
		{
			eBScanLineDrawType lineType = LineDrawType[LineIdx];

			if(lineType == LINEDRAW_STD)
			{
				BtnBigTextRect[BtnIdx][LineIdx] = DelTop(BtnRect[BtnIdx][LineIdx], 30);
				BtnSmallTextRect[BtnIdx][LineIdx] = GetTop(BtnRect[BtnIdx][LineIdx], 30);
				BtnBtnRect[BtnIdx][LineIdx] = GetTop(BtnRect[BtnIdx][LineIdx], 30);
			}
			else if(lineType == LINEDRAW_SMALL_LINE)
			{
				BtnBigTextRect[BtnIdx][LineIdx] = TRect(0,0,0,0);
				BtnSmallTextRect[BtnIdx][LineIdx] = BtnRect[BtnIdx][LineIdx];
				BtnBtnRect[BtnIdx][LineIdx] = TRect(0,0,0,0);
            }
        }
}

void cBScanLines::ApplyBtnHighlightLevel(const char level,unsigned int LineIdx, unsigned int BtnIdx)
{
	//if(!BtnVisible[BtnIdx][LineIdx])
	//	return;

	if(BtnChannelBehind[BtnIdx][LineIdx])
		BtnColor[BtnIdx][LineIdx] = RGB(255,255-level,255-level);
	else
		BtnColor[BtnIdx][LineIdx] = RGB(255-level,255-level,255);
}

//Added by KirillB
//��� ��������� ����� ������ � ����������� �� ����� ����

float CalcLuminance( TColor srcColor )
{
    float R,G,B;
    R = float(COLORVALUE( srcColor,0))/255.f;
    G = float(COLORVALUE( srcColor,1))/255.f;
    B = float(COLORVALUE( srcColor,2))/255.f;

    return sqrt(0.299 * R * R + 0.587 * G * G  + 0.114 * B * B);
}

TColor CalcTextColor(TColor srcColor)
{
    return (CalcLuminance(srcColor) < 0.5) ? clWhite : clBlack;
}

TColor ConvertToGrayscale(TColor srcCol, float highlight_level = 0.9)
{
    unsigned char val = ((0.30 * GetRValue(srcCol)) +
                  (0.59 * GetGValue(srcCol)) +
                  (0.11 * GetBValue(srcCol)));
    val = LERP(val, 255,highlight_level);
    return TColor(RGB(val,val,val));
}

TColor FadeColor(TColor srcCol, float highlight_level = 0.9)
{
    unsigned char R,G,B;
    R = GetRValue(srcCol);
    G = GetGValue(srcCol);
    B = GetBValue(srcCol);


    float eps = 0.00001f;
    float rgbMax = std::max(R, std::max(G,B)) + eps;
    float rCoeff = float(R) / rgbMax;
    float gCoeff = float(G) / rgbMax;
    float bCoeff = float(B) / rgbMax;

    float avval = 1.f - highlight_level;
    R = (highlight_level + rCoeff * avval) * 255.f;
    G = (highlight_level + gCoeff * avval) * 255.f;
    B = (highlight_level + bCoeff * avval) * 255.f;

    return TColor(RGB(R,G,B));
}

void DrawTextMultiline(TCanvas* Canvas,UnicodeString& text, TRect& rect,TTextFormat TextFormat)
{
    TRect rc;
    UnicodeString strTmp;
    UnicodeString strVal = text;

    int intPos, i = 0;
    int ItemHeight = abs(Canvas->Font->Height);

    while((intPos = strVal.Pos("\r\n")) > 0)
    {
        strTmp = (intPos > 0) ? strVal.SubString(1, intPos - 1) : strVal;
        rc = rect;
        rc.Top = rect.Top + i * ItemHeight;
        Canvas->TextRect(rc, strTmp,TextFormat);
        strVal = strVal.SubString(intPos + 2, strVal.Length());
        i += 1;
    }

    rc = rect;
    rc.Top = rect.Top + i * ItemHeight;
    Canvas->TextRect(rc, strVal,TextFormat);
    Canvas->Brush->Style = bsClear;
    Canvas->Rectangle(rect);
}

void cBScanLines::PaintToBuffer(void)
{
    // ���������

    UnicodeString str;
    TTextFormat TextFormat_EnableBtn;
    TextFormat_EnableBtn.Clear();
#ifndef DISABLE_DEFECT_IN_BTM_HIGHLIGHTING
    TextFormat_EnableBtn << tfRight << tfVerticalCenter <<  tfSingleLine;
#else
    TextFormat_EnableBtn << tfCenter << tfVerticalCenter <<  tfSingleLine;
#endif

    TTextFormat TextFormat_BigBtn;
    TextFormat_BigBtn.Clear();
    TextFormat_BigBtn << tfCenter << tfBottom;

    TTextFormat TextFormat_SmallBtn;
    TextFormat_SmallBtn.Clear();
    TextFormat_SmallBtn << tfCenter << tfVerticalCenter << tfSingleLine;

    for (int LineIdx = 0; LineIdx < BScanLineCount; LineIdx++)
    {
        //Draw center line
        Buffer->Canvas->MoveTo((LineRect[LineIdx].left + LineRect[LineIdx].right) / 2, LineRect[LineIdx].Top);
        Buffer->Canvas->LineTo((LineRect[LineIdx].left + LineRect[LineIdx].right) / 2, LineRect[LineIdx].Bottom);

        //Temporary colors for highlighting
        TColor btn_Col, btn_ChCol, btn_FontCol,btn_BtnCol,btn_ChFontCol, btn_BorderCol;
        eBScanHighlightType btn_Hg;

		for (int BtnIdx = 0; BtnIdx < BtnColumnCount * 2; BtnIdx++)
			if (BtnVisible[BtnIdx][LineIdx])
			{
                //If active - set to default colors
                if(BtnChannelIsActive[BtnIdx][LineIdx] && BtnBtnState[BtnIdx][LineIdx])
                {
                    btn_Col = BtnColor[BtnIdx][LineIdx];
                    btn_ChCol = BtnChColor[BtnIdx][LineIdx];
                    btn_BtnCol = BtnBtnState[BtnIdx][LineIdx] ? btn_ChCol : BtnBtnColor;
                    btn_FontCol = clBlack;
                    btn_ChFontCol = CalcTextColor(btn_ChCol);
                    btn_Hg = BtnHighlight[BtnIdx][LineIdx];
                    btn_BorderCol = clBlack;
                }
                //If not active - calculate whit'er colors
                else
                {
                    btn_Col = FadeColor(BtnColor[BtnIdx][LineIdx],0.98);
                    btn_ChCol = FadeColor(BtnChColor[BtnIdx][LineIdx]);
                    btn_BtnCol = FadeColor(BtnChColor[BtnIdx][LineIdx],0.9);
                    btn_FontCol = FadeColor(clBlack);
                    btn_ChFontCol = FadeColor(CalcTextColor(btn_ChCol),0.5);
                    btn_Hg = HIGHLIGHT_NONE;
                    btn_BorderCol = RGB(220,220,220);
                }

                //Draw big text rect
				Buffer->Canvas->Brush->Color = btn_Col;
				Buffer->Canvas->FillRect(BtnBigTextRect[BtnIdx][LineIdx]);
                //Draw small text rect
				Buffer->Canvas->FillRect(BtnSmallTextRect[BtnIdx][LineIdx]);
                //Draw button (on/off) text rect
				Buffer->Canvas->Brush->Color = btn_BtnCol;
				Buffer->Canvas->FillRect(BtnBtnRect[BtnIdx][LineIdx]);

				/*Buffer->Canvas->Brush->Style = bsClear;
				if(LineHilightType == HIGHLIGHT_ERROR)//���� ��������� ��� ����� �� ����������� ��
				{
					Buffer->Canvas->Pen->Color = clRed;
					Buffer->Canvas->Pen->Width = 2;
				}*/


                //Draw button frame rectangle
                Buffer->Canvas->Pen->Color=btn_BorderCol;
				Buffer->Canvas->Pen->Width = 1;
				Buffer->Canvas->Brush->Style = bsClear;
				Buffer->Canvas->Rectangle(BtnRect[BtnIdx][LineIdx]);

                //Draw btn on/off label
				Buffer->Canvas->Font->Height = - LineHeight * 1 / 7;
                Buffer->Canvas->Font->Color = btn_ChFontCol;
                if (BtnBtnState[BtnIdx][LineIdx]) str = "���"; else str = "����";
                TRect stTextRect = BtnBtnRect[BtnIdx][LineIdx];
#ifndef DISABLE_DEFECT_IN_BTM_HIGHLIGHTING
                stTextRect.right -= 5;
#endif
                Buffer->Canvas->TextRect(stTextRect, str, TextFormat_EnableBtn);

#ifndef DISABLE_DEFECT_IN_BTM_HIGHLIGHTING

                //Calculate highlight color
				Buffer->Canvas->Brush->Style = bsSolid;
				switch(btn_Hg)
				{
					case HIGHLIGHT_WARNING:
						Buffer->Canvas->Brush->Color = clYellow;
						break;
					case HIGHLIGHT_ERROR:
						Buffer->Canvas->Brush->Color = clRed;
						break;
					case HIGHLIGHT_OK:
						Buffer->Canvas->Brush->Color = RGB(0,255,0);
						break;
					default:
					   Buffer->Canvas->Brush->Color = clGray;
				}

                //Calculate highlight pictogramm rectangle
				TRect pictoRectTop;
				TRect pictoRectBtm;
				if(LineDrawType[LineIdx] == LINEDRAW_SMALL_LINE)
				{
					int size = BtnSmallTextRect[BtnIdx][LineIdx].Height() - 4;
					pictoRectTop=TRect(BtnSmallTextRect[BtnIdx][LineIdx].left+2,
								BtnSmallTextRect[BtnIdx][LineIdx].top+2,
								BtnSmallTextRect[BtnIdx][LineIdx].left+2+size,
								BtnSmallTextRect[BtnIdx][LineIdx].top+2+size);
				}
				else
				{
                    int size = BtnSmallTextRect[BtnIdx][LineIdx].Height()/2 - 2;
					pictoRectTop=TRect(BtnSmallTextRect[BtnIdx][LineIdx].left+2,
								BtnSmallTextRect[BtnIdx][LineIdx].top+2,
								BtnSmallTextRect[BtnIdx][LineIdx].left+2+size,
								BtnSmallTextRect[BtnIdx][LineIdx].top+2+size);
                    pictoRectBtm = pictoRectTop;
                    pictoRectBtm.Offset(0,pictoRectTop.Height() + 1);
				}

                //Draw pictogramm rectangle with highlight color
				Buffer->Canvas->Rectangle(pictoRectTop.left,pictoRectTop.top,
                                            pictoRectTop.right,pictoRectTop.bottom);
#endif


				Buffer->Canvas->Brush->Style = bsClear;
                Buffer->Canvas->Font->Name = "Myriad Pro";
                Buffer->Canvas->Font->Style = TFontStyles() << fsBold;
                Buffer->Canvas->Font->Color = btn_FontCol;

                if(LineDrawType[LineIdx] != LINEDRAW_SMALL_LINE)
                {
                    Buffer->Canvas->Font->Height = -LineHeight * 1 / 4;
                    Buffer->Canvas->TextRect(BtnBigTextRect[BtnIdx][LineIdx], BtnBigText[BtnIdx][LineIdx], TextFormat_BigBtn);
                }
                else
                {
                    stTextRect = BtnSmallTextRect[BtnIdx][LineIdx];
#ifndef DISABLE_DEFECT_IN_BTM_HIGHLIGHTING
                    stTextRect.left = pictoRectTop.right + 1;
#else
                    stTextRect.left = BtnSmallTextRect[BtnIdx][LineIdx].left+2;
#endif
                    Buffer->Canvas->Font->Height = -BtnSmallTextRect[BtnIdx][LineIdx].Height()*3/4;//(pictoRect.Height() - 2);
                    Buffer->Canvas->TextRect(stTextRect, BtnBigText[BtnIdx][LineIdx], TextFormat_SmallBtn);
                }


                //End - clear to default values
                Buffer->Canvas->Font->Color = clBlack;
                Buffer->Canvas->Brush->Style = bsSolid;
                Buffer->Canvas->Brush->Color = clBlack;
                Buffer->Canvas->Pen->Color = clBlack;
            }
    }
}

int cBScanLines::SysCoordToScreen(int SysCoord, int MaxSysCoord)
{
    return LineRect[0].Left + (LineRect[0].Right - LineRect[0].Left) * SysCoord / MaxSysCoord;
};

cBScanLines& cBScanLines::operator=(const cBScanLines& other)
{
	if(this->Buffer)
	{
		delete this->Buffer;
		this->Buffer = NULL;
	}

	memcpy(this, &other, sizeof(other));

	this->Buffer = new TBitmap;
	this->Buffer->SetSize(other.Buffer->Width,other.Buffer->Height);

	this->Buffer->Transparent = true;
	this->Buffer->TransparentColor = clWebDeepPink; //transparent color
	this->Buffer->TransparentMode = tmFixed;
    return *this;
}
