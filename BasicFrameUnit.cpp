//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "BasicFrameUnit.h"
#include "MainUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//TBasicFrame *MainUnitFrame;
//---------------------------------------------------------------------------
__fastcall TBasicFrame::TBasicFrame(TComponent* Owner)
    : TFrame(Owner)
{
    bModal = false;
    aquiredSignalsFlags = NULL;
    frameFlags = NULL;
    pParentFrame = NULL;
    pConnectedOwner = NULL;
    defaultOwner = this;
    currFrame = NULL;
    bPopOnEmpty = false;
}
//---------------------------------------------------------------------------

void TBasicFrame::attachFrame(TBasicFrame* pFrame)
{
    if(!defaultOwner)
        return;
    detachFrame();
    currFrame = pFrame;
    attachFrame(pFrame, defaultOwner);
}
void TBasicFrame::detachFrame()
{
    if(currFrame && defaultOwner)
    {
        detachFrame(currFrame, defaultOwner);
        currFrame = NULL;
    }
}
void TBasicFrame::attachFrame(TBasicFrame* pFrame, TWinControl* pOwner)
{
    TRACE("Attach frame: %s",AnsiString(pFrame->GetFrameName()).c_str());
    pFrame->Parent = pOwner;
    pFrame->Align = alClient;
    pFrame->Enabled = true;
    pOwner->InsertComponent(pFrame);
    pFrame->Visible = true;
    pFrame->DoMFrameAttach(this);
    pFrame->SetFrameFlags(pFrame->GetFrameFlags());
}
void TBasicFrame::detachFrame(TBasicFrame* pFrame, TWinControl* pOwner)
{
    TRACE("Detach frame: %s",AnsiString(pFrame->GetFrameName()).c_str());
    pFrame->DoMFrameDetach(this);
    pFrame->Visible = false;
    pOwner->RemoveComponent(pFrame);
    pFrame->Parent = NULL;
}
void TBasicFrame::__pushFrame(TBasicFrame* pFrame,DWORD frameFlags,bool bModal)
{
    TRACE("Push frame: %s",AnsiString(pFrame->GetFrameName()).c_str());
    if(!frameStack.empty() && bModal)
        frameStack.back()->Enabled = false;
    frameStack.push_back(pFrame);

    pFrame->Initialize(this,pMainUnitForm,frameFlags,bModal);
    pFrame->OnMFrameCreate(this);
    attachFrame(pFrame);
}
void TBasicFrame::popFrame()
{
    if(frameStack.empty())
        return;

    TBasicFrame* pFrame = frameStack.back();
    TRACE("Pop frame: %s",AnsiString(pFrame->GetFrameName()).c_str());
    frameStack.pop_back();

    detachFrame();
    pMainUnitForm->deleteLater( pFrame );
    if(!frameStack.empty())
    {
        attachFrame(frameStack.back());
    }
    else
    {
        if(bPopOnEmpty)
        {
            if(pParentFrame)
                pParentFrame->popFrame();
            else
                pMainUnitForm->DoMainUnitDestroy();
        }
    }

}

void TBasicFrame::__connectFrame(TBasicFrame* pFrame, DWORD frameFlags,TWinControl* pOwner)
{
    connectedFrames.push_back(pFrame);
    pFrame->Initialize(this,pMainUnitForm,frameFlags,false,pOwner);
    pFrame->OnMFrameCreate(this);
    attachFrame( pFrame, pOwner );
}
void TBasicFrame::disconnectFrame(TBasicFrame* pFrame)
{
    std::list<TBasicFrame*>::iterator it = connectedFrames.begin();
    while(it!=connectedFrames.end())
    {
        if((*it) == pFrame)
        {
            if(pFrame->GetConnectedOwner())
                detachFrame( pFrame, pFrame->GetConnectedOwner() );
            pMainUnitForm->deleteLater( pFrame );
            it = connectedFrames.erase(it);
        }
        else it++;
    }
}


//---------------------------------------------------------------------------

void TBasicFrame::DoMFrameAttach(TBasicFrame* pMain)
{
    OnMFrameAttach(pMain);
}
void TBasicFrame::DoMFrameDetach(TBasicFrame* pMain)
{
    OnMFrameDetach(pMain);
}
void TBasicFrame::DoMFrameCreate(TBasicFrame* pMain)
{
    OnMFrameCreate(pMain);
}
void TBasicFrame::DoMFrameDestroy(TBasicFrame* pMain)
{
    OnMFrameDestroy(pMain);
    detachFrame();
    while(!frameStack.empty())
    {
        pMainUnitForm->deleteLater(frameStack.back());
        frameStack.pop_back();
    }
}

void TBasicFrame::OnUMUSignal(eMainUnitSignalType sigtype, sPtrListItem& sig)
{
    std::deque<TBasicFrame*>::reverse_iterator it = frameStack.rbegin();
    while(it != frameStack.rend())
    {
        TBasicFrame* pTopFrame = *it;
        it++;
        if(!pTopFrame->Enabled)
            break;

        DWORD aqSignals = pTopFrame->GetAquiredSignals();

        if(aqSignals & sigtype)
        {
            pTopFrame->OnUMUSignal(sigtype, sig);
        };
    }
};
void TBasicFrame::OnUMUArray(ringbuf<sPtrListItem>& sigList)
{
    std::deque<TBasicFrame*>::reverse_iterator it = frameStack.rbegin();
    while(it != frameStack.rend())
    {
        TBasicFrame* pTopFrame = *it;
        it++;
        if(!pTopFrame->Enabled)
            break;

        DWORD aqSignals = pTopFrame->GetAquiredSignals();

        if(aqSignals & MU_SIGNAL_UMU_ARRAY)
        {
            pTopFrame->OnUMUArray( sigList );
        };
    }
};
void TBasicFrame::OnAcMotion(eMainUnitSignalType sigtype, int WorkCycle, eMotionState state, bool bStateChanged)
{
    std::deque<TBasicFrame*>::reverse_iterator it = frameStack.rbegin();
    while(it != frameStack.rend())
    {
        TBasicFrame* pTopFrame = *it;
        it++;
        if(!pTopFrame->Enabled)
            break;
        if(pTopFrame->GetAquiredSignals() & sigtype)
            pTopFrame->OnAcMotion(sigtype,WorkCycle,state,bStateChanged);
    }
};
void TBasicFrame::OnFormShortCut(TWMKey &Msg, bool &Handled)
{
    if(frameStack.empty())
        return;
    if(frameStack.back()->GetAquiredSignals() & MU_SIGNAL_FORM_SHORTCUT)
        frameStack.back()->OnFormShortCut(Msg,Handled);
};

bool TBasicFrame::SetAcState(TACState state, DWORD timeout)
{
    if ((!AutoconMain) || (!AutoconMain->ac))
        return false;

    if(!AutoconMain->ac->SetMode(state))
        return false;

    DWORD StartTime = GetTickCount();
    bool bSuccess = false;
    while((GetTickCount() - StartTime) < timeout)
    {
        if(AutoconMain->ac->GetMode() ==  state)
        {
            bSuccess = true;
            break;
        }

        Sleep(100);
        if(AutoconMain->ac->GetMode() != acWait)
    		continue;
    }

    return bSuccess;
}
