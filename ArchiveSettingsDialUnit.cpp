//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ArchiveDefinitions.h"
#include "ArchiveSettingsDialUnit.h"
//---------------------------------------------------------------------
#pragma resource "*.dfm"
TArchiveSettingsDial *ArchiveSettingsDial;
//--------------------------------------------------------------------- 
__fastcall TArchiveSettingsDial::TArchiveSettingsDial(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------
void __fastcall TArchiveSettingsDial::FormShow(TObject *Sender)
{
	if(tableColBtnArr.empty())
	{
		sTableColBtnDesc desc;
		UnicodeString caption;
		for( int i = 0; i < ADB_FIELDTYPE_END; i++)
		{
			if((GetFieldAssigns((eADBFieldType)i) & ADB_FIELDASSIGN_TABLE))
			{
				desc.pBtn = new TSpeedButton(this);
				desc.tableFieldType = i;
				desc.bState = ColBtnMask & (1<<i);

				caption = ADBFieldTypeToName(desc.tableFieldType);
				caption = StringReplace(caption,L" ", L"\r\n",TReplaceFlags(rfReplaceAll));

				desc.pBtn->Caption = caption;
				desc.pBtn->Width = 190;
				desc.pBtn->Height = 70;
				desc.pBtn->Font->Height = -16;
				desc.pBtn->Font->Style = TFontStyles() << fsBold;
				desc.pBtn->Font->Name = "Myriad Pro";
				desc.pBtn->GroupIndex = i + 100;
				desc.pBtn->Down = desc.bState;
				desc.pBtn->AllowAllUp = true;

				TNotifyEvent notifyOnClick = &(this->ColBtnClick);
				desc.pBtn->OnClick = notifyOnClick;

				tableColBtnArr.push_back(desc);
				m_SelectTableBtnsPanel->InsertControl(desc.pBtn);
			}
		}
	}
    else
    {
        updateButtonMask(true);
    }

	updateTableHeaderLabel();
}
//---------------------------------------------------------------------------

void TArchiveSettingsDial::updateTableHeaderLabel()
{
	int selCount = 0;
	for(unsigned int i = 0; i < tableColBtnArr.size(); i++)
	{
		if(tableColBtnArr[i].bState)
			selCount++;
    }

	m_TableHeaderLabel->Caption = StringFormatU(L"���� ������� (������� %d �� %d):",selCount,tableColBtnArr.size());
}

void __fastcall TArchiveSettingsDial::ColBtnClick(TObject *Sender)
{
	for(unsigned int i = 0; i < tableColBtnArr.size(); i++)
	{
		if(tableColBtnArr[i].pBtn == Sender)
		{
			tableColBtnArr[i].bState = tableColBtnArr[i].pBtn->Down;

			if(tableColBtnArr[i].bState) //������� �������, ������������� ���
				ColBtnMask |= (1<<tableColBtnArr[i].tableFieldType);
			else						//������� �� �������, ������� ���
				ColBtnMask &= ~(1<<tableColBtnArr[i].tableFieldType);

		}
	}
	updateTableHeaderLabel();
}
//---------------------------------------------------------------------------

DWORD TArchiveSettingsDial::GetSelectedColumns()
{
	return ColBtnMask;
}

void TArchiveSettingsDial::SetSelectedColumns(DWORD val)
{
	ColBtnMask = val;
}

void __fastcall TArchiveSettingsDial::m_ClearBtnClick(TObject *Sender)
{
    for(unsigned int i = 0; i < tableColBtnArr.size(); i++)
	{
        tableColBtnArr[i].pBtn->Down = false;
    }
    updateButtonMask();
}
//---------------------------------------------------------------------------

void __fastcall TArchiveSettingsDial::m_FillBtnClick(TObject *Sender)
{
    for(unsigned int i = 0; i < tableColBtnArr.size(); i++)
	{
        tableColBtnArr[i].pBtn->Down = true;
    }
    updateButtonMask();
}
//---------------------------------------------------------------------------

void __fastcall TArchiveSettingsDial::m_SetDefaultBtnClick(TObject *Sender)
{
    for(unsigned int i = 0; i < tableColBtnArr.size(); i++)
	{
        switch(tableColBtnArr[i].tableFieldType)
        {
            case ADB_FIELDTYPE_FILENAME:
            case ADB_FIELDTYPE_GANG_NUMBER:
            case ADB_FIELDTYPE_DATETIME:
            case ADB_FIELDTYPE_OPERATOR:
            case ADB_FIELDTYPE_PLET_NUMBER:
            case ADB_FIELDTYPE_JOINT_NUMBER:
            case ADB_FIELDTYPE_DEFECT_CODE:
            case ADB_FIELDTYPE_VALIDITY_GROUP:
                tableColBtnArr[i].pBtn->Down = true;
                break;
            default:
                tableColBtnArr[i].pBtn->Down = false;
                break;
        }

    }
    updateButtonMask();
}
//---------------------------------------------------------------------------

void TArchiveSettingsDial::updateButtonMask(bool bFromMaskToBtns)
{
    if(bFromMaskToBtns)
    {
        for(unsigned int i = 0; i < tableColBtnArr.size(); i++)
        {
            tableColBtnArr[i].bState = ColBtnMask & (1<<tableColBtnArr[i].tableFieldType);
            tableColBtnArr[i].pBtn->Down = tableColBtnArr[i].bState;
        }
    }
    else
    {
        for(unsigned int i = 0; i < tableColBtnArr.size(); i++)
        {
            tableColBtnArr[i].bState = tableColBtnArr[i].pBtn->Down;

            if(tableColBtnArr[i].bState) //������� �������, ������������� ���
                ColBtnMask |= (1<<tableColBtnArr[i].tableFieldType);
            else						//������� �� �������, ������� ���
                ColBtnMask &= ~(1<<tableColBtnArr[i].tableFieldType);
        }
    }
	updateTableHeaderLabel();
}

