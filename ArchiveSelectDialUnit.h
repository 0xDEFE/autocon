﻿/**
 * @file ArchiveSelectDialUnit.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef ArchiveSelectDialUnitH
#define ArchiveSelectDialUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ToolWin.hpp>
#include <Vcl.Imaging.pngimage.hpp>
//---------------------------------------------------------------------------

#include <vector>

/** \struct sArchiveSelectComponent
    \brief Описывает выбираемый элемент - кнопку
 */
struct sArchiveSelectComponent
{
	TBitmap* pImage;        //!< Иконка кнопки
	UnicodeString text;     //!< Текст кнопки
	int id;                 //!< Идентификатор
	TSpeedButton* btn;      //!< Указатель на кнопку
};

class TArchiveSelectDial : public TForm
{
__published:	// IDE-managed Components
	TFlowPanel *m_FlowPanel;
    TPanel *Panel1;
    TGridPanel *GridPanel1;
    TLabel *m_CaptionLabel;
    TSpeedButton *m_CloseBtn;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall m_SelectComponentClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall m_CloseBtnClick(TObject *Sender);
private:	// User declarations
	std::vector<sArchiveSelectComponent> selComponents; //!< Выбираемые элементы

	int selectedId;                                     //!< Идентификатор выбранного элемента
public:		// User declarations
	__fastcall TArchiveSelectDial(TComponent* Owner);

    TPoint buttonSize; //!< Размер кнопки
    int MinWidth;      //!< Минимальная ширина окна
    int GetSelectedComponentId(); //!< Получение идентификатора выбранного ответа

    void SetDialCaption(const char* str);  //!< Установка заголовка окна
	void AddComponent(int id, UnicodeString text,TBitmap* pImage = NULL); //!< Добавление нового варианта выбора
	int ShowSelect();   //!< Показать диалог
};
//---------------------------------------------------------------------------
extern PACKAGE TArchiveSelectDial *ArchiveSelectDial;
//---------------------------------------------------------------------------
#endif
