﻿//---------------------------------------------------------------------------

#include <vcl.h>
#include <DateUtils.hpp>
//#pragma hdrstop

#include "MainMenuUnit.h"
#include "AdjustingFormUnit.h"
#include "TuneTableFormUnit.h"
#include "CalibrationFormUnit.h"
#include "BScanFrameUnit.h"
#include "ManualTuneFrameUnit.h"
#include "EnterParamFrameUnit.h"

#include "AScanFrameUnit.h"
#include "Autocon.inc"

#include "ScreenMessageUnit3.h"

//---------------------------------------------------------------------------
//#pragma package(smart_init)
#pragma resource "*.dfm"
TMainMenuForm *MainMenuForm;

//---------------------------------------------------------------------------
__fastcall TMainMenuForm::TMainMenuForm(TComponent* Owner)
    : TForm(Owner)
{
    #ifdef BScanSize_12Byte
        assert(false && "BScanSize is 12 byte");
    #endif

	ChangeModeFlag = false;
    AutoconMain = new cAutoconMain;
}
//---------------------------------------------------------------------------//---------------------------------------------------------------------------
void __fastcall TMainMenuForm::FormCreate(TObject *Sender)
{
	this->BorderWidth = 100;
	//AutoconMain = NULL;

	#ifdef NDEBUG
//	IPComboBox->ItemIndex = 4;
//	OpenConnectionTimer->Enabled = true;
	#endif

    #ifdef FILTER_UMU_COUNT_TO_ONE         // Фильтруется конфиг - остаётся один БУМ (можно работать с 1-м побключенным БУМом, вля выбора IP включить DEBUG)
	IPComboBox->ItemIndex = 0;
	#endif

	#ifndef NDEBUG
	DebugMenuPanel->Visible = true;
	#endif

	TextLog = new TStringList();

    //Added by KirillB
    AutoconMain->Rep->UpdateChannelIndicationData();
    //Button6->Caption = "Настройка\r\nРучных ПЭП";
    //Button8->Caption = "Настройки\r\nПрограммы";

    #ifndef NDEBUG
        bDebugMenusAreHidden = false;
    #else
        bDebugMenusAreHidden = true;
    #endif
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::ExitButtonClick(TObject *Sender)
{
    Timer1->Enabled = false;
    ExitButton->Enabled = false;
    //MainForm->Close();

    this->Close();
    Application->Terminate();

    if (AutoconMain)
    {
        delete AutoconMain;
        AutoconMain = NULL;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::Timer1Timer(TObject *Sender)
{
	int UMUOKCount = 0;
    bool cavtk_test_ = false;

    InfoPanel->Caption = DateTimeToStr(Now());
//    UnicodeString Text;
	Memo1->Lines->Clear();

	if ((AutoconMain) && (AutoconMain->cavtk_test))
	{
        if (AutoconMain->cavtk_test->StateOK)
        {
            if (DebugStateForm)
                DebugStateForm->Green1->BringToFront();
            Text = "соединение установлено";
            cavtk_test_ = true;
        }
        else
        {
            if (DebugStateForm)
                DebugStateForm->Red1->BringToFront();
            Text = "нет соединения";
		}
    } else Text = "нет соединения";
    Memo1->Lines->Add(Format("Контроллер - %s", ARRAYOFCONST((Text))));

    if ((AutoconMain) && (AutoconMain->DEV) && (DebugStateForm)) {
        DebugStateForm->Series1->Clear();
        for (int i = -5; i < 5; i++)
            DebugStateForm->Series1->AddXY(i, AutoconMain->DEV->PathEncoderGis[i + 5]);
    }

	if ((AutoconMain) && (AutoconMain->cavtk_test))
	{
		for (int i = 0; i < AutoconMain->DEV->GetUMUCount(); i++)
		{
			if (AutoconMain->DEV->GetUMUOnLineStatus(i))
			{
                if (DebugStateForm)
                    switch (i)
                    {
                        case 0: {
                                    DebugStateForm->Red2->Visible = false;
                                    DebugStateForm->Green2->Visible = true;
                                    break;
                                }
                        case 1: {
                                    DebugStateForm->Red3->Visible = false;
                                    DebugStateForm->Green3->Visible = true;
                                    break;
                                }
                        case 2: {
                                    DebugStateForm->Red4->Visible = false;
                                    DebugStateForm->Green4->Visible = true;
                                    break;
                                }
                    }

				Text = "соединение установлено";
				UMUOKCount++;

                UpdateInfo();
			}
			else
			{
                if (DebugStateForm)
                    switch (i)
                    {
                        case 0: { DebugStateForm->Red2->Visible = true;
                                  DebugStateForm->Green2->Visible = false ;
                                  break; }
                        case 1: { DebugStateForm->Red3->Visible =true ;
								  DebugStateForm->Green3->Visible = false;
								  break; }
						case 2: { DebugStateForm->Red4->Visible =true ;
								  DebugStateForm->Green4->Visible =false ;
									break; }
					}
				Text = "нет соединения";
			}

			if (DebugStateForm)
			{
                if (AutoconMain->DEV->GetUMUCount() >= 1) DebugStateForm->m_ErrorsLabel1->Caption = "E: " + IntToStr(AutoconMain->DEV->GetUMUErrorMessageCount(0)) + "/" + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(0));
                if (AutoconMain->DEV->GetUMUCount() >= 2) DebugStateForm->m_ErrorsLabel2->Caption = "E: " + IntToStr(AutoconMain->DEV->GetUMUErrorMessageCount(1)) + "/" + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(1));
                if (AutoconMain->DEV->GetUMUCount() >= 3) DebugStateForm->m_ErrorsLabel3->Caption = "E: " + IntToStr(AutoconMain->DEV->GetUMUErrorMessageCount(2)) + "/" + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(2));
				//if (AutoconMain->DEV->GetUMUCount() >= 1) DebugStateForm->PanelUMU1->Caption = "  БУМ №1       " + IntToStr(AutoconMain->DEV->GetUMUErrorMessageCount(0)) + "/" + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(0));
				//if (AutoconMain->DEV->GetUMUCount() >= 2) DebugStateForm->PanelUMU2->Caption = "  БУМ №2       " + IntToStr(AutoconMain->DEV->GetUMUErrorMessageCount(1)) + "/" + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(1));
				//if (AutoconMain->DEV->GetUMUCount() >= 3) DebugStateForm->PanelUMU3->Caption = "  БУМ №3       " + IntToStr(AutoconMain->DEV->GetUMUErrorMessageCount(2)) + "/" + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(2));

                DebugStateForm->m_DownloadSpeedLabel1->Caption = StringFormatU(L"D: %.2f KB/s", float(AutoconMain->DEV->GetUMUDownloadSpeed(0)) / 1024.f);
                DebugStateForm->m_UploadSpeedLabel1->Caption = StringFormatU(L"U: %.2f KB/s", float(AutoconMain->DEV->GetUMUUploadSpeed(0)) / 1024.f);

                DebugStateForm->m_DownloadSpeedLabel2->Caption = StringFormatU(L"D: %.2f KB/s", float(AutoconMain->DEV->GetUMUDownloadSpeed(1)) / 1024.f);
                DebugStateForm->m_UploadSpeedLabel2->Caption = StringFormatU(L"U: %.2f KB/s", float(AutoconMain->DEV->GetUMUUploadSpeed(1)) / 1024.f);

                DebugStateForm->m_DownloadSpeedLabel3->Caption = StringFormatU(L"D: %.2f KB/s", float(AutoconMain->DEV->GetUMUDownloadSpeed(2)) / 1024.f);
                DebugStateForm->m_UploadSpeedLabel3->Caption = StringFormatU(L"U: %.2f KB/s", float(AutoconMain->DEV->GetUMUUploadSpeed(2)) / 1024.f);

                DebugStateForm->Refresh();
            }
            #ifndef NDEBUG
			if (AutoconMain->DEV->GetUMUSkipMessageCount(i) != 0) Text = "Ошибка сети: " + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(i));
            #endif
			Memo1->Lines->Add(Format("БУМ %d - %s", ARRAYOFCONST((i + 1, Text))));
		}
	}
	else
	{
		Memo1->Lines->Add("БУМ 1 - нет соединения");
		Memo1->Lines->Add("БУМ 2 - нет соединения");
		Memo1->Lines->Add("БУМ 3 - нет соединения");
	}


	if ((AutoconMain) && (AutoconMain->ac) && (AutoconMain->ac->ctrl))
		TestButton->Enabled = (UMUOKCount ==

								#ifndef FILTER_UMU_COUNT_TO_ONE
								3
								#endif
								#ifdef FILTER_UMU_COUNT_TO_ONE
								1
								#endif
							  )
							  #ifndef NO_CONTROLER_AND_MOTORS
							  &&
							  cavtk_test_
							  #endif
							  && (AutoconMain->ac->ctrl->getmode() != - 1);

 	#ifdef SIMULATION_CONTROLER_AND_MOTORS
 	TestButton->Enabled = true;
 	#endif

    SearchButton->Enabled = TestButton->Enabled;
    AlignButton->Enabled = TestButton->Enabled;
    CalibButton->Enabled = TestButton->Enabled;
    Button5->Enabled = TestButton->Enabled;
    Button6->Enabled = TestButton->Enabled;
    Button8->Enabled = TestButton->Enabled;
    //ArchButton->Enabled = TestButton->Enabled;
}

//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::OpenConnectionButtonClick(TObject *Sender)
{
	Timer1->Enabled = true;
    OpenConnectionButton->Enabled = false;

    Timer1Timer(NULL);
	if (!AutoconMain->OpenConnection(IPComboBox->ItemIndex))
	{
		OpenConnectionTimer->Interval = 7500;
		OpenConnectionTimer->Enabled = true;
        AutoconMain->Release();
        AutoconMain->Create();
		return;
	}
    MainUnit->Initialize();
    AdjustingForm->Initialize();

    UpdateInfo();
}

AnsiString __fastcall GetWVersion()
{
  AnsiString temp;
  VS_FIXEDFILEINFO VerValue;
  DWORD size, n;
  n = GetFileVersionInfoSize(Application->ExeName.c_str(), &size);
  if (n > 0)
  {
	char *pMyBuf = new char[n+1];
	GetFileVersionInfo(Application->ExeName.c_str(), 0, n, pMyBuf);
    LPVOID pValue;
    UINT Len;
	if (VerQueryValueA(pMyBuf, "\\", &pValue, &Len))
    {
      memmove(&VerValue, pValue, Len);
      temp = IntToStr((__int64)VerValue.dwFileVersionMS >> 16);
      temp += "." + IntToStr((__int64)VerValue.dwFileVersionMS & 0xFFFF);
      temp += "." + IntToStr((__int64)VerValue.dwFileVersionLS >> 16);
      temp += "." + IntToStr((__int64)VerValue.dwFileVersionLS & 0xFF);
    }
	delete [] pMyBuf;
  }
  return temp;
}

void TMainMenuForm::UpdateInfo()
{
    const int daysSinceLastTestLimit = 7;
    int daysSinceLastTest = DaysBetween(ACConfig->Last_SSC_TestDateTime, Now());
    m_TestSOPLabel->Caption = daysSinceLastTest ? IntToStr(daysSinceLastTest) + " дн. назад" : UnicodeString("Сегодня");
    m_TestSOPLabel->Font->Color = (daysSinceLastTest >= daysSinceLastTestLimit) ? clRed : RGB(43,135,29);
    daysSinceLastTest = DaysBetween(ACConfig->Last_SS3R_TestDateTime, Now());
    m_TestSO3PLabel->Caption = daysSinceLastTest ? IntToStr(daysSinceLastTest) + " дн. назад" : UnicodeString("Сегодня");
    m_TestSO3PLabel->Font->Color = (daysSinceLastTest >= daysSinceLastTestLimit) ? clRed : RGB(43,135,29);


    m_JointsCheckedLabel->Caption = IntToStr(ACConfig->Last_JointNumber) + " ст.";

    unsigned int CurrWorkTime = ACConfig->Last_WorkTimeSec +
                                    SecondsBetween(ACConfig->Last_WorkStartTime, Now());
    m_WorkTimeLabel->Caption = IntToStr((int)CurrWorkTime/60/60) + " ч. " +
                                IntToStr((int)CurrWorkTime/60%60) + " м.";

    m_SoftwareVersionLabel->Caption = GetWVersion();

    TLabel* BumLabels[3][2] = {{m_BUM1StatusLabel,m_BUM1NameLabel},
                                {m_BUM2StatusLabel,m_BUM2NameLabel},
                                {m_BUM3StatusLabel,m_BUM3NameLabel}};
    for(unsigned int i = 0; i < 3; i++)
    {
        if(i < AutoconMain->DEV->UnitsData.size())
        {
            if(AutoconMain->DEV->UnitsData[i].FirmwareVer)
                BumLabels[i][0]->Caption = AutoconMain->DEV->UnitsData[i].FirmwareVer;
            else
            {
                #ifndef SIMULATION
                    BumLabels[i][0]->Caption = "Обновление...";
                #else
                    BumLabels[i][0]->Caption = "Симуляция...";
                #endif
            }

            if(AutoconMain->DEV->UnitsData[i].WorksNumber)
                BumLabels[i][1]->Caption = UnicodeString("БУМ №")+
                                        AutoconMain->DEV->UnitsData[i].WorksNumber +
                                        UnicodeString(" ");
            else
                BumLabels[i][1]->Caption = UnicodeString("БУМ №") + IntToStr((int)i+1) + " ";
        }
        else
        {
            BumLabels[i][0]->Caption = "";
            BumLabels[i][1]->Caption = "";
        }
    }
}

//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::Button8Click(TObject *Sender)
{
    //ConfigForm->Visible = true;
    //ConfigForm->BringToFront();
    //CalibrationForm->Visible = true;
    CalibrationForm->ShowModal();
}

//---------------------------------------------------------------------------
// ----- Тест ---------------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::TestButtonClick(TObject *Sender)
{
    if(!AutoconMain)
        return;

    TEnterParamFrame* pFrame = MainUnit->pushFrame<TEnterParamFrame>(MF_EPF_TEST_FLAGS);
    pFrame->SetNextFrameFlags(MF_BSCAN_TEST_FLAGS);
    pFrame->SetNextState(acTest);

    AutoconMain->SetFlags(AM_TEST_FLAGS);

    AutoconMain->Rep->ClearData();
    AutoconMain->SetReportFileType(2);

    MainUnit->Visible = true;
}

//---------------------------------------------------------------------------
// ----- Поиск --------------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::SearchButtonClick(TObject *Sender)
{
    if(!AutoconMain)
        return;

    TEnterParamFrame* pFrame = MainUnit->pushFrame<TEnterParamFrame>(MF_EPF_SEARCH_FLAGS);
    pFrame->SetNextFrameFlags(MF_BSCAN_SEARCH_FLAGS);
    pFrame->SetNextState(acSearch);

    AutoconMain->SetFlags(AM_SEARCH_FLAGS);

	ACConfig->Last_Сonclusion = "";
    AutoconMain->Rep->ClearData();
    AutoconMain->SetReportFileType(1);

    MainUnit->Visible = true;
}

void __fastcall TMainMenuForm::SearchButtonClick2(TObject *Sender)
{
	if ((AutoconMain) && (AutoconMain->ac))
	{
		AutoconMain->ac->SetMode(acSearch);
		ChangeModeFlag = true;
	}
}


//---------------------------------------------------------------------------
// ----- Юстировка ----------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::AlignButtonClick(TObject *Sender)
{
	if ((AutoconMain) && (AutoconMain->ac))
	{
        //MainForm->ViewMode = false;
        AutoconMain->Rep->ClearData();
        AutoconMain->SetReportFileType(3); // Тип данных: 1 - контроль; 2 - тест; 3 - юстировка; 4 - контроль только ручик;
        AdjustingForm->Clear_Data();
        AdjustingForm->Visible = true;
        AutoconMain->ac->SetMode(acAdjusting1);
        //AutoconMain->ac->SetMode(acSearch);
        ChangeModeFlag = true;
	}
}

//---------------------------------------------------------------------------
// ----- Настройка ----------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::CalibButtonClick(TObject *Sender)
{
    if(!AutoconMain)
        return;

	if ((AutoconMain) && (AutoconMain->ac))
	{
        AutoconMain->SetFlags(AM_TUNE_FLAGS);
        AutoconMain->XSysCoord_Center = ScanLen / 2;


		sScanChannelDescription scanChannelDesc;
		sChannelDescription channelDesc;
        AutoconMain->Rep->ClearData();
		for(unsigned int i=0;i<AutoconMain->Config->GetAllScanChannelsCount();i++)
		{
			if(!AutoconMain->Config->getSChannelbyIdx(i, &scanChannelDesc))
				continue;

			//if(!AutoconMain->Config->getSChannelbyID(sensTuneParam.id, &scanChannelDesc))//для получения
			//	continue;                                                      //DeviceSide (? зачем столько структур)
			if(!AutoconMain->Table->ItemByCID(scanChannelDesc.Id, &channelDesc))//для получения
				continue;                                      		//рекомендованной чувствительности

			int defSens=channelDesc.RecommendedSens[0];//Не понятно, почему там два значения [0][1]?
			int defGain = 45;//45;
			//int defPrismDelay = 100;
			int defPrismDelay = 1000;
            UNUSED(defPrismDelay);
            UNUSED(defGain);
            UNUSED(defSens);
//			AutoconMain->Calibration->SetSens(scanChannelDesc.DeviceSide,
//												scanChannelDesc.Id,
//												0,
//												defSens);
//
//			AutoconMain->Calibration->SetGain(scanChannelDesc.DeviceSide,
//												scanChannelDesc.Id,
//												0,
//												defGain);
//			AutoconMain->Calibration->SetPrismDelay(scanChannelDesc.DeviceSide,
//													scanChannelDesc.Id,
//													defPrismDelay);
			//AutoconMain->Calibration->SetStGate(scanChannelDesc.DeviceSide,
			//										scanChannelDesc.Id,0,0);
			//AutoconMain->Calibration->SetEdGate(scanChannelDesc.DeviceSide,
			//										scanChannelDesc.Id,0,45);
		}
		AutoconMain->DEV->Update(false);

		AutoconMain->ac->SetMode(acTuning);
#ifndef DISABLE_AUTO_CALIBRATION
        AutoconMain->ac->TuneSetState(tmAutInit);
#endif
		ChangeModeFlag = true;
	}
}
//---------------------------------------------------------------------------


void __fastcall TMainMenuForm::OpenConnectionTimerTimer(TObject *Sender)
{
	OpenConnectionTimer->Enabled = false;
	OpenConnectionButtonClick(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::ChangeModeTimerTimer(TObject *Sender)
{

	if (ChangeModeFlag && (AutoconMain) && (AutoconMain->ac))
	{
		//MainForm->setVisualMode(BS_VISUAL_UNKNOWN);
		switch (AutoconMain->ac->GetMode())
		{
			   case acNotSet     : { /* Label1->Caption = "State: NotSet   "; */ break; }
			   case acError      : { /* Label1->Caption = "State: Error    "; */ break; }
			   case acWait       : { /* Label1->Caption = "State: Wait     "; */ break; }
			   case acReady      : {
									  MainMenuForm->Visible = true;
									  break;
								   }
			   case acSearch     : {
									  /*//MainForm->HandScanFlag = false;
									  MainForm->Visible = true;
									  // MainForm->BringToFront();
									  MainForm->setVisualMode(BS_VISUAL_SEARCH);
									  MainForm->PageControl->ActivePageIndex = 13;
                                      MainForm->PageControlChange(NULL);
									  MainForm->PageControl->Update();
                                      MainForm->Visible = true;  */
                                      MainUnit->Visible = true;
                                      MainUnit->pushFrame<TBScanFrame>(MF_BSCAN_SEARCH_FLAGS);
									  break;
								   }
			   case acTest       : {
									  /*//MainForm->SendToBack();
									  MainForm->Visible = true;
									  //MainForm->BringToFront();
									  MainForm->setVisualMode(BS_VISUAL_SEARCH);
									  MainForm->PageControl->ActivePageIndex = 13;
                                      MainForm->PageControlChange(NULL);
  									  MainForm->PageControl->Update();
                                      MainForm->Visible = true; */
                                      MainUnit->Visible = true;

									  break;
								   }

			   //Added by KirillB
			   case acTuning     : {
										/*MainForm->Visible = true;
										MainForm->setVisualMode(BS_VISUAL_TUNE);
										MainForm->PageControl->ActivePageIndex = 13;
										MainForm->PageControl->Update();
                                        MainForm->Visible = true;*/
                                        MainUnit->Visible = true;
#ifndef DISABLE_AUTO_CALIBRATION
                                        MainUnit->pushFrame<TBScanFrame>(MF_BSCAN_TUNE_FLAGS);
#else
                                        MainUnit->pushFrame<TManualTuneFrame>(0);
#endif
										break;
									}
			   case acAdjusting1 : {
                                        AdjustingForm->Visible = true;
                                        break;
                                    }
			   case acAdjusting2 : {  break; }
		}

        //Added by KirillB
        //Если AutoconMain->ac->GetMode() == acWait, то код выше выполнится до
        //  перехода из состояния acWait в следующее состояние (напр. acAdjusting1)
        //  поэтому не сбрасываем ChangeModeFlag во время переходных процессов
        if(AutoconMain->ac->GetMode() != acWait)
    		ChangeModeFlag = false;

	}
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::Button1Click(TObject *Sender)
{
	//	MainForm->GetMessages(TMessagesType::mtError, " TEST1 !!! ", GetTickCount());
	//	MainForm->GetMessages(TMessagesType::mtInfo, " TEST2 !!! ", GetTickCount());
	//	MainForm->GetMessages(TMessagesType::mtError, " TEST3 !!! ", GetTickCount());
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::FormShow(TObject *Sender)
{
/*
	#ifndef NDEBUG
	DebugMenuPanel->Visible = true;
    //LogForm->Visible = true;
    DebugStateForm->Visible = true;
	#endif

 	#ifdef NDEBUG */
 	DebugMenuPanel->Visible = false;
    LogForm->Visible = false;
    DebugStateForm->Visible = false;
//	#endif

    if (Tag == 0)
    {
        Tag = 1;
        ACConfig = new cACConfig;
        ACConfig->LoadData();
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::FormDestroy(TObject *Sender)
{
	ACConfig->SaveData();
	delete ACConfig;
   //	TextLog->SaveToFile("log.txt");
	delete TextLog;
}
//---------------------------------------------------------------------------


void __fastcall TMainMenuForm::Button5Click(TObject *Sender)
{
    if(!AutoconMain)
        return;

    TEnterParamFrame* pFrame = MainUnit->pushFrame<TEnterParamFrame>(MF_EPF_HANDSCAN_FLAGS);
    pFrame->SetNextFrameFlags(MF_ASCAN_HAND_FLAGS);
    pFrame->SetNextState(acHandScan);

    AutoconMain->SetFlags(AM_HSCAN_FLAGS);

    AutoconMain->Rep->ClearData();
    AutoconMain->DEV->SetMode(dmSearch);
    AutoconMain->SetReportFileType(4);

    MainUnit->Visible = true;
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::Button6Click(TObject *Sender)
{
    AutoconMain->Rep->ClearData();
    AutoconMain->SetFlags(AM_HSCAN_TUNE_FLAGS);
    AutoconMain->DEV->InitTuningGateList();
    AutoconMain->DEV->SetMode(dmCalibration);
    AutoconMain->DEV->Update(false);

    AutoconMain->SkipRep = true;

    MainUnit->Visible = true;
    MainUnit->pushFrame<TAScanFrame>(MF_ASCAN_HAND_TUNE_FLAGS);
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::OnManualModeBtnClicked(TObject *Sender)
{
	//MainForm->Visible = true;
	//MainForm->setVisualMode(MF_VISUAL_TUNE);
//	MainForm->PageControl->ActivePageIndex = 15;
//	MainForm->PageControl->Update();
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::ArchButtonClick(TObject *Sender)
{
    int selMode = ArchiveForm->ShowSelectModeDial();
    if(selMode != -1)
    {
        ArchiveForm->SetProtocolMode(selMode);
    	ArchiveForm->Show();
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::Button2Click(TObject *Sender)
{
    if (!Timer1->Enabled)
        OpenConnectionButtonClick(Sender);
    if (OpenDialog->Execute())
    {
        //MainForm->Clear_Data();
        AutoconMain->Rep->ReadFromFile(OpenDialog->FileName, false);
        HeaderTestForm->Memo->Lines->Add("FileType - " + IntToStr(AutoconMain->Rep->Header.FileType));
        try
        {
            HeaderTestForm->Memo->Lines->Add("Дата / Время контроля - " + DateTimeToStr(AutoconMain->Rep->Header.DateTime));
        }
        catch (...)
        {
            HeaderTestForm->Memo->Lines->Add("Дата / Время контроля - ОШИБКА");
        }
        HeaderTestForm->Memo->Lines->Add("Номер смены - " + IntToStr(AutoconMain->Rep->Header.GangNumber));
        HeaderTestForm->Memo->Lines->Add("Номер протокола контроля - " + IntToStr(AutoconMain->Rep->Header.ReportNumber));
        HeaderTestForm->Memo->Lines->Add("Номер плети - " + IntToStr(AutoconMain->Rep->Header.PletNumber));
        HeaderTestForm->Memo->Lines->Add("Номер стыка - " + IntToStr(AutoconMain->Rep->Header.JointNumber));
        HeaderTestForm->Memo->Lines->Add("Прямолинейность сверху [мм] - " + FloatToStr(AutoconMain->Rep->Header.TopStraightness_));
        HeaderTestForm->Memo->Lines->Add("Прямолинейность сбоку (РГ) [мм] - " + FloatToStr(AutoconMain->Rep->Header.WF_Straightness_));
        HeaderTestForm->Memo->Lines->Add("Прямолинейность сбоку (НРГ) [мм] - " + FloatToStr(AutoconMain->Rep->Header.WNF_Straightness_));

        HeaderTestForm->Memo->Lines->Add("Оператор ФИО - " + AutoconMain->Rep->Header.Operator);
        HeaderTestForm->Memo->Lines->Add("Код дефекта - " + AutoconMain->Rep->Header.DefectCode);
        HeaderTestForm->Memo->Lines->Add("Гр. Годности - " + AutoconMain->Rep->Header.ValidityGroup);
        HeaderTestForm->Memo->Lines->Add("Заключение - " + AutoconMain->Rep->Header.Сonclusion);
        HeaderTestForm->Memo->Lines->Add("Измерение твёрдости - " + IntToStr(AutoconMain->Rep->Header.HardnessState));
        if ((bool)AutoconMain->Rep->Header.HardnessState)
        {
            HeaderTestForm->Memo->Lines->Add("Твёрдость (центр, точка 1) - " + IntToStr(AutoconMain->Rep->Header.Hardness[0]));
            HeaderTestForm->Memo->Lines->Add("Твёрдость (центр, точка 2) - " + IntToStr(AutoconMain->Rep->Header.Hardness[1]));
            HeaderTestForm->Memo->Lines->Add("Твёрдость (центр, точка 3) - " + IntToStr(AutoconMain->Rep->Header.Hardness[2]));
            HeaderTestForm->Memo->Lines->Add("Твёрдость (слева, точка 1) - " + IntToStr(AutoconMain->Rep->Header.Hardness[3]));
            HeaderTestForm->Memo->Lines->Add("Твёрдость (слева, точка 1) - " + IntToStr(AutoconMain->Rep->Header.Hardness[4]));
            HeaderTestForm->Memo->Lines->Add("Твёрдость (слева, точка 1) - " + IntToStr(AutoconMain->Rep->Header.Hardness[5]));
            HeaderTestForm->Memo->Lines->Add("Твёрдость (справа, точка 1) - " + IntToStr(AutoconMain->Rep->Header.Hardness[6]));
            HeaderTestForm->Memo->Lines->Add("Твёрдость (справа, точка 2) - " + IntToStr(AutoconMain->Rep->Header.Hardness[7]));
            HeaderTestForm->Memo->Lines->Add("Твёрдость (справа, точка 3) - " + IntToStr(AutoconMain->Rep->Header.Hardness[8]));
        }

        HeaderTestForm->Memo->Lines->Add("Время в канале: 0° ЗТМ - " + FloatToStr(AutoconMain->Rep->GetTimeInHandScanChannel(0x1E)));
        HeaderTestForm->Memo->Lines->Add("Время в канале: 0° ЭХО - " + FloatToStr(AutoconMain->Rep->GetTimeInHandScanChannel(0x1D)));
        HeaderTestForm->Memo->Lines->Add("Время в канале: H45 - " + FloatToStr(AutoconMain->Rep->GetTimeInHandScanChannel(0x1F)));
        HeaderTestForm->Memo->Lines->Add("Время в канале: H50 - " + FloatToStr(AutoconMain->Rep->GetTimeInHandScanChannel(0x20)));
        HeaderTestForm->Memo->Lines->Add("Время в канале: H58 - " + FloatToStr(AutoconMain->Rep->GetTimeInHandScanChannel(0x21)));
        HeaderTestForm->Memo->Lines->Add("Время в канале: H65 - " + FloatToStr(AutoconMain->Rep->GetTimeInHandScanChannel(0x22)));
        HeaderTestForm->Memo->Lines->Add("Время в канале: H70 - " + FloatToStr(AutoconMain->Rep->GetTimeInHandScanChannel(0x23)));


        if (AutoconMain->Rep->Photo) HeaderTestForm->Image1->Picture->Assign(AutoconMain->Rep->Photo);
        if (AutoconMain->Rep->PhoneScreenShot) HeaderTestForm->Image2->Picture->Assign(AutoconMain->Rep->PhoneScreenShot);
        if (AutoconMain->Rep->ScreenShotHandScanList.size() > 0)
            HeaderTestForm->Image3->Picture->Assign(AutoconMain->Rep->ScreenShotHandScanList[0]);
        if (AutoconMain->Rep->ScreenShotHandScanList.size() > 1)
            HeaderTestForm->Image4->Picture->Assign(AutoconMain->Rep->ScreenShotHandScanList[1]);
        HeaderTestForm->ShowModal();


        MainUnit->Visible = true;
        MainUnit->pushFrame<TBScanFrame>(MF_BSCAN_SEARCH_FLAGS);

//	    MainForm->Visible = true;
//        MainForm->PageControl->ActivePageIndex = 13;
//        MainForm->PageControl->Update();
//        MainForm->PageControlChange(NULL);
//        MainForm->Visible = true;
    }
}
//---------------------------------------------------------------------------


void __fastcall TMainMenuForm::ShowHideDebugMenusClick(TObject *Sender)
{
    if(bDebugMenusAreHidden)
    {
        LogForm->Visible = true;
        DebugStateForm->Visible = true;
        ShowHideDebugMenus->Caption = "Скрыть меню";
    }
    else
    {
        LogForm->Visible = false;
        DebugStateForm->Visible = false;
        ShowHideDebugMenus->Caption = "Показать меню";
    }

    bDebugMenusAreHidden = !bDebugMenusAreHidden;
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::TuneTableBtnClick(TObject *Sender)
{
    TuneTableForm->Visible = true;
}
//---------------------------------------------------------------------------


void __fastcall TMainMenuForm::OnButtonClick(TObject *Sender)
{
    OnButton->Enabled = false;
    IPComboBox->ItemIndex = 4;
    OpenConnectionTimer->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::Panel5Click(TObject *Sender) // Вкл / Выкл отображения параметров фильтра
{
    if (Panel5->Tag == 0)
    {
        Panel5->Tag = GetTickCount();
        Panel6->Tag = 0;
    }
    else
    {
        if (GetTickCount() - Panel5->Tag < 1500)
        {
            Panel5->Tag = GetTickCount();
            Panel6->Tag++;
        }
        if (Panel6->Tag == 3)
        {
            Panel5->Tag = 0;
            if (Panel4->Tag == 0) Panel4->Tag = 1; else Panel4->Tag = 0;


            if (Panel4->Tag == 0)
            {
                Panel5->Caption = "АВТОКОН-С";
                ACConfig->ShowFiltrParam_ = false;
            }
            else
            {
                Panel5->Caption = "АВТОКОН~С";
                ACConfig->ShowFiltrParam_ = true;
            }
        }
    }
}
//---------------------------------------------------------------------------

