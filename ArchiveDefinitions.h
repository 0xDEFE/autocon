﻿/**
 * @file ArchiveDefinitions.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef ArchiveDefinitionsH
#define ArchiveDefinitionsH

#include "AutoconMain.h"
#include <vector>
#include <list>

/** \enum eADBProtocolType
    \brief Тип отображаемых протоколов
    № соответствует типу файла в sJointTestingReportHeader (исключая ADB_PROTO_ALL)
 */
enum eADBProtocolType
{
	ADB_PROTO_ALL  = 0,         //!< Все типы протоколов
	ADB_PROTO_SCAN = 1,         //!< Протокол Поиск
	ADB_PROTO_TEST = 2,         //!< Протокол Тест
    ADB_PROTO_ADJUSTING = 3,    //!< Протокол Юстировка
    ADB_PROTO_HANDSCAN = 4,     //!< Протокол Ручной
	ADB_PROTO_END               //!< Кол-во элементов
};

/** \enum eADBFieldType
    \brief Тип поля в базе данных (для преобразования в имя, выбора отдельных полей в настройке таблицы)
    \note !! Обновить поле и функции ADBFieldTypeToName, ADBFieldTypeToDBFieldName при изменении полей в БД
 */
enum eADBFieldType
{
    ADB_FIELDTYPE_UNKNOWN = -1,         //!< Неизвестное поле
	ADB_FIELDTYPE_ID = 0,               //!< Идентификатор записи в БД
	ADB_FIELDTYPE_FILENAME,             //!< Имя файла отчета
	ADB_FIELDTYPE_FILETYPE_IDX,         //!< Индекс типа файла (см. #eADBProtocolType)
	ADB_FIELDTYPE_FILETYPE_NAME,        //!< Имя типа файла  (см. #eADBProtocolType)
	ADB_FIELDTYPE_DATETIME,             //!< Дата создания
	ADB_FIELDTYPE_GANG_NUMBER,          //!< Номер смены
	ADB_FIELDTYPE_REPORT_NUMBER,        //!< Номер отчета
	ADB_FIELDTYPE_PLET_NUMBER,          //!< Номер плети
	ADB_FIELDTYPE_JOINT_NUMBER,         //!< Номер стыка
	ADB_FIELDTYPE_TOP_STRAIGHTNESS,     //!< Прямолинейность сверху
	ADB_FIELDTYPE_SIDE_STRAIGHTNESS,    //!< Прямолинейность сбоку
	ADB_FIELDTYPE_HARDNESS,             //!< Твердость
	ADB_FIELDTYPE_HARDNESS_UNIT,        //!< Ед. изм. Твёрдости: 1 – HB, 2 - HV, 3 - HRC
	ADB_FIELDTYPE_CENTR_SYS_COORD,      //!< Центр, Положение стыка
	ADB_FIELDTYPE_MAX_SYS_COORD,        //!< Максимальная координата
	ADB_FIELDTYPE_MAX_CHANNEL,          //!< Количество каналов сканирования
	ADB_FIELDTYPE_PLAINT,               //!< Предприятие
	ADB_FIELDTYPE_PLANT_NUMBER,         //!< Заводской номер установки
	ADB_FIELDTYPE_VER_SOFT,             //!< Версия ПО
	ADB_FIELDTYPE_OPERATOR,             //!< Оператор ФИО
	ADB_FIELDTYPE_DEFECT_CODE,          //!< Код дефекта
	ADB_FIELDTYPE_VALIDITY_GROUP,       //!< Гр. Годности
	ADB_FIELDTYPE_CONCLUSION,           //!< Заключение
    ADB_FIELDTYPE_HAS_BSCAN,            //!< Флаг присутствия В-развертки
	ADB_FIELDTYPE_HAS_HSCAN_IMAGES,     //!< Флаг присутствия изображений А-развертки с ручников
	ADB_FIELDTYPE_HAS_HSCAN_BSCAN,      //!< Флаг присутствия изображений В-развертки с ручников
	ADB_FIELDTYPE_HAS_LINEARITY,        //!< Флаг присутствия изображений линейности
	ADB_FIELDTYPE_HAS_CAMERA_IMAGES,    //!< Флаг присутствия фото с камеры
	ADB_FIELDTYPE_END                   //!< Кол-во полей
};

//! Преобразование в имя поля (для оператора)
UnicodeString ADBFieldTypeToName(eADBFieldType fieldType);
//! Преобразование в имя поля (для БД)
UnicodeString ADBFieldTypeToDBFieldName(eADBFieldType fieldType);
//! Преобразование в значение БД (автоподстановка кавычек для строковых значений, формат даты/времени и т.п.)
UnicodeString ADBFieldValTypeToDBFieldVal(eADBFieldType fieldType, UnicodeString& val);

//Назначение поля
#define ADB_FIELDASSIGN_TEST 	0x01
#define ADB_FIELDASSIGN_CONTROL	0x02
#define ADB_FIELDASSIGN_TABLE 	0x04
#define ADB_FIELDASSIGN_SORT 	0x08
#define ADB_FIELDASSIGN_SEARCH 	0x10
//! Возвращает методы, в которых используется поле в виде маски
DWORD GetFieldAssigns(eADBFieldType fieldType);

/** \enum eAFDOpType
    \brief Диалог поиска/фильтрации - тип операции сравнения
 */
enum eAFDOpType
{
	AFD_OPTYPE_EQUAL = 0,
	AFD_OPTYPE_NOTEQUAL,
	AFD_OPTYPE_LESS,
	AFD_OPTYPE_GREAT,
	AFD_OPTYPE_LESS_EQUAL,
	AFD_OPTYPE_GREAT_EQUAL,
	AFD_OPTYPE_END
};

/** \struct sAFDFieldValues
    \brief Диалог поиска/фильтрации - уникальные значения поля (для автоподстановки)
 */
struct sAFDFieldValues
{
	eADBFieldType fieldType;
	std::vector<UnicodeString> values;
};

/** \struct sAFDFindQuery
    \brief Диалог поиска/фильтрации - конечный результат - структура, содержащая данные для запроса в БД
 */
struct sAFDFindQuery
{
	eADBFieldType fieldType;
	UnicodeString fieldTypeStr;
	eAFDOpType opType;
	UnicodeString opTypeStr;
	UnicodeString findValue;
};

//! Диалог поиска/фильтрации - преобразование в операцию сравнения БД
UnicodeString AFDOpTypeToDBOp(eAFDOpType afdOp);
//! Диалог поиска/фильтрации - преобразование в операцию сравнения БД
UnicodeString AFDOpTypeToName(eAFDOpType afdOp);


UnicodeString ADBFileTypeIdxToFileTypeName(int idx);
int ADBFileTypeNameToFileTypeIdx(UnicodeString name);

#include "ArchivePDFCreator.h"
#include "ArchiveUnit.h"
#include "ArchiveSettingsDialUnit.h"
#include "ArchiveSelectDialUnit.h"


#endif
