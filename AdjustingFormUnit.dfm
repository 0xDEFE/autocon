object AdjustingForm: TAdjustingForm
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'AdjustingForm'
  ClientHeight = 710
  ClientWidth = 952
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 417
    Top = 0
    Width = 9
    Height = 710
    ExplicitLeft = 425
  end
  object m_ChartGrid: TGridPanel
    Left = 426
    Top = 0
    Width = 526
    Height = 710
    Align = alClient
    ColumnCollection = <
      item
        Value = 33.333551447674420000
      end
      item
        Value = 33.333551447674420000
      end
      item
        Value = 33.332897104651160000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = m_KP2Panel
        Row = 0
      end
      item
        Column = 1
        Control = m_RailDrawPanel
        Row = 1
      end
      item
        Column = 1
        Control = Panel2
        Row = 0
      end
      item
        Column = 2
        Control = Panel5
        Row = 0
      end
      item
        Column = 0
        Control = Panel6
        Row = 1
      end
      item
        Column = 2
        Control = Panel7
        Row = 1
      end
      item
        Column = 0
        Control = Panel8
        Row = 2
      end
      item
        Column = 1
        Control = Panel9
        Row = 2
      end
      item
        Column = 2
        Control = Panel10
        Row = 2
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    RowCollection = <
      item
        Value = 33.333525960025630000
      end
      item
        Value = 33.333525960025630000
      end
      item
        Value = 33.332948079948740000
      end
      item
        SizeStyle = ssAuto
      end>
    TabOrder = 0
    object m_KP2Panel: TPanel
      Left = 1
      Top = 1
      Width = 174
      Height = 236
      Align = alClient
      Caption = 'KP2'
      TabOrder = 0
      object m_GridPanel1: TGridPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 24
        Align = alTop
        ColumnCollection = <
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.332667888641580000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_NLabel_kp2
            Row = 0
          end
          item
            Column = 1
            Control = m_HLabel_kp2
            Row = 0
          end
          item
            Column = 2
            Control = m_KuLabel_kp2
            Row = 0
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 0
        object m_NLabel_kp2: TLabel
          Left = 1
          Top = 1
          Width = 56
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_HLabel_kp2: TLabel
          Left = 57
          Top = 1
          Width = 56
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_KuLabel_kp2: TLabel
          Left = 113
          Top = 1
          Width = 58
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
      end
      object m_AscanChartKP2: TChart
        Left = 1
        Top = 25
        Width = 172
        Height = 210
        AllowPanning = pmNone
        BackWall.Brush.BackColor = clDefault
        BackWall.Color = clDefault
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Maximum = 24.000000000000000000
        BottomAxis.Title.Caption = #1084#1082#1089
        BottomAxis.Title.Brush.Color = clBlack
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsSize = 10
        LeftAxis.Maximum = 18.000000000000000000
        LeftAxis.Minimum = -12.000000000000000000
        LeftAxis.Title.Angle = 0
        LeftAxis.Title.Caption = #1076#1041
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Allow = False
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        OnClick = OnChartClicked
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object AScanSeries2: TLineSeries
          Marks.Visible = False
          SeriesColor = 394758
          Title = 'AScanSeries'
          Brush.BackColor = clDefault
          LinePen.Width = 2
          Pointer.Brush.Gradient.EndColor = 394758
          Pointer.Gradient.EndColor = 394758
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
    object m_RailDrawPanel: TPanel
      Left = 175
      Top = 237
      Width = 174
      Height = 236
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      OnResize = m_RailDrawPanelResize
      object m_RailDrawPBox: TPaintBox
        Left = 0
        Top = 0
        Width = 174
        Height = 236
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnPaint = m_RailDrawPBoxPaint
        ExplicitLeft = 5
        ExplicitTop = -5
        ExplicitWidth = 183
      end
    end
    object Panel2: TPanel
      Left = 175
      Top = 1
      Width = 174
      Height = 236
      Align = alClient
      Caption = 'KP2'
      TabOrder = 2
      object m_GridPanel: TGridPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 24
        Align = alTop
        ColumnCollection = <
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.332667888641580000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_NLabel_kp1
            Row = 0
          end
          item
            Column = 1
            Control = m_HLabel_kp1
            Row = 0
          end
          item
            Column = 2
            Control = m_KuLabel_kp1
            Row = 0
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 0
        object m_NLabel_kp1: TLabel
          Left = 1
          Top = 1
          Width = 56
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_HLabel_kp1: TLabel
          Left = 57
          Top = 1
          Width = 56
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_KuLabel_kp1: TLabel
          Left = 113
          Top = 1
          Width = 58
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
      end
      object m_AscanChartKP1: TChart
        Left = 1
        Top = 25
        Width = 172
        Height = 210
        AllowPanning = pmNone
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Maximum = 24.000000000000000000
        BottomAxis.Ticks.SmallSpace = 1
        BottomAxis.Title.Caption = #1084#1082#1089
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsSize = 10
        LeftAxis.Maximum = 18.000000000000000000
        LeftAxis.Minimum = -12.000000000000000000
        LeftAxis.Title.Angle = 0
        LeftAxis.Title.Caption = #1076#1041
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Allow = False
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        OnClick = OnChartClicked
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object Series1: TLineSeries
          Marks.Visible = False
          SeriesColor = 394758
          Title = 'AScanSeries1'
          Brush.BackColor = clDefault
          LinePen.Width = 2
          Pointer.Brush.Gradient.EndColor = 394758
          Pointer.Gradient.EndColor = 394758
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
    object Panel5: TPanel
      Left = 349
      Top = 1
      Width = 176
      Height = 236
      Align = alClient
      Caption = 'KP2'
      TabOrder = 3
      object GridPanel4: TGridPanel
        Left = 1
        Top = 1
        Width = 174
        Height = 24
        Align = alTop
        ColumnCollection = <
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.332667888641580000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_NLabel_kp3
            Row = 0
          end
          item
            Column = 1
            Control = m_HLabel_kp3
            Row = 0
          end
          item
            Column = 2
            Control = m_KuLabel_kp3
            Row = 0
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 0
        object m_NLabel_kp3: TLabel
          Left = 1
          Top = 1
          Width = 57
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_HLabel_kp3: TLabel
          Left = 58
          Top = 1
          Width = 57
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_KuLabel_kp3: TLabel
          Left = 115
          Top = 1
          Width = 58
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
      end
      object m_AscanChartKP3: TChart
        Left = 1
        Top = 25
        Width = 174
        Height = 210
        AllowPanning = pmNone
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Maximum = 24.000000000000000000
        BottomAxis.Title.Caption = #1084#1082#1089
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsSize = 10
        LeftAxis.Maximum = 18.000000000000000000
        LeftAxis.Minimum = -12.000000000000000000
        LeftAxis.Title.Angle = 0
        LeftAxis.Title.Caption = #1076#1041
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Allow = False
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        OnClick = OnChartClicked
        DefaultCanvas = 'TGDIPlusCanvas'
        PrintMargins = (
          28
          15
          28
          15)
        ColorPaletteIndex = 13
        object LineSeries2: TLineSeries
          Marks.Visible = False
          SeriesColor = 394758
          Title = 'AScanSeries'
          Brush.BackColor = clDefault
          LinePen.Width = 2
          Pointer.Brush.Gradient.EndColor = 394758
          Pointer.Gradient.EndColor = 394758
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {
            00190000000000000000A881400000000000E882400000000000907F40000000
            000080814000000000005881400000000000407F400000000000487C40000000
            00003081400000000000108340000000000070824000000000006C8140000000
            0000008440000000000094814000000000005881400000000000B08340000000
            0000D88340000000000068804000000000001C81400000000000788440000000
            00007C85400000000000D0864000000000007087400000000000D88840000000
            0000208C400000000000088B40}
        end
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 237
      Width = 174
      Height = 236
      Align = alClient
      Caption = 'KP2'
      TabOrder = 4
      object GridPanel5: TGridPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 24
        Align = alTop
        ColumnCollection = <
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.332667888641580000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_NLabel_kp4
            Row = 0
          end
          item
            Column = 1
            Control = m_HLabel_kp4
            Row = 0
          end
          item
            Column = 2
            Control = m_KuLabel_kp4
            Row = 0
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 0
        object m_NLabel_kp4: TLabel
          Left = 1
          Top = 1
          Width = 56
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_HLabel_kp4: TLabel
          Left = 57
          Top = 1
          Width = 56
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_KuLabel_kp4: TLabel
          Left = 113
          Top = 1
          Width = 58
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
      end
      object m_AscanChartKP4: TChart
        Left = 1
        Top = 25
        Width = 172
        Height = 210
        AllowPanning = pmNone
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Maximum = 24.000000000000000000
        BottomAxis.Title.Caption = #1084#1082#1089
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsSize = 10
        LeftAxis.Maximum = 18.000000000000000000
        LeftAxis.Minimum = -12.000000000000000000
        LeftAxis.Title.Angle = 0
        LeftAxis.Title.Caption = #1076#1041
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Allow = False
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        OnClick = OnChartClicked
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object LineSeries3: TLineSeries
          Marks.Visible = False
          SeriesColor = 394758
          Title = 'AScanSeries'
          Brush.BackColor = clDefault
          LinePen.Width = 2
          Pointer.Brush.Gradient.EndColor = 394758
          Pointer.Gradient.EndColor = 394758
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
    object Panel7: TPanel
      Left = 349
      Top = 237
      Width = 176
      Height = 236
      Align = alClient
      Caption = 'KP2'
      TabOrder = 5
      object GridPanel6: TGridPanel
        Left = 1
        Top = 1
        Width = 174
        Height = 24
        Align = alTop
        ColumnCollection = <
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.332667888641580000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_NLabel_kp5
            Row = 0
          end
          item
            Column = 1
            Control = m_HLabel_kp5
            Row = 0
          end
          item
            Column = 2
            Control = m_KuLabel_kp5
            Row = 0
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 0
        object m_NLabel_kp5: TLabel
          Left = 1
          Top = 1
          Width = 57
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_HLabel_kp5: TLabel
          Left = 58
          Top = 1
          Width = 57
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_KuLabel_kp5: TLabel
          Left = 115
          Top = 1
          Width = 58
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
      end
      object m_AscanChartKP5: TChart
        Left = 1
        Top = 25
        Width = 174
        Height = 210
        AllowPanning = pmNone
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Maximum = 24.000000000000000000
        BottomAxis.Title.Caption = #1084#1082#1089
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsSize = 10
        LeftAxis.Maximum = 18.000000000000000000
        LeftAxis.Minimum = -12.000000000000000000
        LeftAxis.Title.Angle = 0
        LeftAxis.Title.Caption = #1076#1041
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Allow = False
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        OnClick = OnChartClicked
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object LineSeries4: TLineSeries
          Marks.Visible = False
          SeriesColor = 394758
          Title = 'AScanSeries'
          Brush.BackColor = clDefault
          LinePen.Width = 2
          Pointer.Brush.Gradient.EndColor = 394758
          Pointer.Gradient.EndColor = 394758
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
    object Panel8: TPanel
      Left = 1
      Top = 473
      Width = 174
      Height = 235
      Align = alClient
      Caption = 'KP2'
      TabOrder = 6
      object GridPanel7: TGridPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 24
        Align = alTop
        ColumnCollection = <
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.332667888641580000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_NLabel_kp6
            Row = 0
          end
          item
            Column = 1
            Control = m_HLabel_kp6
            Row = 0
          end
          item
            Column = 2
            Control = m_KuLabel_kp6
            Row = 0
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 0
        object m_NLabel_kp6: TLabel
          Left = 1
          Top = 1
          Width = 56
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_HLabel_kp6: TLabel
          Left = 57
          Top = 1
          Width = 56
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_KuLabel_kp6: TLabel
          Left = 113
          Top = 1
          Width = 58
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
      end
      object m_AscanChartKP6: TChart
        Left = 1
        Top = 25
        Width = 172
        Height = 209
        AllowPanning = pmNone
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Maximum = 24.000000000000000000
        BottomAxis.Title.Caption = #1084#1082#1089
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsSize = 10
        LeftAxis.Maximum = 18.000000000000000000
        LeftAxis.Minimum = -12.000000000000000000
        LeftAxis.Title.Angle = 0
        LeftAxis.Title.Caption = #1076#1041
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Allow = False
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        OnClick = OnChartClicked
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object LineSeries5: TLineSeries
          Marks.Visible = False
          SeriesColor = 394758
          Title = 'AScanSeries'
          Brush.BackColor = clDefault
          LinePen.Width = 2
          Pointer.Brush.Gradient.EndColor = 394758
          Pointer.Gradient.EndColor = 394758
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
    object Panel9: TPanel
      Left = 175
      Top = 473
      Width = 174
      Height = 235
      Align = alClient
      Caption = 'KP2'
      TabOrder = 7
      object GridPanel8: TGridPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 24
        Align = alTop
        ColumnCollection = <
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.332667888641580000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_NLabel_kp8
            Row = 0
          end
          item
            Column = 1
            Control = m_HLabel_kp8
            Row = 0
          end
          item
            Column = 2
            Control = m_KuLabel_kp8
            Row = 0
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 0
        object m_NLabel_kp8: TLabel
          Left = 1
          Top = 1
          Width = 56
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_HLabel_kp8: TLabel
          Left = 57
          Top = 1
          Width = 56
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_KuLabel_kp8: TLabel
          Left = 113
          Top = 1
          Width = 58
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
      end
      object m_AscanChartKP8: TChart
        Left = 1
        Top = 25
        Width = 172
        Height = 209
        AllowPanning = pmNone
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Maximum = 24.000000000000000000
        BottomAxis.Title.Caption = #1084#1082#1089
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsSize = 10
        LeftAxis.Maximum = 18.000000000000000000
        LeftAxis.Minimum = -12.000000000000000000
        LeftAxis.Title.Angle = 0
        LeftAxis.Title.Caption = #1076#1041
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Allow = False
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        OnClick = OnChartClicked
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object LineSeries6: TLineSeries
          Marks.Visible = False
          SeriesColor = 394758
          Title = 'AScanSeries'
          Brush.BackColor = clDefault
          LinePen.Width = 2
          Pointer.Brush.Gradient.EndColor = 394758
          Pointer.Gradient.EndColor = 394758
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
    object Panel10: TPanel
      Left = 349
      Top = 473
      Width = 176
      Height = 235
      Align = alClient
      Caption = 'KP2'
      TabOrder = 8
      object GridPanel9: TGridPanel
        Left = 1
        Top = 1
        Width = 174
        Height = 24
        Align = alTop
        ColumnCollection = <
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.333666055679210000
          end
          item
            Value = 33.332667888641580000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_NLabel_kp7
            Row = 0
          end
          item
            Column = 1
            Control = m_HLabel_kp7
            Row = 0
          end
          item
            Column = 2
            Control = m_KuLabel_kp7
            Row = 0
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 0
        object m_NLabel_kp7: TLabel
          Left = 1
          Top = 1
          Width = 57
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_HLabel_kp7: TLabel
          Left = 58
          Top = 1
          Width = 57
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
        object m_KuLabel_kp7: TLabel
          Left = 115
          Top = 1
          Width = 58
          Height = 22
          Align = alClient
          Alignment = taCenter
          Caption = '---'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          Layout = tlCenter
          ExplicitWidth = 15
          ExplicitHeight = 17
        end
      end
      object m_AscanChartKP7: TChart
        Left = 1
        Top = 25
        Width = 174
        Height = 209
        AllowPanning = pmNone
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Maximum = 24.000000000000000000
        BottomAxis.Title.Caption = #1084#1082#1089
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsSize = 10
        LeftAxis.Maximum = 18.000000000000000000
        LeftAxis.Minimum = -12.000000000000000000
        LeftAxis.Title.Angle = 0
        LeftAxis.Title.Caption = #1076#1041
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Allow = False
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        OnClick = OnChartClicked
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object LineSeries7: TLineSeries
          Marks.Visible = False
          SeriesColor = 394758
          Title = 'AScanSeries'
          Brush.BackColor = clDefault
          LinePen.Width = 2
          Pointer.Brush.Gradient.EndColor = 394758
          Pointer.Gradient.EndColor = 394758
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
  end
  object m_LeftPanel: TPanel
    Left = 0
    Top = 0
    Width = 417
    Height = 710
    Align = alLeft
    TabOrder = 1
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 415
      Height = 72
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label1: TLabel
        AlignWithMargins = True
        Left = 0
        Top = 4
        Width = 173
        Height = 33
        Alignment = taCenter
        AutoSize = False
        Caption = #1040#1042#1058#1054#1050#1054#1053'-'#1057
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
      object Label2: TLabel
        AlignWithMargins = True
        Left = 31
        Top = 29
        Width = 117
        Height = 13
        Caption = #1054#1040#1054' '#171#1056#1072#1076#1080#1086#1072#1074#1080#1086#1085#1080#1082#1072#187
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
      end
      object Label3: TLabel
        Left = 43
        Top = 43
        Width = 89
        Height = 13
        Caption = #1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object m_DateLabel: TLabel
        AlignWithMargins = True
        Left = 179
        Top = 7
        Width = 265
        Height = 26
        AutoSize = False
        Caption = '02.14.2015'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 432
      Width = 415
      Height = 277
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      DesignSize = (
        415
        277)
      object m_CloseBtn: TSpeedButton
        Left = 7
        Top = 5
        Width = 132
        Height = 51
        Caption = #1047#1072#1082#1088#1099#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = m_CloseBtnClick
      end
      object SpeedButton2: TSpeedButton
        Left = 145
        Top = 5
        Width = 125
        Height = 51
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object m_BScanExpandBtn: TSpeedButton
        Left = 315
        Top = 6
        Width = 95
        Height = 51
        Anchors = [akTop, akRight]
        Caption = '>>>'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = m_BScanExpandBtnClick
      end
      object Label5: TLabel
        AlignWithMargins = True
        Left = 4
        Top = 54
        Width = 407
        Height = 31
        Align = alBottom
        Caption = #1040#1082#1090#1080#1074#1085#1099#1077' '#1082#1072#1085#1072#1083#1099':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -26
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        ExplicitWidth = 203
      end
      object Panel11: TPanel
        AlignWithMargins = True
        Left = 4
        Top = 143
        Width = 407
        Height = 130
        Align = alBottom
        BevelKind = bkFlat
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object m_AdjustingNextBtn: TSpeedButton
          AlignWithMargins = True
          Left = 3
          Top = 34
          Width = 397
          Height = 89
          Align = alClient
          Caption = #1044#1074#1080#1075#1072#1090#1100' '#1085#1080#1078#1085#1102#1102' '#1082#1072#1088#1077#1090#1082#1091
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -28
          Font.Name = 'Myriad Pro'
          Font.Style = []
          ParentFont = False
          Spacing = 0
          OnClick = m_AdjustingNextBtnClick
          ExplicitLeft = 7
        end
        object m_AdjustingStateLabel: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 397
          Height = 25
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          Caption = #1070#1089#1090#1080#1088#1086#1074#1082#1072': '#1044#1074#1080#1078#1077#1085#1080#1077' '#1074#1077#1088#1093#1085#1077#1081' '#1082#1072#1088#1077#1090#1082#1080
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Myriad Pro'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitLeft = 9
          ExplicitTop = 9
          ExplicitWidth = 376
        end
      end
      object m_ActiveChannelsCombo: TComboFlat
        AlignWithMargins = True
        Left = 4
        Top = 91
        Width = 407
        Height = 46
        Align = alBottom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -32
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnSelect = m_ActiveChannelsComboSelect
        Items.Strings = (
          #1054#1090#1082#1083#1102#1095#1080#1090#1100
          #1050#1055'1, '#1050#1055'2, '#1050#1055'4'
          #1050#1055'3, '#1050#1055'5, '#1050#1055'8'
          #1050#1055'6, '#1050#1055'7')
      end
    end
    object m_BScanPanel: TPanel
      Left = 1
      Top = 73
      Width = 415
      Height = 359
      Align = alClient
      TabOrder = 2
      OnResize = m_BScanPanelResize
      object m_BScanPBox: TPaintBox
        Left = 1
        Top = 1
        Width = 413
        Height = 357
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnMouseDown = m_BScanPBoxMouseDown
        OnPaint = m_BScanPBoxPaint
        ExplicitLeft = 5
        ExplicitTop = -4
        ExplicitWidth = 381
        ExplicitHeight = 550
      end
    end
  end
  object m_DrawTimer: TTimer
    Enabled = False
    Interval = 40
    OnTimer = m_DrawTimerTimer
    Left = 49
    Top = 81
  end
  object m_UpdateTimer: TTimer
    Enabled = False
    Interval = 300
    OnTimer = m_UpdateTimerTimer
    Left = 48
    Top = 152
  end
  object m_ChangeModeTimer: TTimer
    Enabled = False
    Interval = 300
    OnTimer = m_ChangeModeTimerTimer
    Left = 192
    Top = 80
  end
end
