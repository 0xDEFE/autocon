//---------------------------------------------------------------------------

#pragma hdrstop

#include "ArchiveDefinitions.h"
#include "ArchivePDFCreator.h"
#include "ReportViewerUnit.h"
#include <DateUtils.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)

cArchivePDFCreator::cArchivePDFCreator()
{
	memset(RailImageBitmaps, 0, sizeof(RailImageBitmaps));
	memset(ChannelsImageBitmaps, 0, sizeof(ChannelsImageBitmaps));
	pCameraImageBitmap = NULL;
    bInitialized = false;
    titleNumber = 0;
    subTitleNumber = 0;
};

cArchivePDFCreator::~cArchivePDFCreator()
{
	release();
};

bool cArchivePDFCreator::init()
{
	if(bInitialized)
		return true;

	if(!initPathes())
		return false;

	for(int i = 0; i < 8; i++)
	{
		RailImageBitmaps[i] = new TBitmap();
        RailImageBitmaps[i]->PixelFormat = pf24bit;
		ChannelsImageBitmaps[i] = new TBitmap();
        ChannelsImageBitmaps[i]->PixelFormat = pf24bit;

		RailImageUnvisibleFlags[i] = ~(1<<i) & ~(cDrawRailUnit::RAIL_HGL_S1 |
											cDrawRailUnit::RAIL_HGL_S2 |
											cDrawRailUnit::RAIL_HGL_S3 |
											cDrawRailUnit::RAIL_HGL_S4 |
											cDrawRailUnit::RAIL_HGL_S5);
	}

    painter.init(AutoconMain->Rep);
    painter.setHideChannelsMode(CFM_HIDE_NONE);

	bInitialized = true;
	return true;
}

//init alive file pathes
bool cArchivePDFCreator::initPathes()
{
	PathMgr::PushPath(L"$pdf_layouts", L"resources\\archive\\print\\layouts");
	PathMgr::PushPath(L"$pdf_fonts", L"resources\\archive\\print\\fonts");

	titlePagePath = PathMgr::GetPath(L"($pdf_layouts)\\title.svg");
	endingPagePath = PathMgr::GetPath(L"($pdf_layouts)\\Avtokon2.svg");
	bscanPagePath = PathMgr::GetPath(L"($pdf_layouts)\\bscan_large.svg");
	bscanPageShortPath = PathMgr::GetPath(L"($pdf_layouts)\\bscan_short.svg");
    hscanPagePath = PathMgr::GetPath(L"($pdf_layouts)\\hscan.svg");
    hscanBScanPagePath = PathMgr::GetPath(L"($pdf_layouts)\\hscan_bscan.svg");
    channelsTablePath = PathMgr::GetPath(L"($pdf_layouts)\\channels.svg");
    linearityPagePath = PathMgr::GetPath(L"($pdf_layouts)\\linearity.svg");
	fontsPath = PathMgr::GetPath(L"($pdf_fonts)");



	if(titlePagePath.Length() == 0)
		return false;
	if(endingPagePath.Length() == 0)
		return false;
	if(bscanPagePath.Length() == 0)
		return false;
    if(hscanPagePath.Length() == 0)
		return false;
    if(hscanBScanPagePath.Length() == 0)
		return false;
    if(linearityPagePath.Length() == 0)
		return false;
	if(fontsPath.Length() == 0)
		return false;
    return true;
}

void cArchivePDFCreator::release()
{
	for(int i = 0; i < 8; i++)
	{
		if(RailImageBitmaps[i])
		{
			delete RailImageBitmaps[i];
			RailImageBitmaps[i] = NULL;
		}
		if(ChannelsImageBitmaps[i])
		{
			delete ChannelsImageBitmaps[i];
			ChannelsImageBitmaps[i] = NULL;
		}
	}

	if(pCameraImageBitmap)
	{
		delete pCameraImageBitmap;
		pCameraImageBitmap = NULL;
	}

    for(unsigned int i = 0; i < temporaryBitmaps.size(); i++)
    {
        if(temporaryBitmaps[i])
            delete temporaryBitmaps[i];
    }
    temporaryBitmaps.clear();

	pdfDoc.ClearPages();
}

bool cArchivePDFCreator::save(const char* outPath, unsigned int ThIndex, bool bTitlePage, bool bBScanShort, bool bBScanDetail, bool bHScanPages, bool bChannelsTable, bool bLinearityPage)
{
	if(!bInitialized)
	{
		if(!init())
			return false;
	};
    if(ThIndex > 16)
        ThIndex = 16;

    titleNumber = 1;
    subTitleNumber = 1;

    if(bTitlePage)
    	addTitlePage(titlePagePath.c_str());

    if(bBScanShort)
    {
    	addBScanPageShort(bscanPageShortPath.c_str(),ThIndex);
        titleNumber++;
    }

    if(bBScanDetail)
    {
        subTitleNumber = 1;

        addBScanPage(bscanPagePath.c_str(),ThIndex, 0, 0, 7); //KP1 and KP8
        subTitleNumber++;
        addBScanPage(bscanPagePath.c_str(),ThIndex, 0, 1, 2); //KP2 and KP3
        subTitleNumber++;
        addBScanPage(bscanPagePath.c_str(),ThIndex, 0, 3, 4); //KP4 and KP5
        subTitleNumber++;
        addBScanPage(bscanPagePath.c_str(),ThIndex, 0, 5, 6); //KP6 and KP7
        subTitleNumber++;
        titleNumber++;
    }

    if(bHScanPages)
    {
        subTitleNumber = 1;
        std::vector < TJPEGImage* >& arr = AutoconMain->Rep->ScreenShotHandScanList;
        int size = arr.size();
        for(int i = 0; i < size; i+=4)
        {
            addHandScanPage(hscanPagePath.c_str(), (i==0),i,
                                (i<size) ? arr[i] : NULL,
                                ((i+1)<size) ? arr[i+1] : NULL,
                                ((i+2)<size) ? arr[i+2] : NULL,
                                ((i+3)<size) ? arr[i+3] : NULL);
            subTitleNumber++;
        }
        if(subTitleNumber != 1) //if a-scan exists
            titleNumber++;
        subTitleNumber = 1;

        size = AutoconMain->Rep->GetHandScanCount();
        for(int i = 0; i < size; i+=3)
        {
            addHandBScanPage(hscanBScanPagePath.c_str(), (i==0),i, std::min(3, size-i));
            subTitleNumber++;
        }
        if(subTitleNumber != 1) //if b-scan exists
            titleNumber++;
    }

    if(bChannelsTable)
    {
        subTitleNumber = 1;
        bool bNeedMoreChannels = true;
        int startChNum = 1;
        int startCnIdx = 0;
        bool bTitle = true;
        while( bNeedMoreChannels )
        {
           int nextChNum, nextChIdx;
           bNeedMoreChannels = addChannelsTablePage(channelsTablePath.c_str(),bTitle, startChNum,startCnIdx, &nextChNum, &nextChIdx);
           bTitle = false;
           startChNum = nextChNum;
           startCnIdx = nextChIdx;
           subTitleNumber++;
        }
        titleNumber++;
    }

    if(bLinearityPage)
    {
        subTitleNumber = 1;
        addLinearityPage(linearityPagePath.c_str(),
                            AutoconMain->Rep->PhoneScreenShot,
                            AutoconMain->Rep->PhoneScreenShot2,
                            AutoconMain->Rep->PhoneScreenShot3);
    }



	bool bRet = pdfDoc.SaveAsPDF(outPath,fontsPath.c_str());
	return bRet;
}

void cArchivePDFCreator::addTitlePage(const char* titlePagePath_)
{
	int pi = pdfDoc.LoadPage(titlePagePath_);
	if(pi == -1)
		return;

	PDFPage* page = pdfDoc.GetPage(pi);

	if(AutoconMain->Rep->Photo)
	{
		pCameraImageBitmap = new TBitmap();
        pCameraImageBitmap->Assign(AutoconMain->Rep->Photo);
        pCameraImageBitmap->PixelFormat = pf32bit;
		page->GetRectangle("_CameraImage")->image = pCameraImageBitmap;
	}

	page->GetText("_ProtocolNumber")->text +=
						StringFormatA("  %d",AutoconMain->Rep->Header.ReportNumber);
	page->GetText("_Date")->text +=
						StringFormatU(L"  %s",AutoconMain->Rep->Header.DateTime.DateTimeString().c_str());
	page->GetText("_Plant")->text +=
						StringFormatU(L" ���1"); //FDV
//						StringFormatU(L"  %s",AutoconMain->Rep->Header.Plaint.c_str()); //FDV
	page->GetText("_PlantNumber")->text +=
						StringFormatU(L"  %s",AutoconMain->Rep->Header.PlantNumber.c_str());
	page->GetText("_PletNumber")->text +=
						StringFormatA("  %d",AutoconMain->Rep->Header.PletNumber);
	page->GetText("_JointNumber")->text +=
						StringFormatA("  %d",AutoconMain->Rep->Header.JointNumber);
	page->GetText("_Operator")->text +=
						StringFormatU(L"  %s",AutoconMain->Rep->Header.Operator.c_str());
	page->GetText("_ValidityGroup")->text +=
						StringFormatU(L"  %s",AutoconMain->Rep->Header.ValidityGroup.c_str());
	page->GetText("_StraightnessTop")->text +=
						StringFormatA("  %.3f",AutoconMain->Rep->Header.TopStraightness_); //FDV
	page->GetText("_WFStraightnessSide")->text +=
						StringFormatA("  %.3f",AutoconMain->Rep->Header.WF_Straightness_); //FDV
	page->GetText("_UWFStraightnessSide")->text +=
						StringFormatA("  %.3f",AutoconMain->Rep->Header.WNF_Straightness_); //FDV

    page->GetText("_TestOnSSC")->text +=
						StringFormatA("  %d ��. �����",DaysBetween(AutoconMain->Rep->Header.LastSSCTestDateTime, Now()));
    page->GetText("_TestOnSS3R")->text +=
						StringFormatA("  %d ��. �����",DaysBetween(AutoconMain->Rep->Header.LastSS3RTestDateTime, Now()));

    switch(AutoconMain->Rep->Header.HardnessState)
    {
        case 0:
            page->GetText("_Hardness")->text += "  �� ����������";
            break;
        case 1:
            page->GetText("_Hardness")->text += "  ����������";
            break;
        case 2:
        {
            int daysAgo = DaysBetween(AutoconMain->Rep->Header.HardnessLastMeasureDateTime,
                AutoconMain->Rep->Header.DateTime);
            page->GetText("_Hardness")->text += "  ���������� " + IntToStr(daysAgo) + " ��. �����";
            break;
        }
    }

	for(int i = 0; i < 9; i++)
	{
		int hs = AutoconMain->Rep->Header.Hardness[i];
		AnsiString val = (AutoconMain->Rep->Header.HardnessState) ? StringFormatA("  %d HB",hs).c_str() : "  -";
		AnsiString name = StringFormatA("_HardnessLine%d",i+1);

		page->GetText(name.c_str())->text += val;
	}

    assert(AutoconMain->Config->GetHandChannelsCount() == 7);

    sHandChannelDescription scanChDesc;
    sChannelDescription chDesc;
    int nLine = 0;
    for(int i = 0; i < 7; i++)
    {
        AutoconMain->Config->getHChannelbyIdx(i,&scanChDesc);
        AutoconMain->Table->ItemByCID(scanChDesc.Id, &chDesc);
        AnsiString name = StringFormatA("_HandControlLine%d",nLine+1);

        //Convert to minutes
        int timeInChannelSec = AutoconMain->Rep->GetTimeInHandScanChannel(scanChDesc.Id);
        if(timeInChannelSec == 0)
            continue;
        AnsiString timeInChannelStr = (timeInChannelSec < 60) ? "������ ������" :
                                     StringFormatA("%d ���",int((float(timeInChannelSec)+0.5)/60.f)).c_str();

        page->GetText(name.c_str())->text += StringFormatA("  %d� (%s)",chDesc.cdEnterAngle, timeInChannelStr.c_str());
        nLine++;
    }
    for(int i = nLine; i < 7; i++)
    {
        AnsiString name = StringFormatA("_HandControlLine%d",i+1);
        page->GetText(name.c_str())->text = "";
    }

	page->GetText("_HandControl")->text +=
						StringFormatA("  %s",nLine ? "����������" : "�� ����������");

	page->GetText("_Conclusion")->text +=
						StringFormatU(L"  %s",AutoconMain->Rep->Header.�onclusion.c_str());

	page->GetText("_Comment")->text +=
						StringFormatU(L"  %s",L" ");
	page->GetText("_CreateDate")->text +=
						StringFormatU(L"  %s",AutoconMain->Rep->Header.DateTime.DateTimeString().c_str());
}

void cArchivePDFCreator::addBScanPage(const char* bscanPagePath_,unsigned int ThIndex,int pageIndex, int firstScanIndex, int secondScanIndex)
{
	int pi = pdfDoc.LoadPage(bscanPagePath_);
	if(pi == -1)
		return;

    const UnicodeString TextList[17] = {"����", "-12", "-10", "-8", "-6", "-4", "-2", "0", "2", "4", "6", "8", "10", "12", "14", "16", "18"};
    const int ValList[17] = {0, 8, 10, 12, 16, 20, 25, 32, 40, 50, 64, 80, 101, 128, 160, 202, 254};

	PDFPage* page = pdfDoc.GetPage(pi);

	sPDFRect* rail_rect1 = page->GetRectangle("_RailImage1");
	sPDFRect* bscan_rect1 = page->GetRectangle("_ChannelsImage1");
	sPDFRect* rail_rect2 = page->GetRectangle("_RailImage2");
	sPDFRect* bscan_rect2 = page->GetRectangle("_ChannelsImage2");

	//-----------------Update scans-------------------
    const int size_mult = 1;
    int rail_width = rail_rect1->width * 2;
    int rail_height= rail_rect1->height * 2;
    int bscan_width = bscan_rect1->width;
    int bscan_height= bscan_rect1->height;

    painter.updateHightlight(true);

	painter.setBScanPageRect(TRect(0,0, bscan_width, bscan_height ),firstScanIndex+1);
	painter.setBScanPageRect(TRect(0,0, bscan_width, bscan_height ),secondScanIndex+1);
    painter.pScanDraw->Prepare();

    painter.pScanDraw->Th = ValList[ThIndex];
    painter.pScanDraw->Draw(false);

	painter.setRailRect(TRect(0,0,rail_width, rail_height ));
	painter.updateRail();

    RailImageBitmaps[firstScanIndex]->SetSize( rail_width, rail_height );
    RailImageBitmaps[secondScanIndex]->SetSize( rail_width, rail_height );

    ChannelsImageBitmaps[firstScanIndex]->SetSize( bscan_width, bscan_height );
    ChannelsImageBitmaps[secondScanIndex]->SetSize( bscan_width, bscan_height );

    painter.railDraw.draw(RailImageBitmaps[firstScanIndex], false, RailImageUnvisibleFlags[firstScanIndex]);
    painter.railDraw.draw(RailImageBitmaps[secondScanIndex], false, RailImageUnvisibleFlags[secondScanIndex]);
	//painter.drawRail(RailImageUnvisibleFlags[firstScanIndex],false,RailImageBitmaps[firstScanIndex]->Canvas);
	//painter.drawRail(RailImageUnvisibleFlags[secondScanIndex],false,RailImageBitmaps[secondScanIndex]->Canvas);

	painter.drawBScanPage(firstScanIndex  + 1,ChannelsImageBitmaps[firstScanIndex]->Canvas);
	painter.drawBScanPage(secondScanIndex + 1,ChannelsImageBitmaps[secondScanIndex]->Canvas);

	//------------------Titles------------------------
	if(subTitleNumber!=1)
	    page->GetText("_Title1")->text = "";
    else
        page->GetText("_Title1")->text = StringFormatU(L"%d. ���������� ������������������� �������� (����� %s%s)",titleNumber,TextList[ThIndex].c_str(), ThIndex ? L" ��" : L"");

	page->GetText("_Title2")->text = StringFormatU(L"%d.%d. ������ ������������ ��%d � ��%d",titleNumber,subTitleNumber,firstScanIndex+1,secondScanIndex+1);

	//---------------Page number----------------------
	page->GetText("_PageNumber")->text = StringFormatU(L"���. %d", pdfDoc.GetPageCount());

	//-----------------Protocol Number----------------
	page->GetText("_ProtocolNumber")->text +=
						StringFormatA("  %d",AutoconMain->Rep->Header.ReportNumber);
	page->GetText("_Date")->text +=
						StringFormatU(L"  %s",AutoconMain->Rep->Header.DateTime.DateTimeString().c_str());
	//------------------First scan------------------------

	rail_rect1->bRotate = true;
	rail_rect1->image = RailImageBitmaps[firstScanIndex];

	bscan_rect1->bRotate = true;
	bscan_rect1->image = ChannelsImageBitmaps[firstScanIndex];

	rail_rect2->bRotate = true;
	rail_rect2->image = RailImageBitmaps[secondScanIndex];

	bscan_rect2->bRotate = true;
	bscan_rect2->image = ChannelsImageBitmaps[secondScanIndex];
}


void cArchivePDFCreator::addBScanPageShort(const char* bscanPagePath,unsigned int ThIndex)
{
	int pi = pdfDoc.LoadPage(bscanPagePath);
	if(pi == -1)
		return;

    const UnicodeString TextList[17] = {"����", "-12", "-10", "-8", "-6", "-4", "-2", "0", "2", "4", "6", "8", "10", "12", "14", "16", "18"};
    const int ValList[17] = {0, 8, 10, 12, 16, 20, 25, 32, 40, 50, 64, 80, 101, 128, 160, 202, 254};

	PDFPage* page = pdfDoc.GetPage(pi);

    page->GetText("_Title1")->text = StringFormatU(L"%d. ���������� ������������������� �������� (����� %s%s)",titleNumber,TextList[ThIndex].c_str(), ThIndex ? L" ��" : L"");

	//---------------Page number----------------------
	page->GetText("_PageNumber")->text = StringFormatU(L"���. %d", pdfDoc.GetPageCount());

	//-----------------Protocol Number----------------
	page->GetText("_ProtocolNumber")->text +=
						StringFormatA("  %d",AutoconMain->Rep->Header.ReportNumber);
	page->GetText("_Date")->text +=
						StringFormatU(L"  %s",AutoconMain->Rep->Header.DateTime.DateTimeString().c_str());
	//------------------------------------------------

	TBitmap* BScanBitmap = new TBitmap();
	//TRect Rects[9];
	int RailWidth, RailHeight;

    sPDFRect* BScanRect = page->GetRectangle("_ChannelsImage1");
    int bscan_width =  float(BScanRect->width) * 1.5;
    int bscan_height =  float(BScanRect->height) * 1.5;

	BScanRect->image = BScanBitmap;
	BScanRect->bRotate = true;
	BScanBitmap->SetSize(bscan_width,bscan_height);

	painter.applyBScanLayout();
	painter.setBScanRect(TRect(0,0,bscan_width,bscan_height));


    painter.pScanDraw->Th = ValList[ThIndex];
    painter.updateHightlight(true);
	painter.updateBScan(false);
	painter.drawBScan(BScanBitmap->Canvas);

    temporaryBitmaps.push_back(BScanBitmap);
}

void cArchivePDFCreator::addHandScanPage(const char* hscanPagePath, bool bTitle,int imgFirstCnt,
				TJPEGImage* firstScanImg, TJPEGImage* secondScanImg,
				TJPEGImage* thirdScanImg, TJPEGImage* fourthScanImg)
{
    int pi = pdfDoc.LoadPage(hscanPagePath);
	if(pi == -1)
		return;
    PDFPage* page = pdfDoc.GetPage(pi);

    //---------------Page number----------------------
	page->GetText("_PageNumber")->text = StringFormatU(L"���. %d", pdfDoc.GetPageCount());
    //--------------Title-----------------------------

    if(subTitleNumber!=1)
	    page->GetText("_Title1")->text = "";
    else
        page->GetText("_Title1")->text = StringFormatU(L"%d. ���������� �������� ������ ��� (�-����)",titleNumber);

    int imgCount = (firstScanImg ? 1 : 0) + (secondScanImg ? 1 : 0) + (thirdScanImg ? 1 : 0) + (fourthScanImg ? 1 : 0);
	page->GetText("_Title2")->text = StringFormatU(L"%d.%d. ������ %d - %d",titleNumber,subTitleNumber,imgFirstCnt+1, imgFirstCnt+imgCount);

    if(!bTitle)
        page->GetText("_Title1")->text = "";
    //-----------------Protocol Number----------------
	page->GetText("_ProtocolNumber")->text +=
						StringFormatA("  %d",AutoconMain->Rep->Header.ReportNumber);
	page->GetText("_Date")->text +=
						StringFormatU(L"  %s",AutoconMain->Rep->Header.DateTime.DateTimeString().c_str());
    //--------------Images-----------------------------
    sPDFRect* ImgRects[4];
    TBitmap* ImgBmps[4] = {NULL,NULL,NULL,NULL};

    for(int i = 0; i < 4; i++)
        ImgRects[i] = page->GetRectangle(StringFormatA("_HandImage%d",i+1).c_str());

    if(firstScanImg)    {(ImgBmps[0]=new TBitmap())->Assign(firstScanImg);  ImgBmps[0]->PixelFormat = pf32bit;};
    if(secondScanImg)   {(ImgBmps[1]=new TBitmap())->Assign(secondScanImg); ImgBmps[1]->PixelFormat = pf32bit;};
    if(thirdScanImg)    {(ImgBmps[2]=new TBitmap())->Assign(thirdScanImg);  ImgBmps[2]->PixelFormat = pf32bit;};
    if(fourthScanImg)   {(ImgBmps[3]=new TBitmap())->Assign(fourthScanImg); ImgBmps[3]->PixelFormat = pf32bit;};

    for(int i = 0; i < 4; i++)
    {
        if(ImgBmps[i])
        {
            ImgRects[i]->image = ImgBmps[i];
            temporaryBitmaps.push_back(ImgBmps[i]);
        }
    }
}

void cArchivePDFCreator::addHandBScanPage(const char* hscanPagePath_, bool bTitle,int scanStartIndex, int itemCount)
{
    int pi = pdfDoc.LoadPage(hscanPagePath_);
	if(pi == -1)
		return;
    PDFPage* page = pdfDoc.GetPage(pi);

    //---------------Page number----------------------
	page->GetText("_PageNumber")->text = StringFormatU(L"���. %d", pdfDoc.GetPageCount());
    //--------------Title-----------------------------

    if(subTitleNumber!=1)
	    page->GetText("_Title1")->text = "";
    else
        page->GetText("_Title1")->text = StringFormatU(L"%d. ���������� �������� ������ ��� (B-����)",titleNumber);

	page->GetText("_Title2")->text = StringFormatU(L"%d.%d. ������ %d - %d",titleNumber,subTitleNumber,scanStartIndex+1, scanStartIndex+itemCount);

    if(!bTitle)
        page->GetText("_Title1")->text = "";
    //-----------------Protocol Number----------------
	page->GetText("_ProtocolNumber")->text +=
						StringFormatA("  %d",AutoconMain->Rep->Header.ReportNumber);
	page->GetText("_Date")->text +=
						StringFormatU(L"  %s",AutoconMain->Rep->Header.DateTime.DateTimeString().c_str());
    //--------------Images-----------------------------

    for(int i = 0; i < 3; i++)
    {
        if(i < itemCount)
        {
            int curHandScanIndex = i + scanStartIndex;

            sHandScanParams params;
            bool bRes = false;
            int coordCount;
            PsHandSignalsOneCrd coords_arr = AutoconMain->Rep->GetHandSignals( curHandScanIndex, &params, &bRes, &coordCount);

            if(!bRes)    //error
                continue;

            sPDFRect* pRect = page->GetRectangle(StringFormatA("_Image%d",i+1).c_str());

            if(coords_arr)
            {
                TBitmap* pBitmap = new TBitmap();
                temporaryBitmaps.push_back(pBitmap);
                pBitmap->SetSize(pRect->width,pRect->height);
                pRect->image = pBitmap;
                DrawHandBScanLine(curHandScanIndex,coords_arr,coordCount, pBitmap->Canvas, pRect->width, pRect->height);
            }
            else
            {
                pRect->bDrawRect = true;
                pRect->text = "��� ��������";
                pRect->font_size = 28;
                pRect->font = NULL;
            }

            AnsiString MethodStr;
            switch( params.Method )
            {
                case imNotSet:          MethodStr = "�� �����"; break;
                case imEcho:            MethodStr = "��� �����"; break;
                case imMirrorShadow:    MethodStr = "��������� ������� �����"; break;
                case imMirror:          MethodStr = "���������� �����"; break;
                case imShadow:          MethodStr = "������� �����"; break;
            }

            page->GetText(StringFormatA("_EnterAngle%d",i+1).c_str())->text += IntToStr(params.EnterAngle) + "�";
            page->GetText(StringFormatA("_Method%d",i+1).c_str())->text += MethodStr;
            page->GetText(StringFormatA("_Surface%d",i+1).c_str())->text += IntToStr(params.ScanSurface);
            page->GetText(StringFormatA("_Sensitivity%d",i+1).c_str())->text += IntToStr(params.Ku) + L" ��";
            page->GetText(StringFormatA("_Att%d",i+1).c_str())->text += IntToStr(params.Att) + L" ��";
            page->GetText(StringFormatA("_StGate%d",i+1).c_str())->text += IntToStr(params.StGate) + L" ���";
            page->GetText(StringFormatA("_EdGate%d",i+1).c_str())->text += IntToStr(params.EdGate) + L" ���";
            page->GetText(StringFormatA("_TVG%d",i+1).c_str())->text += IntToStr(params.TVG) + L" ���";
            page->GetText(StringFormatA("_PrismDelay%d",i+1).c_str())->text += IntToStr(params.PrismDelay) + L" ���";
        }
        else
        {
            page->GetText(StringFormatA("_EnterAngle%d",i+1).c_str())->text = "";
            page->GetText(StringFormatA("_Method%d",i+1).c_str())->text = "";
            page->GetText(StringFormatA("_Surface%d",i+1).c_str())->text = "";
            page->GetText(StringFormatA("_Sensitivity%d",i+1).c_str())->text = "";
            page->GetText(StringFormatA("_Att%d",i+1).c_str())->text = "";
            page->GetText(StringFormatA("_StGate%d",i+1).c_str())->text = "";
            page->GetText(StringFormatA("_EdGate%d",i+1).c_str())->text = "";
            page->GetText(StringFormatA("_TVG%d",i+1).c_str())->text = "";
            page->GetText(StringFormatA("_PrismDelay%d",i+1).c_str())->text = "";
        }
    }
}

void cArchivePDFCreator::addLinearityPage(const char* linearityPagePath, TJPEGImage* firstImg, TJPEGImage* secondImg,TJPEGImage* thirdImg)
{
    int pi = pdfDoc.LoadPage(linearityPagePath);
	if(pi == -1)
		return;
    PDFPage* page = pdfDoc.GetPage(pi);

    //---------------Page number----------------------
	page->GetText("_PageNumber")->text = StringFormatU(L"���. %d", pdfDoc.GetPageCount());
    //--------------Title-----------------------------

    if(subTitleNumber!=1)
	    page->GetText("_Title1")->text = "";
    else
        page->GetText("_Title1")->text = StringFormatU(L"%d. ���������� ��������� ����������",titleNumber);

	page->GetText("_Title2")->text = StringFormatU(L"%d.%d. ��������� ���������� ������ (1), �� ������� ����� (2) � �� ��������� ����� (3)",titleNumber,subTitleNumber);

    //-----------------Protocol Number----------------
	page->GetText("_ProtocolNumber")->text +=
						StringFormatA("  %d",AutoconMain->Rep->Header.ReportNumber);
	page->GetText("_Date")->text +=
						StringFormatU(L"  %s",AutoconMain->Rep->Header.DateTime.DateTimeString().c_str());
    //--------------Images-----------------------------

    if(firstImg)
    {
        sPDFRect* pRect = page->GetRectangle("_LinearityImage1");
        TBitmap* pBmp = new TBitmap();
        pBmp->Assign(firstImg); pBmp->PixelFormat = pf32bit;
        pRect->image = pBmp;
        pRect->bRotate = true;
        //pRect->bDrawRect = true;
        temporaryBitmaps.push_back(pBmp);
    }

    if(secondImg)
    {
        sPDFRect* pRect = page->GetRectangle("_LinearityImage2");
        TBitmap* pBmp = new TBitmap();
        pBmp->Assign(secondImg); pBmp->PixelFormat = pf32bit;
        pRect->image = pBmp;
        pRect->bRotate = true;
        //pRect->bDrawRect = true;
        temporaryBitmaps.push_back(pBmp);
    }

    if(thirdImg)
    {
        sPDFRect* pRect = page->GetRectangle("_LinearityImage3");
        TBitmap* pBmp = new TBitmap();
        pBmp->Assign(thirdImg); pBmp->PixelFormat = pf32bit;
        pRect->image = pBmp;
        pRect->bRotate = true;
        //pRect->bDrawRect = true;
        temporaryBitmaps.push_back(pBmp);
    }
}

bool cArchivePDFCreator::addChannelsTablePage(const char* tablePagePath, bool bTitle, int startChannelNumber, int startChannelId, int* pEndChannelNumberOut, int* pEndChannelIdOut)
{
    int pi = pdfDoc.LoadPage(tablePagePath);
	if(pi == -1)
		return false;
    PDFPage* page = pdfDoc.GetPage(pi);

    //---------------Page number----------------------
	page->GetText("_PageNumber")->text = StringFormatU(L"���. %d", pdfDoc.GetPageCount());
    //--------------Title-----------------------------

    if(subTitleNumber!=1)
	    page->GetText("_Title1")->text = "";
    else
        page->GetText("_Title1")->text = StringFormatU(L"%d. ��������� �������",titleNumber);

    if(!bTitle)
        page->GetText("_Title1")->text = "";
    //-----------------Protocol Number----------------
	page->GetText("_ProtocolNumber")->text +=
						StringFormatA("  %d",AutoconMain->Rep->Header.ReportNumber);
	page->GetText("_Date")->text +=
						StringFormatU(L"  %s",AutoconMain->Rep->Header.DateTime.DateTimeString().c_str());

    //-----------------Table---------------------------

    sPDFRect* pRect = page->GetRectangle("tableRect");
    pRect->bDrawRect = false;

    const char* Titles[] =  {"�",   "��� ������",   "��",   "���,���",  "2��,���",  "��,��",   "���,��",  "�.���.,���",  "�.���.,���"};
    float TitleWidths[] =   {4,     12,             4,      8,           7,          6,         7,          10,                 10};
    float sum = 0;
    for(int i = 0; i < 9; i++)
        sum += TitleWidths[i];
    for(int i = 0; i < 9; i++)
        TitleWidths[i] /= sum;

    const int lineHeight = pRect->height / 40;
    int cell_y = 0;
    for(int verCrd = 0; verCrd < (pRect->height - lineHeight); cell_y++)
    {
        int currLineHeight = !verCrd ? (lineHeight*1.25) : lineHeight;

        float horCrd = 0;
        for(int cell_x = 0; cell_x < 9; cell_x++)
        {
            sPDFRect rect;
            rect.bDrawRect = true;
            rect.x = pRect->x + horCrd;
            rect.y = pRect->y + verCrd;
            rect.width = TitleWidths[cell_x] * pRect->width;
            rect.height = currLineHeight;
            rect.text = "";
            rect.bDrawRect = true;
            strcpy(rect.id, StringFormatA("cell%d%d",cell_x,cell_y).c_str());
            memset(rect.transform, 0, sizeof(rect.transform));
            rect.transform[0] = rect.transform[3] = 1; //Identity matrix
            rect.transform[4] = rect.x;
            rect.transform[5] = rect.y;

            rect.font_size = lineHeight - 4;
            rect.text = "nnn";

            page->AddRectangle(rect);

            horCrd += TitleWidths[cell_x] * pRect->width;
        }

        verCrd += currLineHeight;
    }

    const int cellCountX = 9;
    const int cellCountY = cell_y;

    for(int i = 0; i < cellCountX; i++)
        page->GetRectangle(StringFormatA("cell%d0",i).c_str())->text = Titles[i];

    int currChanNumber = startChannelNumber;
    CID currChanId = startChannelId;
    bool bBreak = false;
    cell_y = 1;
    bool bNeedMoreChannels = false;

    sChannelDescription chanDesc;
    sScanChannelDescription scanChanDesc;
    while(!bBreak)
    {
        CID chId;
        if(currChanId >= (int)AutoconMain->Table->Count())
        {
            bBreak = true;
            continue;
        }
        if(!AutoconMain->Table->CIDByIndex(&chId,currChanId++))
            continue;
        if(!AutoconMain->Table->ItemByCID(chId, &chanDesc))
            continue;
        bool bIsScanChannel = AutoconMain->Config->ScanChannelExists(chId);
        bool bIsHandChannel = AutoconMain->Config->HandChannelExists(chId);
        if(!(bIsScanChannel || bIsHandChannel))
            continue;

        page->GetRectangle(StringFormatA("cell0%d",cell_y).c_str())->text = currChanNumber;

        AnsiString chName = chanDesc.Title;
        if(bIsHandChannel)
            chName = L"���. " + chName;
        page->GetRectangle(StringFormatA("cell1%d",cell_y).c_str())->text = chName;

        if(bIsHandChannel)
        {
            page->GetRectangle(StringFormatA("cell2%d",cell_y).c_str())->text = "--";
        }
        else
        {
            if(AutoconMain->Config->getFirstSChannelbyID(chId, &scanChanDesc) != -1)
            {
                page->GetRectangle(StringFormatA("cell2%d",cell_y).c_str())->text = IntToStr(scanChanDesc.BScanGroup);
            }
        }

        int gateCount = chanDesc.cdGateCount;
        sScanChannelParams params_gate1, params_gate2;
        params_gate1 = AutoconMain->Rep->GetChannelParams(chId, 1);
        if(gateCount == 2)
            params_gate2 = AutoconMain->Rep->GetChannelParams(chId, 2);


        page->GetRectangle(StringFormatA("cell3%d",cell_y).c_str())->text = IntToStr(params_gate1.TVG);
        page->GetRectangle(StringFormatA("cell4%d",cell_y).c_str())->text = FloatToStr(float(params_gate1.PrismDelay)/10.f);
        page->GetRectangle(StringFormatA("cell5%d",cell_y).c_str())->text = IntToStr(params_gate1.Sens);
        page->GetRectangle(StringFormatA("cell6%d",cell_y).c_str())->text = IntToStr(params_gate1.Gain);
        if(gateCount == 2)
        {
            page->GetRectangle(StringFormatA("cell5%d",cell_y).c_str())->text += ", " + IntToStr(params_gate2.Sens);
            page->GetRectangle(StringFormatA("cell6%d",cell_y).c_str())->text += ", " + IntToStr(params_gate2.Gain);
        }
        page->GetRectangle(StringFormatA("cell7%d",cell_y).c_str())->text = IntToStr(params_gate1.StGate);
        page->GetRectangle(StringFormatA("cell8%d",cell_y).c_str())->text = IntToStr(params_gate1.EdGate);
        if(gateCount == 2)
        {
            page->GetRectangle(StringFormatA("cell7%d",cell_y).c_str())->text += ", " + IntToStr(params_gate2.StGate);
            page->GetRectangle(StringFormatA("cell8%d",cell_y).c_str())->text += ", " + IntToStr(params_gate2.EdGate);
        }

        currChanNumber++;
        cell_y++;
        if(cell_y >= cellCountY)
            bBreak = true;
    }

    for(int i = cell_y; i < cellCountY; i++)
        for(int j = 0; j < 9; j++)
            page->GetRectangle(StringFormatA("cell%d%d",j,i).c_str())->bDrawRect = false;

    if(currChanId < (int)AutoconMain->Table->Count())
    {
        bNeedMoreChannels = true;
    }

    *pEndChannelNumberOut = currChanNumber;
    *pEndChannelIdOut = currChanId;
    page->GetText("_Title2")->text = StringFormatU(L"%d.%d. ������ %d - %d",titleNumber,subTitleNumber,startChannelNumber,*pEndChannelNumberOut);

    return bNeedMoreChannels;
}
