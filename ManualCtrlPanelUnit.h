//---------------------------------------------------------------------------

#ifndef ManualCtrlPanelUnitH
#define ManualCtrlPanelUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Samples.Spin.hpp>

#include "AutoconMain.h"
#include <Vcl.ComCtrls.hpp>
//---------------------------------------------------------------------------
class TManualCtrlPanel : public TFrame
{
__published:	// IDE-managed Components
	TSpeedButton *m_CaretsHomeBtn;
	TSpeedButton *m_MoveTopCaretToLeftBtn;
	TSpeedButton *m_MoveBotCaretToLeftBtn;
	TSpeedButton *m_MoveBotCaretToRightBtn;
	TSpeedButton *m_MoveTopCaretToRightBtn;
	TSpeedButton *m_ExecBtn;
	TSpeedButton *m_ScanGroup10_Btn;
	TSpeedButton *m_ScanGroup12_Btn;
	TSpeedButton *m_ScanGroup14_Btn;
	TSpeedButton *m_ScanGroup3_Btn;
	TSpeedButton *m_ScanGroup5_Btn;
	TSpeedButton *m_ScanGroup6_Btn;
	TSpeedButton *m_ScanGroup8_Btn;
	TStaticText *m_TuneMaxCountLabel;
	TSpeedButton *m_TuneBtn;
	TSpeedButton *m_TuneClearBtn;
	TGridPanel *m_OffsetBtnsGrid;
	TGridPanel *m_CaretMovementGrid;
	TGridPanel *m_ScanGroupsGrid;
	TGridPanel *m_TuneGrid;
	TSpinEdit *m_BotCaretOffset;
	TSpinEdit *m_TopCaretOffset;
	TTimer *Timer1;
	TTrackBar *m_TopCaretTrack;
	TTrackBar *m_BotCaretTrack;
	TGridPanel *GridPanel1;
	TStaticText *m_TopCaretLabel;
	TStaticText *m_BotCaretLabel;
	TGridPanel *GridPanel2;
	TSpeedButton *m_ResetPathEncoderBtn;
	TLabel *m_XSysCoordLabel;
	TLabel *m_XSysDirLabel;
	TLabel *m_XSysCrdTrLabel;
	TSpeedButton *m_UpdateCaretsPosBtn;
	TGridPanel *GridPanel3;
	TGridPanel *GridPanel4;
    TSpeedButton *m_ClearBScanBtn;
    TSpeedButton *m_HelpBtn;
    TPanel *m_TopSpinPanel;
    TPanel *m_BotSpinPanel;
    TTimer *m_ChangeModeTimer;
	void __fastcall m_ExecBtnClick(TObject *Sender);
	void __fastcall m_MoveTopCaretToLeftBtnClick(TObject *Sender);
	void __fastcall m_MoveBotCaretToLeftBtnClick(TObject *Sender);
	void __fastcall m_CaretsHomeBtnClick(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall m_TuneBtnClick(TObject *Sender);
	void __fastcall m_ScanGroup3_BtnClick(TObject *Sender);
	void __fastcall m_TuneClearBtnClick(TObject *Sender);
	void __fastcall m_ResetPathEncoderBtnClick(TObject *Sender);
	void __fastcall m_UpdateCaretsPosBtnClick(TObject *Sender);
    void __fastcall m_ClearBScanBtnClick(TObject *Sender);
    void __fastcall m_HelpBtnClick(TObject *Sender);
    void __fastcall m_TopSpinPanelResize(TObject *Sender);
    void __fastcall m_BotSpinPanelResize(TObject *Sender);
    void __fastcall m_ChangeModeTimerTimer(TObject *Sender);
private:	// User declarations
	int curChGroup;
	void UpdateLabels();

    void __fastcall OnFormShow(TObject *Sender);
    void __fastcall OnFormHide(TObject *Sender);

public:		// User declarations
	__fastcall TManualCtrlPanel(TComponent* Owner);

	static TForm* FormDial;
	static void DialogShow();
	static void DialogHide();

    static TForm* HelpDial;
    static void ShowHelpDial();
};
//---------------------------------------------------------------------------
extern PACKAGE TManualCtrlPanel *ManualCtrlPanel;
//---------------------------------------------------------------------------


extern cAutoconMain *AutoconMain;

#endif
