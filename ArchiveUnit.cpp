//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ArchiveDefinitions.h"
#include "ArchiveUnit.h"
#include "ReportViewerUnit.h"
#include "ArchiveFilterFormUnit.h"
#include <FileCtrl.hpp>
//#include "Utils.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TArchiveForm *ArchiveForm;
//---------------------------------------------------------------------------
__fastcall TArchiveForm::TArchiveForm(TComponent* Owner)
	: TForm(Owner)
{
	bColSortOrderAsc = true;
	PreviousColumnIndex=-1;
	maxDBGridRowCount = 10;
	tableItemCount = 0;
	curItemIndex = 0;

	currProtocolType = ADB_PROTO_ALL;

	bBdNotFound = true;

	SelectedColumns = 0;
	SelectedColumns |= (1<<ADB_FIELDTYPE_FILENAME);
	SelectedColumns |= (1<<ADB_FIELDTYPE_DATETIME);
	SelectedColumns |= (1<<ADB_FIELDTYPE_GANG_NUMBER);
	SelectedColumns |= (1<<ADB_FIELDTYPE_OPERATOR);
	SelectedColumns |= (1<<ADB_FIELDTYPE_PLET_NUMBER);
	SelectedColumns |= (1<<ADB_FIELDTYPE_JOINT_NUMBER);
	SelectedColumns |= (1<<ADB_FIELDTYPE_DEFECT_CODE);
	SelectedColumns |= (1<<ADB_FIELDTYPE_VALIDITY_GROUP);

    bUISetStatus = false;
    bUISetProgress = false;
}
//---------------------------------------------------------------------------


void TArchiveForm::uiSetStatus(UnicodeString& str)
{
    uiStatusString = str;
    bUISetStatus = true;
}

void TArchiveForm::uiSetStatus(const char* str)
{
    uiStatusString = str;
    bUISetStatus = true;
}

void TArchiveForm::uiSetProgress(int value)
{
    uiProgressVal = value;
    bUISetProgress = true;
}

void __fastcall TArchiveForm::FormShow(TObject *Sender)
{
	bBdNotFound = true;

	const int dbPathsCount = 3;
	const UnicodeString dbPaths[] = {".\\AutoconDB.mdb",
									 ".\\resources\\archive\\AutoconDB.mdb",
									 "..\\resources\\archive\\AutoconDB.mdb"};

	UnicodeString dbPath;
	for( int i = 0; i < dbPathsCount; i++)
	{
		if(FileExists(dbPaths[i],true))
		{
			dbPath = dbPaths[i];
			bBdNotFound = false;
			break;
        }
	}

	if(bBdNotFound)
	{
		MessageBoxA(NULL,"���� ������ �� �������!","������!", MB_OK | MB_ICONWARNING);
		return;
    }


	UnicodeString connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=";
	connectionString+=dbPath + "; ";
	connectionString+="Mode=Share Deny None;Persist Security Info=False;Jet OLEDB:System database="";\
	Jet OLEDB:Registry Path="";Jet OLEDB:Database Password="";Jet OLEDB:Engine Type=5;\
	Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;\
	Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Database Password="";\
	Jet OLEDB:Create System Database=False;Jet OLEDB:Encrypt Database=False;\
	Jet OLEDB:Don't Copy Locale on Compact=False;Jet OLEDB:Compact Without Replica Repair=False;\
	Jet OLEDB:SFP=False";

	ADOConnection1->Connected = false;
	ADOConnection1->ConnectionString = connectionString;
	ADOConnection1->Connected = true;


	/*SQLConnection1->Params->Add("Database=..\\resources\\archive\\AutoconDB.db");
	SQLConnection1->LibraryName = ".\\sqlite3.dll";

	SQLConnection1->Connected = true;

	m_ComQuery->Active = true;
	m_ShowQuery->Active = true;
	//m_MainDataSet->Active = true;
	m_ComClientDataSet->Active = true;
	m_ShowClientDataSet->Active = true;
    */

	updateSelectedColumns();
    updateFilterQuery();
    updatePageLabel();
    updateUI();

	//m_ModeBtn->OnClick(NULL);

    m_UpdateBtnClick(NULL);
}
//---------------------------------------------------------------------------

void TArchiveForm::updateSelectedColumns()
{
	DBGrid1->Columns->Clear();
	for( int i = 0; i < ADB_FIELDTYPE_END; i++)
	{
		if(!(GetFieldAssigns((eADBFieldType)i) & ADB_FIELDASSIGN_TABLE))
			continue;

		if(!(SelectedColumns & (1<<i)))
			continue;

		TColumn* col = DBGrid1->Columns->Add();
		col->FieldName = ADBFieldTypeToDBFieldName((eADBFieldType)i);
		col->Title->Caption = ADBFieldTypeToName((eADBFieldType)i);
		col->Visible = true;
	}
}

void TArchiveForm::updateUI()
{
	if(!isValid())
		return;

	updateItemCount();
	updateTable();

	bool bEnaBtns = DBGrid1->SelectedIndex!=-1;
	m_PrintBtn->Enabled = bEnaBtns;
	m_GraphBtn->Enabled = bEnaBtns;
}

void TArchiveForm::updateItemCount()
{
	if(!isValid())
		return;

	m_ComQuery->Close();
	m_ComQuery->SQL->Clear();
	m_ComQuery->SQL->Add(GetFilterQuery());
	for(unsigned int i = 0; i < filterQueryParameters.size(); i++)
	{
		TDateTime valDt = filterQueryParameters[i].value;

		m_ComQuery->Parameters->ParamByName(filterQueryParameters[i].name)->DataType = ftDateTime;
		m_ComQuery->Parameters->ParamByName(filterQueryParameters[i].name)->Value = valDt;

    }
	m_ComQuery->Open();

	tableItemCount = m_ComQuery->RecordCount;

    updatePageLabel();
}

void TArchiveForm::updatePageLabel()
{
	UnicodeString protoStr;
	switch(currProtocolType)
	{
		case ADB_PROTO_ALL:
			protoStr = "���� ����������  ";
			break;
		case ADB_PROTO_SCAN:
			protoStr = "���������� ��������  ";
			break;
		case ADB_PROTO_TEST:
			protoStr = "���������� ������������  ";
			break;
        case ADB_PROTO_ADJUSTING:
			protoStr = "���������� ���������  ";
			break;
        case ADB_PROTO_HANDSCAN:
			protoStr = "���������� �������� ������ ���  ";
			break;
	};

	UnicodeString searchStr;
	if(searchQueryString.Length())
		searchStr = "��������";
	else
		searchStr = "�� ��������";


	int curPageNumber = floor( float(curItemIndex) / float(maxDBGridRowCount) ) + 1;
	int totPages = std::max( ceil( float(tableItemCount) / float(maxDBGridRowCount) ), 1.0);

    m_PageLabel->Caption = StringFormatU(L"�������� %d/%d",curPageNumber,totPages);
    m_ProtocolLabel->Caption = StringFormatU(L"����������� %s",protoStr.c_str());
    m_FilterLabel->Caption = StringFormatU(L"������ ������ %s",searchStr.c_str());

    if(searchQueryString.Length())
        m_FilterLabel->Font->Style = TFontStyles() << fsBold;
    else
        m_FilterLabel->Font->Style = TFontStyles();

    if(currProtocolType != ADB_PROTO_ALL)
        m_ProtocolLabel->Font->Style = TFontStyles() << fsBold;
    else
        m_ProtocolLabel->Font->Style = TFontStyles();


	/*m_PageLabel->Caption = StringFormatU(L"�������� %d/%d\t����������� %s\t������ ������ %s",
								curPageNumber,//curItemIndex/maxDBGridRowCount + 1,
								totPages,//tableItemCount/(maxDBGridRowCount) + 1,
								protoStr.c_str(), searchStr.c_str());*/
}

void TArchiveForm::updateTable()
{
	if(!isValid())
		return;
	if(curItemIndex == -1)
		return;
	//if(tableItemCount == 0)
	//	return;
	int startIndex = curItemIndex;
	int endIndex = std::max(1, std::min(startIndex + maxDBGridRowCount, tableItemCount));
	int indexCount = std::max(1, endIndex - startIndex);

	//if(indexCount <= 0)
	//	return;

	UnicodeString fieldName;
	if((PreviousColumnIndex!=-1) && (PreviousColumnIndex < DBGrid1->Columns->Count))
	{
		if((*DBGrid1->Columns)[PreviousColumnIndex]->Field)
			fieldName = (*DBGrid1->Columns)[PreviousColumnIndex]->Field->FieldName;
		else
			fieldName = "ID";
    }
	else
		fieldName = "ID";

	//Shitcode warning! For select in some range witn sorting (MS Access only!) need to create three nested SELECT query
	// -> SELECT * FROM ( SELECT TOP #Count FROM (SELECT TOP #EndIndex FROM Reports ORDER BY ... #sort_dir) ORDER BY ... #!sort_dir ) ORDER BY #sort_dir;
	UnicodeString query = StringFormatU(L"SELECT * FROM ( SELECT TOP %d * FROM ( SELECT TOP %d * FROM (%s) ORDER BY %s %s, ID %s) t ORDER BY %s %s, ID %s) t1 ORDER BY %s %s, ID %s",
							indexCount, endIndex, GetFilterQuery().c_str(),
							fieldName.c_str(), bColSortOrderAsc ? L"ASC" : L"DESC",bColSortOrderAsc ? L"ASC" : L"DESC",
							fieldName.c_str(), bColSortOrderAsc ? L"DESC" : L"ASC",bColSortOrderAsc ? L"DESC" : L"ASC",
							fieldName.c_str(), bColSortOrderAsc ? L"ASC" : L"DESC",bColSortOrderAsc ? L"ASC" : L"DESC");

	/*UnicodeString query = StringFormatU(L"%s",
							GetFilterQuery().c_str());*/
	/*UnicodeString query = StringFormatU(L"%s ORDER BY %s %s",
							GetFilterQuery().c_str(), fieldName.c_str(),
							bColSortOrderAsc ? L"ASC" : L"DESC",fieldName.c_str());
	*/

	m_ShowQuery->Close();
	//m_ShowQuery->CommandText = query;
	//m_ShowQuery->Active = true;
	m_ShowQuery->SQL->Clear();
	m_ShowQuery->SQL->Add(query);
	for(unsigned int i = 0; i < filterQueryParameters.size(); i++)
	{
		TDateTime valDt = filterQueryParameters[i].value;

		m_ShowQuery->Parameters->ParamByName(filterQueryParameters[i].name)->DataType = ftDateTime;
		m_ShowQuery->Parameters->ParamByName(filterQueryParameters[i].name)->Value = valDt;

    }
	m_ShowQuery->Open();

	int t = m_ShowQuery->RecordCount;



	DBGrid1->DataSource=m_ShowDataSource;
	DBGrid1->Refresh();
	adjustTableColumnsWidth();
}

void __fastcall TArchiveForm::DBGrid1TitleClick(TColumn *Column)
{
	if(!isValid())
		return;
	if(PreviousColumnIndex!=-1)
	{
		if(PreviousColumnIndex!=Column->Index)
			bColSortOrderAsc = true;
		else
			bColSortOrderAsc = !bColSortOrderAsc;

		(*DBGrid1->Columns)[PreviousColumnIndex]->Title->Font->Style = TFontStyles();
	}
	Column->Title->Font->Style = TFontStyles()<<fsBold;
	PreviousColumnIndex = Column->Index;

	updateTable();
}
//---------------------------------------------------------------------------

void TArchiveForm::updateFileList()  //Scan folder and store all filenames, ends with .acd
{
	if(!isValid())
		return;

	TSearchRec sr;
	UnicodeString path = GetCurrentDir() + "\\*.acd";
	int iAttributes = faAnyFile;
	int nestingLevel = 1;

	fileList.clear();

	if (FindFirst(path, iAttributes, sr) == 0)
	{
		do
		{
			if(sr.Attr & faDirectory)
				continue;
			if(sr.Name.SubString(sr.Name.Length()-3,4) == L".acd")
			{
				fileList.push_back(sr.Name);
            }

		} while (FindNext(sr) == 0);
		FindClose(sr);
	}
}

bool TArchiveForm::readFileToDB(UnicodeString& str)
{
	if(!isValid())
		return false;

    sFileHeaderVersion_05 fileHeader;
	if(!AutoconMain->Rep->ReadFromFile(str,true,&fileHeader))
		return false;


	sJointTestingReportHeader_Version_06 header = AutoconMain->Rep->Header;

	UnicodeString query = "INSERT INTO Reports (FileName, FileType_idx, FileType_name, GangNumber, \
ReportNumber, PletNumber, JointNumber, TopStraightness, SideStraightness, Hardness, Plaint, \
PlantNumber, VerSoft, Operator, DefectCode, ValidityGroup, Conclusion, ReportDateTime, \
HasBScan, HasHScanImages, HasHScanBScan, HasLinearity, HasCameraImages)\r\n";


	query += StringFormatU(L"VALUES(\'%s\', %d, \'%s\',  %d, ",
					str.c_str(),header.FileType,ADBFileTypeIdxToFileTypeName(header.FileType), header.GangNumber);

	query += StringFormatU(L"%d, %d, %d, %d, %d, %d, ",
					header.ReportNumber, header.PletNumber, header.JointNumber,
					header.TopStraightness_, header.WF_Straightness_, header.Hardness); //FDV

	query += StringFormatU(L"\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', :DateTimeParam,",
					header.Plaint.c_str(), header.PlantNumber.c_str(), header.VerSoft.c_str(), header.Operator.c_str(),
					header.DefectCode.c_str(), header.ValidityGroup.c_str(), header.�onclusion.c_str()/*, DateTime.c_str()*/);


    query +=  StringFormatU(L"\'%c\', \'%c\', \'%c\', \'%c\', \'%c\' );",
                    ((AutoconMain->Rep->Header.FileType == 1) || (AutoconMain->Rep->Header.FileType == 2)) ?
                                                                        'X' : ' ',
                    (fileHeader.HandScanScreenShotCount != 0) ?         'X' : ' ',
                    (fileHeader.HandDataItemsCount) ?                   'X' : ' ',
                    (fileHeader.ScreenShotDataSize) ?                   'X' : ' ',
                    (fileHeader.FotoDataSize) ?                         'X' : ' ');

	m_ComQuery->Close();
	m_ComQuery->SQL->Clear();
	m_ComQuery->SQL->Add(query);

//#ifndef _DEBUG
    try {
	    m_ComQuery->Parameters->ParamByName("DateTimeParam")->Value = header.DateTime;
    }
    catch(EOleException* ex)
    {
        m_ComQuery->Parameters->ParamByName("DateTimeParam")->Value = TDateTime(0);
    }
//#else
    //m_ComQuery->Parameters->ParamByName("DateTimeParam")->Value = header.DateTime;//TDateTime(0);
//#endif

	m_ComQuery->ExecSQL();

	return true;
}

void __fastcall TArchiveForm::m_UpdateBtnClick(TObject *Sender)
{
	if(!isValid())
		return;

	tableItemCount = 0;
	curItemIndex = 0;

	//Updating array for existing files
	updateFileList();

	//Getting filenames from database
	m_ComQuery->Close();
	m_ComQuery->SQL->Clear();
	m_ComQuery->SQL->Add("SELECT ID,FileName FROM Reports");
	m_ComQuery->Open();


	//-----Filling array, that contains id from missing (deleted) files----
	std::vector<UnicodeString> idToDelete;
	m_ComQuery->First();
	while(!m_ComQuery->Eof)
	{
		UnicodeString value = m_ComQuery->FieldByName("FileName")->AsString;
		UnicodeString id = m_ComQuery->FieldByName("ID")->AsString;

		if(!FileExists(value,true))
			idToDelete.push_back(id);

		m_ComQuery->Next();
	}
	//----------------------------------------------------------------------


	//----Determining the presence of the file in the database---------------
	//---------And if not - reading file header, and add to database---------
	UnicodeString statusStr;
	std::vector<UnicodeString> filesToReadList;
	for(unsigned int i=0;i<fileList.size();i++)
	{
		bool bFileExists = false;

		//Find file in database
        m_ComQuery->First();
		while(!m_ComQuery->Eof)
		{
			UnicodeString value = m_ComQuery->FieldByName("FileName")->AsString;
			UnicodeString id = m_ComQuery->FieldByName("ID")->AsString;

			if(fileList[i] == value)
			{
				bFileExists = true;
				break;
			}
			m_ComQuery->Next();
		}

		if(!bFileExists)//No file in DB
		{
			filesToReadList.push_back(fileList[i]);
		}

		statusStr = "���� ";
		statusStr+= fileList[i] + "...";
		uiSetStatus(statusStr);
		uiSetProgress((i*100)/fileList.size());
        updateProgressUI();
	}
	uiSetProgress(100);
    updateProgressUI();
    //--------------------------------------------------------------------------


	//---------------Reading marked files to database---------------------------
	for(unsigned int i = 0; i < filesToReadList.size(); i++)
	{
		statusStr = "������ ";
		statusStr+= fileList[i] + "...";
		uiSetStatus(statusStr);
		uiSetProgress((i*100)/filesToReadList.size());
        updateProgressUI();

		if(!readFileToDB(filesToReadList[i]))
        {

        }
	}
	uiSetStatus("������ ������ ���������!");
    updateProgressUI();
	//--------------------------------------------------------------------------
	/////////////^FROM HERE^ - COM QUERY CLOSED (modifyed in readFileToDB)////////

	//---------------Remove missing records from db-----------------------------
	uiSetStatus("�������� ������������ �������...");
	for(unsigned int i = 0; i < idToDelete.size(); i++)
	{
		m_ComQuery->Close();
		m_ComQuery->SQL->Clear();
		m_ComQuery->SQL->Add(UnicodeString("DELETE * FROM Reports WHERE ID = ") + idToDelete[i]);
		m_ComQuery->ExecSQL();

		uiSetProgress((i*100)/idToDelete.size());
        updateProgressUI();
	}
	uiSetStatus("�������� ������������ ������� ���������!");
	uiSetProgress(100);
	//--------------------------------------------------------------------------

    uiSetStatus("���������� ���� ������ ���������!");
	uiSetProgress(100);

    updateProgressUI();
	updateUI();
}
//---------------------------------------------------------------------------

void __fastcall TArchiveForm::m_ClearBtnClick(TObject *Sender)
{
	if(!isValid())
		return;

	m_ComQuery->Close();
	m_ComQuery->SQL->Clear();
	m_ComQuery->SQL->Add("DELETE * FROM Reports");
	//m_ComQuery->CommandText = "DELETE * FROM Reports";
	m_ComQuery->ExecSQL();

	tableItemCount = 0;
	curItemIndex = 0;

	updateTable();
}
//---------------------------------------------------------------------------



void __fastcall TArchiveForm::FormResize(TObject *Sender)
{
	if(!isValid())
		return;

	int rowHeight = 28;//abs(DBGrid1->Font->Height) + 7;
	int titleHeight = 28;//abs(DBGrid1->TitleFont->Height) + 6 + 8;
	int scrollHeight = 17;
	maxDBGridRowCount = (DBGrid1->Height-(titleHeight+scrollHeight))/rowHeight;
	maxDBGridRowCount = std::max(1,maxDBGridRowCount);

	updateUI();
}
//---------------------------------------------------------------------------

void __fastcall TArchiveForm::m_UpBtnClick(TObject *Sender)
{
	if(!isValid())
		return;
	updateItemCount();

	if(curItemIndex >= maxDBGridRowCount)
		curItemIndex-=maxDBGridRowCount;

	updateUI();
}
//---------------------------------------------------------------------------

void __fastcall TArchiveForm::m_DownBtnClick(TObject *Sender)
{
	if(!isValid())
		return;
	updateItemCount();

	if(curItemIndex < (tableItemCount-maxDBGridRowCount))
		curItemIndex+=maxDBGridRowCount;

	updateUI();
}
//---------------------------------------------------------------------------

UnicodeString TArchiveForm::GetFilterQuery()
{
	if(filterQueryString.Length()==0)
    	updateFilterQuery();
	return filterQueryString;
}

void TArchiveForm::updateFilterQuery()
{
	switch(currProtocolType)
	{
		case ADB_PROTO_SCAN:
			protocolQueryConditions = " FileType_idx = 1 ";
			break;
		case ADB_PROTO_TEST:
			protocolQueryConditions = " FileType_idx = 2 ";
			break;
        case ADB_PROTO_ADJUSTING:
			protocolQueryConditions = " FileType_idx = 3 ";
			break;
		case ADB_PROTO_HANDSCAN:
			protocolQueryConditions = " FileType_idx = 4 ";
			break;
		default:
			protocolQueryConditions = "";
    }

	UnicodeString query = "SELECT * FROM Reports";

	if( ( searchQueryString.Length() ) || ( protocolQueryConditions.Length() ))
		query += " WHERE ";

	if(protocolQueryConditions.Length())
	{
		query += protocolQueryConditions;

		if(searchQueryString.Length())
			query += " AND ";
	}

	filterQueryParameters.clear();
	if(searchQueryString.Length())
	{
		query += searchQueryString;
		for(unsigned int i = 0; i < searchQueryParameters.size(); i++)
		{
			filterQueryParameters.push_back(searchQueryParameters[i]);
        }
    }

	filterQueryString = query;
}

TDateTime MakeEndOfDay(TDateTime dt)
{
    WORD currYear, currDay, currMonth;
    DecodeDate(dt, currYear, currMonth, currDay);

    return EncodeDate( currYear, currMonth, currDay) + EncodeTime(23, 59, 59, 59);
}

TDateTime MakeBeginOfDay(TDateTime dt)
{
    WORD currYear, currDay, currMonth;
    DecodeDate(dt, currYear, currMonth, currDay);

    return EncodeDate( currYear, currMonth, currDay) + EncodeTime(0, 0, 0, 0);
}

TDateTime MakeCorrectDateTime(TDateTime dt, eAFDOpType opType)
{
    switch(opType)
    {
        case AFD_OPTYPE_LESS:   return MakeBeginOfDay(dt);
        case AFD_OPTYPE_GREAT:  return MakeEndOfDay(dt);
        case AFD_OPTYPE_LESS_EQUAL: return MakeEndOfDay(dt);
        case AFD_OPTYPE_GREAT_EQUAL: return MakeBeginOfDay(dt);
        case AFD_OPTYPE_EQUAL:
        case AFD_OPTYPE_NOTEQUAL:
        default:;
    };
    return dt;
}


void __fastcall TArchiveForm::m_FindBtnClick(TObject *Sender)
{
	if(!isValid())
		return;

    int val = ArchiveFilterForm->ShowModal();
    if(val == mrOk)
	{
        std::vector<sAFDFilterQuery> query_arr;
        ArchiveFilterForm->getQueries(query_arr);

        searchQueryString = "";
		searchQueryParameters.clear();

        for(unsigned int i = 0; i < query_arr.size(); i++)
        {
            sAFDFilterQuery& query = query_arr[i];

            UnicodeString dbFieldName = ADBFieldTypeToDBFieldName(query.fieldType);
            UnicodeString opType1 = AFDOpTypeToDBOp(query.opType1);
            UnicodeString opType2 = AFDOpTypeToDBOp(query.opType2);
            UnicodeString booleanOp = (i==0) ? "" : "AND";

            if(query.fieldType != ADB_FIELDTYPE_DATETIME)
            {
                if(query.bTwoOp)
                {
                    searchQueryString += StringFormatU(L"%s ((%s %s %s) AND (%s %s %s)) ",booleanOp.c_str(),
                                dbFieldName.c_str(),opType1.c_str(),
                                ADBFieldValTypeToDBFieldVal(query.fieldType,query.findValue1).c_str(),
                                dbFieldName.c_str(),opType2.c_str(),
                                ADBFieldValTypeToDBFieldVal(query.fieldType,query.findValue2).c_str());
                }
                else
                {
                    searchQueryString += StringFormatU(L"%s (%s %s %s) ",booleanOp.c_str(),
                                dbFieldName.c_str(),opType1.c_str(),
                                ADBFieldValTypeToDBFieldVal(query.fieldType,query.findValue1).c_str());
                }
            }
            else
            {
                if(query.bTwoOp)
                {
                    UnicodeString paramName1 = StringFormatU(L"SQP%d",i*2);
                    UnicodeString paramName2 = StringFormatU(L"SQP%d",i*2+1);
                    TDateTime dateTime1 = MakeCorrectDateTime(TDateTime(query.findValue1),query.opType1);
                    TDateTime dateTime2 = MakeCorrectDateTime(TDateTime(query.findValue2),query.opType2);

                    searchQueryString += StringFormatU(L"%s ((%s %s %s) AND (%s %s %s)) ",booleanOp.c_str(),
                                dbFieldName.c_str(),opType1.c_str(),
                                (UnicodeString(":") + paramName1).c_str(),
                                dbFieldName.c_str(),opType2.c_str(),
                                (UnicodeString(":") + paramName2).c_str());

                    UnicodeString temp1 = DateTimeToStr(dateTime1);
                    UnicodeString temp2 = DateTimeToStr(dateTime2);

                    searchQueryParameters.push_back( sFilterQueryParameter(paramName1, dateTime1) );
                    searchQueryParameters.push_back( sFilterQueryParameter(paramName2, dateTime2) );
                }
                else
                {
                    TDateTime dateTime1 = TDateTime(query.findValue1);
                    UnicodeString paramName = StringFormatU(L"SQP%d",i*2);
                    searchQueryString += StringFormatU(L"%s (%s %s %s) ",booleanOp.c_str(),
                                dbFieldName.c_str(),opType1.c_str(),
                                (UnicodeString(":") + paramName).c_str());
                    searchQueryParameters.push_back( sFilterQueryParameter(paramName, dateTime1) );
                }


            }
        }
    }

	updateFilterQuery();

	curItemIndex=0;
	updatePageLabel();
	updateUI();
}
//---------------------------------------------------------------------------

void __fastcall TArchiveForm::m_GraphBtnClick(TObject *Sender)
{
    if(!isValid())
		return;

	if(DBGrid1->DataSource->DataSet->RecNo == -1)
		return;

	UnicodeString selectedPath = DBGrid1->DataSource->DataSet->FieldByName("FileName")->AsString;

	if(AutoconMain->Rep->ReadFromFile(selectedPath, false))
	{
        ReportViewerForm->SetArchiveForm(this,selectedPath);
        ReportViewerForm->Show();
	}
	else
	{
		MessageBoxA(NULL,"������ ������ �����!","������!", MB_OK | MB_ICONWARNING);
	}
}
//---------------------------------------------------------------------------

bool TArchiveForm::isValid()
{
	return !bBdNotFound;
}

void __fastcall TArchiveForm::m_PropertiesBtnClick(TObject *Sender)
{
	//Load current columns to dialog
	ArchiveSettingsDial->SetSelectedColumns( SelectedColumns );
	if( ArchiveSettingsDial->ShowModal() == mrOk )
	{
		//Update current column array from dialog
		SelectedColumns = ArchiveSettingsDial->GetSelectedColumns();

		//Set selected columns to table
		updateSelectedColumns();
		updateUI();
	}
}
//---------------------------------------------------------------------------

void TArchiveForm::adjustTableColumnsWidth()
{
	const int DEFBORDER = 10;

	int* colWidth = new int[ DBGrid1->Columns->Count ];
	memset(colWidth,0,sizeof(int)*DBGrid1->Columns->Count);
	TFont* oldFont = Canvas->Font;

	//Getting column title width
	Canvas->Font = DBGrid1->TitleFont;
	for( int i = 0; i < DBGrid1->Columns->Count; i++)
	{
		colWidth[i] = Canvas->TextWidth((*DBGrid1->Columns)[i]->Title->Caption) + DEFBORDER;
	}

	//Getting column elements width
	Canvas->Font = DBGrid1->Font;
	DBGrid1->DataSource->DataSet->First();
	while(!DBGrid1->DataSource->DataSet->Eof)
	{
		for( int i = 0; i < DBGrid1->Columns->Count; i++)
		{
			int fieldWidth =
					DBGrid1->Canvas->TextWidth(Trim((*DBGrid1->Columns)[i]->Field->DisplayText)) + DEFBORDER;
			colWidth[i] = std::max(colWidth[i],fieldWidth);
		}
		DBGrid1->DataSource->DataSet->Next();
	}
	DBGrid1->DataSource->DataSet->First();

	for( int i = 0; i < DBGrid1->Columns->Count; i++)
	{
		if(colWidth[i] > 0)
			(*DBGrid1->Columns)[i]->Width = colWidth[i];
	}

	Canvas->Font = oldFont;
    delete [] colWidth;
}

int TArchiveForm::ShowSelectModeDial()
{
    ArchiveSelectDial->AddComponent(ADB_PROTO_ALL, "��� ���������");
	ArchiveSelectDial->AddComponent(ADB_PROTO_SCAN, "��������� ��������");
	ArchiveSelectDial->AddComponent(ADB_PROTO_TEST, "��������� ������������");
	ArchiveSelectDial->AddComponent(ADB_PROTO_HANDSCAN, "��������� ������ ���");

    ArchiveSelectDial->SetDialCaption("�������� ��� �������:");

    if(ArchiveSelectDial->ShowSelect() != -1)
        return ArchiveSelectDial->GetSelectedComponentId();
    return -1;
}

void TArchiveForm::SetProtocolMode(int mode)
{
    currProtocolType = mode;
}

void __fastcall TArchiveForm::m_ModeBtnClick(TObject *Sender)
{
    int selMode = ShowSelectModeDial();

    if(selMode != -1)
	{
		currProtocolType = selMode;
		updateFilterQuery();
		updatePageLabel();
		updateUI();
	}
}
//---------------------------------------------------------------------------

int TArchiveForm::ShowSelectThDial()
{
    TPoint oldButtonSize = ArchiveSelectDial->buttonSize;
    ArchiveSelectDial->buttonSize = TPoint(100,80);

    ArchiveSelectDial->AddComponent(0, "����");
    ArchiveSelectDial->AddComponent(1, "-12 ��");
	ArchiveSelectDial->AddComponent(2, "-10 ��");
	ArchiveSelectDial->AddComponent(3, "-8 ��");
    ArchiveSelectDial->AddComponent(4, "-6 ��");
    ArchiveSelectDial->AddComponent(5, "-4 ��");
    ArchiveSelectDial->AddComponent(6, "-2 ��");
    ArchiveSelectDial->AddComponent(7, "0 ��");
    ArchiveSelectDial->AddComponent(8, "2 ��");
    ArchiveSelectDial->AddComponent(9, "4 ��");
    ArchiveSelectDial->AddComponent(10, "6 ��");
    ArchiveSelectDial->AddComponent(11, "8 ��");
    ArchiveSelectDial->AddComponent(12, "10 ��");
    ArchiveSelectDial->AddComponent(13, "12 ��");
    ArchiveSelectDial->AddComponent(14, "14 ��");
    ArchiveSelectDial->AddComponent(15, "16 ��");
    ArchiveSelectDial->AddComponent(16, "18 ��");

    ArchiveSelectDial->SetDialCaption("�������� ����� �-���������:");
	int selectedButtonId = ArchiveSelectDial->ShowSelect();

    ArchiveSelectDial->buttonSize = oldButtonSize;

	return selectedButtonId;
}

int TArchiveForm::ShowSelectPrintDocTypeDial()
{
    	//Show print select dialog
    ArchiveSelectDial->AddComponent(0, "������ ��������� ����");
	ArchiveSelectDial->AddComponent(1, "������");
	ArchiveSelectDial->AddComponent(2, "��������");

    ArchiveSelectDial->SetDialCaption("�������� ��� ���������:");
	int selectedButtonId = ArchiveSelectDial->ShowSelect();

	return selectedButtonId;
}

int TArchiveForm::ShowSelectPrintChanTableDial()
{
    	//Show print select dialog
    ArchiveSelectDial->AddComponent(1, "��");
	ArchiveSelectDial->AddComponent(0, "���");

    ArchiveSelectDial->SetDialCaption("���������� ��������� �������:");
	int selectedButtonId = ArchiveSelectDial->ShowSelect();

	return selectedButtonId;
}

int TArchiveForm::ShowSelectPrintLinearityTableDial()
{
    //Show print select dialog
    ArchiveSelectDial->AddComponent(1, "��");
	ArchiveSelectDial->AddComponent(0, "���");

    ArchiveSelectDial->SetDialCaption("���������� ��������� ����������:");
	int selectedButtonId = ArchiveSelectDial->ShowSelect();

	return selectedButtonId;
}

int TArchiveForm::ShowSelectPrintDial()
{
	//Save bitmap
	TBitmap* pSaveBitmap = new TBitmap();
	pSaveBitmap->Transparent = true;
	pSaveBitmap->TransparentColor = clWhite;
	pSaveBitmap->TransparentMode = tmAuto;
	pSaveBitmap->AlphaFormat = afDefined;
	pSaveBitmap->PixelFormat = pf32bit;
	pSaveBitmap->SetSize(ImageList2->Width,ImageList2->Height);
	pSaveBitmap->Canvas->FillRect(TRect(0,0,ImageList2->Width,ImageList2->Height));
	ImageList2->GetBitmap(1, pSaveBitmap);

	//Print bitmap
	TBitmap* pPrintBitmap = new TBitmap();
	pPrintBitmap->Transparent = true;
	pPrintBitmap->TransparentColor = clWhite;
	pPrintBitmap->TransparentMode = tmAuto;
	pPrintBitmap->AlphaFormat = afDefined;
	pPrintBitmap->PixelFormat = pf32bit;
	pPrintBitmap->SetSize(ImageList2->Width,ImageList2->Height);
	pPrintBitmap->Canvas->FillRect(TRect(0,0,ImageList2->Width,ImageList2->Height));
	ImageList2->GetBitmap(0, pPrintBitmap);

	//View bitmap
	TBitmap* pViewBitmap = new TBitmap();
	pViewBitmap->Transparent = true;
	pViewBitmap->TransparentColor = clWhite;
	pViewBitmap->TransparentMode = tmAuto;
	pViewBitmap->AlphaFormat = afDefined;
	pViewBitmap->PixelFormat = pf32bit;
	pViewBitmap->SetSize(ImageList2->Width,ImageList2->Height);
	pViewBitmap->Canvas->FillRect(TRect(0,0,ImageList2->Width,ImageList2->Height));
	ImageList2->GetBitmap(2, pViewBitmap);


	//Show print select dialog
	ArchiveSelectDial->AddComponent(0, "�����������",pPrintBitmap);
	ArchiveSelectDial->AddComponent(1, "��������",pViewBitmap);
	ArchiveSelectDial->AddComponent(2, "���������",pSaveBitmap);
	//ArchiveSelectDial->AddComponent(-1, "�������", pExitBitmap);

    ArchiveSelectDial->SetDialCaption("�������� ����� ����������:");
	int selectedButtonId = ArchiveSelectDial->ShowSelect();
	//delete pExitBitmap;
	delete pPrintBitmap;
	delete pViewBitmap;
	delete pSaveBitmap;

	return selectedButtonId;
}

bool GetDriveLetters(TStrings* pStrings, UINT DriveFilterMask)
{
	wchar_t disks[256];
	wchar_t *disk;
	DWORD sizebuf=256;
	int size = GetLogicalDriveStringsW(sizebuf, disks);
	if(size == 0)
		return false;
	disk=disks;
	while (*disk)
	{
		UnicodeString str = StrPas(disk);
		UINT DriveType = GetDriveType(str.c_str());
		if((1<<DriveType) & DriveFilterMask)
			pStrings->AddObject(str,(TObject*)DriveType);

		disk=disk+wcslen(disk)+1;
	}
	return true;
}

int TArchiveForm::ShowSelectExportDial(TStringList * drives)
{
	const int iconWidth = 32;

	TSHFileInfo fileInfo;
	memset( &fileInfo, 0, sizeof(fileInfo));

	//Get directory icon
	SHGetFileInfo(
	_T("Doesn't matter"),
	FILE_ATTRIBUTE_DIRECTORY,
	&fileInfo, sizeof(fileInfo),
	SHGFI_ICON | SHGFI_LARGEICON | SHGFI_USEFILEATTRIBUTES);

	std::vector<TBitmap*> bitmapsToDelete;

	TBitmap* pDirBitmap = new TBitmap();
	bitmapsToDelete.push_back(pDirBitmap);
	pDirBitmap->SetSize(iconWidth,iconWidth);
	pDirBitmap->Transparent = true;
	pDirBitmap->TransparentColor = clWhite;
	pDirBitmap->TransparentMode = tmAuto;
	pDirBitmap->Canvas->FillRect(TRect(0,0,iconWidth,iconWidth));
	DrawIconEx(pDirBitmap->Canvas->Handle,0,0,fileInfo.hIcon,iconWidth,iconWidth,0,0,DI_NORMAL);
	ArchiveSelectDial->AddComponent(0, "������� �����", pDirBitmap);

	UINT DriveComponentId = 1;
	for(int i = 0; i < drives->Count; i++)
	{
		UINT type = (UINT)drives->Objects[i];

		SHGetFileInfo(drives->Strings[i].c_str(), 0, &fileInfo, sizeof(fileInfo),
					SHGFI_SYSICONINDEX | SHGFI_LARGEICON | SHGFI_ICON | SHGFI_DISPLAYNAME);

		TBitmap* pBitmap = new TBitmap();
		bitmapsToDelete.push_back(pBitmap);
		pBitmap->SetSize(iconWidth,iconWidth);
		pBitmap->Transparent = true;
		pBitmap->TransparentColor = clWhite;
		pBitmap->TransparentMode = tmAuto;
		pBitmap->Canvas->FillRect(TRect(0,0,iconWidth,iconWidth));
		DrawIconEx(pBitmap->Canvas->Handle,0,0,fileInfo.hIcon,iconWidth,iconWidth,0,0,DI_NORMAL);

		ArchiveSelectDial->AddComponent(DriveComponentId++, fileInfo.szDisplayName, pBitmap);
	}

	//ArchiveSelectDial->AddComponent(-1, "�������", pExitBitmap);
    ArchiveSelectDial->SetDialCaption("");
	int selectedButtonId = ArchiveSelectDial->ShowSelect();
	for(unsigned int i = 0; i < bitmapsToDelete.size(); i++)
		delete bitmapsToDelete[i];
	bitmapsToDelete.clear();

	return selectedButtonId;
}

bool TArchiveForm::SaveProtocolAsReport(UnicodeString selectedPath, unsigned int ThIndex)
{
    if(!AutoconMain->Rep->ReadFromFile(selectedPath, false))
	{
		MessageBoxA(NULL,"������ ������ �����!","������!", MB_OK | MB_ICONWARNING);
		return false;
	}
	//MainForm->UpdatePBoxData(true);

    //Init pathes
    PathMgr::PushPath(L"$pdf_print", L"resources\\archive\\print");

	//UnicodeString pdf_dir = PathMgr::GetPath("($pdf_print)");
	UnicodeString outp_path = PathMgr::GetPath(L"($pdf_print)\\out.pdf");

	const int iconWidth = 64;

    bool bTitlePage, bBScanShort, bBScanDetail, bHScan, bChannelsTable, bLinearityTable;
    bTitlePage = bBScanShort = bBScanDetail = bHScan = bChannelsTable = bLinearityTable = false;

    if(AutoconMain->Rep->Header.FileType == 4)
    {
        bTitlePage = bHScan = true;
    }
    else
    {
        int docType = ShowSelectPrintDocTypeDial();
        if(docType == -1) //exit
		    return false;
        switch(docType)
        {
            case 0: //Title only
                bTitlePage = true;
                break;
            case 1: //Short
                bTitlePage = true;
                bBScanShort = true;
                bHScan = true;
                break;
            case 2: //Detail
                bTitlePage = true;
                bBScanDetail = true;
                bHScan = true;
                break;
        }
    }

    bChannelsTable = ShowSelectPrintChanTableDial();

    if(AutoconMain->Rep->PhoneScreenShot || AutoconMain->Rep->PhoneScreenShot2 || AutoconMain->Rep->PhoneScreenShot3)
        bLinearityTable = ShowSelectPrintLinearityTableDial();

	int selId = ShowSelectPrintDial();

	if(selId == -1) //exit
		return false;

	cArchivePDFCreator pdfCreator;
	pdfCreator.save(AnsiString(outp_path).c_str(),ThIndex, bTitlePage,bBScanShort,bBScanDetail,bHScan,bChannelsTable,bLinearityTable);

    TDateTime curDate = TDateTime::CurrentDateTime();

	if(selId == 0) //print
	{
		UnicodeString print_path = PathMgr::GetPath(L"($pdf_print)\\doc_print.pdf");
		CopyFileW(outp_path.c_str(), print_path.c_str(),false);
		ShellExecuteW(Handle,L"print",print_path.c_str(),NULL,NULL,SW_RESTORE);
	}
	else if(selId == 1) //show
	{
		UnicodeString show_copy_path = PathMgr::GetPath(L"($pdf_print)\\doc_show_copy.pdf");
		CopyFileW(outp_path.c_str(), show_copy_path.c_str(),false);
		ShellExecuteW(NULL, L"open", show_copy_path.c_str(), NULL, NULL, SW_SHOW);
	}
	else if(selId == 2) //save
	{
		TStringList * strings = new TStringList();
		if( !GetDriveLetters( strings , 1<<DRIVE_REMOVABLE ) )
		{
			delete strings;
			return false;
		}

		int saveSelId = ShowSelectExportDial(strings);


        UnicodeString fname = UnicodeString("report_")+selectedPath/*curDate.FormatString("dd-mm-yyyy_hh-mm-ss")*/ + L".pdf";

		if(saveSelId==-1)//close
			return false;
		if(saveSelId==0)//select folder
		{
			UnicodeString dir = GetCurrentDir();
			if(SelectDirectory("",System::WideString(),dir))
			{
				CopyFileW(outp_path.c_str(), (dir + "\\" + fname).c_str(),false);
			}
		}
		else
		{
			int DriveComponentId = saveSelId - 1;
			assert((strings->Count > DriveComponentId) && (DriveComponentId >= 0));
			CopyFileW(outp_path.c_str(), (strings->Strings[DriveComponentId] + fname).c_str(),false);
		}
    }
    return true;
}

void __fastcall TArchiveForm::m_PrintBtnClick(TObject *Sender)
{
    if(!isValid())
		return;

	if(DBGrid1->DataSource->DataSet->RecNo == -1)
		return;

    int Th = ShowSelectThDial();
    if(Th == -1)
        return;

	UnicodeString selectedPath = DBGrid1->DataSource->DataSet->FieldByName("FileName")->AsString;
    SaveProtocolAsReport(selectedPath,Th);
}
//---------------------------------------------------------------------------

void __fastcall TArchiveForm::m_CloseBtnClick(TObject *Sender)
{
    AutoconMain->Rep->ClearData();
	Visible = false;
}
//---------------------------------------------------------------------------

void __fastcall TArchiveForm::m_ExportBtnClick(TObject *Sender)
{
	TStringList * strings = new TStringList();
	if( !GetDriveLetters( strings , 1<<DRIVE_REMOVABLE ) )
	{
		delete strings;
		return;
	}

	int selectedButtonId = ShowSelectExportDial(strings);

	if(selectedButtonId==-1)//close
		return;
	if(selectedButtonId==0)//select folder
	{
		UnicodeString dir = GetCurrentDir();
		//if(SelectDirectory(dir,TSelectDirOpts() << sdAllowCreate << sdPerformCreate << sdPrompt,0))
		if(SelectDirectory("",System::WideString(),dir))
		{
			exportFiles(dir);
		}
	}
	else
	{
		int DriveComponentId = selectedButtonId - 1;
		assert((strings->Count > DriveComponentId) && (DriveComponentId >= 0));

		exportFiles(strings->Strings[DriveComponentId]);
	}

	delete strings;
}

void TArchiveForm::exportFiles(UnicodeString dir)
{
	TDateTime dt = TDateTime::CurrentDateTime();
	UnicodeString saveDir = StringFormatU(L"%s\\ACR_%s\\",dir,dt.FormatString("dd_mm_yyyy_hh_mm_ss").c_str());
	UnicodeString currDir = GetCurrentDir() + L"\\";
	CreateDir(saveDir);

    //Getting filenames from database
	updateItemCount();

	m_ComQuery->First();
	float fieldCount = m_ComQuery->FieldCount;
	float cnt = 0;
	bool bErr = false;
	while(!m_ComQuery->Eof)
	{
		UnicodeString srcFilePath = m_ComQuery->FieldByName("FileName")->AsString;
		UnicodeString dstFilePath = saveDir + srcFilePath;
		srcFilePath = currDir + srcFilePath;

		if(!CopyFileW(srcFilePath.c_str(),dstFilePath.c_str(),false))
		{
			int errcode = GetLastError();
			UnicodeString errstr = StringFormatU(L"������ ����������� ����� [%d] \"%s\" -> \"%s\"",
					GetLastError(),srcFilePath.c_str(),dstFilePath.c_str());

			MessageBox(NULL, errstr.c_str(),
					L"������ ����������� �����",MB_OK | MB_ICONWARNING);
			uiSetStatus(errstr);
			bErr = true;
			break;
		}

		uiSetStatus(dstFilePath);
		uiSetProgress((cnt*100.f)/fieldCount);

		m_ComQuery->Next();
		cnt++;
	}
	m_ComQuery->First();

	if(!bErr)
	{
		UnicodeString str = StringFormatU(L"������� � %s ��������!",saveDir.c_str());
		uiSetStatus(str);
	}
	uiSetProgress(100);
}
//---------------------------------------------------------------------------


void __fastcall TArchiveForm::FormDestroy(TObject *Sender)
{
	/*if(filterQueryParameters)
	{
		delete filterQueryParameters;
		filterQueryParameters = NULL;
	}*/
}
//---------------------------------------------------------------------------

void __fastcall TArchiveForm::FormCreate(TObject *Sender)
{
	//filterQueryParameters = new TParameters();
}
//---------------------------------------------------------------------------

void __fastcall TArchiveForm::DBGrid1ColumnMoved(TObject *Sender, int FromIndex, int ToIndex)
{
    PreviousColumnIndex = -1;
}
//---------------------------------------------------------------------------

void __fastcall TArchiveForm::DBGrid1DblClick(TObject *Sender)
{
    m_GraphBtn->Click();
}
//---------------------------------------------------------------------------

void TArchiveForm::setProgressUIValues()
{
    if(bUISetStatus)
    {
        m_StatusLabel->Caption = uiStatusString;
        bUISetStatus = false;
    }

    if(bUISetProgress)
    {
        m_ProgressBar->Min = 0;
	    m_ProgressBar->Max = 100;
	    m_ProgressBar->Position = uiProgressVal;
        bUISetProgress = false;
    }
}

void TArchiveForm::updateProgressUI()
{
    DWORD currTime = GetTickCount();
    if((currTime - PrevUIUpdateTime) > 300)
    {
        UpdateUITimer->Enabled = false;
        setProgressUIValues();
        PrevUIUpdateTime = currTime;
    }
    else
    {
        if(!UpdateUITimer->Enabled)
            UpdateUITimer->Enabled = true;
    }
}

void __fastcall TArchiveForm::UpdateUITimerTimer(TObject *Sender)
{
    setProgressUIValues();
    PrevUIUpdateTime = GetTickCount();
    UpdateUITimer->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TArchiveForm::m_SelectBtnClick(TObject *Sender)
{
    if(m_SelectBtn->Down)
    {
        DBGrid1->Options = DBGrid1->Options << dgMultiSelect;
    }
    else
    {
        DBGrid1->Options = DBGrid1->Options >> dgMultiSelect;
        DBGrid1->SelectedRows->Clear();
    }
}
//---------------------------------------------------------------------------

void __fastcall TArchiveForm::DBGrid1CellClick(TColumn *Column)
{
    if(!m_SelectBtn->Down)
        return;

    DBGrid1->DataSource->DataSet->DisableControls();
    if(DBGrid1->DataSource->DataSet->RecNo == -1)
        return;
    DBGrid1->DataSource->DataSet->EnableControls();
}
//---------------------------------------------------------------------------

void __fastcall TArchiveForm::m_CompareBtnClick(TObject *Sender)
{
//    if(DBGrid1->SelectedRows->Count != 2)
//        return;
//
//    UnicodeString paths[2];
//
//    DBGrid1->DataSource->DataSet->GotoBookmark(DBGrid1->SelectedRows->Items[0]);
//    paths[0] = DBGrid1->DataSource->DataSet->FieldByName("FileName")->AsString;
//    DBGrid1->DataSource->DataSet->GotoBookmark(DBGrid1->SelectedRows->Items[1]);
//    paths[1] = DBGrid1->DataSource->DataSet->FieldByName("FileName")->AsString;
//
//    TRACE("Selecting: %s and %s",AnsiString(paths[0]).c_str(), AnsiString(paths[1]).c_str());
//
//    cJointTestingReport rep1(ScanLen);
//    cJointTestingReport rep2(ScanLen);
//
//    if(!rep1.ReadFromFile(paths[0], false))
//	{
//        MessageBoxA(NULL,"������ ������ ����� 1!","������!", MB_OK | MB_ICONWARNING);
//	}
//
//    if(!rep2.ReadFromFile(paths[1], false))
//	{
//        MessageBoxA(NULL,"������ ������ ����� 2!","������!", MB_OK | MB_ICONWARNING);
//	}
//
//    rep1.GetSignalDifference(AutoconMain->Rep, &rep2, 32,32,32,0);
//    ReportViewerForm->SetArchiveForm(this,"");
//    ReportViewerForm->Show();
}
//---------------------------------------------------------------------------

