//---------------------------------------------------------------------------

//�������� ���� �������� �����
//#pragma hdrstop


#include "Autocon.inc"
#include "DrawRailUnit.h"
//---------------------------------------------------------------------------
//#pragma package(smart_init)

const int cDrawRailUnit::KP_TO_RAIL[] = {RAIL_HGL_S1|RAIL_HGL_S2|RAIL_HGL_S4,//KP1
												RAIL_HGL_S1,	//KP2
												RAIL_HGL_S1,    //KP3
												RAIL_HGL_S2,    //KP4
												RAIL_HGL_S2,    //KP5
												RAIL_HGL_S3,    //KP6
												RAIL_HGL_S5,    //KP7
												RAIL_HGL_S4|RAIL_HGL_S2|RAIL_HGL_S1//KP8
												};


cDrawRailUnit::cDrawRailUnit()
{
	Buffer = new TBitmap();
	bNeedUpdateFlag=true;
	fontHeight = 1;

	for(int i = 0; i < 14; i++)
	{
        colors[i] = clWhite;
	}
	for(int i = 13; i < 32; i++)
	{
        colors[i] = clBlack;
	}

	for(int i=0;i<5;i++)
		for(int j=0;j<3;j++)
			RailImages[i][j] = new TPicture();

	bImagesLoaded = false;
	bRailColorFromKPColor = true;
	bOrientationLeftToRight = true;
	bNeedUpdateSize = true;

#ifdef ENABLE_KP_ADDITIONAL_DATA
    KPText[0][0] = "1";
    KPText[0][1] = "2";
    KPText[0][2] = "3";

    KPText[3][0] = "1";
    KPText[3][1] = "2";
    KPText[3][2] = "3";

    KPText[4][0] = "1";
    KPText[4][1] = "2";
    KPText[4][2] = "3";

    KPText[5][0] = "3";
    KPText[5][1] = "2";
    KPText[5][2] = "1";

    KPText[6][0] = "1";
    KPText[6][1] = "2";
    KPText[6][2] = "3";

    KPText[7][0] = "3";
    KPText[7][1] = "2";
    KPText[7][2] = "1";
#endif
}
cDrawRailUnit::~cDrawRailUnit()
{
	if(Buffer)
	{
		delete Buffer;
		Buffer = NULL;
	}

	for(int i=0;i<5;i++)
		for(int j=0;j<3;j++)
			delete RailImages[i][j];
}

void cDrawRailUnit::_updateKPSizes(unsigned int width, unsigned int height)
{
	if( (lastCalculatedBufferSize.x == (int)width) &&
			(lastCalculatedBufferSize.y == (int)height) && (!bNeedUpdateSize))
	{
		return;
	}
	bNeedUpdateSize = false;

	lastCalculatedBufferSize = TPoint(width,height);
	//-----------------------------------------------------

	float imgCaretH = 0.4;
	float imgCaretW = (imgCaretH / 3.f) * 2.f;

	TRect riBoundRect(0,0,
			RailImages[2][0]->Width + RailImages[3][0]->Width + RailImages[4][0]->Width,
			RailImages[0][0]->Height + RailImages[1][0]->Height + RailImages[3][0]->Height);

	int caretHeight = float(riBoundRect.Width()) * imgCaretH;
	int caretWidth = float(riBoundRect.Width()) * imgCaretW;

	//-----------------------------------------------------
	SRects[0] = TRect(-RailImages[0][0]->Width/2,
						0,
						RailImages[0][0]->Width/2,
						RailImages[0][0]->Height);
	SRects[1] = TRect(-RailImages[1][0]->Width/2,
						SRects[0].bottom,
						RailImages[1][0]->Width/2,
						SRects[0].bottom + RailImages[1][0]->Height);

	SRects[3] = TRect(-RailImages[3][0]->Width/2,
						SRects[1].bottom,
						RailImages[3][0]->Width/2,
						SRects[1].bottom + RailImages[3][0]->Height);

	SRects[2] = TRect(SRects[3].left - RailImages[2][0]->Width,
						SRects[3].top,
						SRects[3].left,
						SRects[3].bottom);

	SRects[4] = TRect(SRects[3].right,
						SRects[3].top,
						SRects[3].right + RailImages[4][0]->Width,
						SRects[3].bottom);

	for(int i = 0; i < 5; i++)
	{
		SRects[i].top+=caretHeight;
		SRects[i].bottom+=caretHeight;
    }
	//--------------------------------------------------


	//
	KPRects[0].init(SRects[0].CenterPoint().x - caretWidth/2,
					SRects[0].top - caretHeight,
					SRects[0].CenterPoint().x + caretWidth/2,
					SRects[0].top);
	//
	KPRects[1].init(SRects[0].left - caretHeight,
					SRects[0].CenterPoint().y - caretWidth/2,
					SRects[0].left,
					SRects[0].CenterPoint().y + caretWidth/2);
	KPRects[2].init(SRects[0].right,
					SRects[0].CenterPoint().y - caretWidth/2,
					SRects[0].right + caretHeight,
					SRects[0].CenterPoint().y + caretWidth/2);
	//
	KPRects[3].init(SRects[1].left - caretHeight + SRects[1].Width()/4,
					SRects[1].CenterPoint().y - caretWidth/2,
					SRects[1].left + SRects[1].Width()/4,
					SRects[1].CenterPoint().y + caretWidth/2);
	KPRects[4].init(SRects[1].right - SRects[1].Width()/4,
					SRects[1].CenterPoint().y - caretWidth/2,
					SRects[1].right + caretHeight - SRects[1].Width()/4,
					SRects[1].CenterPoint().y + caretWidth/2);
	//
	KPRects[5].init(SRects[2].CenterPoint().x - caretWidth/2,
					SRects[2].bottom,
					SRects[2].CenterPoint().x + caretWidth/2,
					SRects[2].bottom + caretHeight);
	KPRects[6].init(SRects[4].CenterPoint().x - caretWidth/2,
					SRects[4].bottom,
					SRects[4].CenterPoint().x + caretWidth/2,
					SRects[4].bottom + caretHeight);
	//
	KPRects[7].init(SRects[3].CenterPoint().x - caretWidth/2,
					SRects[3].bottom,
					SRects[3].CenterPoint().x + caretWidth/2,
					SRects[3].bottom + caretHeight);

	TRect boundRect(0x0FFFFFFF,0x0FFFFFFF,0xFFFFFFFF,0xFFFFFFFF);
	for(int i = 0; i < 5; i++)  //SRects
	{
		boundRect.left = std::min(boundRect.left,SRects[i].left);
		boundRect.top = std::min(boundRect.top,SRects[i].top);
		boundRect.right = std::max(boundRect.right,SRects[i].right);
		boundRect.bottom = std::max(boundRect.bottom,SRects[i].bottom);
	}
	for(int i = 0; i < 8; i++)  //KPRects
	{
		boundRect.left = std::min(boundRect.left,KPRects[i].left);
		boundRect.top = std::min(boundRect.top,KPRects[i].top);
		boundRect.right = std::max(boundRect.right,KPRects[i].right);
		boundRect.bottom = std::max(boundRect.bottom,KPRects[i].bottom);
	}

	float railScaleCoeff = std::min(float(width) / float(boundRect.Width()),
										float(height) / float(boundRect.Height()));
	caretHeight*=railScaleCoeff;
	caretWidth*=railScaleCoeff;

	float fontWidth = (float(caretHeight)/3.f)*2.f;
	fontHeight = ((float(caretWidth)/3.f)/2.f)*3.f;

	for(int i = 0; i < 5; i++)
	{
		SRects[i].left *= railScaleCoeff;
		SRects[i].top *= railScaleCoeff;
		SRects[i].right *= railScaleCoeff;
		SRects[i].bottom *= railScaleCoeff;

		SRects[i].left += width/2;
		SRects[i].right += width/2;
	}
	for(int i = 0; i < 8; i++)
	{
		KPRects[i].left *= railScaleCoeff;
		KPRects[i].top *= railScaleCoeff;
		KPRects[i].right *= railScaleCoeff;
		KPRects[i].bottom *= railScaleCoeff;

		KPRects[i].left += width/2;
		KPRects[i].right += width/2;
	}

    int verOffset = int(height/2) - (SRects[1].CenterPoint().y);
    if(verOffset > 0)
    {
        for(int i = 0; i < 5; i++)
            SRects[i].Offset(0,verOffset);
        for(int i = 0; i < 8; i++)
            KPRects[i].Offset(0,verOffset);
    }

	if(!bOrientationLeftToRight)
	{
		TRect tmpRect;
		//swap 2 and 3
		tmpRect = KPRects[1];
		KPRects[1] = KPRects[2];
		KPRects[2] = tmpRect;

		//swap 4 and 5
		tmpRect = KPRects[3];
		KPRects[3] = KPRects[4];
		KPRects[4] = tmpRect;

		//swap 6 and 7
		tmpRect = KPRects[5];
		KPRects[5] = KPRects[6];
		KPRects[6] = tmpRect;

		//swap 3 and 5
		tmpRect = SRects[2];
		SRects[2] = SRects[4];
		SRects[4] = tmpRect;
	}
}

void cDrawRailUnit::setOrientation( bool bLeftToRight )
{
	if(bLeftToRight == bOrientationLeftToRight)
		return;
	bOrientationLeftToRight = bLeftToRight;
	bNeedUpdateSize = true;

	//swap rail images
	TPicture* tmpPict;
	for(int i=0; i<3; i++)
	{
		tmpPict = RailImages[2][i];
		RailImages[2][i] = RailImages[4][i];
		RailImages[4][i] = tmpPict;
	}

	update(true);
}

void cDrawRailUnit::resize(unsigned int width, unsigned int height)
{
	Buffer->FreeImage();
	Buffer->SetSize(width, height);
	Buffer->Transparent = true;
	Buffer->TransparentColor = FILL_COLOR;
	Buffer->TransparentMode = tmFixed;

	loadRailImages();

	update(true);
}

void cDrawRailUnit::scaleToBuffer(TBitmap* pDrawBuffer, bool bDrawHardpoints, DWORD UnvisibleFlags )
{
	draw(Buffer,bDrawHardpoints,UnvisibleFlags);
	pDrawBuffer->Canvas->StretchDraw(TRect(0,0,pDrawBuffer->Width,pDrawBuffer->Height), Buffer);
	bNeedUpdateFlag = true;
}

void cDrawRailUnit::draw(TBitmap* pDrawBuffer, bool bDrawHardpoints, DWORD UnvisibleFlags)
{
	if(!pDrawBuffer)
		return;
	if((pDrawBuffer->Width <=0) || (pDrawBuffer->Height <= 0))
		return;

	pDrawBuffer->Canvas->Brush->Color = FILL_COLOR;
	pDrawBuffer->Canvas->Brush->Style = bsSolid;

	pDrawBuffer->Canvas->FillRect(TRect(0,0,pDrawBuffer->Width,pDrawBuffer->Height));

	_updateKPSizes(pDrawBuffer->Width,pDrawBuffer->Height);


	pDrawBuffer->Canvas->Brush->Style = bsSolid;
	pDrawBuffer->Canvas->Pen->Color = clBlack;
	pDrawBuffer->Canvas->Pen->Width = 1;
	pDrawBuffer->Canvas->Pen->Style = psSolid;

	TTextFormat TextFormat;
	TextFormat.Clear();
	TextFormat << tfCenter << tfVerticalCenter <<  tfSingleLine;

	for(int i = 0; i < 8; i++) //for all carets
	{
		if(UnvisibleFlags & (1<<i))
			continue;

		TColor innerCol = TColor(colors[i]);
		if(innerCol==FILL_COLOR) //If color is equal to OpacityColor (from canvas) then we change it
			innerCol = ((UINT)innerCol)-1;
		TColor outerCol = TColor(colors[5+8+i]);
		if(outerCol==FILL_COLOR) //If color is equal to OpacityColor (from canvas) then we change it
			outerCol = TColor(((UINT)outerCol)-1);

		pDrawBuffer->Canvas->Brush->Color = innerCol;
        pDrawBuffer->Canvas->Brush->Style = bsSolid;
		pDrawBuffer->Canvas->Font->Height = - fontHeight;

		if(outerCol != clBlack)
		{
			pDrawBuffer->Canvas->Pen->Width = 3;
			pDrawBuffer->Canvas->Pen->Color = outerCol;
		}

        //Calculate rectangle rount amount for current KP size
		int round = float(std::max(KPRects[i].Width(),KPRects[i].Height())) * 0.2f;

		pDrawBuffer->Canvas->RoundRect(KPRects[i].left,KPRects[i].top,KPRects[i].right,KPRects[i].bottom,round,round);

		pDrawBuffer->Canvas->Pen->Width = 1;
		pDrawBuffer->Canvas->Pen->Color = clBlack;

		UnicodeString str = StringFormatU(L"��%d",i + 1);
		TRect r = KPRects[i];
		r.Inflate(-2,-2);
		pDrawBuffer->Canvas->TextRect(r,str,TextFormat);

#ifdef ENABLE_KP_ADDITIONAL_DATA
        bool bKpTextHor = false;
        bool bEnableKpText = false;
        TRect KpTextRect;
        const int KpTextWidth = 15;

        switch(i)
        {
            case 0:
                bEnableKpText = true;
                bKpTextHor = true;
                KpTextRect = TRect(KPRects[i].left,KPRects[i].bottom-KpTextWidth, KPRects[i].right, KPRects[i].bottom);
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                bEnableKpText = true;
                bKpTextHor = false;
                KpTextRect = TRect(KPRects[i].right-KpTextWidth, KPRects[i].top, KPRects[i].right, KPRects[i].bottom);
                break;
            case 4:
                bEnableKpText = true;
                bKpTextHor = false;
                KpTextRect = TRect(KPRects[i].left, KPRects[i].top, KPRects[i].left+KpTextWidth, KPRects[i].bottom);
                break;
            case 5:
            case 6:
            case 7:
                bEnableKpText = true;
                bKpTextHor = true;
                KpTextRect = TRect(KPRects[i].left, KPRects[i].top, KPRects[i].right, KPRects[i].top+KpTextWidth);
                break;
        }

        if(bEnableKpText)
        {
            pDrawBuffer->Canvas->Font->Height = -10;
            pDrawBuffer->Canvas->Brush->Style = bsClear;
            TRect textRect = KpTextRect;
            for(int j = 0; j < 3; j++)
            {
                if(bKpTextHor)
                {
                    textRect.left = KpTextRect.left + KpTextRect.Width() * j / 3;
                    textRect.right = textRect.left + KpTextRect.Width()/3;
                }
                else
                {
                    textRect.top = KpTextRect.top + KpTextRect.Height() * j / 3;
                    textRect.bottom = textRect.top + KpTextRect.Height()/3;
                }

                pDrawBuffer->Canvas->TextRect(textRect,KPText[i][j],TextFormat);
            }
        }
#endif //ENABLE_KP_ADDITIONAL_DATA


	}

	if(bRailColorFromKPColor) //Calculating rail color from KP colors (by segments, example: on kp is red - all segment red)
	{
		for(unsigned int i = 0; i < 5; i++)
		{
			colors[i+8] = clWhite;
		}

		for(unsigned int kpInd = 0; kpInd < 8; kpInd++)
		{
			for(unsigned int railInd=0;railInd<5;railInd++)
			{
				if(KP_TO_RAIL[kpInd]&(1<<(railInd+8)))
				{
					TColor railCol = TColor(colors[railInd+8]);
					TColor kpCol = TColor(colors[kpInd]);

					int railColPriority = 0;
					switch(railCol)
					{
						case clRed: railColPriority = 2; break;
						case clLime: railColPriority = 1; break;
						case clGreen: railColPriority = 1; break;
						case clWhite: railColPriority = 0; break;
						case clGray: railColPriority = 0; break;
					}

					int kpColPriority = 0;
					switch(kpCol)
					{
						case clRed: kpColPriority = 2; break;
						case clLime: kpColPriority = 1; break;
						case clGreen: kpColPriority = 1; break;
						case clWhite: kpColPriority = 0; break;
						case clGray: kpColPriority = 0; break;
					}

					if(kpColPriority>railColPriority)
						colors[railInd+8] = kpCol;

				}
			}
        }
	}

	for(unsigned int i = 0; i < 5; i++) //Draw rail segments
	{
		if(UnvisibleFlags & (1<<(i + 8)))
			continue;

		TColor col = TColor(colors[i+8]);
		int imageIndex = 0;
		switch(col) //Calculate rail image index for color
		{
			case clRed: imageIndex = 2; break;
			case clGreen: imageIndex = 1; break;
			case clLime: imageIndex = 1; break;
			case clWhite: imageIndex = 0; break;
			case clGray: imageIndex = 0; break;
		}

		pDrawBuffer->Canvas->StretchDraw(SRects[i], RailImages[i][imageIndex]->Graphic);
	}

	//Draw hardpoint circles
	if(bDrawHardpoints) //Draw hardpoint circles from precalculated hardpoint positions
	{
		for(unsigned int i=0;i<8;i++)
		{
            if(UnvisibleFlags & (1<<i))
				continue;

			TColor outerCol = TColor(colors[5+8+i]);

			TPoint pos = OptimalHardpoints[i] - _RailOffset;
			pDrawBuffer->Canvas->Brush->Color = outerCol;
			pDrawBuffer->Canvas->Pen->Color = outerCol;
			pDrawBuffer->Canvas->Ellipse(pos.x - 4,pos.y - 4, pos.x + 4, pos.y + 4);
		}
	}
}

void cDrawRailUnit::update(bool bForceUpdate)
{
	bNeedUpdateFlag |= bForceUpdate;

	if(!bNeedUpdateFlag)
		return;

    draw(Buffer);

	bNeedUpdateFlag = false;
}

void cDrawRailUnit::setColor(unsigned int mask, unsigned int color)
{
	for(unsigned int i = 0; i < 32; i++)
	{
		if( (1<<i) & mask)
			if(colors[i] != color)
			{
				bNeedUpdateFlag = true;
				colors[i] = color;
            }
	}
}

void cDrawRailUnit::setColorByIndex(unsigned int index, unsigned int color)
{
	if(index >= 32)
		return;
	colors[index] = color;
}
unsigned int cDrawRailUnit::getColorByIndex( unsigned int index)
{
	if(index >= 32)
		return 0;
	return colors[index];
}

TPoint cDrawRailUnit::getKPPoint(unsigned int kp_index)
{
	if(kp_index>=8)
		return TPoint();
	return KPRects[kp_index].CenterPoint();
}

bool LineIntersection(TPoint start1, TPoint end1, TPoint start2, TPoint end2, TPoint *out_intersection)
{
	TPoint dir1 = end1 - start1;
	TPoint dir2 = end2 - start2;

	//������� ��������� ������ ���������� ����� �������
	float a1 = -dir1.y;
	float b1 = +dir1.x;
	float d1 = -(a1*float(start1.x) + b1*float(start1.y));

	float a2 = -dir2.y;
	float b2 = +dir2.x;
	float d2 = -(a2*float(start2.x) + b2*float(start2.y));

	//����������� ����� ��������, ��� ��������� � ����� ������������� ���
	float seg1_line2_start = a2*float(start1.x) + b2*float(start1.y) + d2;
	float seg1_line2_end = a2*float(end1.x) + b2*float(end1.y) + d2;

	float seg2_line1_start = a1*float(start2.x) + b1*float(start2.y) + d1;
	float seg2_line1_end = a1*float(end2.x) + b1*float(end2.y) + d1;

	//���� ����� ������ ������� ����� ���� ����, ������ �� � ����� ������������� � ����������� ���.
	if (seg1_line2_start * seg1_line2_end >= 0 || seg2_line1_start * seg2_line1_end >= 0)
		return false;

	double u = double(seg1_line2_start) / double(seg1_line2_start - seg1_line2_end);
	double t1 = u*double(dir1.X);
	double t2 = u*double(dir1.Y);

	TPoint pt = TPoint(t1,t2);
	*out_intersection =  start1 + pt;

	return true;
}

bool LineRectIntersects(TRect rectangle, float ray_posX, float ray_posY,float ray_dirX, float ray_dirY, float* pResult)
{
    float num = 0.f;
    float maxValue = 3.40282347E+38F;
    if (fabs(ray_dirX) < 1E-06f)
    {
        if ((ray_posY < rectangle.Left) || (ray_dirX > rectangle.Right))
        {
            (*pResult) = 0;
            return false;
        }
    }
    else
    {
        float num11 = 1.f / ray_dirX;
        float num8 = (rectangle.Left - ray_posX) * num11;
        float num7 = (rectangle.Right - ray_posX) * num11;
        if (num8 > num7)
        {
            float num14 = num8;
            num8 = num7;
            num7 = num14;
        }
        num = std::max(num8, num);
        maxValue = std::min(num7, maxValue);
        if (num > maxValue)
        {
            (*pResult) = 0;
            return false;
        }
    }
    if (fabs(ray_dirY) < 1E-06f)
    {
        if ((ray_posY < rectangle.Top) || (ray_posY > rectangle.Bottom))
        {
            (*pResult) = 0;
            return false;
        }
    }
    else
    {
        float num10 = 1.f / ray_dirY;
        float num6 = (rectangle.Top - ray_posY) * num10;
        float num5 = (rectangle.Bottom - ray_posY) * num10;
        if (num6 > num5)
        {
            float num13 = num6;
            num6 = num5;
            num5 = num13;
        }
        num = std::max(num6, num);
        maxValue = std::min(num5, maxValue);
        if (num > maxValue)
        {
            (*pResult) = 0;
            return false;
        }
    }

    (*pResult) = num;
    return true;
}

void cDrawRailUnit::calculateOptimalHardpoint(unsigned int kp_index,
												TPoint RailOffset,
												TPoint RectPoint,
												TPoint RectNormal,
												bool bNoCorners)
{
	_RailOffset = RailOffset;

	TPoint KPHardpoints[8];
	KPHardpoints[0] = TPoint(KPRects[kp_index].Left,KPRects[kp_index].Top) + RailOffset;
	KPHardpoints[1] = TPoint(KPRects[kp_index].CenterPoint().x,KPRects[kp_index].Top) + RailOffset;
	KPHardpoints[2] = TPoint(KPRects[kp_index].Right,KPRects[kp_index].Top) + RailOffset;
	KPHardpoints[3] = TPoint(KPRects[kp_index].Right,KPRects[kp_index].CenterPoint().y) + RailOffset;
	KPHardpoints[4] = TPoint(KPRects[kp_index].Right,KPRects[kp_index].Bottom) + RailOffset;
	KPHardpoints[5] = TPoint(KPRects[kp_index].CenterPoint().x,KPRects[kp_index].Bottom) + RailOffset;
	KPHardpoints[6] = TPoint(KPRects[kp_index].Left,KPRects[kp_index].Bottom) + RailOffset;
	KPHardpoints[7] = TPoint(KPRects[kp_index].Left,KPRects[kp_index].CenterPoint().y) + RailOffset;

	float KPNormals[8][2];// = {{-1,-1}, {0,-1}, {1,-1}, {1,0}, {1,1}, {0,1}, {-1,1}, {-1,0}};
	KPNormals[0][0] = -1;	KPNormals[0][1] = -1;
	KPNormals[1][0] = 0; 	KPNormals[1][1] = -1;
	KPNormals[2][0] = 1; 	KPNormals[2][1] = -1;
	KPNormals[3][0] = 1; 	KPNormals[3][1] = 0;
	KPNormals[4][0] = 1; 	KPNormals[4][1] = 1;
	KPNormals[5][0] = 0; 	KPNormals[5][1] = 1;
	KPNormals[6][0] = -1; 	KPNormals[6][1] = 1;
	KPNormals[7][0] = -1; 	KPNormals[7][1] = 0;

	const float AngularRestrictions[] = {0.75,
										0,
										0.75,
										0,
										0.75,
										0,
										0.75,
										0};

#define NORMALIZE_FLT2(x) fInvLen = 1.f/sqrt(x[0]*x[0]+x[1]*x[1]); \
							x[0] *= fInvLen; x[1] *= fInvLen
	float fInvLen = 0;
	//Normalize normals
	for(int i=0;i<8;i++)
	{
		NORMALIZE_FLT2( KPNormals[i] );
	}

	float KPHardpointCoeffs[8];
	for(int i=0;i<8;i++)
	{
		float Vec[2];
		//Vector from KP to rect
		Vec[0] = RectPoint.x - KPHardpoints[i].x + 0.001f;
		Vec[1] = RectPoint.y - KPHardpoints[i].y + 0.001f;

		float distance = sqrtf(Vec[0]*Vec[0]+Vec[1]*Vec[1]);

		NORMALIZE_FLT2( Vec );

		//dot product - angle between kp normal and vector [-1..1]
		float angle1 = KPNormals[i][0]*Vec[0] + KPNormals[i][1]*Vec[1];
		if((angle1<AngularRestrictions[i]) || (bNoCorners && (i%2==0)))
		{
			KPHardpointCoeffs[i] =  distance;
			continue;
        }
		angle1 = std::max(0.f,angle1);//[x...1]

		//Vector from rect to KP
		Vec[0] = KPHardpoints[i].x - RectPoint.x;
		Vec[1] = KPHardpoints[i].y - RectPoint.y;

		NORMALIZE_FLT2( Vec );

		//dot product - angle between rect normal and vector [-1..1]
		float angle2 = float(RectNormal.x)*Vec[0] + float(RectNormal.y)*Vec[1];
		angle2 = std::max(0.f,angle2);//[0...1]

		//Calculating angle coeff: less - better
		float angleCoeff1 = 1.f - angle1;
		float angleCoeff2 = 1.f - angle2;

        float intersectCoeff = 1;
        for(unsigned int j = 0; j < 8; j++)
        {
            if(kp_index == j)
                continue;

            float res;
            if(LineRectIntersects( KPRects[j], KPHardpoints[i].x,KPHardpoints[i].y,
                                    Vec[0],Vec[1],&res ))
            {
                if(res <= 1)
                    intersectCoeff+=50;
            }
        }


		KPHardpointCoeffs[i] =  distance * (angleCoeff1 + angleCoeff2)/2.f*intersectCoeff;
	}
#undef NORMALIZE_FLT2

	int betterPoint = -1;
	float maxCoeff = 1000000;

	//Search for better point
	for(int i = 0; i < 8; i++)
	{
		if(maxCoeff>KPHardpointCoeffs[i])
		{
			maxCoeff = KPHardpointCoeffs[i];
            betterPoint = i;
        }
	}

	if(betterPoint!=-1)
		OptimalHardpoints[kp_index] = KPHardpoints[betterPoint];
	else
		OptimalHardpoints[kp_index] = getKPPoint(kp_index);//failed

	BScanHardpoints[kp_index] = RectPoint;

	if(betterPoint==-1)
		return;
	//Calc midpoint
	TPoint intersectPoint;
	if(LineIntersection(OptimalHardpoints[kp_index],OptimalHardpoints[kp_index]+TPoint(KPNormals[betterPoint][0]*500,KPNormals[betterPoint][1]*500),
						BScanHardpoints[kp_index],BScanHardpoints[kp_index]+TPoint(RectNormal.x*500,RectNormal.y*500),&intersectPoint))
	{
		OptimalMidpoints[kp_index][0] = intersectPoint;
		OptimalMidpoints[kp_index][1] = intersectPoint;
	}
	else //failed
	{
		TPoint dir = OptimalHardpoints[kp_index] - BScanHardpoints[kp_index];
		float hDir, vDir;
		if(RectNormal.X==0)
		{
			hDir = abs(dir.y);
			vDir = dir.x;
		}
		else
		{
			hDir = abs(dir.x);
			vDir = dir.y;
		}

		OptimalMidpoints[kp_index][0] = TPoint(BScanHardpoints[kp_index].X+RectNormal.x*hDir/2,
												BScanHardpoints[kp_index].Y+RectNormal.y*hDir/2);
		OptimalMidpoints[kp_index][1] = TPoint(OptimalHardpoints[kp_index].X+KPNormals[betterPoint][0]*hDir/2,
												OptimalHardpoints[kp_index].Y+KPNormals[betterPoint][1]*hDir/2);

	}
}

TPoint cDrawRailUnit::getOptimalHardpoint(unsigned int kp_index)
{
	return OptimalHardpoints[kp_index];
}

TPoint cDrawRailUnit::getOptimalMidpoint(unsigned int kp_index, int ptIndex)
{
	return OptimalMidpoints[kp_index][ptIndex];
}

TPoint cDrawRailUnit::getBScanHardpoint(unsigned int kp_index)
{
	return BScanHardpoints[kp_index];
}

TBitmap* cDrawRailUnit::getBuffer()
{
	return Buffer;
}

void cDrawRailUnit::loadRailImages()
{
	if(bImagesLoaded)
		return;

	UnicodeString path;
	char cFragment[] = {'t','m','l','b','r'};
	char cColor[] = {'w','g','r'};

	for(int i=0; i<5;i++)
	{
		for(int j = 0; j < 3; j++)
		{
			path = StringFormatU(L"../resources/rail_slices/rail_%c%c.png",cFragment[i],cColor[j]);
			if(!FileExists(path,true)) //���� ���������� ������ ����, �� ������� ������
			{
				path = StringFormatU(L"resources/rail_slices/rail_%c%c.png",cFragment[i],cColor[j]);
				if(!FileExists(path,true)) //���� ���������� ������ - ������� ��������� � ������� ������ ������
				{
					MessageBox(NULL,StringFormatU(L"Can't load resource: %s (%d/%d)!",path.c_str(),j+i*3,5*3).c_str(),
									L"Warning",MB_OK | MB_ICONWARNING);

					//���� �� �������� �������� ������ ��������
					RailImages[i][j]->Bitmap->SetSize(5,5);
					continue;
				}
			}

			RailImages[i][j]->LoadFromFile(path);
		}
	}

	bImagesLoaded = true;
}

int cDrawRailUnit::TestPoint(TPoint pt)
{
	for(int i = 0; i < 8; i++)
	{
		if (KPRects[i].PtInRect(pt))
        {
            return i;
        }
	}
    return - 1;
}

TColor cDrawRailUnit::getHardpointColor(unsigned int kp_index)
{
	return colors[kp_index+5+8];
}
