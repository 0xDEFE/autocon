//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AutoconMain.h"
extern cAutoconMain *AutoconMain;

#include "AdjustingFormUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TAdjustingForm *AdjustingForm;
//---------------------------------------------------------------------------
__fastcall TAdjustingForm::TAdjustingForm(TComponent* Owner)
    : TForm(Owner)
{
    charts[0] = NULL;
    charts[1] = m_AscanChartKP1;
    charts[2] = m_AscanChartKP2;
    charts[3] = m_AscanChartKP3;
    charts[4] = m_AscanChartKP4;
    charts[5] = m_AscanChartKP5;
    charts[6] = m_AscanChartKP6;
    charts[7] = m_AscanChartKP7;
    charts[8] = m_AscanChartKP8;

    chartSerieses[0] = NULL;
    chartSerieses[1] = Series1;
    chartSerieses[2] = AScanSeries2;
    chartSerieses[3] = LineSeries2;
    chartSerieses[4] = LineSeries3;
    chartSerieses[5] = LineSeries4;
    chartSerieses[6] = LineSeries5;
    chartSerieses[7] = LineSeries7;
    chartSerieses[8] = LineSeries6;

    chartColors[0] = clBlack;
    chartColors[1] = clLime;
    chartColors[2] = clFuchsia;
    chartColors[3] = clBlue;
    chartColors[4] = clRed;
    chartColors[5] = RGB(255,127,39);
    chartColors[6] = clGreen;
    chartColors[7] = RGB(194,47,221);
    chartColors[8] = clAqua;


    NLabels[0].pLbl = NULL;          HLabels[0].pLbl = NULL;          KuLabels[0].pLbl = NULL;
    NLabels[1].pLbl = m_NLabel_kp1;  HLabels[1].pLbl = m_HLabel_kp1;  KuLabels[1].pLbl = m_KuLabel_kp1;
    NLabels[2].pLbl = m_NLabel_kp2;  HLabels[2].pLbl = m_HLabel_kp2;  KuLabels[2].pLbl = m_KuLabel_kp2;
    NLabels[3].pLbl = m_NLabel_kp3;  HLabels[3].pLbl = m_HLabel_kp3;  KuLabels[3].pLbl = m_KuLabel_kp3;
    NLabels[4].pLbl = m_NLabel_kp4;  HLabels[4].pLbl = m_HLabel_kp4;  KuLabels[4].pLbl = m_KuLabel_kp4;
    NLabels[5].pLbl = m_NLabel_kp5;  HLabels[5].pLbl = m_HLabel_kp5;  KuLabels[5].pLbl = m_KuLabel_kp5;
    NLabels[6].pLbl = m_NLabel_kp6;  HLabels[6].pLbl = m_HLabel_kp6;  KuLabels[6].pLbl = m_KuLabel_kp6;
    NLabels[7].pLbl = m_NLabel_kp7;  HLabels[7].pLbl = m_HLabel_kp7;  KuLabels[7].pLbl = m_KuLabel_kp7;
    NLabels[8].pLbl = m_NLabel_kp8;  HLabels[8].pLbl = m_HLabel_kp8;  KuLabels[8].pLbl = m_KuLabel_kp8;

    for(int i = 1; i < 9; i++)
    {
        NLabels[i].prefix = "N=";
        NLabels[i].postfix = "��";

        HLabels[i].prefix = "H=";
        HLabels[i].postfix = "���";

        KuLabels[i].prefix = "��=";
        KuLabels[i].postfix = "��";
    }

    ChangeModeFlag = false;
    bLeftPanelExpanded = false;

    for(int i = 1; i < 9; i++)
    {
        chartSerieses[i]->Color = chartColors[i];
        channelStates[i] = -1;
    }
    channelStates[0] = false;

    currExpandedChart = -1;

    bAllChannelsDisabled = false;

     SelKPComboStrings[0] = L"���������";
     SelKPComboStrings[1] = L"��1, ��2, ��4";
     SelKPComboStrings[2] = L"��3, ��5, ��8";
     SelKPComboStrings[3] = L"��6, ��7";
}
//---------------------------------------------------------------------------

void TAdjustingForm::Initialize()
{
    adjState = ADJSTATE_NONE;
    newAdjState = ADJSTATE_INITIAL;
    AutoconMain->ac->SetAdjustMotion(AdjustingMotion_);

    painter.init(AutoconMain->Rep, BSL_ONLY_CONTROL_CHANNELS,BSVC_ONLY_CONTROL_CHANNELS);
    painter.setBScanRect(TRect(0,0,m_BScanPBox->Width,m_BScanPBox->Height));
	painter.applyBScanLayout();

    painter.railDraw.bRailColorFromKPColor = false;

    for(int i = 1; i < 9; i++)
    {
        painter.scanLines[i]->BtnChColor[0][0] = chartColors[i];
    }

    UpdatePBoxData(false);
}

void TAdjustingForm::UpdatePBoxData(bool NewItemFlag)
{
    painter.updateBScanPages(NewItemFlag);
}

void TAdjustingForm::DrawPBoxData()
{
    for (int idx = 1; idx < 9; idx++)
	{
		painter.drawBScanPage(idx,m_BScanPBox->Canvas);
	}
}
//---------------------------------------------------------------------------

void __fastcall TAdjustingForm::m_BScanPBoxPaint(TObject *Sender)
{
    DrawPBoxData();
}
void __fastcall TAdjustingForm::m_BScanPanelResize(TObject *Sender)
{
    painter.setBScanRect(TRect(0,0,m_BScanPBox->Width,m_BScanPBox->Height));
    UpdatePBoxData(false);
}
//---------------------------------------------------------------------------
void __fastcall TAdjustingForm::FormShow(TObject *Sender)
{
    //Initialize();
    adjState = ADJSTATE_NONE;
    AutoconMain->ac->SetAdjustMotion(AdjustingMotion_);

    m_DrawTimer->Enabled = true;
    m_UpdateTimer->Enabled = true;
    m_DateLabel->Caption = DateToStr(Now());

    //m_LeftPanel->Width = float(this->Width) * 0.333f;

    painter.clearBScan();
    UpdatePBoxData(false);

   // AutoconMain->ac->SetManualMode(true);
    //AutoconMain->ac->SetWorkCycle(3);


    AutoconMain->DEV->ResetPathEncoder();
    AutoconMain->DEV->SetChannelGroup(3);
    //AutoconMain->DEV->DisableAll();


    //AutoconMain->DEV->SetChannel(0x46);
	AutoconMain->DEV->Update(true);


    m_ActiveChannelsCombo->Items->Clear();
    m_ActiveChannelsCombo->Items->Add(SelKPComboStrings[0]);
    m_ActiveChannelsCombo->Items->Add(SelKPComboStrings[1]);
    m_ActiveChannelsCombo->ItemIndex = 1;

    m_AdjustingNextBtn->Enabled = false;
    m_ActiveChannelsCombo->Enabled = false;


}
void __fastcall TAdjustingForm::FormHide(TObject *Sender)
{
    m_DrawTimer->Enabled = false;
    m_UpdateTimer->Enabled = false;
    AutoconMain->ac->SetAdjustMotion(NULL);
    adjState = ADJSTATE_NONE;
    newAdjState = ADJSTATE_INITIAL;
}
//---------------------------------------------------------------------------
void __fastcall TAdjustingForm::m_DrawTimerTimer(TObject *Sender)
{
    if(!this->Visible)
        return;
    ValueToScreen();
}
//---------------------------------------------------------------------------

void TAdjustingForm::ValueToScreen()
{
    if(!this->Visible)
        return;

    AUTO_LOCK_WCS(*(AutoconMain->CS));

    PutBScanSignals();

    /*for(int i = 1; i < 9; i++)
    {
        AScanFrames[i]->PutBScanSignals(RailCheckChannels[i]);
    }*/

    ringbuf<sPtrListItem>& Ptr_List = AutoconMain->devt->Ptr_List;
    while(!Ptr_List.empty())
    {
        Ptr_List.front().free();
        Ptr_List.pop_front();
    }


    UpdatePBoxData(true);
    DrawPBoxData();
}
void __fastcall TAdjustingForm::m_RailDrawPBoxPaint(TObject *Sender)
{
    painter.updateRail();
    painter.drawRail(0,true,m_RailDrawPBox->Canvas);
    /*
    m_RailDrawPBox->Canvas->Draw(0,
					0,
					painter.railDraw.getBuffer());*/
}
//---------------------------------------------------------------------------

void __fastcall TAdjustingForm::m_RailDrawPanelResize(TObject *Sender)
{
    if(currExpandedChart!=-1)
        return;

    TPoint railOffset(30,30);
    TPoint railSize = TPoint(m_RailDrawPanel->Width - railOffset.x*2,m_RailDrawPanel->Height - railOffset.y*2);

    painter.setRailRect(TRect(30,30,m_RailDrawPanel->Width-30,m_RailDrawPanel->Height-30));

    //---------Calculating optimal hardpoints---------

	TPoint RectPoint;
	TPoint RectNormal;

    for (int idx = 1; idx < 9; idx++)
	{
		switch(idx)
		{
			case 1: //KP1
				RectPoint = TPoint(m_RailDrawPanel->Width/2,0);
				RectNormal = TPoint(0,1);
				break;
			case 2: //KP2
				RectPoint = TPoint(0,0);
				RectNormal = TPoint(1,1);
				break;
			case 3: //KP3
				RectPoint = TPoint(m_RailDrawPanel->Width,0);
				RectNormal = TPoint(-1,1);
				break;
			case 4: //KP4
				RectPoint = TPoint(0,m_RailDrawPanel->Height/2);
				RectNormal = TPoint(1,0);
				break;
			case 5: //KP5
				RectPoint = TPoint(m_RailDrawPanel->Width,m_RailDrawPanel->Height/2);
				RectNormal = TPoint(-1,0);
				break;
			case 6: //KP6
				RectPoint = TPoint(0,m_RailDrawPanel->Height);
				RectNormal = TPoint(1,-1);
				break;
			case 7: //KP7
				RectPoint = TPoint(m_RailDrawPanel->Width,m_RailDrawPanel->Height);
				RectNormal = TPoint(-1,-1);
				break;
			case 8: //KP8
				RectPoint = TPoint(m_RailDrawPanel->Width/2,m_RailDrawPanel->Height);
				RectNormal = TPoint(0,-1);
				break;
		};

        painter.railDraw.calculateOptimalHardpoint(idx-1,railOffset,RectPoint, RectNormal);
    }
    painter.railDraw.update(true);


}
//---------------------------------------------------------------------------

void TAdjustingForm::PutBScanSignals()
{
    ringbuf<sPtrListItem>& Ptr_List = AutoconMain->devt->Ptr_List; //Getting link to ringbuffer (for ease of access)

    PtDEV_BScan2Head   DEV_BScan2Head_ptr = NULL;
	//PtUMU_BScanData    PBScanData_ptr = NULL;

    //const int ThValList[16] = { 8, 10, 12, 16, 20, 25, 32, 40, 50, 63, 80, 101, 127, 160, 201, 254};


    bool ChartIsFilled[9] = {0,0,0,0,0,0,0,0};

    for(int PtrListIdx = Ptr_List.size() - 1; PtrListIdx >= 0; PtrListIdx--)
    {
        sPtrListItem& Ptr_Item = Ptr_List[PtrListIdx];

        if(Ptr_Item.Type != edBScan2Data)
            continue;

        DEV_BScan2Head_ptr = Ptr_Item.Ptr1.pBScan2Head;
        //PBScanData_ptr = Ptr_Item.Ptr2.pBScanData;

        sChannelDescription tmp;
        unsigned char buff[768];
        unsigned int aidx2;
        int Index;

        for (unsigned int ItemIdx = 0; ItemIdx < DEV_BScan2Head_ptr->Items.size(); ItemIdx++)
        {
            int chartIndex = -1;
            int nChannel = DEV_BScan2Head_ptr->Items[ItemIdx].Channel;

            for(int i = 1; i < 9; i++)
            {
                if( RailCheckChannels[i] == nChannel)
                {
                    chartIndex = i;
                    break;
                }
            }

            if(chartIndex == -1)
                continue;

            if(ChartIsFilled[chartIndex])
                continue;
            ChartIsFilled[chartIndex] = true;


            chartSerieses[chartIndex]->Clear();
            memset(&(buff[0]), 0, 768);

            tUMU_BScanSignalList BScanDataPtr = *(DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr);

            for (int SignalIdx = 0; SignalIdx < DEV_BScan2Head_ptr->Items[ItemIdx].Count; SignalIdx++)
            {
                for (int AmplIdx = 0; AmplIdx < 24; AmplIdx++)
                {
                    Index = int(BScanDataPtr[SignalIdx].Delay) * 3 + (AmplIdx - 11);
                    aidx2 = AmplIdx;////////////////////
                    if ((Index >= 0) && (Index < 768)) buff[Index] = BScanDataPtr[SignalIdx].Ampl[aidx2];
                }
            }

            AutoconMain->Table->ItemByCID(nChannel, &tmp);
            charts[chartIndex]->BottomAxis->SetMinMax(0, tmp.cdScanDuration);

            chartSerieses[chartIndex]->Visible = true;

            int maxVal = -20;
            int maxPos = 0;
            for (int idx = 0; idx < 768; idx++)
                if (buff[idx] != 0)
                {
                    float val = 20.f * log10((float)buff[idx] / 32.f);
                    if(val > maxVal) // calc max
                    {
                        maxVal = val;
                        maxPos = idx / 3;
                    }

					chartSerieses[chartIndex]->AddXY((float)idx / 3.f, 20.f * log10((float)buff[idx] / 32.f));
                } else chartSerieses[chartIndex]->AddXY((float)idx / 3.f, -20);

            NLabels[chartIndex].newValue = maxVal;
            HLabels[chartIndex].newValue = maxPos;
        }
        //break;
    }
}
//---------------------------------------------------------------------------

void __fastcall TAdjustingForm::m_UpdateTimerTimer(TObject *Sender)
{
    if(!this->Visible)
        return;

    bool bNeedRailRedraw = false;
    const TColor InactiveColor = RGB(220,220,220);
    const TColor Grid_ActiveColor = RGB(220,220,220);
    const TColor Ticks_ActiveColor = clGray;
    const TColor Chart_ActiveColor = clBlack;

    for(int i = 1; i < 9; i++)
    {
        bool bChInGroup = AutoconMain->DEV->ChannelInCurrentGroup(RailCheckChannels[i]) && !bAllChannelsDisabled;
        if(channelStates[i] == bChInGroup)
            continue;
        channelStates[i] = bChInGroup;

        if(bChInGroup)
        {
            charts[i]->BackWall->Pen->Color = Chart_ActiveColor;
            charts[i]->BottomAxis->Axis->Color = Chart_ActiveColor;
            charts[i]->BottomAxis->LabelsFont->Color = Chart_ActiveColor;
            charts[i]->BottomAxis->Title->Font->Color = Chart_ActiveColor;
            charts[i]->BottomAxis->Ticks->Color = Ticks_ActiveColor;
            charts[i]->BottomAxis->MinorTicks->Color = Ticks_ActiveColor;
            charts[i]->BottomAxis->Grid->Color =  Grid_ActiveColor;
            charts[i]->LeftAxis->Axis->Color = Chart_ActiveColor;
            charts[i]->LeftAxis->LabelsFont->Color = Chart_ActiveColor;
            charts[i]->LeftAxis->Title->Font->Color = Chart_ActiveColor;
            charts[i]->LeftAxis->Ticks->Color = Ticks_ActiveColor;
            charts[i]->LeftAxis->MinorTicks->Color = Ticks_ActiveColor;
            charts[i]->LeftAxis->Grid->Color =  Grid_ActiveColor;
            charts[i]->Color = clWhite;
            chartSerieses[i]->Color = chartColors[i];

            painter.scanLines[i]->BtnChannelIsActive[0][0] = true;
            painter.scanLines[i]->PaintToBuffer();

            painter.railDraw.setColor(RAIL_HGL_KP_SURF(i), chartColors[i]);
            painter.railDraw.setColor(RAIL_HGL_KP(i),chartColors[i]);//clHighlight);
            bNeedRailRedraw = true;
        }
        else
        {
            charts[i]->BackWall->Pen->Color = InactiveColor;
            charts[i]->BottomAxis->Axis->Color = InactiveColor;
            charts[i]->BottomAxis->LabelsFont->Color = InactiveColor;
            charts[i]->BottomAxis->Title->Font->Color = InactiveColor;
            charts[i]->BottomAxis->Ticks->Color = InactiveColor;
            charts[i]->BottomAxis->MinorTicks->Color = InactiveColor;
            charts[i]->BottomAxis->Grid->Color =  InactiveColor;
            charts[i]->LeftAxis->Axis->Color = InactiveColor;
            charts[i]->LeftAxis->LabelsFont->Color = InactiveColor;
            charts[i]->LeftAxis->Title->Font->Color = InactiveColor;
            charts[i]->LeftAxis->Ticks->Color = InactiveColor;
            charts[i]->LeftAxis->MinorTicks->Color = InactiveColor;
            charts[i]->LeftAxis->Grid->Color =  InactiveColor;
            charts[i]->Color = clBtnFace;
            chartSerieses[i]->Color = RGB(180,180,180);

            painter.scanLines[i]->BtnChannelIsActive[0][0] = false;
            painter.scanLines[i]->PaintToBuffer();

            //painter.railDraw.setColor(RAIL_HGL_KP_SURF(i), chartColors[i]);
            painter.railDraw.setColor(RAIL_HGL_KP(i) | RAIL_HGL_KP_SURF(i),RGB(220,220,220));

            bNeedRailRedraw = true;
        }
    }

    if(bNeedRailRedraw && (currExpandedChart==-1))
    {
        painter.updateRail();
        painter.drawRail(0,true,m_RailDrawPBox->Canvas);
    }

    //Update labels
    for(int i = 1; i < 9; i++)
    {
        NLabels[i].setValue();
        HLabels[i].setValue();

        KuLabels[i].newValue = AutoconMain->Calibration->GetSens(dsNone, RailCheckChannels[i], 0);
        KuLabels[i].setValue();
    }

    if(newAdjState != adjState)
    {
        switch(newAdjState)
        {
            case ADJSTATE_INITIAL:
                m_AdjustingNextBtn->Enabled = false;
                m_ActiveChannelsCombo->Enabled = false;
                break;
            case ADJSTATE_TOP_CARET_MOVEMENT:
                m_AdjustingStateLabel->Caption = "���������: �������� ������� �������";
                m_AdjustingNextBtn->Caption = "������� ������ �������";
                m_AdjustingNextBtn->Enabled = true;
                m_ActiveChannelsCombo->Enabled = true;
                break;
            case ADJSTATE_BOT_CARET_MOVEMENT:
                m_AdjustingStateLabel->Caption = "���������: �������� ������ �������";

                #ifndef SIMULATION_CONTROLER_AND_MOTORS
                            m_AdjustingNextBtn->Caption = "������� ������ �� ������";
                            m_AdjustingNextBtn->Enabled = false;
                #else
                            m_AdjustingNextBtn->Caption = "������ \"����\"";
                #endif
                break;
            case ADJSTATE_AUTO_CARET_MOVEMENT:
                bAllChannelsDisabled = false;
                m_ActiveChannelsCombo->Items->Clear();
                m_ActiveChannelsCombo->Enabled = false;
                m_AdjustingNextBtn->Caption = "���������";
                m_AdjustingNextBtn->Enabled = true;
                break;
            case ADJSTATE_FINISHED:
                m_AdjustingStateLabel->Caption = "��������� ��������!";
                m_AdjustingNextBtn->Caption = "�����";
                m_AdjustingNextBtn->Enabled = true;
                break;
        }
        adjState = newAdjState;
    }
}
void __fastcall TAdjustingForm::m_CloseBtnClick(TObject *Sender)
{
	AutoconMain->ac->SetMode(acReady);
	ChangeModeFlag = true;
    m_ChangeModeTimer->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TAdjustingForm::m_BScanPBoxMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
    int MouseX = X;
    int MouseY = Y;
    int id = -1;

    for (int idx = 1; idx < 9; idx++)
    {
        id = painter.click(idx, MouseX, MouseY);
        if (id != -1)
        {
            break;
        }
    }

    painter.pScanDraw->Prepare();
    if (id < - 1) // ���������� ���������� ���������� ������
    {
		UpdatePBoxData(false);
        DrawPBoxData();
    }

    for (int idx = 1; idx < 9; idx++)
    {
		painter.scanLines[idx]->Refresh();
		painter.scanLines[idx]->PaintToBuffer();
	}

    DrawPBoxData();
}

//---------------------------------------------------------------------------


void __fastcall TAdjustingForm::OnChartClicked(TObject *Sender)
{
    if(currExpandedChart == -1)
    {
        int ColIndex = -1;
        int RowIndex = -1;
        for(int i = 1; i < 9; i++)
        {
            if(charts[i] == Sender)
            {
                currExpandedChart = i;

                RowIndex = (i <= 3) ? 0 : (i >= 6) ? 2 : 1;
                ColIndex = ((i == 2) || (i == 4) || (i ==6)) ? 0 :
                                ((i == 1) || (i == 8)) ? 1 :
                                ((i == 3) || (i == 5) || (i ==7)) ? 2 : -1;
            }
        }

        if((ColIndex != -1) && (RowIndex != -1))
        {
            m_ChartGrid->ColumnCollection->BeginUpdate();
            m_ChartGrid->RowCollection->BeginUpdate();

            for(int i = 0; i < 3; i++)
            {
                m_ChartGrid->ColumnCollection->Items[i]->Value =
                    (ColIndex == i) ? 100 : 0;
                m_ChartGrid->RowCollection->Items[i]->Value =
                    (RowIndex == i) ? 100 : 0;
            }
            m_ChartGrid->ColumnCollection->EndUpdate();
            m_ChartGrid->RowCollection->EndUpdate();
        }
    }
    else
    {
        m_ChartGrid->ColumnCollection->BeginUpdate();
        m_ChartGrid->RowCollection->BeginUpdate();
        for(int i = 0; i < 3; i++)
        {
            m_ChartGrid->ColumnCollection->Items[i]->Value = 33.3333;
            m_ChartGrid->RowCollection->Items[i]->Value = 33.3333;
        }
        m_ChartGrid->ColumnCollection->EndUpdate();
        m_ChartGrid->RowCollection->EndUpdate();

        currExpandedChart = -1;
    }
}
//---------------------------------------------------------------------------


void __fastcall TAdjustingForm::m_ChangeModeTimerTimer(TObject *Sender)
{
    if (ChangeModeFlag && (AutoconMain) && (AutoconMain->ac))
	{
		if (AutoconMain->ac->GetMode() == acReady)
		{
            ChangeModeFlag = false;
            m_ChangeModeTimer->Enabled = false;
			this->Visible = false;

		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TAdjustingForm::m_BScanExpandBtnClick(TObject *Sender)
{
    if(!bLeftPanelExpanded)
    {
        m_LeftPanel->Width = float(this->Width) * 0.666f;
        m_BScanExpandBtn->Caption = "<<<";
    }
    else
    {
        m_LeftPanel->Width = float(this->Width) * 0.333f;
        m_BScanExpandBtn->Caption = ">>>";
    }

    bLeftPanelExpanded = !bLeftPanelExpanded;
}
//---------------------------------------------------------------------------


void __fastcall TAdjustingForm::m_AdjustingNextBtnClick(TObject *Sender)
{
    switch(newAdjState)
    {
        case ADJSTATE_TOP_CARET_MOVEMENT:
            //��������� � ��������� �������� ������ �������
            AutoconMain->DEV->DisableAll();
            AutoconMain->ac->SetMode(acAdjusting2);

            m_ActiveChannelsCombo->Items->Clear();
            m_ActiveChannelsCombo->Items->Add(SelKPComboStrings[0]);
            m_ActiveChannelsCombo->Items->Add(SelKPComboStrings[2]);
            m_ActiveChannelsCombo->Items->Add(SelKPComboStrings[3]);
            m_ActiveChannelsCombo->ItemIndex = 1;

            AutoconMain->DEV->ResetPathEncoder();
            //AutoconMain->DEV->SetChannelGroup(5);
            AutoconMain->DEV->Update(false);
            break;
        case ADJSTATE_BOT_CARET_MOVEMENT:
            //�� ������� �� ������ ����
            #ifdef SIMULATION_CONTROLER_AND_MOTORS
            AutoconMain->ac->Debug_CallEventSignProc(true);
            #endif
            break;
        case ADJSTATE_AUTO_CARET_MOVEMENT:
            //���������
            m_CloseBtnClick(Sender);
            break;
        case ADJSTATE_FINISHED:
            //���������
            m_CloseBtnClick(Sender);
            break;
    }
}
//---------------------------------------------------------------------------

void __fastcall TAdjustingForm::m_ActiveChannelsComboSelect(TObject *Sender)
{
    int itemIndex = m_ActiveChannelsCombo->ItemIndex;
    bAllChannelsDisabled = false;

    if(m_ActiveChannelsCombo->Items->Strings[itemIndex] == SelKPComboStrings[0])
    {
        AutoconMain->DEV->DisableAll();
        bAllChannelsDisabled = true;
    }
    else if(m_ActiveChannelsCombo->Items->Strings[itemIndex] == SelKPComboStrings[1])
    {
        AutoconMain->DEV->SetChannelGroup(3);
    }
    else if(m_ActiveChannelsCombo->Items->Strings[itemIndex] == SelKPComboStrings[2])
    {
        AutoconMain->DEV->SetChannelGroup(5);
    }
    else if(m_ActiveChannelsCombo->Items->Strings[itemIndex] == SelKPComboStrings[3])
    {
        AutoconMain->DEV->SetChannelGroup(6);
    }

    AutoconMain->DEV->Update(false);
    Sleep(500);
}
//---------------------------------------------------------------------------



void TAdjustingForm::AdjustingMotion_(int WorkCycle) //����� "���������"
{
    if(WorkCycle >= 0)
    {
        m_AdjustingStateLabel->Caption = StringFormatU(L"���������: %d%%", (WorkCycle+1)*100/8);
    }

    switch(WorkCycle)
    {
        case -5:    //������ �������� ������� �������
            newAdjState = ADJSTATE_TOP_CARET_MOVEMENT;
            break;
        case -4:    //������ �������� ������ �������
            newAdjState = ADJSTATE_BOT_CARET_MOVEMENT;
            break;
        case -3:    //��������� �������� ������� ������� ��� �������� � Adj2
            AutoconMain->DEV->ResetPathEncoder();
            AutoconMain->DEV->SetChannelGroup(5);
            AutoconMain->DEV->Update(false);
            break;
        case -1:
            break;
        case 0: //(�->|) - ������ ������
            newAdjState = ADJSTATE_AUTO_CARET_MOVEMENT;
            AutoconMain->DEV->ResetPathEncoder();
            AutoconMain->DEV->SetChannelGroup(3);
            AutoconMain->DEV->Update(true);
            break;
        case 1: //(�->|)
            AutoconMain->DEV->ResetPathEncoder();
            AutoconMain->DEV->SetChannelGroup(5);
            AutoconMain->DEV->Update(true);
            break;
        case 2: //(|<-H)
            AutoconMain->DEV->SetChannelGroup(5);
            AutoconMain->DEV->Update(true);
            break;
        case 3: //(H->|)
            AutoconMain->DEV->ResetPathEncoder();
            AutoconMain->DEV->SetChannelGroup(6);
            AutoconMain->DEV->Update(true);
            break;
        case 4: //(|<-H)
            AutoconMain->DEV->SetChannelGroup(6);
            AutoconMain->DEV->Update(true);
            break;
        case 5: //(|<-B)
            AutoconMain->DEV->SetChannelGroup(3);
            AutoconMain->DEV->Update(true);
            break;
        case 6: // - ����� ������
            AutoconMain->DEV->DisableAll();
            break;
        case 7:    //��� �������� ��������
            newAdjState = ADJSTATE_FINISHED;
            break;
    }
    Sleep(100);
}


void TAdjustingForm::Clear_Data()
{
    m_UpdateTimer->Enabled = false;
    m_DrawTimer->Enabled = false;
    AutoconMain->Rep->ClearData(); // ������� ������
    m_DrawTimer->Enabled = true;
    m_UpdateTimer->Enabled = true;


    for(int i = 1; i < 9; i++)
    {
        channelStates[i] = false;
        chartSerieses[i]->Clear();
        //charts[i]->ClearChart();
    }

    painter.clearBScan();
    UpdatePBoxData(false);
	DrawPBoxData();

}
//---------------------------------------------------------------------------


