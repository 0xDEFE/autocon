//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ScreenMessageUnit3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//TScreenMessageForm3 *ScreenMessageForm3;
//---------------------------------------------------------------------------
__fastcall TScreenMessageForm3::TScreenMessageForm3(sSMF3_Data& data, TComponent* Owner)
    : TForm(Owner)
{
    this->data = data;
    selectedCode = 0;
}
//---------------------------------------------------------------------------
void __fastcall TScreenMessageForm3::FormShow(TObject *Sender)
{
    selectedCode = 0;
    captionPanel->Color = data.color;
    this->Color = data.color;
    CaptionLabel->Caption = data.caption;
    TextLabel->Caption = data.text;


    if(data.flags & SMF3_FLAG_MB_OK)
        BtnOkYes->Visible = true;
    if(data.flags & SMF3_FLAG_MB_YESNO)
    {
        BtnOkYes->Visible = true;
        BtnNo->Visible = true;
    }
    if(data.flags & SMF3_FLAG_MB_CANCEL)
        BtnCancel->Visible = true;

    if(data.flags & SMF3_FLAG_MB_YESNO)
        BtnOkYes->Caption = "��";
    else
        BtnOkYes->Caption = "��";

    if(!data.text.IsEmpty())
        TextLabel->Visible = true;

    if(data.timeout != 0xFFFFFFFF)
        TimeoutPB->Visible = true;

    if(!BtnOkYes->Visible && !BtnNo->Visible && !BtnCancel->Visible)
        ResultBtnsPanel->Visible = false;

    if((data.flags & SMF3_FLAG_SELECT_MODE) && (!data.answers.empty()))
    {
        AnswerGridPanel->Visible = true;

        AnswerGridPanel->RowCollection->BeginUpdate();
        AnswerGridPanel->RowCollection->Clear();
        for(unsigned int i = 0; i < data.answers.size(); i++)
        {
            TRowItem* pColItem = AnswerGridPanel->RowCollection->Add();
            pColItem->SizeStyle = ssPercent;
            pColItem->Value = 100.f / data.answers.size();
        }
        AnswerGridPanel->RowCollection->EndUpdate();

        TNotifyEvent notifyOnClick = &(this->OnAnswerClick);
        ansButtons.resize(data.answers.size());
        for(unsigned int i = 0; i < data.answers.size(); i++)
        {
            ansButtons[i] = new TBitBtn(this);
            ansButtons[i]->Align = alClient;
            ansButtons[i]->Glyph = data.answers[i].pGlyph;
            ansButtons[i]->Tag = data.answers[i].retCode;
            ansButtons[i]->Caption = data.answers[i].text;
            ansButtons[i]->AlignWithMargins = true;
            ansButtons[i]->Layout = blGlyphLeft;
            ansButtons[i]->OnClick = notifyOnClick;
            ansButtons[i]->WordWrap = true;

            AnswerGridPanel->InsertControl(ansButtons[i]);
        }
    }

    StartTime = GetTickCount();
    if(data.timeout != 0xFFFFFFFF)
    {
        UpdateTimer->Enabled = true;
    }
}
//---------------------------------------------------------------------------
int TScreenMessageForm3::execute()
{
    if(data.flags & SMF3_FLAG_NONMODAL)
        this->Show();
    else
        this->ShowModal();
    return ModalResult;
}

int TScreenMessageForm3::getSelectedCode()
{
    return selectedCode;
}
void __fastcall TScreenMessageForm3::BtnOkYesClick(TObject *Sender)
{
    if(data.flags & SMF3_FLAG_MB_YESNO)
        ModalResult = mrYes;
    else
        ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TScreenMessageForm3::BtnNoClick(TObject *Sender)
{
    ModalResult = mrNo;
}
//---------------------------------------------------------------------------
void __fastcall TScreenMessageForm3::BtnCancelClick(TObject *Sender)
{
    ModalResult = mrCancel;
}
//---------------------------------------------------------------------------


int TScreenMessageForm3::MessageBoxA(TComponent* Owner, const char* Caption, const char* Text,
                            unsigned int flags, TColor color, DWORD timeout,
                            std::vector<sSMF3_Answer>* pAnswers, int* pRetAnswer)
{
    sSMF3_Data data;
    data.caption = Caption;
    data.text = Text;
    data.flags = flags;
    data.color = color;
    data.timeout = timeout;
    if(pAnswers)
        data.answers = *pAnswers;
    TScreenMessageForm3* pForm = new TScreenMessageForm3(data,Owner);
    int ret = pForm->execute();
    if(pRetAnswer)
        *pRetAnswer = pForm->getSelectedCode();
    delete pForm;
    return ret;
}

int TScreenMessageForm3::MessageBoxW(TComponent* Owner, const wchar_t* Caption, const wchar_t* Text,
                            unsigned int flags, TColor color, DWORD timeout,
                            std::vector<sSMF3_Answer>* pAnswers, int* pRetAnswer)
{
    sSMF3_Data data;
    data.caption = Caption;
    data.text = Text;
    data.flags = flags;
    data.color = color;
    data.timeout = timeout;
    if(pAnswers)
        data.answers = *pAnswers;
    TScreenMessageForm3* pForm = new TScreenMessageForm3(data,Owner);
    int ret = pForm->execute();
    if(pRetAnswer)
        *pRetAnswer = pForm->getSelectedCode();
    delete pForm;
    return ret;
}

void __fastcall TScreenMessageForm3::UpdateTimerTimer(TObject *Sender)
{
    //Update timeout pb
    TimeoutPB->Canvas->Brush->Color = clWhite;
    TimeoutPB->Canvas->FillRect(TRect(0,0,TimeoutPB->Width,TimeoutPB->Height));

    int workAngleDeg = (float(GetTickCount()-StartTime)/(float)data.timeout) * 360;
    if(workAngleDeg > 360)
        workAngleDeg = 360;
    const float PI = 3.14159265359;
    if(workAngleDeg<0)
        workAngleDeg+=360;
    float workAngle = float(workAngleDeg)* (PI/180.f);

    float posX_end = -sin(workAngle);
    float posY_end = +cos(workAngle);
    TRect drawRect = TimeoutPB->ClientRect;
    drawRect.Inflate(-5,-5);
    posX_end = posX_end * drawRect.Width()/2 + TimeoutPB->Width/2;
    posY_end = posY_end * drawRect.Height()/2 + TimeoutPB->Height/2;

    SetArcDirection(TimeoutPB->Canvas->Handle,AD_CLOCKWISE);

    TimeoutPB->Canvas->Pen->Width = 2;
    TimeoutPB->Canvas->Pen->Color = data.color;
    TimeoutPB->Canvas->Pen->Style = psSolid;
    TimeoutPB->Canvas->Arc(drawRect.left,drawRect.top,drawRect.right,drawRect.bottom, 	//Draw rectangle
                        drawRect.left+drawRect.Width()/2,drawRect.bottom,		//Start pos
                        posX_end,posY_end);					//End pos

    //On end
    if((GetTickCount()-StartTime) >= data.timeout)
    {
        ModalResult = mrCancel;
    }
}
//---------------------------------------------------------------------------

void __fastcall TScreenMessageForm3::OnAnswerClick(TObject *Sender)
{
    TBitBtn* pBtn = (TBitBtn*)Sender;
    selectedCode = pBtn->Tag;
    ModalResult = mrOk;
}

void TScreenMessageForm3::ErrorMessageA( const char* Caption, const char* Text, DWORD timeout)
{
    MessageBoxA(NULL, Caption, Text, SMF3_FLAG_MB_OK | SMF3_FLAG_ICONINFO | SMF3_FLAG_OK_ON_CLICK, clRed, timeout);
}

void TScreenMessageForm3::InfoMessageA( const char* Caption, const char* Text, DWORD timeout)
{
    MessageBoxA(NULL, Caption, Text, SMF3_FLAG_MB_OK | SMF3_FLAG_ICONINFO | SMF3_FLAG_OK_ON_CLICK, clHotLight, timeout);
}

void TScreenMessageForm3::ErrorMessageW( const wchar_t* Caption, const wchar_t* Text, DWORD timeout)
{
    MessageBoxW(NULL, Caption, Text, SMF3_FLAG_MB_OK | SMF3_FLAG_ICONINFO | SMF3_FLAG_OK_ON_CLICK, clRed, timeout);
}

void TScreenMessageForm3::InfoMessageW( const wchar_t* Caption, const wchar_t* Text, DWORD timeout)
{
    MessageBoxW(NULL, Caption, Text, SMF3_FLAG_MB_OK | SMF3_FLAG_ICONINFO | SMF3_FLAG_OK_ON_CLICK, clHotLight, timeout);
}
