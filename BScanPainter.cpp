//---------------------------------------------------------------------------

#pragma hdrstop

#include "BScanPainter.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#include "AutoconMain.h"
extern cAutoconMain *AutoconMain;

void cBScanPainter::init(cJointTestingReport* Rep, eBScanLayout layoutType, eBScanVisibleChannels visibleChannelsType)
{
    assert(Rep);
    this->Rep = Rep;

	if(!pScanDraw)
		pScanDraw = new cBScanDraw(AutoconMain->Table, AutoconMain->Config, Rep, AutoconMain->Calibration);

	if(bInitialized)
		pScanDraw->ClearBScanLines();

	for (int idx = 1; idx < 9; idx++)
	{
		if(!scanLines[idx])
			scanLines[idx] = new cBScanLines();
		pScanDraw->AddBScanLine(scanLines[idx]);
	}

    this->layoutType = layoutType;
    this->visibleChannelsType = visibleChannelsType;

    switch(visibleChannelsType)
    {
        case BSVC_ALL_CHANNELS:
            _initBScanPages_AllChannels();
            break;
        case BSVC_ONLY_CONTROL_CHANNELS:
            _initBScanPages_OnlyControl();
            break;

    }

    prevScanChannelGroup = -1;
    prevUpdateWorkCycle = -1;
    chanHideMode = CFM_HIDE_NONE;
    currVisualMode = BS_VISUAL_UNKNOWN;
	bInitialized = true;

}

cBScanPainter::~cBScanPainter()
{
    if(pScanDraw)
    {
        delete pScanDraw;
        pScanDraw = NULL;
    }

    for (int idx = 1; idx < 9; idx++)
	{
		if(scanLines[idx])
        {
            delete scanLines[idx];
            scanLines[idx] = NULL;
        }
	}
}

void cBScanPainter::_initBScanPages_OnlyControl()
{
    //Clear BtnId
	for(int i = 1; i < 9; i++)
		for(int x = 0; x < 6; x++)
			for(int y = 0; y < 6; y++)
				scanLines[i]->BtnId[x][y] = 0x00;

    const int RailCheckChannels[] = {0,0x46,0x52,0x5A,0x63,0x6E,0x79,0x84,0x90};

    for(int i = 1; i < 9; i++)
    {
        cBScanLines* tmp = scanLines[i];
        tmp->BScanLineCount = 1;
        tmp->BtnColumnCount = 1;
        tmp->bOnlyOneCol = true;

        tmp->BtnId[0][0] = RailCheckChannels[i];
        tmp->BtnChColor[0][0] = clRed;
    }

    sChannelDescription ChanDesc;
	for(int i = 1; i < 9; i++)
		for (int x = 0; x < 6; x++)
			for (int y = 0; y < 6; y++)
			{
				AutoconMain->Table->ItemByCID(scanLines[i]->BtnId[x][y], &ChanDesc);
				//scanLines[i]->BtnBigText[x][y] = IntToStr(ChanDesc.cdEnterAngle) + "�";

                UnicodeString title = ChanDesc.Title;
                title.Delete(1, 4);

                for(int c = 0; c < title.Length(); c++)
                {
                    if(title.c_str()[c] == L' ')
                    {
                        title = StringFormatU(L"��%d",i) + title.SubString(c+1,title.Length() - (c));
                        break;
                    }
                }



                if(scanLines[i]->LineDrawType[y] != LINEDRAW_SMALL_LINE)
                    title = StringReplace(title, " ", "\r\n", TReplaceFlags() << rfReplaceAll << rfIgnoreCase);

                scanLines[i]->BtnBigText[x][y] = title;

				//Modifyed by KirillB
				scanLines[i]->BtnChannelBehind[x][y] = ((i == 2) || (i == 3)) ? (x<2) : (x<3);
				scanLines[i]->ApplyBtnHighlightLevel(64,y,x);

				if(scanLines[i]->BtnId[x][y] != 0x00)
				{
					scanLines[i]->BtnVisible[x][y] = true;
				}
			}

	//----------------------------------------------------
	pScanDraw->Prepare();
	//----------------------------------------------------
}

void cBScanPainter::_initBScanPages_AllChannels()
{
	//Init channel ID's and colors
	TColor F1 = clRed;
	TColor F2 = TColor((254) | (1 << 8) | (216 << 16));
	TColor F3 = TColor((233) | (239 << 8) | (1 << 16)); // clFuchsia;

	TColor B1 = clBlue;
	TColor B2 = TColor((1) | (216 << 8) | (254 << 16)); //clNavy;
	TColor B3 = TColor((0) | (255 << 8) | (0 << 16)); //clActiveCaption;

	//Clear BtnId
	for(int i = 1; i < 9; i++)
		for(int x = 0; x < 6; x++)
			for(int y = 0; y < 6; y++)
				scanLines[i]->BtnId[x][y] = 0x00;
	//----------------------KP1----------------------------
	cBScanLines* tmp = scanLines[1];
	tmp->BScanLineCount = 4;
	tmp->BtnColumnCount = 3;

	tmp->BtnId[0][0] = 0x00; tmp->BtnId[1][0] = 0x3F; tmp->BtnId[2][0] = 0x40;
	tmp->BtnId[0][1] = 0x41; tmp->BtnId[1][1] = 0x42; tmp->BtnId[2][1] = 0x43;
	tmp->BtnId[0][2] = 0x00; tmp->BtnId[1][2] = 0x44; tmp->BtnId[2][2] = 0x45;
	tmp->BtnId[0][3] = 0x00; tmp->BtnId[1][3] = 0x00; tmp->BtnId[2][3] = 0x46;

	tmp->BtnId[3][0] = 0x4D; tmp->BtnId[4][0] = 0x4C; tmp->BtnId[5][0] = 0x00;
	tmp->BtnId[3][1] = 0x4B; tmp->BtnId[4][1] = 0x4A; tmp->BtnId[5][1] = 0x49;
	tmp->BtnId[3][2] = 0x48; tmp->BtnId[4][2] = 0x47; tmp->BtnId[5][2] = 0x00;
	tmp->BtnId[3][3] = 0x00; tmp->BtnId[4][3] = 0x00; tmp->BtnId[5][3] = 0x00;

	tmp->BtnChColor[1][0] = F1;
	tmp->BtnChColor[2][0] = F2;
	tmp->BtnChColor[0][1] = F1;
	tmp->BtnChColor[1][1] = F2;
	tmp->BtnChColor[2][1] = F3;
	tmp->BtnChColor[1][2] = F1;
	tmp->BtnChColor[2][2] = F2;
	tmp->BtnChColor[2][3] = F1;

	tmp->BtnChColor[4][0] = B1;
	tmp->BtnChColor[3][0] = B2;
	tmp->BtnChColor[5][1] = B1;
	tmp->BtnChColor[4][1] = B2;
	tmp->BtnChColor[3][1] = B3;
	tmp->BtnChColor[4][2] = B1;
	tmp->BtnChColor[3][2] = B2;
	tmp->BtnChColor[3][3] = B1;
	//----------------------KP2----------------------------
	tmp = scanLines[2];
	tmp->BScanLineCount = 3;
	tmp->BtnColumnCount = 2;
	tmp->LineDrawType[2] = LINEDRAW_SMALL_LINE;

	tmp->BtnId[0][0] = 0x4E; tmp->BtnId[1][0] = 0x4F;
	tmp->BtnId[0][1] = 0x50; tmp->BtnId[1][1] = 0x51;
	tmp->BtnId[0][2] = 0x00; tmp->BtnId[1][2] = 0x52;

	tmp->BtnId[2][0] = 0x56; tmp->BtnId[3][0] = 0x55;
	tmp->BtnId[2][1] = 0x54; tmp->BtnId[3][1] = 0x53;
	tmp->BtnId[2][2] = 0x00; tmp->BtnId[3][2] = 0x00;

	tmp->BtnChColor[0][0] = F1;
	tmp->BtnChColor[1][0] = F2;
	tmp->BtnChColor[0][1] = F1;
	tmp->BtnChColor[1][1] = F2;
	tmp->BtnChColor[1][2] = F1;

	tmp->BtnChColor[2][0] = B1;
	tmp->BtnChColor[3][0] = B2;
	tmp->BtnChColor[2][1] = B1;
	tmp->BtnChColor[3][1] = B2;
	//----------------------KP3----------------------------
	tmp = scanLines[3];
	tmp->BScanLineCount = 3;
	tmp->BtnColumnCount = 2;
	tmp->LineDrawType[2] = LINEDRAW_SMALL_LINE;

	tmp->BtnId[0][0] = 0x00; tmp->BtnId[1][0] = 0x57;
	tmp->BtnId[0][1] = 0x58; tmp->BtnId[1][1] = 0x59;
	tmp->BtnId[0][2] = 0x00; tmp->BtnId[1][2] = 0x5A;

	tmp->BtnId[2][0] = 0x5D; tmp->BtnId[3][0] = 0x00;
	tmp->BtnId[2][1] = 0x5C; tmp->BtnId[3][1] = 0x5B;
	tmp->BtnId[2][2] = 0x00; tmp->BtnId[3][2] = 0x00;

	tmp->BtnChColor[1][0] = F2;
	tmp->BtnChColor[0][1] = F1;
	tmp->BtnChColor[1][1] = F2;
	tmp->BtnChColor[1][2] = F1;

	tmp->BtnChColor[2][0] = B1;
	tmp->BtnChColor[2][1] = B1;
	tmp->BtnChColor[3][1] = B2;
	//----------------------KP4----------------------------
	tmp = scanLines[4];
	tmp->BScanLineCount = 3;
	tmp->BtnColumnCount = 3;
	tmp->LineDrawType[2] = LINEDRAW_SMALL_LINE;

	tmp->BtnId[0][0] = 0x00; tmp->BtnId[1][0] = 0x5E; tmp->BtnId[2][0] = 0x5F;
	tmp->BtnId[0][1] = 0x60; tmp->BtnId[1][1] = 0x61; tmp->BtnId[2][1] = 0x62;
	tmp->BtnId[0][2] = 0x00; tmp->BtnId[1][2] = 0x00; tmp->BtnId[2][2] = 0x63;

	tmp->BtnId[3][0] = 0x68; tmp->BtnId[4][0] = 0x67; tmp->BtnId[5][0] = 0x00;
	tmp->BtnId[3][1] = 0x66; tmp->BtnId[4][1] = 0x65; tmp->BtnId[5][1] = 0x64;
	tmp->BtnId[3][2] = 0x00; tmp->BtnId[4][2] = 0x00; tmp->BtnId[5][2] = 0x00;

	tmp->BtnChColor[0][1] = F1;
	tmp->BtnChColor[1][0] = F1;
	tmp->BtnChColor[2][1] = F3;
	tmp->BtnChColor[1][1] = F2;
	tmp->BtnChColor[2][0] = F2;
	tmp->BtnChColor[2][2] = F1;

	tmp->BtnChColor[5][1] = B1;
	tmp->BtnChColor[4][0] = B2;
	tmp->BtnChColor[4][1] = B2;
	tmp->BtnChColor[3][0] = B3;
	tmp->BtnChColor[3][1] = B3;
	//----------------------KP5----------------------------
	tmp = scanLines[5];
	tmp->BScanLineCount = 3;
	tmp->BtnColumnCount = 3;
	tmp->LineDrawType[2] = LINEDRAW_SMALL_LINE;

	tmp->BtnId[0][0] = 0x00; tmp->BtnId[1][0] = 0x69; tmp->BtnId[2][0] = 0x6A;
	tmp->BtnId[0][1] = 0x6B; tmp->BtnId[1][1] = 0x6C; tmp->BtnId[2][1] = 0x6D;
	tmp->BtnId[0][2] = 0x00; tmp->BtnId[1][2] = 0x00; tmp->BtnId[2][2] = 0x6E;

	tmp->BtnId[3][0] = 0x73; tmp->BtnId[4][0] = 0x72; tmp->BtnId[5][0] = 0x00;
	tmp->BtnId[3][1] = 0x71; tmp->BtnId[4][1] = 0x70; tmp->BtnId[5][1] = 0x6F;
	tmp->BtnId[3][2] = 0x00; tmp->BtnId[4][2] = 0x00; tmp->BtnId[5][2] = 0x00;

	tmp->BtnChColor[0][1] = F1;
	tmp->BtnChColor[1][0] = F1;
	tmp->BtnChColor[2][1] = F3;
	tmp->BtnChColor[1][1] = F2;
	tmp->BtnChColor[2][0] = F2;
	tmp->BtnChColor[2][2] = F1;

	tmp->BtnChColor[5][1] = B1;
	tmp->BtnChColor[4][0] = B2;
	tmp->BtnChColor[4][1] = B2;
	tmp->BtnChColor[3][0] = B3;
	tmp->BtnChColor[3][1] = B3;
	//----------------------KP6----------------------------
	tmp = scanLines[6];
	tmp->BScanLineCount = 3;
	tmp->BtnColumnCount = 3;
	tmp->LineDrawType[2] = LINEDRAW_SMALL_LINE;

	tmp->BtnId[0][0] = 0x00; tmp->BtnId[1][0] = 0x74; tmp->BtnId[2][0] = 0x75;
	tmp->BtnId[0][1] = 0x76; tmp->BtnId[1][1] = 0x77; tmp->BtnId[2][1] = 0x78;
	tmp->BtnId[0][2] = 0x00; tmp->BtnId[1][2] = 0x00; tmp->BtnId[2][2] = 0x79;

	tmp->BtnId[3][0] = 0x7E; tmp->BtnId[4][0] = 0x7D; tmp->BtnId[5][0] = 0x00;
	tmp->BtnId[3][1] = 0x7C; tmp->BtnId[4][1] = 0x7B; tmp->BtnId[5][1] = 0x7A;
	tmp->BtnId[3][2] = 0x00; tmp->BtnId[4][2] = 0x00; tmp->BtnId[5][2] = 0x00;

	tmp->BtnChColor[0][1] = F1;
	tmp->BtnChColor[1][0] = F1;
	tmp->BtnChColor[2][1] = F3;
	tmp->BtnChColor[1][1] = F2;
	tmp->BtnChColor[2][0] = F2;
	tmp->BtnChColor[2][2] = F1;

	tmp->BtnChColor[5][1] = B1;
	tmp->BtnChColor[4][0] = B2;
	tmp->BtnChColor[4][1] = B2;
	tmp->BtnChColor[3][0] = B3;
	tmp->BtnChColor[3][1] = B3;
	//----------------------KP7----------------------------
	tmp = scanLines[7];
	tmp->BScanLineCount = 3;
	tmp->BtnColumnCount = 3;
	tmp->LineDrawType[2] = LINEDRAW_SMALL_LINE;

	tmp->BtnId[0][0] = 0x00; tmp->BtnId[1][0] = 0x7F; tmp->BtnId[2][0] = 0x80;
	tmp->BtnId[0][1] = 0x81; tmp->BtnId[1][1] = 0x82; tmp->BtnId[2][1] = 0x83;
	tmp->BtnId[0][2] = 0x00; tmp->BtnId[1][2] = 0x00; tmp->BtnId[2][2] = 0x84;

	tmp->BtnId[3][0] = 0x89; tmp->BtnId[4][0] = 0x88; tmp->BtnId[5][0] = 0x00;
	tmp->BtnId[3][1] = 0x87; tmp->BtnId[4][1] = 0x86; tmp->BtnId[5][1] = 0x85;
	tmp->BtnId[3][2] = 0x00; tmp->BtnId[4][2] = 0x00; tmp->BtnId[5][2] = 0x00;

	tmp->BtnChColor[0][1] = F1;
	tmp->BtnChColor[1][0] = F1;
	tmp->BtnChColor[2][1] = F3;
	tmp->BtnChColor[1][1] = F2;
	tmp->BtnChColor[2][0] = F2;
	tmp->BtnChColor[2][2] = F1;

	tmp->BtnChColor[5][1] = B1;
	tmp->BtnChColor[4][0] = B2;
	tmp->BtnChColor[4][1] = B2;
	tmp->BtnChColor[3][0] = B3;
	tmp->BtnChColor[3][1] = B3;
	//----------------------KP8----------------------------
	tmp = scanLines[8];
	tmp->BScanLineCount = 4;
	tmp->BtnColumnCount = 3;

	tmp->BtnId[0][0] = 0x00; tmp->BtnId[1][0] = 0x8A; tmp->BtnId[2][0] = 0x8B;
	tmp->BtnId[0][1] = 0x8C; tmp->BtnId[1][1] = 0x8D; tmp->BtnId[2][1] = 0x8E;
	tmp->BtnId[0][2] = 0x00; tmp->BtnId[1][2] = 0x00; tmp->BtnId[2][2] = 0x8F;
	tmp->BtnId[0][3] = 0x00; tmp->BtnId[1][3] = 0x00; tmp->BtnId[2][3] = 0x90;

	tmp->BtnId[3][0] = 0x96; tmp->BtnId[4][0] = 0x95; tmp->BtnId[5][0] = 0x00;
	tmp->BtnId[3][1] = 0x94; tmp->BtnId[4][1] = 0x93; tmp->BtnId[5][1] = 0x92;
	tmp->BtnId[3][2] = 0x91; tmp->BtnId[4][2] = 0x00; tmp->BtnId[5][2] = 0x00;
	tmp->BtnId[3][3] = 0x00; tmp->BtnId[4][3] = 0x00; tmp->BtnId[5][3] = 0x00;

	tmp->BtnChColor[1][0] = F1;
	tmp->BtnChColor[2][0] = F2;
	tmp->BtnChColor[0][1] = F1;
	tmp->BtnChColor[1][1] = F2;
	tmp->BtnChColor[2][1] = F3;
	tmp->BtnChColor[1][2] = F1;
	tmp->BtnChColor[2][2] = F2;
	tmp->BtnChColor[2][3] = F1;

	tmp->BtnChColor[4][0] = B1;
	tmp->BtnChColor[3][0] = B2;
	tmp->BtnChColor[5][1] = B1;
	tmp->BtnChColor[4][1] = B2;
	tmp->BtnChColor[3][1] = B3;
	tmp->BtnChColor[4][2] = B1;
	tmp->BtnChColor[3][2] = B2;
	tmp->BtnChColor[3][3] = B1;
	//-----------------------------------------------------

	sChannelDescription ChanDesc;
	for(int i = 1; i < 9; i++)
		for (int x = 0; x < 6; x++)
			for (int y = 0; y < 6; y++)
			{
				AutoconMain->Table->ItemByCID(scanLines[i]->BtnId[x][y], &ChanDesc);
				//scanLines[i]->BtnBigText[x][y] = IntToStr(ChanDesc.cdEnterAngle) + "�";

                UnicodeString title = ChanDesc.Title;
                title.Delete(1, 4);

                if(scanLines[i]->LineDrawType[y] != LINEDRAW_SMALL_LINE)
                    title = StringReplace(title, " ", "�\r\n", TReplaceFlags() << rfReplaceAll << rfIgnoreCase);

                scanLines[i]->BtnBigText[x][y] = title;

				//Modifyed by KirillB
				scanLines[i]->BtnChannelBehind[x][y] = ((i == 2) || (i == 3)) ? (x<2) : (x<3);
				scanLines[i]->ApplyBtnHighlightLevel(64,y,x);

				if(scanLines[i]->BtnId[x][y] != 0x00)
				{
					scanLines[i]->BtnVisible[x][y] = true;
				}
			}

	//----------------------------------------------------
	pScanDraw->Prepare();
	//----------------------------------------------------
}

void cBScanPainter::setBScanOrientation(bool bLeftToRight)
{
	bLeftToRightBScanLayout = bLeftToRight;
	setRailOrientation( bLeftToRightBScanLayout );
	applyBScanLayout();
}

void cBScanPainter::setRailOrientation(bool bLeftToRight)
{
	bLeftToRightRailLayout = bLeftToRight;
	railDraw.setOrientation(bLeftToRightRailLayout);
}

void cBScanPainter::setRailRect(TRect rect)
{
	if(RailRect != rect)
	{
		RailRect = rect;
		railDraw.resize(rect.Width(),rect.Height());
    }
}

TRect cBScanPainter::setBScanPageRect(TRect rect, unsigned int nPage)
{
	if(scanLines[nPage]->BoundsRect != rect)
	{
		scanLines[nPage]->BoundsRect = rect;
		scanLines[nPage]->Refresh();
		scanLines[nPage]->PaintToBuffer();
		rect.Right = rect.Left + scanLines[nPage]->Buffer->Width;
		rect.Bottom = rect.Top + scanLines[nPage]->Buffer->Height;
    }
	return rect;
}

void cBScanPainter::setBScanLayout(eBScanLayout type)
{
    layoutType = type;
    applyBScanLayout();
}

void cBScanPainter::applyBScanLayout()
{
    switch(layoutType)
    {
        case BSL_ALL_IN_ONE:
            _applyBScanLayout_AllIn();
            break;
        case BSL_ONLY_CONTROL_CHANNELS:
            _applyBScanLayout_OnlyControl();
            break;
    }
}

void cBScanPainter::_applyBScanLayout_OnlyControl()
{
    int W = BScanRect.Width();
	int H = BScanRect.Height();

    TRect Rects[9];
    const int H_Delta = H/8;
    for(int i = 1; i < 9; i++)
    {
        Rects[i] = TRect(0,(i-1) * H_Delta, W, i * H_Delta);
    }

    //------------Applying BScan rects----------------

	for (int idx = 1; idx < 9; idx++)
	{
		Rects[idx] = setBScanPageRect(Rects[idx],idx);
	}

    //------------------------------------------------

	pScanDraw->Prepare();
}

void cBScanPainter::_applyBScanLayout_AllIn()
{
	int W = BScanRect.Width();
	int H = BScanRect.Height();
	int S = 100;

	int midRegStart = H / 4;
	int midRegEnd = 3 * H / 4;
	int midRegHeight = midRegEnd - midRegStart;
	int midRegDelta = midRegHeight / 3;
	int midRegCorrection = midRegDelta / 3;

	TRect Rects[9];

	Rects[1].Left = W / 2 - W / 4;
	Rects[1].Right = W / 2 + W / 4;
	Rects[1].Top = 0;
	//Modifyed by KirillB
	Rects[1].Bottom = H / 4;

	Rects[2].Left = W / 2 - W / 2;
	Rects[2].Right = W / 2 - S;
	//Modifyed by KirillB
	Rects[2].Top = midRegStart;
	Rects[2].Bottom = midRegStart + midRegDelta + midRegCorrection;

	Rects[3].Left = S + W / 2;
	Rects[3].Right = W / 2 + W / 2;
	//Modifyed by KirillB
	Rects[3].Top = midRegStart;
	Rects[3].Bottom = midRegStart + midRegDelta + midRegCorrection;

	Rects[4].Left = W / 2 - W / 2;
	Rects[4].Right = W / 2 - S;
	Rects[4].Top = midRegStart + midRegDelta;
	Rects[4].Bottom = midRegStart + midRegDelta*2 + midRegCorrection;

	Rects[5].Left = S + W / 2;
	Rects[5].Right = W / 2 + W / 2;
	Rects[5].Top = midRegStart + midRegDelta;
	Rects[5].Bottom = midRegStart + midRegDelta*2 + midRegCorrection;

	Rects[6].Left = W / 2 - W / 2;
	Rects[6].Right = W / 2 - S;
	Rects[6].Top = midRegStart + midRegDelta*2;
	Rects[6].Bottom = midRegStart + midRegDelta*3 + midRegCorrection;

	Rects[7].Left = S + W / 2;
	Rects[7].Right = W / 2 + W / 2;
	Rects[7].Top = midRegStart + midRegDelta*2;
	Rects[7].Bottom = midRegStart + midRegDelta*3 + midRegCorrection;

	Rects[8].Left = W / 2 - W / 4;
	Rects[8].Right = W / 2 + W / 4;
	//Modifyed by KirillB
	Rects[8].Top = 3 * H / 4;
	Rects[8].Bottom = H;

	int RailWidth = S*2;
	int RailHeight = S*3.2;
	setRailRect(TRect(W/2 - RailWidth/2, H/2 - RailHeight/2, W/2 + RailWidth/2, H/2 + RailHeight/2));
	setRailOrientation( bLeftToRightRailLayout );

	if(!bLeftToRightBScanLayout)
	{
		TRect tempRect;
		//swap 2 and 3
		tempRect = Rects[2];
		Rects[2] = Rects[3];
		Rects[3] = tempRect;

		//swap 4 and 5
		tempRect = Rects[4];
		Rects[4] = Rects[5];
		Rects[5] = tempRect;

		//swap 6 and 7
		tempRect = Rects[6];
		Rects[6] = Rects[7];
		Rects[7] = tempRect;
	};

	//------------Applying BScan rects----------------

	for (int idx = 1; idx < 9; idx++)
	{
		Rects[idx] = setBScanPageRect(Rects[idx],idx);
	}

	//---------Calculating optimal hardpoints---------
	TPoint RectPoint;
	TPoint RectNormal;
	TPoint RailDrawOffset(W / 2 - railDraw.getBuffer()->Width / 2,
						   H / 2 - railDraw.getBuffer()->Height / 2);

	for (int idx = 1; idx < 9; idx++)
	{
		int rectLeft = bLeftToRightBScanLayout ? Rects[idx].Left : Rects[idx].Right;
		int rectRight = bLeftToRightBScanLayout ? Rects[idx].Right : Rects[idx].Left;
		int normalMult = bLeftToRightBScanLayout ? 1 : -1;

		switch(idx)
		{
			case 1: //KP1
				RectPoint = TPoint(Rects[idx].CenterPoint().x,Rects[idx].Bottom);
				RectNormal = TPoint(0,1);
				break;
			case 2: //KP2
				RectPoint = TPoint(rectRight,Rects[idx].CenterPoint().y);
				RectNormal = TPoint(1*normalMult,0);
				break;
			case 3: //KP3
				RectPoint = TPoint(rectLeft,Rects[idx].CenterPoint().y);
				RectNormal = TPoint(-1*normalMult,0);
				break;
			case 4: //KP4
				RectPoint = TPoint(rectRight,Rects[idx].CenterPoint().y);
				RectNormal = TPoint(1*normalMult,0);
				break;
			case 5: //KP5
				RectPoint = TPoint(rectLeft,Rects[idx].CenterPoint().y);
				RectNormal = TPoint(-1*normalMult,0);
				break;
			case 6: //KP6
				RectPoint = TPoint(rectRight,Rects[idx].CenterPoint().y);
				RectNormal = TPoint(1*normalMult,0);
				break;
			case 7: //KP7
				RectPoint = TPoint(rectLeft,Rects[idx].CenterPoint().y);
				RectNormal = TPoint(-1*normalMult,0);
				break;
			case 8: //KP8
				RectPoint = TPoint(Rects[idx].CenterPoint().x,Rects[idx].Top);
				RectNormal = TPoint(0,-1);
				break;
		};

		railDraw.calculateOptimalHardpoint(idx-1,RailDrawOffset,RectPoint, RectNormal);
	}

    railDraw.update(true);
	//------------------------------------------------

	pScanDraw->Prepare();
}

void cBScanPainter::clearBScan()
{
    if(!bInitialized)
        return;
    for(int idx = 1; idx < 9; idx++)
        for (int LineIdx = 0; LineIdx < scanLines[idx]->BScanLineCount; LineIdx++)
            for (int BtnIdx = 0; BtnIdx < scanLines[idx]->BtnColumnCount * 2; BtnIdx++)
            {
                scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] = HIGHLIGHT_NONE;
            }

    railDraw.setColor(0xFFFFFFFF, clWhite);
    for(int i = 1; i < 9; i++)
    {
        railDraw.setColor(RAIL_HGL_KP_SURF(i), clBlack);
    }
}

void cBScanPainter::setBScanRect(TRect rect)
{
    if(!bInitialized)
        return;
	if(BScanRect == rect)
		return;

	BScanRect = rect;
	applyBScanLayout();
}

void cBScanPainter::updateBScanPages(bool bNewItemsFlag)
{
	pScanDraw->Draw(bNewItemsFlag); //draw signals
}

void cBScanPainter::updateBScanPage(int idx, bool bNewItemsFlag, bool bUpdateSearchHighlight, bool bUpdateTuneHighlight)
{
	if(bUpdateSearchHighlight)
		updateSearchHighlight(idx);
	if(bUpdateTuneHighlight)
		updateTuneHighlight(idx);

	pScanDraw->Draw(bNewItemsFlag);
}

bool IsScanWorkCycle(int wc)
{
    if((wc == 3) || (wc == 5) || (wc == 6) || (wc == 8) || (wc == 10) ||
        (wc == 12) || (wc == 14))
            return true;
    return false;
}

void cBScanPainter::updateHightlight(bool bForceUpdate)
{
    bool bNeedUpdateHideChannels = false;
    //Updates every work cycle
	if((!AutoconMain->ac) || (prevUpdateWorkCycle != AutoconMain->ac->GetWorkCycle()) || (bForceUpdate))
	{
        if((currVisualMode == BS_VISUAL_SEARCH) && IsScanWorkCycle(prevUpdateWorkCycle))
        {
            LOGINFO("UpdateSurfChannelsState: %d",prevUpdateWorkCycle);
            Rep->CheckForLossContactDefects(AutoconMain->Calibration,prevUpdateWorkCycle);
        }
        prevUpdateWorkCycle = AutoconMain->ac ? AutoconMain->ac->GetWorkCycle() : 0;

		//Update tune highlight
		if( currVisualMode == BS_VISUAL_TUNE)
		{

            Rep->UpdateCtrlStateForTuneMode(AutoconMain->Table);
			for(int i = 1; i < 9; i++)
				updateTuneHighlight(i);
		}
        else
        {
            Rep->UpdateCtrlStateForSearchMode();
            for(int i = 1; i < 9; i++)
				updateSearchHighlight(i);
        }
        bNeedUpdateHideChannels = true;
	}

	// Update alarm indication
	if(Rep->CheckAlarmUpdated(true) || bForceUpdate)
	{
        if( currVisualMode != BS_VISUAL_TUNE)
        {
            for(int i = 1; i < 9; i++)
            {
                updateSearchHighlight(i);
                bNeedUpdateHideChannels = true;
            }
        }
	}

    if(bNeedUpdateHideChannels)
        updateBScanHideChannelsMode();

    for(int idx = 1; idx < 9; idx++)
        if(bNeedBufferRedraw[idx] || bForceUpdate)
        	scanLines[idx]->PaintToBuffer();
    memset(bNeedBufferRedraw,0,sizeof(bNeedBufferRedraw));
}

void cBScanPainter::updateRail()
{
	railDraw.update();
}

void cBScanPainter::updateBScan(bool bNewItemsFlag)
{
    updateHightlight(false);
	updateBScanPages(bNewItemsFlag);
	updateRail();
}

void cBScanPainter::setHideChannelsMode(eChannelHideMode mode)
{
    chanHideMode = mode;
    updateBScanHideChannelsMode();
    for(int idx = 1; idx < 9; idx++)
        scanLines[idx]->PaintToBuffer();
}

void cBScanPainter::updateBScanHideChannelsMode()
{
    sScanChannelDescription desc;
    for(int idx = 1; idx < 9; idx++)
        for (int LineIdx = 0; LineIdx < scanLines[idx]->BScanLineCount; LineIdx++)
            for (int BtnIdx = 0; BtnIdx < scanLines[idx]->BtnColumnCount * 2; BtnIdx++)
            {
                if(!scanLines[idx]->BtnVisible[BtnIdx][LineIdx])
                    continue;

                if(chanHideMode == CFM_HIDE_NONE)
                {
                    //Update flag
                    bNeedBufferRedraw[idx] |= !scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx];

                    scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx] = true;
                }
                else if(chanHideMode == CFM_HIDE_INACTIVE)
                {
                    int btnId = scanLines[idx]->BtnId[BtnIdx][LineIdx];

                    unsigned int i;
                    for( i = 0; i < AutoconMain->Config->GetAllScanChannelsCount(); i++)
                    {
                        if(!AutoconMain->Config->getSChannelbyIdx(i,&desc))
                            continue;
                        if(desc.Id != btnId)
                            continue;
                        if( currVisualMode == BS_VISUAL_SEARCH)
                            if(desc.StrokeGroupIdx == (AutoconMain->ac ? AutoconMain->ac->GetWorkCycle() : 0))
                                break;
                        if( currVisualMode == BS_VISUAL_TUNE)
                            if(desc.StrokeGroupIdx == (AutoconMain->DEV ? AutoconMain->DEV->GetChannelGroup() : 0))
                                break;
                    }

                    bool bEnable = (i !=  AutoconMain->Config->GetAllScanChannelsCount());
                    //Update flag
                    bNeedBufferRedraw[idx] |= (scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx] != bEnable);

                    scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx] = bEnable;

                    //scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx] =
                     //    btnId && AutoconMain->DEV->ChannelInCurrentGroup(btnId);

                    /*if(AutoconMain->Config->getSChannelbyID(scanLines[idx]->BtnId[BtnIdx][LineIdx], &desc))
                        sg = desc.StrokeGroupIdx;
                    else
                        sg = -1;

                    scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx] = (sg == AutoconMain->ac->GetWorkCycle());*/
                }
                else if(chanHideMode == CFM_HIDE_WITH_NO_DEFECTS)
                {
                    if((scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] == HIGHLIGHT_WARNING) ||
                        (scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] == HIGHLIGHT_ERROR))
                    {
                        //Update flag
                        bNeedBufferRedraw[idx] |= !scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx];
                        scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx] = true;
                    }
                    else
                    {
                        //Update flag
                        bNeedBufferRedraw[idx] |= scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx];

                        scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx] = false;
                    }
                }
                else if(chanHideMode == CFM_HIDE_ALL)
                {
                    //Update flag
                    bNeedBufferRedraw[idx] |= scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx];

                    scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx] = false;
                }
                else if(chanHideMode == CFM_HIDE_SELECTED)
                {
                    //Update flag
                    int chId = scanLines[idx]->BtnId[BtnIdx][LineIdx];
                    bool chState = scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx];
                    bool newChState = false;

                    if(chId < (int)chHideState.size())
                        newChState = !chHideState[chId];
                    else
                        newChState = false;

                    if(chState != newChState)
                    {
                        bNeedBufferRedraw[idx] = true;
                        scanLines[idx]->BtnChannelIsActive[BtnIdx][LineIdx] = newChState;
                    }
                }
            }
}

void cBScanPainter::setChannelHide(unsigned int chId, bool bHide)
{
    if( chId >= chHideState.size() )
        chHideState.resize(chId + 1, true);

    chHideState[chId] = bHide;
}

void cBScanPainter::setAllChannelsHide(bool bVal)
{
    for(unsigned int i = 0; i < chHideState.size(); i++)
    {
        chHideState[i] = bVal;
    }
}

void cBScanPainter::updateTuneHighlight(int idx)
{
    for (int LineIdx = 0; LineIdx < scanLines[idx]->BScanLineCount; LineIdx++)
		for (int BtnIdx = 0; BtnIdx < scanLines[idx]->BtnColumnCount * 2; BtnIdx++)
		{
			if(!scanLines[idx]->BtnVisible[BtnIdx][LineIdx])
				continue;

			int chId = scanLines[idx]->BtnId[BtnIdx][LineIdx];

            switch(Rep->GetChannelCtrlData(chId)->controlState)
            {
                case -1:
                    bNeedBufferRedraw[idx] |= (scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] != HIGHLIGHT_NONE);
					scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] = HIGHLIGHT_NONE;
                    break;
                case 0:
                    bNeedBufferRedraw[idx] |= (scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] != HIGHLIGHT_OK);
					scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] = HIGHLIGHT_OK;
                    break;
                case 1:
                    bNeedBufferRedraw[idx] |= (scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] != HIGHLIGHT_WARNING);
					scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] = HIGHLIGHT_WARNING;
                    break;
                case 2:
                    bNeedBufferRedraw[idx] |= (scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] != HIGHLIGHT_ERROR);
					scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] = HIGHLIGHT_ERROR;
                    break;
            }

        }
}

void cBScanPainter::updateSearchHighlight(int idx)
{
#ifndef DISABLE_DEFECT_HIGHLIGHTING
    bool bIsKPAlarmed = false;
    int kpSurfChanId = Rep->GetSurfaceChannelIdByKP(idx);
    bool bIsSurfaceAlarmed = Rep->GetChannelCtrlData(kpSurfChanId)->bHasLossContactDefect;

    for (int LineIdx = 0; LineIdx < scanLines[idx]->BScanLineCount; LineIdx++)
		for (int BtnIdx = 0; BtnIdx < scanLines[idx]->BtnColumnCount * 2; BtnIdx++)
		{
			if(!scanLines[idx]->BtnVisible[BtnIdx][LineIdx])
				continue;

            int chId = scanLines[idx]->BtnId[BtnIdx][LineIdx];
            bool bChannelAlarmed = Rep->GetChannelCtrlData(chId)->controlState > 0;
            bIsKPAlarmed |= bChannelAlarmed;

            if(currVisualMode != BS_VISUAL_TUNE)
			{
				if(bChannelAlarmed)
                {
					//Update flag
                    bNeedBufferRedraw[idx] |= (scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] != HIGHLIGHT_ERROR);
					scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] = HIGHLIGHT_ERROR;
                }
				else
                {
					//Update flag
                    bNeedBufferRedraw[idx] |= (scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] != HIGHLIGHT_OK);
					scanLines[idx]->BtnHighlight[BtnIdx][LineIdx] = HIGHLIGHT_OK;
                }
			}
        }

    if(bIsKPAlarmed)
		railDraw.setColor(1<<(idx-1),clRed);
	else
		railDraw.setColor(1<<(idx-1),clLime);

	if(!bIsSurfaceAlarmed)
		railDraw.setColor(cDrawRailUnit::RAIL_HGL_KP1_SURF<<(idx-1),RGB(0,176,80));
	else
		railDraw.setColor(cDrawRailUnit::RAIL_HGL_KP1_SURF<<(idx-1),RGB(200,0,0));
#else
    railDraw.setColor(1<<(idx-1),clWhite);
    railDraw.setColor(cDrawRailUnit::RAIL_HGL_KP1_SURF<<(idx-1),clBlack);
#endif
}

int cBScanPainter::click(unsigned int nPage, int x, int y)
{
	return scanLines[nPage]->Click(x - scanLines[nPage]->BoundsRect.left,
									y - scanLines[nPage]->BoundsRect.top);
}

void cBScanPainter::drawBScan(TCanvas* pCanvas)
{
	for (int idx = 1; idx < 9; idx++)
	{
		drawBScanPage(idx,pCanvas);
	}

	drawRail(0, true, pCanvas); //draw bitmap to canvas
}

void cBScanPainter::drawBScanPage(unsigned int nPage, TCanvas* pCanvas)
{
	if (scanLines[nPage]->Buffer)
	{
		TRect rect = scanLines[nPage]->BoundsRect;
		pCanvas->Draw(rect.left, rect.top, scanLines[nPage]->Buffer); //draw bitmap to canvas
	}
}

void cBScanPainter::drawRail(UINT UnvisibleFlags,bool bDrawHardpoints, TCanvas* pCanvas)
{
	//Draw rail
	pCanvas->Draw(RailRect.left,
					RailRect.top,
					railDraw.getBuffer());

	//Draw hardpoints
	if(bDrawHardpoints)
	{
		pCanvas->Brush->Style = bsClear;
		for (int idx = 1; idx < 9; idx++)
		{
			//Draw lines
			TPoint startp = railDraw.getBScanHardpoint(idx-1);
			TPoint endp = railDraw.getOptimalHardpoint(idx-1);
			TPoint midp1 = railDraw.getOptimalMidpoint(idx-1,0);
			TPoint midp2 = railDraw.getOptimalMidpoint(idx-1,1);

			pCanvas->Pen->Width = 3;
			pCanvas->Pen->Color = railDraw.getHardpointColor(idx-1);
			pCanvas->MoveTo(startp.x,startp.y);
			pCanvas->LineTo(midp1.x,midp1.y);
			pCanvas->MoveTo(midp1.x,midp1.y);
			pCanvas->LineTo(midp2.x,midp2.y);
			pCanvas->MoveTo(midp2.x,midp2.y);
			pCanvas->LineTo(endp.x,endp.y);
		}
	}
}
