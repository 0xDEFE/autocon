object AScanViewForm: TAScanViewForm
  Left = 0
  Top = 0
  BorderStyle = bsNone
  BorderWidth = 5
  Caption = #1040' '#1088#1072#1079#1074#1077#1088#1090#1082#1072
  ClientHeight = 529
  ClientWidth = 758
  Color = clBlue
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 758
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    object ValueCountCheckBox: TCheckBox
      Left = 67
      Top = 0
      Width = 247
      Height = 33
      Align = alLeft
      Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1086#1090#1089#1095#1077#1090#1086#1074' '#1072#1084#1087#1083#1080#1090#1091#1076#1099': 256 / 16'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Panel2: TPanel
      Left = 536
      Top = 0
      Width = 14
      Height = 33
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
    end
    object PointsCheckBox: TCheckBox
      Left = 16
      Top = 0
      Width = 51
      Height = 33
      Align = alLeft
      Caption = #1059#1079#1083#1099
      TabOrder = 2
      OnClick = PointsCheckBoxClick
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 16
      Height = 33
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 3
    end
    object Panel4: TPanel
      Left = 550
      Top = 0
      Width = 14
      Height = 33
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 4
    end
    object DelayCountCheckBox: TCheckBox
      Left = 314
      Top = 0
      Width = 222
      Height = 33
      Align = alLeft
      Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1086#1090#1089#1095#1077#1090#1086#1074' '#1079#1072#1076#1077#1088#1078#1082#1080': 24 / 8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 33
    Width = 758
    Height = 496
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel7'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -26
    Font.Name = 'Verdana'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 0
      Top = 41
      Width = 758
      Height = 455
      Margins.Top = 0
      ActivePage = InfoTabSheet
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Style = tsButtons
      TabOrder = 0
      object ChartTabSheet: TTabSheet
        Caption = 'ChartTabSheet'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        object ButtonsLabel: TLabel
          Left = 0
          Top = 361
          Width = 750
          Height = 22
          Align = alBottom
          Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1089#1090#1088#1086#1073' '#1076#1083#1103' '#1082#1072#1085#1072#1083#1072':'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -18
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitWidth = 230
        end
        object AscanChart: TChart
          Left = 0
          Top = 0
          Width = 750
          Height = 361
          AllowPanning = pmNone
          Legend.Visible = False
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          BottomAxis.Automatic = False
          BottomAxis.AutomaticMaximum = False
          BottomAxis.AutomaticMinimum = False
          BottomAxis.LabelsFormat.TextAlignment = taCenter
          BottomAxis.Maximum = 24.000000000000000000
          BottomAxis.Title.Caption = #1084#1082#1089
          DepthAxis.LabelsFormat.TextAlignment = taCenter
          DepthTopAxis.LabelsFormat.TextAlignment = taCenter
          LeftAxis.Automatic = False
          LeftAxis.AutomaticMaximum = False
          LeftAxis.AutomaticMinimum = False
          LeftAxis.LabelsFormat.TextAlignment = taCenter
          LeftAxis.Maximum = 18.000000000000000000
          LeftAxis.Minimum = -12.000000000000000000
          LeftAxis.Title.Angle = 0
          LeftAxis.Title.Caption = #1076#1041
          RightAxis.LabelsFormat.TextAlignment = taCenter
          TopAxis.LabelsFormat.TextAlignment = taCenter
          View3D = False
          Zoom.Allow = False
          Zoom.Pen.Mode = pmNotXor
          Align = alClient
          BevelOuter = bvNone
          Color = clWhite
          TabOrder = 0
          OnClick = AscanChartClick
          DefaultCanvas = 'TGDIPlusCanvas'
          PrintMargins = (
            15
            22
            15
            22)
          ColorPaletteIndex = 1
          object AScanSeries1: TLineSeries
            Marks.Visible = False
            Brush.BackColor = clDefault
            LinePen.Width = 2
            LinePen.SmallSpace = 1
            Pointer.Brush.Gradient.EndColor = 10708548
            Pointer.Gradient.EndColor = 10708548
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object AScanSeries2: TLineSeries
            Marks.Visible = False
            SeriesColor = clRed
            Brush.BackColor = clDefault
            LinePen.Width = 2
            Pointer.Brush.Gradient.EndColor = clRed
            Pointer.Gradient.EndColor = clRed
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object AScanSeries3: TLineSeries
            Marks.Visible = False
            SeriesColor = 65408
            Brush.BackColor = clDefault
            LinePen.Width = 2
            Pointer.Brush.Gradient.EndColor = 65408
            Pointer.Gradient.EndColor = 65408
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object AScanSeries4: TLineSeries
            Marks.Visible = False
            SeriesColor = clYellow
            Brush.BackColor = clDefault
            LinePen.Width = 2
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object AScanSeries5: TLineSeries
            Marks.Visible = False
            SeriesColor = 16777088
            Brush.BackColor = clDefault
            LinePen.Width = 2
            Pointer.Brush.Gradient.EndColor = 16777088
            Pointer.Gradient.EndColor = 16777088
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object AScanSeries6: TLineSeries
            Marks.Visible = False
            SeriesColor = 33023
            Brush.BackColor = clDefault
            LinePen.Width = 2
            Pointer.Brush.Gradient.EndColor = 33023
            Pointer.Gradient.EndColor = 33023
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object GateStSeries1: TChartShape
            Marks.Visible = False
            SeriesColor = 16744448
            Title = 'GateStSeries1'
            Brush.Color = 16744448
            Pen.Color = 1962934272
            Pen.Visible = False
            Style = chasRectangle
            X0 = 3.000000000000000000
            X1 = 3.200000000000000000
            Y0 = 0.500000000000000000
            Y1 = -0.700000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000000000000000000840000000000000E03F9A99999999990940666666
              666666E6BF}
          end
          object GateEdSeries1: TChartShape
            Marks.Visible = False
            SeriesColor = 16744448
            Title = 'GateEdSeries1'
            Brush.Color = 16744448
            Pen.Color = 1912602624
            Pen.Visible = False
            Style = chasRectangle
            X0 = 20.800000000000000000
            X1 = 21.000000000000000000
            Y0 = 0.500000000000000000
            Y1 = -0.700000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              0102000000CDCCCCCCCCCC3440000000000000E03F0000000000003540666666
              666666E6BF}
          end
          object GateMainSeries1: TChartShape
            Marks.Visible = False
            SeriesColor = 16744448
            Title = 'GateMainSeries1'
            Brush.Color = 16744448
            Pen.Visible = False
            Style = chasRectangle
            X0 = 3.000000000000000000
            X1 = 21.000000000000000000
            Y1 = -0.300000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              0102000000000000000000084000000000000000000000000000003540333333
              333333D3BF}
          end
          object GateStSeries2: TChartShape
            Marks.Visible = False
            SeriesColor = 16744448
            Title = 'GateStSeries2'
            Brush.Color = 16744448
            Pen.Visible = False
            Style = chasRectangle
            X0 = 21.300000000000000000
            X1 = 21.500000000000000000
            Y0 = 0.500000000000000000
            Y1 = -0.700000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              0102000000CDCCCCCCCC4C3540000000000000E03F0000000000803540666666
              666666E6BF}
          end
          object GateEdSeries2: TChartShape
            Marks.Visible = False
            SeriesColor = 16744448
            Title = 'GateEdSeries2'
            Brush.Color = 16744448
            Pen.SmallSpace = 1
            Pen.Visible = False
            Style = chasRectangle
            X0 = 23.700000000000000000
            X1 = 23.900000000000000000
            Y0 = 0.500000000000000000
            Y1 = -0.700000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000003333333333B33740000000000000E03F6666666666E63740666666
              666666E6BF}
          end
          object GateMainSeries2: TChartShape
            Marks.Visible = False
            SeriesColor = 16744448
            Title = 'GateMainSeries2'
            Brush.Color = 16744448
            Pen.Visible = False
            Style = chasRectangle
            X0 = 21.300000000000000000
            X1 = 23.700000000000000000
            Y1 = -0.300000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              0102000000CDCCCCCCCC4C354000000000000000803333333333B33740333333
              333333D3BF}
          end
        end
        object ButtonsPanel: TGridPanel
          Left = 0
          Top = 383
          Width = 750
          Height = 41
          Align = alBottom
          ColumnCollection = <
            item
              Value = 16.666670462937560000
            end
            item
              Value = 16.666670462937560000
            end
            item
              Value = 16.666663379321970000
            end
            item
              Value = 16.666664400285540000
            end
            item
              Value = 16.666665273680680000
            end
            item
              Value = 16.666666020836700000
            end>
          ControlCollection = <
            item
              Column = 1
              Control = SelGateBtn2
              Row = 0
            end
            item
              Column = 2
              Control = SelGateBtn3
              Row = 0
            end
            item
              Column = 3
              Control = SelGateBtn4
              Row = 0
            end
            item
              Column = 4
              Control = SelGateBtn5
              Row = 0
            end
            item
              Column = 5
              Control = SelGateBtn6
              Row = 0
            end
            item
              Column = 0
              Control = SelGateBtn1
              Row = 0
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          RowCollection = <
            item
              Value = 100.000000000000000000
            end>
          TabOrder = 1
          object SelGateBtn2: TSpeedButton
            Tag = 1
            AlignWithMargins = True
            Left = 127
            Top = 1
            Width = 120
            Height = 39
            Margins.Left = 2
            Margins.Top = 0
            Margins.Right = 2
            Margins.Bottom = 0
            Align = alClient
            GroupIndex = 10
            Caption = #1050#1055'1 65 '#1054'2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = OnSelGateBtnClicked
            ExplicitLeft = 176
            ExplicitTop = 32
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object SelGateBtn3: TSpeedButton
            Tag = 2
            AlignWithMargins = True
            Left = 251
            Top = 1
            Width = 120
            Height = 39
            Margins.Left = 2
            Margins.Top = 0
            Margins.Right = 2
            Margins.Bottom = 0
            Align = alClient
            GroupIndex = 10
            Caption = #1050#1055'1 65 '#1054'2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = OnSelGateBtnClicked
            ExplicitLeft = 176
            ExplicitTop = 32
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object SelGateBtn4: TSpeedButton
            Tag = 3
            AlignWithMargins = True
            Left = 375
            Top = 1
            Width = 120
            Height = 39
            Margins.Left = 2
            Margins.Top = 0
            Margins.Right = 2
            Margins.Bottom = 0
            Align = alClient
            GroupIndex = 10
            Caption = #1050#1055'1 65 '#1054'2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = OnSelGateBtnClicked
            ExplicitLeft = 176
            ExplicitTop = 32
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object SelGateBtn5: TSpeedButton
            Tag = 4
            AlignWithMargins = True
            Left = 499
            Top = 1
            Width = 120
            Height = 39
            Margins.Left = 2
            Margins.Top = 0
            Margins.Right = 2
            Margins.Bottom = 0
            Align = alClient
            GroupIndex = 10
            Caption = #1050#1055'1 65 '#1054'2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = OnSelGateBtnClicked
            ExplicitLeft = 176
            ExplicitTop = 32
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object SelGateBtn6: TSpeedButton
            Tag = 5
            AlignWithMargins = True
            Left = 623
            Top = 1
            Width = 124
            Height = 39
            Margins.Left = 2
            Margins.Top = 0
            Margins.Right = 2
            Margins.Bottom = 0
            Align = alClient
            GroupIndex = 10
            Caption = #1050#1055'1 65 '#1054'2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = OnSelGateBtnClicked
            ExplicitLeft = 176
            ExplicitTop = 32
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
          object SelGateBtn1: TSpeedButton
            AlignWithMargins = True
            Left = 3
            Top = 1
            Width = 120
            Height = 39
            Margins.Left = 2
            Margins.Top = 0
            Margins.Right = 2
            Margins.Bottom = 0
            Align = alClient
            GroupIndex = 10
            Down = True
            Caption = #1050#1055'1 65 '#1054'2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = OnSelGateBtnClicked
            ExplicitLeft = 176
            ExplicitTop = 32
            ExplicitWidth = 23
            ExplicitHeight = 22
          end
        end
      end
      object InfoTabSheet: TTabSheet
        Caption = 'InfoTabSheet'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        object GridPanel3: TGridPanel
          Left = 0
          Top = 0
          Width = 750
          Height = 424
          Align = alClient
          ColumnCollection = <
            item
              Value = 63.581842710725350000
            end
            item
              SizeStyle = ssAbsolute
              Value = 15.000000000000000000
            end
            item
              Value = 36.418157289274650000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = m_ChannelNameNameLabel
              Row = 0
            end
            item
              Column = 0
              Control = m_KuNameLabel
              Row = 1
            end
            item
              Column = 0
              Control = m_AttNameLabel
              Row = 2
            end
            item
              Column = 0
              Control = m_StGateNameLabel
              Row = 3
            end
            item
              Column = 0
              Control = m_EdGateNameLabel
              Row = 4
            end
            item
              Column = 0
              Control = m_TVGNameLabel
              Row = 5
            end
            item
              Column = 2
              Control = m_ChannelNameLabel
              Row = 0
            end
            item
              Column = 2
              Control = m_KuLabel
              Row = 1
            end
            item
              Column = 2
              Control = m_AttLabel
              Row = 2
            end
            item
              Column = 2
              Control = m_StGateLabel
              Row = 3
            end
            item
              Column = 2
              Control = m_EdGateLabel
              Row = 4
            end
            item
              Column = 2
              Control = m_TVGLabel
              Row = 5
            end
            item
              Column = 0
              Control = m_PrismDelayNameLabel
              Row = 6
            end
            item
              Column = 2
              Control = m_PrismDelayLabel
              Row = 6
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -26
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          RowCollection = <
            item
              Value = 14.285714297559450000
            end
            item
              Value = 14.285714257281910000
            end
            item
              Value = 14.285714170439890000
            end
            item
              Value = 14.285714300779260000
            end
            item
              Value = 14.285714253711010000
            end
            item
              Value = 14.285714368409890000
            end
            item
              Value = 14.285714351818590000
            end>
          TabOrder = 0
          object m_ChannelNameNameLabel: TLabel
            Left = 1
            Top = 1
            Width = 466
            Height = 60
            Align = alClient
            Alignment = taRightJustify
            BiDiMode = bdLeftToRight
            Caption = #1048#1084#1103' '#1082#1072#1085#1072#1083#1072':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -26
            Font.Name = 'Verdana'
            Font.Style = []
            ParentBiDiMode = False
            ParentFont = False
            ExplicitLeft = 296
            ExplicitWidth = 171
            ExplicitHeight = 32
          end
          object m_KuNameLabel: TLabel
            Left = 1
            Top = 61
            Width = 466
            Height = 60
            Align = alClient
            Alignment = taRightJustify
            BiDiMode = bdLeftToRight
            Caption = #1059#1089#1083#1086#1074#1085#1072#1103' '#1095#1091#1074#1089#1090#1074#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -26
            Font.Name = 'Verdana'
            Font.Style = []
            ParentBiDiMode = False
            ParentFont = False
            ExplicitLeft = 81
            ExplicitWidth = 386
            ExplicitHeight = 32
          end
          object m_AttNameLabel: TLabel
            Left = 1
            Top = 121
            Width = 466
            Height = 60
            Align = alClient
            Alignment = taRightJustify
            BiDiMode = bdLeftToRight
            Caption = #1040#1090#1090#1077#1085#1102#1072#1090#1086#1088':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -26
            Font.Name = 'Verdana'
            Font.Style = []
            ParentBiDiMode = False
            ParentFont = False
            ExplicitLeft = 296
            ExplicitWidth = 171
            ExplicitHeight = 32
          end
          object m_StGateNameLabel: TLabel
            Left = 1
            Top = 181
            Width = 466
            Height = 60
            Align = alClient
            Alignment = taRightJustify
            BiDiMode = bdLeftToRight
            Caption = #1053#1072#1095#1072#1083#1086' '#1089#1090#1088#1086#1073#1072' '#1040#1057#1044':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -26
            Font.Name = 'Verdana'
            Font.Style = []
            ParentBiDiMode = False
            ParentFont = False
            ExplicitLeft = 191
            ExplicitWidth = 276
            ExplicitHeight = 32
          end
          object m_EdGateNameLabel: TLabel
            Left = 1
            Top = 241
            Width = 466
            Height = 60
            Align = alClient
            Alignment = taRightJustify
            BiDiMode = bdLeftToRight
            Caption = #1050#1086#1085#1077#1094' '#1089#1090#1088#1086#1073#1072' '#1040#1057#1044':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -26
            Font.Name = 'Verdana'
            Font.Style = []
            ParentBiDiMode = False
            ParentFont = False
            ExplicitLeft = 208
            ExplicitWidth = 259
            ExplicitHeight = 32
          end
          object m_TVGNameLabel: TLabel
            Left = 1
            Top = 301
            Width = 466
            Height = 60
            Align = alClient
            Alignment = taRightJustify
            BiDiMode = bdLeftToRight
            Caption = #1042#1056#1063':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -26
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentFont = False
            ExplicitLeft = 412
            ExplicitWidth = 55
            ExplicitHeight = 31
          end
          object m_ChannelNameLabel: TLabel
            Left = 482
            Top = 1
            Width = 267
            Height = 60
            Align = alClient
            BiDiMode = bdLeftToRight
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -26
            Font.Name = 'Verdana'
            Font.Style = []
            ParentBiDiMode = False
            ParentFont = False
            ExplicitWidth = 27
            ExplicitHeight = 32
          end
          object m_KuLabel: TLabel
            Left = 482
            Top = 61
            Width = 267
            Height = 60
            Align = alClient
            BiDiMode = bdLeftToRight
            Caption = '...'
            ParentBiDiMode = False
            ExplicitWidth = 27
            ExplicitHeight = 32
          end
          object m_AttLabel: TLabel
            Left = 482
            Top = 121
            Width = 267
            Height = 60
            Align = alClient
            BiDiMode = bdLeftToRight
            Caption = '...'
            ParentBiDiMode = False
            ExplicitWidth = 27
            ExplicitHeight = 32
          end
          object m_StGateLabel: TLabel
            Left = 482
            Top = 181
            Width = 267
            Height = 60
            Align = alClient
            BiDiMode = bdLeftToRight
            Caption = '...'
            ParentBiDiMode = False
            ExplicitWidth = 27
            ExplicitHeight = 32
          end
          object m_EdGateLabel: TLabel
            Left = 482
            Top = 241
            Width = 267
            Height = 60
            Align = alClient
            BiDiMode = bdLeftToRight
            Caption = '...'
            ParentBiDiMode = False
            ExplicitWidth = 27
            ExplicitHeight = 32
          end
          object m_TVGLabel: TLabel
            Left = 482
            Top = 301
            Width = 267
            Height = 60
            Align = alClient
            BiDiMode = bdLeftToRight
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -26
            Font.Name = 'Verdana'
            Font.Style = []
            ParentBiDiMode = False
            ParentFont = False
            ExplicitWidth = 27
            ExplicitHeight = 32
          end
          object m_PrismDelayNameLabel: TLabel
            Left = 1
            Top = 361
            Width = 466
            Height = 62
            Align = alClient
            Alignment = taRightJustify
            BiDiMode = bdLeftToRight
            Caption = #1042#1088#1077#1084#1103' '#1074' '#1087#1088#1080#1079#1084#1077':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -26
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBiDiMode = False
            ParentFont = False
            ExplicitLeft = 267
            ExplicitWidth = 200
            ExplicitHeight = 31
          end
          object m_PrismDelayLabel: TLabel
            Left = 482
            Top = 361
            Width = 267
            Height = 62
            Align = alClient
            BiDiMode = bdLeftToRight
            Caption = '...'
            ParentBiDiMode = False
            ExplicitWidth = 27
            ExplicitHeight = 32
          end
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 758
      Height = 41
      Align = alTop
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = ' '#1050#1086#1086#1088#1076#1080#1085#1072#1090#1072': 0 '#1084#1084
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -21
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 472
        Top = 0
        Width = 286
        Height = 41
        Align = alRight
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Button1: TButton
          AlignWithMargins = True
          Left = 7
          Top = 1
          Width = 84
          Height = 39
          Margins.Top = 1
          Margins.Bottom = 1
          Align = alRight
          Caption = '<<'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = Button1Click
        end
        object Button2: TButton
          AlignWithMargins = True
          Left = 97
          Top = 1
          Width = 84
          Height = 39
          Margins.Top = 1
          Margins.Bottom = 1
          Align = alRight
          Caption = '>>'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = Button2Click
        end
        object Button3: TButton
          AlignWithMargins = True
          Left = 187
          Top = 1
          Width = 96
          Height = 39
          Margins.Top = 1
          Margins.Bottom = 1
          Align = alRight
          Caption = #1047#1072#1082#1088#1099#1090#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          OnClick = Button3Click
        end
      end
    end
  end
end
