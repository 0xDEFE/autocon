//---------------------------------------------------------------------------

#include <vcl.h>
#include <algorithm>
#pragma hdrstop

#include "ArchiveSelectDialUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TArchiveSelectDial *ArchiveSelectDial;
//---------------------------------------------------------------------------
__fastcall TArchiveSelectDial::TArchiveSelectDial(TComponent* Owner)
	: TForm(Owner)
{
	buttonSize = TPoint(240,200);
    MinWidth = 0;
}
//---------------------------------------------------------------------------
void __fastcall TArchiveSelectDial::FormShow(TObject *Sender)
{
	//cleanup
	selectedId = -1;


	int itemCount = selComponents.size();
	if(itemCount==0)
		return;

	const int horisontalItemsCount = 4;
    int horItemsCount =  0;
    int verItemsCount = 0;

    if( itemCount <= 4 )
    {
        horItemsCount = itemCount;
        verItemsCount = 1;
    }
    else if( itemCount <= 6 )
    {
        horItemsCount = 3;
        verItemsCount = 2;
    }
    else if( itemCount <= 8 )
    {
        horItemsCount = 4;
        verItemsCount = 2;
    }
    else if( itemCount <= 9 )
    {
        horItemsCount = 3;
        verItemsCount = 3;
    }
    else if( itemCount <= 12 )
    {
        horItemsCount = 4;
        verItemsCount = 3;
    }
    else if( itemCount <= 16 )
    {
        horItemsCount = 4;
        verItemsCount = 4;
    }
    else if( itemCount <= 20 )
    {
        horItemsCount = 5;
        verItemsCount = 4;
    }
    else
    {
	    horItemsCount = std::min(horisontalItemsCount, itemCount);
	    verItemsCount = ceil (float(itemCount) / float(horItemsCount));
    }

	this->Width  = std::max(horItemsCount * buttonSize.x + 10 + 6*horItemsCount,(long)MinWidth);
    //this->Width = std::max(this->Width, int(buttonSize.x * 1.5));
	this->Height = verItemsCount * buttonSize.y + 10 + 7*verItemsCount + 55;

	this->Left = Screen->Width/2 - this->Width/2;
	this->Top = Screen->Height/2 - this->Height/2;

	for(unsigned int i = 0; i<selComponents.size(); i++)
	{
		m_FlowPanel->InsertControl(selComponents[i].btn);
	}
}

UnicodeString AdjustTextWidth(TCanvas* pCanvas, TPoint textRect, UnicodeString& text)
{
	std::vector<UINT> carryPos;

	for(int i = 0; i < text.Length(); i++)
	{
		if(text.c_str()[i] == L' ')
			carryPos.push_back(i);
	}
	carryPos.push_back(text.Length());

	UnicodeString finalString;
	UnicodeString tempBuffer;
	tempBuffer+=text.SubString(0,carryPos[0]);

	for(unsigned int i = 0; i < carryPos.size()-1; i++)
	{
		UnicodeString substr = Trim(text.SubString(carryPos[i]+1,carryPos[i+1] - carryPos[i]));
		UnicodeString temp = tempBuffer + substr;

		if( pCanvas->TextWidth(temp) >= textRect.x)
		{
			finalString+= tempBuffer + L"\r\n";
			tempBuffer = substr;
		}
		else
		{
            tempBuffer += L" ";
			tempBuffer += substr;
		}
	}
	finalString+= tempBuffer;
	return finalString;
}

void TArchiveSelectDial::AddComponent(int id, UnicodeString text,TBitmap* pImage)
{
	sArchiveSelectComponent comp;
	comp.pImage = pImage;
	comp.text = text;
	comp.id = id;

	comp.btn = new TSpeedButton(this);

	comp.btn->Glyph = pImage;
	comp.btn->Width = buttonSize.x;
	comp.btn->Height = buttonSize.y;
	comp.btn->Font->Height = -30;
	comp.btn->Font->Style = TFontStyles() << fsBold;
	comp.btn->Font->Name = "Myriad Pro";
	comp.btn->Tag = id;
	comp.btn->AlignWithMargins = true;
	comp.btn->Layout = blGlyphTop;
	comp.btn->Transparent = true;

	this->Canvas->Font = comp.btn->Font;
    TPoint innerBtnSize = TPoint(buttonSize.X - 6, buttonSize.Y - 6);
	comp.btn->Caption = AdjustTextWidth(this->Canvas,innerBtnSize, text);

	TNotifyEvent notifyOnClick = &(this->m_SelectComponentClick);
	comp.btn->OnClick = notifyOnClick;

	selComponents.push_back(comp);
}

//---------------------------------------------------------------------------
void __fastcall TArchiveSelectDial::m_SelectComponentClick(TObject *Sender)
{
    selectedId = ((TSpeedButton*)Sender)->Tag;
    ModalResult = mrOk;
}
//---------------------------------------------------------------------------

int TArchiveSelectDial::ShowSelect()
{
	this->ShowModal();
	return selectedId;
}

int TArchiveSelectDial::GetSelectedComponentId()
{
	return selectedId;
}
void __fastcall TArchiveSelectDial::FormClose(TObject *Sender, TCloseAction &Action)
{
	//for(int i = 0; i<m_FlowPanel->ControlCount; i++)
	while(m_FlowPanel->ControlCount)
	{
		m_FlowPanel->RemoveControl(m_FlowPanel->Controls[0]);
	}

	for(unsigned int i = 0; i < selComponents.size(); i++)
	{
		if(selComponents[i].btn)
		{
			delete selComponents[i].btn;
			selComponents[i].btn = NULL;
		}
	}
	selComponents.clear();
}
//---------------------------------------------------------------------------

void __fastcall TArchiveSelectDial::m_CloseBtnClick(TObject *Sender)
{
    selectedId = -1;
    ModalResult = mrOk;
}
//---------------------------------------------------------------------------

void TArchiveSelectDial::SetDialCaption(const char* str)
{
     m_CaptionLabel->Caption = str;
}
