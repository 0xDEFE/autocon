﻿/**
 * @file ArchiveUnit.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef ArchiveUnitH
#define ArchiveUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Bde.DBTables.hpp>
#include <Data.DB.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.SQLite.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <Data.Win.ADODB.hpp>
#include <Data.DbxSqlite.hpp>
#include <Vcl.DBCtrls.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Datasnap.DBClient.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.ToolWin.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.DBCGrids.hpp>
#include <Datasnap.Provider.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------

struct sFilterQueryParameter
{
	sFilterQueryParameter()
	{
	};

	sFilterQueryParameter(UnicodeString name, TDateTime value)
	{
		this->name=name;
		this->value=value;
	};

	UnicodeString name;
	TDateTime value;
};

//! Класс предназначен для взаимодействия в базой данных и отображения списка отчетов
class TArchiveForm : public TForm
{
__published:	// IDE-managed Components
	TDBGrid *DBGrid1;
	TDBNavigator *DBNavigator1;
	TSpeedButton *m_UpBtn;
	TProgressBar *m_ProgressBar;
	TStaticText *m_StatusLabel;
	TSpeedButton *m_DownBtn;
	TSpeedButton *m_PrintBtn;
	TSpeedButton *m_ExportBtn;
	TSpeedButton *m_GraphBtn;
	TFlowPanel *m_RightBtnPanel;
	TStaticText *m_PageLabel;
	TGridPanel *GridPanel1;
	TADOConnection *ADOConnection1;
	TADOQuery *m_ComQuery;
	TADOQuery *m_ShowQuery;
	TDataSource *m_ComDataSource;
	TDataSource *m_ShowDataSource;
	TADOTable *ADOTable1;
	TToolBar *m_MainToolBar;
	TToolButton *m_ModeBtn;
	TToolButton *m_UpdateBtn;
	TToolButton *m_ClearBtn;
	TImageList *ImageList1;
	TToolButton *m_FindBtn;
	TToolButton *m_PropertiesBtn;
	TToolButton *ToolButton7;
	TToolButton *ToolButton8;
	TToolButton *ToolButton9;
	TToolButton *ToolButton10;
	TToolButton *ToolButton2;
	TToolButton *m_CloseBtn;
	TToolButton *ToolButton1;
	TImageList *ImageList2;
	TPrintDialog *PrintDialog1;
    TPanel *Panel1;
    TPanel *SpacerPanel1;
    TLabel *m_ProtocolLabel;
    TPanel *Panel2;
    TLabel *m_FilterLabel;
    TTimer *UpdateUITimer;
    TSpeedButton *m_CompareBtn;
    TSpeedButton *m_SelectBtn;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall DBGrid1TitleClick(TColumn *Column);
	void __fastcall m_UpdateBtnClick(TObject *Sender);
	void __fastcall m_ClearBtnClick(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall m_UpBtnClick(TObject *Sender);
	void __fastcall m_DownBtnClick(TObject *Sender);
	void __fastcall m_FindBtnClick(TObject *Sender);
	void __fastcall m_GraphBtnClick(TObject *Sender);
	void __fastcall m_PropertiesBtnClick(TObject *Sender);
	void __fastcall m_ModeBtnClick(TObject *Sender);
	void __fastcall m_PrintBtnClick(TObject *Sender);
	void __fastcall m_CloseBtnClick(TObject *Sender);
	void __fastcall m_ExportBtnClick(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
    void __fastcall DBGrid1ColumnMoved(TObject *Sender, int FromIndex, int ToIndex);
    void __fastcall DBGrid1DblClick(TObject *Sender);
    void __fastcall UpdateUITimerTimer(TObject *Sender);
    void __fastcall m_SelectBtnClick(TObject *Sender);
    void __fastcall DBGrid1CellClick(TColumn *Column);
    void __fastcall m_CompareBtnClick(TObject *Sender);

private:	// User declarations
	void updateFileList(); //!< Scan folder and store all filenames, ends with .acd
	void updateTable();    //!< Делает выборку из БД, после чего отображает записи
	void updateItemCount(); //!< Делает запрос в БД чтобы узнать кол-во записей
	void updateUI();        //!< Выполняет #updateItemCount() и #updateTable()
	void updatePageLabel();     //!< Обновляет номер текущей страницы
	void updateSelectedColumns(); //!< Обновляет заголовки для выбранных колонок
	bool readFileToDB(UnicodeString& str);  //!< Считывает отчет и добавляет его в БД
	void adjustTableColumnsWidth();         //!< Подгоняет размер колонок под содержимое
	void exportFiles(UnicodeString dir);    //!< Экспортирует отчеты в директорию
	void updateFilterQuery();               //!< Обновляет запрос поисковой выборки

	int ShowSelectPrintDial();              //!< Сконфигурировать и показать диалог выбора типа печати
    int ShowSelectPrintDocTypeDial();       //!< Сконфигурировать и показать диалог выбора типа документа
	int ShowSelectExportDial(TStringList * drives); //!< Сконфигурировать и показать диалог выбора места для экспорта
    int ShowSelectThDial();                 //!< Сконфигурировать и показать диалог выбора уровня В-развертки
    int ShowSelectPrintChanTableDial();     //!< Сконфигурировать и показать диалог выбора печати таблицы каналов
    int ShowSelectPrintLinearityTableDial();//!< Сконфигурировать и показать диалог выбора печати линейности

	UnicodeString GetFilterQuery(); //!< Получить SQL запрос фильтрации

    bool bUISetStatus;
    UnicodeString uiStatusString;
    bool bUISetProgress;
    unsigned int uiProgressVal;
    DWORD PrevUIUpdateTime;
    void updateProgressUI();
    void setProgressUIValues();


	eADBProtocolType currProtocolType;

	DWORD SelectedColumns;
	bool bBdNotFound;
	bool bColSortOrderAsc;
	int PreviousColumnIndex;
	int maxDBGridRowCount;
	int tableItemCount;
	int curItemIndex;
	//UnicodeString filterQueryConditions;
	UnicodeString protocolQueryConditions;
	std::vector<UnicodeString> fileList;

	UnicodeString filterQueryString;
	std::vector<sFilterQueryParameter> filterQueryParameters;
	UnicodeString searchQueryString;
	std::vector<sFilterQueryParameter> searchQueryParameters;
public:		// User declarations
	__fastcall TArchiveForm(TComponent* Owner);

    int ShowSelectModeDial();
    void SetProtocolMode(int mode);

    bool SaveProtocolAsReport(UnicodeString fileName, unsigned int ThIndex = 0); //!< Сохранить протокол в PDF

	bool isValid();  //!< Проверка валидности
	void uiSetStatus(UnicodeString& str);    //!< Установка статуса
	void uiSetStatus(const char* str);       //!< Установка статуса
	void uiSetProgress(int value);           //!< Установка прогресса
};
//---------------------------------------------------------------------------
extern PACKAGE TArchiveForm *ArchiveForm;
//---------------------------------------------------------------------------

extern cAutoconMain *AutoconMain;
#endif
