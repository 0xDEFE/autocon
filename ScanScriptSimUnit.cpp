//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <cmath>
#include "AutoconMain.h"
extern cAutoconMain *AutoconMain;

#include "ScanScriptSimUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TScanScriptSimForm *ScanScriptSimForm;
//---------------------------------------------------------------------------
__fastcall TScanScriptSimForm::TScanScriptSimForm(TComponent* Owner)
    : TForm(Owner)
{
    pCodeMemo = new TMemoEx(m_CodePanel);
    pCodeMemo->Parent = m_CodePanel;
    pCodeMemo->Visible = true;
    pCodeMemo->Enabled = true;
    pCodeMemo->Align = alClient;
    pCodeMemo->Font->Height = -14;
    pCodeMemo->Font->Color = RGB(70,70,70);
    pCodeMemo->Font->Name = "Lucida Console";
    pCodeMemo->ScrollBars = ssVertical;
    pCodeMemo->setImageList(ImageList1);

    pCodeMemo->OnLeftLineClicked = &OnMemoLineClick;
    pCodeMemo->OnChange = &OnScriptChanged;
    pCodeMemo->Show();

    SimFlags = 0;
    SimState = SIMSTATE_EDITING;


    //-------TEMP------------
    sRailDefect def;
    def.offset = 500;
    def.delta = 100;
    def.strobeStart = 10;
    def.strobeEnd = 100;
    railDefects.push_back(def);

    def.offset = 700;
    def.delta = 60;
    def.strobeStart = 40;
    def.strobeEnd = 180;
    railDefects.push_back(def);

    def.offset = 740;
    def.delta = 60;
    def.strobeStart = 40;
    def.strobeEnd = 140;
    railDefects.push_back(def);

    def.offset = 50;
    def.delta = 10;
    def.strobeStart = 20;
    def.strobeEnd = 30;
    railDefects.push_back(def);

    def.offset = 1800;
    def.delta = 10;
    def.strobeStart = 60;
    def.strobeEnd = 120;
    railDefects.push_back(def);
    //-------------------------
}


void TScanScriptSimForm::drawCarets(bool bFullRedraw)
{
    //----------Draw parameters----------------
    const int rail_length_real = 1300;
    const int rail_height_real = 152;
    const float rail_height_perc = 0.4;
    const float caret_height_perc = (1.0f - rail_height_perc) / 2.f;
    const float rail_screen_offset = 10;
//    const float caret_width_perc = 0.6;
    const float caret_screen_offset = 5;
    const float caret_rail_offset = 10;

    const BYTE rail_color[] = {220,220,220};
    const int rail_lines_count = 7;
    const float rail_lines_y[] = {0, 15.4/152.0, 33.0/152.0, 40.0/152.0, 125.0/152.0, 141.0/152.0, 1.0};
    const float rail_lines_x[] = {45.7/152.0*2, 72.0/152.0*2, 72.0/152.0*2, 20.0/152.0*2, 20.0/152.0*2, 132.0/152.0*2, 132.0/152.0*2};
    const float rail_ass_offset = 10;
    const float rail_offset = rail_ass_offset + rail_screen_offset;

    //----------Calc shade----------------

    float rail_light_normal[2] = {0.f,1.f};
    float rail_lines_normals[7][2];
    float rail_lines_shade[7];
    for(int i = 0; i < (rail_lines_count-1); i++) //calc normals
    {
        rail_lines_normals[i][0] = rail_lines_x[i+1] - rail_lines_x[i];
        rail_lines_normals[i][1] = rail_lines_y[i+1] - rail_lines_y[i];
        float invLen = 1.0/sqrt((rail_lines_normals[i][0]*rail_lines_normals[i][0] +
                                    rail_lines_normals[i][1]*rail_lines_normals[i][1]) + 0.000001f);
        rail_lines_normals[i][0]*= invLen;
        rail_lines_normals[i][1]*= invLen;
    }
    rail_lines_normals[6][0] = 1;
    rail_lines_normals[6][1] = 0;

    for(int i = 0; i < rail_lines_count; i++) //calc shade
    {
        const float smooth_coeff = 0.7;
        float norm_smooth[2] = {LERP(rail_lines_normals[i][0],rail_light_normal[0],smooth_coeff),
                                LERP(rail_lines_normals[i][1],rail_light_normal[1],smooth_coeff)};
        rail_lines_shade[i] = (norm_smooth[0]*rail_light_normal[0] +
                                norm_smooth[1]*rail_light_normal[1]);
    }
    //------------------------------------

    TRect cliRect = m_CaretDrawPBox->ClientRect;

    TRect railRect(rail_screen_offset,cliRect.top + cliRect.Height()*caret_height_perc,
                    cliRect.right - rail_screen_offset, cliRect.bottom - cliRect.Height()*caret_height_perc);

    TPoint rail_segment_border_points[7][2];
    for(int i = 0; i < 7; i++)
    {
        rail_segment_border_points[i][0].x = railRect.left + rail_ass_offset * (1.f - rail_lines_x[i]);
        rail_segment_border_points[i][1].x = railRect.right - rail_ass_offset * (1.f - rail_lines_x[i]);

        rail_segment_border_points[i][0].y = rail_segment_border_points[i][1].y = railRect.top + float(railRect.Height())*rail_lines_y[i];
    }


    //---------------Draw rail----------------------

    if(bFullRedraw)
    {
        m_CaretDrawPBox->Canvas->Brush->Color = clWhite;
        m_CaretDrawPBox->Canvas->FillRect(cliRect);


        TPoint rail_segment_pts[5];
        m_CaretDrawPBox->Canvas->Pen->Color = clGray;
        for(int i = 0; i < rail_lines_count - 1; i++)
        {
            TRect lineRect(railRect.left,railRect.top + float(railRect.Height())*rail_lines_y[i],
                                railRect.right, railRect.top + float(railRect.Height())*rail_lines_y[i+1]);

            TColor col = TColor(RGB(rail_color[0]*rail_lines_shade[i],
                                rail_color[1]*rail_lines_shade[i],
                                rail_color[2]*rail_lines_shade[i]));

            rail_segment_pts[0] = rail_segment_border_points[i][0];
            rail_segment_pts[1] = rail_segment_border_points[i][1];
            rail_segment_pts[2] = rail_segment_border_points[i+1][1];
            rail_segment_pts[3] = rail_segment_border_points[i+1][0];
            rail_segment_pts[4] = rail_segment_pts[0];

            m_CaretDrawPBox->Canvas->Brush->Color = col;
            m_CaretDrawPBox->Canvas->Polygon(rail_segment_pts,4);
        }

        //----------------Draw defects------------------

        int rail_interval_draw = cliRect.Width() - rail_offset*2;
        int offsetTr[2];
        int deltaTr;
        float strobeTrY[2];
        float strobeTrX[2];

        for(unsigned int i = 0; i < railDefects.size(); i++)
        {
            deltaTr = railDefects[i].delta * rail_interval_draw / rail_length_real;
            offsetTr[0] = rail_offset + railDefects[i].offset * rail_interval_draw / rail_length_real - deltaTr/2;
            offsetTr[1] = rail_offset + railDefects[i].offset * rail_interval_draw / rail_length_real + deltaTr/2;

            strobeTrY[0] = float(railDefects[i].strobeStart) / float(rail_height_real);
            strobeTrY[1] = float(railDefects[i].strobeEnd) / float(rail_height_real);

            float projCoeff[2];
            projCoeff[0] = float(offsetTr[0] - railRect.CenterPoint().x) / (railRect.Width()/2);
            projCoeff[1] = float(offsetTr[1] - railRect.CenterPoint().x) / (railRect.Width()/2);

            for(int j = 0; j < rail_lines_count - 1; j++)
            {
                rail_segment_pts[0] = TPoint(offsetTr[0] + (rail_lines_x[j] - 0.5f)*projCoeff[0]*rail_ass_offset,
                                        railRect.top + float(railRect.Height())*rail_lines_y[j]);
                rail_segment_pts[1] = TPoint(offsetTr[1] + (rail_lines_x[j] - 0.5f)*projCoeff[1]*rail_ass_offset,
                                        railRect.top + float(railRect.Height())*rail_lines_y[j]);
                rail_segment_pts[2] = TPoint(offsetTr[1] + (rail_lines_x[j+1] - 0.5f)*projCoeff[1]*rail_ass_offset,
                                        railRect.top + float(railRect.Height())*rail_lines_y[j+1]);
                rail_segment_pts[3] = TPoint(offsetTr[0] + (rail_lines_x[j+1] - 0.5f)*projCoeff[0]*rail_ass_offset,
                                        railRect.top + float(railRect.Height())*rail_lines_y[j+1]);
                rail_segment_pts[4] = rail_segment_pts[0];

                m_CaretDrawPBox->Canvas->Pen->Style = psSolid;
                m_CaretDrawPBox->Canvas->Pen->Color = clBlack;
                m_CaretDrawPBox->Canvas->Pen->Width = 1;
                m_CaretDrawPBox->Canvas->MoveTo((rail_segment_pts[0].x + rail_segment_pts[1].x)/2,
                                                        rail_segment_pts[0].y);
                m_CaretDrawPBox->Canvas->LineTo((rail_segment_pts[3].x + rail_segment_pts[2].x)/2,
                                                        rail_segment_pts[3].y);

                int drtyp = -1;
                if((rail_lines_y[j] < strobeTrY[0]) && (rail_lines_y[j+1] >= strobeTrY[0]))
                    drtyp = 1;
                else if((rail_lines_y[j] <= strobeTrY[1]) && (rail_lines_y[j+1] > strobeTrY[1]))
                    drtyp = 2;
                else if((rail_lines_y[j] >= strobeTrY[0]) && (rail_lines_y[j+1] <= strobeTrY[1]))
                    drtyp = 0;

                float xval;
                switch(drtyp)
                {
                    case 0:
                        break;
                    case 1:
                        rail_segment_pts[0].y = rail_segment_pts[1].y = railRect.top + strobeTrY[0]*railRect.Height();
                        xval = LERP(rail_lines_x[j],rail_lines_x[j+1],
                                            (strobeTrY[0]-rail_lines_y[j])/(rail_lines_y[j+1]-rail_lines_y[j]));
                        rail_segment_pts[0].x = offsetTr[0] + (xval - 0.5f)*projCoeff[0]*rail_ass_offset;
                        rail_segment_pts[1].x = offsetTr[1] + (xval - 0.5f)*projCoeff[1]*rail_ass_offset;
                        break;
                    case 2:
                        rail_segment_pts[2].y = rail_segment_pts[3].y = railRect.top + strobeTrY[1]*railRect.Height();
                        xval = LERP(rail_lines_x[j],rail_lines_x[j+1],
                                            (strobeTrY[1]-rail_lines_y[j])/(rail_lines_y[j+1]-rail_lines_y[j]));
                        rail_segment_pts[2].x = offsetTr[1] + (xval - 0.5f)*projCoeff[1]*rail_ass_offset;
                        rail_segment_pts[3].x = offsetTr[0] + (xval - 0.5f)*projCoeff[0]*rail_ass_offset;
                        break;
                }

                if(drtyp != -1)
                {
                    rail_segment_pts[4] = rail_segment_pts[0];
                    m_CaretDrawPBox->Canvas->Brush->Color = clRed;
                    m_CaretDrawPBox->Canvas->Brush->Style = bsClear;
                    m_CaretDrawPBox->Canvas->Pen->Style = psDot;
                    m_CaretDrawPBox->Canvas->Pen->Color = clRed;
                    m_CaretDrawPBox->Canvas->Pen->Width = 2;
                    m_CaretDrawPBox->Canvas->Polygon(rail_segment_pts,4);
                }
            }
        }
    }

    //---------------Draw carets--------------------

    TRect caretIntervalRects[2] = {TRect(railRect.left + rail_ass_offset,caret_screen_offset,railRect.right - rail_ass_offset,railRect.top - caret_rail_offset),
                                    TRect(railRect.left + rail_ass_offset,railRect.bottom + caret_rail_offset, railRect.right - rail_ass_offset,cliRect.bottom - caret_screen_offset)
                                    };

    if(!bFullRedraw)
    {
        m_CaretDrawPBox->Canvas->Brush->Color = clWhite;
        TRect r1 = caretIntervalRects[0];
        r1.Bottom += (caret_rail_offset);
        r1.left = 0;
        r1.right = ClientRect.Width();
        m_CaretDrawPBox->Canvas->FillRect(r1);
        r1 = caretIntervalRects[1];
        r1.Top -= (caret_rail_offset);
        r1.left = 0;
        r1.right = ClientRect.Width();
        m_CaretDrawPBox->Canvas->FillRect(r1);
    }

    bool bCaretIsActive[2];
    int caretPos[2];
    scriptSim.sim_Ac_GetCaretStatus(true,&bCaretIsActive[0],&caretPos[0]);
    scriptSim.sim_Ac_GetCaretStatus(false,&bCaretIsActive[1],&caretPos[1]);

    TTextFormat tf;
    tf.Clear();
    tf << tfCenter << tfVerticalCenter << tfSingleLine;

    m_CaretDrawPBox->Canvas->Brush->Color = scriptSim.sim_scanEnabled ? clLime : RGB(200,200,200);
    m_CaretDrawPBox->Canvas->Pen->Color = clBlack;
    m_CaretDrawPBox->Canvas->Pen->Width = 1;
    m_CaretDrawPBox->Canvas->Pen->Style = psSolid;

    TPoint caret_pts[6];
    for(int i = 0; i < 2; i++)
    {
        m_CaretDrawPBox->Canvas->Font->Height = caretIntervalRects[i].Height();

        int caretDrawPos =  (caretPos[i] * caretIntervalRects[i].Width()) / rail_length_real + caretIntervalRects[i].left;
        UnicodeString caretDrawText = IntToStr(caretPos[i]);
        int caretDrawWidth = std::max(m_CaretDrawPBox->Canvas->TextWidth(caretDrawText) + 4, 18);
        TRect caretRect = TRect(caretDrawPos - caretDrawWidth/2,caretIntervalRects[i].top,
                                caretDrawPos + caretDrawWidth/2,caretIntervalRects[i].bottom);
        if(i == 0)
        {
            caret_pts[0] = TPoint(caretRect.left,caretRect.top);
            caret_pts[1] = TPoint(caretRect.right,caretRect.top);
            caret_pts[2] = TPoint(caretRect.right,caretRect.bottom);
            caret_pts[3] = TPoint(caretRect.left+caretRect.Width()/2,caretRect.bottom + (caret_rail_offset - 1));
            caret_pts[4] = TPoint(caretRect.left,caretRect.bottom);
            caret_pts[5] = TPoint(caretRect.left,caretRect.top);
        }
        else
        {
            caret_pts[0] = TPoint(caretRect.left,caretRect.top);
            caret_pts[1] = TPoint(caretRect.left+caretRect.Width()/2,caretRect.top - (caret_rail_offset - 1));
            caret_pts[2] = TPoint(caretRect.right,caretRect.top);
            caret_pts[3] = TPoint(caretRect.right,caretRect.bottom);
            caret_pts[4] = TPoint(caretRect.left,caretRect.bottom);
            caret_pts[5] = TPoint(caretRect.left,caretRect.top);
        }


        m_CaretDrawPBox->Canvas->Polygon(caret_pts,5);
        m_CaretDrawPBox->Canvas->TextRect(caretRect, caretDrawText, tf);
    }
}

void TScanScriptSimForm::drawChannels()
{
    const float scangr_height_perc = 0.8;
    const float scanch_height_perc = 1.f - scangr_height_perc;
    const int screen_offset = 10;
    const float scangr_text_offset_perc = 0.2;



    int curScanGroup = scriptSim.sim_GetChannelGroup();
    int channelsCount = AutoconMain->Config->GetAllScanChannelsCount();
    TRect cliRect = m_ChannelsDrawPBox->ClientRect;

    TRect scangr_rect(screen_offset,screen_offset,cliRect.right - screen_offset,
                        screen_offset + float(cliRect.Height() - screen_offset*2) * scangr_height_perc);
    TRect scangr_text_rect(scangr_rect.left, scangr_rect.top + scangr_rect.Height() * scangr_text_offset_perc,
                            scangr_rect.right, scangr_rect.bottom - scangr_rect.Height() * scangr_text_offset_perc);
    TRect scanch_rect(scangr_rect.left, scangr_rect.bottom,scangr_rect.right,cliRect.bottom - screen_offset);

    m_ChannelsDrawPBox->Canvas->Pen->Color = clBlack;
    m_ChannelsDrawPBox->Canvas->Pen->Width = 1;
    m_ChannelsDrawPBox->Canvas->Pen->Style = psSolid;

    m_ChannelsDrawPBox->Canvas->Brush->Color = clWhite;
    m_ChannelsDrawPBox->Canvas->Rectangle(cliRect.left,cliRect.top,cliRect.right,cliRect.bottom);

    sScanChannelDescription scanChanDesc;
    bool bFirst = true;
    for(int i=0;i<channelsCount-1;i++)
    {
        if(!AutoconMain->Config->getSChannelbyIdx(i, &scanChanDesc))
            continue;

        int offset0 = scanch_rect.Width() * i /channelsCount;
        int offset1 = scanch_rect.Width() * (i+1) /channelsCount;

        TRect chRect((scanch_rect.left + offset0)-1, scanch_rect.top, scanch_rect.left + offset1, scanch_rect.bottom);

        bool bChannelInFilter = false;
        for(unsigned int j = 0; j < scriptSim.tuneFilter.size(); j++)
        {
            if(scanChanDesc.Id == (int)scriptSim.tuneFilter[j])
            {
                bChannelInFilter = true;
                break;
            }
        }

        if((scanChanDesc.StrokeGroupIdx == curScanGroup) && bChannelInFilter)
        {
            if(bFirst)
            {
                scangr_text_rect.left = chRect.left + chRect.Width()/2;
                scangr_text_rect.right = chRect.left + chRect.Width()/2;
                bFirst = false;
            }
            else
            {
                scangr_text_rect.left = std::min( scangr_text_rect.left, chRect.left + chRect.Width()/2);
                scangr_text_rect.right = std::max( scangr_text_rect.right, chRect.left + chRect.Width()/2);
            }
            m_ChannelsDrawPBox->Canvas->Brush->Color = clLime;
            m_ChannelsDrawPBox->Canvas->MoveTo(chRect.left + chRect.Width()/2, chRect.top);
            m_ChannelsDrawPBox->Canvas->LineTo(chRect.left + chRect.Width()/2, scangr_text_rect.bottom);
        }
        else
            m_ChannelsDrawPBox->Canvas->Brush->Color = clWhite;

        m_ChannelsDrawPBox->Canvas->Rectangle(chRect.left,chRect.top,chRect.right,chRect.bottom);
    }

    if(curScanGroup != -1)
    {
        TTextFormat tf;
        tf.Clear();
        tf << tfCenter << tfVerticalCenter << tfSingleLine;

        m_ChannelsDrawPBox->Canvas->MoveTo(scangr_text_rect.left, scangr_text_rect.bottom);
        m_ChannelsDrawPBox->Canvas->LineTo(scangr_text_rect.right, scangr_text_rect.bottom);

        UnicodeString text = StringFormatU(L"������ ������������: %d",curScanGroup);
        m_ChannelsDrawPBox->Canvas->Font->Height = std::min(scangr_text_rect.Height(), 28);
        int textWidth = m_ChannelsDrawPBox->Canvas->TextWidth(text) + 4;
        if(textWidth > scangr_text_rect.Width())
        {
            int center = scangr_text_rect.left + scangr_text_rect.Width()/2;
            scangr_text_rect.left = center - textWidth/2;
            scangr_text_rect.right = center + textWidth/2;
        }
        if(scangr_text_rect.left < scangr_rect.left)
            scangr_text_rect.Offset(scangr_rect.left - scangr_text_rect.left,0);
        else if(scangr_text_rect.right > scangr_rect.right)
            scangr_text_rect.Offset(scangr_rect.right - scangr_text_rect.right,0);

        m_ChannelsDrawPBox->Canvas->TextRect(scangr_text_rect,text,tf);
    }
}

void __fastcall TScanScriptSimForm::m_CaretDrawPBoxPaint(TObject *Sender)
{
    drawCarets(true);
}

void __fastcall TScanScriptSimForm::m_ChannelsDrawPBoxPaint(TObject *Sender)
{
    drawChannels();
}

void TScanScriptSimForm::draw()
{
    drawCarets();
    drawChannels();
}


void __fastcall TScanScriptSimForm::OnMemoLineClick(TObject *Sender)
{
    int lineNo = int(((TMemoEx*)Sender)->ClickedLineNum);
    if((lineNo < 0) || (lineNo >= pCodeMemo->Lines->Count))
        return;

    if(pCodeMemo->getMark(lineNo) == -1)
    {
        pCodeMemo->setMark(lineNo,0);
        scriptSim.insertBreakpoint(lineNo);
    }
    else
    {
        pCodeMemo->setMark(lineNo,-1);
        scriptSim.removeBreakpoint(lineNo);
    }

    pCodeMemo->UpdateDraw();
}

void __fastcall TScanScriptSimForm::OnScriptChanged(TObject *Sender)
{
    SimFlags |= SIMFLAG_SCRIPT_CHANGED;
    SimFlags &= ~(SIMFLAG_SAVED | SIMFLAG_COMPILED);
}

void __fastcall TScanScriptSimForm::FrameResize(TObject *Sender)
{
    pCodeMemo->UpdateMargins();
}

//---------------------------------------------------------------------------

void TScanScriptSimForm::processStates()
{
    switch(SimState)
    {
        case SIMSTATE_EDITING:
        {
            break;
        }
        case SIMSTATE_RUNNING:
        {
            if(scriptSim.sim_processMove())
            {
                drawCarets();
                break;
            }


            eScanScriptRetCode excode = scriptSim.executeLine();
            processMessageStream();

            if(!scriptSim.sim_isMoving())
            {
                int codeLine = scriptSim.getCurrentLine();
                for(int i = 0; i < pCodeMemo->Lines->Count; i++) //Clear older marks
                {
                    if(pCodeMemo->getMark(i) == 2)
                        pCodeMemo->setMark(i,0);
                    else if(pCodeMemo->getMark(i) == 1)
                        pCodeMemo->setMark(i,-1);
                }

                if(codeLine != -1)
                {
                    if((codeLine <= pCodeMemo->getTopLine()) || (codeLine >= pCodeMemo->getBtmLine()))
                    {
                        pCodeMemo->SelStart = pCodeMemo->Perform(EM_LINEINDEX, codeLine, 0);
                        pCodeMemo->Perform(EM_SCROLLCARET, 0, 0);
                        pCodeMemo->UpdateVisibleLines(true);
                    }

                    if(pCodeMemo->getMark(codeLine) == 0)//debuggingSymb
                        pCodeMemo->setMark(codeLine,2);
                    else
                        pCodeMemo->setMark(codeLine,1);
                }

                pCodeMemo->UpdateDraw();
            }

            drawChannels();


            if(excode == SSRC_DEBUG_BREAK)
            {
                setState( SIMSTATE_BREAK );
                break;
            }
            else if(excode == SSRC_RUNTIME_ERROR)
            {
                SimFlags |= SIMFLAG_ERROR;
                setState( SIMSTATE_BREAK );
                break;
            }
            else if(excode == SSRC_END_PROGRAMM)
            {
                setState( SIMSTATE_EDITING );
                break;
            }
            else if(SimFlags & SIMFLAG_STEP)
            {
                SimFlags &= ~SIMFLAG_STEP;
                setState( SIMSTATE_BREAK );
            }
            break;
        }
        case SIMSTATE_BREAK:
        {
            break;
        }
    }
}

void TScanScriptSimForm::setState(eScanScriptSimState newState)
{
    if(newState == SIMSTATE_RUNNING)
    {
        if(!(SimFlags & SIMFLAG_COMPILED))
            return;

        pCodeMemo->ReadOnly = true;
        m_RunScriptBtn->Enabled = false;
        m_CompileScriptBtn->Enabled = false;
        m_StopScriptBtn->Enabled = true;
        m_StepOverScriptBtn->Enabled = false;
        m_PauseScriptBtn->Enabled = true;
    }
    else if(newState == SIMSTATE_BREAK)
    {
        if(!(SimFlags & SIMFLAG_COMPILED))
            return;

        m_RunScriptBtn->Enabled = true;
        m_CompileScriptBtn->Enabled = false;
        m_StopScriptBtn->Enabled = true;
        m_StepOverScriptBtn->Enabled = true;
        m_PauseScriptBtn->Enabled = false;
    }
    else if(newState == SIMSTATE_EDITING)
    {
        pCodeMemo->ReadOnly = false;
        m_RunScriptBtn->Enabled = true;
        m_CompileScriptBtn->Enabled = true;
        m_StopScriptBtn->Enabled = false;
        m_StepOverScriptBtn->Enabled = false;
        m_PauseScriptBtn->Enabled = false;

        for(int i = 0; i < pCodeMemo->Lines->Count; i++) //Clear older marks
        {
            if(pCodeMemo->getMark(i) == 2)
                pCodeMemo->setMark(i,0);
            else if(pCodeMemo->getMark(i) == 1)
                pCodeMemo->setMark(i,-1);
        }
    }
    SimState = newState;
    processStates();
}

void __fastcall TScanScriptSimForm::m_CompileScriptBtnClick(TObject *Sender)
{
    m_LogMemo->Clear();
    scriptSim.release();

    AnsiString str = pCodeMemo->Text;

    scriptSim.createFromMemory(str.c_str());
    processMessageStream();

    for(int i = 0; i < pCodeMemo->Lines->Count; i++)
    {
        if(pCodeMemo->getMark(i) != -1)
            scriptSim.insertBreakpoint(i);
    }

    SimFlags |= SIMFLAG_COMPILED;
}
//---------------------------------------------------------------------------

void __fastcall TScanScriptSimForm::m_RunScriptBtnClick(TObject *Sender)
{
    if(!(SimFlags & SIMFLAG_COMPILED))
        m_CompileScriptBtnClick( Sender );

    if(SimState != SIMSTATE_BREAK)
        scriptSim.cleanup();

    setState( SIMSTATE_RUNNING );
}
//---------------------------------------------------------------------------


void __fastcall TScanScriptSimForm::m_ScriptTimerTimer(TObject *Sender)
{
    processStates();
}
//---------------------------------------------------------------------------

void __fastcall TScanScriptSimForm::m_PauseScriptBtnClick(TObject *Sender)
{
    setState( SIMSTATE_BREAK );
}
//---------------------------------------------------------------------------


void __fastcall TScanScriptSimForm::m_StopScriptBtnClick(TObject *Sender)
{
    setState( SIMSTATE_EDITING );
}
//---------------------------------------------------------------------------

void __fastcall TScanScriptSimForm::m_StepOverScriptBtnClick(TObject *Sender)
{
    if(SimState != SIMSTATE_BREAK)
        return;
    SimFlags |= SIMFLAG_STEP;
    setState( SIMSTATE_RUNNING );
}
//---------------------------------------------------------------------------

void TScanScriptSimForm::processMessageStream()
{
    UnicodeString LineStr;

    LineStr = scriptSim.getOutputStream().str().c_str();
    scriptSim.getOutputStream().str("");

    logOutput(LineStr);
}

void TScanScriptSimForm::logOutput(UnicodeString str)
{
    m_LogMemo->Text = m_LogMemo->Text + str;
    //m_LogMemo->Lines->Add("");
    m_LogMemo->Refresh();
}
void __fastcall TScanScriptSimForm::FrameEnter(TObject *Sender)
{
    setState( SIMSTATE_EDITING );
}

//-----------------------ScanScriptSim--------------------------------------

eScanScriptRetCode cScanScriptSim::executeCmd(USHORT cmdCode)
{
    UINT argc = 0;
    //Check argument count
    popStack(&argc,4); //pop argument count
    sScanScriptCmdDecl* pDecl = codeDecl.GetCmdDeclByCmdCode(cmdCode);
    if(!pDecl)
    {
        ScanScriptMessageOut(&outputStream,&errBuf,
			StringFormatStdA("Runtime error: command is not valid").c_str(),0,getCurrentLine());
        return SSRC_RUNTIME_ERROR;
    };
    if((pDecl->argc != argc) && (pDecl->argc != 0xFFFF))
    {
        ScanScriptMessageOut(&outputStream,&errBuf,
			StringFormatStdA("Runtime error: %d arguments in command \"%s\" (must be %d)",argc,pDecl->name,pDecl->argc).c_str(),0,getCurrentLine());
        return SSRC_RUNTIME_ERROR;
    };



    eScanScriptRetCode retCode;
    if(!bSimulationMode)
    {
        retCode = handleCmd( cmdCode,argc );
    }
    else
    {
        retCode = handleSimCmd( cmdCode,argc );
    }
	return retCode;
}

eScanScriptRetCode cScanScriptSim::handleSimCmd( USHORT cmdCode, UINT argc)
{
    UINT arg1, arg2;
	int j = 0;

	bool bCaretActive;
	int iCaretCoord;

	switch(cmdCode)
	{
		case SSC_MOVCARET:
			popStack(&arg2,4);
			popStack(&arg1,4);

			sim_Ac_CaretManualMove(arg1, arg2);
			break;
		case SSC_MOVTOBASE:
			sim_Ac_MoveCaretsToStart();
			break;
		case SSC_SETSG:
			popStack(&arg1,4);
			sim_Dev_SetChannelGroup(arg1);
			sim_Dev_Update(false);
			break;
		case SSC_SETSGSENS:
		case SSC_SETSGGAIN:
		case SSC_SETSGPRISM:
			popStack(&arg2,4);
			popStack(&arg1,4);

            /* EMPTY */
			break;
        case SSC_SETCH:
			popStack(&arg1,4);
			sim_Dev_SetChannel(arg1);
			sim_Dev_Update(false);
			break;
		case SSC_SETCHSENS:
		case SSC_SETCHGAIN:
		case SSC_SETCHPRISM:
			popStack(&arg2,4);
			popStack(&arg1,4);

			/* EMPTY */
			break;
		case SSC_PRINTSG:
			popStack(&arg1,4);

			outputStream << " --> PRINTING ScanGroup\r\n";
			break;
		case SSC_PRINTSGMAX:
			popStack(&arg1,4);

            outputStream << " --> PRINTING ScanGroup Max\r\n";
			break;
		case SSC_PRINTCH:
			popStack(&arg1,4);

			outputStream << " --> PRINTING Channel\r\n";
			break;
		case SSC_PRINTCHMAX:
			popStack(&arg1,4);

			outputStream << " --> PRINTING Channel Max\r\n";
			break;
		case SSC_PRINTCARETS:
			sim_Ac_GetCaretStatus(false,&bCaretActive,&iCaretCoord);
			outputStream<<"-> Top Caret Status:\tactive-"<<bCaretActive<<", coord-"<<iCaretCoord<<"\r\n";
			sim_Ac_GetCaretStatus(true,&bCaretActive,&iCaretCoord);
			outputStream<<"-> Bottom Caret Status:\tactive-"<<bCaretActive<<", coord-"<<iCaretCoord<<"\r\n";
			break;
		case SSC_TUNE:
			outputStream << " --> TUNING Channels\r\n";
			break;
        case SSC_TUNESG:
            for(unsigned int i = 0; i < argc; i++)
            {
                popStack(&arg1,4);
			    outputStream << " --> TUNING Channel Group\r\n";
            }
			break;
		case SSC_TUNECH:
            for(unsigned int i = 0; i < argc; i++)
            {
                popStack(&arg1,4);
                tuneFilter.push_back(arg1);
			    outputStream << " --> TUNING Channel\r\n";
            }
            break;
		case SSC_TUNECLEAR:
			outputStream << " --> TUNING Clear\r\n";
			break;
		case SSC_TUNESETWCCNT:
			popStack(&arg1,4);
			//AutoconMain->ac->SetTuneWorCycleCount(arg1);
			break;
		case SSC_PATHRESET:
			outputStream << " --> RESETING PathEncoder\r\n";
			break;
		case SSC_SCAN:
			popStack(&arg1,4);
			if(arg1)
				sim_Dev_Update(false);//????
			else
				sim_Dev_DisableAll();
            sim_scanEnabled = arg1;
			break;
		case SSC_ENDPRG:
			return SSRC_END_PROGRAMM;
			break;
		case SSC_SLEEP:
			popStack(&arg1,4);
			Sleep(arg1);
			break;
		case SSC_SETWCCTL:
			popStack(&arg1,4);
			//AutoconMain->ac->EnableManualWorkCycleCtrl(arg1);
			break;
		case SSC_SETWC:
			popStack(&arg1,4);
			//AutoconMain->ac->SetWorkCycle(arg1);
			break;
		break;
	}
	return SSRC_RUNNING;
}

void cScanScriptSim::sim_Ac_CaretManualMove(int topOffset,int bottomOffset)
{
    sim_caretTargets[0] = sim_caretPos[0] + topOffset;
    sim_caretTargets[1] = sim_caretPos[1] + bottomOffset;
    sim_caretStatus[0] = sim_caretStatus[1] = true;
}
void cScanScriptSim::sim_Ac_MoveCaretsToStart()
{
    sim_caretTargets[0] = sim_caretTargets[1] = 50;
    sim_caretStatus[0] = sim_caretStatus[1] = true;
}
bool cScanScriptSim::sim_processMove()
{
    DWORD CurrentTime = GetTickCount();

    float dT = float(CurrentTime - moveProcessPrevTime)/1000.f;
    float dS = caretSpeed * dT;
    moveProcessPrevTime = CurrentTime;

    bool bAllStatus = false;
    for(int i = 0; i < 2; i++)
    {
        bAllStatus = bAllStatus || sim_caretStatus[i];
        if(!sim_caretStatus[i])
            continue;
        if(abs(long(sim_caretPos[i] - sim_caretTargets[i])) < dS)
        {
            sim_caretPos[i] = sim_caretTargets[i];
            sim_caretStatus[i] = false;
        }
        else
        {
            if(sim_caretPos[i] < sim_caretTargets[i])
                sim_caretPos[i] += dS;
            else if(sim_caretPos[i] > sim_caretTargets[i])
                sim_caretPos[i] -= dS;
        }
    }
    return bAllStatus;
}
void cScanScriptSim::sim_Ac_GetCaretStatus(bool caretPos, bool* bActive, int* pCoord)
{
    if(bSimulationMode)
    {
        if(caretPos) {*bActive = sim_caretStatus[0]; *pCoord = sim_caretPos[0];}
        else {*bActive = sim_caretStatus[1]; *pCoord = sim_caretPos[1];};
    }
    else
    {
        AutoconMain->ac->GetCaretStatus(caretPos,bActive,pCoord);
    }
}
void cScanScriptSim::sim_Dev_Update(bool bVal)
{
    sim_scanEnabled = true;
}
void cScanScriptSim::sim_Dev_DisableAll()
{
    sim_curScanGroup = -1;
    sim_curChannel = -1;
    sim_scanEnabled = false;
}
void cScanScriptSim::sim_Dev_SetChannelGroup(int chGr)
{
    sim_curScanGroup = chGr;
}
void cScanScriptSim::sim_Dev_SetChannel(int ch)
{
    sim_curChannel = ch;
}

int cScanScriptSim::sim_GetActiveChannel()
{
    if(bSimulationMode)
    {
        return sim_curChannel;
    }
    return AutoconMain->DEV->GetChannel();
}
int cScanScriptSim::sim_GetChannelGroup()
{
    if(bSimulationMode)
    {
        return sim_curScanGroup;
    }
    return AutoconMain->DEV->GetChannelGroup();
}
//---------------------------------------------------------------------------
void __fastcall TScanScriptSimForm::m_CloseBtnClick(TObject *Sender)
{
    this->Visible = false;
}
//---------------------------------------------------------------------------

void TScanScriptSimForm::SetScriptCode(UnicodeString str)
{
    pCodeMemo->Text = str;
}

void TScanScriptSimForm::AddDefectToRail(int offset, int delta, int strobeStart, int strobeEnd)
{
    sRailDefect def;
    def.offset = offset;
    def.delta = delta;
    def.strobeEnd = strobeEnd;
    def.strobeStart = strobeStart;
    railDefects.push_back(def);
}

void TScanScriptSimForm::ClearRailDefects()
{
    railDefects.clear();
}
void __fastcall TScanScriptSimForm::m_LoadBtnClick(TObject *Sender)
{
    OpenDialog1->InitialDir = GetCurrentDir();
    if(!OpenDialog1->Execute(NULL))
        return;

    TStringList *List=new TStringList;
    try
    {
        List->LoadFromFile(OpenDialog1->FileName);
    }
    catch(...)
    {
        delete List; return;
    }

    SetScriptCode(List->Text);

    delete List;
}
//---------------------------------------------------------------------------
void __fastcall TScanScriptSimForm::m_SaveBtnClick(TObject *Sender)
{
    SaveDialog1->InitialDir = GetCurrentDir();
    if(!SaveDialog1->Execute(NULL))
        return;

    pCodeMemo->Lines->SaveToFile(SaveDialog1->FileName);
}
//---------------------------------------------------------------------------
void __fastcall TScanScriptSimForm::m_ClearBtnClick(TObject *Sender)
{
    pCodeMemo->Clear();
}
//---------------------------------------------------------------------------
