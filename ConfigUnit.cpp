//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <DateUtils.hpp>

#include "ConfigUnit.h"
//---------------------------------------------------------------------------
//#pragma package(smart_init)
#pragma resource "*.dfm"
TConfigForm *ConfigForm;

//---------------------------------------------------------------------------
__fastcall TConfigForm::TConfigForm(TComponent* Owner)
    : TForm(Owner)
{
}

//---------------------------------------------------------------------------
void __fastcall TConfigForm::Button1Click(TObject *Sender)
{
     int addr;
     int size;

     //TJPEGImage * Jpg = new TJPEGImage;
     TMemoryStream * Str = new TMemoryStream;

     if (GetPhoto(&Edit1->Text[1], &addr, &size))
     {
       Label1->Caption = L"State: OK ";
       Str->SetSize(size);
       memcpy(Str->Memory, (void*)addr, size);
       TJPEGImage *ggg=new TJPEGImage;
       ggg->LoadFromStream(Str);
       Image1->Picture->Assign(ggg);
     }  else Label1->Caption = L"State: Error ";
}
//---------------------------------------------------------------------------
void __fastcall TConfigForm::BackButtonClick(TObject *Sender)
{
    this->Visible = false;
}


//---------------------------------------------------------------------------
//--  cACConfig                                                            --
//---------------------------------------------------------------------------

cACConfig::cACConfig(void)
{
    Operators = new TStringList();
	Ini = new TIniFile(ExtractFilePath(Application->ExeName) + "config.ini");
	Ini2 = new TIniFile(ExtractFilePath(Application->ExeName) + "BScanConfig.ini");
	BScan_ThTrackBarPos = 0;
	ShowFiltrParam_ = false;
}

cACConfig::~cACConfig(void)
{
	delete Operators;
	delete Ini;
	delete Ini2;
}

void cACConfig::LoadData(void)
{
	WideChar save;
	save = FormatSettings.DecimalSeparator;
	FormatSettings.DecimalSeparator = '.';

	Last_DateTime         = (TDateTime)Ini->ReadFloat("EnterParams", "Last_DateTime", Now()); // ���� / ����� ��������
    Last_HardnessMeasTime = (TDateTime)Ini->ReadFloat("EnterParams", "Last_HardnessMeasTime", Now()); // ���� / ����� ��������� ���������
    Last_SSC_TestDateTime     = (TDateTime)Ini->ReadFloat("EnterParams", "Last_SSC_TestDateTime", Now());   // ���� / ����� ������������ �� ���
    Last_SS3R_TestDateTime    = (TDateTime)Ini->ReadFloat("EnterParams", "Last_SS3R_TestDateTime", Now());  // ���� / ����� ������������ �� ��-3�

	Last_TopStraightness = Ini->ReadFloat("EnterParams", "Last_TopStraightness", 0); // ��������������� ������ [��]
	Last_WF_Straightness = Ini->ReadFloat("EnterParams", "Last_WF_Straightness", 0); // ��������������� (��) [��]
	Last_NWF_Straightness = Ini->ReadFloat("EnterParams", "Last_NWF_Straightness", 0); // ��������������� (���) [��]

	Last_GangNumber	  = Ini->ReadInteger("EnterParams", "Last_GangNumber", 1); // ����� �����
	Last_ReportNumber = Ini->ReadInteger("EnterParams", "Last_ReportNumber", 1); // ����� ��������� ��������
    Last_TestReportNumber = Ini->ReadInteger("EnterParams", "Last_TestReportNumber", 1); // ����� ��������� ������������
	Last_PletNumber   = Ini->ReadInteger("EnterParams", "Last_PletNumber", 1); // ����� �����
	Last_JointNumber  = Ini->ReadInteger("EnterParams", "Last_JointNumber", 1); // ����� �����
//	Last_Hardness_	  = Ini->ReadInteger("EnterParams", "Last_Hardness", 0); // ��������
//	Last_HardnessUnit = Ini->ReadInteger("EnterParams", "Last_HardnessUnit", 1); // ��. ���. ��������: 1 � HB, 2 - HV, 3 - HRC

    Last_HardnessState = Ini->ReadInteger("EnterParams", "HardnessState", 0); // �������� - ���������� ��� ���
    Last_Hardness[0] = Ini->ReadInteger("EnterParams", "Last_Hardness0", 0); // ��������
    Last_Hardness[1] = Ini->ReadInteger("EnterParams", "Last_Hardness1", 0); // ��������
    Last_Hardness[2] = Ini->ReadInteger("EnterParams", "Last_Hardness2", 0); // ��������
    Last_Hardness[3] = Ini->ReadInteger("EnterParams", "Last_Hardness3", 0); // ��������
    Last_Hardness[4] = Ini->ReadInteger("EnterParams", "Last_Hardness4", 0); // ��������
    Last_Hardness[5] = Ini->ReadInteger("EnterParams", "Last_Hardness5", 0); // ��������
	Last_Hardness[6] = Ini->ReadInteger("EnterParams", "Last_Hardness6", 0); // ��������
	Last_Hardness[7] = Ini->ReadInteger("EnterParams", "Last_Hardness7", 0); // ��������
	Last_Hardness[8] = Ini->ReadInteger("EnterParams", "Last_Hardness8", 0); // ��������

	Last_VerSoft	   = Ini->ReadString("EnterParams", "Last_VerSoft", "-"); // ������ ��
	Last_Operator 	   = Ini->ReadString("EnterParams", "Last_Operator", "-"); // �������� ���
	Last_DefectCode    = ""; //FDV Ini->ReadString("EnterParams", "Last_DefectCode", "-"); // ��� �������
	Last_ValidityGroup = ""; //FDV Ini->ReadString("EnterParams", "Last_ValidityGroup", "-"); // ��. ��������
	Last_�onclusion    = ""; //FDV Ini->ReadString("EnterParams", "Last_�onclusion", "-"); // ����������

    FotoSide = Ini->ReadBool("Common", "FotoSide", true); // ������� ��������� ���������� (true - ������� ���������)

    Last_WorkTimeSec = Ini->ReadInteger("Common", "Last_WorkTimeSec", 0);
    Last_WorkStartTime = Now();

    BScan_ThTrackBarPos = Ini->ReadInteger("Common", "BScan_ThTrackBarPos", 0);

    Current_Plaint   	   = Ini->ReadString("Common", "Current_Plaint", "-"); // �����������
	Current_PlantNumber   = Ini->ReadString("Common", "Current_PlantNumber", "-"); // ��������� ����� ���������

	Test_EtalonModelName = Ini->ReadString("Common", "Test_EtalonModelName", "");

	BScanMeasureTh = Ini->ReadInteger("Common", "BScanMeasureTh", 100);

    Operators->Clear();
    Ini->ReadSection("OperatorsList", Operators);
    int Count = Operators->Count;
    Operators->Clear();
    for (int i = 0; i < Count; i++)
		Operators->Add(Ini->ReadString("OperatorsList", IntToStr(i), ""));

	MinLenEdit_ = Ini->ReadInteger("FiltrParams", "MinLenEdit", 9);
	MaxSpace_ = Ini->ReadInteger("FiltrParams",   "MaxSpace", 4);
	DelayDelta_ = Ini->ReadInteger("FiltrParams", "DelayDelta", 1);
	SameCount_ = Ini->ReadInteger("FiltrParams",  "SameCount", 3);
	UseFiltr_ = Ini->ReadBool("FiltrParams",  "UseFiltr", false);


	for (int i = 0; i < 90; i++)
		for (int j = 0; j < 4; j++)
		{
			BScanMeasure[i][j] = Ini2->ReadBool("BScanMeasureCh"+IntToStr(i), IntToStr(j), true);
		}

	FormatSettings.DecimalSeparator = save;
}

void cACConfig::SaveData(void)
{

	WideChar save;
	save = FormatSettings.DecimalSeparator;
	FormatSettings.DecimalSeparator = '.';

	Ini->WriteFloat("EnterParams", "Last_DateTime",         Last_DateTime        	 	); // ���� / ����� ��������

    Ini->WriteFloat("EnterParams", "Last_HardnessMeasTime", Last_HardnessMeasTime); // ���� / ����� ��������� ���������
    Ini->WriteFloat("EnterParams", "Last_SSC_TestDateTime", Last_SSC_TestDateTime);   // ���� / ����� ������������ �� ���
    Ini->WriteFloat("EnterParams", "Last_SS3R_TestDateTime", Last_SS3R_TestDateTime);  // ���� / ����� ������������ �� ��-3�

    Ini->WriteFloat("EnterParams", "Last_TopStraightness",  Last_TopStraightness 	 	); // ��������������� ������ [��]
    Ini->WriteFloat("EnterParams", "Last_WF_Straightness", Last_WF_Straightness	 	); // ��������������� (��) [��]
    Ini->WriteFloat("EnterParams", "Last_NWF_Straightness", Last_NWF_Straightness	 	); // ��������������� (���) [��]

    Ini->WriteInteger("EnterParams", "Last_GangNumber",     Last_GangNumber	     	 	); // ����� �����
    Ini->WriteInteger("EnterParams", "Last_ReportNumber",   Last_ReportNumber    	 	); // ����� ��������� ��������
    Ini->WriteInteger("EnterParams", "Last_TestReportNumber",   Last_TestReportNumber 	 	); // ����� ��������� ������������
    Ini->WriteInteger("EnterParams", "Last_PletNumber",     Last_PletNumber      	 	); // ����� �����
    Ini->WriteInteger("EnterParams", "Last_JointNumber",    Last_JointNumber     	 	); // ����� �����
//    Ini->WriteInteger("EnterParams", "Last_Hardness",       Last_Hardness_	     	 	); // ��������
//    Ini->WriteInteger("EnterParams", "Last_HardnessUnit",   Last_HardnessUnit    	 	); // ��. ���. ��������: 1 � HB, 2 - HV, 3 - HRC

    Ini->WriteInteger("EnterParams", "HardnessState", Last_HardnessState); // �������� - ���������� ��� ���
    Ini->WriteInteger("EnterParams", "Last_Hardness0",   Last_Hardness[0]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness1",   Last_Hardness[1]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness2",   Last_Hardness[2]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness3",   Last_Hardness[3]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness4",   Last_Hardness[4]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness5",   Last_Hardness[5]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness6",   Last_Hardness[6]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness7",   Last_Hardness[7]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness8",   Last_Hardness[8]); // ��������

	Ini->WriteString("EnterParams", "Last_VerSoft",         Last_VerSoft	     	     	); // ������ ��
	Ini->WriteString("EnterParams", "Last_Operator",        Last_Operator 	     	 	); // �������� ���
	Ini->WriteString("EnterParams", "Last_DefectCode",      Last_DefectCode      	 	); // ��� �������
	Ini->WriteString("EnterParams", "Last_ValidityGroup",   Last_ValidityGroup   	 	); // ��. ��������
	Ini->WriteString("EnterParams", "Last_�onclusion",      Last_�onclusion  	 	    ); // ����������

	for (int i = 0; i < Operators->Count; i++)
		Ini->WriteString("OperatorsList", IntToStr(i), Operators->Strings[i]);

	Ini->WriteBool("Common", "FotoSide", FotoSide); // ������� ��������� ���������� (true - ������� ���������)

	Last_WorkTimeSec += SecondsBetween(Last_WorkStartTime, Now());
	Last_WorkStartTime = Now();
	Ini->WriteInteger("Common", "Last_WorkTimeSec", Last_WorkTimeSec);

	Ini->WriteInteger("Common", "BScan_ThTrackBarPos", BScan_ThTrackBarPos);

	Ini->WriteString("Common", "Test_EtalonModelName", Test_EtalonModelName);

	Ini->WriteInteger("Common", "BScanMeasureTh", BScanMeasureTh);

	Ini->WriteInteger("FiltrParams", "MinLenEdit", MinLenEdit_);
	Ini->WriteInteger("FiltrParams",   "MaxSpace", MaxSpace_  );
	Ini->WriteInteger("FiltrParams", "DelayDelta", DelayDelta_);
	Ini->WriteInteger("FiltrParams",  "SameCount", SameCount_ );
	Ini->WriteBool("FiltrParams",  "UseFiltr", UseFiltr_);


	//Do not overwrite Current_Plaint and Current_PlantNumber in config
	//Ini->WriteString("Common", "Current_Plaint", Current_Plaint); // �����������
	//Ini->WriteString("Common", "Current_PlantNumber", Current_PlantNumber); // ��������� ����� ���������

	for (int i = 0; i < 90; i++)
		for (int j = 0; j < 4; j++)
			Ini2->WriteBool("BScanMeasureCh" + IntToStr(i), IntToStr(j), BScanMeasure[i][j]);

	FormatSettings.DecimalSeparator = save;
};




