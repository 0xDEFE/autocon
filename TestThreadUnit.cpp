//---------------------------------------------------------------------------

#pragma hdrstop

#include "TestThreadUnit.h"
//---------------------------------------------------------------------------
//#pragma package(smart_init)

__fastcall cTestThread::cTestThread(cAutoconMain *Main_) : TThread(true)
{
    Main = Main_;
    StateOK = false;
    if (Main)
      if (Main->CtrlWork) Resume();
}

__fastcall cTestThread::~cTestThread(void)
{
}

void __fastcall cTestThread::Execute(void)
{
    while (!EndWorkFlag)
    {
       Sleep(300);
       #ifndef SIMULATION_CONTROLER_AND_MOTORS
       if (Main)
            if (Main->ac)
                if (Main->ac->ctrl)
					if (Main->ac->ctrl->getDeviceNumber() < 0) StateOK = false; else StateOK = true;
	   #endif
       #ifdef SIMULATION_CONTROLER_AND_MOTORS
       StateOK = true;
	   #endif
    }
}
