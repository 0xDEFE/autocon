//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MeasureSelChannelFormUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMeasureSelChannelForm *MeasureSelChannelForm;
//---------------------------------------------------------------------------
__fastcall TMeasureSelChannelForm::TMeasureSelChannelForm(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TMeasureSelChannelForm::FormCreate(TObject *Sender)
{
    chData[0].idx = 0;
    chData[0].pPanel = SelPanel1;
    chData[0].pCheck = SelCheck1;
    chData[0].pPB = SelPB1;

    chData[1].idx = 0;
    chData[1].pPanel = SelPanel2;
    chData[1].pCheck = SelCheck2;
    chData[1].pPB = SelPB2;

    chData[2].idx = 0;
    chData[2].pPanel = SelPanel3;
    chData[2].pCheck = SelCheck3;
    chData[2].pPB = SelPB3;

    chData[3].idx = 0;
    chData[3].pPanel = SelPanel4;
    chData[3].pCheck = SelCheck4;
    chData[3].pPB = SelPB4;

    chData[4].idx = 0;
    chData[4].pPanel = SelPanel5;
    chData[4].pCheck = SelCheck5;
    chData[4].pPB = SelPB5;

    chData[5].idx = 0;
    chData[5].pPanel = SelPanel6;
    chData[5].pCheck = SelCheck6;
    chData[5].pPB = SelPB6;
}
//---------------------------------------------------------------------------

void TMeasureSelChannelForm::clearChannels()
{
    for(int i = 0; i < 6; i++)
    {
        chData[i].pPanel->Visible = false;
    }
}

void TMeasureSelChannelForm::setChannel(unsigned int idx,int id, const UnicodeString& name, TColor color)
{
    if(idx >= 6)
        return;

    chData[idx].id = id;
    chData[idx].pPanel->Visible = true;
    chData[idx].pCheck->Checked = false;
    chData[idx].pCheck->Caption = name;
    chData[idx].pPB->Color = color;
//    chData[idx].pPB->Canvas->Brush->Color = color;
//    chData[idx].pPB->Canvas->Brush->Style = bsSolid;
//    chData[idx].pPB->Canvas->FillRect(chData[idx].pPB->ClientRect);
}
void __fastcall TMeasureSelChannelForm::FormShow(TObject *Sender)
{
    for(int i = 0; i < 6; i++)
    {
        chData[i].pPB->Repaint();
    }
}

bool TMeasureSelChannelForm::channelState(int id)
{
    for(int i = 0; i < 6; i++)
    {
        if((chData[i].id == id) && (chData[i].pPanel->Visible) && chData[i].pCheck->Checked)
            return true;
    }
    return false;
}
//---------------------------------------------------------------------------

