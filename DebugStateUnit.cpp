//---------------------------------------------------------------------------

#include <vcl.h>
#include "ConsoleDlgUnit.h"
#include "ManualCtrlPanelUnit.h"
#include "LogUnit.h"
#include "Utils.h"
#pragma hdrstop

#include "DebugStateUnit.h"
#include "TuneTableFormUnit.h"
//---------------------------------------------------------------------------
//#pragma package(smart_init)
#pragma resource "*.dfm"
TDebugStateForm *DebugStateForm;
//---------------------------------------------------------------------------
__fastcall TDebugStateForm::TDebugStateForm(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TDebugStateForm::FormShow(TObject *Sender)
{
//    Top = 0;
//    Left = 0;
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::FormCreate(TObject *Sender)
{
    Left = Screen->Width - Width - 125;
    Top = 05;
}
//---------------------------------------------------------------------------


void __fastcall TDebugStateForm::m_ShowConsoleClick(TObject *Sender)
{
	if(m_ShowConsole->Checked)
		ConsoleDlg->Show();
	else
	   ConsoleDlg->Hide();
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::m_ManualModeCheckClick(TObject *Sender)
{
    //TuneTableFrame->Visible = true;

	if(m_ManualModeCheck->Checked)
		TManualCtrlPanel::DialogShow();
	else
		TManualCtrlPanel::DialogHide();
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::PECheckClick(TObject *Sender)
{
	if(PECheck->Checked)
		Width = 416;
	else
		Width = 139;
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::ClearButtonClick(TObject *Sender)
{
    AutoconMain->DEV->ResetPathEncoderGis();
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::StartStopButtonClick(TObject *Sender)
{
    if (StartStopButton->Tag == 0) {

        StartStopButton->Tag = 1;
        AutoconMain->DEV->SetPathEncoderGisState(true);
        GreenShape->BringToFront();

    } else {

        StartStopButton->Tag = 0;
        AutoconMain->DEV->SetPathEncoderGisState(false);
        RedShape->BringToFront();
    }
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::ScreenshotBtnClick(TObject *Sender)
{
    this->Visible = false;
    LogForm->Visible = false;
    ScreenshotTimer->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::ScreenshotTimerTimer(TObject *Sender)
{
    HDC ScreenDC = GetDC(0);
    TBitmap* ScreenBmp = new TBitmap();

    __try
    {
        int W = Screen->Width;
        int H = Screen->Height;
        ScreenBmp->Width = W;
        ScreenBmp->Height = H;
        BitBlt(ScreenBmp->Canvas->Handle, 0, 0, W, H, ScreenDC, 0, 0, SRCCOPY);

        TJPEGImage* jpg = new TJPEGImage();
        __try
        {
            PathMgr::PushPath(L"$screenshots", L"resources\\screenshots");

            UnicodeString screenshot_path =
                        PathMgr::GetPath(StringFormatU(L"($screenshots)\\%s.jpg",FormatDateTime(L"ddmmyyyy_hh_mm_ss",Now()).c_str()).c_str());

            jpg->Assign(ScreenBmp);
            jpg->CompressionQuality = 100;
            jpg->SaveToFile(screenshot_path);
        }
        __finally
        {
            delete jpg;
        };
    }
    __finally
    {
        delete ScreenBmp;
    };

    this->Visible = true;
    LogForm->Visible = true;

    ScreenshotTimer->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::btnSaveClick(TObject *Sender)
{

    saved2TP.clear();
    sScanChannelDescription scanChannelDesc;
    for(unsigned int i=0;i<AutoconMain->Config->GetAllScanChannelsCount();i++)
    {
        if(!AutoconMain->Config->getSChannelbyIdx(i, &scanChannelDesc))
            continue;

        saved2TP.push_back(std::make_pair<int,int>(scanChannelDesc.Id,AutoconMain->Calibration->GetPrismDelay(scanChannelDesc.DeviceSide,
                                                scanChannelDesc.Id)));
    }

    btnSave->Font->Style = TFontStyles() << fsBold;
}
//---------------------------------------------------------------------------


void __fastcall TDebugStateForm::btnLoadClick(TObject *Sender)
{
    for(unsigned int i = 0; i < saved2TP.size(); i++)
    {
        AutoconMain->Calibration->SetPrismDelay(dsNone,
                                                saved2TP[i].first, saved2TP[i].second);
    }
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::btnSet0Click(TObject *Sender)
{
    sScanChannelDescription scanChannelDesc;
    for(unsigned int i=0;i<AutoconMain->Config->GetAllScanChannelsCount();i++)
    {
        if(!AutoconMain->Config->getSChannelbyIdx(i, &scanChannelDesc))
            continue;

        AutoconMain->Calibration->SetPrismDelay(scanChannelDesc.DeviceSide,
                                                scanChannelDesc.Id,0);
    }

    AutoconMain->DEV->Update(false);
}
//---------------------------------------------------------------------------


void __fastcall TDebugStateForm::btnSet25Click(TObject *Sender)
{
    sScanChannelDescription scanChannelDesc;
    for(unsigned int i=0;i<AutoconMain->Config->GetAllScanChannelsCount();i++)
    {
        if(!AutoconMain->Config->getSChannelbyIdx(i, &scanChannelDesc))
            continue;

        AutoconMain->Calibration->SetPrismDelay(scanChannelDesc.DeviceSide,
                                                scanChannelDesc.Id,250);
    }
    AutoconMain->DEV->Update(false);
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::btnSet100Click(TObject *Sender)
{
    sScanChannelDescription scanChannelDesc;
    for(unsigned int i=0;i<AutoconMain->Config->GetAllScanChannelsCount();i++)
    {
        if(!AutoconMain->Config->getSChannelbyIdx(i, &scanChannelDesc))
            continue;

        AutoconMain->Calibration->SetPrismDelay(scanChannelDesc.DeviceSide,
                                                scanChannelDesc.Id,1000);
    }
    AutoconMain->DEV->Update(false);
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::cSaveBtnClick(TObject *Sender)
{
//    AutoconMain->Calibration->collectValidationInfo();
//    AutoconMain->Calibration->storeValidationInfo();
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::cLoadBtnClick(TObject *Sender)
{
//    AutoconMain->Calibration->loadValidationInfo();
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::cCheckBtnClick(TObject *Sender)
{
//    bool bRes = AutoconMain->Calibration->validateCalibration();
//    if(bRes)
//        Application->MessageBoxW(L"Validation OK!",L"Validation OK!", MB_OK | MB_ICONINFORMATION);
//    else
//        Application->MessageBoxW(L"Validation FAILED!",L"Validation FAILED!", MB_OK | MB_ICONERROR);
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::ValidationTimerTimer(TObject *Sender)
{
//    bool bRes = AutoconMain->Calibration->validateCalibration();
//    if(!bRes)
//    {
//        LOGERROR_NF("Validation FAILED!!");
//        AutoconMain->Calibration->collectValidationInfo();
//        AutoconMain->Calibration->storeValidationInfo();
//        Application->MessageBoxW(L"Validation FAILED!",L"Validation FAILED!", MB_OK | MB_ICONERROR);
//    }


//    for(int nCalib = 0; nCalib < AutoconMain->Calibration->Count(); nCalib++)
//    {
//        AutoconMain->Calibration->SetCurrent(nCalib);
//
//        for(int i=0;i<AutoconMain->Table->Count();i++)
//        {
//            CID chId;
//            if(!AutoconMain->Table->CIDByIndex(&chId,i))
//                continue;
//
//            for(int j = 0; j < 2; j++)
//            {
//                //AutoconMain->Calibration->SetSens(dsNone,chId,j, RandomRange(12,100));
//                AutoconMain->Calibration->SetSens(dsNone,chId,j, 33);
//                AutoconMain->Calibration->SetGain(dsNone,chId,j, RandomRange(12,100));
//            }
//
//            for(int j = 0; j < 4; j++)
//            {
//                AutoconMain->Calibration->SetStGate(dsNone,chId,j, RandomRange(12,100));
//                AutoconMain->Calibration->SetEdGate(dsNone,chId,j, RandomRange(12,100));
//            }
//
//        }
//    }

}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::IgnoreExceptBtnClick(TObject *Sender)
{
//    int val;
//    if(TryStrToInt(exceptChannelEdit->Text, val))
//    {
//        AutoconMain->DEV->disableAllExceptChannel( val );
//    }
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::DisableAScanStrobeCheckClick(TObject *Sender)
{
    cBScanDraw::bDisableAScanStrobeCheck = DisableAScanStrobeCheck->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::DisableBScanStrobeCheckClick(TObject *Sender)
{
    cBScanDraw::bDisableBScanStrobeCheck = DisableBScanStrobeCheck->Checked;
}
//---------------------------------------------------------------------------

