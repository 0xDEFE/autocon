object ManualCtrlPanel: TManualCtrlPanel
  Left = 0
  Top = 0
  Width = 540
  Height = 272
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object m_CaretMovementGrid: TGridPanel
    Left = 0
    Top = 0
    Width = 540
    Height = 129
    Align = alClient
    ColumnCollection = <
      item
        Value = 21.164941473522900000
      end
      item
        Value = 58.385673844010710000
      end
      item
        Value = 20.449384682466380000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = m_CaretsHomeBtn
        Row = 0
      end
      item
        Column = 1
        Control = m_OffsetBtnsGrid
        Row = 0
      end
      item
        Column = 2
        Control = m_ExecBtn
        Row = 0
      end>
    RowCollection = <
      item
        Value = 100.000000000000000000
      end
      item
        SizeStyle = ssAuto
      end
      item
        SizeStyle = ssAuto
      end
      item
        SizeStyle = ssAuto
      end>
    TabOrder = 0
    object m_CaretsHomeBtn: TSpeedButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 107
      Height = 121
      Align = alClient
      AllowAllUp = True
      GroupIndex = 83
      Caption = 'Home'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = m_CaretsHomeBtnClick
      ExplicitTop = 2
      ExplicitWidth = 79
      ExplicitHeight = 77
    end
    object m_OffsetBtnsGrid: TGridPanel
      Left = 114
      Top = 1
      Width = 314
      Height = 127
      Align = alClient
      BevelOuter = bvNone
      ColumnCollection = <
        item
          Value = 33.342861973827160000
        end
        item
          Value = 33.451125237980220000
        end
        item
          Value = 33.206012788192620000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = m_MoveTopCaretToLeftBtn
          Row = 0
        end
        item
          Column = 2
          Control = m_MoveTopCaretToRightBtn
          Row = 0
        end
        item
          Column = 0
          Control = m_MoveBotCaretToLeftBtn
          Row = 1
        end
        item
          Column = 2
          Control = m_MoveBotCaretToRightBtn
          Row = 1
        end
        item
          Column = 1
          Control = m_TopSpinPanel
          Row = 0
        end
        item
          Column = 1
          Control = m_BotSpinPanel
          Row = 1
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      RowCollection = <
        item
          Value = 50.000000000000000000
        end
        item
          Value = 50.000000000000000000
        end>
      TabOrder = 0
      object m_MoveTopCaretToLeftBtn: TSpeedButton
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 98
        Height = 57
        Align = alClient
        AllowAllUp = True
        GroupIndex = 81
        Caption = '<<'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = m_MoveTopCaretToLeftBtnClick
        ExplicitLeft = 71
        ExplicitWidth = 62
        ExplicitHeight = 30
      end
      object m_MoveTopCaretToRightBtn: TSpeedButton
        AlignWithMargins = True
        Left = 212
        Top = 3
        Width = 99
        Height = 57
        Align = alClient
        AllowAllUp = True
        GroupIndex = 81
        Caption = '>>'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = m_MoveTopCaretToLeftBtnClick
        ExplicitLeft = 223
        ExplicitWidth = 62
        ExplicitHeight = 30
      end
      object m_MoveBotCaretToLeftBtn: TSpeedButton
        AlignWithMargins = True
        Left = 3
        Top = 66
        Width = 98
        Height = 58
        Align = alClient
        AllowAllUp = True
        GroupIndex = 82
        Caption = '<<'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = m_MoveBotCaretToLeftBtnClick
        ExplicitLeft = 71
        ExplicitTop = 35
        ExplicitWidth = 62
        ExplicitHeight = 30
      end
      object m_MoveBotCaretToRightBtn: TSpeedButton
        AlignWithMargins = True
        Left = 212
        Top = 66
        Width = 99
        Height = 58
        Align = alClient
        AllowAllUp = True
        GroupIndex = 82
        Caption = '>>'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = m_MoveBotCaretToLeftBtnClick
        ExplicitLeft = 223
        ExplicitTop = 35
        ExplicitWidth = 62
        ExplicitHeight = 30
      end
      object m_TopSpinPanel: TPanel
        Left = 104
        Top = 0
        Width = 105
        Height = 63
        Align = alClient
        TabOrder = 0
        OnResize = m_TopSpinPanelResize
        DesignSize = (
          105
          63)
        object m_TopCaretOffset: TSpinEdit
          Left = 10
          Top = -1
          Width = 92
          Height = 58
          Anchors = [akLeft, akTop, akRight, akBottom]
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Myriad Pro'
          Font.Style = []
          Increment = 10
          MaxValue = 2000
          MinValue = 0
          ParentFont = False
          TabOrder = 0
          Value = 0
        end
      end
      object m_BotSpinPanel: TPanel
        Left = 104
        Top = 63
        Width = 105
        Height = 64
        Align = alClient
        TabOrder = 1
        OnResize = m_BotSpinPanelResize
        DesignSize = (
          105
          64)
        object m_BotCaretOffset: TSpinEdit
          Left = 10
          Top = -1
          Width = 92
          Height = 64
          Anchors = [akLeft, akTop, akRight, akBottom]
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Myriad Pro'
          Font.Style = []
          Increment = 10
          MaxValue = 2000
          MinValue = 0
          ParentFont = False
          TabOrder = 0
          Value = 0
        end
      end
    end
    object m_ExecBtn: TSpeedButton
      AlignWithMargins = True
      Left = 431
      Top = 4
      Width = 105
      Height = 121
      Align = alClient
      Caption = 'Exec'
      OnClick = m_ExecBtnClick
      ExplicitLeft = 291
      ExplicitTop = 3
      ExplicitWidth = 62
      ExplicitHeight = 62
    end
  end
  object m_ScanGroupsGrid: TGridPanel
    Left = 0
    Top = 129
    Width = 540
    Height = 41
    Align = alBottom
    ColumnCollection = <
      item
        Value = 14.072685559318870000
      end
      item
        Value = 14.524199363233690000
      end
      item
        Value = 14.537344594081210000
      end
      item
        Value = 14.543011315137830000
      end
      item
        Value = 13.943645045536630000
      end
      item
        Value = 14.263483735882900000
      end
      item
        Value = 14.115630386808860000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = m_ScanGroup3_Btn
        Row = 0
      end
      item
        Column = 1
        Control = m_ScanGroup5_Btn
        Row = 0
      end
      item
        Column = 2
        Control = m_ScanGroup6_Btn
        Row = 0
      end
      item
        Column = 3
        Control = m_ScanGroup8_Btn
        Row = 0
      end
      item
        Column = 4
        Control = m_ScanGroup10_Btn
        Row = 0
      end
      item
        Column = 5
        Control = m_ScanGroup12_Btn
        Row = 0
      end
      item
        Column = 6
        Control = m_ScanGroup14_Btn
        Row = 0
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    RowCollection = <
      item
        Value = 100.000000000000000000
      end>
    TabOrder = 1
    object m_ScanGroup3_Btn: TSpeedButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 69
      Height = 33
      Align = alClient
      AllowAllUp = True
      GroupIndex = 80
      Caption = '3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = m_ScanGroup3_BtnClick
      ExplicitLeft = 63
      ExplicitTop = 256
      ExplicitWidth = 30
      ExplicitHeight = 30
    end
    object m_ScanGroup5_Btn: TSpeedButton
      AlignWithMargins = True
      Left = 79
      Top = 4
      Width = 72
      Height = 33
      Align = alClient
      AllowAllUp = True
      GroupIndex = 80
      Caption = '5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = m_ScanGroup3_BtnClick
      ExplicitLeft = 63
      ExplicitTop = 256
      ExplicitWidth = 30
      ExplicitHeight = 30
    end
    object m_ScanGroup6_Btn: TSpeedButton
      AlignWithMargins = True
      Left = 157
      Top = 4
      Width = 72
      Height = 33
      Align = alClient
      AllowAllUp = True
      GroupIndex = 80
      Caption = '6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = m_ScanGroup3_BtnClick
      ExplicitLeft = 63
      ExplicitTop = 256
      ExplicitWidth = 30
      ExplicitHeight = 30
    end
    object m_ScanGroup8_Btn: TSpeedButton
      AlignWithMargins = True
      Left = 235
      Top = 4
      Width = 72
      Height = 33
      Align = alClient
      AllowAllUp = True
      GroupIndex = 80
      Caption = '8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = m_ScanGroup3_BtnClick
      ExplicitLeft = 176
      ExplicitTop = 5
      ExplicitWidth = 52
    end
    object m_ScanGroup10_Btn: TSpeedButton
      AlignWithMargins = True
      Left = 313
      Top = 4
      Width = 69
      Height = 33
      Align = alClient
      AllowAllUp = True
      GroupIndex = 80
      Caption = '10'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = m_ScanGroup3_BtnClick
      ExplicitLeft = 63
      ExplicitTop = 256
      ExplicitWidth = 30
      ExplicitHeight = 30
    end
    object m_ScanGroup12_Btn: TSpeedButton
      AlignWithMargins = True
      Left = 388
      Top = 4
      Width = 70
      Height = 33
      Align = alClient
      AllowAllUp = True
      GroupIndex = 80
      Caption = '12'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = m_ScanGroup3_BtnClick
      ExplicitLeft = 63
      ExplicitTop = 256
      ExplicitWidth = 30
      ExplicitHeight = 30
    end
    object m_ScanGroup14_Btn: TSpeedButton
      AlignWithMargins = True
      Left = 464
      Top = 4
      Width = 72
      Height = 33
      Align = alClient
      AllowAllUp = True
      GroupIndex = 80
      Caption = '14'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = m_ScanGroup3_BtnClick
      ExplicitLeft = 347
      ExplicitTop = 6
      ExplicitWidth = 56
    end
  end
  object m_TuneGrid: TGridPanel
    Left = 0
    Top = 170
    Width = 540
    Height = 32
    Align = alBottom
    ColumnCollection = <
      item
        Value = 100.000000000000000000
      end
      item
        SizeStyle = ssAbsolute
        Value = 70.000000000000000000
      end
      item
        SizeStyle = ssAbsolute
        Value = 70.000000000000000000
      end
      item
        SizeStyle = ssAbsolute
        Value = 70.000000000000000000
      end
      item
        SizeStyle = ssAbsolute
        Value = 70.000000000000000000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = m_TuneMaxCountLabel
        Row = 0
      end
      item
        Column = 1
        Control = m_TuneBtn
        Row = 0
      end
      item
        Column = 2
        Control = m_TuneClearBtn
        Row = 0
      end
      item
        Column = 3
        Control = m_ClearBScanBtn
        Row = 0
      end
      item
        Column = 4
        Control = m_HelpBtn
        Row = 0
      end>
    RowCollection = <
      item
        Value = 100.000000000000000000
      end
      item
        SizeStyle = ssAuto
      end>
    TabOrder = 2
    object m_TuneMaxCountLabel: TStaticText
      Left = 1
      Top = 1
      Width = 258
      Height = 30
      Align = alClient
      Alignment = taCenter
      Caption = 'Tune maximums: N'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object m_TuneBtn: TSpeedButton
      AlignWithMargins = True
      Left = 262
      Top = 4
      Width = 64
      Height = 24
      Align = alClient
      Caption = 'Tune'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = m_TuneBtnClick
      ExplicitLeft = 268
      ExplicitTop = 194
      ExplicitWidth = 62
      ExplicitHeight = 30
    end
    object m_TuneClearBtn: TSpeedButton
      AlignWithMargins = True
      Left = 332
      Top = 4
      Width = 64
      Height = 24
      Align = alClient
      Caption = 'Clear'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = m_TuneClearBtnClick
      ExplicitLeft = 270
      ExplicitTop = 5
      ExplicitWidth = 131
      ExplicitHeight = 33
    end
    object m_ClearBScanBtn: TSpeedButton
      AlignWithMargins = True
      Left = 402
      Top = 4
      Width = 64
      Height = 24
      Align = alClient
      Caption = 'Clear BScan'
      OnClick = m_ClearBScanBtnClick
      ExplicitLeft = 424
      ExplicitTop = 8
      ExplicitWidth = 23
      ExplicitHeight = 22
    end
    object m_HelpBtn: TSpeedButton
      AlignWithMargins = True
      Left = 472
      Top = 4
      Width = 64
      Height = 24
      Align = alClient
      Caption = 'Help'
      OnClick = m_HelpBtnClick
      ExplicitLeft = 432
      ExplicitTop = 16
      ExplicitWidth = 23
      ExplicitHeight = 22
    end
  end
  object GridPanel1: TGridPanel
    Left = 0
    Top = 231
    Width = 540
    Height = 41
    Align = alBottom
    ColumnCollection = <
      item
        SizeStyle = ssAbsolute
        Value = 50.000000000000000000
      end
      item
        SizeStyle = ssAbsolute
        Value = 100.000000000000000000
      end
      item
        Value = 100.000000000000000000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = m_UpdateCaretsPosBtn
        Row = 0
      end
      item
        Column = 1
        Control = GridPanel3
        Row = 0
      end
      item
        Column = 2
        Control = GridPanel4
        Row = 0
      end>
    RowCollection = <
      item
        Value = 100.000000000000000000
      end>
    TabOrder = 3
    object m_UpdateCaretsPosBtn: TSpeedButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 44
      Height = 33
      Align = alClient
      Caption = 'Update'
      OnClick = m_UpdateCaretsPosBtnClick
      ExplicitLeft = 40
      ExplicitTop = 32
      ExplicitWidth = 23
      ExplicitHeight = 22
    end
    object GridPanel3: TGridPanel
      Left = 51
      Top = 1
      Width = 100
      Height = 39
      Align = alClient
      ColumnCollection = <
        item
          Value = 100.000000000000000000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = m_TopCaretLabel
          Row = 0
        end
        item
          Column = 0
          Control = m_BotCaretLabel
          Row = 1
        end>
      RowCollection = <
        item
          Value = 50.000000000000000000
        end
        item
          Value = 50.000000000000000000
        end>
      TabOrder = 0
      object m_TopCaretLabel: TStaticText
        Left = 1
        Top = 1
        Width = 98
        Height = 18
        Align = alClient
        Alignment = taCenter
        Anchors = []
        Caption = 'Top: ???'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Myriad Pro'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object m_BotCaretLabel: TStaticText
        Left = 1
        Top = 19
        Width = 98
        Height = 19
        Align = alClient
        Alignment = taCenter
        Anchors = []
        Caption = 'Bot: ???'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Myriad Pro'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
    end
    object GridPanel4: TGridPanel
      Left = 151
      Top = 1
      Width = 388
      Height = 39
      Align = alClient
      ColumnCollection = <
        item
          Value = 100.000000000000000000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = m_TopCaretTrack
          Row = 0
        end
        item
          Column = 0
          Control = m_BotCaretTrack
          Row = 1
        end>
      RowCollection = <
        item
          Value = 50.000000000000000000
        end
        item
          Value = 50.000000000000000000
        end>
      TabOrder = 1
      object m_TopCaretTrack: TTrackBar
        Left = 1
        Top = 1
        Width = 386
        Height = 18
        Align = alClient
        Anchors = []
        Ctl3D = True
        Max = 100
        ParentCtl3D = False
        ShowSelRange = False
        TabOrder = 0
        ThumbLength = 10
        TickMarks = tmTopLeft
      end
      object m_BotCaretTrack: TTrackBar
        Left = 1
        Top = 19
        Width = 386
        Height = 19
        Align = alClient
        Anchors = []
        Ctl3D = True
        Max = 100
        ParentCtl3D = False
        ShowSelRange = False
        TabOrder = 1
        ThumbLength = 10
      end
    end
  end
  object GridPanel2: TGridPanel
    Left = 0
    Top = 202
    Width = 540
    Height = 29
    Align = alBottom
    ColumnCollection = <
      item
        SizeStyle = ssAbsolute
        Value = 100.000000000000000000
      end
      item
        SizeStyle = ssAbsolute
        Value = 100.000000000000000000
      end
      item
        SizeStyle = ssAbsolute
        Value = 130.000000000000000000
      end
      item
        Value = 100.000000000000000000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = m_XSysCoordLabel
        Row = 0
      end
      item
        Column = 1
        Control = m_XSysDirLabel
        Row = 0
      end
      item
        Column = 2
        Control = m_ResetPathEncoderBtn
        Row = 0
      end
      item
        Column = 3
        Control = m_XSysCrdTrLabel
        Row = 0
      end>
    RowCollection = <
      item
        Value = 100.000000000000000000
      end>
    TabOrder = 4
    object m_XSysCoordLabel: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 94
      Height = 21
      Align = alClient
      Caption = 'XSysCoord'
      Layout = tlCenter
      ExplicitWidth = 52
      ExplicitHeight = 13
    end
    object m_XSysDirLabel: TLabel
      Left = 101
      Top = 1
      Width = 100
      Height = 27
      Align = alClient
      Caption = 'XSysCoord'
      Layout = tlCenter
      ExplicitWidth = 52
      ExplicitHeight = 13
    end
    object m_ResetPathEncoderBtn: TSpeedButton
      AlignWithMargins = True
      Left = 204
      Top = 4
      Width = 124
      Height = 21
      Align = alClient
      Caption = 'Reset path encoder'
      OnClick = m_ResetPathEncoderBtnClick
      ExplicitLeft = 240
      ExplicitTop = 128
      ExplicitWidth = 117
      ExplicitHeight = 33
    end
    object m_XSysCrdTrLabel: TLabel
      Left = 331
      Top = 1
      Width = 208
      Height = 27
      Align = alClient
      Caption = 'm_XSysCrdTrLabel'
      Layout = tlCenter
      ExplicitWidth = 89
      ExplicitHeight = 13
    end
  end
  object Timer1: TTimer
    Interval = 200
    OnTimer = Timer1Timer
    Left = 16
    Top = 14
  end
  object m_ChangeModeTimer: TTimer
    Enabled = False
    Interval = 500
    OnTimer = m_ChangeModeTimerTimer
    Left = 18
    Top = 73
  end
end
