//---------------------------------------------------------------------------
#ifndef _DataStorage
#define _DataStorage

#include "ChannelsTable.h"
#include "DeviceCalibration.h"
#include "AutoconMain.h"
extern cAutoconMain *AutoconMain;
#include "DataStorage.h"
#include <stdio.h>	// For in/out - FILE*

#pragma hdrstop
//---------------------------------------------------------------------------
//#pragma package(smart_init)

//---------------------------------------------------------------------------

cJointTestingReport::cJointTestingReport(int MaxSysCoord)
{
	FromFile = false;
	Photo = NULL;
	PhoneScreenShot = NULL;     // �������� ������ �������� �������
	PhoneScreenShot2 = NULL;    // �������� ������ �������� �������
    PhoneScreenShot3 = NULL;    // �������� ������ �������� �������
    RailBottomPhoto = NULL;     // ���� ������� ������
	ScreenShotHandScanList.resize(0); // ��������� ������ ������� ������������

	Header.MaxSysCoord = MaxSysCoord; // ������������ ����������
	Header.MaxChannel = ScanChannelCount; // ���������� ������� ������������
	FExitFlag = false;
	ReportFileName = "";
    NewItemListIdx = 0;

	FCoordPtrList.resize(MaxSysCoord + 1);
	for (unsigned int idx = 0; idx < FCoordPtrList.size(); idx++)
	{
		FCoordPtrList[idx] = - 1;
	}

	bAlarmsUpdated = true;
    bHandScanStoreEnabled = false;
    bEnableSounds = true;

    PathMgr::PushPath(L"$sounds", L"resources\\sounds");
    prevSoundTime = GetTickCount();

    FFiltrState_ = false;
	for (unsigned int idx = 0; idx < ScanChannelCount - 1; idx++)
	{
        MasksCount[idx] = 0;
	}
}

//---------------------------------------------------------------------------

cJointTestingReport::~cJointTestingReport()
{
    //�������
        for(unsigned int i = 0; i < FSignalList.size(); i++)
		{
            if(FSignalList[i])
            {
                delete FSignalList[i];
                FSignalList[i] = NULL;
            }
        }
        FSignalList.resize(0);

	 Release();
}

//---------------------------------------------------------------------------

void cJointTestingReport::Release()
{
    //Clear scan items
    ClearNewItemList(NewItemListIdx);
    FHandScanList.clear();
    //FSignalList.resize(0);
    //FCoordPtrList.resize(Header.MaxSysCoord, -1);

    for(unsigned int i = 0; i < FSignalList.size(); i++)
        {
            if(FSignalList[i])
            {
                delete FSignalList[i];
                FSignalList[i] = NULL;
            }
		}
        FSignalList.resize(0);

      FCoordPtrList.resize(Header.MaxSysCoord + 1);
	  for (unsigned int idx = 0; idx < FCoordPtrList.size(); idx++)
	  {
			FCoordPtrList[idx] = - 1;
	  }
	// But now FCoordPtrList.resize called in read report function

    ClearStoredHandSignals();

    //Clear photos
    ClearScreenShotHandScanList();
	if(PhoneScreenShot)
    {
        delete PhoneScreenShot;
        PhoneScreenShot = NULL;
    }
    if(PhoneScreenShot2)
    {
        delete PhoneScreenShot2;
        PhoneScreenShot2 = NULL;
    }
    if(PhoneScreenShot3)
    {
        delete PhoneScreenShot3;
        PhoneScreenShot3 = NULL;
    }
	if (RailBottomPhoto)
    {
        delete(RailBottomPhoto);
	    RailBottomPhoto = NULL;
    }
    if (Photo)
    {
        delete(Photo);
	    Photo = NULL;
    }

    //Clear alarm and maximum items
    CleanupChannelCtrlData();
	//ClearMaximumsData();
	FAlarmList.clear();
    bAlarmsUpdated = true;
    //memset(FKPSurfaceStates,0,sizeof(FKPSurfaceStates));

    // Other stuff
    //FChannelsDelta.clear();
    //FChannelsOldValue.clear();

    //Don't know
	FExitFlag = true;
	for (unsigned int idx = 0; idx < 10; idx++) Header.TimeInHandScanChannel[idx] = 0;

}

void cJointTestingReport::ClearData(void)
{
	FromFile = false;
	Release();
}

//---------------------------------------------------------------------------

void cJointTestingReport::PutScanSignals(int SysCoord, int Channel, PsScanSignalsOneCrdOneChannel pData)
{

//	if (Channel != 0)  return;
//	if (SysCoord < 379)  return;

//	if ((FFiltrState) && (Channel != 0))  return;

	if((SysCoord >= (int)FCoordPtrList.size()) || (SysCoord < 0))
	{
		LOGERROREX(LOGCHANNEL_CHECK,"SysCoord >= FCoordPtrList.size() (1) - sc-%d, max-%d",SysCoord, FCoordPtrList.size());
		return;
	}
	//Added by KirillB
	if(SysCoord>=Header.MaxSysCoord)
	{
		LOGERROREX(LOGCHANNEL_CHECK,"Wrong sys coord - %d (channel-%d)",SysCoord, Channel);
		return;
	}

	if ((SysCoord >= 0) && (SysCoord <= Header.MaxSysCoord) && (pData->Count != 0))
	{

		if (FCoordPtrList[SysCoord] == - 1)
		{
			FSignalList.resize(FSignalList.size() + 1, NULL);
            FSignalList[FSignalList.size() - 1] = new sScanSignalsOneCrdAllChannel();

			for (int Ch = 0; Ch < ScanChannelCount; Ch++)
				FSignalList[FSignalList.size() - 1]->Channel[Ch].Count = 0;

			FCoordPtrList[SysCoord] = FSignalList.size() - 1;
		}

		//Added by KirillB (bug)
		if(Channel>=ScanChannelCount)
		{
			LOGERROREX(LOGCHANNEL_CHECK,"Wrong channel: %d (max-%d, fullIndex-%#04x)!",Channel,ScanChannelCount,Channel  + 63);
			return;
		}

		if(FCoordPtrList[SysCoord] >= (int)FSignalList.size())
        {
            assert(false && "FCoordPtrList out of bound");
            LOGERROREX(LOGCHANNEL_CHECK,"FCoordPtrList out of bound: need-%d, has-%d!",FCoordPtrList[SysCoord], FSignalList.size());
			return;
        }

        if(!FSignalList[FCoordPtrList[SysCoord]])
        {
            assert(FSignalList[FCoordPtrList[SysCoord]]);
            LOGERROREX(LOGCHANNEL_CHECK,"Channel data is NULL!!: %d (max-%d, fullIndex-%#04x)!",Channel,ScanChannelCount,Channel  + 63);
			return;
        }

//        if(Channel == 0x01)
//        {
//            TRACE("Channel PUT: %d", GetAlarmSignals(Channel).LastState[1]);
//        }

        if ((!FFiltrState_) ||
            (Channel == 9) ||  // ��1 ���� 45 ����.
            (Channel == 6) ||  // ��1 ���� 45 ����.
            (Channel == 7) ||  // ��1 0 ����.
            (Channel == 19) || // ��2 0 ����.
            (Channel == 27) || // ��3 0 ����.
            (Channel == 36) || // ��4 0 ����.
            (Channel == 47) || // ��5 0 ����.
            (Channel == 58) || // ��6 0 ����.
            (Channel == 69) || // ��7 0 ����.
            (Channel == 81))   // ��8 0 ����. // ��� ����������
        {
            pData->bIsAlarmed = GetAlarmSignals(Channel).LastState[1];
            FSignalList[FCoordPtrList[SysCoord]]->Channel[Channel] = *pData;
        }
        else // � �����������
        {


			int MinusDeltaDelay;
			int PlusDeltaDelay;
			int MinusDeltaDelay_;
			int PlusDeltaDelay_;
			bool Flg;

					 // ������ ����� ��� ���������� �������
			if ((Channel != 24) &&
                (Channel != 30) &&
                (Channel != 80)) Flg = (AutoconMain->Table->GetChannelDir(Channel + AutoconStartCID) == cdZoomIn);
                else
                {
                    if (Channel == 24) Flg = false;
                    if (Channel == 30) Flg = true;
                    if (Channel == 80) Flg = false;
                }

			if (Flg) // ��������� �� ������� �����
            {
				MinusDeltaDelay = fp.DeltaDelay;
				PlusDeltaDelay = 0;
			}
			else
			{
				MinusDeltaDelay = 0;
				PlusDeltaDelay = fp.DeltaDelay;
			};
			int PutIndexList[8];     // ��������� �������: 0 - �� ����� �� � ���� �����; 1 - ��������; 2 - ����� ���������

			for (int SIdx = 0; SIdx < pData->Count; SIdx++) PutIndexList[SIdx] = 0; // ��������� ������� ����������� ��������

			for (int SIdx = 0; SIdx < pData->Count; SIdx++) // ������������� �������
			{
			  //	pData->WorkCycle

				int cnt = 0;
				if (MasksCount[Channel] != 0)
					for (int Idx = 0; Idx < MasksMaxCount; Idx++) // ������������� �����
						if (Masks[Channel][Idx].Busy) // ����� ������������
						{

							// �������� ��������� ���������� - � ���� ��� � ����� ?
							if (SysCoord - Masks[Channel][Idx].LastSysCoord > 0)
							{
								MinusDeltaDelay_ = MinusDeltaDelay;
								PlusDeltaDelay_ = PlusDeltaDelay;
							}
							else
							{
								MinusDeltaDelay_ = PlusDeltaDelay;
								PlusDeltaDelay_ = MinusDeltaDelay;
							}

							if                                      // �������� ������� �� ��������� � ����� �� ��������
							   ((pData->Signals[SIdx].Delay >= Masks[Channel][Idx].LastDelay - MinusDeltaDelay_ * (std::abs(SysCoord - Masks[Channel][Idx].LastSysCoord))) &&
								(pData->Signals[SIdx].Delay <= Masks[Channel][Idx].LastDelay + PlusDeltaDelay_ * (std::abs(SysCoord - Masks[Channel][Idx].LastSysCoord))) &&
																	// �������� ������� �� ��������� � ����� �� ����������
								(std::abs(SysCoord - Masks[Channel][Idx].LastSysCoord) <= fp.MaxSysCoordStep))
						 //	if                                      // �������� ������� �� ��������� � ����� �� ��������
						 //	   ((pData->Signals[SIdx].Delay >= Masks[Channel][Idx].Signals[Masks[Channel][Idx].Count - 1].Signal.Delay - MinusDeltaDelay) &&
						 //		(pData->Signals[SIdx].Delay <= Masks[Channel][Idx].Signals[Masks[Channel][Idx].Count - 1].Signal.Delay + PlusDeltaDelay) &&
						 //											// �������� ������� �� ��������� � ����� �� ����������
						 //		(std::abs(SysCoord - Masks[Channel][Idx].Signals[Masks[Channel][Idx].Count - 1].SysCoord) <= fp.MaxSysCoordStep))
								{
									bool Flg = true; // �������� �������� � ���������� ���������

									if (pData->Signals[SIdx].Delay != Masks[Channel][Idx].LastDelay)
										Masks[Channel][Idx].SameDelay = fp.SameDelayCount; // �������� ������ - ��������� �������
									if (pData->Signals[SIdx].Delay == Masks[Channel][Idx].LastDelay)
									{                                                   // �������� ���������� - ��������� �������
										Masks[Channel][Idx].SameDelay--;
										if (Masks[Channel][Idx].SameDelay <= 0) Flg = false;
									}

									if (Flg)
									{
										if (Masks[Channel][Idx].OK) // ���� ������ ���������� �������� - ����������� ����� ����� ����������
										{
											PutIndexList[SIdx] = 2; // ���� ������ ����� ���������
										}
										else
																	   // �������� ���������� ����������� ���������� ������ �����
										if ((std::abs(SysCoord - Masks[Channel][Idx].StartSysCoord) >= fp.OKLength))
										{
											for (int sidx = 0; sidx < Masks[Channel][Idx].Count; sidx++) // ��������� ����� ����������� ������� � DataStorage
											{
												PsScanSignalsOneCrdOneChannel ptmp;
												ptmp = &FSignalList[FCoordPtrList[Masks[Channel][Idx].Signals_[sidx].SysCoord]]->Channel[Channel];
												if (ptmp->Count < 8)
												{
													ptmp->Signals[ptmp->Count] = Masks[Channel][Idx].Signals_[sidx].Signal;
													ptmp->Count++;
												}
											}
											Masks[Channel][Idx].OK = true;
											PutIndexList[SIdx] = 2; // ���� ������ ����� ���������
										}
										else
										{
											Masks[Channel][Idx].Signals_[Masks[Channel][Idx].Count].SysCoord = SysCoord;
											Masks[Channel][Idx].Signals_[Masks[Channel][Idx].Count].Signal = pData->Signals[SIdx];
											Masks[Channel][Idx].LastSysCoord = SysCoord;
											Masks[Channel][Idx].LastDelay = pData->Signals[SIdx].Delay;
											Masks[Channel][Idx].Count++;
											PutIndexList[SIdx] = 1; // ���� ������ �������� � �����
                                        }
                                        break;
                                    }
                                }
							cnt++;
                            if (cnt >= MasksCount[Channel]) break;
                        }
			}

			for (int SIdx = 0; SIdx < pData->Count; SIdx++) // ������������� �������
				if (PutIndexList[SIdx]  == 0) // ������ �� ����� �� � ���� �� ����� - ������� �����
					for (int Idx = 0; Idx < MasksMaxCount; Idx++) // ����� ������ ������ ��� ����� �����
						if (!Masks[Channel][Idx].Busy)
						{
							Masks[Channel][Idx].Busy = true;
							Masks[Channel][Idx].OK = false;
							Masks[Channel][Idx].Count = 1;
							Masks[Channel][Idx].SameDelay = fp.SameDelayCount;
							Masks[Channel][Idx].StartSysCoord = SysCoord;
							Masks[Channel][Idx].LastSysCoord = SysCoord;
							Masks[Channel][Idx].LastDelay = pData->Signals[SIdx].Delay;
							Masks[Channel][Idx].Signals_[0].SysCoord = SysCoord;
							Masks[Channel][Idx].Signals_[0].Signal = pData->Signals[SIdx];

							MasksCount[Channel]++; // ����������� ���������� ����� (������������ ��������� ������� Masks)
							break;
						}

			// ���������� �������� ��� ��� ���������� �����
			sScanSignalsOneCrdOneChannel newData; // ��������� ��������� ��������
			newData.Count = 0;

			for (int ti = 0; ti < pData->Count; ti++)  // ������������� �������
				if (PutIndexList[ti] == 2) // ��������� �� ������� �����
				{
					newData.Signals[newData.Count] = pData->Signals[ti];
					newData.Count++;
				}

			if (newData.Count != 0)
			{
				pData->Count = newData.Count; // �� ���� ����� ������������ - pData ?
				memcpy(pData->Signals, &newData.Signals, sizeof(tUMU_BScanSignal) * OneCrdOneChannelSignalsCount);
				FSignalList[FCoordPtrList[SysCoord]]->Channel[Channel] = *pData;
            }

			// �������� ����� - �� ����� ������� ������������������� ��������
            int cnt = 0;
			if (MasksCount[Channel] != 0)
                for (int Idx = 0; Idx < MasksMaxCount; Idx++) // ������������� �����
                    if (Masks[Channel][Idx].Busy)
					{
						if (std::abs(SysCoord - Masks[Channel][Idx].LastSysCoord) > fp.MaxSysCoordStep)
						{
							 Masks[Channel][Idx].Busy = false;
                        }
                        cnt++;
                        if (cnt >= MasksCount[Channel]) break;
					}
        }

		//---------------��������� ����������-------------------------
		int AbsChannelId = Channel + 0x3F;
        if(AbsChannelId >= (int)GetChannelCtrlDataCount())
        {
            TRACE("!!! count=%d, size=%d", GetChannelCtrlDataCount(),AbsChannelId);
            //assert(false);
        }
        else
        {
            sChannelControlData& curChanCtl = *GetChannelCtrlData( AbsChannelId );
            //���������� ����������
            float currAmplMax = -1;
            bool bAmplFound = false;

            if(INRANGE_EQ(SysCoord,curChanCtl.collectIntervalMin, curChanCtl.collectIntervalMax) ||
                    (curChanCtl.collectIntervalMin == -1))
            {
                for(int nSig = 0; nSig < pData->Count; nSig++)
                {
                    float RealDelay = float(pData->Signals[nSig].Delay) / float(curChanCtl.BScanMult);
                    if(INRANGE_EQ(int(RealDelay),curChanCtl.channelStrobes[0][0],curChanCtl.channelStrobes[0][1]) &&
                         INRANGE_EQ(int(RealDelay),curChanCtl.channelStrobes[1][0],curChanCtl.channelStrobes[1][1]))
                    {
                        for(int i = 0; i < 24; i++)
                            currAmplMax = std::max((int)currAmplMax, (int)pData->Signals[nSig].Ampl[i]);
                        bAmplFound = true;
                    }
                }
            }


            if(bAmplFound)
            {
                curChanCtl.currentMaximum =
                                std::max(curChanCtl.currentMaximum, int(currAmplMax));
            }
        }
		//--------------------------------------------------------

		if (NewItemListIdx < NewItemListSize)
		{
		  NewItemList[NewItemListIdx].Crd = SysCoord;
		  NewItemList[NewItemListIdx].Ch = Channel;
		  NewItemListIdx++;
		}
	}
}

//---------------------------------------------------------------------------

PsScanSignalsOneCrdOneChannel cJointTestingReport::GetScanSignals(int SysCoord, int Channel)
{
	bool res;
	return GetFirstScanSignals(SysCoord, Channel, &res);
}

//---------------------------------------------------------------------------

PsScanSignalsOneCrdOneChannel cJointTestingReport::GetFirstScanSignals(int SysCoord, int Channel, bool* Result)
{
     FSearchSysCoord = SysCoord;
	 FSearchChannel = Channel;

	 if((FSearchSysCoord >= (int)FCoordPtrList.size()) || (FSearchSysCoord < 0))
	 {
		 LOGERROREX(LOGCHANNEL_CHECK,"SysCoord >= FCoordPtrList.size() (2)!!! - sc-%d, max-%d",SysCoord, FCoordPtrList.size());
		 *Result = false;
			return NULL;
	 }

	 if ((FSearchSysCoord >= 0) && (FSearchSysCoord <= Header.MaxSysCoord))
	 {
		  if (FCoordPtrList[FSearchSysCoord] == - 1)
		  {
              FSearchSysCoord++;
			  FSearchChannel = 0;
			  *Result = false;
			  return NULL;
		  }
		  else
          {
              if((FSearchChannel >= ScanChannelCount) || (FSearchChannel < 0))
              {
                  LOGERROR("Wrong channel number! FSearchChannel = %d (must be below %d)",FSearchChannel,ScanChannelCount);
                  *Result = false;
                  return NULL;
              }

              if(FCoordPtrList[FSearchSysCoord] >= (int)FSignalList.size())
              {
				assert(false && "FCoordPtrList[FSearchSysCoord] >= FSignalList.size()");
                *Result = false;
                return NULL;
              }

              if(!FSignalList[FCoordPtrList[FSearchSysCoord]])
              {
				assert(FSignalList[FCoordPtrList[FSearchSysCoord]]);
                *Result = false;
                return NULL;
              }

			  PsScanSignalsOneCrdOneChannel tmp = &FSignalList[FCoordPtrList[FSearchSysCoord]]->Channel[FSearchChannel];
	/*
			  if ((int)tmp < 0x10000)
			  {
				*Result = false;
			  }
		   */
			  if (FSearchChannel != (ScanChannelCount - 1))
			  {
				 FSearchChannel++;
			  }
			  else
			  {
				 FSearchChannel = 0;
				 FSearchSysCoord++;
			  }
			  *Result = true;
			  return tmp;
          }
	 }
     else
     {
        *Result = false;
        return NULL;
     }
}

//---------------------------------------------------------------------------

PsScanSignalsOneCrdOneChannel cJointTestingReport::GetNextScanSignals(int * SysCoord, int * Channel, bool * Result)
{
	 while (TRUE)
	 {
          if (FExitFlag)
          {
				FExitFlag = false;
                break;
          }
		  if ((FSearchSysCoord < 0) || (FSearchSysCoord >= (int)FCoordPtrList.size()))
          {
			  *Result = false;
			  return NULL;
		  }

		  if(FSearchSysCoord >= (int)FCoordPtrList.size())
		 {
			 LOGERROREX(LOGCHANNEL_CHECK,"SysCoord >= FCoordPtrList.size() (3)!!! - sc-%d, max-%d",SysCoord, FCoordPtrList.size());
			 *Result = false;
				return NULL;
		 }

		  if (FCoordPtrList[FSearchSysCoord] == - 1)
		  {
              FSearchSysCoord++;
              FSearchChannel = 0;
		      continue;
		  }
          else
          {
              int FCoord = FCoordPtrList[FSearchSysCoord];
              if( FCoord >= (int)FSignalList.size() )
              {
                    *Result = false;
			        return NULL;
              };

              if((FSearchChannel >= ScanChannelCount) || (FSearchChannel < 0))
              {
                  LOGERROR("Wrong channel number! FSearchChannel = %d (must be below %d)",FSearchChannel,ScanChannelCount);
                  *Result = false;
                  return NULL;
              }

              if(FSignalList[FCoord] == NULL)
              {
                  LOGERROR_NF("FSignalList[FCoord] == NULL");
                  assert(false && "FSignalList[FCoord] == NULL");
                  *Result = false;
                  return NULL;
              }

              PsScanSignalsOneCrdOneChannel tmp = &(FSignalList[FCoord]->Channel[FSearchChannel]);

              if (FSearchChannel != (ScanChannelCount - 1))
              {
                 FSearchChannel++;
              }
              else
              {
                 FSearchChannel = 0;
                 FSearchSysCoord++;
              }

              if (tmp->Count == 0)
                continue;

              *SysCoord = FSearchSysCoord;
              *Channel = FSearchChannel - 1;
              *Result = true;
		      return tmp;
          }
	 }
     *Result = false;
	 return NULL;
}

//---------------------------------------------------------------------------

void cJointTestingReport::EnableHandScanStore(bool bVal)
{
    bHandScanStoreEnabled = bVal;
}

void cJointTestingReport::StoreScanSignal(sHandSignalsOneCrd& signal)
{
    if(bHandScanStoreEnabled)
        storedHandSignals.BScanData.push_back(signal);
}

void cJointTestingReport::ClearStoredHandSignals()
{
    storedHandSignals.BScanData.clear();
}

int cJointTestingReport::AddStoredHandSignals(sHandScanParams Params)
{
    int iRet = AddHandSignals(Params, &(storedHandSignals.BScanData[0]), storedHandSignals.BScanData.size());
    ClearStoredHandSignals();
    return iRet;
}

int cJointTestingReport::AddHandSignals(sHandScanParams Params, PsHandSignalsOneCrd pData, int CrdCount)
{
	int NewIdx = FHandScanList.size();
	if (NewIdx > 31) return - 1;
	FHandScanList.resize(NewIdx + 1);
	FHandScanList[NewIdx].Params = Params;

    if(pData)
    {
        FHandScanList[NewIdx].BScanData.resize(CrdCount);
        memcpy(&FHandScanList[NewIdx].BScanData[0], pData, CrdCount * sizeof(sHandSignalsOneCrd));
    }
	return NewIdx;
}

//---------------------------------------------------------------------------

PsHandSignalsOneCrd cJointTestingReport::GetHandSignals(int RecIndex, sHandScanParams* Params, bool* Result, int* CrdCount)
{
    if((RecIndex < 0) || (RecIndex >= (int)FHandScanList.size()))
    {
        *Result = false;
        return NULL;
    };

//    if(FHandScanList[RecIndex].BScanData.empty())
//    {
//        *Result = false;
//        return NULL;
//    }

	*Params = FHandScanList[RecIndex].Params;
	*CrdCount  = FHandScanList[RecIndex].BScanData.size();
    *Result = true;

    if(!FHandScanList[RecIndex].BScanData.empty())
    	return &FHandScanList[RecIndex].BScanData[0];
    return NULL;
}

//---------------------------------------------------------------------------

int cJointTestingReport::GetHandScanCount(void) const
{
	return FHandScanList.size();
}

//---------------------------------------------------------------------------

bool cJointTestingReport::DeleteHandScan(int Index)
{
	FHandScanList.erase(FHandScanList.begin() + Index);
	return true;
}

//---------------------------------------------------------------------------

bool cJointTestingReport::WriteToFile(UnicodeString FileName)
{
	TFileStream * out = NULL;

    // --- ������ ���������� ��������� ---

	sFileHeaderVersion_05 head;

	head.FileID[0] = 0x52; // ������������� ����� [RSPBRAFD]
	head.FileID[1] = 0x53;
	head.FileID[2] = 0x50;
	head.FileID[3] = 0x42;
	head.FileID[4] = 0x52;
	head.FileID[5] = 0x41;
	head.FileID[6] = 0x46;
	head.FileID[7] = 0x44;
	head.FileID[8] = 0x41; // ������������� ����� [AUTOCONS]
	head.FileID[9] = 0x55;
	head.FileID[10] = 0x54;
	head.FileID[11] = 0x4F;
	head.FileID[12] = 0x43;
	head.FileID[13] = 0x4F;
	head.FileID[14] = 0x4E;
	head.FileID[15] = 0x53;

	head.Version = 5; // ������ 5
	head.HeaderDataOffset = 0;
	head.HeaderDataSize = 0;
	head.FotoDataOffset = 0;
	head.FotoDataSize = 0;
	head.ScanDataOffset = 0;
    head.ScanDataSize = 0;
	for (int i = 0; i < 32; i++) { head.HandDataOffset[i] = 0; }
	head.HandDataSize = 0;
	head.HandDataItemsCount = 0;

	head.ScreenShotDataOffset = 0;      // ��������� ������ �������� �������
	head.ScreenShotDataSize = 0;

	head.ScreenShot2_DataOffset = 0;
	head.ScreenShot2_DataSize = 0;

	head.ScreenShot3_DataOffset = 0;
	head.ScreenShot3_DataSize = 0;

	head.AlarmRecordsDataOffset = 0;    // ������ ��� ��� ������� ������
	head.AlarmRecordsDataSize = 0;
    head.AlarmRecordsItemsCount = 0;
	for (int i = 0; i < 32; i++)        // �������� ������ ������� ������������
    {
        head.HandScanScreenShotOffset[i] = 0;
        head.HandScanScreenShotSize[i] = 0;
    }
	head.HandScanScreenShotCount = 0;

	head.RailBottomFotoDataOffset = 0;       // ���� �������
	head.RailBottomFotoDataSize = 0;

    try {
	    out = new TFileStream("tmp.data", fmCreate /*fmOpenReadWrite*/);
	    out->WriteBuffer(&head, sizeof(sFileHeaderVersion_05)); // ������ ���������
    }
    catch(...)
    {
        if(out)
			out->Free();
        return false;
    }

    // --- ������ ��������������� ��������� ---

    // ��������� ����

    TStringList * TextData = new TStringList;
    TextData->Add(Header.Plaint);   	 // �����������
    TextData->Add(Header.PlantNumber);   // ��������� ����� ���������
    TextData->Add(Header.VerSoft);	     // ������ ��
    TextData->Add(Header.Operator); 	 // �������� ���
    TextData->Add(Header.DefectCode);    // ��� �������
    TextData->Add(Header.ValidityGroup); // ��. ��������
    TextData->Add(Header.�onclusion);    // ����������

    TMemoryStream * tmp = new TMemoryStream;
    TextData->SaveToStream(tmp);

    int HeaderDataSize = (int)(&(Header.END_)) - (int)(&(Header)); // ������ ��������� ��� ��������� ������

	head.HeaderDataOffset = out->Position;
	head.HeaderDataSize = HeaderDataSize + tmp->Size;
    out->WriteBuffer(&(Header.FileType), HeaderDataSize);
    out->WriteBuffer(tmp->Memory, tmp->Size); // ��������� ����

    delete tmp;
	delete TextData;

	// --- ������ �������� ������������ ---

	head.ScanDataOffset = out->Position;
	int cnt;
	for (int SysCoord = 0; SysCoord <= Header.MaxSysCoord; SysCoord++)
	{
		if (FCoordPtrList[SysCoord] == -1) continue;

		// ������� ���������� �������� �� ���������� SysCoord
		cnt = 0;
		for (int Channel = 0; Channel < ScanChannelCount; Channel++)
		{
            if(FCoordPtrList[SysCoord] >= (int)FSignalList.size())
            {
                assert(false && "FCoordPtrList[SysCoord] >= FSignalList.size()");
                continue;
            }
            if(!FSignalList[FCoordPtrList[SysCoord]])
            {
                assert(FSignalList[FCoordPtrList[SysCoord]]);
                continue;
            }
		   cnt = cnt + (FSignalList[FCoordPtrList[SysCoord]]->Channel[Channel].Count != 0);
		}

		// ������ �������� ���������� SysCoord
		if (cnt != 0)
		{
			out->WriteBuffer(&SysCoord, 4);
			out->WriteBuffer(&cnt, 4);
			for (int Channel = 0; Channel < ScanChannelCount; Channel++)
			{
                if(FCoordPtrList[SysCoord] >= (int)FSignalList.size())
                {
                    assert(false && "FCoordPtrList[SysCoord] >= FSignalList.size()");
                    continue;
                }
                if(!FSignalList[FCoordPtrList[SysCoord]])
                {
                    assert(FSignalList[FCoordPtrList[SysCoord]]);
                    continue;
                }
			   cnt = FSignalList[FCoordPtrList[SysCoord]]->Channel[Channel].Count;
			   if (cnt != 0)
			   {
				   out->WriteBuffer(&Channel, 1);
				   out->WriteBuffer(&cnt, 1);
				   out->WriteBuffer(&FSignalList[FCoordPtrList[SysCoord]]->Channel[Channel].Signals[0], cnt * sizeof(tUMU_BScanSignal));
			   }
			}
		}
	}
	head.ScanDataSize = out->Position - head.ScanDataOffset - 1;

	// --- ������ �������� ������� ������������ ---

	int RecCount;
	head.HandDataItemsCount = FHandScanList.size();
	for (int recitem = 0; recitem < head.HandDataItemsCount; recitem++)
	{
		head.HandDataOffset[recitem] = out->Position;
		// ������ ��������� ������� ������������
		out->WriteBuffer(&FHandScanList[recitem].Params, sizeof(sHandScanParams));
		// ������� ���������� ������� �-���������
        RecCount = 0;
		for (unsigned int item = 0; item < FHandScanList[recitem].BScanData.size(); item++)
			if (FHandScanList[recitem].BScanData[item].Count != 0) { RecCount++; }
		// ������ ���������� ������� �-���������
		out->WriteBuffer(&RecCount, 4);
		// ������ �������� �-���������
		for (unsigned int item = 0; item < FHandScanList[recitem].BScanData.size(); item++)
		{
			cnt = FHandScanList[recitem].BScanData[item].Count;
            if (cnt != 0)
            {
                out->WriteBuffer(&FHandScanList[recitem].BScanData[item].SysCoord, 4);
                out->WriteBuffer(&cnt, 1);
                out->WriteBuffer(&FHandScanList[recitem].BScanData[item].Signals[0], cnt * sizeof(tUMU_BScanSignal));
            }
		}
	}
	head.HandDataSize = out->Position - head.HandDataOffset[0] - 1;

	// --- ������ ���������� ---

	if (Photo)
	{
		head.FotoDataOffset = out->Position;
		Photo->SaveToStream(out);
		head.FotoDataSize = out->Position - head.FotoDataOffset - 1;
	}

	// --- ������ ���������� ������ �������� ������� ---

	if (PhoneScreenShot)
	{
		head.ScreenShotDataOffset = out->Position;
		PhoneScreenShot->SaveToStream(out);
		head.ScreenShotDataSize = out->Position - head.ScreenShotDataOffset - 1;
	}

	if (PhoneScreenShot2)
	{
		head.ScreenShot2_DataOffset = out->Position;
		PhoneScreenShot2->SaveToStream(out);
		head.ScreenShot2_DataSize = out->Position - head.ScreenShot2_DataOffset - 1;
	}

	if (PhoneScreenShot3)
	{
		head.ScreenShot3_DataOffset = out->Position;
		PhoneScreenShot3->SaveToStream(out);
		head.ScreenShot3_DataSize = out->Position - head.ScreenShot3_DataOffset - 1;
	}

	// --- ������ ���������� ������� ������ ---

	if (RailBottomPhoto)
	{
		head.RailBottomFotoDataOffset = out->Position;
		RailBottomPhoto->SaveToStream(out);
		head.RailBottomFotoDataSize = out->Position - head.RailBottomFotoDataOffset - 1;
	}

	// --- ������ ���������� ������� �������� ---

	for (unsigned int idx = 0; idx < ScreenShotHandScanList.size(); idx++)
        if ((ScreenShotHandScanList[idx]) && (idx < 32))
        {
            head.HandScanScreenShotOffset[idx] = out->Position;
            ScreenShotHandScanList[idx]->SaveToStream(out);
            head.HandScanScreenShotSize[idx] = out->Position - head.HandScanScreenShotOffset[idx] - 1;
            head.HandScanScreenShotCount++;
        }
    ClearScreenShotHandScanList();
    // --- ������ ������ ��� ��� ������� ������ ---

	head.AlarmRecordsDataOffset = out->Position;
	out->WriteBuffer(&FAlarmList[0], sizeof(sAlarmRecord) * FAlarmList.size());
	head.AlarmRecordsDataSize = out->Position - head.AlarmRecordsDataOffset - 1;
    head.AlarmRecordsItemsCount = FAlarmList.size();

    // --- ���������� ��������� � �������������� ���������� ---

	out->Position = 0;
	out->WriteBuffer(&head, sizeof(sFileHeaderVersion_05));

	out->Position = 0;
	// Create the Output and Compressed streams.
	TFileStream *output = new TFileStream(FileName, fmCreate);
	TZCompressionStream *zip = new TZCompressionStream(System::Zlib::clMax, (TStream*)output);

	// Compress data.
	zip->CopyFrom(out, out->Size);

	// Free the streams.
	zip->Free();
	out->Free();
	output->Free();
	return true;
}

//---------------------------------------------------------------------------

bool cJointTestingReport::ReadFromFile(UnicodeString filename, bool OnlyHeader, sFileHeaderVersion_05* pRawHeader)
{
    bool SaveFFiltrState = FFiltrState_;
    FFiltrState_ = false;

	Release(); //������� ���������
	FExitFlag = false;

	ReportFileName = filename;

	/* Create the Input and Decompressed streams. */
    TFileStream *input = NULL;
    TZDecompressionStream *unzip_ = NULL;
    TMemoryStream * src = NULL;

    bool bErrRead = false;
    try {
        input = new TFileStream(filename, fmOpenRead);
	    unzip_ = new TZDecompressionStream(input);
        src = new TMemoryStream;
        src->LoadFromStream(unzip_);
    }
    catch(...)
    {
        if(src)
            src->Free();//delete src;
        if(unzip_)
            unzip_->Free();//delete unzip_;
        if(input)
			input->Free();//delete input;

        FFiltrState_ = SaveFFiltrState;
        return false;
    }

    // --- �������� ���������� ��������� ---



    __try {
        sFileHeaderVersion_05 head;
        memset(&head, 0, sizeof(sFileHeaderVersion_05)); // ������� ���������� ����� ��������� ������ �� �����
        src->ReadBuffer(&head, 20); // �������� ������ 20-�� ����, ������� ����� ������ ���� ��� ����������� �� ������ ��������� - ������������� + ������ ������� �����

        switch (head.Version) {
            case 1: src->ReadBuffer(&(head.HeaderDataOffset), sizeof(sFileHeaderVersion_02) - 20); break;
            case 2: src->ReadBuffer(&(head.HeaderDataOffset), sizeof(sFileHeaderVersion_02) - 20); break;
            case 3: src->ReadBuffer(&(head.HeaderDataOffset), sizeof(sFileHeaderVersion_03) - 20); break;
            case 4: src->ReadBuffer(&(head.HeaderDataOffset), sizeof(sFileHeaderVersion_04) - 20); break;
            case 5: src->ReadBuffer(&(head.HeaderDataOffset), sizeof(sFileHeaderVersion_05) - 20); break;
        }

        if (((head.FileID[0] == 0x52) && // ������������� ����� [RSPBRAFD]
             (head.FileID[1] == 0x53) &&
             (head.FileID[2] == 0x50) &&
             (head.FileID[3] == 0x42) &&
             (head.FileID[4] == 0x52) &&
			 (head.FileID[5] == 0x41) &&
             (head.FileID[6] == 0x46) &&
             (head.FileID[7] == 0x44) &&
             (head.FileID[8] == 0x41) && // ������������� ����� [AUTOCONS]
             (head.FileID[9] == 0x55) &&
             (head.FileID[10] == 0x54) &&
             (head.FileID[11] == 0x4F) &&
             (head.FileID[12] == 0x43) &&
             (head.FileID[13] == 0x4F) &&
             (head.FileID[14] == 0x4E) &&
             (head.FileID[15] == 0x53)) &&
             ((head.Version == 1) || (head.Version == 2) || (head.Version == 3) || (head.Version == 4) || (head.Version == 5)))
        {

            // --- �������� ��������������� ��������� ---

            // ��������� ����
            TMemoryStream * tmp = new TMemoryStream; // ����� ��� ��������� ������
            TStringList * TextData = new TStringList;

            __try {
                int HeaderDataSize = (int)&(Header.END_) - (int)&(Header); // ������ ��������� ��� ��������� ������

//                if (HeaderDataSize == 1) ;    //<-- FIXME: ";" after if
//                  HeaderDataSize = (int)&(Header.END_) - (int)&(Header) - 1; // ������ ��������� ��� ��������� ������

//                int HeaderDataSize = (int)(&(Header.END_)) - (int)(&(Header)); // ������ ��������� ��� ��������� ������
//                sJointTestingReportHeader_Version_05
//                HeaderDataSize

                if (head.HeaderDataSize - HeaderDataSize > 0)
                {
                    tmp->SetSize(head.HeaderDataSize - HeaderDataSize);
                }

                //Check for wrong file size
                if(src->Size < (head.HeaderDataOffset + HeaderDataSize))
                {
                    FFiltrState_ = SaveFFiltrState;
                    return false;
                }

                src->Position = head.HeaderDataOffset;
                src->ReadBuffer(&(Header.FileType), HeaderDataSize);
                if (head.Version < 5) {

                    Header.LastSSCTestDateTime = Now();
                    Header.LastSS3RTestDateTime = Now();
                }

                src->ReadBuffer(tmp->Memory, tmp->Size);


                TextData->LoadFromStream(tmp);
        //        if (((TextData->Count == 6) && (head.Version == 1)) ||
        //            ((TextData->Count == 7) && (head.Version == 2)))
				if ((TextData->Count == 6) || (TextData->Count == 7))
                {
                    Header.Plaint        = TextData->Strings[0]; // �����������
                    Header.PlantNumber   = TextData->Strings[1]; // ��������� ����� ���������
                    Header.VerSoft       = TextData->Strings[2]; // ������ ��
                    Header.Operator      = TextData->Strings[3]; // �������� ���
                    Header.DefectCode    = TextData->Strings[4]; // ��� �������
                    Header.ValidityGroup = TextData->Strings[5]; // ��. ��������
                    Header.�onclusion    = "";
                }

                if /*(*/ (TextData->Count == 7) /* && (head.Version == 2)) */
                {
                    Header.�onclusion = TextData->Strings[6]; // ����������
                }
            }
            __finally
            {
                delete tmp;
                delete TextData;
            }

            // --- ���������� ������ � �������� ---

            if (Photo) delete(Photo);
            Photo = NULL;
            if(PhoneScreenShot) delete PhoneScreenShot;
            PhoneScreenShot = NULL;
			if(PhoneScreenShot2) delete PhoneScreenShot2;
            PhoneScreenShot2 = NULL;
            if(PhoneScreenShot3) delete PhoneScreenShot3;
            PhoneScreenShot3 = NULL;
            if (RailBottomPhoto) delete(RailBottomPhoto);
            RailBottomPhoto = NULL;

            ClearScreenShotHandScanList();
            FCoordPtrList.resize(Header.MaxSysCoord + 1);
            for (unsigned int idx = 0; idx < FCoordPtrList.size(); idx++)
            {
                FCoordPtrList[idx] = - 1;
            }

            if (!OnlyHeader)
            {
                // --- �������� �������� ������������ ---
                src->Position = head.ScanDataOffset;

                int SysCoord;
                int Channel = 0;
                int SignalCount;
                sScanSignalsOneCrdOneChannel Data;

                while (src->Position < head.ScanDataOffset + head.ScanDataSize)
                {
                    src->ReadBuffer(&SysCoord, 4);
                    src->ReadBuffer(&SignalCount, 4);
					for (int idx = 0; idx < SignalCount; idx++)
                    {
                        src->ReadBuffer(&Channel, 1);
                        src->ReadBuffer(&Data.Count, 1);

                        if(Data.Count > OneCrdOneChannelSignalsCount)
                        {
                            FFiltrState_ = SaveFFiltrState;
                            return false;
                        }

                        src->ReadBuffer(&Data.Signals[0], Data.Count * sizeof(tUMU_BScanSignal));
                        PutScanSignals(SysCoord, Channel, &Data);
                    }
                }

                // --- �������� �������� ������� ������������ ---

                int RecCount;
                FHandScanList.resize(head.HandDataItemsCount);
                for (int recitem = 0; recitem < head.HandDataItemsCount; recitem++)
                {
                    src->Position = head.HandDataOffset[recitem];
                    // �������� ��������� ������� ������������
                    src->ReadBuffer(&FHandScanList[recitem].Params, sizeof(sHandScanParams));
                    // �������� ���������� ������� �-���������
                    src->ReadBuffer(&RecCount, 4);
                    // �������� �������� �-���������
					FHandScanList[recitem].BScanData.resize(RecCount);
                    for (int item = 0; item < RecCount; item++)
                    {
                        src->ReadBuffer(&FHandScanList[recitem].BScanData[item].SysCoord, 4);
                        src->ReadBuffer(&FHandScanList[recitem].BScanData[item].Count, 1);
                        src->ReadBuffer(&FHandScanList[recitem].BScanData[item].Signals[0], FHandScanList[recitem].BScanData[item].Count * sizeof(tUMU_BScanSignal));
                    }
                }

                // --- �������� ���������� ����� ---

                if (head.FotoDataSize != 0)
                {
                    src->Position = head.FotoDataOffset;
                    TMemoryStream * tmp = new TMemoryStream;
                    tmp->SetSize(head.FotoDataSize);
                    src->ReadBuffer(tmp->Memory, tmp->Size);
                    if (!Photo) Photo = new TJPEGImage;
                    Photo->LoadFromStream(tmp);
                    delete tmp;
                }  /*
                else
                {
                    head.FotoDataOffset = 0;
                    head.FotoDataSize = 0;
                }*/ ;

                // --- �������� ���������� ������ �������� ������� ---

                if (head.ScreenShotDataSize > 0) {

                    src->Position = head.ScreenShotDataOffset;
                    TMemoryStream * tmp = new TMemoryStream;
                    tmp->SetSize(head.ScreenShotDataSize);
                    src->ReadBuffer(tmp->Memory, tmp->Size);
                    if (!PhoneScreenShot) PhoneScreenShot = new TJPEGImage;
                    PhoneScreenShot->LoadFromStream(tmp);
//                    PhoneScreenShot->SaveToFile("d:\\a_src.jpg");
                    TBitmap * tmp_ = new TBitmap();
                    tmp_->Assign(PhoneScreenShot);

                    tmp_->Canvas->Brush->Color = clBlack;
                    tmp_->Canvas->FillRect(Rect(  0,  92, 256, 111));
//                    tmp_->Canvas->FillRect(Rect(  0, 300,  28, 320));
//                    tmp_->Canvas->FillRect(Rect( 68, 300,  98, 320));
//                    tmp_->Canvas->FillRect(Rect(133, 300, 163, 320));

                    tmp_->Height = PhoneScreenShot->Height + 40;

                    tmp_->Canvas->FillRect(Rect(0, tmp_->Height - 40, 256, tmp_->Height));

                    tmp_->Canvas->Font->Color = clWhite;
                    tmp_->Canvas->Brush->Style = bsClear;
                    tmp_->Canvas->Font->Size = 11;
                    tmp_->Canvas->TextOutW( 25,  90, "���������� ��������� [���]");

					tmp_->Canvas->CopyRect(Rect(48, 300, 89, 320), tmp_->Canvas, Rect(28, 300, 69, 320));
                    tmp_->Canvas->CopyRect(Rect(48, 320, 84, 340), tmp_->Canvas, Rect(99, 300, 135, 320));
                    tmp_->Canvas->CopyRect(Rect(48, 340, 85, 360), tmp_->Canvas, Rect(163, 300, 200, 320));

                    tmp_->Canvas->Brush->Color = clBlack;
                    tmp_->Canvas->FillRect(Rect(0, 300,  49, 320));
                    tmp_->Canvas->FillRect(Rect(88, 300,  256, 320));

                    tmp_->Canvas->Brush->Style = bsClear;
                    tmp_->Canvas->TextOutW(  3, 298, "���:           [��]");
                    tmp_->Canvas->TextOutW(  3, 318, "����:         [��]");
                    tmp_->Canvas->TextOutW(  3, 338, "����:          [��]");
                    tmp_->Canvas->TextOutW(242, 279, "�");

                    tmp_->Canvas->CopyRect(Rect(0, 0, 256, 270), tmp_->Canvas, Rect(0, 90, 256, 360));
                    tmp_->Height = tmp_->Height - 90;

                    PhoneScreenShot->Assign(tmp_);
//                    PhoneScreenShot->SaveToFile("d:\\a_res.jpg");
                    delete tmp_;
                    delete tmp;
                }

                if (head.ScreenShot2_DataSize > 0) {

                    src->Position = head.ScreenShot2_DataOffset;
                    TMemoryStream * tmp = new TMemoryStream;
                    tmp->SetSize(head.ScreenShot2_DataSize);
					src->ReadBuffer(tmp->Memory, tmp->Size);
                    if (!PhoneScreenShot2) PhoneScreenShot2 = new TJPEGImage;
                    PhoneScreenShot2->LoadFromStream(tmp);
//                    PhoneScreenShot2->SaveToFile("d:\\b_src.jpg");
                    TBitmap * tmp_ = new TBitmap();
                    tmp_->Assign(PhoneScreenShot2);

                    tmp_->Canvas->Brush->Color = clBlack;
                    tmp_->Canvas->FillRect(Rect(  0,  92, 256, 111));
//                    tmp_->Canvas->FillRect(Rect(  0, 300,  28, 320));
//                    tmp_->Canvas->FillRect(Rect( 68, 300,  98, 320));
//                    tmp_->Canvas->FillRect(Rect(133, 300, 163, 320));

                    tmp_->Height = PhoneScreenShot2->Height + 40;

                    tmp_->Canvas->FillRect(Rect(0, tmp_->Height - 40, 256, tmp_->Height));

                    tmp_->Canvas->Font->Color = clWhite;
                    tmp_->Canvas->Brush->Style = bsClear;
                    tmp_->Canvas->Font->Size = 11;
                    tmp_->Canvas->TextOutW( 25,  90, "���������� ��������� [���]");

                    tmp_->Canvas->CopyRect(Rect(48, 300, 89, 320), tmp_->Canvas, Rect(28, 300, 69, 320));
                    tmp_->Canvas->CopyRect(Rect(48, 320, 84, 340), tmp_->Canvas, Rect(99, 300, 135, 320));
                    tmp_->Canvas->CopyRect(Rect(48, 340, 85, 360), tmp_->Canvas, Rect(163, 300, 200, 320));

                    tmp_->Canvas->Brush->Color = clBlack;
                    tmp_->Canvas->FillRect(Rect(0, 300,  49, 320));
					tmp_->Canvas->FillRect(Rect(88, 300,  256, 320));

                    tmp_->Canvas->Brush->Style = bsClear;
                    tmp_->Canvas->TextOutW(  3, 298, "���:           [��]");
                    tmp_->Canvas->TextOutW(  3, 318, "����:         [��]");
                    tmp_->Canvas->TextOutW(  3, 338, "����:          [��]");
                    tmp_->Canvas->TextOutW(242, 279, "�");

                    tmp_->Canvas->CopyRect(Rect(0, 0, 256, 270), tmp_->Canvas, Rect(0, 90, 256, 360));
                    tmp_->Height = tmp_->Height - 90;

                    PhoneScreenShot2->Assign(tmp_);
//                    PhoneScreenShot2->SaveToFile("d:\\b_res.jpg");
                    delete tmp_;
                    delete tmp;
                }

                if (head.ScreenShot3_DataSize > 0) {

                    src->Position = head.ScreenShot3_DataOffset;
                    TMemoryStream * tmp = new TMemoryStream;
                    tmp->SetSize(head.ScreenShot3_DataSize);
                    src->ReadBuffer(tmp->Memory, tmp->Size);
                    if (!PhoneScreenShot3) PhoneScreenShot3 = new TJPEGImage;
                    PhoneScreenShot3->LoadFromStream(tmp);
                    TBitmap * tmp_ = new TBitmap();
                    tmp_->Assign(PhoneScreenShot3);

					tmp_->Canvas->Brush->Color = clBlack;
                    tmp_->Canvas->FillRect(Rect(  0,  92, 256, 111));
//                    tmp_->Canvas->FillRect(Rect(  0, 300,  28, 320));
//                    tmp_->Canvas->FillRect(Rect( 68, 300,  98, 320));
//                    tmp_->Canvas->FillRect(Rect(133, 300, 163, 320));

                    tmp_->Height = PhoneScreenShot3->Height + 40;

                    tmp_->Canvas->FillRect(Rect(0, tmp_->Height - 40, 256, tmp_->Height));

                    tmp_->Canvas->Font->Color = clWhite;
                    tmp_->Canvas->Brush->Style = bsClear;
                    tmp_->Canvas->Font->Size = 11;
                    tmp_->Canvas->TextOutW( 25,  90, "���������� ��������� [���]");

                    tmp_->Canvas->CopyRect(Rect(48, 300, 89, 320), tmp_->Canvas, Rect(28, 300, 69, 320));
                    tmp_->Canvas->CopyRect(Rect(48, 320, 84, 340), tmp_->Canvas, Rect(99, 300, 135, 320));
                    tmp_->Canvas->CopyRect(Rect(48, 340, 85, 360), tmp_->Canvas, Rect(163, 300, 200, 320));

                    tmp_->Canvas->Brush->Color = clBlack;
                    tmp_->Canvas->FillRect(Rect(0, 300,  49, 320));
                    tmp_->Canvas->FillRect(Rect(88, 300,  256, 320));

                    tmp_->Canvas->Brush->Style = bsClear;
                    tmp_->Canvas->TextOutW(  3, 298, "���:           [��]");
                    tmp_->Canvas->TextOutW(  3, 318, "����:         [��]");
                    tmp_->Canvas->TextOutW(  3, 338, "����:          [��]");
                    tmp_->Canvas->TextOutW(242, 279, "�");

                    tmp_->Canvas->CopyRect(Rect(0, 0, 256, 270), tmp_->Canvas, Rect(0, 90, 256, 360));
                    tmp_->Height = tmp_->Height - 90;


                    PhoneScreenShot3->Assign(tmp_);
                    delete tmp_;
                    delete tmp;
                }

                // --- �������� ���������� ������� �������� ---

                for (int idx = 0; idx < head.HandScanScreenShotCount; idx++)
                    {
                        src->Position = head.HandScanScreenShotOffset[idx];
                        TMemoryStream * tmp = new TMemoryStream;
                        tmp->SetSize(head.HandScanScreenShotSize[idx]);
                        src->ReadBuffer(tmp->Memory, tmp->Size);
                        TJPEGImage * ss = new TJPEGImage;
                        ss->LoadFromStream(tmp);
                        ScreenShotHandScanList.push_back(ss);
                        delete tmp;
                    }


                // --- �������� ���� ������� ������ ---

                if (head.RailBottomFotoDataSize > 0) {

                    src->Position = head.RailBottomFotoDataOffset;
                    TMemoryStream * tmp = new TMemoryStream;
                    tmp->SetSize(head.RailBottomFotoDataSize);
                    src->ReadBuffer(tmp->Memory, tmp->Size);
                    if (!RailBottomPhoto) RailBottomPhoto = new TJPEGImage;
                    RailBottomPhoto->LoadFromStream(tmp);
                    delete tmp;
                }

                // --- �������� ������ ��� ��� ������� ������ ---

                if (head.AlarmRecordsDataSize != 0) {
                    FAlarmList.resize(head.AlarmRecordsItemsCount);
                    src->Position = head.AlarmRecordsDataOffset;
                    src->ReadBuffer(&FAlarmList[0], head.AlarmRecordsDataSize);
                }
            }
        }

        if(pRawHeader)
            *pRawHeader = head;
    }
    __finally
    {
        if(src)
            src->Free();//delete src;
        if(unzip_)
			unzip_->Free();//delete unzip_;
		if(input)
			input->Free();//delete input;
	}


	FFiltrState_ = SaveFFiltrState;

	FromFile = true;
	return true;
}

//---------------------------------------------------------------------------

void cJointTestingReport::ClearScreenShotHandScanList()
{
    for(unsigned int i = 0; i < ScreenShotHandScanList.size(); i++)
    {
        if(ScreenShotHandScanList[i])
            delete ScreenShotHandScanList[i];
    };
    ScreenShotHandScanList.clear();
};

void cJointTestingReport::ClearNewItemList(int Idx)
{
    NewItemListIdx = 0;

    memcpy(&NewItemList[0], &NewItemList[Idx], (NewItemListIdx - Idx) * sizeof(TNewItemStruct));
}

//--------------------------------------------------------------------------

//����� ������� ������� �������
bool cJointTestingReport::CheckForLossContactDefects(cDeviceCalibration* Calib, int workCycle)
{
	bool bGapsFound = false;
	return false;

    int LastKnownPositions[9]; //��������� ����������, �� ������� ������ ���
    memset(LastKnownPositions,-1,sizeof(LastKnownPositions));

    /*int SurfaceChannelsGates[9][2]; //[����� �������][������ ������, ����� ������]
    for(int j = 1; j < 9; j ++) //���������� ������� �������
    {
        int chId = GetSurfaceChannelIdByKP(j);
        SurfaceChannelsGates[j][0] =
                Calib->GetStGate(dsNone, chId, 0); //������ ������
        SurfaceChannelsGates[j][1] =
                Calib->GetEdGate(dsNone, chId, 0); //����� ������
    } */

    for(unsigned int crdX = 0; crdX < FCoordPtrList.size(); crdX++) //������ �� ���� �����������
    {
        int nSig = FCoordPtrList[crdX];
        if( nSig == -1 )
            continue;

        for(int j = 1; j < 9; j ++)
        {
            int chIdRel = GetSurfaceChannelIdByKP(j) - 0x3F;
            sChannelControlData* pCtrlData = GetChannelCtrlData(GetSurfaceChannelIdByKP(j));

            assert(nSig < (int)FSignalList.size());
            assert(FSignalList[nSig]);

            sScanSignalsOneCrdOneChannel& sig = FSignalList[nSig]->Channel[chIdRel];

            if((sig.Count == 0) ||            //���� ��� ��������
                (sig.WorkCycle != workCycle))  //��� ������� �� �� ���� �������� �����
                continue;                      //�� ����������

            for (int idx = 0; idx < sig.Count; idx++)
            {
                float RealDelay = float(sig.Signals[idx].Delay) / float(pCtrlData->BScanMult);
                //���� ������ �� ������ � ����� ������� ������� - ����������
                if ((RealDelay < pCtrlData->channelStrobes[1][0]) ||
                        (RealDelay > pCtrlData->channelStrobes[1][1]))
                    continue;

                //���� ������ �� ������ � ����� b-��������� - ����������
                if ((RealDelay < pCtrlData->channelStrobes[0][0]) ||
                        (RealDelay > pCtrlData->channelStrobes[0][1]))
                    continue;

                unsigned int coordDistance = 0;
                if(LastKnownPositions[j] != -1)
                   coordDistance = abs(int(crdX) - LastKnownPositions[j]);

                LastKnownPositions[j] = crdX;

                const unsigned int MaxCoordGap = 10; //(��?)

                if(coordDistance > MaxCoordGap)
                {
                    bGapsFound = true;
                    LOGWARNING("(KP%d, ch-0x%X) Gaps found: crd-%dmm, wc-%d, dist-%d",
                                    j,GetSurfaceChannelIdByKP(j),crdX - 500,workCycle,coordDistance);
                    pCtrlData->bHasLossContactDefect |= true;
                }

            }

        }
    }
    return bGapsFound;
}

//��������� ������ �������� ��� ������
sChannelControlData* cJointTestingReport::GetChannelCtrlData(unsigned int idx)
{
	if(idx >= FChanCtrlData.size())
		return NULL;
    return &FChanCtrlData[idx];
}

unsigned int cJointTestingReport::GetChannelCtrlDataCount()
{
    return FChanCtrlData.size();
}

// ������� ������ ���������� ��� ���� �������
void cJointTestingReport::ClearChannelMaximumsData()
{
    for(unsigned int i = 0; i < FChanCtrlData.size(); i++)
    {
        FChanCtrlData[i].currentMaximum = -1;
    }
}
// ��������� ������ �������� ����������� �� ������� ��
int cJointTestingReport::GetSurfaceChannelIdByKP(unsigned int kp_index) const
{
    const static int RailCheckChannels[] = {0,0x46,0x52,0x5A,0x63,0x6E,0x79,0x84,0x90};
    return RailCheckChannels[kp_index];
}

void cJointTestingReport::CleanupChannelCtrlData()
{
    for(unsigned int i = 0; i < FChanCtrlData.size(); i++)
    {
        FChanCtrlData[i].currentMaximum = -1;
        FChanCtrlData[i].bHasAlarmDefect = false;
        FChanCtrlData[i].bHasLossContactDefect = false;
        FChanCtrlData[i].lossContactPos = 0;
        FChanCtrlData[i].controlState = -1;
    }
}

void cJointTestingReport::SetChannelCollectInterval(unsigned int idx, int int1, int int2)
{
    FChanCtrlData[idx].collectIntervalMax = std::max(int1,int2);
    FChanCtrlData[idx].collectIntervalMin = std::min(int1,int2);
}

int cJointTestingReport::GetChannelMaximum(unsigned int index)
{
    return (index >= FChanCtrlData.size()) ? -1 : FChanCtrlData[index].currentMaximum;
}

void cJointTestingReport::UpdateCtrlStateForSearchMode()
{
	for(unsigned int i = 0; i < FChanCtrlData.size(); i++)
	{
		FChanCtrlData[i].bHasAlarmDefect = GetAlarmSignals(FChanCtrlData[i].id - 0x3F).State[1];
		bool bChanIsAlarmed = FChanCtrlData[i].bHasAlarmDefect;
		if(FChanCtrlData[i].isSurfaceControlChannel)
			bChanIsAlarmed |= FChanCtrlData[i].bHasLossContactDefect;
		FChanCtrlData[i].controlState = bChanIsAlarmed ? 2 : 0;
	}
}

void cJointTestingReport::UpdateCtrlStateForTuneMode(cChannelsTable* Tbl)
{
    return;
    sChannelDescription ChanDesc;
    for(unsigned int i = 0; i < FChanCtrlData.size(); i++)
    {
        if(!Tbl->ItemByCID(FChanCtrlData[i].id, &ChanDesc))
        {
            LOGERROR("Channel %d not found!",FChanCtrlData[i].id);
            continue;
        }

        int chMax = FChanCtrlData[i].currentMaximum;
        FChanCtrlData[i].BScanMult = ChanDesc.cdBScanDelayMultiply;

        if(chMax < 0)
        {
           //FChanCtrlData[i].controlState = 2;
        }
        else
        {
            float Tmp = (float)chMax / (float)32;
            int N = 0;
            if(Tmp!=0)
                N = (int)(20 * log10 (Tmp));

            if((N <= 0) || (N >= 18))
            {
                FChanCtrlData[i].controlState = 2; //error
            }
            else if( abs(N - ChanDesc.RecommendedSens[0]) < 2)
            {
				FChanCtrlData[i].controlState = 0; //ok
            }
            else
            {
                FChanCtrlData[i].controlState = 1; //warning
            }
        }
    }
}

/*int cJointTestingReport::GetChannelMaximum(unsigned int index)
{
	if(index >= FTuneMaximumsList.size())
		return -1;

	return FTuneMaximumsList[index];
}

void cJointTestingReport::AddChannelMaximum(unsigned int index, int chMax)
{
	if(index>200)
	{
		LOGERROR("Wrong channel index: %d",index);
		return;
    }
	if(FTuneMaximumsList.size() <= index)
		FTuneMaximumsList.resize(index+1,-1);

	FTuneMaximumsList[index] = std::max(FTuneMaximumsList[index], chMax);
}

unsigned int cJointTestingReport::GetChannelMaximumCount()
{
	return FTuneMaximumsList.size();
}

unsigned int cJointTestingReport::GetChannelMaximumAliveCount()
{
	unsigned int count = 0;
	for( unsigned int i = 0; i < FTuneMaximumsList.size(); i++)
	{
		if(FTuneMaximumsList[i] > 0)
			count++;
    }
	return count;
}

void cJointTestingReport::ClearMaximumsData()
{
	FTuneMaximumsList.clear();
}

bool cJointTestingReport::GetKPSurfaceState(unsigned int index)
{
	if(index>=9)
		return false;
	return FKPSurfaceStates[index];
}*/

void cJointTestingReport::UpdateChannelIndicationData()
{
    FChanCtrlData.clear();
    sChannelDescription chDesc;

    for(unsigned int i = 0; i < AutoconMain->Table->Count(); i++)
    {
        CID id;
        if(!AutoconMain->Table->CIDByIndex(&id, i))
            continue;
        sChannelControlData ctrlData;
        ctrlData.id = id;

        if(id >= (int)FChanCtrlData.size())
            FChanCtrlData.resize(id + 1);

        FChanCtrlData[id] = ctrlData;

        AutoconMain->Table->ItemByCID(id, &chDesc);
        FChanCtrlData[id].channelStrobes[0][0] = chDesc.cdBScanGate.gStart;
        FChanCtrlData[id].channelStrobes[0][1] = chDesc.cdBScanGate.gEnd;
        FChanCtrlData[id].channelStrobes[1][0] = AutoconMain->Calibration->GetStGate(dsNone, id, 0);
        FChanCtrlData[id].channelStrobes[1][1] = AutoconMain->Calibration->GetEdGate(dsNone, id, 0);
        FChanCtrlData[id].channelStrobes[2][0] = AutoconMain->Calibration->GetStGate(dsNone, id, 1);
        FChanCtrlData[id].channelStrobes[2][1] = AutoconMain->Calibration->GetEdGate(dsNone, id, 1);
    }

	for(int i=1;i<9;i++)
	{
        int chId = GetSurfaceChannelIdByKP(i);
        FChanCtrlData[chId].isSurfaceControlChannel = true;
    }
}
/*
int cJointTestingReport::GetKPSurfaceChannelId(unsigned int kp_index)
{
    return FKPCheckChannels[kp_index].id;
}
*/
void cJointTestingReport::AddAlarmSignals(int Channel, sAlarmRecord& item, bool bHandScan)
{
	if( (Channel >= ScanChannelCount ) || (Channel < 0))
	{
		LOGERROREX(LOGCHANNEL_CHECK,"Wrong channel: %d (max-%d, fullIndex-%#04x)!",Channel,ScanChannelCount,Channel  + 63);
		return;
	}

	if( Channel >= (int)FAlarmList.size() )
	{
		FAlarmList.resize( Channel + 1 );
		bAlarmsUpdated = true;
	}

//    if(item.State[1])
//    {
//        TRACE("Channel %d ALARM: %d", Channel, item.State[1]);
//    }

    if(bHandScan)
    {
        DWORD currTime = GetTickCount();
        if(item.State[1] && bEnableSounds && (currTime - prevSoundTime) > 500)
        {
            prevSoundTime = currTime;
            AnsiString path = PathMgr::GetPath(L"($sounds)\\asd.wav");
            PlaySoundA(path.c_str(),0, SND_ASYNC | SND_NOSTOP | SND_NOWAIT);
        }
    }
    else
    {
        bool bNewVal;
        for(unsigned int i = 0; i < 4; i++ )
        {
			bNewVal = FAlarmList[Channel].State[i] | item.State[i];
            if(bNewVal != FAlarmList[Channel].State[i])
            {
                bAlarmsUpdated = true;

                DWORD currTime = GetTickCount();
                if(bNewVal && bEnableSounds && (currTime - prevSoundTime) > 500)
                {
                    prevSoundTime = currTime;
                    AnsiString path = PathMgr::GetPath(L"($sounds)\\asd.wav");
                    PlaySoundA(path.c_str(),0, SND_ASYNC | SND_NOSTOP | SND_NOWAIT);
                }
            }
            FAlarmList[Channel].State[i] = bNewVal;
            FAlarmList[Channel].LastState[i] = item.State[i];
        }
    }
}

void cJointTestingReport::EnableAsdSounds(bool bVal)
{
    bEnableSounds = bVal;
}

sAlarmRecord cJointTestingReport::GetAlarmSignals(int Channel)
{
	if( (Channel >= (int)FAlarmList.size()) || (Channel < 0))
	{
     	return sAlarmRecord();
	}
	return FAlarmList[Channel];
}

bool cJointTestingReport::CheckAlarmUpdated(bool bResetUpdateToFalse)
{
	bool bRet = bAlarmsUpdated;
	if(bResetUpdateToFalse)
		bAlarmsUpdated = false;
	return bRet;
}

int cJointTestingReport::GetAlarmSignalsCount()
{
	return FAlarmList.size();
}

void cJointTestingReport::ClearAlarmSignals()
{
	FAlarmList.clear();
}


float cJointTestingReport::GetTimeInHandScanChannel(CID cid)
{
    switch (cid)
    {
        case 0x1E: return Header.TimeInHandScanChannel[0]; break;
        case 0x1D: return Header.TimeInHandScanChannel[1]; break;
        case 0x1F: return Header.TimeInHandScanChannel[2]; break;
        case 0x20: return Header.TimeInHandScanChannel[3]; break;
        case 0x21: return Header.TimeInHandScanChannel[4]; break;
        case 0x22: return Header.TimeInHandScanChannel[5]; break;
        case 0x23: return Header.TimeInHandScanChannel[6]; break;
    }
    return 0;
}

float cJointTestingReport::GetTimeInHandScanChannelByAngle(int angle,eInspectionMethod meth)
{
    switch (angle)
    {
        case 0:
            return (meth==imEcho) ?
                Header.TimeInHandScanChannel[0] :
                Header.TimeInHandScanChannel[1];
            break;
        case 45: return Header.TimeInHandScanChannel[2]; break;
        case 50: return Header.TimeInHandScanChannel[3]; break;
        case 58: return Header.TimeInHandScanChannel[4]; break;
        case 65: return Header.TimeInHandScanChannel[5]; break;
        case 70: return Header.TimeInHandScanChannel[6]; break;
    }
    return 0;
}

void cJointTestingReport::SetTimeInHandScanChannel(CID cid, float NewValue)
{
    switch (cid)
    {
        case 0x1E: Header.TimeInHandScanChannel[0] = NewValue; break;
        case 0x1D: Header.TimeInHandScanChannel[1] = NewValue; break;
        case 0x1F: Header.TimeInHandScanChannel[2] = NewValue; break;
        case 0x20: Header.TimeInHandScanChannel[3] = NewValue; break;
        case 0x21: Header.TimeInHandScanChannel[4] = NewValue; break;
        case 0x22: Header.TimeInHandScanChannel[5] = NewValue; break;
        case 0x23: Header.TimeInHandScanChannel[6] = NewValue; break;
    }
}

// ��������� ���������� ������ ��������
sScanChannelParams cJointTestingReport::GetChannelParams(CID Channel, int GateIdx)
{
	if (GateIdx == 1)
		return Header.ChannelParams[Channel - 0x3F];
    if (Channel == 0x46)
        return Header.ChannelParams_WP1N0EMS_0x46;
    if (Channel == 0x90)
        return Header.ChannelParams_WP8N0EMS_0x90;
    return sScanChannelParams(); //fixme
}

// ��������� ���������� ������ ��������
void cJointTestingReport::SetChannelParams(CID Channel, int GateIdx, sScanChannelParams Par)
{
    if (GateIdx == 1)
        Header.ChannelParams[Channel - 0x3F] = Par;
    else
    {
        if (Channel == 0x46)
            Header.ChannelParams_WP1N0EMS_0x46 = Par;
        else if (Channel == 0x90)
            Header.ChannelParams_WP8N0EMS_0x90 = Par;
    }
}

void cJointTestingReport::ResetFilter_(void)
{
    memset(&(Masks[0][0]), sizeof(Masks), 0);
}

void cJointTestingReport::SetFilterState_(bool State)
{
    FFiltrState_ = State;
}

int DCount = 0;

void cJointTestingReport::CalcAlarmSignals(void)
{
	const int KPChIdxs[9][2] = {{0, 0},
								{0, 14},
								{15, 23},
								{24, 30},
								{31, 41},
								{42, 52},
								{53, 63},
								{64, 74},
								{75, 86}};

	sAlarmRecord tmp;
	sScanChannelParams prms;
	PsScanSignalsOneCrdOneChannel sgl;
	bool Result;


	ClearAlarmSignals();

	for (int KP = 1; KP <= 8; KP++)
		for (int ch = KPChIdxs[KP][0]; ch <= KPChIdxs[KP][1]; ch++)
		{
	//	for (int ch = 0; ch < 86; ch++)

			// ������� ������ ������ !!!!

			if ((ch != 7)  &&  // ��1 0 ����.
				(ch != 19) &&  // ��2 0 ����.
				(ch != 27) && // ��3 0 ����.
				(ch != 36) && // ��4 0 ����.
				(ch != 47) && // ��5 0 ����.
				(ch != 58) && // ��6 0 ����.
				(ch != 69) && // ��7 0 ����.
				(ch != 81))   // ��8 0 ����. // ��� ����������
			{
				tmp.State[1] = false;
				tmp.Channel = ch;
				DCount++;
				if (FromFile)
					prms = GetChannelParams(ch + 0x3F, 1); //!< ��������� ���������� ������ ��������; GateIdx = 1, 2
				else
				{
					prms.StGate = AutoconMain->Calibration->GetStGate(dsNone, ch + AutoconStartCID, 0);         // ������ ����� ��� [���]
					prms.EdGate = AutoconMain->Calibration->GetEdGate(dsNone, ch + AutoconStartCID, 0);         // ����� ������ ��� [���]
				}

				for (int i = Header.MaxSysCoord; i > 0; i--)
				{
					sgl = GetFirstScanSignals(i, ch, &Result);
					if (Result)
					{
						for (int sidx = 0; sidx < sgl->Count; sidx++)
						{
							if ((sgl->Signals[sidx].Delay >= prms.StGate) &&
								(sgl->Signals[sidx].Delay <= prms.EdGate))
								for (int aidx = 0; aidx < 24; aidx++)
									if (sgl->Signals[sidx].Ampl[aidx] >= 32 ) {

									tmp.State[1] = true;
									break;
								}
							if (tmp.State[1]) break;
						}
					}
					if (tmp.State[1]) break;
				}
				AddAlarmSignals(ch, tmp);
			}
			if (tmp.State[1]) break;
		}
}

#endif

