﻿/**
 * @file CalibrationFormUnit.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef CalibrationFormUnitH
#define CalibrationFormUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.ToolWin.hpp>
#include <Vcl.ValEdit.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Touch.Keyboard.hpp>

/** \enum eCailbCol
    \brief Содержит типы данных для отоброжаемых в таблице колонок
    \note При добавлении/удалении - изменить GetCalibColName, TCalibrationForm::GetTableItem
 */
enum eCailbCol
{
    CALIB_COL_UNKNOWN = -1, //!< Не известно
    CALIB_COL_ID = 0,       //!< Идентификатор канала
    CALIB_COL_NUMBER,       //!< Номер по порядку
    CALIB_COL_CHNAME,       //!< Имя канала
    CALIB_COL_KPNUM,        //!< Номер колеса
    CALIB_COL_TVG,          //!< ВРЧ
    CALIB_COL_PRISM_DELAY,  //!< Время в призме
    CALIB_COL_SENS,         //!< Чувствительность
    CALIB_COL_GAIN,         //!< Усиление
    CALIB_COL_ST_GATE,      //!< Начало строба
    CALIB_COL_ED_GATE,      //!< Конец строба
    CALIB_COL_IS_CALIB,     //!< Настроен/Не настроен
    CALIB_COL_MARK,         //!< Метка
    CALIB_COL_DATETIME,     //!< Время настройки
    CALIB_COL_END           //!< Количество типов в enum
};

/** \struct sCalibData
    \brief Содержит данные для отоброжаемых в таблице колонок
 */
struct sCalibData
{
    CID ID;              //!< ID
	int TVG;             //!< ВРЧ [мкс]
	float PrismDelay;    //!< Время в призме, 2TP [10 * мкс]
	int Sens[2];         //!< Условная чувствительность, Ky (0 - ближняя зона, 1 - дальняя зона) [дБ]
	int Gain[2];         //!< Аттенуатор (0 - ближняя зона, 1 - дальняя зона) [дБ]
	int StGate[2];       //!< Начало строба АСД (0 - ближняя зона, 1 - дальняя зона, 2 - Настройка чуствительности канала, 3 - Настройка времени в призме) [мкс]
	int EdGate[2];       //!< Конец строба АСД (0 - ближняя зона, 1 - дальняя зона, 2 - Настройка чуствительности канала, 3 - Настройка времени в призме) [мкс]
	bool Calibrated[2];  //!< Канал настроен (0 - ближняя зона, 1 - дальняя зона)
	int Mark;            //!< Метка (центр) [мкс] ( < 0 Выключенна )
	double DataTime;     //!< Дата время выполнения настройки (TDataTime - Borland)
    int GateCount;       //!< Кол-во стробов

    UnicodeString ChannelName; //!< Имя канала
    int KpNumber;              //!< Номер колеса
    int itemNumber;            //!< Номер по порядку
};

//! Получить имя колонки по типу
UnicodeString GetCalibColName(eCailbCol val);
//! Получить кол-во вложенных элементов по типу (например, у типа CALIB_COL_ST_GATE может быть 2 вложенных элемента (2 строба))
int CalibColSubItemCount(eCailbCol val);

//! Делает из номера маску
#define MAKE_MASK(x) (1 << (x))
//! Запаковывает два числа меньше 65535 (WORD) в одно (DWORD)
#define CALIB_MAKE_PAIR(x,y) DWORD(WORD(x) | (WORD(y) << 16))
//! Распаковывает два числа меньше 65535 (WORD) из одного (DWORD)
#define CALIB_GET_PAIR(x,np) DWORD((x >> (16 * (np))) & 0x0000FFFF)

//! Видимые столбцы таблицы
#define CALIB_DEFAULT_TABLE_VISIBLE \
    /*MAKE_MASK(CALIB_COL_ID) |*/ MAKE_MASK(CALIB_COL_TVG) | MAKE_MASK(CALIB_COL_PRISM_DELAY) |\
	MAKE_MASK(CALIB_COL_SENS) | MAKE_MASK(CALIB_COL_GAIN) /*| MAKE_MASK(CALIB_COL_DATETIME)*/ |\
	MAKE_MASK(CALIB_COL_CHNAME) /*| MAKE_MASK(CALIB_COL_IS_CALIB)*/ |  MAKE_MASK(CALIB_COL_KPNUM) | MAKE_MASK(CALIB_COL_NUMBER) | \
    MAKE_MASK(CALIB_COL_ST_GATE) | MAKE_MASK(CALIB_COL_ED_GATE)
//! Редактируемые столюцы таблицы
#define CALIB_DEFAULT_TABLE_EDITABLE \
    MAKE_MASK(CALIB_COL_TVG) | MAKE_MASK(CALIB_COL_PRISM_DELAY) |\
    MAKE_MASK(CALIB_COL_SENS) | MAKE_MASK(CALIB_COL_GAIN) | MAKE_MASK(CALIB_COL_ST_GATE) |\
    MAKE_MASK(CALIB_COL_ED_GATE)
//---------------------------------------------------------------------------


//! Класс формы изменения параметров каналов
class TCalibrationForm : public TForm
{
__published:	// IDE-managed Components
    TGroupBox *GroupBox1;
    TStringGrid *CalibTable;
    TToolBar *m_MainToolBar;
    TToolButton *m_CurSetupBtn;
    TToolButton *m_CopyBtn;
    TToolButton *m_ResetToDefBtn;
    TToolButton *CloseBtn;
    TToolButton *ToolButton6;
    TToolButton *ToolButton7;
    TToolButton *ToolButton9;
    TImageList *ImageList1;
    TValueListEditor *ValueList;
    TSplitter *Splitter1;
    TTouchKeyboard *TouchKeyboard1;
    TPanel *Panel1;
    TBitBtn *PageDownBtn;
    TBitBtn *PageUpBtn;
    TPanel *Panel2;
    TLabel *m_CurrCalibName;
    void __fastcall CloseBtnClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall CalibTableSelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
    void __fastcall ValueListSetEditText(TObject *Sender, int ACol, int ARow, const UnicodeString Value);
    void __fastcall m_CurSetupBtnClick(TObject *Sender);
    void __fastcall m_CopyBtnClick(TObject *Sender);
    void __fastcall m_ResetToDefBtnClick(TObject *Sender);
    void __fastcall FormHide(TObject *Sender);
    void __fastcall CalibTableDrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State);
    void __fastcall PageDownBtnClick(TObject *Sender);
    void __fastcall PageUpBtnClick(TObject *Sender);



private:	// User declarations
    void UpdateTable(); //!< Выводит данные из массива каналов в таблицу
    void UpdateTableItem(int idx);  //!< Выводит данные из массива каналов в таблицу
    void UpdateCalibrationData(bool bFromDbToTable = true); //!< Считывает данные о каналах в массив #currCalibData
    void AdjustTableColumnsWidth(); //!< Подстраивает ширину колонок под размер данных
    bool CopyCalib(int fromIdx, int toIdx); //!< Копирует настройку
    void Store(); //!< Сохраняет текущую настройку

    //! Получить кол-во вложенных элементов по типу и индексу (например, у типа CALIB_COL_ST_GATE может быть 2 вложенных элемента (2 строба))
    int CalibColSubItemCount(unsigned int idx, eCailbCol val);

    int changesCount; //!< Кол-во изменений в таблице

    //! Получение элемента таблицы в виде текстового значения
    UnicodeString GetTableItem(unsigned int idx, eCailbCol col, int colSubIdx = -1);
    //! Установка элемента таблицы в виде текстового значения
    void SetTableItem(unsigned int idx, eCailbCol col, UnicodeString val, int colSubIdx = -1);
    unsigned int TableVisibleItemsMask; //!< Маска видимых колонок таблицы
    unsigned int TableEditableItemsMask; //!< Маска видимых в редакторе колонок таблицы
    int currEditedItem;  //!< Индекс редактируемого элемента

    std::vector<TColor> channelHgData; //!< Данные подсветки строк
    std::vector<sCalibData> currCalibData; //!< Данные каналов
public:		// User declarations
    __fastcall TCalibrationForm(TComponent* Owner);

    void HighlightChannel(CID chId, TColor col);
};
//---------------------------------------------------------------------------
extern PACKAGE TCalibrationForm *CalibrationForm;
//---------------------------------------------------------------------------

#endif
