//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Utils.h"
#include "BScanWriteProgressUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TBScanWriteProgressForm *BScanWriteProgressForm;
//---------------------------------------------------------------------------
__fastcall TBScanWriteProgressForm::TBScanWriteProgressForm(TComponent* Owner)
    : TForm(Owner)
{
    bCloseFlag = false;
}
//---------------------------------------------------------------------------
void TBScanWriteProgressForm::SetPercent(int val)
{
    ProgressBar1->Position = val;
    Label1->Caption = StringFormatU(L"���� ������ �-��������� (%d%%):", val);
}

bool TBScanWriteProgressForm::IsCloseBtnClicked()
{
    return bCloseFlag;
}
void __fastcall TBScanWriteProgressForm::CloseBtnClick(TObject *Sender)
{
    bCloseFlag = true;
}
//---------------------------------------------------------------------------
void __fastcall TBScanWriteProgressForm::FormShow(TObject *Sender)
{
    bCloseFlag = false;
    SetPercent(0);
}
//---------------------------------------------------------------------------
