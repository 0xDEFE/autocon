#ifndef UtilsH
#define UtilsH

 #include "AutoconLogging.h"
 #include "HTMLLogger.h"
 #include "BasicUtils.h"

bool EnableTRACE(bool bVal);
void TRACE(const char* Str, ...);

#endif
