//---------------------------------------------------------------------------

//#pragma hdrstop

#include "BScanMeasureUnit.h"
//---------------------------------------------------------------------------
//#pragma package(smart_init)

#include "dependencies/pugixml/pugixml.hpp"
#include <string.h>

void BScanMeasure::clear()
{
    name = "unknown";
    regions.clear();
    measureValue = 0;
}

bool BScanMeasure::save(const char* path,cChannelsTable* pChanTable)
{
    pugi::xml_document doc;
	pugi::xml_node rootNode=doc.append_child("BScanMeasure");
    rootNode.append_attribute("name").set_value(name.c_str());
    rootNode.append_attribute("perRegion").set_value(perRegionTag.c_str());

    for(unsigned int i = 0; i < regions.size(); i++)
    {
        pugi::xml_node regionNode=rootNode.append_child("region");
        regionNode.append_attribute("defect_name").set_value(regions[i].defect_name.c_str());
        regionNode.append_attribute("region_name").set_value(regions[i].region_name.c_str());

        for(unsigned int j = 0; j < regions[i].measureData.size(); j++)
        {
            const BScanMeasureData& cd = regions[i].measureData[j];

            pugi::xml_node compareNode=regionNode.append_child("measure");
            compareNode.append_attribute("signal").set_value(cd.signalTag.c_str());
            compareNode.append_attribute("perSignal").set_value(cd.perSignalTag.c_str());
            compareNode.append_attribute("perChannel").set_value(cd.perChannelTag.c_str());
            compareNode.append_attribute("val").set_value(cd.measureValue);


            for(std::set<CID>::const_iterator it = cd.channels.begin();
                it != cd.channels.end(); it++)
            {
                pugi::xml_node channelNode = compareNode.append_child("channel");
                //channelNode.append_attribute("id").set_value(*it);
                CTID name;
                pChanTable->CTIDbyCID(*it,&name);
                channelNode.append_attribute("name").set_value(name);
            }
        }

        for(unsigned int j = 0; j < regions[i].poly.points.size(); j++)
        {
            const PolyPt& point = regions[i].poly.points[j];

            pugi::xml_node pointNode = regionNode.append_child("point");
            pointNode.append_attribute("pos").set_value(point.x);
            pointNode.append_attribute("delay").set_value(point.y);
        }
    }

    return doc.save_file(path);
}

bool BScanMeasure::load(const char* path,cChannelsTable* pChanTable,cDeviceConfig* pConfig)
{
    clear();

    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(path);
    if (!result)
    {
        TRACE("PUGIXML: %s [file=%s]",result.description(),path);
        return false;
    }

    pugi::xml_node rootNode=doc.child("BScanMeasure");
    if(!rootNode)
        return false;

    pugi::xml_attribute nameAttr = rootNode.attribute("name");
    if(nameAttr)
        this->name = nameAttr.as_string("unknown");

    nameAttr = rootNode.attribute("perRegion");
    if(nameAttr) perRegionTag = nameAttr.as_string("unknown");

    for (pugi::xml_node regionNode = rootNode.child("region");
            regionNode; regionNode = regionNode.next_sibling("region"))
    {
        BScanRegion region;

        nameAttr = regionNode.attribute("defect_name");
        if(nameAttr) region.defect_name = nameAttr.as_string("unknown");

        nameAttr = regionNode.attribute("region_name");
        if(nameAttr) region.region_name = nameAttr.as_string("unknown");

        for (pugi::xml_node compareNode = regionNode.child("measure");
                compareNode; compareNode = compareNode.next_sibling("measure"))
        {
            BScanMeasureData compareData;

            nameAttr = compareNode.attribute("signal");
            if(nameAttr) compareData.signalTag = nameAttr.as_string("unknown");

            nameAttr = compareNode.attribute("perSignal");
            if(nameAttr) compareData.perSignalTag = nameAttr.as_string("unknown");

            nameAttr = compareNode.attribute("perChannel");
            if(nameAttr) compareData.perChannelTag = nameAttr.as_string("unknown");

            nameAttr = compareNode.attribute("val");
            if(nameAttr) compareData.measureValue = nameAttr.as_double(-1.0);

            for (pugi::xml_node channelNode = compareNode.child("channel");
                channelNode; channelNode = channelNode.next_sibling("channel"))
            {
                CID channelId = 0;
                nameAttr = channelNode.attribute("id");
                if(nameAttr) channelId = nameAttr.as_int();
                else
                {
                    nameAttr = channelNode.attribute("name");
                    if(nameAttr)
                    {
                        AnsiString name_id = nameAttr.as_string("none");
                        if(name_id.Length() <= 8)
                        {
                            CTID Tid;
                            strcpy(Tid, name_id.c_str());

                            pChanTable->CIDbyCNID(Tid, &channelId);
                        }
                    }
                }

                if(channelId != 0)
                    compareData.channels.insert(channelId);
            }

            for (pugi::xml_node channelsNode = compareNode.child("channels");
                channelsNode; channelsNode = channelsNode.next_sibling("channels"))
            {
                int kpId = -1;
                int lineId = -1;

                nameAttr = channelsNode.attribute("kp");
                if(nameAttr) kpId = nameAttr.as_int(-1);

                nameAttr = channelsNode.attribute("line");
                if(nameAttr) lineId = nameAttr.as_int(-1);

                if(kpId != -1)
                {
                    if(lineId != -1) {}
                    else
                    {
                        sScanChannelDescription desc;
                        for(unsigned int i = 0; i < pConfig->GetAllScanChannelsCount(); i++)
                        {
                            if(!pConfig->getSChannelbyIdx(i, &desc))
                                continue;
                            if(desc.BScanGroup == kpId)
                                compareData.channels.insert(desc.Id);

                        }
                    }
                }
            }

            if(!compareData.channels.empty())
                region.measureData.push_back(compareData);
        }

        for (pugi::xml_node pointNode = regionNode.child("point");
                    pointNode; pointNode = pointNode.next_sibling("point"))
        {
            PolyPt val;
            nameAttr = pointNode.attribute("pos");
            if(nameAttr) val.x = nameAttr.as_float();

            nameAttr = pointNode.attribute("delay");
            if(nameAttr) val.y = nameAttr.as_float();

            region.poly.points.push_back(val);
        }

        //if(!region.compareData.empty() && !region.poly.points.empty())
            this->regions.push_back(region);
    }

    return true;
}

void BScanMeasure::clearDrivers()
{
    std::map<std::string, BScanMeasureDriver*>::iterator it = drivers.begin();
    while(it != drivers.end())
    {
        if(it->second)
        {
            delete it->second;
            it->second = 0;
        }
        it++;
    }
    drivers.clear();
}

BScanMeasureDriver* BScanMeasure::newDriver(const std::string& tag)
{
    std::map<std::string, BScanMeasureDriver*>::iterator it = drivers.find(tag);
    if(it == drivers.end())
        return NULL;
    return it->second->clone();
}

BScanMeasureDriver* BScanMeasure::firstDriver()
{
    tempDrvIt = drivers.begin();
    return (tempDrvIt != drivers.end()) ? tempDrvIt->second : NULL;
}

BScanMeasureDriver* BScanMeasure::nextDriver()
{
    if(tempDrvIt == drivers.end())
        return NULL;
    tempDrvIt++;
    return (tempDrvIt != drivers.end()) ? tempDrvIt->second : NULL;
}
/*
void BScanMeasure::calculate(cJointTestingReport* pRep)
{
	double signalValue = 0;
	double perChannelValue = 0;
	double perSignalValue = 0;

    BScanMeasureDriver* pPerRegionDriver =
		newDriver(perRegionTag);

	for(unsigned int i = 0; i < regions.size(); i++) // �������
    {
        const PolyReg& poly = regions[i].poly;
        TRect bounds = poly.bounds();

		for(unsigned int j = 0; j < regions[i].measureData.size(); j++) // ���������
        {
            BScanMeasureDriver* pSignalDriver =
                newDriver(regions[i].measureData[j].signalTag);
			BScanMeasureDriver* pPerChannelDriver =
                newDriver(regions[i].measureData[j].perChannelTag);
            BScanMeasureDriver* pPerSignalDriver =
                newDriver(regions[i].measureData[j].perSignalTag);

			if(!pSignalDriver || !pPerChannelDriver || !pPerSignalDriver)
                continue;

			pPerChannelDriver->clear();
			const std::set<CID>& channels = regions[i].measureData[j].channels;

			// �������� �������
			for(std::set<CID>::const_iterator it = channels.begin(); it != channels.end(); it++)
			{
				bool bRes = false;
				pPerSignalDriver->clear();

				// �������� ���������
				for(int k = bounds.left; k <= bounds.right; k++)
                {
					PsScanSignalsOneCrdOneChannel sgl = pRep->GetFirstScanSignals(k, *it - 0x3F, &bRes);
                    if(bRes)
                    {
                        for(int p = 0; p < sgl->Count; p++)
                        {
                            if(!poly.pointInside(Vec2f(k, sgl->Signals[p].Delay)))
                                continue;
							// ������� �� �������� ������ ������ ������ �������

							*it - �����


							pSignalDriver->clear();
                            pSignalDriver->nextSig( sgl->Signals[p] );
                            pPerSignalDriver->nextVal(pSignalDriver->result());
                        }
                    }
				}
                // ������� �� �������
                pPerChannelDriver->nextVal(pPerSignalDriver->result());
			}
			regions[i].measureData[j].measureValue = pPerChannelDriver->result();
			if(pPerRegionDriver)
				// ������� �� ��������
				pPerRegionDriver->nextVal(regions[i].measureData[j].measureValue);

			delete pSignalDriver;
			delete pPerChannelDriver;
			delete pPerSignalDriver;
		}
	}

	if(pPerRegionDriver)
		this->measureValue =  pPerRegionDriver->result();
	delete pPerRegionDriver;
}
*/

void BScanMeasure::calculate(cJointTestingReport* pRep)
{
/*	double signalValue = 0;
	double perChannelValue = 0;
	double perSignalValue = 0;

	BScanMeasureDriver* pPerRegionDriver =
		newDriver(perRegionTag);
  */

//	TStringList* log = new TStringList();
//	int ChCount = 0;

	memset(ResList, 0, sizeof(ResList));

	for(unsigned int i = 0; i < regions.size(); i++) // �������
	{
		const PolyReg& poly = regions[i].poly;
		TRect bounds = poly.bounds();

		for(unsigned int j = 0; j < regions[i].measureData.size(); j++) // ���������
        {
		  /*  BScanMeasureDriver* pSignalDriver =
				newDriver(regions[i].measureData[j].signalTag);
			BScanMeasureDriver* pPerChannelDriver =
				newDriver(regions[i].measureData[j].perChannelTag);
			BScanMeasureDriver* pPerSignalDriver =
				newDriver(regions[i].measureData[j].perSignalTag);

			if(!pSignalDriver || !pPerChannelDriver || !pPerSignalDriver)
				continue;
			*/
			//pPerChannelDriver->clear();
			const std::set<CID>& channels = regions[i].measureData[j].channels;

			// �������� �������
			for(std::set<CID>::const_iterator it = channels.begin(); it != channels.end(); it++)
			{
				bool bRes = false;

				//int SignalCount = 0;
				//ChCount++;
				//pPerSignalDriver->clear();

				// �������� ���������
				for(int k = bounds.left; k <= bounds.right; k++)
				{
					PsScanSignalsOneCrdOneChannel sgl = pRep->GetFirstScanSignals(k, *it - 0x3F, &bRes);
					if(bRes)
					{
						for(int p = 0; p < sgl->Count; p++)
						{
							if(!poly.pointInside(Vec2f(k, sgl->Signals[p].Delay)))
								continue;
							// ������� �� �������� ������ ������ ������ �������

							ResList[*it - 0x3F].EchoCount[ResList[*it - 0x3F].PolyCount]++;

							/*
							pSignalDriver->clear();
							pSignalDriver->nextSig( sgl->Signals[p] );
							pPerSignalDriver->nextVal(pSignalDriver->result()); */
						}
					}
				}

				//log->Add(Format("����� - %d; ���������� - %d", ARRAYOFCONST((*it, SignalCount))));
				//SignalCount
				//*it - �����


				// ������� �� �������
			//    pPerChannelDriver->nextVal(pPerSignalDriver->result());
				ResList[*it - 0x3F].PolyCount++;
			}
/*
			regions[i].measureData[j].measureValue = pPerChannelDriver->result();
			if(pPerRegionDriver)
				// ������� �� ��������
				pPerRegionDriver->nextVal(regions[i].measureData[j].measureValue);

			delete pSignalDriver;
			delete pPerChannelDriver;
			delete pPerSignalDriver; */
		}
	}

 /*	if(pPerRegionDriver)
		this->measureValue =  pPerRegionDriver->result();
	delete pPerRegionDriver; */
//   log->SaveToFile("C:\\Denis\\a3\\BScanMeasureRes.log");
//   delete log;
}

double BScanMeasure::compare(const BScanMeasure& other)
{
    if(this->regions.size() != other.regions.size())
        return -1.f;

    double fTotalError = 0;
    for(unsigned int i = 0; i < regions.size(); i++)
    {
        if(regions[i].measureData.size() != other.regions[i].measureData.size())
			return -1.f;
        for(unsigned int j = 0; j < regions[i].measureData.size(); j++)
        {
            BScanMeasureData& currMeasure = regions[i].measureData[j];
            const BScanMeasureData& otherMeasure = other.regions[i].measureData[j];
            if((currMeasure.measureValue == otherMeasure.measureValue) || (std::max(currMeasure.measureValue, otherMeasure.measureValue) == 0))
            {
                currMeasure.errorValue = 0;
            }
            else
            {
                currMeasure.errorValue =
                    fabs(currMeasure.measureValue - otherMeasure.measureValue) / std::max(currMeasure.measureValue, otherMeasure.measureValue);
            }
            fTotalError += currMeasure.errorValue;
        }
    }

    return fTotalError;
}
