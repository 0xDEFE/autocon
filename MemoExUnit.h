//---------------------------------------------------------------------------

#ifndef MemoExUnitH
#define MemoExUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.ToolWin.hpp>

#include <vector>

struct sMemoMark
{
    TImage* pImage;
    int line;
    int level;
};
//---------------------------------------------------------------------------
class TMemoEx : public TMemo
{
__published:	// IDE-managed Components
private:	// User declarations
    void __fastcall WMPaint(TMessage &Msg);
    void __fastcall WMResize(TMessage &Msg);
    void __fastcall WMUpdateLines(TMessage &Msg);
    void __fastcall WMOnClick(TMessage &Msg);
    void __fastcall WMOnMMove(TMessage &Msg);

BEGIN_MESSAGE_MAP
    MESSAGE_HANDLER(WM_PAINT, TMessage, WMPaint)
    MESSAGE_HANDLER(WM_SIZE, TMessage, WMResize)
    MESSAGE_HANDLER(WM_SHOWWINDOW, TMessage, WMResize)
    MESSAGE_HANDLER(WM_CHAR, TMessage, WMUpdateLines)
    MESSAGE_HANDLER(WM_VSCROLL, TMessage, WMUpdateLines)
    MESSAGE_HANDLER(WM_MOUSEWHEEL, TMessage, WMUpdateLines)
    MESSAGE_HANDLER(WM_LBUTTONDOWN, TMessage, WMOnClick)
    MESSAGE_HANDLER(WM_MOUSEMOVE, TMessage, WMOnMMove)
END_MESSAGE_MAP(TMemo)

protected:
    int prevScrollPos;
    int prevLineCount;
    int topLineIndex;
    int bottomLineIndex;
    unsigned int TextMarginSize;
    unsigned int ImageMarginSize;
    TControlCanvas* canvas;
    std::vector<int> marks;
    TImageList* pImageList;
    HCURSOR arrowCursor;
public:		// User declarations
    void setMark(unsigned int line, int imageIndex);
    int getMark(unsigned int line);
    void setImageList( TImageList* pImageList);

    int getTopLine() {return topLineIndex;};
    int getBtmLine() {return bottomLineIndex;};

    TNotifyEvent OnLeftLineClicked;
    int ClickedLineNum;

    void UpdateVisibleLines(bool bForceUpdate = false);
    void UpdateMargins();
    void UpdateDraw();
    __fastcall TMemoEx(TComponent* Owner);
    virtual __fastcall ~TMemoEx();
};
#endif
