﻿/**
 * @file BScanLines.h
 * @author Denis Fedorenko
 */

#ifndef BScanLinesH
#define BScanLinesH

#include <Vcl.Graphics.hpp>
#include <System.Types.hpp>

//Added by KirillB
#include "Utils.h"

/** \enum eBScanLineDrawType
 *  Размер линии В-развертки
 */
enum eBScanLineDrawType
{
	LINEDRAW_STD = 0,       //!< Стандартная
	LINEDRAW_SMALL_LINE,    //!< Уменьшенная (для каналов контроля)
};

/** \enum eBScanHighlightType
 *  Тип индикации канала
 */
enum eBScanHighlightType
{
	HIGHLIGHT_NONE = 0, //!< Без подсветки (серый цвет)
	HIGHLIGHT_WARNING,  //!< Предупреждение (желтый цвет)
	HIGHLIGHT_ERROR,    //!< Ошибка (красный цвет)
	HIGHLIGHT_OK,       //!< ОК (зеленый цвет)
    HIGHLIGHT_DISABLE,  //!< Отключено
};


//! Описывает одну страницу В-развертки. Отображает каналы, органы управления и индикации каналов, а так же линии В-развертки.
class cBScanLines
{
	private:

	   int LineHeight;
	   //Added by KirillB
	   int SmallLineHeight;
       int BtnWidth;
       int LineWidth;

       TRect BtnBigTextRect[6][6];      //!< Прямоугольник, в котором рисуется наименование (Title) канала
       TRect BtnSmallTextRect[6][6];    //!< На текущий момент используется только в каналах контроля (маленькие кнопки)
       TRect BtnBtnRect[6][6];          //!< Прямоугольник, в котором рисуется кнопка включения/выключения канала и цвет канала
	   TRect BtnRect[6][6];             //!< Прямоугольник канала

	public:

        cBScanLines(void);
		~cBScanLines(void);
        //! Конструктор копирования
        cBScanLines(const cBScanLines& other)
        {
            this->operator=(other);
        }

        cBScanLines& operator=(const cBScanLines& other);

        TRect BoundsRect;       //!< Ограничивающий прямоугольник
        int BScanLineCount;     //!< Количество линий В-развертки
		int BtnColumnCount;     //!< Количество колонок кнопок
        bool bOnlyOneCol;
		bool BtnVisible[6][6];  //!< Флаг видимости кнопки вкл/выкл определенного канала
        TColor BtnColor[6][6];  //!< Цвет прямоукольника с названием канала (Title)
        TColor BtnChColor[6][6];//!< Цвет канала
        TColor BtnBtnColor;     //!< Цвет неактивной кнопки канала
        UnicodeString BtnBigText[6][6];  //!< Название канала (Title)
        int BtnId[6][6];            //!< Идентификатор канала
		bool BtnBtnState[6][6];     //!< Состояние кновки канала
		TRect LineRect[6];          //!< Прямоугольник, содержащий линию В-развертки
		//Added by KirillB
		eBScanHighlightType BtnHighlight[6][6]; //!< Тип индикации канала
		eBScanLineDrawType LineDrawType[6];     //!< Размер линии В-развертки
		bool BtnChannelBehind[6][6];     //!< Признак наезжающего/отъезжающего канала (для правильной подсветки)
        bool BtnChannelIsActive[6][6];   //!< Признак активности канала. Используется для подсветки.
		TBitmap* Buffer;                 //!< Буфер, в который ведется отрисовка
        bool Used;

		eBScanHighlightType LineHilightType; //!< Цвет подсветки для линии от изображения КП

        //! Перерасчитывает все размеры
        void Refresh(void);
        //! Отрисовывает в #Buffer
		void PaintToBuffer(void);
        //! Обработчик нажатия кнопки мыши
        /**
            \return ChannelId если попали в прямоуг. кнопки канала
            \return -ChannelId если не попали в прямоуг. кнопки канала, но попали в канал
            \return -1 если никуда не попали
        */
		int Click(int X, int Y);
        //! Изменяет уровень подсветки и цвет канала (для наезжающих и отъезжающих)
        /**
            \param level Уровень подсветки
            \param LineIdx Индекс линии В-развертки
            \param BtnIdx Индекс колонки канала
        */
		void ApplyBtnHighlightLevel(const char level, unsigned int LineIdx, unsigned int BtnIdx);
        //! Позволяет получить линию В-развертки и координату сканирования по экранным координатам
        bool GetSysCoord(int X, int Y, int MaxSysCoord, int* SysCoord, int* BScanLine);
        bool GetBtnState(int id);
        void SetBtnState(int id, bool bState);

        //! Преобразует экранные координаты в координаты В-развертки
        int SysCoordToScreen(int SysCoord, int MaxSysCoord);
};

#endif

