inherited AScanFrame: TAScanFrame
  Width = 1314
  Height = 721
  ExplicitWidth = 1314
  ExplicitHeight = 721
  object ChartPanel: TPanel
    Left = 0
    Top = 41
    Width = 1097
    Height = 680
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object ChInfoGridPanel: TGridPanel
      Left = 0
      Top = 0
      Width = 1097
      Height = 54
      Align = alTop
      BevelOuter = bvNone
      ColumnCollection = <
        item
          Value = 24.780300906858890000
        end
        item
          Value = 50.025855872152690000
        end
        item
          Value = 25.193843220988420000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = HPanel
          Row = 0
        end
        item
          Column = 1
          Control = GridPanel2
          Row = 0
        end
        item
          Column = 2
          Control = InfoPanel
          Row = 0
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      RowCollection = <
        item
          Value = 100.000000000000000000
        end
        item
          SizeStyle = ssAuto
        end>
      TabOrder = 0
      object HPanel: TPanel
        Left = 0
        Top = 0
        Width = 271
        Height = 54
        Align = alClient
        BevelOuter = bvNone
        BorderStyle = bsSingle
        Caption = 'H 182'
        Color = clBtnHighlight
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -40
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
      end
      object GridPanel2: TGridPanel
        Left = 271
        Top = 0
        Width = 548
        Height = 54
        Align = alClient
        BevelOuter = bvNone
        ColumnCollection = <
          item
            Value = 100.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = GridPanel3
            Row = 0
          end
          item
            Column = 0
            Control = ChannelTitlePanel
            Row = 1
          end>
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        RowCollection = <
          item
            Value = 50.000000000000000000
          end
          item
            Value = 50.000000000000000000
          end>
        TabOrder = 1
        object GridPanel3: TGridPanel
          Left = 0
          Top = 0
          Width = 548
          Height = 27
          Align = alClient
          BevelOuter = bvNone
          ColumnCollection = <
            item
              Value = 33.333335656557470000
            end
            item
              Value = 33.333335656557470000
            end
            item
              Value = 33.333328686885050000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = LPanel
              Row = 0
            end
            item
              Column = 1
              Control = RPanel
              Row = 0
            end
            item
              Column = 2
              Control = NPanel
              Row = 0
            end>
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          RowCollection = <
            item
              Value = 100.000000000000000000
            end>
          TabOrder = 0
          object LPanel: TPanel
            Left = 0
            Top = 0
            Width = 182
            Height = 27
            Align = alClient
            BevelOuter = bvNone
            BorderStyle = bsSingle
            Caption = 'L 50'
            Color = clBtnHighlight
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -26
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentBackground = False
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
          end
          object RPanel: TPanel
            Left = 182
            Top = 0
            Width = 182
            Height = 27
            Align = alClient
            BevelOuter = bvNone
            BorderStyle = bsSingle
            Caption = 'R 14'
            Color = clBtnHighlight
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -26
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentBackground = False
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
          end
          object NPanel: TPanel
            Left = 364
            Top = 0
            Width = 184
            Height = 27
            Align = alClient
            BevelOuter = bvNone
            BorderStyle = bsSingle
            Caption = 'N 14'
            Color = clBtnHighlight
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -26
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentBackground = False
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
          end
        end
        object ChannelTitlePanel: TPanel
          Left = 0
          Top = 27
          Width = 548
          Height = 27
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clWhite
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
      end
      object InfoPanel: TPanel
        Left = 819
        Top = 0
        Width = 278
        Height = 54
        Align = alClient
        BevelOuter = bvNone
        BorderStyle = bsSingle
        Caption = 'H 182'
        Color = clBtnHighlight
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -40
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
      end
    end
    object BScanDataPanel: TPanel
      Left = 0
      Top = 442
      Width = 1097
      Height = 190
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object ControlPanel: TPanel
        Left = 0
        Top = 0
        Width = 1097
        Height = 35
        Align = alTop
        TabOrder = 0
        Visible = False
        object Button34: TButton
          Left = 5
          Top = 5
          Width = 75
          Height = 25
          Caption = 'Set Axiss'
          TabOrder = 0
        end
        object CheckBox1: TCheckBox
          Left = 88
          Top = 9
          Width = 97
          Height = 17
          Caption = 'Points'
          TabOrder = 1
        end
        object CheckBox6: TCheckBox
          Left = 200
          Top = 10
          Width = 97
          Height = 17
          Caption = 'BScan'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
      end
      object BScanPanel: TPanel
        Left = 0
        Top = 130
        Width = 1097
        Height = 60
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        OnResize = BScanPanelResize
        object BScanPB: TPaintBox
          Left = 0
          Top = 1
          Width = 1097
          Height = 58
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitLeft = 88
          ExplicitTop = 5
        end
        object Shape2: TShape
          Left = 0
          Top = 0
          Width = 1097
          Height = 1
          Align = alTop
          ExplicitLeft = 1
          ExplicitTop = 1
          ExplicitWidth = 1067
        end
        object Shape3: TShape
          Left = 0
          Top = 59
          Width = 1097
          Height = 1
          Align = alBottom
          ExplicitLeft = 1
          ExplicitTop = -6
          ExplicitWidth = 1067
        end
      end
      object BScanDebugPanel: TPanel
        Left = 0
        Top = 35
        Width = 1097
        Height = 95
        Align = alTop
        TabOrder = 2
        Visible = False
        object AmplDebugPanel: TPanel
          Left = 844
          Top = 1
          Width = 252
          Height = 93
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          Visible = False
          object BScanChart: TChart
            Left = 0
            Top = 0
            Width = 252
            Height = 71
            LeftWall.Visible = False
            Legend.Visible = False
            Title.Text.Strings = (
              'TChart')
            Title.Visible = False
            BottomAxis.Automatic = False
            BottomAxis.AutomaticMaximum = False
            BottomAxis.AutomaticMinimum = False
            BottomAxis.LabelsFormat.TextAlignment = taCenter
            BottomAxis.Maximum = 24.000000000000000000
            DepthAxis.Automatic = False
            DepthAxis.AutomaticMaximum = False
            DepthAxis.AutomaticMinimum = False
            DepthAxis.LabelsFormat.TextAlignment = taCenter
            DepthAxis.Maximum = 0.829999999999996900
            DepthAxis.Minimum = -0.170000000000000700
            DepthTopAxis.Automatic = False
            DepthTopAxis.AutomaticMaximum = False
            DepthTopAxis.AutomaticMinimum = False
            DepthTopAxis.LabelsFormat.TextAlignment = taCenter
            DepthTopAxis.Maximum = 0.829999999999996900
            DepthTopAxis.Minimum = -0.170000000000000700
            LeftAxis.Automatic = False
            LeftAxis.AutomaticMaximum = False
            LeftAxis.AutomaticMinimum = False
            LeftAxis.Grid.Visible = False
            LeftAxis.LabelsFormat.TextAlignment = taCenter
            LeftAxis.Maximum = 255.000000000000000000
            LeftAxis.Minimum = -0.000000000000006217
            RightAxis.Automatic = False
            RightAxis.AutomaticMaximum = False
            RightAxis.AutomaticMinimum = False
            RightAxis.LabelsFormat.TextAlignment = taCenter
            RightAxis.Visible = False
            TopAxis.LabelsFormat.TextAlignment = taCenter
            TopAxis.Visible = False
            View3D = False
            Zoom.Pen.Mode = pmNotXor
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            DefaultCanvas = 'TGDIPlusCanvas'
            ColorPaletteIndex = 13
            object BScanSeries: TLineSeries
              Marks.Visible = False
              SeriesColor = -1
              Brush.BackColor = clDefault
              LinePen.Width = 2
              Pointer.InflateMargins = True
              Pointer.Style = psRectangle
              Pointer.Visible = False
              XValues.Name = 'X'
              XValues.Order = loAscending
              YValues.Name = 'Y'
              YValues.Order = loNone
            end
          end
          object Panel54: TPanel
            Left = 0
            Top = 71
            Width = 252
            Height = 22
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object Label1: TLabel
              Left = 0
              Top = 0
              Width = 87
              Height = 22
              Align = alLeft
              Alignment = taCenter
              AutoSize = False
              Caption = ' '#1053#1086#1084#1077#1088' '#1089#1080#1075#1085#1072#1083#1072':  '
              Layout = tlCenter
              ExplicitHeight = 13
            end
            object SelEchoIdx: TComboBox
              Left = 87
              Top = 0
              Width = 165
              Height = 21
              Align = alRight
              Style = csDropDownList
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              Items.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7')
            end
          end
        end
        object BScanChart2: TChart
          Left = 1
          Top = 1
          Width = 843
          Height = 93
          LeftWall.Visible = False
          Legend.Visible = False
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          BottomAxis.Automatic = False
          BottomAxis.AutomaticMaximum = False
          BottomAxis.AutomaticMinimum = False
          BottomAxis.LabelsFormat.TextAlignment = taCenter
          DepthAxis.Automatic = False
          DepthAxis.AutomaticMaximum = False
          DepthAxis.AutomaticMinimum = False
          DepthAxis.LabelsFormat.TextAlignment = taCenter
          DepthAxis.Maximum = -0.040000000000000000
          DepthAxis.Minimum = -1.040000000000000000
          DepthTopAxis.Automatic = False
          DepthTopAxis.AutomaticMaximum = False
          DepthTopAxis.AutomaticMinimum = False
          DepthTopAxis.LabelsFormat.TextAlignment = taCenter
          DepthTopAxis.Maximum = -0.040000000000000260
          DepthTopAxis.Minimum = -1.040000000000000000
          LeftAxis.Automatic = False
          LeftAxis.AutomaticMaximum = False
          LeftAxis.AutomaticMinimum = False
          LeftAxis.Grid.Visible = False
          LeftAxis.LabelsFormat.TextAlignment = taCenter
          LeftAxis.Maximum = 255.000000000000000000
          RightAxis.Automatic = False
          RightAxis.AutomaticMaximum = False
          RightAxis.AutomaticMinimum = False
          RightAxis.LabelsFormat.TextAlignment = taCenter
          RightAxis.Visible = False
          TopAxis.LabelsFormat.TextAlignment = taCenter
          TopAxis.Visible = False
          View3D = False
          Zoom.Pen.Mode = pmNotXor
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          Visible = False
          DefaultCanvas = 'TGDIPlusCanvas'
          ColorPaletteIndex = 13
          object BSSeries1: TLineSeries
            Marks.Visible = False
            SeriesColor = clRed
            Brush.BackColor = clDefault
            ClickableLine = False
            Dark3D = False
            LinePen.Color = clRed
            LinePen.Width = 3
            OutLine.Width = 2
            Pointer.Brush.Gradient.EndColor = 10708548
            Pointer.Gradient.EndColor = 10708548
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {0000000000}
          end
          object BSSeries2: TLineSeries
            Marks.Visible = False
            SeriesColor = clBlue
            Brush.BackColor = clDefault
            ClickableLine = False
            Dark3D = False
            LinePen.Color = clRed
            LinePen.Width = 3
            OutLine.Width = 2
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {0000000000}
          end
          object BSSeries3: TLineSeries
            Marks.Visible = False
            SeriesColor = clLime
            Brush.BackColor = clDefault
            ClickableLine = False
            Dark3D = False
            LinePen.Color = clRed
            LinePen.Width = 3
            OutLine.Width = 2
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {0000000000}
          end
          object BSSeries4: TLineSeries
            Marks.Visible = False
            SeriesColor = 33023
            Brush.BackColor = clDefault
            ClickableLine = False
            Dark3D = False
            LinePen.Color = clRed
            LinePen.Width = 3
            OutLine.Width = 2
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {0000000000}
          end
          object BSSeries5: TLineSeries
            Marks.Visible = False
            SeriesColor = 16777088
            Brush.BackColor = clDefault
            ClickableLine = False
            Dark3D = False
            LinePen.Color = clRed
            LinePen.Width = 3
            OutLine.Width = 2
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {0000000000}
          end
          object BSSeries6: TLineSeries
            Marks.Visible = False
            SeriesColor = 15680478
            Brush.BackColor = clDefault
            ClickableLine = False
            Dark3D = False
            LinePen.Color = clRed
            LinePen.Width = 3
            OutLine.Width = 2
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {0000000000}
          end
          object BSSeries7: TLineSeries
            Marks.Visible = False
            SeriesColor = clSilver
            Brush.BackColor = clDefault
            ClickableLine = False
            Dark3D = False
            LinePen.Color = clRed
            LinePen.Width = 3
            OutLine.Width = 2
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {0000000000}
          end
          object BSSeries8: TLineSeries
            Marks.Visible = False
            SeriesColor = clBlack
            Brush.BackColor = clDefault
            ClickableLine = False
            Dark3D = False
            LinePen.Color = clRed
            LinePen.Width = 3
            OutLine.Width = 2
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {0000000000}
          end
        end
      end
    end
    object HandChannelPanel: TPanel
      Left = 0
      Top = 632
      Width = 1097
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      Visible = False
      object SurfaceLabel: TLabel
        AlignWithMargins = True
        Left = 849
        Top = 3
        Width = 94
        Height = 42
        Align = alRight
        Caption = #1055#1086#1074'-'#1090#1100':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitHeight = 33
      end
      object Button_70echo: TSpeedButton
        Tag = 6
        AlignWithMargins = True
        Left = 746
        Top = 2
        Width = 120
        Height = 44
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        GroupIndex = 111
        Caption = '70'#176' '#1069#1061#1054
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = SetHandChannelBtn
        ExplicitLeft = 756
        ExplicitTop = 0
        ExplicitHeight = 97
      end
      object Button_65echo: TSpeedButton
        Tag = 5
        AlignWithMargins = True
        Left = 622
        Top = 2
        Width = 120
        Height = 44
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        GroupIndex = 111
        Caption = '65'#176' '#1069#1061#1054
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = SetHandChannelBtn
        ExplicitLeft = 636
        ExplicitTop = 0
        ExplicitHeight = 97
      end
      object Button_58echo: TSpeedButton
        Tag = 4
        AlignWithMargins = True
        Left = 498
        Top = 2
        Width = 120
        Height = 44
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        GroupIndex = 111
        Caption = '58'#176' '#1069#1061#1054
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = SetHandChannelBtn
        ExplicitLeft = 516
        ExplicitTop = 0
        ExplicitHeight = 97
      end
      object Button_50echo: TSpeedButton
        Tag = 3
        AlignWithMargins = True
        Left = 374
        Top = 2
        Width = 120
        Height = 44
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        GroupIndex = 111
        Caption = '50'#176' '#1069#1061#1054
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = SetHandChannelBtn
        ExplicitLeft = 396
        ExplicitTop = 0
        ExplicitHeight = 97
      end
      object Button_45echo: TSpeedButton
        Tag = 2
        AlignWithMargins = True
        Left = 250
        Top = 2
        Width = 120
        Height = 44
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        GroupIndex = 111
        Caption = '45'#176' '#1069#1061#1054
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = SetHandChannelBtn
        ExplicitLeft = 276
        ExplicitTop = 0
        ExplicitHeight = 97
      end
      object Button_0echo: TSpeedButton
        Tag = 1
        AlignWithMargins = True
        Left = 126
        Top = 2
        Width = 120
        Height = 44
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        GroupIndex = 111
        Caption = '0'#176' '#1069#1061#1054
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = SetHandChannelBtn
        ExplicitLeft = 156
        ExplicitTop = 0
        ExplicitHeight = 97
      end
      object Button_0ztm: TSpeedButton
        AlignWithMargins = True
        Left = 2
        Top = 2
        Width = 120
        Height = 44
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alLeft
        GroupIndex = 111
        Caption = '0'#176' '#1047#1058#1052
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = SetHandChannelBtn
        ExplicitLeft = 36
        ExplicitTop = 0
        ExplicitHeight = 97
      end
      object cbSurface: TComboFlat
        AlignWithMargins = True
        Left = 949
        Top = 3
        Width = 145
        Height = 39
        Align = alRight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -26
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemIndex = 0
        ParentFont = False
        TabOrder = 0
        TabStop = False
        Text = '0'
        Items.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
    end
    object AScanChart: TChart
      Left = 0
      Top = 54
      Width = 1097
      Height = 341
      AllowPanning = pmNone
      BackWall.Size = 1
      BottomWall.Size = 1
      Gradient.EndColor = clGray
      Gradient.MidColor = clWhite
      Gradient.StartColor = clSilver
      LeftWall.Dark3D = False
      LeftWall.Size = 1
      LeftWall.Visible = False
      Legend.Visible = False
      MarginBottom = -13
      MarginLeft = 0
      MarginRight = 0
      MarginTop = 0
      MarginUnits = muPixels
      RightWall.Size = 1
      Title.Text.Strings = (
        'TChart')
      Title.Visible = False
      BottomAxis.Axis.Visible = False
      BottomAxis.LabelsFormat.Font.Height = -29
      BottomAxis.LabelsFormat.Font.Name = 'Consolas'
      BottomAxis.LabelsFormat.TextAlignment = taCenter
      BottomAxis.PositionPercent = 15.000000000000000000
      BottomAxis.PositionUnits = muPixels
      ClipPoints = False
      DepthAxis.Automatic = False
      DepthAxis.AutomaticMaximum = False
      DepthAxis.AutomaticMinimum = False
      DepthAxis.LabelsFormat.TextAlignment = taCenter
      DepthAxis.Maximum = 0.530000000000000400
      DepthAxis.Minimum = -0.470000000000000000
      DepthTopAxis.Automatic = False
      DepthTopAxis.AutomaticMaximum = False
      DepthTopAxis.AutomaticMinimum = False
      DepthTopAxis.LabelsFormat.TextAlignment = taCenter
      DepthTopAxis.Maximum = 0.530000000000000400
      DepthTopAxis.Minimum = -0.470000000000000000
      LeftAxis.Automatic = False
      LeftAxis.AutomaticMaximum = False
      LeftAxis.AutomaticMinimum = False
      LeftAxis.LabelsFormat.TextAlignment = taCenter
      LeftAxis.LabelsSize = 1
      LeftAxis.Maximum = 255.000000000000000000
      LeftAxis.Minimum = -10.000000000000000000
      LeftAxis.Title.Visible = False
      LeftAxis.Visible = False
      Panning.MouseWheel = pmwNone
      RightAxis.Automatic = False
      RightAxis.AutomaticMaximum = False
      RightAxis.AutomaticMinimum = False
      RightAxis.LabelsFormat.TextAlignment = taCenter
      Shadow.Visible = False
      TopAxis.LabelsFormat.TextAlignment = taCenter
      View3D = False
      View3DOptions.Orthogonal = False
      View3DWalls = False
      Zoom.Allow = False
      Zoom.Pen.Mode = pmNotXor
      Align = alClient
      BevelOuter = bvNone
      BevelWidth = 9
      Color = clWhite
      TabOrder = 3
      OnClick = AScanChartClick
      DefaultCanvas = 'TGDIPlusCanvas'
      PrintMargins = (
        15
        11
        15
        11)
      ColorPaletteIndex = 13
      object AScanSeries_: TAreaSeries
        Active = False
        Marks.Visible = False
        AreaChartBrush.Color = clGray
        AreaChartBrush.BackColor = clDefault
        AreaLinesPen.Visible = False
        DrawArea = True
        LinePen.Width = 2
        Pointer.Brush.Gradient.EndColor = 10708548
        Pointer.Gradient.EndColor = 10708548
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = False
        UseYOrigin = True
        XValues.Name = 'X'
        XValues.Order = loAscending
        YOrigin = -1.000000000000000000
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
      object MaxPosSeries: TChartShape
        Marks.Visible = False
        SeriesColor = clYellow
        Brush.Color = clYellow
        Pen.Width = 3
        Style = chasTriangle
        X0 = 30.000000000000000000
        X1 = 33.000000000000000000
        Y0 = 3.000000000000000000
        Y1 = -8.000000000000000000
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          01020000000000000000003E4000000000000008400000000000804040000000
          00000020C0}
      end
      object TVGSeries_: TAreaSeries
        Active = False
        Marks.Visible = False
        AreaChartBrush.Color = clGray
        AreaChartBrush.BackColor = clDefault
        AreaLinesPen.Visible = False
        Dark3D = False
        DrawArea = True
        LinePen.Visible = False
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = False
        Transparency = 75
        UseYOrigin = True
        XValues.Name = 'X'
        XValues.Order = loAscending
        YOrigin = -1.000000000000000000
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
      object PeakSeries: TChartShape
        Marks.Visible = False
        SeriesColor = clOlive
        Alignment = taLeftJustify
        Brush.Color = clOlive
        Font.Height = -24
        Font.Name = 'Consolas'
        Pen.Width = 3
        Style = chasInvertPyramid
        Transparency = 50
        X0 = 24.700000000000000000
        X1 = 55.300000000000100000
        Y0 = 73.076917812500100000
        Y1 = 198.153835625000000000
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          01020000003333333333B33840BD4AB038EC4452407466666666A64B40B64AB0
          38ECC46840}
      end
      object BScanDebugSeries: TPointSeries
        Marks.Visible = False
        ClickableLine = False
        Pointer.Brush.Gradient.EndColor = 1330417
        Pointer.Gradient.EndColor = 1330417
        Pointer.InflateMargins = True
        Pointer.Pen.Width = 2
        Pointer.Style = psRectangle
        Pointer.Visible = False
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          01190000009A999999999933406CA9CD49A9953B40CDCCCCCCCC4C35403B9336
          5527762F4000000000000037403B93365527762F403333333333B338404B17AC
          825A293E406666666666663A403A4E1B1F33734B409999999999193C40328C60
          8899194840CCCCCCCCCCCC3D404B17AC825A293E40FFFFFFFFFF7F3F40864886
          1C3BB1154099999999999940407BA30C0BF4422A4033333333337341405532EF
          27B9911140CDCCCCCCCC4C42402208EB5A6666354067666666662643407C725E
          ADD04B4640010000000000444063E712B30F3C44409B99999999D94440F8B30E
          FD7DA040403533333333B34540321697F4811F2440CFCCCCCCCC8C464021F47D
          82955A2D406966666666664740CEC18E5ADCC81B4003000000004048401977DE
          21F00322409D9999999919494009693288D44A2B403733333333F34940E774B4
          053FF01D40D1CCCCCCCCCC4A4000000000000020406B66666666A64B40FA1E3F
          65466EF43F0500000000804C40DF597160E0873A409F99999999594D409DBF64
          3E2BB543403933333333334E40C6CE25661F783840}
      end
      object BScanGateMainSeries: TChartShape
        Active = False
        Marks.Visible = False
        SeriesColor = clAqua
        Brush.Color = clAqua
        Pen.Visible = False
        Style = chasRectangle
        X0 = 20.000000000000000000
        X1 = 60.000000000000000000
        Y0 = 100.000000000000000000
        Y1 = 98.000000000000000000
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          0102000000000000000000344000000000000059400000000000004E40000000
          0000805840}
      end
      object BScanGateLeftSeries: TChartShape
        Active = False
        Marks.Visible = False
        SeriesColor = clAqua
        Brush.Color = clAqua
        Pen.Visible = False
        Style = chasRectangle
        X0 = 19.600000000000000000
        X1 = 20.000000000000000000
        Y0 = 110.000000000000000000
        Y1 = 90.000000000000000000
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          01020000009A999999999933400000000000805B400000000000003440000000
          0000805640}
      end
      object BScanGateRightSeries: TChartShape
        Active = False
        Marks.Visible = False
        SeriesColor = clAqua
        Brush.Color = clAqua
        Pen.Visible = False
        Style = chasRectangle
        X0 = 60.000000000000000000
        X1 = 60.400000000000000000
        Y0 = 110.000000000000000000
        Y1 = 90.000000000000000000
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          01020000000000000000004E400000000000805B403333333333334E40000000
          0000805640}
      end
      object GateSeriesMain: TChartShape
        Marks.Visible = False
        SeriesColor = clBlue
        Brush.Color = clBlue
        Pen.Visible = False
        Style = chasRectangle
        X0 = 20.000000000000000000
        X1 = 60.000000000000000000
        Y0 = 100.000000000000000000
        Y1 = 98.000000000000000000
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          0102000000000000000000344000000000000059400000000000004E40000000
          0000805840}
      end
      object GateSeriesLeft: TChartShape
        Marks.Visible = False
        SeriesColor = clBlue
        Brush.Color = clBlue
        Pen.Visible = False
        Style = chasRectangle
        X0 = 19.600000000000000000
        X1 = 20.000000000000000000
        Y0 = 110.000000000000000000
        Y1 = 90.000000000000000000
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          01020000009A999999999933400000000000805B400000000000003440000000
          0000805640}
      end
      object GateSeriesRight: TChartShape
        Marks.Visible = False
        SeriesColor = clBlue
        Brush.Color = clBlue
        Pen.Visible = False
        Style = chasRectangle
        X0 = 60.000000000000000000
        X1 = 60.400000000000000000
        Y0 = 110.000000000000000000
        Y1 = 90.000000000000000000
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          01020000000000000000004E400000000000805B403333333333334E40000000
          0000805640}
      end
      object Series1: TChartShape
        Active = False
        Marks.Visible = False
        SeriesColor = clWhite
        Font.Color = clWhite
        Font.Height = -16
        Font.Style = [fsBold]
        Style = chasVertLine
        VertAlign = vaTop
        X0 = 24.700000000000000000
        X1 = 55.300000000000100000
        Y0 = 7.786953920000010000
        Y1 = 141.573907840000000000
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          01020000003333333333B33840A96F973FD7251F407466666666A64B40FA76F9
          735DB26140}
      end
      object Series2: TChartShape
        Active = False
        Marks.Visible = False
        SeriesColor = clWhite
        Style = chasVertLine
        X0 = 24.700000000000000000
        X1 = 55.300000000000100000
        Y0 = 46.107259126400000000
        Y1 = 227.214518252800000000
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          01020000003333333333B33840F30AC4AABA0D47407466666666A64B40790562
          55DD666C40}
      end
      object AScanSeries: TLineSeries
        ColorEachLine = False
        Marks.Visible = False
        SeriesColor = -1
        Brush.BackColor = clDefault
        Dark3D = False
        LinePen.Width = 5
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = False
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
      object TVGSeries: TLineSeries
        ColorEachLine = False
        Marks.Visible = False
        SeriesColor = clYellow
        Brush.BackColor = clDefault
        Dark3D = False
        LinePen.Width = 3
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = False
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
      end
      object MarkSeries: TChartShape
        Marks.Visible = False
        SeriesColor = 217
        Title = 'MarkSeries'
        Brush.Color = 4227327
        Brush.Style = bsBDiagonal
        Brush.BackColor = clWhite
        Brush.Gradient.Colors = <
          item
            Color = clWhite
          end
          item
            Color = 6579455
            Offset = 0.383233532934131800
          end
          item
            Color = 2105599
            Offset = 1.000000000000000000
          end>
        Brush.Gradient.EndColor = 2105599
        Brush.Gradient.MidColor = 6579455
        Brush.Gradient.Visible = True
        Gradient.Colors = <
          item
            Color = clWhite
          end
          item
            Color = 6579455
            Offset = 0.383233532934131800
          end
          item
            Color = 2105599
            Offset = 1.000000000000000000
          end>
        Gradient.EndColor = 2105599
        Gradient.MidColor = 6579455
        Gradient.Visible = True
        Style = chasRectangle
        X0 = 24.700000000000000000
        X1 = 55.300000000000100000
        Y0 = 102.000000000000000000
        Y1 = 96.000000000000000000
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          01020000003333333333B3384000000000008059407466666666A64B40000000
          0000005840}
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 395
      Width = 1097
      Height = 47
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      object m_MarkEnableBtn: TBitBtn
        AlignWithMargins = True
        Left = 4
        Top = 4
        Width = 182
        Height = 39
        Align = alLeft
        Caption = #1042#1082#1083'. '#1084#1077#1090#1082#1091
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = m_MarkEnableBtnClick
      end
      object m_MarkOffsetGrid: TGridPanel
        Left = 189
        Top = 1
        Width = 380
        Height = 45
        Align = alLeft
        BevelOuter = bvNone
        ColumnCollection = <
          item
            SizeStyle = ssAbsolute
            Value = 220.000000000000000000
          end
          item
            Value = 50.000000568588060000
          end
          item
            Value = 49.999999431411940000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = m_MarkOffsetPanel
            Row = 0
          end
          item
            Column = 1
            Control = m_MarkOffsetLeftBtn
            Row = 0
          end
          item
            Column = 2
            Control = m_MarkOffsetRightBtn
            Row = 0
          end>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 1
        object m_MarkOffsetPanel: TPanel
          Left = 0
          Top = 0
          Width = 220
          Height = 45
          Margins.Left = 30
          Margins.Right = 5
          Align = alClient
          BevelOuter = bvNone
          Caption = #1057#1084#1077#1097#1077#1085#1080#1077':'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -22
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object m_MarkOffsetLeftBtn: TSpeedButton
          AlignWithMargins = True
          Left = 223
          Top = 3
          Width = 74
          Height = 39
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Glyph.Data = {
            361B0000424D361B000000000000360000002800000030000000300000000100
            180000000000001B0000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFBCBCBC4343430B0B0B0B0B0B424242BABABAFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE828282010101000000
            0000000000000000000101019F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFF7F7F75C5C5C000000000000000000000000000000000000000000131313FB
            FBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E9393939000000000000000000000000
            000000000000000000000000000000E2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2D2D22020
            20000000000000000000000000000000000000000000000000000000161616FB
            FBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFB6B6B60D0D0D000000000000000000000000000000000000
            000000000000000000030303A9A9A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E0303030000000000
            000000000000000000000000000000000000000000000C0C0CB2B2B2FFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA
            FAFA676767000000000000000000000000000000000000000000000000000000
            0000001D1D1DCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFEEEEEE4343430000000000000000000000000000
            00000000000000000000000000000000363636E7E7E7FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDADADA27272700
            0000000000000000000000000000000000000000000000000000000000575757
            F6F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFBFBFBF1212120000000000000000000000000000000000000000
            000000000000000101017D7D7DFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9B9B9B05050500000000000000
            0000000000000000000000000000000000000000080808A6A6A6FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFC
            7474740000000000000000000000000000000000000000000000000000000000
            0000000050505088888888888888888888888888888888888888888888888888
            8888888888888888888888888888888888888888888888888888888888888888
            8888888888888888888888888888888888888888888888888888888888889696
            96D2D2D2FFFFFFFFFFFFF8F8F84E4E4E00000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000000000000000000000000000000000005A5A5AF8F8F86E6E6E000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000006F6F6F11111100000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000111111111111000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000001111116E6E6E00000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000707070F8F8F84E4E4E
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000005A5A5AF8F8F8FFFFFFFCFCFC74747400000000000000000000000000
            0000000000000000000000000000000000000000505050888888888888888888
            8888888888888888888888888888888888888888888888888888888888888888
            8888888888888888888888888888888888888888888888888888888888888888
            8888888888888888888888888888969696D2D2D2FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF9B9B9B0505050000000000000000000000000000000000000000000000
            00000000080808A6A6A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF12121200000000
            00000000000000000000000000000000000000000000000101017D7D7DFEFEFE
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFDADADA2727270000000000000000000000000000000000
            00000000000000000000000000575757F6F6F6FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEE43
            4343000000000000000000000000000000000000000000000000000000000000
            363636E7E7E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFA6767670000000000000000000000
            000000000000000000000000000000000000001D1D1DCFCFCFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF8E8E8E030303000000000000000000000000000000000000000000
            0000000000000C0C0CB2B2B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB6B6B60D0D0D0000
            00000000000000000000000000000000000000000000000000030303A9A9A9FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFD2D2D2202020000000000000000000000000000000
            000000000000000000000000161616FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9
            E9393939000000000000000000000000000000000000000000000000000000E2
            E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F75C5C5C000000000000000000
            000000000000000000000000131313FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFEFEFE8282820101010000000000000000000000000101019F9F9FFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCBCBC434343
            0B0B0B0B0B0B424242BABABAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          ParentFont = False
          OnMouseDown = OnMarkBtnMouseDown
          OnMouseLeave = OnMarkBtnMouseUp
          OnMouseUp = OnMarkBtnMouseUp2
          ExplicitLeft = 136
          ExplicitTop = 0
          ExplicitWidth = 164
          ExplicitHeight = 51
        end
        object m_MarkOffsetRightBtn: TSpeedButton
          Tag = 1
          AlignWithMargins = True
          Left = 303
          Top = 3
          Width = 74
          Height = 39
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Glyph.Data = {
            361B0000424D361B000000000000360000002800000030000000300000000100
            180000000000001B0000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFB9B9B94242420B0B0B0B0B0B434343BCBCBCFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9E9E9E0101010000000000
            00000000000000010101828282FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFBFBFB1212120000000000000000000000000000000000000000005B5B5B
            F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3E3E30000000000000000000000
            00000000000000000000000000000000393939E9E9E9FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFBFBFB161616000000000000000000000000000000000000000000000000
            000000202020D2D2D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9A9A90303030000000000
            000000000000000000000000000000000000000000000D0D0DB5B5B5FFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFB2B2B20C0C0C000000000000000000000000000000000000
            0000000000000000000303038E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF1D1D
            1D00000000000000000000000000000000000000000000000000000000000067
            6767FAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E7363636000000000000000000000000
            000000000000000000000000000000000000434343EEEEEEFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFF6F6F657575700000000000000000000000000000000000000000000000000
            0000000000272727DADADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFE7D7D7D010101000000
            000000000000000000000000000000000000000000000000121212C0C0C0FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFA6A6A608080800000000000000000000000000000000
            00000000000000000000000505059B9B9BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            D2D2D29494948888888888888888888888888888888888888888888888888888
            8888888888888888888888888888888888888888888888888888888888888888
            8888888888888888888888888888888888888888888888888888888888505050
            0000000000000000000000000000000000000000000000000000000000000000
            00747474FCFCFCFFFFFFF8F8F859595900000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000000000000000000000000000000000004E4E4EF8F8F8707070000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000006E6E6E11111100000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000111111111111000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000011111170707000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000000000000000000000000000000000000000006E6E6EF8F8F8595959
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000004E4E4EF8F8F8FFFFFFFFFFFFD2D2D294949488888888888888888888
            8888888888888888888888888888888888888888888888888888888888888888
            8888888888888888888888888888888888888888888888888888888888888888
            8888888888888888888888888850505000000000000000000000000000000000
            0000000000000000000000000000000000747474FCFCFCFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6A6A6080808
            0000000000000000000000000000000000000000000000000000000505059B9B
            9BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFEFEFE7D7D7D01010100000000000000000000000000000000000000
            0000000000000000121212C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F6575757000000000000000000
            000000000000000000000000000000000000000000272727DADADAFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7
            E736363600000000000000000000000000000000000000000000000000000000
            0000434343EEEEEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFCFCFCF1D1D1D000000000000000000000000000000
            000000000000000000000000000000676767FAFAFAFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB2B2B20C0C0C0000
            000000000000000000000000000000000000000000000000000303038E8E8EFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFA9A9A9030303000000000000000000000000000000000000000000
            0000000000000D0D0DB5B5B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFB1616160000000000000000
            00000000000000000000000000000000000000202020D2D2D2FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFE3E3E3000000000000000000000000000000000000000000000000000000
            393939E9E9E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFB1212120000000000000000
            000000000000000000000000005B5B5BF7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF9E9E9E010101000000000000000000000000010101828282FEFEFE
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B9B94242420B0B
            0B0B0B0B434343BCBCBCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          ParentFont = False
          OnMouseDown = OnMarkBtnMouseDown
          OnMouseLeave = OnMarkBtnMouseUp
          OnMouseUp = OnMarkBtnMouseUp2
          ExplicitLeft = 294
          ExplicitTop = 5
          ExplicitWidth = 80
          ExplicitHeight = 51
        end
      end
      object m_MarkWidthPanel: TPanel
        Left = 569
        Top = 1
        Width = 527
        Height = 45
        Align = alClient
        BevelOuter = bvNone
        Caption = #1064#1080#1088#1080#1085#1072' - 0 '#1084#1082#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -22
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
    end
  end
  object RightPanel: TPanel
    Left = 1097
    Top = 41
    Width = 217
    Height = 680
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object NumPadPanel: TPanel
      Left = 0
      Top = 480
      Width = 217
      Height = 200
      Align = alBottom
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Visible = False
      object GridPanel4: TGridPanel
        Left = 168
        Top = 0
        Width = 49
        Height = 200
        Align = alRight
        BevelOuter = bvNone
        ColumnCollection = <
          item
            Value = 100.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = SpeedButton29
            Row = 0
          end
          item
            Column = 0
            Control = SpeedButton21
            Row = 1
          end
          item
            Column = 0
            Control = SpeedButton33
            Row = 2
          end>
        RowCollection = <
          item
            Value = 33.331138729279750000
          end
          item
            Value = 33.331138729279750000
          end
          item
            Value = 33.337722541440500000
          end>
        TabOrder = 0
        object SpeedButton29: TSpeedButton
          Tag = 14
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 43
          Height = 60
          Align = alClient
          Caption = 'CLR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 141
          ExplicitTop = 45
          ExplicitWidth = 49
          ExplicitHeight = 29
        end
        object SpeedButton21: TSpeedButton
          Tag = 13
          AlignWithMargins = True
          Left = 3
          Top = 69
          Width = 43
          Height = 60
          Align = alClient
          Caption = 'DEL'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 103
          ExplicitTop = 92
          ExplicitWidth = 49
          ExplicitHeight = 29
        end
        object SpeedButton33: TSpeedButton
          Tag = 12
          AlignWithMargins = True
          Left = 3
          Top = 135
          Width = 43
          Height = 62
          Align = alClient
          Caption = 'OK'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 103
          ExplicitTop = 133
          ExplicitWidth = 49
          ExplicitHeight = 29
        end
      end
      object GridPanel1: TGridPanel
        Left = 0
        Top = 0
        Width = 168
        Height = 200
        Align = alClient
        BevelOuter = bvNone
        ColumnCollection = <
          item
            Value = 33.333790881290850000
          end
          item
            Value = 33.333790881290850000
          end
          item
            Value = 33.332418237418300000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = SpeedButton10
            Row = 0
          end
          item
            Column = 1
            Control = SpeedButton11
            Row = 0
          end
          item
            Column = 2
            Control = SpeedButton20
            Row = 0
          end
          item
            Column = 0
            Control = SpeedButton26
            Row = 1
          end
          item
            Column = 1
            Control = SpeedButton27
            Row = 1
          end
          item
            Column = 2
            Control = SpeedButton28
            Row = 1
          end
          item
            Column = 0
            Control = SpeedButton30
            Row = 2
          end
          item
            Column = 1
            Control = SpeedButton31
            Row = 2
          end
          item
            Column = 2
            Control = SpeedButton32
            Row = 2
          end
          item
            Column = 0
            Control = SpeedButton34
            Row = 3
          end
          item
            Column = 1
            Control = SpeedButton35
            Row = 3
          end
          item
            Column = 2
            Control = SpeedButton36
            Row = 3
          end>
        RowCollection = <
          item
            Value = 25.000763069056930000
          end
          item
            Value = 25.000763069056930000
          end
          item
            Value = 24.998902371107790000
          end
          item
            Value = 24.999571490778360000
          end
          item
            SizeStyle = ssAuto
          end>
        TabOrder = 1
        object SpeedButton10: TSpeedButton
          Tag = 7
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 50
          Height = 44
          Align = alClient
          Caption = '7'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 2
          ExplicitWidth = 49
          ExplicitHeight = 46
        end
        object SpeedButton11: TSpeedButton
          Tag = 8
          AlignWithMargins = True
          Left = 59
          Top = 3
          Width = 50
          Height = 44
          Align = alClient
          Caption = '8'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 31
          ExplicitTop = 92
          ExplicitWidth = 49
          ExplicitHeight = 29
        end
        object SpeedButton20: TSpeedButton
          Tag = 9
          AlignWithMargins = True
          Left = 115
          Top = 3
          Width = 50
          Height = 44
          Align = alClient
          Caption = '9'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 40
          ExplicitTop = 32
          ExplicitWidth = 49
          ExplicitHeight = 29
        end
        object SpeedButton26: TSpeedButton
          Tag = 4
          AlignWithMargins = True
          Left = 3
          Top = 53
          Width = 50
          Height = 44
          Align = alClient
          Caption = '4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 168
          ExplicitTop = 171
          ExplicitWidth = 49
          ExplicitHeight = 29
        end
        object SpeedButton27: TSpeedButton
          Tag = 5
          AlignWithMargins = True
          Left = 59
          Top = 53
          Width = 50
          Height = 44
          Align = alClient
          Caption = '5'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 40
          ExplicitTop = 166
          ExplicitWidth = 49
          ExplicitHeight = 29
        end
        object SpeedButton28: TSpeedButton
          Tag = 6
          AlignWithMargins = True
          Left = 115
          Top = 53
          Width = 50
          Height = 44
          Align = alClient
          Caption = '6'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 77
          ExplicitTop = 12
          ExplicitWidth = 49
          ExplicitHeight = 29
        end
        object SpeedButton30: TSpeedButton
          Tag = 1
          AlignWithMargins = True
          Left = 3
          Top = 103
          Width = 50
          Height = 43
          Align = alClient
          Caption = '1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 102
          ExplicitTop = 128
          ExplicitWidth = 49
          ExplicitHeight = 29
        end
        object SpeedButton31: TSpeedButton
          Tag = 2
          AlignWithMargins = True
          Left = 59
          Top = 103
          Width = 50
          Height = 43
          Align = alClient
          Caption = '2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 37
          ExplicitTop = 45
          ExplicitWidth = 49
          ExplicitHeight = 29
        end
        object SpeedButton32: TSpeedButton
          Tag = 3
          AlignWithMargins = True
          Left = 115
          Top = 103
          Width = 50
          Height = 43
          Align = alClient
          Caption = '3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 31
          ExplicitTop = 115
          ExplicitWidth = 49
          ExplicitHeight = 29
        end
        object SpeedButton34: TSpeedButton
          AlignWithMargins = True
          Left = 3
          Top = 152
          Width = 50
          Height = 43
          Align = alClient
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 109
          ExplicitTop = 26
          ExplicitWidth = 49
          ExplicitHeight = 46
        end
        object SpeedButton35: TSpeedButton
          Tag = 10
          AlignWithMargins = True
          Left = 59
          Top = 152
          Width = 50
          Height = 43
          Align = alClient
          Caption = '-'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 86
          ExplicitTop = 95
          ExplicitWidth = 49
          ExplicitHeight = 46
        end
        object SpeedButton36: TSpeedButton
          Tag = 11
          AlignWithMargins = True
          Left = 115
          Top = 152
          Width = 50
          Height = 43
          Align = alClient
          Caption = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          OnClick = NumPadClick
          ExplicitLeft = 29
          ExplicitTop = 8
          ExplicitWidth = 49
          ExplicitHeight = 46
        end
      end
    end
    object ScrollBox1: TScrollBox
      Left = 0
      Top = 65
      Width = 217
      Height = 350
      HorzScrollBar.Visible = False
      VertScrollBar.ParentColor = False
      VertScrollBar.Position = 1983
      VertScrollBar.Smooth = True
      VertScrollBar.Style = ssFlat
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      TabOrder = 1
      object Panel7: TPanel
        Left = 0
        Top = -652
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 0
        object PrismDelayPanel: TPanel
          Tag = 9
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object SpinButton05: TSpinButton
          Tag = 9
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 1
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
        object Panel8: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' 2'#1058#1087' ['#1084#1082#1089']'
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = -773
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 10
        object TVGPanel: TPanel
          Tag = 8
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object SpinButton04: TSpinButton
          Tag = 8
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 1
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
        object Panel10: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1042#1056#1063' ['#1084#1082#1089']'
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
      end
      object AScanKuPanel: TPanel
        Left = 0
        Top = -894
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 1
        object SensPanel: TPanel
          Tag = 7
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object SpinButton03: TSpinButton
          Tag = 7
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 1
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
        object Panel12: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1050#1091' ['#1076#1041']'
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
      end
      object Panel13: TPanel
        Left = 0
        Top = -168
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 2
        object EdGatePanel: TPanel
          Tag = 12
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object SpinButton02: TSpinButton
          Tag = 12
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 1
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
        object Panel15: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1050#1086#1085#1077#1094' c'#1090#1088#1086#1073#1072' ['#1084#1084']'
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
      end
      object Panel16: TPanel
        Left = 0
        Top = -289
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 3
        object StGatePanel: TPanel
          Tag = 11
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object SpinButton01: TSpinButton
          Tag = 11
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 1
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
        object Panel18: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1053#1072#1095#1072#1083#1086' c'#1090#1088#1086#1073#1072' ['#1084#1084']'
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
      end
      object LabelPanel: TPanel
        Left = 0
        Top = -1620
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 4
        Visible = False
        object GeneratorPanel: TPanel
          Tag = 2
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object SpinButton06: TSpinButton
          Tag = 2
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 1
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
        object Panel21: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1043#1077#1085#1077#1088#1072#1090#1086#1088' '
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
      end
      object BScanRecPanel: TPanel
        Left = 0
        Top = -1499
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 5
        Visible = False
        object ReceiverPanel: TPanel
          Tag = 3
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel14: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1055#1088#1080#1077#1084#1085#1080#1082' '
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
        object SpinButton3: TSpinButton
          Tag = 3
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 2
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
      end
      object EndPanel: TPanel
        Left = 0
        Top = 1405
        Width = 196
        Height = 9
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 6
      end
      object Panel24: TPanel
        Left = 0
        Top = 921
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 7
        object SpeedButton1: TSpeedButton
          Tag = 19
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
            A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
            B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnClick = sbSideLineClick
          ExplicitTop = 37
        end
        object RullerModePanel: TPanel
          Tag = 19
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = #1084#1082#1089
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel26: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1064#1082#1072#1083#1072' '#1075#1083#1091#1073#1080#1085#1099
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
      end
      object TuningPanel: TPanel
        Left = 0
        Top = 74
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 8
        object CallibrationButton: TSpeedButton
          Tag = 14
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
            A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
            B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnClick = sbSideLineClick
          ExplicitLeft = 142
          ExplicitTop = 34
          ExplicitHeight = 81
        end
        object ValuePanel00: TPanel
          Tag = 14
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -24
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel29: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1053#1072#1089#1090#1088#1086#1080#1090#1100
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel17: TPanel
        Left = 0
        Top = 1414
        Width = 196
        Height = 85
        Align = alTop
        ParentBackground = False
        TabOrder = 9
        Visible = False
        object Button15: TButton
          Tag = 7
          Left = 33
          Top = 11
          Width = 134
          Height = 62
          Caption = #1042#1099#1093#1086#1076
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Consolas'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
      end
      object Panel19: TPanel
        Left = 0
        Top = -1741
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 11
        Visible = False
        object sbSideLine: TSpeedButton
          Tag = 1
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
            A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
            B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnClick = sbSideLineClick
          ExplicitLeft = 142
          ExplicitTop = 33
          ExplicitHeight = 81
        end
        object ValuePanel08: TPanel
          Tag = 1
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -23
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel20: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1057#1090#1086#1088#1086#1085#1072' / '#1051#1080#1085#1080#1103
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel25: TPanel
        Left = 0
        Top = 558
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 12
        Visible = False
        object BScanGateLevelPanel: TPanel
          Tag = 17
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel28: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' BScan '#1087#1086#1088#1086#1075' ['#1076#1041']'
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
        object SpinButton5: TSpinButton
          Tag = 17
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 2
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
      end
      object Panel32: TPanel
        Left = 0
        Top = 800
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 13
        Visible = False
        object SpeedButton8: TSpeedButton
          Tag = 18
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
            A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
            B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnClick = sbSideLineClick
          ExplicitLeft = 142
          ExplicitTop = 32
          ExplicitHeight = 81
        end
        object NoiseRedPanel: TPanel
          Tag = 18
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel34: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1064#1091#1084#1086#1087#1086#1076#1072#1074#1083#1077#1085#1080#1077' '
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel35: TPanel
        Left = 0
        Top = -47
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 14
        Visible = False
        object SpeedButton9: TSpeedButton
          Tag = 13
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
            A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
            B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnClick = sbSideLineClick
          ExplicitLeft = 144
          ExplicitTop = 32
          ExplicitHeight = 81
        end
        object AnglePanel: TPanel
          Tag = 13
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel37: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1059#1075#1086#1083' '#1074#1074#1086#1076#1072' ['#1075#1088#1072#1076'] '
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel38: TPanel
        Left = 0
        Top = -1378
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 15
        Visible = False
        object DurationPanel: TPanel
          Tag = 4
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel40: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1044#1083#1080#1090'. '#1090#1072#1082#1090#1072' ['#1084#1082#1089']'
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
        object SpinButton9: TSpinButton
          Tag = 4
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 2
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
      end
      object Panel41: TPanel
        Left = 0
        Top = -1257
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 16
        Visible = False
        object DelayScalePanel: TPanel
          Tag = 5
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel43: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1052#1072#1089#1096#1090#1072#1073' '#1040' '#1088#1072#1079#1074#1077#1088#1090#1082#1080
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
        object SpinButton10: TSpinButton
          Tag = 5
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 2
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
      end
      object Panel44: TPanel
        Left = 0
        Top = -1136
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 17
        Visible = False
        object ZondAmplPanel: TPanel
          Tag = 6
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel46: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1040#1084#1087#1083#1080#1090#1091#1076#1072' '#1047#1048' '
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
        object SpinButton11: TSpinButton
          Tag = 6
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 2
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
      end
      object Panel33: TPanel
        Left = 0
        Top = 437
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 18
        Visible = False
        object BScanMaxDelayPanel: TPanel
          Tag = 16
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object SpinButton1: TSpinButton
          Tag = 16
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 1
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
        object Panel39: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = '1 BScan '#1082#1086#1085#1077#1094' c'#1090#1088#1086#1073#1072
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
      end
      object Panel42: TPanel
        Left = 0
        Top = 316
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 19
        Visible = False
        object BScanMinDelayPanel: TPanel
          Tag = 15
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object SpinButton2: TSpinButton
          Tag = 15
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 1
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
        object Panel47: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' BScan '#1085#1072#1095#1072#1083#1086' c'#1090#1088#1086#1073#1072
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
      end
      object Panel22: TPanel
        Left = 0
        Top = -531
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 20
        Visible = False
        object EvaluationGateLevelPanel: TPanel
          Tag = 10
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel30: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1055#1086#1088#1086#1075' '#1040#1057#1044' ['#1076#1041']'
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
        object SpinButton4: TSpinButton
          Tag = 10
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 2
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
      end
      object AScanAttPanel: TPanel
        Left = 0
        Top = -1015
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 21
        object AttPanel: TPanel
          Tag = 77
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object SpinButton6: TSpinButton
          Tag = 77
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 1
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
        object Panel31: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1040#1058#1058' ['#1076#1041']'
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 195
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 22
        Visible = False
        object BScanGatePanel: TPanel
          Tag = 78
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1055#1086#1088#1086#1075' B-Scan ['#1076#1041']'
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
        object SpinButton7: TSpinButton
          Tag = 78
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 2
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 679
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 23
        Visible = False
        object BScanViewBtn: TSpeedButton
          Tag = 38
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
            A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
            B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnClick = sbSideLineClick
          ExplicitLeft = 142
          ExplicitTop = 32
          ExplicitHeight = 81
        end
        object BScanViewPanel: TPanel
          Tag = 38
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel52: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1054#1090#1086#1073#1088#1072#1078#1077#1085#1080#1077' '#1042'-Scan '
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel55: TPanel
        Left = 0
        Top = -1862
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 24
        Visible = False
        object Stroke: TPanel
          Tag = 1
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -23
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel57: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1058#1072#1082#1090
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
        object SpinButton8: TSpinButton
          Tag = 256
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 2
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
      end
      object Panel56: TPanel
        Left = 0
        Top = -1983
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 25
        Visible = False
        object StrokeCount: TPanel
          Tag = -1
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -23
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object Panel59: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1090#1072#1082#1090#1086#1074
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
        end
        object SpinButton12: TSpinButton
          Tag = -1
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 2
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 1284
        Width = 196
        Height = 121
        Align = alTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 26
        object SpeedButton2: TSpeedButton
          Tag = 19
          AlignWithMargins = True
          Left = 11
          Top = 11
          Width = 174
          Height = 99
          Margins.Left = 10
          Margins.Top = 10
          Margins.Right = 10
          Margins.Bottom = 10
          Align = alClient
          Caption = #1053#1072#1079#1072#1076
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -29
          Font.Name = 'Tahoma'
          Font.Style = []
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
            A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
            B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          ParentFont = False
          OnClick = SpeedButton2Click
          ExplicitLeft = 26
          ExplicitTop = 18
          ExplicitWidth = 145
          ExplicitHeight = 81
        end
      end
      object GateIdxPanel: TPanel
        Left = 0
        Top = -410
        Width = 196
        Height = 121
        Align = alTop
        ParentBackground = False
        TabOrder = 27
        Visible = False
        object GateIdxTextPanel: TPanel
          Tag = 16
          AlignWithMargins = True
          Left = 6
          Top = 38
          Width = 125
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Caption = '-'
          Color = clBtnHighlight
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnClick = EditModePanelClick
        end
        object SpinButton13: TSpinButton
          Tag = 22
          AlignWithMargins = True
          Left = 141
          Top = 38
          Width = 49
          Height = 77
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alRight
          DownGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          TabOrder = 1
          UpGlyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
            34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
            86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
            71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
            7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
            3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
            17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          OnDownClick = OnSpinDownBtnClick
          OnUpClick = OnSpinUpBtnClick
        end
        object Panel49: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1057#1090#1088#1086#1073
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
      end
      object AScanPhotoPanel: TPanel
        Left = 0
        Top = 1042
        Width = 196
        Height = 121
        Align = alTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 28
        object SpeedButton5: TSpeedButton
          Tag = 39
          AlignWithMargins = True
          Left = 11
          Top = 36
          Width = 174
          Height = 74
          Margins.Left = 10
          Margins.Right = 10
          Margins.Bottom = 10
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
            A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
            B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          Layout = blGlyphTop
          ParentFont = False
          OnClick = sbSideLineClick
          ExplicitLeft = 26
          ExplicitTop = 39
          ExplicitWidth = 145
          ExplicitHeight = 73
        end
        object AScanPhotoLabelPanel: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1057#1085#1080#1084#1086#1082' '#1101#1082#1088#1072#1085#1072
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
        end
      end
      object WriteBScanPanel: TPanel
        Left = 0
        Top = 1163
        Width = 196
        Height = 121
        Align = alTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 29
        object WriteBScanBtn: TSpeedButton
          Tag = 40
          AlignWithMargins = True
          Left = 11
          Top = 36
          Width = 174
          Height = 74
          Margins.Left = 10
          Margins.Right = 10
          Margins.Bottom = 10
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -29
          Font.Name = 'Tahoma'
          Font.Style = []
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
            9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
            A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
            B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
            BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
            F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
          ParentFont = False
          OnClick = sbSideLineClick
          ExplicitTop = 38
        end
        object WriteBScanLabel: TPanel
          Left = 1
          Top = 1
          Width = 194
          Height = 32
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Caption = ' '#1047#1072#1087#1080#1089#1100' B-'#1088#1072#1079#1074#1077#1088#1090#1082#1080
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentBackground = False
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
        end
      end
    end
    object rightPanelDownBtn: TBitBtn
      Left = 0
      Top = 415
      Width = 217
      Height = 65
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36140000424D3614000000000000360400002800000040000000400000000100
        08000000000000100000EB080000EB0800000001000000010000000000000101
        0100020202000303030004040400050505000606060007070700080808000909
        09000A0A0A000B0B0B000C0C0C000D0D0D000E0E0E000F0F0F00101010001111
        1100121212001313130014141400151515001616160017171700181818001919
        19001A1A1A001B1B1B001C1C1C001D1D1D001E1E1E001F1F1F00202020002121
        2100222222002323230024242400252525002626260027272700282828002929
        29002A2A2A002B2B2B002C2C2C002D2D2D002E2E2E002F2F2F00303030003131
        3100323232003333330034343400353535003636360037373700383838003939
        39003A3A3A003B3B3B003C3C3C003D3D3D003E3E3E003F3F3F00404040004141
        4100424242004343430044444400454545004646460047474700484848004949
        49004A4A4A004B4B4B004C4C4C004D4D4D004E4E4E004F4F4F00505050005151
        5100525252005353530054545400555555005656560057575700585858005959
        59005A5A5A005B5B5B005C5C5C005D5D5D005E5E5E005F5F5F00606060006161
        6100626262006363630064646400656565006666660067676700686868006969
        69006A6A6A006B6B6B006C6C6C006D6D6D006E6E6E006F6F6F00707070007171
        7100727272007373730074747400757575007676760077777700787878007979
        79007A7A7A007B7B7B007C7C7C007D7D7D007E7E7E007F7F7F00808080008181
        8100828282008383830084848400858585008686860087878700888888008989
        89008A8A8A008B8B8B008C8C8C008D8D8D008E8E8E008F8F8F00909090009191
        9100929292009393930094949400959595009696960097979700989898009999
        99009A9A9A009B9B9B009C9C9C009D9D9D009E9E9E009F9F9F00A0A0A000A1A1
        A100A2A2A200A3A3A300A4A4A400A5A5A500A6A6A600A7A7A700A8A8A800A9A9
        A900AAAAAA00ABABAB00ACACAC00ADADAD00AEAEAE00AFAFAF00B0B0B000B1B1
        B100B2B2B200B3B3B300B4B4B400B5B5B500B6B6B600B7B7B700B8B8B800B9B9
        B900BABABA00BBBBBB00BCBCBC00BDBDBD00BEBEBE00BFBFBF00C0C0C000C1C1
        C100C2C2C200C3C3C300C4C4C400C5C5C500C6C6C600C7C7C700C8C8C800C9C9
        C900CACACA00CBCBCB00CCCCCC00CDCDCD00CECECE00CFCFCF00D0D0D000D1D1
        D100D2D2D200D3D3D300D4D4D400D5D5D500D6D6D600D7D7D700D8D8D800D9D9
        D900DADADA00DBDBDB00DCDCDC00DDDDDD00DEDEDE00DFDFDF00E0E0E000E1E1
        E100E2E2E200E3E3E300E4E4E400E5E5E500E6E6E600E7E7E700E8E8E800E9E9
        E900EAEAEA00EBEBEB00ECECEC00EDEDED00EEEEEE00EFEFEF00F0F0F000F1F1
        F100F2F2F200F3F3F300F4F4F400F5F5F500F6F6F600F7F7F700F8F8F800F9F9
        F900FAFAFA00FBFBFB00FCFCFC00FDFDFD00FEFEFE00FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9B42429BFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5B000000005BFCFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5A0000000000005AFCFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5A00000000000000005AFC
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5A000000000000000000005A
        FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC59000000000000000000000000
        59FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5900000000000000000000000000
        0059FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC580000000000000000000000000000
        000058FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC58000000000000000087870000000000
        00000058FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFC58000000000000000087FFFF8700000000
        0000000058FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFC57000000000000000087FFFFFFFF87000000
        000000000057FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFC57000000000000000087FFFFFFFFFFFF870000
        00000000000057FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFC57000000000000000087FFFFFFFFFFFFFFFF8700
        0000000000000057FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFC56000000000000000087FFFFFFFFFFFFFFFFFFFF87
        000000000000000056FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFB56000000000000000087FFFFFFFFFFFFFFFFFFFFFFFF
        87000000000000000056FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFB56000000000000000087FFFFFFFFFFFFFFFFFFFFFFFFFF
        FF87000000000000000056FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFB56000000000000000087FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF87000000000000000055FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFB55000000000000000087FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF87000000000000000055FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFB55000000000000000087FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF87000000000000000055FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFB55000000000000000087FFFFFFFFFFFFFFFFFFE49A9AE4FFFFFFFF
        FFFFFFFFFF87000000000000000055FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFB54000000000000000087FFFFFFFFFFFFFFFFFFAE0E00000EAEFFFFFF
        FFFFFFFFFFFF87000000000000000054FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF80000000000000000087FFFFFFFFFFFFFFFFFFAB040000000004ABFFFF
        FFFFFFFFFFFFFF8700000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFE100000000000000087FFFFFFFFFFFFFFFFFFAA0400000000000004AAFF
        FFFFFFFFFFFFFFFF870000000000000010FEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF60200000000000087FFFFFFFFFFFFFFFFFFAA04000000000000000004AA
        FFFFFFFFFFFFFFFFFF8700000000000002F6FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF39000000000087FFFFFFFFFFFFFFFFFFAA040000000000000000000004
        AAFFFFFFFFFFFFFFFFFF87000000000039FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFD11A0000058FFFFFFFFFFFFFFFFFFFA904000000000000000000000000
        04A9FFFFFFFFFFFFFFFFFF8F0500001AD1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFEFA293D8FFFFFFFFFFFFFFFFFFA90400000000000000000000000000
        0004A9FFFFFFFFFFFFFFFFFFD893A2EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA904000000000000003C3C0000000000
        000004A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA803000000000000003CF3F33C00000000
        00000003A8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFA803000000000000003CF3FFFFF33C000000
        0000000003A8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFA803000000000000003CF3FFFFFFFFF33C0000
        000000000003A8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFA703000000000000003CF3FFFFFFFFFFFFF33C00
        00000000000003A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFA703000000000000003CF3FFFFFFFFFFFFFFFFF33C
        0000000000000003A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFA703000000000000003CF3FFFFFFFFFFFFFFFFFFFFF3
        3C0000000000000003A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFA603000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFF
        F33C0000000000000003A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFA603000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFF33C0000000000000003A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFA603000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF33C0000000000000003A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFA503000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFF33C0000000000000003A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFA503000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFF33C0000000000000003A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFA503000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFF33C0000000000000003A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFBA03000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF33C0000000000000003BAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF29000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFF33C0000000000000029FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF5000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFF33C00000000000000F5FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF1900000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFF33C000000000019FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF9B000000003DF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFF33D000000009BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFB14A3B86FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFA863B4AB1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ParentFont = False
      TabOrder = 2
      OnClick = rightPanelDownBtnClick
    end
    object rightPanelUpBtn: TBitBtn
      Left = 0
      Top = 0
      Width = 217
      Height = 65
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -22
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36140000424D3614000000000000360400002800000040000000400000000100
        08000000000000100000EB080000EB0800000001000000010000000000000101
        0100020202000303030004040400050505000606060007070700080808000909
        09000A0A0A000B0B0B000C0C0C000D0D0D000E0E0E000F0F0F00101010001111
        1100121212001313130014141400151515001616160017171700181818001919
        19001A1A1A001B1B1B001C1C1C001D1D1D001E1E1E001F1F1F00202020002121
        2100222222002323230024242400252525002626260027272700282828002929
        29002A2A2A002B2B2B002C2C2C002D2D2D002E2E2E002F2F2F00303030003131
        3100323232003333330034343400353535003636360037373700383838003939
        39003A3A3A003B3B3B003C3C3C003D3D3D003E3E3E003F3F3F00404040004141
        4100424242004343430044444400454545004646460047474700484848004949
        49004A4A4A004B4B4B004C4C4C004D4D4D004E4E4E004F4F4F00505050005151
        5100525252005353530054545400555555005656560057575700585858005959
        59005A5A5A005B5B5B005C5C5C005D5D5D005E5E5E005F5F5F00606060006161
        6100626262006363630064646400656565006666660067676700686868006969
        69006A6A6A006B6B6B006C6C6C006D6D6D006E6E6E006F6F6F00707070007171
        7100727272007373730074747400757575007676760077777700787878007979
        79007A7A7A007B7B7B007C7C7C007D7D7D007E7E7E007F7F7F00808080008181
        8100828282008383830084848400858585008686860087878700888888008989
        89008A8A8A008B8B8B008C8C8C008D8D8D008E8E8E008F8F8F00909090009191
        9100929292009393930094949400959595009696960097979700989898009999
        99009A9A9A009B9B9B009C9C9C009D9D9D009E9E9E009F9F9F00A0A0A000A1A1
        A100A2A2A200A3A3A300A4A4A400A5A5A500A6A6A600A7A7A700A8A8A800A9A9
        A900AAAAAA00ABABAB00ACACAC00ADADAD00AEAEAE00AFAFAF00B0B0B000B1B1
        B100B2B2B200B3B3B300B4B4B400B5B5B500B6B6B600B7B7B700B8B8B800B9B9
        B900BABABA00BBBBBB00BCBCBC00BDBDBD00BEBEBE00BFBFBF00C0C0C000C1C1
        C100C2C2C200C3C3C300C4C4C400C5C5C500C6C6C600C7C7C700C8C8C800C9C9
        C900CACACA00CBCBCB00CCCCCC00CDCDCD00CECECE00CFCFCF00D0D0D000D1D1
        D100D2D2D200D3D3D300D4D4D400D5D5D500D6D6D600D7D7D700D8D8D800D9D9
        D900DADADA00DBDBDB00DCDCDC00DDDDDD00DEDEDE00DFDFDF00E0E0E000E1E1
        E100E2E2E200E3E3E300E4E4E400E5E5E500E6E6E600E7E7E700E8E8E800E9E9
        E900EAEAEA00EBEBEB00ECECEC00EDEDED00EEEEEE00EFEFEF00F0F0F000F1F1
        F100F2F2F200F3F3F300F4F4F400F5F5F500F6F6F600F7F7F700F8F8F800F9F9
        F900FAFAFA00FBFBFB00FCFCFC00FDFDFD00FEFEFE00FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFB14A3B86FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFA863B4AB1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF9B000000003DF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFF33D000000009BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF1900000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFF33C000000000019FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF5000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFF33C00000000000000F5FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF29000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFF33C0000000000000029FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFBA03000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF33C0000000000000003BAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFA503000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFF33C0000000000000003A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFA503000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFF33C0000000000000003A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFA503000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFF33C0000000000000003A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFA603000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF33C0000000000000003A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFA603000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFF33C0000000000000003A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFA603000000000000003CF3FFFFFFFFFFFFFFFFFFFFFFFF
        F33C0000000000000003A6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFA703000000000000003CF3FFFFFFFFFFFFFFFFFFFFF3
        3C0000000000000003A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFA703000000000000003CF3FFFFFFFFFFFFFFFFF33C
        0000000000000003A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFA703000000000000003CF3FFFFFFFFFFFFF33C00
        00000000000003A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFA803000000000000003CF3FFFFFFFFF33C0000
        000000000003A8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFA803000000000000003CF3FFFFF33C000000
        0000000003A8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA803000000000000003CF3F33C00000000
        00000003A8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA904000000000000003C3C0000000000
        000004A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFEFA293D8FFFFFFFFFFFFFFFFFFA90400000000000000000000000000
        0004A9FFFFFFFFFFFFFFFFFFD893A2EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFD11A0000058FFFFFFFFFFFFFFFFFFFA904000000000000000000000000
        04A9FFFFFFFFFFFFFFFFFF8F0500001AD1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF39000000000087FFFFFFFFFFFFFFFFFFAA040000000000000000000004
        AAFFFFFFFFFFFFFFFFFF87000000000039FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF60200000000000087FFFFFFFFFFFFFFFFFFAA04000000000000000004AA
        FFFFFFFFFFFFFFFFFF8700000000000002F6FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFE100000000000000087FFFFFFFFFFFFFFFFFFAA0400000000000004AAFF
        FFFFFFFFFFFFFFFF870000000000000010FEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF80000000000000000087FFFFFFFFFFFFFFFFFFAB040000000004ABFFFF
        FFFFFFFFFFFFFF8700000000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFB54000000000000000087FFFFFFFFFFFFFFFFFFAE0E00000EAEFFFFFF
        FFFFFFFFFFFF87000000000000000054FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFB55000000000000000087FFFFFFFFFFFFFFFFFFE49A9AE4FFFFFFFF
        FFFFFFFFFF87000000000000000055FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFB55000000000000000087FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF87000000000000000055FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFB55000000000000000087FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF87000000000000000055FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFB56000000000000000087FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF87000000000000000055FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFB56000000000000000087FFFFFFFFFFFFFFFFFFFFFFFFFF
        FF87000000000000000056FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFB56000000000000000087FFFFFFFFFFFFFFFFFFFFFFFF
        87000000000000000056FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFC56000000000000000087FFFFFFFFFFFFFFFFFFFF87
        000000000000000056FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFC57000000000000000087FFFFFFFFFFFFFFFF8700
        0000000000000057FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFC57000000000000000087FFFFFFFFFFFF870000
        00000000000057FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFC57000000000000000087FFFFFFFF87000000
        000000000057FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFC58000000000000000087FFFF8700000000
        0000000058FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC58000000000000000087870000000000
        00000058FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC580000000000000000000000000000
        000058FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5900000000000000000000000000
        0059FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC59000000000000000000000000
        59FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5A000000000000000000005A
        FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5A00000000000000005AFC
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5A0000000000005AFCFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5B000000005BFCFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9B42429BFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ParentFont = False
      TabOrder = 3
      OnClick = rightPanelUpBtnClick
    end
  end
  object AScanDebugPanel: TPanel
    Left = 0
    Top = 0
    Width = 1314
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object DebugPanel: TPanel
      Left = 267
      Top = 0
      Width = 1047
      Height = 41
      Align = alClient
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 18
      Font.Name = 'Verdana'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnDblClick = DebugPanelDblClick
      object DebugPanelLabel: TLabel
        Left = 0
        Top = 0
        Width = 1047
        Height = 41
        Align = alClient
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 18
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        WordWrap = True
        ExplicitLeft = 104
        ExplicitTop = 10
        ExplicitWidth = 48
        ExplicitHeight = 18
      end
    end
    object Panel36: TPanel
      Left = 0
      Top = 0
      Width = 267
      Height = 41
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
    end
  end
  object BScanHandTimer: TTimer
    Enabled = False
    Interval = 500
    OnTimer = BScanHandTimerTimer
    Left = 24
    Top = 95
  end
  object BScanDrawTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = BScanDrawTimerTimer
    Left = 88
    Top = 95
  end
  object ApplicationEvents1: TApplicationEvents
    OnMessage = ApplicationEvents1Message
    Left = 136
    Top = 95
  end
  object Timer1: TTimer
    Interval = 250
    OnTimer = Timer1Timer
    Left = 184
    Top = 95
  end
  object HandScanTimeTimer: TTimer
    Interval = 5000
    OnTimer = HandScanTimeTimerTimer
    Left = 240
    Top = 95
  end
  object MarkMovementTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = MarkMovementTimerTimer
    Left = 336
    Top = 95
  end
end
