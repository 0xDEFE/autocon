﻿/**
 * @file BScanMeasureUnit.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef BScanMeasureUnitH
#define BScanMeasureUnitH
//---------------------------------------------------------------------------

#include <set>
//#include "Utils.h"
#include "defcore/ChannelsTable.h"
#include "defcore/DeviceConfig.h"
#include "DataStorage.h"

typedef int CID;

//! Непосредственные данные измерения. Содержит значение измерения и результат сравнения с эталоном.
class BScanMeasureData
{
public:
    BScanMeasureData()
    {
        measureValue = 0;
        errorValue = -1.0;
    };

    bool hasChannel(CID id)
    {
        std::set<CID>::iterator it = channels.find(id);
        return it != channels.end();
    }

    std::string signalTag;      //!< Тэг используемого драйвера для измерения сигнала.
    std::string perSignalTag;   //!< Тэг используемого драйвера для измерения набора данных, полученных при измерении сигнала в одном канале.
    std::string perChannelTag;  //!< Тэг используемого драйвера для измерения набора данных,
    double measureValue;        //!< Значение измерения
    double errorValue;          //!< Значение ошибки
    std::set<CID> channels;     //!< Измеряемые каналы
};

typedef float PolyVal;
typedef Vec2T<PolyVal> PolyPt;
typedef PolyT<PolyVal> PolyReg;
//! Регион для проведения измерения
class BScanRegion
{
public:
    bool hasChannel(CID id)
    {
        for(unsigned int i = 0; i < measureData.size(); i++)
        {
            if(measureData[i].hasChannel(id))
                return true;
        }
        return false;
    }

    AnsiString defect_name; //!< Имя измеряемого дефекта
    AnsiString region_name; //!< Имя измеряемого региона
    std::vector<BScanMeasureData> measureData; //!< Массив проводимых для региона измерений
    PolyReg poly;   //!< Регион
};

#define BSM_ABIL_SIGNAL         0x01
#define BSM_ABIL_PER_SIGNAL     0x02
#define BSM_ABIL_PER_CHANNEL    0x04
#define BSM_ABIL_PER_REGION     0x08

//! Интерфейс драйвера измерения
class BScanMeasureDriver
{
public:
    virtual const char* tag() const = 0;  //!< Текстовый тэг, название драйвера
    virtual void nextSig(const tUMU_BScanSignal& signal) = 0; //!< Функция измерения следующего сигнала
    virtual void nextVal(const double& signal) = 0; //!< Функция измерения следующего значения
    virtual double result() const = 0;              //!< Отдает результат измерения
    virtual void clear() = 0;                       //!< Очищает результат измерения
    virtual void init(double val) = 0;              //!< Инициализирует начальным значением
    virtual unsigned int flags() const = 0;         //!< Возвращает флаги применимости драйвера
    virtual BScanMeasureDriver* clone() const = 0;
};

//! Базовый тип драйвера измерения
class SimpleDrv : public BScanMeasureDriver
{
public:
    SimpleDrv() : BScanMeasureDriver(), bFM(true) {clear();}
    SimpleDrv(const SimpleDrv& other) {res = other.res; bFM = other.bFM;}
    void nextSig(const tUMU_BScanSignal& signal) {};
    void nextVal(const double& signal) {};
    double result() const {return res;};
    void clear() {res = 0; bFM = true;};
    void init(double val) {res = val;}
    unsigned int flags() const {return BSM_ABIL_PER_SIGNAL | BSM_ABIL_PER_CHANNEL | BSM_ABIL_PER_REGION;}
protected:
    double res;
    bool bFM;
};

//! Измеряет амплитуду в децибелах (осторожно, есть отрицательные значения)
class AmplitudeMeasDrv_db : public SimpleDrv
{
public:
    AmplitudeMeasDrv_db() : SimpleDrv() {};
    const char* tag() const {return "амплитуда (дБ)";};
    unsigned int flags() const {return BSM_ABIL_SIGNAL;}
    void nextSig(const tUMU_BScanSignal& signal)
    {
        float maxAmp = 20.f * log10((float)signal.Ampl[0] / 32.f + 0.0001f);
        for(int j = 1; j < 24; j++)
        {
            maxAmp = std::max<float>(maxAmp,20.f * log10((float)signal.Ampl[j] / 32.f + 0.0001f));
        }
        res = maxAmp;
    };
    BScanMeasureDriver* clone() const {return new AmplitudeMeasDrv_db(*this);}
};

//! Измеряет амплитуду в отсчетах
class AmplitudeMeasDrv_raw : public SimpleDrv
{
public:
    AmplitudeMeasDrv_raw() : SimpleDrv() {};
    const char* tag() const {return "амплитуда (отсч.)";};
    unsigned int flags() const {return BSM_ABIL_SIGNAL;}
    void nextSig(const tUMU_BScanSignal& signal)
    {
        float maxAmp = signal.Ampl[0];
        for(int j = 1; j < 24; j++)
        {
            maxAmp = std::max<float>(maxAmp,signal.Ampl[j]);
        }
        res = maxAmp;
    };
    BScanMeasureDriver* clone() const {return new AmplitudeMeasDrv_raw(*this);}
};

//! Измеряет задержку
class DelayMeasDrv : public SimpleDrv
{
public:
    DelayMeasDrv() : SimpleDrv() {};
    const char* tag() const {return "задержка";};
    unsigned int flags() const {return BSM_ABIL_SIGNAL;}
    void nextSig(const tUMU_BScanSignal& signal)
    {
        res = signal.Delay;
    };
    BScanMeasureDriver* clone() const {return new DelayMeasDrv(*this);}
};

//! Получает максимум из набора значений
class MaxMeasDrv : public SimpleDrv
{
public:
    MaxMeasDrv() : SimpleDrv() {};
    const char* tag() const {return "максимум";};
    void nextVal(const double& signal) {res = bFM ? signal : std::max(signal, res); bFM = false;};
    BScanMeasureDriver* clone() const {return new MaxMeasDrv(*this);}
};

//! Получает минимум из набора значений
class MinMeasDrv : public SimpleDrv
{
public:
    MinMeasDrv() : SimpleDrv() {};
    const char* tag() const {return "минимум";};
    void nextVal(const double& signal) {res = bFM ? signal : std::min(signal, res); bFM = false;};
    BScanMeasureDriver* clone() const {return new MinMeasDrv(*this);}
};

//! Получает сумму набора значений
class SumMeasDrv : public SimpleDrv
{
public:
    SumMeasDrv() : SimpleDrv() {};
    const char* tag() const {return "сумма";};
    void nextVal(const double& signal) {res += signal;};
    BScanMeasureDriver* clone() const {return new SumMeasDrv(*this);}
};

//! Получает среднее из набора значений
class AverageMeasDrv : public SimpleDrv
{
public:
    AverageMeasDrv() : SimpleDrv(), count(0) {};
    AverageMeasDrv(const AverageMeasDrv& other) : SimpleDrv(other) {count = other.count;}
    const char* tag() const {return "среднее";};
    void nextVal(const double& signal) {res += signal;count++;};
    double result() const {return count ? res/double(count) : 0;};
    void clear() {count = 0; res = 0;}
    BScanMeasureDriver* clone() const {return new AverageMeasDrv(*this);}
protected:
    int count;
};


typedef struct {
	int EchoCount[5]; // Количество сигналов в зоне
	int PolyCount;    // Количество зон
} BScanMeasureResItem;

//! Выполняет измерения
class BScanMeasure
{
public:
    BScanMeasure() {
        measureValue = 0;
    }

    //! Конструктор копирования
	BScanMeasure(const BScanMeasure& other) {
        name = other.name;
        perRegionTag = other.perRegionTag;
        measureValue = other.measureValue;
        regions = other.regions;

        std::map<std::string, BScanMeasureDriver*>::const_iterator it = other.drivers.begin();
        while(it != other.drivers.end())
        {
            addDriver(it->second->clone());
            it++;
		}
		tempDrvIt = drivers.begin();
	}

	//! Для приравнивания путем замены
	friend void swap(BScanMeasure& first, BScanMeasure& second)
	{
		using std::swap;

		swap(first.name, second.name);
		swap(first.perRegionTag, second.perRegionTag);
		swap(first.measureValue, second.measureValue);
		swap(first.regions, second.regions);
		swap(first.drivers, second.drivers);
		swap(first.tempDrvIt, second.tempDrvIt);
	};

	//! Moveable
	BScanMeasure& operator=(const BScanMeasure& other)
	{
		BScanMeasure tmp(other);
		swap(*this, tmp);
		return *this;
	}

	~BScanMeasure()
	{
		clear();
		clearDrivers();
	}


    //! Сохранение в XML файл
    bool save(const char* path,cChannelsTable* pChanTable);
    //! Загрузка из XML файла
    bool load(const char* path,cChannelsTable* pChanTable,cDeviceConfig* pConfig);
    //! Установка регионов из другого измерения
    bool setRegionsFrom( const BScanMeasure& other )
    {
        perRegionTag = other.perRegionTag;
        measureValue = other.measureValue;
        regions = other.regions;
        return true;
    };

    //! Сравнение с другим измерением. При этом в BScanMeasureData::errorValue попадет ошибка по регионам, а в fTotalError - общая ошибка.
    double compare(const BScanMeasure& other);

    //! Очистка измерения
    void clear();

    //! Расчет измерений по регионам из отчета
    void calculate(cJointTestingReport* pRep);

    //! Очистка драйверов измерений
    void clearDrivers();
    template<typename T>
    T* addDriver() //!< Добавление нового типа драйвера (рождается по темплейту внутри функции)
    {
        T* pDriver = new T();

        if(!addDriver(dynamic_cast<BScanMeasureDriver*>(pDriver)))
        {
            delete pDriver;
        }
        return pDriver;
	}

	BScanMeasureDriver* newDriver(const std::string& tag); //! Порождение нового экземпляра драйвера по его тегу
	BScanMeasureDriver* firstDriver(); //! Для итерации по драйверам
	BScanMeasureDriver* nextDriver();  //! Для итерации по драйверам
public:
	bool addDriver(BScanMeasureDriver* driver)
	{
		std::map<std::string, BScanMeasureDriver*>::iterator it = drivers.find(std::string(driver->tag()));
		if(it != drivers.end())
			return false;
		drivers[std::string(driver->tag())] = driver;
		return true;
	}

	AnsiString name;
	std::string perRegionTag;
	double measureValue;
	std::vector<BScanRegion> regions;

	BScanMeasureResItem ResList[90];

    std::map<std::string, BScanMeasureDriver*>::iterator tempDrvIt;
    std::map<std::string, BScanMeasureDriver*> drivers;
};

#endif
