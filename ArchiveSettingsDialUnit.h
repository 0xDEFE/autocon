﻿/**
 * @file ArchiveSettingsDialUnit.h
 * @author Kirill Beliaevskiy
 */
//----------------------------------------------------------------------------
#ifndef ArchiveSettingsDialUnitH
#define ArchiveSettingsDialUnitH
//----------------------------------------------------------------------------
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <StdCtrls.hpp>
#include <Controls.hpp>
#include <Forms.hpp>
#include <Graphics.hpp>
#include <Classes.hpp>
#include <SysUtils.hpp>
#include <Windows.hpp>
#include <System.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ToolWin.hpp>
//----------------------------------------------------------------------------

/** \struct sTableColBtnDesc
    \brief Описывает кнопку, отвечающую за отображение элемента таблицы
 */
struct sTableColBtnDesc
{
	sTableColBtnDesc()
	{
		pBtn = 0;
        bState = false;
    }
	TSpeedButton* pBtn;     //!< Указатель на отображаемую кнопку
	eADBFieldType tableFieldType;  //!< Тип колонки таблицы
	bool bState;  //!< Состояние: выбрано\не выбрано
};

//! Диалог настроек архива. Содержит выбор отображаемых столбцов.
class TArchiveSettingsDial : public TForm
{
__published:
	TButton *OKBtn;
	TButton *CancelBtn;
	TLabel *m_TableHeaderLabel;
	TFlowPanel *m_SelectTableBtnsPanel;
    TPanel *Panel1;
    TSpeedButton *m_ClearBtn;
    TSpeedButton *m_FillBtn;
    TSpeedButton *m_SetDefaultBtn;
    TPanel *Panel5;
    TLabel *Label2;
    TPanel *Panel3;
    TPanel *Panel2;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall ColBtnClick(TObject *Sender);
    void __fastcall m_ClearBtnClick(TObject *Sender);
    void __fastcall m_FillBtnClick(TObject *Sender);
    void __fastcall m_SetDefaultBtnClick(TObject *Sender);
private:
	std::vector<sTableColBtnDesc> tableColBtnArr;       //!< Набор кнопок и сопутствующей информации для выбора активных столбцов таблицы
	void updateTableHeaderLabel();  //!< Обновить TLabel, в котором показана инфа о кол-ве выбранных столбцов
    void updateButtonMask(bool bFromMaskToBtns = false); //!< Сделать кнопки активныме по маске или сделать маску из активных кнопок
	DWORD ColBtnMask;  //!< Маска, в которой бит соответствует состоянию (активна/не активна) столбца таблицы
public:
	virtual __fastcall TArchiveSettingsDial(TComponent* AOwner);

	DWORD GetSelectedColumns(); //!< Получить маску выбранных столбцов
	void SetSelectedColumns(DWORD val); //!< Установить выбранные столбцы по маске
};
//----------------------------------------------------------------------------
extern PACKAGE TArchiveSettingsDial *ArchiveSettingsDial;
//----------------------------------------------------------------------------
#endif    
