//---------------------------------------------------------------------------

#ifndef ScanScriptSimUnitH
#define ScanScriptSimUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>

#include "MemoExUnit.h"
#include "ScanScriptParser.h"
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.ToolWin.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------

class cScanScriptSim : public cScanScriptProgramm
{
public:
    cScanScriptSim()
    {
        bSimulationMode = true;
        caretSpeed = 200;
        moveProcessPrevTime = GetTickCount();
    };

    void init();

    bool sim_processMove();
    bool sim_isMoving() {return sim_caretStatus[0] || sim_caretStatus[1];};
    int sim_GetActiveChannel();  //two modes
    int sim_GetChannelGroup();  //two modes
    void sim_Ac_GetCaretStatus(bool caretPos, bool* bActive, int* pCoord); //two modes
public:
    bool bSimulationMode;

    float sim_caretPos[2];
    bool sim_caretStatus[2];
    float sim_caretTargets[2];
    int sim_curScanGroup;
    int sim_curChannel;
    bool sim_scanEnabled;

    std::vector<UINT> tuneFilter;

    float caretSpeed; //mm/sec
    DWORD moveProcessPrevTime;

    void sim_Ac_CaretManualMove(int topOffset,int bottomOffset);
    void sim_Ac_MoveCaretsToStart();

    void sim_Dev_Update(bool bVal);
    void sim_Dev_DisableAll();
    void sim_Dev_SetChannelGroup(int chGr);
    void sim_Dev_SetChannel(int ch);
protected:
    eScanScriptRetCode handleSimCmd( USHORT cmdCode, UINT argc);
    eScanScriptRetCode executeCmd(USHORT cmdCode); //Overrided
};

//---------------------------------------------------------------------------

#define SIMFLAG_SCRIPT_CHANGED  0x1
#define SIMFLAG_SAVED           0x2
#define SIMFLAG_COMPILED        0x4

#define SIMFLAG_ERROR           0x20

#define SIMFLAG_STEP            0x40

enum eScanScriptSimState
{
    SIMSTATE_EDITING = 0,
    SIMSTATE_RUNNING,
    SIMSTATE_BREAK,
};

struct sRailDefect
{
    int offset;
    int delta;
    int strobeStart;
    int strobeEnd;
};
//---------------------------------------------------------------------------
class TScanScriptSimForm : public TForm
{
__published:	// IDE-managed Components
    TPanel *m_ScriptEditPanel;
    TSplitter *Splitter2;
    TToolBar *ToolBar1;
    TToolButton *m_CompileScriptBtn;
    TToolButton *ToolButton2;
    TToolButton *m_RunScriptBtn;
    TToolButton *m_PauseScriptBtn;
    TToolButton *m_StopScriptBtn;
    TToolButton *ToolButton3;
    TToolButton *m_StepOverScriptBtn;
    TPanel *m_CodePanel;
    TMemo *m_LogMemo;
    TPanel *Panel2;
    TPaintBox *m_CaretDrawPBox;
    TPaintBox *m_ChannelsDrawPBox;
    TImageList *ImageList1;
    TImageList *ImageList2;
    TTimer *m_ScriptTimer;
    TToolBar *m_MainToolBar;
    TToolButton *m_LoadBtn;
    TToolButton *ToolButton1;
    TToolButton *m_SaveBtn;
    TToolButton *ToolButton7;
    TToolButton *m_ClearBtn;
    TToolButton *ToolButton8;
    TToolButton *m_CloseBtn;
    TImageList *ImageList3;
    TToolButton *ToolButton4;
    TOpenDialog *OpenDialog1;
    TSaveDialog *SaveDialog1;
    TPageControl *PageControl1;
    TTabSheet *TabSheet1;
    TTabSheet *TabSheet2;
    void __fastcall FrameResize(TObject *Sender);
    void __fastcall m_CompileScriptBtnClick(TObject *Sender);
    void __fastcall m_RunScriptBtnClick(TObject *Sender);
    void __fastcall OnMemoLineClick(TObject *Sender);
    void __fastcall OnScriptChanged(TObject *Sender);
    void __fastcall m_ScriptTimerTimer(TObject *Sender);
    void __fastcall m_PauseScriptBtnClick(TObject *Sender);
    void __fastcall m_StopScriptBtnClick(TObject *Sender);
    void __fastcall m_StepOverScriptBtnClick(TObject *Sender);
    void __fastcall FrameEnter(TObject *Sender);
    void __fastcall m_CaretDrawPBoxPaint(TObject *Sender);
    void __fastcall m_ChannelsDrawPBoxPaint(TObject *Sender);
    void __fastcall m_CloseBtnClick(TObject *Sender);
    void __fastcall m_LoadBtnClick(TObject *Sender);
    void __fastcall m_SaveBtnClick(TObject *Sender);
    void __fastcall m_ClearBtnClick(TObject *Sender);
private:	// User declarations
    TMemoEx* pCodeMemo;
    cScanScriptSim scriptSim;

    DWORD SimFlags;
    eScanScriptSimState SimState;

    std::vector<sRailDefect> railDefects;

    void processStates();
    void setState(eScanScriptSimState newState);
    void processMessageStream();
    void logOutput(UnicodeString str);
    void drawCarets(bool bFullRedraw = false);
    void drawChannels();
    void draw();
public:		// User declarations
    void SetScriptCode(UnicodeString str);
    void AddDefectToRail(int offset, int delta, int strobeStart, int strobeEnd);
    void ClearRailDefects();

    __fastcall TScanScriptSimForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TScanScriptSimForm *ScanScriptSimForm;
//---------------------------------------------------------------------------
#endif
