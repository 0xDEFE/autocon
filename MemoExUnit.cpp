//---------------------------------------------------------------------------

#pragma hdrstop

#include "MemoExUnit.h"
#include <algorithm>
//---------------------------------------------------------------------------
#pragma package(smart_init)


__fastcall TMemoEx::TMemoEx(TComponent* Owner)
    : TMemo( Owner )
{
    TextMarginSize = 15;
    ImageMarginSize = 16;
    prevLineCount = -1;
    topLineIndex = 0;
    bottomLineIndex = 0;
    canvas = new TControlCanvas();
    pImageList = NULL;
    Font->Height = -16;
    OnLeftLineClicked = NULL;

    arrowCursor = LoadCursor(NULL, IDC_ARROW);
}

__fastcall TMemoEx::~TMemoEx()
{
    DestroyCursor(arrowCursor);
    delete canvas;
}

//----------Message handlers---------------------

void __fastcall TMemoEx::WMResize(TMessage &Msg)
{
    TMemo::Dispatch(&Msg);
    UpdateMargins();
}

void __fastcall TMemoEx::WMUpdateLines(TMessage &Msg)
{
    TMemo::Dispatch(&Msg);
    UpdateVisibleLines( (Msg.Msg == WM_VSCROLL) || (Msg.Msg == WM_MOUSEWHEEL) );

}

void __fastcall TMemoEx::WMPaint(TMessage &Msg)
{
    TMemo::Dispatch(&Msg);
    UpdateDraw();
};

void __fastcall TMemoEx::WMOnClick(TMessage &Msg)
{
    TMemo::Dispatch(&Msg);

    POINTS pts = MAKEPOINTS(Msg.LParam);

    if(pts.x < (int)(ImageMarginSize + TextMarginSize))
    {
        int LineNo = Perform(EM_LINEFROMCHAR, -1, 0);

        if(OnLeftLineClicked)
        {
            ClickedLineNum = LineNo;
            OnLeftLineClicked(this);
        }
    }
}

void __fastcall TMemoEx::WMOnMMove(TMessage &Msg)
{
    TMemo::Dispatch(&Msg);

    POINTS pts = MAKEPOINTS(Msg.LParam);

    if(pts.x < (int)(ImageMarginSize + TextMarginSize))
    {
        ::SetCursor(arrowCursor);
    }
}

//---------------------Other stuff--------------------------

void TMemoEx::UpdateMargins()
{
    TRect Rect;
    SendMessage(Handle, EM_GETRECT, 0, LongInt(&Rect));
    Rect.Left = TextMarginSize + ImageMarginSize + 4;
    Rect.Top = 0;
    Rect.Bottom = Height;
    Rect.Right = Width;
    SendMessage(Handle, EM_SETRECT, 0, LongInt(&Rect));
    Refresh();
}

void TMemoEx::UpdateVisibleLines(bool bForceUpdate)
{
    if((prevLineCount != Lines->Count) || (prevScrollPos != CaretPos.y) || bForceUpdate)
    {
        prevLineCount = Lines->Count;
        prevScrollPos = CaretPos.y;

        topLineIndex = SendMessage(Handle,EM_GETFIRSTVISIBLELINE,0,0);
        int linechar = SendMessage(Handle,EM_CHARFROMPOS,0,(ClientHeight-1) << 16 );
        bottomLineIndex = (linechar & 0xffff0000) >> 16;

        //Update margins
        for(int i = topLineIndex; i < bottomLineIndex; i++)
        {
            if((i % 10) == 0)
            {
                UnicodeString numStr = IntToStr(i);
                unsigned int numWidth = canvas->TextWidth(numStr) + 6;
                TextMarginSize = std::max(numWidth,TextMarginSize);
                UpdateMargins();
            }
        }

        UpdateDraw();
    }
}

void TMemoEx::UpdateDraw()
{

    TRect drawBounds = ClientRect;
    TRect leftRectImage = TRect(0,drawBounds.top,
                                ImageMarginSize,drawBounds.bottom);
    TRect leftRectText = TRect(leftRectImage.right,drawBounds.top,
                                leftRectImage.right + TextMarginSize,drawBounds.bottom);
    TRect rightRect = TRect(leftRectText.right,drawBounds.top,
                                drawBounds.right,drawBounds.bottom);

    TColor backgroundCol = TColor(RGB(238,232,212));
    TColor markColor = clGray;

    try
    {
        canvas->Control = this;
        canvas->Font->Height = Font->Height;
        canvas->Font->Name = Font->Name;
        canvas->Brush->Color = backgroundCol;
        canvas->FillRect(leftRectText);
        canvas->FillRect(leftRectImage);

        if((topLineIndex != bottomLineIndex) && (topLineIndex >= 0) && (bottomLineIndex <= Lines->Count))
        {
            TTextFormat tf;
            tf.Clear();
            tf << tfCenter << tfVerticalCenter << tfSingleLine;

            canvas->Font->Color = markColor;
            canvas->Pen->Color = markColor;
            canvas->Pen->Width = 1;

            TBitmap* pictBmp = new TBitmap();
            pictBmp->PixelFormat = pf32bit;

            if(pImageList)
            {
                pictBmp->SetSize(pImageList->Width,pImageList->Height);
                pImageList->BkColor = backgroundCol;
            }


            int totH = 3;
            for(int i = topLineIndex; i < bottomLineIndex; i++)
            {
                int lineH = canvas->TextHeight( "asdDAsad" );

                TRect textRect(leftRectText.left,totH,leftRectText.right,totH+lineH);
                TRect imageRect(leftRectImage.left,totH,leftRectImage.right,totH+lineH);

                if((i % 10) == 0)
                {
                    UnicodeString numStr = IntToStr(i);

                    canvas->TextRect(textRect,numStr,tf);
                }
                else
                {
                    canvas->MoveTo(textRect.right/2 + textRect.right/4,textRect.top+textRect.Height()/2);
                    canvas->LineTo(textRect.right,textRect.top+textRect.Height()/2);
                }

                if((i < (int)marks.size()) && (marks[i] != -1) && pImageList)
                {
                    TRect pictRect(imageRect.Width()/2 - imageRect.Height()/2, imageRect.top,
                                    imageRect.Width()/2 + imageRect.Height()/2, imageRect.bottom);

                    pictBmp->Canvas->Brush->Color = pImageList->BkColor;
                    pictBmp->Canvas->FillRect(TRect(0,0,pImageList->Width,pImageList->Height));


                    pImageList->GetBitmap(marks[i],pictBmp);
                    canvas->StretchDraw(pictRect,pictBmp);
                }
                totH+=lineH;
            }
            delete pictBmp;

        }
    }
    catch (...)
    {
    }
}

void TMemoEx::setMark(unsigned int line, int imageIndex)
{
    if(line >= marks.size())
        marks.resize(line+1, -1);

    marks[line] = imageIndex;
}

int TMemoEx::getMark(unsigned int line)
{
    if(line >= marks.size())
        return -1;
    return marks[line];
}

void TMemoEx::setImageList( TImageList* pImageList)
{
    this->pImageList = pImageList;
}