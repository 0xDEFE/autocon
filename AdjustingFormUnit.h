﻿/**
 * @file AdjustingFormUnit.h
 * @author Kirill Beliaevskiy
 */
//---------------------------------------------------------------------------

#ifndef AdjustingFormUnitH
#define AdjustingFormUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
#include "BScanPainter.h"
#include <Vcl.Buttons.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.Series.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <Vcl.ButtonGroup.hpp>
#include <Vcl.ComCtrls.hpp>
#include <VCLTee.TeCanvas.hpp>


/** \struct sLabelValue
    \brief Содержит текстовую информацию о канале контроля, выводимую при помощи TLabel.
 */
struct sLabelValue
{
    int value;
    int newValue;

    TLabel* pLbl;
    UnicodeString prefix;
    UnicodeString postfix;

    void setValue()
    {
        if(newValue!=value)
        {
            pLbl->Caption = prefix + IntToStr(newValue) + postfix;
            value = newValue;
        };
    };
};

/** \enum eAdjustingState
    \brief Текущее состояние юстировки
 */
enum eAdjustingState
{
    ADJSTATE_NONE,                  //!< Не известно
    ADJSTATE_INITIAL,               //!< Юстировка в начальном состоянии
    ADJSTATE_TOP_CARET_MOVEMENT,    //!< Ручное движение верхней каретки
    ADJSTATE_BOT_CARET_MOVEMENT,    //!< Ручное движение нижней каретки
    ADJSTATE_AUTO_CARET_MOVEMENT,   //!< Автоматический проход верхней и нижней кареткой
    ADJSTATE_FINISHED,              //!< Юстировка окончена
};

//! Форма юстировки
class TAdjustingForm : public TForm
{
__published:	// IDE-managed Components
    TPanel *m_LeftPanel;
    TPanel *Panel3;
    TPanel *Panel4;
    TGridPanel *m_ChartGrid;
    TPanel *m_KP2Panel;
    TSplitter *Splitter1;
    TPaintBox *m_BScanPBox;
    TPanel *m_BScanPanel;
    TTimer *m_DrawTimer;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label3;
    TLabel *m_DateLabel;
    TSpeedButton *m_CloseBtn;
    TSpeedButton *SpeedButton2;
    TSpeedButton *m_AdjustingNextBtn;
    TPanel *m_RailDrawPanel;
    TPaintBox *m_RailDrawPBox;
    TGridPanel *m_GridPanel1;
    TChart *m_AscanChartKP2;
    TLineSeries *AScanSeries2;
    TPanel *Panel2;
    TGridPanel *m_GridPanel;
    TChart *m_AscanChartKP1;
    TPanel *Panel5;
    TGridPanel *GridPanel4;
    TChart *m_AscanChartKP3;
    TLineSeries *LineSeries2;
    TPanel *Panel6;
    TGridPanel *GridPanel5;
    TChart *m_AscanChartKP4;
    TLineSeries *LineSeries3;
    TPanel *Panel7;
    TGridPanel *GridPanel6;
    TChart *m_AscanChartKP5;
    TLineSeries *LineSeries4;
    TPanel *Panel8;
    TGridPanel *GridPanel7;
    TChart *m_AscanChartKP6;
    TLineSeries *LineSeries5;
    TPanel *Panel9;
    TGridPanel *GridPanel8;
    TChart *m_AscanChartKP8;
    TLineSeries *LineSeries6;
    TPanel *Panel10;
    TGridPanel *GridPanel9;
    TChart *m_AscanChartKP7;
    TLineSeries *LineSeries7;
    TLineSeries *Series1;
    TTimer *m_UpdateTimer;
    TPanel *Panel11;
    TLabel *m_NLabel_kp2;
    TLabel *m_HLabel_kp2;
    TLabel *m_KuLabel_kp2;
    TLabel *m_NLabel_kp1;
    TLabel *m_NLabel_kp3;
    TLabel *m_NLabel_kp4;
    TLabel *m_NLabel_kp5;
    TLabel *m_NLabel_kp6;
    TLabel *m_NLabel_kp8;
    TLabel *m_NLabel_kp7;
    TLabel *m_HLabel_kp1;
    TLabel *m_HLabel_kp3;
    TLabel *m_HLabel_kp4;
    TLabel *m_HLabel_kp5;
    TLabel *m_HLabel_kp6;
    TLabel *m_HLabel_kp8;
    TLabel *m_HLabel_kp7;
    TLabel *m_KuLabel_kp3;
    TLabel *m_KuLabel_kp4;
    TLabel *m_KuLabel_kp5;
    TLabel *m_KuLabel_kp6;
    TLabel *m_KuLabel_kp8;
    TLabel *m_KuLabel_kp7;
    TLabel *m_KuLabel_kp1;
    TTimer *m_ChangeModeTimer;
    TSpeedButton *m_BScanExpandBtn;
    TLabel *m_AdjustingStateLabel;
    TComboFlat *m_ActiveChannelsCombo;
    TLabel *Label5;
    void __fastcall m_BScanPBoxPaint(TObject *Sender);
    void __fastcall m_BScanPanelResize(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall m_DrawTimerTimer(TObject *Sender);
    void __fastcall m_RailDrawPBoxPaint(TObject *Sender);
    void __fastcall m_RailDrawPanelResize(TObject *Sender);
    void __fastcall m_UpdateTimerTimer(TObject *Sender);
    void __fastcall m_CloseBtnClick(TObject *Sender);
    void __fastcall m_BScanPBoxMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
    void __fastcall OnChartClicked(TObject *Sender);
    void __fastcall m_ChangeModeTimerTimer(TObject *Sender);
    void __fastcall m_BScanExpandBtnClick(TObject *Sender);
    void __fastcall m_AdjustingNextBtnClick(TObject *Sender);
    void __fastcall m_ActiveChannelsComboSelect(TObject *Sender);
    void __fastcall FormHide(TObject *Sender);
private:	// User declarations
    cBScanPainter painter; //!< Рисует В-развертку
    eAdjustingState adjState; //!< Предыдущее состояние юстировки
    eAdjustingState newAdjState; //!< Последнее состояние юстировки

    static const int RailCheckChannels[9]; //!< Идентификаторы каналов контроля

    TColor chartColors[9];  //!< Цвета графиков А-развертки
    TChart* charts[9];      //!< Графики А-развертки
    TLineSeries* chartSerieses[9]; //!< Точки графиков А-развертки

    sLabelValue NLabels[9]; //!< Для отображения измерения "N" в канале контроля
    sLabelValue HLabels[9]; //!< Для отображения измерения "H" в канале контроля
    sLabelValue KuLabels[9];  //!< Для отображения измерения "Ку" в канале контроля

    int channelStates[9];   //!< Состояния каналов контроля
    int currExpandedChart;  //!< Индекс графика, расширенного на весь экран

    bool ChangeModeFlag;    //!< Флаг, говорящий о наличии переходных процессов в контроллере.
    bool bLeftPanelExpanded;   //!< Индицирует расширение левой панели (с В_разверткой) на весь экран
    bool bAllChannelsDisabled;  //!< Индицирует о отключении всех каналов

    void ValueToScreen();  //!< Производит считывание новых сигналов с каналов контроля и перерисовка А- и В- развертки
    void PutBScanSignals(); //!< Производит считывание новых сигналов с каналов контроля

    void AdjustingMotion_(int WorkCycle); //!< Режим "Юстировка" (callback)

    UnicodeString SelKPComboStrings[4]; //!< Значения для комбо-бокса m_ActiveChannelsCombo
public:		// User declarations
    __fastcall TAdjustingForm(TComponent* Owner);

    void UpdatePBoxData(bool NewItemFlag);
    void DrawPBoxData();

    void Initialize();

    void Clear_Data();
};

const int TAdjustingForm::RailCheckChannels[9] = {0,0x46,0x52,0x5A,0x63,0x6E,0x79,0x84,0x90};
//---------------------------------------------------------------------------
extern PACKAGE TAdjustingForm *AdjustingForm;
//---------------------------------------------------------------------------
#endif
