﻿/**
 * @file ReportViewerUnit.h
 * @author Kirill Beliaevskiy
 */

//---------------------------------------------------------------------------

#ifndef ReportViewerUnitH
#define ReportViewerUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ToolWin.hpp>
#include <VCLTee.TeCanvas.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtDlgs.hpp>
//---------------------------------------------------------------------------

#include "BScanLine.h"
#include <Vcl.ImgList.hpp>
#include <Vcl.Imaging.jpeg.hpp>
#include <Vcl.Grids.hpp>
#include "FiltrParamUnit.h"

#include "BScanMeasureFormUnit.h"
#include "BScanAnalyzeDebugUnit.h"
#include "TestFiltrUnit.h"
#include "FiltrParamUnit.h"

#define REPVIEW_FLAG_BSCAN      0x01 //!< Флаг наличия b-развертки
#define REPVIEW_FLAG_BSCAN_HAND 0x02 //!< Флаг наличия записи b-развертки с ручников
#define REPVIEW_FLAG_ASCAN      0x04 //!< Флаг наличия изображений a-развертки
#define REPVIEW_FLAG_PHONE      0x08 //!< Флаг наличия изображений с телефона (линейность)
#define REPVIEW_FLAG_CAMERA     0x10 //!< Флаг наличия изображений с камеры
#define REPVIEW_FLAG_CHANNELS   0x20 //!< Флаг наличия таблицы каналов (присутствует всегда, но без флага не отобразится)

//! Отрисовывает В-развертку для одного канала
void DrawHandBScanLine(int curHandScanIndex,PsHandSignalsOneCrd coords_arr,int coord_count, TCanvas *Canvas, int Width, int Height);

/** \enum eMeasureState
    \brief Состояние диалога измерения
 */
enum eMeasureState
{
    MEAS_STATE_NONE,
    MEAS_STATE_MEASURE,
    MEAS_STATE_ADD_REGION,
//    MEAS_STATE_WAIT_SELECTLINE,
//    MEAS_STATE_PROCESS_MEASURE,

};

//! Получает флаги наличия в отчете отображаемых элементов (b-развертка - REPVIEW_FLAG_BSCAN, изобр. с камеры - REPVIEW_FLAG_CAMERA, и т.п.)
DWORD GetReportContentFlags(const cJointTestingReport* rep);

//! Форма для отображения отчетов
class TReportViewerForm : public TForm
{
__published:	// IDE-managed Components
    TToolBar *m_MainToolBar;
    TToolButton *BScanBtn;
    TToolButton *LinearityBtn;
    TToolButton *AScanImagesBtn;
    TToolButton *CloseBtn;
    TImageList *ImageList1;
    TToolButton *BScanHandBtn;
    TToolButton *PrintBtn;
    TLabel *m_ReportInfoLabel;
    TToolButton *ShowCameraPhotoBtn;
    TPageControl *PageControl1;
    TTabSheet *BScanPage;
    TImage *BScanImage;
    TTabSheet *AScanImagesPage;
    TGroupBox *AScanGroup;
    TImage *AScanImage;
    TGridPanel *GridPanel1;
    TSpeedButton *AScanImageUpBtn;
    TListBox *AScanListBox;
    TSpeedButton *AScanImageDownBtn;
    TTabSheet *LargePhotoPage;
    TTabSheet *BScanHandPage;
    TGroupBox *BScanHandGroup;
    TPaintBox *BScanHandPBox;
    TGridPanel *GridPanel2;
    TComboFlat *BScanHandCombo;
    TPanel *Panel1;
    TGridPanel *GridPanel3;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label3;
    TLabel *Label4;
    TLabel *Label5;
    TLabel *Label6;
    TLabel *Label7;
    TLabel *Label8;
    TLabel *Label9;
    TLabel *m_EnterAngleLabel;
    TLabel *m_ControlMethodLabel;
    TLabel *m_ControlSurfaceLabel;
    TLabel *m_KuLabel;
    TLabel *m_AttLabel;
    TLabel *m_StGateLabel;
    TLabel *m_EdGateLabel;
    TLabel *m_TVGLabel;
    TLabel *m_PrismDelayLabel;
    TTabSheet *EmptyTab;
    TLabel *Label10;
    TPanel *LargePhotoPageBtnPanel;
    TSpeedButton *PhotoCameraBtn;
    TSpeedButton *PhotoLinearityRightBtn;
    TSpeedButton *PhotoLinearityTopBtn;
    TImage *NoPhotoImage;
    TTabSheet *ChannelParamsPage;
    TStringGrid *CalibTable;
    TPanel *Panel2;
    TBitBtn *PageDownBtn;
    TBitBtn *PageUpBtn;
    TToolButton *ChannelsTableBtn;
    TGroupBox *GroupBox3;
    TLabel *ThLabel;
    TGridPanel *GridPanel6;
    TBitBtn *ThTrackBarLeftBtn;
    TBitBtn *ThTrackBarRightBtn;
    TTrackBar *ThTrackBar;
    TSpeedButton *MeasureBtn;
    TSpeedButton *PhotoLinearityLeftBtn;
    TLabel *Label20;
    TLabel *m_ControlTimeLabel;
	TButton *DebugButton;
    TButton *FilterButton;
    TButton *Button1;
    TPanel *Panel3;
    TImage *PhonePhotoImage;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall CloseBtnClick(TObject *Sender);
    void __fastcall PageControl1Resize(TObject *Sender);
    void __fastcall AScanListBoxClick(TObject *Sender);
    void __fastcall BScanHandPBoxPaint(TObject *Sender);
    void __fastcall AScanImageUpBtnClick(TObject *Sender);
    void __fastcall AScanImageDownBtnClick(TObject *Sender);
    void __fastcall BScanHandComboSelect(TObject *Sender);
    void __fastcall BScanBtnClick(TObject *Sender);
    void __fastcall AScanImagesBtnClick(TObject *Sender);
    void __fastcall LinearityBtnClick(TObject *Sender);
    void __fastcall BScanImageMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
    void __fastcall BScanHandBtnClick(TObject *Sender);
    void __fastcall ThTrackBarChange(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall PrintBtnClick(TObject *Sender);
    void __fastcall ShowCameraPhotoBtnClick(TObject *Sender);
    void __fastcall PhotoLinearityTopBtnClick(TObject *Sender);
    void __fastcall PhotoLinearityRightBtnClick(TObject *Sender);
    void __fastcall PhotoCameraBtnClick(TObject *Sender);
    void __fastcall ThTrackBarLeftBtnClick(TObject *Sender);
    void __fastcall ThTrackBarRightBtnClick(TObject *Sender);
    void __fastcall PageUpBtnClick(TObject *Sender);
    void __fastcall PageDownBtnClick(TObject *Sender);
    void __fastcall ChannelsTableBtnClick(TObject *Sender);
    void __fastcall MeasureBtnClick(TObject *Sender);
    void __fastcall PhotoLinearityLeftBtnClick(TObject *Sender);
    void __fastcall BScanPageResize(TObject *Sender);
    void __fastcall BScanPageHide(TObject *Sender);
    void __fastcall FormHide(TObject *Sender);
	void __fastcall DebugButtonClick(TObject *Sender);
    void __fastcall FilterButtonClick(TObject *Sender);
    void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
    DWORD ViewFlags;        //!< Флаги отображаемых элементов отчета. Устанавливаются при показе формы, отражают наличие элементов в отчете.
    TBScanLine bsl;         //!< Для рисования В-развертки по одному каналу.
    float BScanHandScale;   //!< Масштаб В-развертки по одному каналу.
    cBScanPainter painter;  //!< Для рисования В-развертки по всем каналам.

    void setReportFlags(DWORD flags); //!< Установка флагов отчета

    //Взято для отрисовки восстановленной А-развертки
    int AScanSysCoord;
    Shift_CID_List Shift_CID;
    int Shift_CID_Cnt;
    BScan_Color_List BSColor;
    void ShowAScanOnBScan(int X, int Y, int showInfoId = -1);
    void ReDrawAScanOnBScan();
    //^^^ Взято для отрисовки восстановленной А-развертки

    TArchiveForm* pArchive;          //!< Указатель на форму архива (функция печати находится там)
    UnicodeString protocolPath;      //!< Путь для сохранения протокола


    void __fastcall OnAScanFrameLeftBtnClick(TObject *Sender);
    void __fastcall OnAScanFrameRightBtnClick(TObject *Sender);
    void __fastcall OnAScanFrameCloseBtnClick(TObject *Sender);

    eMeasureState measureState;
    int measureKP;
    int measureLine;
    PolyPt measureXY;
    int editedRegionIdx;
    void setMeasureState( eMeasureState state);
    void OnBScanMeasureFormMessage(eBScanMeasureMsgType Type, void* pValue);

    void UpdateReport();    //!< Обновление отчета. Перерисовка всех элементов.
    void UpdateBScan(bool NewItemFlag = false); //!< Обновление В-развертки
    void DrawBScan();                           //!< Перерисовка В-развертки
    void UpdateBScanHand();                     //!< Перерисовка В-развертки из ручников
    void UpdateAScan();                         //!< Перерисовка изображений А-развертки
    void UpdatePhonePhoto(int index = 0);       //!< Перерисовка изображений с телефона
    void UpdateCameraPhoto();                   //!< Перерисовка изображений с камеры
    void UpdateChannelsTable();                 //!< Перерисовка таблицы каналов
    void AdjustChannelsTableColumnsWidth();     //!< Подгонка ширины столбцов таблицы каналов под содержимое.
public:		// User declarations
    __fastcall TReportViewerForm(TComponent* Owner);
    void SetArchiveForm(TArchiveForm* pArch,UnicodeString protocolPath);
};
//---------------------------------------------------------------------------
extern PACKAGE TReportViewerForm *ReportViewerForm;
//---------------------------------------------------------------------------

extern cAutoconMain *AutoconMain;
#endif
